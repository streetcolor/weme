<?php
//Controleur pour la gestion de la home
//pour la gestion des dashboard client et prestataire
class reponseController extends controller{
        
        public function __construct($smarty, $request){
                
                parent::__construct($smarty, $request);
                
                //On afficje nos eventuels message de callback setté depuis les autres controlleur
                if($messageCallBack=Tools::getFlashMessage('success-send-message')){
                           $this->smarty->assign('messageCallBack', $messageCallBack);
                }
                elseif($messageCallBack=Tools::getFlashMessage('success-update')){
                          $this->smarty->assign('messageCallBack', $messageCallBack);
                }
                //Fin de la partie

        }
        
        function defaultAction($search=array()){
                
                
                $DBmembre = new DBMembre();
       
                if(DBMembre::$id_type==DBMembre::TYPE_PRESTATAIRE){
                        if($this->request['page']=='new_response')
                                $this->addReponseAction();
                        
                        elseif($this->request['page']=='response')
                                
                                $this->reponseAction();
                        
                        
                        else{
                           

                                $_SESSION['search'] = $search;//si on passe par la methode searchAction on passe la parametre dans la session
                                
                                $DBreponse = new DBReponse();
                                
                                // Numero de page (1 par défaut)
                                $paginate['current_page'] = (isset($_GET['paginate']) && is_numeric($_GET['paginate'])) ? $_GET['paginate'] : 1;
                                // Numéro du 1er enregistrement à lire
                                $paginate['limit']        = ($paginate['current_page'] - 1) * NOMBRE_PER_PAGE;    
                                //on recupere la liste de nos CV pour le comptage
                                $listReponses             = $DBreponse->getListReponses(array('id_identifiant'=>$this->membre->getId_identifiant()), $search, array($paginate['limit'], NOMBRE_PER_PAGE));
                
                                // Pagination
                                $paginate['total_pages']  = ceil($listReponses['count'] / NOMBRE_PER_PAGE);
                               
                                //Si on du monde en sortie on les recupere
                                if($listReponses['count']){
                
                                    //Puis on les envoie dans la vue
                                    $this->smarty->assign('nbrPages', $paginate['total_pages']);
                                    $this->smarty->assign('pageCourante', $paginate['current_page']);
                                    $this->smarty->assign('listReponses', $listReponses['reponses']);
                                    $this->smarty->assign('count', $listReponses['count']);
                
                                }
                                $DBadmin = new DBAdmin();
                                $statutEntete = array();
                                $statutSelect = array();
                                foreach($DBadmin->getListStatutsReponses() as $statut){
                                        $statutEntete[$statut->id_reponse_statut] = $statut->id_reponse_statut;//desinée à etre affiché dans les groupe entete de statut mission
                                        $statutSelect[$statut->id_reponse_statut] = $statut;//destinée pour le selecteur
                                }
                                $this->smarty->assign('getListEntete', $statutEntete);
                                $this->smarty->assign('getListStatuts', $statutSelect);
                        }
                }
                else if(DBMembre::$id_type==DBMembre::TYPE_CLIENT){
                        //on unest la session de recherche directement
                unset($_SESSION['search']);
                        if($this->request['page']=='response'){
                                
                                $this->reponseAction();
                        }
                        else{
                                                        
                                $_SESSION['search'] = $search;//si on passe par la methode searchAction on passe la parametre dans la session
                                
                                $filtreDefault = array('id_identifiant'=>$this->membre->getId_identifiant(), 'id_client'=>$this->membre->getId_client(), 'id_mission'=>$this->crypt->id_mission);   
                              
                                $DBreponse = new DBReponse();
                                
                                // Numero de page (1 par défaut)
                                $paginate['current_page'] = (isset($_GET['paginate']) && is_numeric($_GET['paginate'])) ? $_GET['paginate'] : 1;
                                // Numéro du 1er enregistrement à lire
                                $paginate['limit']        = ($paginate['current_page'] - 1) * NOMBRE_PER_PAGE;    
                                //on recupere la liste de nos CV pour le comptage
                                $listReponses             = $DBreponse->getListReponses($filtreDefault, $search, array($paginate['limit'], NOMBRE_PER_PAGE));
                
                                // Pagination
                                $paginate['total_pages']  = ceil($listReponses['count'] / NOMBRE_PER_PAGE);
                               
                                //Si on a du monde en sortie on les recupere
                                if($listReponses['count']){
                
                                    //Puis on les envoie dans la vue
                                    $this->smarty->assign('nbrPages', $paginate['total_pages']);
                                    $this->smarty->assign('pageCourante', $paginate['current_page']);
                                    $this->smarty->assign('listReponses', $listReponses['reponses']);
                                    $this->smarty->assign('count', $listReponses['count']);
                                    $this->smarty->assign('mode', 'normal');//permet d'ajouter un paramtrer dans lien favoris des reponse
                
                                }
                                $DBadmin = new DBAdmin();
                                
                                $statutEntete = array();
                                $statutSelect = array();
                                foreach($DBadmin->getListStatutsReponses() as $statut){
                                        $statutEntete[$statut->id_reponse_statut] = $statut->id_reponse_statut;//desinée à etre affiché dans les groupe entete de statut mission
                                        $statutSelect[$statut->id_reponse_statut] = $statut;//destinée pour le selecteur
                                }
                                $this->smarty->assign('getListEntete', $statutEntete);
                                $this->smarty->assign('getListStatuts', $statutSelect);
                                $this->smarty->assign('id_mission', $this->crypt->id_mission);
                        }
                                                   

                }
                else{
                        //On est pas connecté :)
                        if($this->request['page']=='new_response'){
                                //On recoit un lien crypté par mail qui doit forcer passer par une connexion de compte
                                $this->request['codeToken'] = $this->crypt->codeToken;
                                $this->request['usr_email'] = $this->crypt->usr_email;
                                include(_CTRL_.'home.php');
                                $home = new homeController($this->smarty, $this->request);
                                $home->connexionAction();
                               
                        }               
                }

                
        }
        function searchAction(){

                if($this->request['id_reponse_statut'])
                        $this->request['search']['id_reponse_statut']=$this->request['id_reponse_statut'];
                
                if($this->request['id_mission'])
                         $this->request['search']['id_mission']=$this->request['id_mission'];
                
                if($this->request['search'])
                        $_SESSION['search'] = $this->request['search'];
                                
                $this->defaultAction($_SESSION['search']);
                $this->smarty->assign('mode', 'search');//permet d'ajouter un paramtrer dans lien des reponses
                $this->smarty->assign('post', $_SESSION['search']);//très pratique puisque il permet de ressortir le post dans la vue

  
        }
        //ajouter une reponse
        function addReponseAction(){

                $request = $this->request;
               
                $DBmembre       = new DBMembre();
                $id_identifiant = DBMembre::$id_identifiant;


                $this->smarty->assign('typeAgence', DBMembre::TYPE_AGENCE);
                
                //Si il s'agit d'une agence on recupere ses petits employes !
                if($this->membre->getStructure()->id_structure == DBMembre::TYPE_AGENCE){
                        $profils = $this->membre->getProfils(true);
                        if($profils->count>0)
                                $this->smarty->assign('getListProfils', $profils->profils);
                }
                else{
                        //QUand on traite un freelance ou un intermittent
                        $request['id_prestataire_profil'][] = $this->membre->getId_profil();//On va l'enregistrer dircetement en BDD (pas besoin de le checker via un input type checkbox puisque je ne suis pas une agence !)
                }
                
                //Recuperation de la mission sur la quelle on souhaite repondre
                //On appele une depuis le controlleur mission
                include(_CTRL_.'mission.php');
                $mission = new missionController($this->smarty,  $this->request);             
                $mission = $mission->missionAction();
          
                //recuperation des réalisations 
                $DBfile       = new DBFile;
                $getListFiles = $this->membre->getRealisations();
                if($getListFiles->count>0)
                        $this->smarty->assign('getListFiles', $getListFiles);
                
                //recuperation des tables adm necessaires
                $DBadmin  = new DBAdmin();
                $this->smarty->assign('getListMonnaies', $DBadmin->getListMonnaies());
                $this->smarty->assign('getListContrats', $DBadmin->getListContrats());
                
                if($_POST){

                        $DBreponse = new DBReponse();
                        $request['id_identifiant']   = $id_identifiant;
                        $request['id_mission']       = $this->crypt->id_mission;
                        $request['mission']          =  $mission;//Le client souhaite il recevoir les reponses par email ?

                        
                        if($error = $DBreponse->makeReponse($request)){

                            if(is_array($error) && count($error)>0){
                                $this->smarty->assign('post', $_POST);
                                $this->smarty->assign('error', $error);
                            }
                            else{
                                Tools::setFlashMessage(XMLEngine::$trad->global_trad->notifReponseTransmise, true, 'success', Tools::getLink('responses', '', array('id_prestataire_profil'=>$this->crypt->id_prestataire_profil), true));
                            }
                        }
                        
                }
        }
        //Afficher une réponse cote client
        function reponseAction(){
                $DBmembre = new DBMembre();
                if(DBMembre::$id_type==DBMembre::TYPE_PRESTATAIRE){
                    
                        $filtreDefault = array('id_identifiant'=>$this->membre->getId_identifiant(), 'id_mission_reponse'=>$this->request['id_mission_reponse']);
                        $DBreponse = new DBReponse();
                                        
                        //on recupere la liste de nos reponse pour le comptage
                        $reponse             = $DBreponse->getListReponses($filtreDefault);

                        //Si on du monde en sortie on les recupere
                        if($reponse['count']){
                                $reponse      = $reponse['reponses'][0];
                                
                                //on recupre la mission depuis nore controller mission
                                //Il s'occupe lui meme d'assigner les varibales smarty
                                include(_CTRL_.'mission.php');
                                $mission = new missionController($this->smarty, $this->request);
                                $mission->missionAction($reponse->getId_mission(), true);
                                
                                $profils      = $reponse->getProfils();
                             
                                $realisations = $reponse->getRealisations();
                                $pj           = $reponse->getPieceJointe();
                               
                                $this->smarty->assign('reponse', $reponse);
                                $this->smarty->assign('profils', $profils);
                                $this->smarty->assign('realisations', $realisations);
                                $this->smarty->assign('pj', $pj);
                        
                        }
                        else{
                                header('location:'.Tools::getLink('responses'));
                        }
                        
                }
                elseif(DBMembre::$id_type==DBMembre::TYPE_CLIENT){
        
                        $filtreDefault = array('id_identifiant'=>$this->membre->getId_identifiant(), 'id_client'=>$this->membre->getId_client(), 'id_mission_reponse'=>$this->crypt->id_mission_reponse);
        
                        $DBreponse = new DBReponse();
                                        
                        //on recupere la liste de nos reponse pour le comptage
                        $reponse             = $DBreponse->getListReponses($filtreDefault);

                        //Si on du monde en sortie on les recupere
                        if($reponse['count']){
                                
                                $mission  = $reponse['reponses'][0]->getMission();
                                $profils  = $reponse['reponses'][0]->getProfils();
                            
                            
                                 //Soyons fou et recuperrons l'hitorique des reponses pour la mission donnée
                                $getOtherReponses  = $DBreponse->getListReponses(array('id_mission'=>$mission->getId_mission()));
                               
                                
                                $this->smarty->assign('profils', $profils);
                                $this->smarty->assign('reponsesArchive', $getOtherReponses);
                                $this->smarty->assign('reponse', $reponse['reponses'][0]);
                                $this->smarty->assign('pj', $reponse['reponses'][0]->getPieceJointe());
                                $this->smarty->assign('realisations', $reponse['reponses'][0]->getRealisations());
                                $this->smarty->assign('mission', $mission);
                                $this->smarty->assign('id_reponse', $reponse['reponses'][0]->getId_reponse());
                                $this->smarty->assign('crypt', Tools::encrypt('forceabonnement=true'));
                                $this->smarty->assign('mode', 'detail');//Le mode sera utile pour s'envoyer la reponse par mail directement depuis le détail
                               
                                
                                //On appele l'action send depuis le controlleur message depuis le controlleur membre
                                include(_CTRL_.'message.php');
                                $messageCtl = new messageController($this->smarty,  $this->request);             
                                $messageCtl->sendReponseAction($this->membre, $reponse['reponses'][0]->getEmail());
                        }
                        else{
                                header('location:'.Tools::getLink('missions'));
                        }
                }
        }
          
        //Changer le statut d'une reponse
        function changeStatusAction(){

                if($_POST){
                       
                        $currentPage = is_numeric($this->crypt->paginate) ? $this->crypt->paginate : $this->request['paginate'];
                        $mode        = ($this->crypt->mode) ? $this->crypt->mode : $this->request['mode'];
                        if($mode=='search'){
                                $link =  Tools::getLink($this->request['page'], 'search', array('id_mission'=>$this->crypt->id_mission), true).'&paginate='.$currentPage;
                        }
                        else{
                                $link =  Tools::getLink($this->request['page'], '', array('id_mission'=>$this->crypt->id_mission), true).'&paginate='.$currentPage;
                        }
                       
                        $DBmembre = new DBMembre();
                        $DBreponse = new DBReponse();
                        $this->request['id_mission'] = $this->crypt->id_mission;
                        $this->request['id_identifiant'] = DBMembre::$id_identifiant;
                        if($error = $DBreponse->editReponse($this->request)){
                                
                                if(is_array($error) && count($error)>0){
                                    $this->smarty->assign('post', $_POST);
                                    $this->smarty->assign('error', $error);
                                    $this->defaultAction();
                                }
                                else{
                                    Tools::setFlashMessage(XMLEngine::$trad->global_trad->notifUpdateOK, true, 'success-update', $link);
                                }  
                        }

                }
             
        }
        //Archiver une reponse
        function archiverAction(){

                $currentPage = is_numeric($this->crypt->paginate) ? $this->crypt->paginate : $this->request['paginate'];
                $mode        = ($this->crypt->mode) ? $this->crypt->mode : $this->request['mode'];
                if($mode=='search'){
                        $link =  Tools::getLink($this->request['page'], 'search', array('id_mission_reponse'=>$this->crypt->id_mission_reponse, 'id_mission'=>$this->crypt->id_mission), true).'&paginate='.$currentPage;
                }
                if($mode=='home'){
                        $link =  Tools::getLink('home');
                }
                else{
                        $link =  Tools::getLink($this->request['page'], '', array('id_mission_reponse'=>$this->crypt->id_mission_reponse, 'id_mission'=>$this->crypt->id_mission), true).'&paginate='.$currentPage;
                }

               
                $DBmembre = new DBMembre();
                $DBreponse = new DBReponse();
                $this->request['id_mission_reponse']  = array($this->crypt->id_mission_reponse);
                $this->request['id_mission']          = $this->crypt->id_mission;
                $this->request['id_identifiant']      = DBMembre::$id_identifiant;
                $this->request['rp_archiver']         = true;
                
                if($error = $DBreponse->editReponse($this->request)){
                        
                        if(is_array($error) && count($error)>0){
                            $this->smarty->assign('post', $_POST);
                            $this->smarty->assign('error', $error);
                            $this->defaultAction();
                        }
                        else{
                            Tools::setFlashMessage(XMLEngine::$trad->global_trad->notifUpdateOK, true, 'success-update', $link);
                        }  
                }

              
             
        }
        //ENvoyer une copie d'une reponse par mail
        function sendCopyAction(){
                if($_POST && !count($_POST['id_mission_reponse'])){
                        $error['id_mission_reponse'] ="Aucun profil slectionnée!";
                        $this->smarty->assign('error', $error);
                        $this->defaultAction();
                }
                else{
                        //On appele la methode d'envoie de copy depuis le controller message
                        //c'est son role apres tout
                        //cette methode ci ne sert qu'a simplifier la passerelle entre la reponse et l'envoi du message
                        include(_CTRL_.'message.php');
                        $message     = new messageController($this->smarty,  $this->request);             
                        $message->sendCopyAction($this->membre);
                }
             
        }    
           
}
?>
