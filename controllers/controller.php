<?php
class controller {
    
    protected $smarty;
    protected $request;
    protected $crypt;
    protected  $membre; //On va stocker notre client ou notre prestataire dans cet attribut et le recuperer dans les autres controllers via $this->membre
    public static $singleton_membre_call = array('presta'=>0, 'client'=>0); // ce singleton permet de conserver les appel au methode getPresta et getClient, on evite ainsi de rappeler deux fois la meme methode
    public static $singleton = array();
    
    public function __construct($smarty, $request){
        $this->smarty          = $smarty;
        $this->request         = $request;
        
	//Succes message commun a tous les controller
	if($messageCallBack=Tools::getFlashMessage('success')){
	    $this->smarty->assign('messageCallBack', $messageCallBack);
	}
	
        if(isset($request['crypt'])){
            $query = Tools::decrypt($_REQUEST['crypt']);
            parse_str($query, $output);//DBManager::debugVar($output, false);
            $this->crypt = (object) $output;
        }
        
        if(!$this->request["page"])
            $this->request["page"] = 'home';
        $this->smarty->assign('page', $this->request["page"]);
        
        //On va grace � ce code r�cuperer les controller qui sont app�l� dans la page
        $calledClass = get_called_class();
        if(!in_array($calledClass, self::$singleton)){
            self::$singleton[] =$calledClass;
        }
        
        //Si je suis conn�ct� on va loader automatiquement des infos de base
        $DBmembre       = new DBMembre();
		
        if(DBMembre::$id_type==DBMembre::TYPE_PRESTATAIRE){
            $this->membre = $this->prestataireAction();
            self::$singleton_membre_call['presta'] ++;
        }
        elseif(DBMembre::$id_type==DBMembre::TYPE_CLIENT){
            $this->membre = $this->clientAction();
            self::$singleton_membre_call['client'] ++;
        }
	
	
	//Gestion des date format pour les date pickers
	$datePicker = array(
			    'fr'=>array('dateFormat'=>"dd/mm/yyyy", "dateDate"=>'%d/%m/%Y'),
			    'en'=>array('dateFormat'=>"yyyy-mm-dd", "dateDate"=>'%Y-%m-%d'),
			    'es'=>array('dateFormat'=>"dd/mm/yyyy", "dateDate"=>'%d/%m/%Y')
			    );
	
        $smarty->assign('datepicker', $datePicker[$_SESSION['langue']]);
    
        $smarty->assign('environnement', $environnement);
        
    }
    
  
    
    //Pour recuperer un prestataire 
    function prestataireAction(){
	if(!self::$singleton_membre_call['presta']){
            $DBmembre       = new DBMembre();
            $id_identifiant = DBMembre::$id_identifiant;//Tools::debugVar($id_identifiant);
            
            //On recupere le prestataire par default afin de d'affciher des infos dans le Header
            $getPrestataire = $DBmembre->getListPrestataires(array('id_identifiant'=>$id_identifiant));//Tools::debugVar($getPrestataire);
            $getPrestataire = $getPrestataire["prestataires"][0];
            $this->smarty->assign('avatar', $getPrestataire->getAvatar());
            if($getPrestataire->getStructure()->id_structure == DBMembre::TYPE_AGENCE){
                $this->smarty->assign('plaquette', $getPrestataire->getPlaquette());
                $this->smarty->assign('agence', true);
            }
            $this->smarty->assign('favorisMission', $getPrestataire->getFavoris()->favoris);
            $this->smarty->assign('prestataire', $getPrestataire);
            
            return $getPrestataire;
        }
 
    }
    //Pour recuperer un client 
    function clientAction(){
	
        if(!self::$singleton_membre_call['client']){
            $DBmembre       = new DBMembre();
            $id_identifiant = DBMembre::$id_identifiant;
          
            //On recupere le prestataire par default afin de d'affciher des infos dans le Header
            $getClient = $DBmembre->getListClients(array('id_identifiant'=>$id_identifiant));
            $getClient = $getClient["clients"][0];
            // Tools::debugVar($getClient->getFavoris());
            $this->smarty->assign('avatar', $getClient->getAvatar());
            //Tools::debugVar($getClient->getFavoris());
            $this->smarty->assign('favorisProfil', $getClient->getFavoris()->favoris);
            $this->smarty->assign('client', $getClient);
            //On va setter notre abonnement pour s'en servir dans le les TPL
            $this->smarty->assign('abonnement', Client::$abonnement);

            return $getClient;          
        }
     
    }
    
}

?>
