<?php
//Controleur pour la gestion de la CVteque
//pour la gestion des missions
class cvtequeController extends controller {
  
   public function __construct($smarty, $request){
			
	parent::__construct($smarty, $request);
        if($messageCallBack=Tools::getFlashMessage('success-send-message')){
                $this->smarty->assign('messageCallBack', $messageCallBack);
        }
                                
        if($messageCallBack=Tools::getFlashMessage('success-send-favoris-message')){
                $this->smarty->assign('messageCallBack', $messageCallBack);
        }
	
	//Pour le moteur de recherche
	$publications = array(
	   ""         => XMLEngine::$trad->global_trad->label_publication,
	   "1 DAY"    => XMLEngine::$trad->global_trad->today,
	   "7 DAY"    => XMLEngine::$trad->global_trad->minus_week,
	   "1 MONTH"  => XMLEngine::$trad->global_trad->minus_month,
	   "6 MONTH"  => XMLEngine::$trad->global_trad->minus_six_month
	);
    	$this->smarty->assign('listPublications', $publications);
	
   }

   function defaultAction($search = array(), $limitForPagine = true){
       unset($_SESSION['search']);
	   $smarty  = $this->smarty;
	   $request =$this->request;
	   $DBmembre= new DBMembre();
	   $DBadmin = new DBAdmin();
   
	    $filtreDefault = array('pf_profil_statut'=>1, 'pf_pourcentage'=>'20'); //Le filtre de recheche de base obligtatoire

	   if($request['page']){
		      
		      if(DBMembre::$id_type==DBMembre::TYPE_CLIENT){
       
			   
			   if($this->request['page']=='my_profils'){
			       $listFavoris = $this->membre->getFavoris();
			       
			       $listFavoris = ($listFavoris->count>0) ? $listFavoris->favoris : array();
			       
			       $filtreDefault = array('pf_profil_statut'=>1, 'id_prestataire_profil'=>$listFavoris); //Le filtre de recheche de base obligtatoire
			   
 
			   }
		       
		      }
		      else if(DBMembre::$id_type==DBMembre::TYPE_PRESTATAIRE){
		      }
		      else{
		       //Non conecté
		       
		      }
	   }
	   
	   $_SESSION['search'] = $search;//si on passe par la methode searchAction on passe la parametre dans la session
	   
	   // Numero de page (1 par défaut)
	   $paginate['current_page'] = (isset($_GET['paginate']) && is_numeric($_GET['paginate'])) ? $_GET['paginate'] : 1;
	   // Numéro du 1er enregistrement à lire
	   $paginate['limit']        = ($paginate['current_page'] - 1) * NOMBRE_PER_PAGE;    
	   //On affiche une limite ou pas
	   $limit = $limitForPagine ? array($paginate['limit'], NOMBRE_PER_PAGE) : false;

	   //on recupere la liste de nos CV pour le comptage
	   $listCv   = $DBmembre->getListProfils($filtreDefault, $search, $limit);
	   
	   // Pagination
	   $paginate['total_pages']  = ceil($listCv['count'] / NOMBRE_PER_PAGE);
	   
	   //Si on du monde en sortie on les recupere
	   if($listCv['count']){
	       
	       //Puis on les envoie dans la vue
	       $smarty->assign('nbrPages', $paginate['total_pages']);
	       $smarty->assign('pageCourante', $paginate['current_page']);
	       $smarty->assign('listCv', $listCv['profils']);
	       $smarty->assign('count', $listCv['count']);
	       $smarty->assign('mode', 'normal');//permet d'ajouter un paramtrer dans lien favoris des profil
	       
	       

	   }
       //Pour le moteur de recherche    
       $DBadmin = new DBAdmin();
       $smarty->assign('listExperiences', $DBadmin->getListExperiences());

	   
   }
   
   
	   
	   
   function cvAction(){
	       
	       $DBmembre = new DBMembre();
	       
	       $decyptForceAbonnement = ($this->crypt->forceabonnement) ==true ?  true : false;//on decrypt depuis la page reponse afin de telecharger un CV sans infos crypée

	       $filtreDefault = array('pf_profil_statut'=>1, 'id_prestataire_profil'=>array($this->request['id_prestataire_profil'])); //Le filtre de recheche de base obligtatoire
	       $cv   = $DBmembre->getListProfils($filtreDefault,  $searchFiltre=array(), $limit=false, $matching=false, $forceAbonnement=$decyptForceAbonnement);

	      if($cv['count']>0){
		    $cv = $cv['profils'][0];
		    $this->smarty->assign('cv', $cv);//permet d'ajouter un paramtrer dans lien favoris des profil
		    $this->smarty->assign('cvComp', $cv->getCompetences());
		    $this->smarty->assign('cvExp', $cv->getExperiences());
		    $this->smarty->assign('cvForm',$cv->getFormations());
		    $this->smarty->assign('cvLang',$cv->getLangues());
		    $this->smarty->assign('cvPostes',$cv->getPostes());
	      }else{
	      header("location:http://".$_SERVER['HTTP_HOST']);
	      exit;
	      }
   }          
   function searchAction(){
       
       
       if($this->crypt->id_prestataire_profil)
		   $this->request['search']['id_prestataire_profil']=$this->crypt->id_prestataire_profil;
       
       if($this->request['search'])
	   $_SESSION['search'] = $this->request['search'];
       
       $this->defaultAction($_SESSION['search'] );
       $this->smarty->assign('mode', 'search');//permet d'ajouter un paramtrer dans lien des profil
       $this->smarty->assign('post', $_SESSION['search']);//très pratique puisque il permet de ressortir le post dans la vue


   }
   //ajouter un favoris profil pour un client
   function addFavoriAction(){
       
       if($this->crypt->mode=='search'){
	   $link =  Tools::getLink($this->request['page'], 'search', array('paginate'=> $this->crypt->paginate));
       }
       else if($this->crypt->mode=='home'){
	   $link =  Tools::getLink('home');
       }
       else if($this->crypt->mode=='normal'){
	    $link =  Tools::getLink('cvteque', '', array('paginate'=> $this->crypt->paginate));
       }
       else if($this->crypt->mode=='my_profils'){
	    $link =  Tools::getLink('my_profils', '', array('paginate'=> $this->crypt->paginate));
       }
       
       $DBmembre = new DBMembre();
       $favoris = array();
       $favoris["id_identifiant"]=DBMembre::$id_identifiant;
       if(is_numeric($this->crypt->id_prestataire_profil)){
	   //le favoris correspond à une sauvegarde de profil
	   //donc pour un cliuent

	   $favoris["id_prestataire_profil"]=$this->crypt->id_prestataire_profil;
	   
	   $DBmembre->makeFavori($favoris);
	   Tools::setFlashMessage(XMLEngine::$trad->global_trad->notifProfilAjoute, true, 'success', $link);
	 
       }
   }
   
   //proposer une mission aux favoris profolf pour un client
   //Quand on clique sur envoyer aux profil selctionné => sendFavorisAction() controller send
   function proposeMissionAction(){
       //Si on a recu un message de notification
       
       $this->defaultAction(array(), false);
       $this->smarty->assign('hideSearch', true);
       
   }
   //supprimer un favoris profil pour un client
   function removeFavoriAction(){
        if(!is_numeric($this->crypt->id_prestataire_profil)){
	    header('location:'.Tools::getLink('my_profils'));
	}
        if($this->crypt->mode=='search'){
            $link =  Tools::getLink($this->request['page'], 'search', array('paginate'=> $this->crypt->paginate));
        }
        else if($this->crypt->mode=='home'){
            $link =  Tools::getLink('home');
        }
        else if($this->crypt->mode=='normal'){
             $link =  Tools::getLink('cvteque', '', array('paginate'=> $this->crypt->paginate));
        }
        else if($this->crypt->mode=='my_profils'){
             $link =  Tools::getLink('my_profils', '', array('paginate'=> $this->crypt->paginate));
        }
        
        $DBmembre = new DBMembre();
        $favoris = array();
        $favoris["id_identifiant"]=DBMembre::$id_identifiant;
        if(is_numeric($this->crypt->id_prestataire_profil)){
            //le favoris correspond à une sauvegarde de profil
            //donc pour un client
            $favoris["id_prestataire_profil"]=$this->crypt->id_prestataire_profil;
            $DBmembre->removeFavori($favoris);
            Tools::setFlashMessage(XMLEngine::$trad->global_trad->notifProfilSupprime, true, 'success', $link);
          
        }
    }
}
?>