<?php
//Controleur pour la gestion des membres
//pour la gestion des clients et prestataires 
class membreController extends controller{
    
    public function __construct($smarty, $request){
			
	parent::__construct($smarty, $request);
			
	if($messageCallBack=Tools::getFlashMessage('success-manager')){
	    $this->smarty->assign('messageCallBack', $messageCallBack);
	}
	elseif($messageCallBack=Tools::getFlashMessage('success-realisation')){
	    $this->smarty->assign('messageCallBack', $messageCallBack);
	}
	
    }
    
    function defaultAction(){
        $smarty  = $this->smarty;
        $request =$this->request;
        
        $DBmembre       = new DBMembre();

        if(DBMembre::$id_type==DBMembre::TYPE_CLIENT){

            if($request['page']=='edit_moncompte')
                $this->editClientAction($smarty, $request);
                
            elseif($request['page']=='new_manager'){
                		
		if($this->membre->getId_role()==DBMembre::TYPE_NORMAL){
		    
		    header('location:'.Tools::getLink("home"));
		}
		
		$DBadmin        = new DBAdmin();
		$listCivilites = $DBadmin->getListCivilites();
    
		$this->smarty->assign('getListCivilites', $listCivilites);
			  
		
		
		$listManagers = $this->membre->getManagers();

		if($listManagers->count>0){
		     $this->smarty->assign('listManagers', $listManagers->managers);
		}

		$this->smarty->assign('count', $listManagers->count);
            }
            else{
                //On recupere le client par default afin de d'affciher des infos dans le Header
                $this->clientAction();
            }
        }
        
        elseif(DBMembre::$id_type==DBMembre::TYPE_PRESTATAIRE){//On est un prestataire
            
            //Jamais on oublie de définir une action sur la page edit_moncompte
            //Ou que tout simplement on estime que de passer une action dans l'url juste pour lister du contenu ne sert à rien
            //style : index.php?page=realisations&action=listrealisations
            if($request['page']=='edit_moncompte')
                $this->editPrestataireAction($smarty, $request);
                 
            elseif($request['page']=='edit_cv')
                $this->editProfilAction($smarty, $request);
                 
            elseif($request['page']=='realisations')
                $this->editRealisationsAction($smarty, $request);
            
            elseif($request['page']=='new_profil'){
		
                $this->addProfilAction($smarty, $request);
	    }
            else
                //On recupere le prestataire par default afin de d'affciher des infos dans le Header
                $this->prestataireAction();

        }
        else{
            //On n'est pas connecté :)
            //DOnc on va recuperer des infos pour les formlaire d'inscription
            //Jamais on oublie de définir une action sur la page edit_moncompte
            if($this->request['page']=='pr_inscription'){
                $DBadmin = new DBAdmin();
                $this->smarty->assign('getListStrcutures', $DBadmin->getListStructures());
                $this->smarty->assign('getListCivilites', $DBadmin->getListCivilites());
		$this->smarty->assign('captcha', md5(uniqid()));
            }
            elseif($this->request['page']=='cl_inscription'){
                 $DBadmin = new DBAdmin();
                $this->smarty->assign('getListStrcutures', $DBadmin->getListStructures());
                $this->smarty->assign('getListCivilites', $DBadmin->getListCivilites());
		$this->smarty->assign('captcha', md5(uniqid()));
                 //$this->editProfilAction($smarty, $request);
            }
        }
        
    }
    //Quand on modifie le prestataire depuis la page edit_moncompte
    function editPrestataireAction(){
        $smarty  = $this->smarty;
        $request =$this->request;
         
        $DBmembre       = new DBMembre();
        $DBadmin        = new DBAdmin();
        $id_identifiant = DBMembre::$id_identifiant;
                
        $listCivilites = $DBadmin->getListCivilites();
        $smarty->assign('getListCivilites', $listCivilites);
        
        if($_POST){

            $request['id_identifiant']= $id_identifiant;
            $request['id_prestataire_profil']= $this->membre->getId_profil();
            
            //Si on effectue le submit (update de mon compte)
            if($error = $DBmembre->editPrestataire($request)){
    
                if(is_array($error) && count($error)>0){
            
                    $smarty->assign('error', $error);
                }
                else{
                    Tools::setFlashMessage(XMLEngine::$trad->global_trad->notifUpdateOK, true, 'success', Tools::getLink($this->request['page']));
                }
            }
        }
        elseif(isset($request["crop"]) && isset($request["image"])){//On vient de charger un avatar
            $pathCrop = 'web/data/'.$id_identifiant.'/avatar/tmp/'.$request["image"];
            $smarty->assign('pathCrop', $pathCrop);
        }
        
        
    }
    //Quand on modifie le client depuis la page edit_moncompte
    function editClientAction(){
        $smarty  = $this->smarty;
        $request =$this->request;
        
        
        $DBmembre       = new DBMembre();
        $DBadmin        = new DBAdmin();
        $id_identifiant = DBMembre::$id_identifiant;
        
        $listCivilites = $DBadmin->getListCivilites();
        $smarty->assign('getListCivilites', $listCivilites);
	$smarty->assign('role', $this->membre->getId_role());
	
		    

        if($_POST || $_FILES){
	    $_POST = true;
            $request['id_identifiant']    = $id_identifiant;
            $request['id_client_manager'] = $this->membre->getId_manager();
            $request['id_client']         = $this->membre->getId_client();
	    $request['id_role']           = $this->membre->getId_role();
	    
	    
            //Si on effectue le submit (update de mon compte)
            if($error = $DBmembre->editClient($request)){
    
                if(is_array($error) && count($error)>0){
            
                    $smarty->assign('error', $error);
                }
                else{
                    Tools::setFlashMessage(XMLEngine::$trad->global_trad->notifUpdateOK, true, 'success', Tools::getLink('edit_moncompte'));
                }
            }
        }
        elseif(isset($request["crop"]) && isset($request["image"]) && !empty($request["image"])){//On vient de charger un avatar
            $pathCrop = 'web/data/'.$id_identifiant.'/avatar/tmp/'.$request["image"];
	    if(file_exists($pathCrop))
		$smarty->assign('pathCrop', $pathCrop);
        }
        
    }
    //Pour faire une inscription
    function inscriptionAction(){
        $smarty  = $this->smarty;
        $request =$this->request;
        
        $DBmembre = new DBMembre();
    
        if($request['page']=='cl_inscription')
            $request['id_type']=DBMembre::TYPE_CLIENT;
        elseif($request['page']=='pr_inscription')
            $request['id_type']=DBMembre::TYPE_PRESTATAIRE;
        else//On evite l'envoi depuis un formualaire externe
            exit;
        if($_POST){
            if($error = $DBmembre->makeInscription($request)){
                           
                if(is_array($error) && count($error)>0){
                    $this->defaultAction();
                    $smarty->assign('post', array_map(htmlspecialchars, $_POST));
                    $smarty->assign('error', $error);
                }
                else{
		    include(_CTRL_.'home.php');
		    $home = new homeController($smarty, $request);
		    $home->connexionAction();
		    
                    Tools::setFlashMessage(XMLEngine::$trad->global_trad->notifConnexionOK, true, 'success', Tools::getLink('home'));
                }
            }
        }
    }
    //ajouter un profil
    function addProfilAction(){
        $DBmembre= new DBMembre();


        //Si il s'agit d'un Freelance ou Intermmittent il ne possede que UN profil !
        if($this->membre->getStructure()->id_structure == DBMembre::TYPE_AGENCE){
            $listProfils = $this->membre->getProfils(true);
	    
	    $DBadmin        = new DBAdmin();
		$listCivilites = $DBadmin->getListCivilites();
		$this->smarty->assign('getListCivilites', $listCivilites);
	    
	    
           if($listProfils->count>0){
                $this->smarty->assign('listProfils', $listProfils->profils);
           }
           $this->smarty->assign('count', $listProfils->count);
            
            if($_POST){
                $this->request['id_identifiant']   = $this->membre->getId_identifiant();
                $this->request['id_prestataire']   = $this->membre->getId_prestataire();
               
                if($error = $DBmembre->makeProfil($this->request)){
                    if(is_array($error) && count($error)>0){
                        $this->smarty->assign('post', $_POST);
                        $this->smarty->assign('error', $error);
                    }
                    else{
                        Tools::setFlashMessage(XMLEngine::$trad->global_trad->notifNewProfilOK, true, 'success', Tools::getLink($request["page"]));
                    }  
                }
            }
            
        }
            
    }
    //Fescativer un profil dans la cvteque
    function hideProfilAction(){
	$DBmembre= new DBMembre();
	//On recupere le manager par default afin de d'afficher ses infos dans le header et plus 
	$this->defaultAction();
      

	$DBadmin        = new DBAdmin();
    
	if(isset($this->crypt->id_prestataire_profil) && is_numeric($this->crypt->id_prestataire_profil)){
	    
	    $editProfil       = $DBmembre->getListProfils(array('id_prestataire_profil'=>$this->crypt->id_prestataire_profil));

	    if($editProfil['count']>0){
		$profil = $editProfil['profils'][0];
		
		$this->request['id_identifiant']        = $profil->getId_identifiant(); 
		$this->request['id_prestataire_profil'] = $this->crypt->id_prestataire_profil;
		$this->request['pf_profil_statut']      = 0;
		if($error = $DBmembre->editProfil($this->request)){
		    Tools::setFlashMessage(XMLEngine::$trad->global_trad->notifDesactivateProfilOK, true, 'success', Tools::getLink($this->request["page"]));
		}
	    }
	    
	}
    }
    //Activer un profil dans la cvteque
    function showProfilAction(){
	$DBmembre= new DBMembre();
	//On recupere le manager par default afin de d'afficher ses infos dans le header et plus 
	$this->defaultAction();
      

	$DBadmin        = new DBAdmin();
    
	if(isset($this->crypt->id_prestataire_profil) && is_numeric($this->crypt->id_prestataire_profil)){
	    
	    $editProfil       = $DBmembre->getListProfils(array('id_prestataire_profil'=>$this->crypt->id_prestataire_profil));

	    if($editProfil['count']>0){
		$profil = $editProfil['profils'][0];
		
		$this->request['id_identifiant']        = $profil->getId_identifiant(); 
		$this->request['id_prestataire_profil'] = $this->crypt->id_prestataire_profil;
		$this->request['pf_profil_statut']      = 1;
		if($error = $DBmembre->editProfil($this->request)){
		    Tools::setFlashMessage(XMLEngine::$trad->global_trad->notifActivateProfilOK, true, 'success', Tools::getLink($this->request["page"]));
		}
	    }
	    
	}
    }
    //ajouter un manager
    function addManagerAction(){
            $DBmembre= new DBMembre();
            //On recupere le prestataire par default afin de d'afficher ses infos dans le header et plus 
	    
            if($_POST){
                
                $this->request['id_identifiant'] = $this->membre->getId_identifiant(); 
                $this->request['id_client']      = $this->membre->getId_client(); 
		
		
            
                if($error = $DBmembre->makeManager($this->request)){
                    if(is_array($error) && count($error)>0){
                        $this->smarty->assign('post', $_POST);
                        $this->smarty->assign('error', $error);
			$this->defaultAction();
			
                    }
                    else{
                        Tools::setFlashMessage(XMLEngine::$trad->global_trad->notifNewManager, true, 'success-manager', Tools::getLink($request["page"]));
                    }  
                }
            }
    
    }
    //modifier un manager
    function editManagerAction(){
            $DBmembre= new DBMembre();
            //On recupere le prestataire par default afin de d'afficher ses infos dans le header et plus 
            $this->defaultAction();
            
	    $id_client_manager = $this->crypt->id_client_manager;
            $editManager       = $DBmembre->getListManagers(array('id_client_manager'=>$id_client_manager));

            if($editManager['count']>0){
                $manager = $editManager['managers'][0];
                $this->smarty->assign('manager', $manager);
                $this->smarty->assign('id_client_manager',$id_client_manager);//Pour tester si on est bien sur l'edition d'un manager
            }
            
            
            $DBadmin        = new DBAdmin();
            $listCivilites = $DBadmin->getListCivilites();
            $this->smarty->assign('getListCivilites', $listCivilites);
            
            if($_POST){

                $this->request['id_identifiant']    = $manager->getId_identifiant(); 
                $this->request['id_client_manager'] = $id_client_manager; 
                if($error = $DBmembre->editManager($this->request)){
                    if(is_array($error) && count($error)>0){
                        $this->smarty->assign('post', $_POST);
                        $this->smarty->assign('error', $error);
                    }
                    else{
                        Tools::setFlashMessage(XMLEngine::$trad->global_trad->notifUpdateManager, true, 'success-manager', Tools::getLink($request["page"]));
                    }  
                }
            }
	
    
    }
    
    //supprimer un manager 
    function removeManagerAction(){
	$DBmembre= new DBMembre();
	//On recupere le manager par default afin de d'afficher ses infos dans le header et plus 
	$this->defaultAction();
      

	$DBadmin        = new DBAdmin();
    
	if(isset($this->crypt->id_client_manager) && is_numeric($this->crypt->id_client_manager)){
	    
	    $editManager       = $DBmembre->getListManagers(array('id_client_manager'=>$this->crypt->id_client_manager));

	    if($editManager['count']>0){
		$manager = $editManager['managers'][0];
		
		$this->request['id_identifiant']        = $manager->getId_identifiant(); 
		$this->request['rmv_id_client_manager'] = $this->crypt->id_client_manager;
		if($error = $DBmembre->editManager($this->request)){
		    Tools::setFlashMessage(XMLEngine::$trad->global_trad->notifDeleteManager, true, 'success-manager', Tools::getLink($this->request["page"]));
		}
	    }
	    
	}
    }
    //Partie qui gere la mofification des CV
    function editProfilAction(){
        $smarty                = $this->smarty;
        $request               = $this->request;
        
        $DBmembre              = new DBMembre();
        $id_identifiant        = DBMembre::$id_identifiant;
        $id_type               = DBMembre::$id_type;

        $DBadmin               = new DBAdmin();
        $smarty->assign('getListExperiences', $DBadmin->getListExperiences());
        $smarty->assign('getListCivilites', $DBadmin->getListCivilites());
        $smarty->assign('getListMonnaies', $DBadmin->getListMonnaies());
        $smarty->assign('getListPermis', array("non","oui"));
                
        if($id_type==DBMembre::TYPE_PRESTATAIRE){
            
            //Si il s'agit d'un Freelance ou Intermmittent il ne possede que UN profil !
            if($this->membre->getStructure()->id_structure != DBMembre::TYPE_AGENCE){
                
                $profil                = $this->membre->getProfils()->profils[0];     
                $id_prestataire_profil = $profil->getId_profil();
                $listLocalites         = $profil->getLocalites();
                
            }
            else{
                //Si on edit un profil agence on doit forcement avoir un id_profil en parametre
                if(is_numeric($this->crypt->id_prestataire_profil) && ($this->crypt->id_prestataire_profil >0)){
                    $listProfil = $DBmembre->getListProfils(array(
                                                    'id_prestataire_profil'=>$this->crypt->id_prestataire_profil,
                                                    'id_identifiant'=>$id_identifiant,
                                                    'id_role'=>DBMembre::TYPE_NORMAL
                                                    )
                                              );
                    if($listProfil['count']>0){
                        $profil = $listProfil['profils'][0];
                        $id_prestataire_profil = $profil->getId_profil();
                        $listLocalites         = $profil->getLocalites();
                    }
		    //pour l'afficher dans les form autocomplete
		    $smarty->assign('id_prestataire_profil', $id_prestataire_profil);
                    
                }
                else{
                    header('location:'.Tools::getLink('new_profil'));
                }
            }
            
            //on assigne les localites
            $smarty->assign('localites',$listLocalites);
	    //on assigne les Experience
            $smarty->assign('listExperiences',$profil->getExperiences());
	    //on assigne les Formations
            $smarty->assign('listFormations',$profil->getFormations());
            //on assigne la variable CV
            $smarty->assign('cv',$profil->getCv());
            //Recuperation des informations en cours pour un id_prestataire_profil
            $smarty->assign('profil', $profil);
            //tres pratique pour crypter des infos que l'on envoi via ajax et mettre d'autres choses
            //Notamentt quand on editer des profil pour les agences
            $smarty->assign('crypt', Tools::encrypt('id_prestataire_profil='.$id_prestataire_profil));
            
            if(isset($id_prestataire_profil) && is_numeric($id_prestataire_profil) && isset($id_identifiant) && is_numeric($id_identifiant)){
                            
                //Si on effectue le submit (update ou insert ou suppresion)
                if($_POST || isset($request['rmv_id_profil_formation']) || isset($request['rmv_id_profil_experience']) || isset($request['rmv_id_profil_complement']) || isset($request['rmv_id_profil_localite'])){
                 
                    $request['id_prestataire_profil'] = $id_prestataire_profil;
                    $request['id_identifiant']        = $id_identifiant;
                   
			
                    if($error = $DBmembre->editProfil($request)){
                        if(is_array($error) && count($error)>0){
                            $smarty->assign('post', $_POST);
                            $smarty->assign('error', $error);
                        }
                        else{
                            Tools::setFlashMessage(XMLEngine::$trad->global_trad->notifUpdateOK, true, 'success-image', Tools::getLink('edit_cv', '', array('id_prestataire_profil'=>$this->crypt->id_prestataire_profil), true));
                        }
                    }
                    
                }
                
                if($request["id_profil_formation"]){//Edition d'une formation
                    $editFormation = $DBmembre->getListCvFormations(array('id_profil_formation'=>$request["id_profil_formation"], 'id_prestataire_profil'=>$id_prestataire_profil));
                    $smarty->assign('editFormation', $editFormation['formations'][0]);
                }
                
                elseif($request["id_profil_experience"]){//Edition d'une experience
                    $editExperience = $DBmembre->getListCvExperiences(array('id_profil_experience'=>$request["id_profil_experience"], 'id_prestataire_profil'=>$id_prestataire_profil));
                    $smarty->assign('editExperience', $editExperience['experiences'][0]);
                }

                elseif(isset($request["crop"]) && isset($request["image"])){//On vient de charger un avatar
                    $request['id_identifiant']        = 5; //On chargera $_SESSION["user"]["id_identifiant"];
                    $pathCrop = 'web/data/'.$request['id_identifiant'].'/avatar/tmp/'.$request["image"];
                    $smarty->assign('pathCrop', $pathCrop);
                }
		
                
            }
            else{
                
               header('location:index.php');
            }
        }
        else{
            header('location:index.php');
        }
    }
    
    function editRealisationsAction(){
        $request =$this->request;

        $DBmembre = new DBMembre();
        $id_type               = DBMembre::$id_type;
        $DBfile                = new DBFile;
        $request["file"]       = $_FILES;
        
        if($id_type==DBMembre::TYPE_PRESTATAIRE){

            //Si il s'agit d'un Freelance ou Intermmittent il ne possede que UN profil !
            if($this->membre->getStructure()->id_structure != DBMembre::TYPE_AGENCE){
                      
                $listRealisations      = $this->membre->getRealisations();
                $this->smarty->assign('countRealisations', $listRealisations->count);
                if($listRealisations->count > 0)
                    $this->smarty->assign('listRealisations', $listRealisations->files);

            }
            else{
                 $listRealisations      = $this->membre->getRealisations();
                $this->smarty->assign('countRealisations', $listRealisations->count);
                if($listRealisations->count > 0)
                    $this->smarty->assign('listRealisations', $listRealisations->files);
            }
            
            
            if($_POST){//Ajout et edition des realisations
		
                $request['id_prestataire_profil'] =  $this->membre->getId_profil();
                $request['id_identifiant']        =  $this->membre->getId_identifiant();
		
                if($request["id_file"]){
		  
                    $error = $DBfile->editFile($request);
                }
                else{
                    $error = $DBfile->makeFile($request);
                }
	    
                if(is_array($error) && count($error)>0){
                    $this->smarty->assign('error', $error);
                }
                else{
                    Tools::setFlashMessage(XMLEngine::$trad->global_trad->notifUploadOK, true, 'success-realisation', Tools::getLink($request["page"]));
                }
                    
            }
	    elseif(isset($request["rmv_id_file"])){//Suppression d'une réalisation

		    if($error = $DBfile->removeFile(array('id_file'=>$request["rmv_id_file"], 'fichier_type'=>array(DBFile::TYPE_VISUALISABLE, DBFile::TYPE_TELECHARGEABLE, DBFile::TYPE_CONSULTABLE),'id_prestataire_profil'=>$this->membre->getId_profil()))){

			if(is_array($error) && count($error)>0){
			    $this->smarty->assign('post', $_POST);
			    $this->smarty->assign('error', $error);
			   
			}
			else{
			     
			    Tools::setFlashMessage(XMLEngine::$trad->global_trad->notifSuppression, true, 'success-realisation', Tools::getLink($request["page"]));
			}
		    }
		}
            elseif(isset($request['id_file']) && is_numeric($request['id_file']) && !empty($request['id_file'])){//SI on souhaute editer une realisation
            
                $editFile = $DBfile->getListFiles(array('id_file'=>$request["id_file"], 'fichier_type'=>array(DBFile::TYPE_VISUALISABLE, DBFile::TYPE_TELECHARGEABLE, DBFile::TYPE_CONSULTABLE), 'id_prestataire_profil'=>$this->membre->getId_profil()));
                $this->smarty->assign('editFile', $editFile["files"][0]);

                if($editFile['count']>0)
                    $this->smarty->assign('id_file', $editFile["files"][0]->getId_file());
                
            }
            
           
        }
	
	
    }
 
    
    function addCvAction(){
        $smarty  = $this->smarty;
        $request =$this->request;
        
        
        $DBmembre = new DBMembre();
        $id_identifiant        = DBMembre::$id_identifiant;
        $id_type               = DBMembre::$id_type;
        $DBfile                = new DBFile;
        $request["file"]       = $_FILES;
        
        if($id_type==DBMembre::TYPE_PRESTATAIRE){
            
            //Si il s'agit d'un Freelance ou Intermmittent il ne possede que UN profil !
            if($this->membre->getStructure()->id_structure != DBMembre::TYPE_AGENCE){
		$request['id_prestataire_profil'] =  $this->membre->getId_profil();
		$request['id_identifiant']        =  $id_identifiant;
            }
            else{
		$request['id_prestataire_profil'] =  $this->crypt->id_prestataire_profil;
                $request['id_identifiant']        =  $id_identifiant;
            }
            
            if($error = $DBfile->makeFile($request)){
                //Tools::debugVar($error);
                if(is_array($error) && count($error)>0){
                    //Si une erreur survient, comme nous n'effectuons pas le header location (qui est appelé que quand le submit se transmforme en succes )
                    //Il faut aller rechercher les infos que l'on souhaite afficher sur la page (celles que l'on recupere par default quand on arrive sur $_REQUEST['page'])
                    //Comme cette fonction est appelé depuis differente pas et pas que edit_cv ou home on appel directement defaultAction qui faira le routage sur les bonnes fonciton
    
		    if($request["page"]=='home' || empty($request["page"])){
                         
			include(_CTRL_.'home.php');
                        $home = new homeController($smarty, $request);
                        $home->defaultAction($this->membre);
                    }
		    else{
			$this->editProfilAction();
		    }
                    $smarty->assign('error', $error);
                }
                else{
                    Tools::setFlashMessage(XMLEngine::$trad->global_trad->notifUpdateOK, true, 'success', Tools::getLink('edit_cv', '', array('id_prestataire_profil'=>$this->crypt->id_prestataire_profil), true));
                }
            }
        } 
        
    }
    function addPlaquetteAction(){
        $smarty  = $this->smarty;
        $request =$this->request;
        
        
        $DBmembre = new DBMembre();
        $id_identifiant        = DBMembre::$id_identifiant;
        $id_type               = DBMembre::$id_type;
        $DBfile                = new DBFile;
        $request["plaquette"]  = $_FILES;
        

                
        if($id_type==DBMembre::TYPE_PRESTATAIRE){
            
            
            //Si il s'agit d'un Freelance ou Intermmittent il ne possede que UN profil !
            if($this->membre->getStructure()->id_structure = DBMembre::TYPE_AGENCE){
                $request['id_identifiant']        =  $id_identifiant;
            }

            if($error = $DBfile->makeFile($request)){
                //Tools::debugVar($error);
                if(is_array($error) && count($error)>0){
                    //Si une erreur survient, comme nous n'effectuons pas le header location (qui est appelé que quand le submit se transmforme en succes )
                    //Il faut aller rechercher les infos que l'on souhaite afficher sur la page (celles que l'on recupere par default quand on arrive sur $_REQUEST['page'])
                    //Comme cette fonction est appelé depuis differente pas et pas que edit_cv ou home on appel directement defaultAction qui faira le routage sur les bonnes fonciton
                    if($request["page"]=='home' || empty($request["page"])){
                        include(_CTRL_.'home.php');
                        $home = new homeController($smarty, $request);
                        $home->defaultAction($this->membre);
                    }
                 
                     $smarty->assign('error', $error);
                }
                else{
                    Tools::setFlashMessage(XMLEngine::$trad->global_trad->notifUpdateOK, true, 'success', Tools::getLink('edit_cv', '', array('id_prestataire_profil'=>$this->crypt->id_prestataire_profil), true));
                }
            }
        } 
        
    }
   
        

   
}

?>
