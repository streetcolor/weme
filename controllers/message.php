<?php
//Controleur pour la gestion de la message et mail et contact
class messageController extends controller {
    
    function defaultAction(){
	
	 $DBmembre = new DBMembre();
       
        if(DBMembre::$id_type==DBMembre::TYPE_PRESTATAIRE){
            

	    if($this->request['page']=='contact'){
		
		$this->sendContactAction($this->membre);
	    
	    }
	    
        }
	elseif(DBMembre::$id_type==DBMembre::TYPE_CLIENT){
		
		if($this->request['page']=='new_message'){
				    
		    $getListMessages = $this->membre->getMessages();
                       
		       
		    $this->smarty->assign('countMessages', $getListMessages->count);
		    if($getListMessages->count>0)
			$this->smarty->assign('listMessages', $getListMessages->messages);
		}
		elseif($this->request['page']=='contact'){
                        
		    $this->sendContactAction($this->membre);
		}
        }
	else{
            //On est pas connecté
	    if($this->request['page']=='contact'){
                        
		$this->sendContactAction(false);
	    }
	    else{
		header('location:'.Tools::getLink('home'));
	    }
        }
	
    }
    
  
    //ajouter un message type
    function addMessageAction(){
	    $DBmemebre=new DBMembre();
	    $DBmembre  = new DBMembre();
	    $DBmessage = new DBMessage();
            $this->defaultAction();
            
            if($_POST){
                
                $this->request['id_identifiant'] = DBMembre::$id_identifiant; 
		
                if($error = $DBmessage->makeMessage($this->request)){
                    if(is_array($error) && count($error)>0){
                        $this->smarty->assign('post', $_POST);
                        $this->smarty->assign('error', $error);
                    }
                    else{
                        Tools::setFlashMessage(XMLEngine::$trad->global_trad->notifMessageAjoute, true, 'success', Tools::getLink($request["page"]));
                    }  
                }
            }
    
    }
    //editer un message type
    function editMessageAction(){
           
	    $DBmembre  = new DBMembre();
	    $DBmessage = new DBMessage();
            $this->defaultAction();
	     
            $editMessage = $DBmessage->getListMessages(array('id_client_message'=>$this->crypt->id_client_message, 'id_identifiant'=>DBMembre::$id_identifiant));
            
            if($editMessage['count']>0){
                $message = $editMessage['messages'][0];
                $this->smarty->assign('editMessage', $message);
                $this->smarty->assign('id_client_message', $this->crypt->id_client_message);//Pour tester si on est bien sur l'edition d'un manager
            }
	    
            if($_POST){
                
                $this->request['id_identifiant']    = DBMembre::$id_identifiant; 
		$this->request['id_client_message'] = $this->crypt->id_client_message;
		
                if($error = $DBmessage->editMessage($this->request)){
                    if(is_array($error) && count($error)>0){
                        $this->smarty->assign('post', $_POST);
                        $this->smarty->assign('error', $error);
                    }
                    else{
                        Tools::setFlashMessage(XMLEngine::$trad->global_trad->notifUpdateOK, true, 'success', Tools::getLink($request["page"]));
                    }  
                }
            }
    
    }
    //supprimer un message type
    function removeMessageAction(){
            $DBmembre  = new DBMembre();
	    $DBmessage = new DBMessage();
            $this->defaultAction();
            
	    if(!is_numeric($this->crypt->id_client_message)){
		
		header('location:'.Tools::getLink('new_message'));
	     
	    }
	    
	    $this->request['id_identifiant'] = DBMembre::$id_identifiant;
	    $this->request['id_client_message'] = $this->crypt->id_client_message;
	    
	    if($error = $DBmessage->removeMessage($this->request)){
		if(is_array($error) && count($error)>0){
		    $this->smarty->assign('error', $error);
		}
		else{
		    Tools::setFlashMessage(XMLEngine::$trad->global_trad->notifMessageSupprimé, true, 'success', Tools::getLink($request["page"]));
		}  
	    }
    
    }
    //envoyer un mail au profil selctionné depuis la CVteque
    function sendAction(){
	
	$DBmembre = new DBMembre();
	$DBmessage = new DBMessage();
	
	if(!DBMembre::$id_identifiant)
	    header('location:'.Tools::getLink("home"));
	    
	if(!is_array($id_prestataire_profil))
	    $id_prestataire_profil = is_array($_POST['id_prestataire_profil']) ? $_POST['id_prestataire_profil'] : array($this->crypt->id_prestataire_profil);
	
	$listProfils           = $DBmembre->getListProfils(array('id_prestataire_profil'=>$id_prestataire_profil));
	
	if(!$listProfils['count']){
	    header('location:'.Tools::getLink('home'));
	}
	
	//Recuperation de nos mails types
	$getListMessages          = $this->membre->getMessages();

	//Si on appel un mail type
	if($_POST['id_client_message'] && isset($this->request['chooseMessage'])){
	    
	    $id_client_messages       = array();
	    foreach($getListMessages->messages as $message){
		$id_client_messages[] = $message['id_client_message'];
	    }	
	    
	    if(in_array($_POST['id_client_message'], $id_client_messages)){

		$callMessage = $DBmessage->getListMessages(array('id_client_message'=>$_POST['id_client_message'], 'id_identifiant'=>DBMembre::$id_identifiant));
            
		if($callMessage['count']>0){
		    $message = $callMessage['messages'][0];
		    $this->smarty->assign('post', $message);
		}
		
	    }
	    
	}
	else if($_POST){
	    
	    //On va recuperer les email afin de pouvoir envoyer des message 
	    $emails = array();
	    foreach($listProfils['profils'] as $profil){
		$emails[] = $profil->getEmail();
	    }
	    
	    $this->request['from_name']      = $this->membre->getPrenom().' '.$this->membre->getNom();
	    $this->request['mail_author']    = MAIL_REPLY_TO;
	    $this->request['usr_email']      = array_unique($emails);
	    $this->request['id_identifiant'] = $this->membre->getId_identifiant();
	    if($error=$DBmessage->makeMail($this->request)){
		if(is_array($error) && count($error)>0){
		    $this->smarty->assign('post', $_POST);
		    $this->smarty->assign('error', $error);
		}
		else{
		    
		    Tools::setFlashMessage(XMLEngine::$trad->global_trad->notifMessageEnvoye, true, 'success-send-message', Tools::getLink('my_profils'));
		}  
	    }
	    
	}
	$this->smarty->assign('countMessages', $getListMessages->count);
	if($getListMessages->count>0)
	    $this->smarty->assign('listMessages', $getListMessages->messages);
	
	$this->smarty->assign('id_prestataire_profil', Tools::encrypt('id_prestataire_profil='.$this->crypt->id_prestataire_profil));//Utile pour le chargement d'un message type
	$this->smarty->assign('listProfils', $listProfils['profils']);
	
	//Si on ne vient pas de la page reponse
	//En effet la page réponse integre son propre module de d'envoide message donc pas besoin de lui afficher

	global $setTemplate; 
	$setTemplate = 'message';
	
    }
    //envoyer des missions au favaoris selctionnés
    function sendFavorisAction(){
	//Si jamais on plus de profil de sélectionné
	//Par mesure de sécurité on revient directement sur la page my_profils
	
	$id_mission = explode(',', $this->crypt->id_mission);

	if(!is_array($_POST['id_prestataire_profil']) || !is_array($id_mission)){
	      Tools::setFlashMessage(XMLEngine::$trad->global_trad->notifSelectionProfil, false, 'success-send-favoris-message', Tools::getLink('my_profils', 'proposeMission', array('id_mission'=>$this->crypt->id_mission),true));
	}

	//On recuper les favoris du client puis on verfifie que se sont bien ses favoris (securité)
	$listFavoris = $this->membre->getFavoris()->favoris;
	$id_prestataire_profil = array_intersect($listFavoris, $_POST['id_prestataire_profil']);//Pour eviter que des pirates viennenet ajouter des id_profil au hasard on ne prend que ceux qui exist dans les favoris
	$DBmembre = new DBMembre();
	$listProfils = $DBmembre->getListProfils(array('id_prestataire_profil'=>$id_prestataire_profil));
	
	//on recupere la mission de la quelle on vient pour le recapitulatif
	$DBmission = new DBMission();
	$mission = $DBmission->getListMissions(array('id_mission_statut'=>DBMission::MISSION_EN_COURS, 'id_mission'=>$id_mission));

	if($_POST && !$_POST['controller']){
	    
	    $DBmessage = new DBMessage();
	    
	    //On va recuperer les email afin de pouvoir envoyer des message 
	    $emails = array();
	    foreach($listProfils['profils'] as $profil){
		$emails[] = $profil->getEmail();
	    }
	    $this->request['mail_author']    = MAIL_REPLY_TO;
	    $this->request['usr_email']      = array_unique($emails);
	    $this->request['id_identifiant'] = $this->membre->getId_identifiant();
	    if($error=$DBmessage->makeMail($this->request)){
		if(is_array($error) && count($error)>0){
		    $this->smarty->assign('post', $_POST);
		    $this->smarty->assign('error', $error);
		}
		else{
		    
		    Tools::setFlashMessage(XMLEngine::$trad->global_trad->notifMessageEnvoye, true, 'success-send-favoris-message', Tools::getLink('my_profils', 'proposeMission', array('id_mission'=>$this->crypt->id_mission),true));
		}  
	    }
	    
	}
	//On balscule les variable sur la vue
	$this->smarty->assign('countMessages', $getListMessages->count);
	if($getListMessages->count>0)
	    $this->smarty->assign('listMessages', $getListMessages->messages);
	
	
	$this->smarty->assign('listProfils', $listProfils['profils']);
	$this->smarty->assign('missions', $mission);
	$this->smarty->assign('id_mission', $this->crypt->id_mission);
	global $setTemplate; 
	$setTemplate = 'message';
    
    }
    
    //repondre à la reponse d'un presta cote client
    function sendReponseAction($membre, $email_destinataire=false){
	$DBmembre = new DBMembre();
	$DBmessage = new DBMessage();	
	//Recuperation de nos mails types
	$getListMessages          = $membre->getMessages();
	//Si on appel un mail type
	if($_POST['id_client_message'] && isset($this->request['chooseMessage'])){
	    
	    $id_client_messages       = array();
	    foreach($getListMessages->messages as $message){
		$id_client_messages[] = $message['id_client_message'];
	    }	
	    
	    if(in_array($_POST['id_client_message'], $id_client_messages)){

		$callMessage = $DBmessage->getListMessages(array('id_client_message'=>$_POST['id_client_message'], 'id_identifiant'=>DBMembre::$id_identifiant));
            
		if($callMessage['count']>0){
		    $message = $callMessage['messages'][0];
		    $this->smarty->assign('post', $message);
		}
		
	    }
	    
	}
	else if($_POST){
	    $emails[] = $email_destinataire;
	    $this->request['usr_email']      = array_unique($emails);
	    $this->request['id_identifiant'] = $membre->getId_identifiant();
	    $this->request['mail_author']    = $membre->getEmail();//Attention on ne met pas le mail de contacto car on le client peux acceder au prestataire dans le cadre d'une reponse
	    
	    $urlDirect = $_SERVER["HTTP_HOST"]."/".Tools::getLink('new_response', '', array('id_mission'=>$this->crypt->id_mission, 'codeToken'=>TOKEN_CODE, 'usr_email'=>implode('',$this->request['usr_email'])), true);//Cette url permet de connecter un utilisateur directement sur la plateforme
	    
	    $patterns = array();
	    $patterns[0] = '/{{urlDirect}}/';
	    
	    $replacements = array();
	    $replacements[0] = $urlDirect;
		
	    $prefixMessage = preg_replace($patterns, $replacements, XMLEngine::$trad->global_trad->mailPrestaMissionAvailable);
			    
	    $this->request['mail_message']   =  $prefixMessage.$this->request['mail_message'];
	     

	    if($error=$DBmessage->makeMail($this->request)){
		if(is_array($error) && count($error)>0){
		    $this->smarty->assign('post', $_POST);
		    $this->smarty->assign('error', $error);
		}
		else{
		    
		    Tools::setFlashMessage(XMLEngine::$trad->global_trad->notifMessageEnvoye, true, 'success-send-message', Tools::getLink('responses', '', array('id_mission'=>$this->crypt->id_mission), true));
		}  
	    }
	    
	}
	
	$this->smarty->assign('countMessages', $getListMessages->count);
	if($getListMessages->count>0)
	    $this->smarty->assign('listMessages', $getListMessages->messages);
    }
    //M'envoyer par email
    function sendCopyAction($membre){
	
	$currentPage = is_numeric($this->crypt->paginate) ? $this->crypt->paginate : $this->request['paginate'];
	$mode        = ($this->crypt->mode) ? $this->crypt->mode : $this->request['mode'];

	if($mode=='search'){
		$link =  Tools::getLink($this->request['page'], 'search', array('id_mission'=>$this->crypt->id_mission), true).'&paginate='.$currentPage;
	}
	elseif($mode=='detail'){
		$link =  Tools::getLink($this->request['page'], '', array('id_mission'=>$this->crypt->id_mission, 'id_mission_reponse'=>$this->crypt->id_mission_reponse), true);
	}
	elseif($mode=='normal'){
		$link =  Tools::getLink($this->request['page'], 'search', array('id_mission'=>$this->crypt->id_mission), true).'&paginate='.$currentPage;
	}
	elseif($mode=='home'){
		$link =  Tools::getLink('home');
	}
	else{
	    	$link =  Tools::getLink($this->request['page'], '', array('id_mission'=>$this->crypt->id_mission), true).'&paginate='.$currentPage;

	}

	$DBreponse  = new DBReponse();
	$id_mission_reponse = is_numeric($this->crypt->id_mission_reponse) ? array($this->crypt->id_mission_reponse) : $this->request['id_mission_reponse'];
	$getReponse = $DBreponse->getListReponses(array('id_mission_reponse'=>$id_mission_reponse, 'id_mission'=>$this->crypt->id_mission));

	if($getReponse['count']>0){
		$string = XMLEngine::$trad->global_trad->notifClientSendPropMail;
		$patterns = array();
		$patterns[0] = '/{{nom}}/';
		$patterns[1] = '/{{prenom}}/';
		$patterns[2] = '/{{reponse}}/';
		$patterns[3] = '/{{tarif}}/';
		
 		foreach($getReponse['reponses'] as $reponse){
		    $replacements = array();
		    $replacements[0] = $reponse->getNom();
		    $replacements[1] = $reponse->getPrenom();
		    $replacements[2] = $reponse->getReponse();
		    $replacements[3] = $reponse->getTarif(true);
		    
		    $message .= preg_replace($patterns, $replacements, $string);
		}
	}
	$this->request['mail_author']    = MAIL_REPLY_TO;
	$this->request['usr_email']      = array($membre->getEmail());
	$this->request['id_identifiant'] = $membre->getId_identifiant();
	$this->request['mail_message']   = $message;
	$this->request['mail_titre']     = 'Voici la copie des reponses que vous avez demandé';
	
	$DBmessage = new DBMessage();
	
	if($error=$DBmessage->makeMail($this->request)){

		
		Tools::setFlashMessage(XMLEngine::$trad->global_trad->notifMessageEnvoye, true, 'success-send-message', $link);
	}
    
   }
   
    //Cote presta on recommande des missions
    function sendFriendAction(){
	$DBmembre = new DBMembre;
	
	if(DBMembre::$id_type == DBMembre::TYPE_PRESTATAIRE){
	   
	    $DBmission = new DBMission();
	
	    $getMission = $DBmission->getListMissions(array('id_mission'=>$this->request['id_mission']));
	    if($getMission['count']>0){
		    $string = XMLEngine::$trad->global_trad->mailPrestaRecommandeMission;
		    $patterns = array();
		    $patterns[0] = '/{{nom}}/';
		    $patterns[1] = '/{{prenom}}/';
		    $patterns[2] = '/{{missionTitre}}/';
		    
		    $replacements = array();
		    $replacements[0] = $this->membre->getNom();
		    $replacements[1] = $this->membre->getPrenom();
		    $replacements[2] = $getMission['missions'][0]->getTitre();
		    
		   if($getMission['missions'][0]->getStatut()->id_statut == DBMission::MISSION_EN_COURS){
			$message .=   preg_replace($patterns, $replacements, $string);
			$message .= URL_SITE.Tools::getLink('mission', '', array('id_mission'=>$this->request['id_mission']));
		    
		    }
	    }
	    else{
		$error['mail_message'] = XMLEngine::$trad->global_trad->mailPrestaRecommandeMissionNonDispo;
		$this->smarty->assign('error', $error);
		
		echo json_encode(array('success'=>0, 'message'=>$error['mail_message']));
		exit;
	    }
	    
	    $this->request['mail_author']    = $this->membre->getEmail();
	    $this->request['usr_email']      = array($this->request['usr_email']);
	    $this->request['id_identifiant'] = $this->membre->getId_identifiant();
	    $this->request['mail_message']   = $message;
	    $this->request['mail_titre']     = XMLEngine::$trad->global_trad->mailPrestaRecommandeMissionTItre;

	    $DBmessage = new DBMessage();
	    
	    if($error=$DBmessage->makeMail($this->request)){
		$success = XMLEngine::$trad->global_trad->notifMessageEnvoye;
		
		echo json_encode(array('success'=>1, 'message'=>$success));


	    }
	
	}
    
    }
    //envoyer un mail au contact du Service Client
    function sendContactAction($getMembre){
    
	    //On va recuperer les email afin de pouvoir envoyer des message 
	    $emails = array();
	    $emails[] = MAIL_REPLY_TO;
	    if($_POST){

		if($getMembre==false){
		    //En tant que visiteur
		     $this->request['id_identifiant'] = true;
		     $this->request['usr_email']         = array_unique($emails);
		}
		else{
		    //En tant que personne connectée
		    $this->request['mail_author']       = $getMembre->getEmail();
		    $this->request['usr_email']         = array_unique($emails);
		    $this->request['id_identifiant']    = $getMembre->getId_identifiant();
		    if(isset($_POST['mail_message']) && !empty($_POST['mail_message'])){
			 $string = XMLEngine::$trad->global_trad->mailServiceClient;
			$patterns = array();
			$patterns[0] = '/{{nom}}/';
			$patterns[1] = '/{{prenom}}/';
			
			
			$replacements = array();
			$replacements[0] = $getMembre->getNom();
			$replacements[1] = $getMembre->getPrenom();
			
		    
			$message   = preg_replace($patterns, $replacements, $string);
			$message  .= '<br><br>'.$this->request['mail_message'];
			$this->request['mail_message']  = $message;
		    }
		}
		
		$DBmessage = new DBMessage();
		
		if($error=$DBmessage->makeMail($this->request)){

		    if(is_array($error) && count($error)>0){
			$this->smarty->assign('post', $_POST);
			$this->smarty->assign('error', $error);
		    }
		    else{
			
			Tools::setFlashMessage(XMLEngine::$trad->global_trad->notifMessageEnvoye, true, 'success-send-message', Tools::getLink('home'));
		    }  
		}
	    }
	    //On aassignera getMemebre pltutot que getCLient/getPrestataire car la methode contactAction est appelé dans les deux espaces
	    //A noter que getNom, getPrenom, getEmail, getId_identifiont sotn egallement present pour les presta et les clients
	    $this->smarty->assign('getMembre', $getMembre);
	
    }
}
?>
