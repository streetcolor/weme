<?php

class entretienController extends controller {
    
    protected $smarty;
    protected $request;
    
    public function __construct($smarty, $request){
	$this->smarty = $smarty;
	$this->request = $request;
	
	if($messageCallBack=Tools::getFlashMessage('success-create-session')){
		$this->smarty->assign('messageCallBack', $messageCallBack);
	}
	
	if($messageCallBack=Tools::getFlashMessage('success-confirm-session')){
		$this->smarty->assign('messageCallBack', $messageCallBack);
	}
	
        parent::__construct($smarty, $request);
    }
    
    function defaultAction(){

	$DBmembre    = new DBMembre();
	$DBentretien = new DBEntretien();
	
	if(DBMembre::$id_type==DBMembre::TYPE_CLIENT){
	    //Si il y a en paramètre un id prestataire, on l'assign via smarty. Cela nous permettra de faire les test de création ou d'édition
	    if($this->request['id_prestataire_profil']>0 &&  is_numeric($this->request['id_prestataire_profil'])){
		$this->smarty->assign('id_prestataire',$this->request['id_prestataire_profil']);
		
		//On récupère le profil du membre choisi pour l'entretien
		$filtre_profil=array('id_prestataire_profil'=>$this->request['id_prestataire_profil']);
		
	    }else{
		//SI il n'y pas de profil défini, on récupère la liste des favoris pour en selectionner un
		$listeFav = $DBmembre->getListFavorisProfils(DBMembre::$id_identifiant);//Tools::debugVar($listeFav, false);
		$filtre_profil=array('id_prestataire_profil'=>$listeFav['favoris']);//Tools::debugVar($filtre_profil, false);
	    }
	
	
	    //SI on a deja un id prestataire, on est en mode edition/consultation et donc listProfil contient le profil demandé
	    //Si on a pas d'id prestataire, on est en mode création, et donc listProfil contient la liste des profils selectionnable (les profils favoris)
	    $listProfil=$DBmembre->getListProfils($filtre_profil);//Tools::debugVar($listProfil, false);
	    $this->smarty->assign('listProfil',$listProfil['profils'] );
	
	
	    //On recupère les mission au statut 'en cours' de la session client
	    $DBmission= new DBMission();
	    $filtre_mission = array('id_identifiant' => DBMembre::$id_identifiant, 'id_mission_statut'=>DBMission::MISSION_EN_COURS);
	    $listMissions   = $DBmission->getListMissions($filtre_mission, false, false);//Tools::debugVar($listMissions); 
	    $this->smarty->assign('listMissions',$listMissions['missions']);
	    
	    //ON change le statut de l'entretien si il est terminé
	    if($this->request['end'] == 1){
		$id_videosession = $this->request['idSession'];
		$update_tab = array('vs_statut'=>$DBentretien::SESSION_TERMINEE);
		$where_tab = array('id_videosession'=>$id_videosession);
		
		$DBentretien->updSessionVideo($update_tab,$where_tab);
	    }
	}
	
	
	
	$this->entretiensAction();
    }
    
    function entretiensAction(){
	$DBentretien = new DBEntretien();
	
	//On récupère la liste des entretiens de l'utilisateur
	if(DBMembre::$id_type==DBMembre::TYPE_CLIENT){
	    $filtre_session = array('id_identifiant'=>DBMembre::$id_identifiant);
	}elseif(DBMembre::$id_type==DBMembre::TYPE_PRESTATAIRE){
	    
	    if($this->membre->getStructure()->id_structure==DBMembre::TYPE_AGENCE){
		//On essaie de générer un tableau contenant tous les id des profils de l'agence
		$listeProfilsId = array();
		$listeProfils = $this->membre->getProfils(true);//Tools::debugVar($listeProfils->profils, false);
		foreach ($listeProfils->profils as  $profil){
		    $listeProfilsId[] = $profil->getId_profil();
		}//Tools::debugVar($listeProfilsId, false);
		
		$filtre_session = array('id_prestataire_profil'=>$listeProfilsId);
		//Tools::debugVar($filtre_session);
	    }else
		$filtre_session = array('id_prestataire_profil'=>$this->membre->getId_profil());//Tools::debugVar($this->membre->id_prestataire_profil, false);
	}
	
	$listEntretiens = $DBentretien->getListEntretiens($filtre_session); //Tools::debugVar($listEntretiens, false);
	$this->smarty->assign('listEntretiens', $listEntretiens);
	
	if(DBMembre::$id_type==DBMembre::TYPE_PRESTATAIRE && !isset($this->crypt->id_videosession) && count($listEntretiens['entretiens'])){
	    $session = $listEntretiens['entretiens'][0];
	    $this->smarty->assign('entretien', $session);
	    
	    //Nom de la session
	    $session_name = "entretien_".$session->getId_videosession()."-".Tools::shortConvertLanDateToDb($session->getSessionTime()->date);
	    $this->smarty->assign('session_name', $session_name);
	}
    }
    
    
    function addSessionAction(){
	$request = $this->request;
	$request['id_client'] = $this->membre->getId_client();//Tools::debugVar($request, false);
	
	$DBentretien = new DBEntretien();
	
	if($result = $DBentretien->makeVideoSession($request)){
	    if(is_array($result) && count($result)>0){
                    $this->smarty->assign('post', $_POST);
                    $this->smarty->assign('error', $result);
		    $this->defaultAction();
		    
	    }else{
		Tools::setFlashMessage(XMLEngine::$trad->global_trad->notifSessionAjoute, true, 'success-create-session', Tools::getLink($this->request['page']));
	    }
	}
	
    }
    
    function showDetailSessionAction(){
		
	$DBentretien = new DBEntretien();
	
	$id_videosession = $this->crypt->id_videosession;
	$this->smarty->assign('id_videosession', $id_videosession);
	
	//On récupère la liste des entretiens de l'utilisateur
	if(DBMembre::$id_type==DBMembre::TYPE_CLIENT){
	    $filtre_session = array('id_identifiant'=>DBMembre::$id_identifiant, 'id_videosession'=>$id_videosession);
	}
	elseif(DBMembre::$id_type==DBMembre::TYPE_PRESTATAIRE){
	    
	    if($this->membre->getStructure()->id_structure==DBMembre::TYPE_AGENCE){
		//On essaie de générer un tableau contenant tous les id des profils de l'agence
		$listeProfilsId = array();
		$listeProfils = $this->membre->getProfils(true);//Tools::debugVar($listeProfils->profils, false);
		foreach ($listeProfils->profils as  $profil){
		    $listeProfilsId[] = $profil->getId_profil();
		}//Tools::debugVar($listeProfilsId, false);
		
		$filtre_session = array('id_prestataire_profil'=>$listeProfilsId, 'id_videosession'=>$id_videosession);
		//Tools::debugVar($filtre_session);
	    }else
		$filtre_session = array('id_prestataire_profil'=>$this->membre->getId_profil(), 'id_videosession'=>$id_videosession);//Tools::debugVar($this->membre->id_prestataire_profil, false);
	}
	
	//ON récupère l'entretien à afficher en détail
	$entretien = $DBentretien->getListEntretiens($filtre_session);//Tools::debugVar($entretien, false);
	
	if($entretien['count']>0){
	
	    $session = $entretien['entretiens'][0];
	    $this->smarty->assign('entretien', $session);
	    //ON change le statut de l'entretien si il est terminé
	    if($this->request['end'] == 1 && DBMembre::$id_type==DBMembre::TYPE_CLIENT){
	       $id_videosession = $this->request['idSession'];
		$update_tab = array('vs_statut'=>$DBentretien::SESSION_TERMINEE);
		$where_tab = array('id_videosession'=>$id_videosession);
		
		$DBentretien->updSessionVideo($update_tab,$where_tab);
	    }
	    //Nom de la session
	    $session_name = "entretien_".$session->getId_videosession()."-".Tools::shortConvertLanDateToDb($session->getSessionTime()->date);
	    $this->smarty->assign('session_name', $session_name);
	    
	    //ON récupère la liste des entretiens
	    $this->entretiensAction();
	
	}
	else{
	    $this->defaultAction();
	}
	
    }
    
    function editSessionAction(){
	$DBentretien = new DBEntretien();

	//Si on est client la seule modification que l'on peut faire est d'joueter un commentaire
	//Si l'on est prestataire, la seule modification que l'on peut faire est au niveau de statut (confirmer)
	if(DBMembre::$id_type==DBMembre::TYPE_CLIENT){
	    $this->request['id_videosession']        = $this->crypt->id_videosession ;	    
	    $msg = XMLEngine::$trad->global_trad->notifEntretienCommentaireOk;
	    $return_page = 'new_entretien';
	    
	}elseif(DBMembre::$id_type==DBMembre::TYPE_PRESTATAIRE){
	    $msg = XMLEngine::$trad->global_trad->notifConfirmEntretienOk;
	    $return_page = 'entretiens';
	    $this->request['vs_statut']              = $this->crypt->vs_statut ;
	    $this->request['id_videosession']        = $this->crypt->id_videosession ;
	    $this->request["vs_guestaccess"]         =Tools::randomString();
	}
	
	if($error = $DBentretien->editEntretien($this->request)){
	    if(is_array($error) && count($error)>0){
		Tools::debugVar($error);
		     $this->smarty->assign('post', $_POST);
		     $this->smarty->assign('error', $error);
	     }else{
	      
		 Tools::setFlashMessage($msg, true, 'success-confirm-session', Tools::getLink($return_page, '', array('action'=>'showDetailSession', 'crypt'=>$this->request['id_videosession']), true));

	     } 
	}
	
	
    }
    
    function entretienAction(){
	//Tools::debugVar($DBmembre::$id_type);
	$this->smarty->assign('idSession',$this->request['id'] );
	$this->smarty->assign('session',$this->request['session'] );
	$this->smarty->assign("swf_path", _SWF_);
	$this->smarty->assign("user_type", DBMembre::$id_type);
    }
    
}

?>