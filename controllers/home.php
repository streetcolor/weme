<?php
//Controleur pour la gestion de la home
//pour la gestion des dashboard client et prestataire
class homeController extends controller{
        
    public function __construct($smarty, $request){
        parent::__construct($smarty, $request);
        
        if($messageCallBack=Tools::getFlashMessage('success-send-message')){
                $this->smarty->assign('messageCallBack', $messageCallBack);
        }
        
    }
    
    function defaultAction($membre=false){

        $smarty  = $this->smarty;
        $request =$this->request;
        
        $DBmembre       = new DBMembre();
        $DBMission      = new DBMission();
        
        if(DBMembre::$id_type==DBMembre::TYPE_PRESTATAIRE){
            
            $membre = !$membre ? $this->membre : $membre;

            //Si il s'agit d'un Freelance ou Intermmittent il ne possede que UN profil !
            if($membre->getStructure()->id_structure != DBMembre::TYPE_AGENCE){
                $listId=array($membre->getId_profil());
                $smarty->assign('cv', $membre->getCv());
                $smarty->assign('agence', false);
            }
            //Si il s'agit d'une Agence on recupere tous ses profils !
            else{
                $listProfil = $membre->getProfils();
                $listId = array();
                foreach($listProfil->profils as $value)
                    $listId[] = $value->getId_profil();
                $smarty->assign('plaquette',$membre->getPlaquette());    
                $smarty->assign('agence', true);
            }
            
            /*Partie qui recupere les missions qui matchent avec les profils*/
            $listMissionMatch = $DBMission->getListMissionsMatchProfils($listId);
            $smarty->assign('listMissions', $listMissionMatch);
            /*Fin de la partie*/
            
            /*Partie qui recupere les reponses aux missions*/
            $listReponses = $membre->getReponses();
            $smarty->assign('countReponses', $listReponses->count);
            
            $smarty->assign('reponses', $listReponses);
            /*Fin de la partie*/
            
            /*Pour afficher nos statut reponse (sert dans pour le menu Mon siuvi de proposition a droire du dashboard)*/
            $DBadmin      = new DBAdmin(); 
            $this->smarty->assign('getListStatuts', $DBadmin->getListStatutsReponses());
            /*Fin de la partie*/
            
            /*On va créer une mini session qui stock nos compteur de projet et reponse pour les afficher dans le header*/
            /*ATTENTION la session doit être setté ici même puisque c'est la premiere page qui s'affiche quand on se connecte !*/
            /*L'assignation de la variable compteur se fait ici pour quon puisse l'afficher en home mais aussi dans la methode getPrestataire()*/
            /*A noter que la methode prestataire recuperera que la session dejé existante autrement dit inutile de recompter les entrées avec les Classes DBmission et DBreponse*/
            $_SESSION['compteur']["all_reponses"] = $listReponses->count; //Toutes nos reponses
            
            $id_mission = array();
            foreach($listMissionMatch['missions'] as $mission){
                //if($mission->getNew()) // pour juse lele nouvelle missions
                    $id_mission[] = $mission->getId_mission();
            }
            
            $_SESSION['compteur']["match_missions"] = implode(',', $id_mission); //Toutes nos profils qu imatchent au client
            
            //$_SESSION['compteur']["new_missions"] = $listMissionMatch['new_missions']; //Les nouvelle missions de 10 jours qui match CF:methode $DBmission->getListMissions()
            $_SESSION['compteur']["new_missions"] = $listMissionMatch['count']; //Les misison qui matchent sans les nouvelle de 10jours

            /*Fin de la partie*/
        }
        elseif(DBMembre::$id_type==DBMembre::TYPE_CLIENT){
            
            $getClient = $this->membre;
            
            //RECUPERATION MATCHING MISSION POUR LES PROFILS
            $listMissions = $getClient->getMissions();
            
            $listId                            = array();
            $localites['lo_pays_short']        = array();
            $localites['lo_region_short']      = array();
            $localites['lo_departement_short'] = array();
            
            foreach($listMissions->missions as $value){
                $listId[] = $value->getId_mission();
                $localite = $value->getLocalite();
                $localites['lo_pays_short'][] = $localite->getLocalite()->lo_pays_short;
                $localites['lo_region_short'][] = $localite->getLocalite()->lo_region_short;
                $localites['lo_departement_short'][] = $localite->getLocalite()->lo_departement_short;
            }
            
            /*Partie qui recupere les Profils qui matchent avec les missions*/
            $listProfilMatch = $DBmembre->getListProfilsMatchMissions($listId, array('lo_pays_short'=>$localites['lo_pays_short'], 'lo_region_short'=>$localites['lo_region_short'], 'lo_departement_short'=>$localites['lo_departement_short']));
            $this->smarty->assign('listProfils', $listProfilMatch);
            /*Fin de la partie*/
            
            /*Partie qui recupere les reponses aux missions*/
            $listReponses = $getClient->getReponses();
            $this->smarty->assign('countReponses', $listReponses->count);
            $this->smarty->assign('reponses', $listReponses);
            $this->smarty->assign('mode', $this->request['page']);//pour envoyer la page en cours
            /*Fin de la partie*/
            /*Pour afficher nos statut reponse disponible*/
            $DBadmin      = new DBAdmin(); 
            $this->smarty->assign('getListStatuts', $DBadmin->getListStatutsMissions());
            $this->smarty->assign('statistiques', $listMissions->statistiques);
            
            //Pour le moteur de recherche    
            $DBadmin = new DBAdmin();
            $smarty->assign('listExperiences', $DBadmin->getListExperiences());
            
             /*On va créer une mini session qui stock nos compteur de profil et projets pour les afficher dans le header*/
            /*ATTENTION la session doit être setté ici même puisque c'est la premiere page qui s'affiche quand on se connecte !*/
            /*L'assignation de la variable compteur se fait ici pour quon puisse l'afficher en home mais aussi dans la methode getPrestataire()*/
            /*A noter que la methode prestataire recuperera que la session dejé existante autrement dit inutile de recompter les entrées avec les Classes DBmission et DBreponse*/
            $_SESSION['compteur']["all_profils"]   = $listProfilMatch['count']; //Toutes nos profils qu imatchent au client
            $id_prestataire_profil = array();
            foreach($listProfilMatch['profils'] as $profils){
                $id_prestataire_profil[] = $profils->getId_profil();
            }
            
            $_SESSION['compteur']["match_profils"] = implode(',', $id_prestataire_profil); //Toutes nos profils qu imatchent au client
            $_SESSION['compteur']["all_projets"]   = $listMissions->count; //Toute les misisondu client
            /*Fin de la partie*/
            
        }
        else{
           
        }
        
        //Pour le moteur de recherche
	$publications = array(
	   ""         => XMLEngine::$trad->global_trad->label_publication,
	   "1 DAY"    => XMLEngine::$trad->global_trad->today,
	   "7 DAY"    => XMLEngine::$trad->global_trad->minus_week,
	   "1 MONTH"  => XMLEngine::$trad->global_trad->minus_month,
	   "6 MONTH"  => XMLEngine::$trad->global_trad->minus_six_month
	);
	$this->smarty->assign('listPublications', $publications);
        
    }
    //Pour faire une connexion
    function connexionAction(){
        
        $DBmembre  = new DBMembre();

        if($error =  $DBmembre->makeConnexion($this->request)){
            if(is_array($error) && count($error)>0 && count($error)<2){
                $this->smarty->assign('error', $error);
            }
            else{

                if(isset($this->request['remember'])){
                    setcookie("user[id_identifiant]", $error['id_identifiant'], (time()+3600*24*365));  /* expire dans 1 an */
                    setcookie("user[id_type]", $error['id_type'], (time()+3600*24*365));  /* expire dans 1 an */
                }
                $_SESSION["user"]=$error;
                
                if(isset($this->request['codeToken']))
                    header('location:'.$_SERVER["REQUEST_URI"]);
                else
                    header('location:index.php');
            }
        }
        
    }
    //Pour faire une deconnexion
    function loggoutAction(){
        $smarty  = $this->smarty;
        $request =$this->request;
        
        setcookie("user[id_identifiant]", $error['id_identifiant'], time() -(3600*24*365));  /* expire dans 1 an */
        setcookie("user[id_type]",        $error['id_type'], time() -(3600*24*365));  /* expire dans 1 an */
        
        unset($_SESSION['user']);
        unset($_SESSION['langue']);
        header('location:'.Tools::getLink('home'));
    }
    //Pour un mot de passe perdu
    function passwordAction(){
        
        
        $DBmembre  = new DBMembre();
        if($error = $DBmembre->makePassword($this->request)){
    
            if(is_array($error) && count($error)>0){
        
                $this->smarty->assign('error', $error);
            }
            else{
                Tools::setFlashMessage(XMLEngine::$trad->global_trad->notifSendMDP, true, 'success', Tools::getLink('home'));
            }
        }
    
       
    }
    
}
?>
