<?php
//Controleur pour la gestion de la home
//pour la gestion des missions
class missionController extends controller {

    public function __construct($smarty, $request){

	parent::__construct($smarty, $request);
	
	if($messageCallBack=Tools::getFlashMessage('success-update-statut')){
		$this->smarty->assign('messageCallBack', $messageCallBack);
	}
	
	//Pour le moteur de recherche
	$publications = array(
	   ""         => XMLEngine::$trad->global_trad->label_publication,
	   "1 DAY"    => XMLEngine::$trad->global_trad->today,
	   "7 DAY"    => XMLEngine::$trad->global_trad->minus_week,
	   "1 MONTH"  => XMLEngine::$trad->global_trad->minus_month,
	   "6 MONTH"  => XMLEngine::$trad->global_trad->minus_six_month
	);
	
    	$this->smarty->assign('listPublications', $publications);
	
    }
    
    //par default on liste les missions
    function defaultAction($search=array()){
        unset($_SESSION['search']);
        $DBmembre = new DBMembre();
       
        if(DBMembre::$id_type==DBMembre::TYPE_PRESTATAIRE){
	    
            if($this->request['page']=='my_missions'){

		$listFavoris = $this->membre->getFavoris();
		$listFavoris = $listFavoris->count>0 ?  $listFavoris->favoris : array();

		$filtreDefault = array('id_mission_statut'=>DBMission::MISSION_EN_COURS, 'id_mission'=>$listFavoris); //Le filtre de recheche de base obligtatoire

	    }
	    elseif($this->request['page']=='missions'){
	
		$filtreDefault = array('id_mission_statut'=>DBMission::MISSION_EN_COURS); //Le filtre de recheche de base obligtatoire
	    }
	    elseif($this->request['page']=='mission'){
	
		$this->missionAction();
	    }
        }
	elseif(DBMembre::$id_type==DBMembre::TYPE_CLIENT){
	    
            if($this->request['page']=='edit_mission'){
                $this->editMissionAction();
            }
            elseif($this->request['page']=='new_mission'){
                $this->addMissionAction();
            }
	    elseif($this->request['page']=='mission'){

		$this->missionAction($this->crypt->id_mission);
	    }
            
	    else{
	
                $filtreDefault = array('id_identifiant'=>DBMembre::$id_identifiant); //Le filtre de recheche de base obligtatoire
                
                $DBadmin = new DBAdmin();
                $this->smarty->assign('getListStatuts', $DBadmin->getListStatutsMissions());
	    }

        }else{
            //On est pas connecté
	    if($this->request['page']=='mission'){
		
                $this->missionAction();
            }
	    else{
		$filtreDefault = array('id_mission_statut'=>DBMission::MISSION_EN_COURS); //Le filtre de recheche de base obligtatoire
	    }
	}
        if(is_array($filtreDefault)){
	    
	    $_SESSION['search'] = $search;//si on passe par la methode searchAction on passe la parametre dans la session
	    
            $DBmission= new DBMission();
            
            // Numero de page (1 par défaut)
            $paginate['current_page'] = (isset($_GET['paginate']) && is_numeric($_GET['paginate'])) ? $_GET['paginate'] : 1;
            // Numéro du 1er enregistrement à lire
            $paginate['limit']        = ($paginate['current_page'] - 1) * NOMBRE_PER_PAGE;    
            //on recupere la liste de nos CV pour le comptage
            $listMissions             = $DBmission->getListMissions($filtreDefault, $search, array($paginate['limit'], NOMBRE_PER_PAGE));
            
	    // Pagination
            $paginate['total_pages']  = ceil($listMissions['count'] / NOMBRE_PER_PAGE);
            
            //Si on du monde en sortie on les recupere
            if($listMissions['count']){
                
                //Puis on les envoie dans la vue
                $this->smarty->assign('nbrPages', $paginate['total_pages']);
                $this->smarty->assign('pageCourante', $paginate['current_page']);
                $this->smarty->assign('listMissions', $listMissions['missions']);
                $this->smarty->assign('count', $listMissions['count']);
                $this->smarty->assign('mode', 'normal');//permet d'ajouter un paramtrer dans lien favoris des mission et updtae statut
            }
        }

    }
    
    //Afficher une mission
    function missionAction($id_mission=false, $forceLoad=false){//forceLoad permet d'afficher une mission quelque soit son statut (depuis controleur repone par exemple)
	    
	    $id_mission = $id_mission ? $id_mission : $this->request['id_mission'];
	    
            $DBmembre=new DBMembre();
            if(DBmembre::$id_type==DBMembre::TYPE_CLIENT){
              
                //$membre->clientAction();
                $filtre = array('id_mission'=>array($id_mission));
            }
            elseif(DBmembre::$id_type==DBMembre::TYPE_PRESTATAIRE){
                $id_mission = is_numeric($this->crypt->id_mission) ? $this->crypt->id_mission : $id_mission;
                
		$filtre =  $forceLoad ? array('id_mission'=>array($id_mission)) :  array('id_mission_statut'=>DBMission::MISSION_EN_COURS, 'id_mission'=>array($id_mission));
            }
            else{
                $filtre = array('id_mission_statut'=>DBMission::MISSION_EN_COURS, 'id_mission'=>array($id_mission));
            }
            
            $DBmission = new DBMission();
                            
            //on recupere la liste de nos mission pour le comptage
            $mission             = $DBmission->getListMissions($filtre);

            //Si on du monde en sortie on les recupere
            if($mission['count']){
                    $mission             = $mission['missions'][0];
		    $this->smarty->assign('missionCompetences', $mission->getCompetences());
		    $this->smarty->assign('missionLangues', $mission->getLangues());
		    $this->smarty->assign('missionEtudes', $mission->getEtudes());
                    $this->smarty->assign('mission', $mission);
		    $this->smarty->assign('mode', 'detail');//permet d'indiquer que l'on est dans du detail dans les urls
		    return  $mission;
            }
	    else{
		header('location:'.Tools::getLink('missions'));
	    }
	    
	    
             
    }
    //ajouter une mission
    function addMissionAction(){
        $smarty  = $this->smarty;
        $request = $this->request;
        
	if(!$this->membre->getId_client()){
	    header('location:'.Tools::getLink('home'));
	}
	
        $DBadmin  = new DBAdmin();
         
        $smarty->assign('getListExperiences', $DBadmin->getListExperiences());
        $smarty->assign('getListMonnaies', $DBadmin->getListMonnaies());
        $smarty->assign('getListContrats', $DBadmin->getListContrats());
        $smarty->assign('getListFinString', array('DAY'=>'Day(s)', 'MONTH'=>'month(s)', 'YEAR'=>'year(s)'));
        
        if($_POST){
            
           
            
            $request['id_client']      = $this->membre->getId_client();
            $request['id_identifiant'] = $this->membre->getId_identifiant();
            $DBmission= new DBMission();
            if($error = $DBmission->makeMission($request)){
                if(is_array($error) && count($error)>0){
                    $this->smarty->assign('post', $_POST);
                    $this->smarty->assign('error', $error);
                    
                    $listCompetences = $DBadmin->getListCompetences(array(), explode(',', $request['id_competence']));
                    $smarty->assign('getListCompetences', $listCompetences);
                   
                    $listEtudes = $DBadmin->getListEtudes(array(), explode(',', $request['id_etude']));
                    $smarty->assign('getListEtudes', $listEtudes);

                    $listPostes = $DBadmin->getListPostes(array(), explode(',', $request['id_poste']));
                    $smarty->assign('getListPostes', $listPostes);

                    $listCategories = $DBadmin->getListCategories(array(), explode(',', $request['id_categorie']));
                    $smarty->assign('getListCategories', $listCategories);

                    $listLangues = $DBadmin->getListLangues(array(), explode(',', $request['id_langue']));
                    $smarty->assign('getListLangues', $listLangues);
                    
                  
                }
                else{
                    Tools::setFlashMessage(XMLEngine::$trad->global_trad->notifUpdateOK, true, 'success', Tools::getLink('missions'));
                }  
            }
            
            
        }
        
       
    }
    
    //editer une mission
    function editMissionAction(){
        $smarty  = $this->smarty;
        $request =$this->request;
        
        $request["id_identifiant"]     = $this->membre->getId_identifiant();
	$request["id_mission"]         = $this->crypt->id_mission;
    
        $DBmission  = new DBMission();
        
        //Si on update ou alors si on supprime des elements postes, etudes, langues
        if($_POST || isset($request["rmv_id_mission_complement"])){

            if($error = $DBmission->editMission($request)){
		
                if(is_array($error) && count($error)>0){
                    $this->smarty->assign('post', $_POST);
                    $this->smarty->assign('error', $error);
		    $DBadmin = new DBAdmin();
                    $listCategories = $DBadmin->getListCategories(array(), explode(',', $request['id_categorie']));
                    $smarty->assign('getListCategories', $listCategories);
                }
                else{
                    Tools::setFlashMessage(XMLEngine::$trad->global_trad->notifUpdateOK, true, 'success', Tools::getLink('missions'));
                }  
            }
            
        }
        
        $mission = $DBmission->getListMissions(array('id_mission'=>$this->crypt->id_mission, 'id_identifiant'=>$this->membre->getId_identifiant()));
        if($mission['count']>0){
	    
	    $editMission = $mission['missions'][0];
	    //Tools::debugVar($editMission);
    
	    $smarty->assign('editMission', $editMission);
	    
	    $smarty->assign('localiteMission', $editMission->getLocalite());
	    $smarty->assign('getCategorie', $editMission->getCategorie());
	    
	    $DBadmin  = new DBAdmin();
	    $smarty->assign('getListExperiences', $DBadmin->getListExperiences());
	    $smarty->assign('getListMonnaies', $DBadmin->getListMonnaies());
	    $smarty->assign('getListContrats', $DBadmin->getListContrats());
	    $smarty->assign('getListFinString', array('DAY'=>'Day(s)', 'MONTH'=>'month(s)', 'YEAR'=>'year(s)'));
	    //tres pratique pour crypter des infos que l'on envoi via ajax et mettre d'autres choses
	    //Notamentt quand on editer des profil pour les agences
	    $smarty->assign('crypt', Tools::encrypt('id_mission='.$this->crypt->id_mission));
        }
	else{
	    header('location:'.Tools::getLink('home'));
	}
    }
    //Changer le statut d'une mission
    function changeStatusAction(){
                      
        if(!is_numeric($this->crypt->id_mission) && !is_numeric($this->request['id_mission_statut'])){
	    header('location:'.Tools::getLink('missions'));
	}

            
        if(($this->crypt->mode=='search') || ($this->request['mode']=='search')){
            $page = is_numeric($this->crypt->paginate) ? $this->crypt->paginate : $this->request['paginate'];
            $link =  Tools::getLink($this->request['page'], 'search', array('paginate'=> $page));
        }
        else if($this->crypt->mode=='home'){
            $link =  Tools::getLink('home');
        }
        else if($this->crypt->mode=='normal'){
             $link =  Tools::getLink('missions', '', array('paginate'=> $this->crypt->paginate));
        }
	else if($this->crypt->mode=='detail'){
             $link =  Tools::getLink('mission', '', array('id_mission'=> $this->crypt->id_mission), true);
        }
        else{
             $link =  Tools::getLink('missions', '', array('paginate'=> $this->request['paginate']));
        }

        if((is_numeric($this->crypt->id_mission_statut) && isset($this->crypt->id_mission_statut)) || (is_numeric($this->request['id_mission_statut']) && isset($this->request['id_mission_statut']))){
                
                $DBmission = new DBMission();

                if($_POST){
                    $this->request['id_mission']        = $_POST['id_mission'];
                    $this->request['id_mission_statut'] = $_POST['id_mission_statut'];
                }
                else{
                    $this->request['id_mission']        = array($this->crypt->id_mission);
                    $this->request['id_mission_statut'] = $this->crypt->id_mission_statut;
                }

                $this->request['id_identifiant']    = $this->membre->getId_identifiant();
                if($error = $DBmission->editMission($this->request)){

                        if(is_array($error) && count($error)>0){

                            $this->smarty->assign('post', $_POST);
                            $this->smarty->assign('error', $error);
                        }
                        else{
                            Tools::setFlashMessage(XMLEngine::$trad->global_trad->notifUpdateOK, true, 'success-update-statut', $link);
                        }  
                }
		else{
		    Tools::setFlashMessage(XMLEngine::$trad->global_trad->notifUpdateOK, true, 'success', $link);
		} 
        }
         
    }
    //Permet de rediriger la page vers my_profil afin d'appeler le controller CVteque
    //Uniqument quand onsubmit le formulaire avec le bouton proposer au favori
    //Attention donc, cette methode n'est pas appeleée quand on clique sur le lien proposer sous la mission...
    //Moyen simple pour eviter les confusions
    function proposeMissionAction(){
        header('location:'.Tools::getLink('my_profils', 'proposeMission', array("id_mission"=>implode(',',$this->request['id_mission'])),true));
    }
    //Changer le statut d'une mission
    function stopNotificationsAction(){
        if(!is_numeric($this->crypt->id_mission )){
	    header('location:'.Tools::getLink('missions'));
	}

            
        if(($this->crypt->mode=='search') || ($this->request['mode']=='search')){
            $page = is_numeric($this->crypt->paginate) ? $this->crypt->paginate : $this->request['paginate'];
            $link =  Tools::getLink($this->request['page'], 'search', array('paginate'=> $page));
        }
        else if($this->crypt->mode=='home'){
            $link =  Tools::getLink('home');
        }
        else if($this->crypt->mode=='normal'){
             $link =  Tools::getLink('missions', '', array('paginate'=> $this->crypt->paginate));
        }
	else if($this->crypt->mode=='detail'){
             $link =  Tools::getLink('mission', '', array('id_mission'=> $this->crypt->id_mission), true);
        }
        else{
             $link =  Tools::getLink($this->request['page'], '', array('paginate'=> $this->request['paginate']));
        }

        if((is_numeric($this->crypt->mi_reponse_mail) && isset($this->crypt->mi_reponse_mail))){

                $DBmission = new DBMission();

		$this->request['id_mission']        = array($this->crypt->id_mission);
                $this->request['mi_reponse_mail'] = $this->crypt->mi_reponse_mail;
                $this->request['id_identifiant']    = $this->membre->getId_identifiant();
               
                if($error = $DBmission->editMission($this->request)){

                        if(is_array($error) && count($error)>0){

                            $this->smarty->assign('post', $_POST);
                            $this->smarty->assign('error', $error);
                            
                        }
                        else{
                            Tools::setFlashMessage(XMLEngine::$trad->global_trad->notifUpdateOK, true, 'success', $link);
                        }  
                }
        }
         
    }    
    //dupliquer une mission
    function duplicateMissionAction(){
	
	if(!is_numeric($this->crypt->id_mission)){
	    header('location:'.Tools::getLink('missions'));
	}
	
        $DBmission  = new DBMission();

	
	$params = array('id_mission'=>$this->crypt->id_mission, 'id_identifiant'=>$this->membre->getId_identifiant());
	if($id_mission = $DBmission->duplicateMission($params)){
	    
	     Tools::setFlashMessage(XMLEngine::$trad->global_trad->notifUpdateOK, true, 'success', Tools::getLink('edit_mission', 'editMission', array('id_mission'=>$id_mission), true));
	}
       // else
	     //Tools::setFlashMessage('Update OK', true, 'success', Tools::getLink('missions'));
    
        
        
    }
    //republier une mission
    function republishMissionAction(){
	
	if(!is_numeric($this->crypt->id_mission)){
	    header('location:'.Tools::getLink('missions'));
	}
	
        $DBmission  = new DBMission();
	
        if($this->crypt->mode=='search'){
            $link =  Tools::getLink('missions', 'search', array('paginate'=> $this->crypt->paginate));
        }
        else if($this->crypt->mode=='home'){
            $link =  Tools::getLink('home');
        }
        else if($this->crypt->mode=='normal'){
             $link =  Tools::getLink('missions', '', array('paginate'=> $this->crypt->paginate));
        }
	else if($this->crypt->mode=='detail'){
             $link =  Tools::getLink('mission', '', array('id_mission'=> $this->crypt->id_mission), true);
        }
        
        if(is_numeric($this->crypt->id_mission)){
            $params = array('id_mission'=>$this->crypt->id_mission, 'id_identifiant'=>$this->membre->getId_identifiant(), 'mi_republication'=>1);
            if($DBmission->editMission($params, true))
                  Tools::setFlashMessage(XMLEngine::$trad->global_trad->notifUpdateOK, true, 'success', $link);
          
        }
    }
    
    //Recherche de missions
    function searchAction(){
        
	if($this->crypt->id_mission)
                    $this->request['search']['id_mission']=$this->crypt->id_mission;
	
	if($this->request['id_mission_statut'])
                    $this->request['search']['id_mission_statut']=$this->request['id_mission_statut'];
            
        if($this->request['search'])
            $_SESSION['search'] = $this->request['search'];
        
        $this->defaultAction($_SESSION['search'] );
        
	$this->smarty->assign('mode', 'search');//permet d'ajouter un paramtrer dans lien favoris des mission et updtae statut
	$this->smarty->assign('post', $_SESSION['search']);//très pratique puisque il permet de ressortir le post dans la vue


    }
    //ajouter un favoris mission pour un prestataire
    function addFavoriAction(){

        if($this->crypt->mode=='search'){
            $link =  Tools::getLink($this->request['page'], 'search', array('paginate'=> $this->crypt->paginate));
        }
        else if($this->crypt->mode=='home'){
            $link =  Tools::getLink('home');
        }
        else if($this->crypt->mode=='normal'){
             $link =  Tools::getLink('missions', '', array('paginate'=> $this->crypt->paginate));
        }
	else if($this->crypt->mode=='reponse'){
             $link =  Tools::getLink('new_response', '', array('id_mission'=> $this->crypt->id_mission), true);
        }
	else{
	    $link =  Tools::getLink('home');
	}

        $DBmembre = new DBMembre();
        $favoris = array();
        $favoris["id_identifiant"]=DBMembre::$id_identifiant;
        if(is_numeric($this->crypt->id_mission)){
            //le favoris correspond à une sauvegarde de mission
            //donc pour un prestataire

            $favoris["id_mission"]=$this->crypt->id_mission;
            
            $DBmembre->makeFavori($favoris);
            Tools::setFlashMessage(XMLEngine::$trad->global_trad->notifUpdateOK, true, 'success', $link);
          
        }
       
    }
    //supprimer un favoris mission pour un prestataire
    function removeFavoriAction(){
	
        if($this->crypt->mode=='search'){
            $link =  Tools::getLink($this->request['page'], 'search', array('paginate'=> $this->crypt->paginate));
        }
        else if($this->crypt->mode=='home'){
            $link =  Tools::getLink('home');
        }
        else if($this->crypt->mode=='normal'){
             $link =  Tools::getLink('missions', '', array('paginate'=> $this->crypt->paginate));
        }
	else if($this->crypt->mode=='reponse'){
             $link =  Tools::getLink('new_response', '', array('id_mission'=> $this->crypt->id_mission), true);
        }
	else{
	    $link =  Tools::getLink('home');
	}

        
        $DBmembre = new DBMembre();
        $favoris = array();
        $favoris["id_identifiant"]=DBMembre::$id_identifiant;
        if(is_numeric($this->crypt->id_mission)){
            //le favoris correspond à une sauvegarde de mission
            //donc pour un prestataire
            $favoris["id_mission"]=$this->crypt->id_mission;
            $DBmembre->removeFavori($favoris);
            Tools::setFlashMessage(XMLEngine::$trad->global_trad->notifUpdateOK, true, 'success', $link);
          
        }
    }
   
}
?>