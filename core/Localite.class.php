<?PHP
class Localite {//Ne sert que pourles profil 
	
        protected $ville;
	protected $pays; 
        protected $region;
        protected $quartier;
        protected $departement_num;
        protected $departement; 
        protected $localite = Localite; //Objet entier
        
	public function __construct($donnees=Localite){
		
		$this->ville           = $donnees->lo_ville;		
                $this->pays            = $donnees->lo_pays;
                $this->region          = $donnees->lo_region;
                $this->quartier        = $donnees->lo_quartier;
                $this->departement_num = $donnees->lo_departement_short;
                $this->departement     = $donnees->lo_departement;
                
                $this->localite        = $donnees;//On s'en sert essentiellement pour ajouter des localit� chez le prestataire ou pour les affciher
	}
	
        public function __toString(){//Methode appel�e quand on echo $profil->getLocalite() dans les apercus CV
            
            $departement_num = !empty($this->departement) ? ' ('.$this->departement_num.') ' : '';
            
            if(!empty($this->ville))
                return $this->ville.$departement_num.' - '.$this->pays;
            
            elseif(!empty($this->departement))
                return $this->departement.$departement_num.$this->pays;
            
            elseif(!empty($this->region))
                return $this->region. ' ('.$this->pays.')';
		
	    elseif(!empty($this->pays))
                return $this->pays;
	
	    else
		return '-';
            
        }
        
	public function __get($name){
		echo "<span class=\"error\">R�cup�ration de '$name' impossible dans  Localite.class.php</span>";
	}
	
	public function __call($name, $arguments){
		echo "<span class=\"error\">Appel de la m�thode '$name' inexistante avec les argument suivants Localite.class.php</span>"
			. implode(', ', $arguments). "\n";
	}

	public static function __callStatic($name, $arguments){
		echo "<span class=\"error\">Appel de la m�thode statique '$name' inexistante avec les argument suivants Localite.class.php</span>"
			. implode(', ', $arguments). "\n";
        }
	
        
        public function getLocalite(){
            return $this->localite;
        }
        
        

}
