<?php 
 class myException extends Exception { 
  
       
	public function __toString() {            
		return 'Type : '. $this->erreurs($this->code) .'<br>Code :[' . $this->code . ']<br>Message : ' . $this->getMessage() . '<br />Fichier : ' . $this->file . '<br> A la ligne <strong>' . $this->line . '</strong>';
	}
		
	private function erreurs($cle){
		$errortype = array (
			E_ERROR              => 'Erreur',
			E_WARNING            => 'Alerte',
			E_PARSE              => 'Erreur d\'analyse',
			E_NOTICE             => 'Note',
			E_CORE_ERROR         => 'Core Error',
			E_CORE_WARNING       => 'Core Warning',
			E_COMPILE_ERROR      => 'Compile Error',
			E_COMPILE_WARNING    => 'Compile Warning',
			E_USER_ERROR         => 'Erreur sp�cifique',
			E_USER_WARNING       => 'Alerte sp�cifique',
			E_USER_NOTICE        => 'Note sp�cifique',
			E_STRICT             => 'Runtime Notice',
			E_RECOVERABLE_ERROR => 'Catchable Fatal Error'
		);
		
		return $errortype[$cle];
	}	
	
	public function traiteerror(){
		
		$errno = $this->getCode();
		$errmsg = $this->getMessage();
		$filename = $this->getFile();
		$linenum = $this->getLine();
		$dt = date("Y-m-d H:i:s (T)");
		
		$user_errors = array(E_USER_ERROR, E_USER_WARNING, E_USER_NOTICE);
		
		$err = "<errorentry>\n";
		$err .= "\t<datetime>" . $dt . "</datetime>\n";
		$err .= "\t<errornum>" . $errno . "</errornum>\n";
		$err .= "\t<errortype>" . $this->erreurs($errno) . "</errortype>\n";
		$err .= "\t<errormsg>" . $errmsg . "</errormsg>\n";
		$err .= "\t<scriptname>" . $filename . "</scriptname>\n";
		$err .= "\t<scriptlinenum>" . $linenum . "</scriptlinenum>\n";
	
		if (in_array($errno, $user_errors)) {
			$err .= "\t<vartrace>".wddx_serialize_value($vars,"Variables")."</vartrace>\n";
		}
		$err .= "</errorentry>\n\n";
	 
	
		error_log($err, 3, "./logs/error.log");
		if ($errno == E_USER_ERROR) {
			mail("phpdev@example.com","Critical User Error",$err);
		}
	
	}
 } 
 ?>