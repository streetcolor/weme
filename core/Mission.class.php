<?PHP
class Mission extends Client {

	protected $id_mission; 
     
        protected $statut = array(); //int : En cours, Termine, Attente de validation, suspendu
        
	protected $categorie;
	protected $experience;
	
        protected $reference;
        protected $titre;
        protected $description;
        protected $tarif = array();//tarif, id_monnaie, id_contrat
        protected $debut; //Date
        protected $fin   = array();//date, int, string (DAY, MONTH, WEEK, YEAR)
        
	protected $localite = array();
	
        protected $date_reponse; //date
        protected $republication; //int qui symbolise le compteur.
        protected $notification_reponse_email; //Boolean
        
        protected $actif; //Bool G�r� par les moderateurs du site
	
	protected $new_mission;//Permet de savoir si la mission est recente ou non
        
	protected $count_reponses = array();
	
	public function __construct( $donnees=array()){
				
                $this->id_mission          = $donnees['id_mission'];
                $this->statut              = array(
						   'id_statut' => $donnees['id_mission_statut'],
						   'statut'    =>$donnees['adm_statut']
						  );
		
		$this->categorie           = array(
						   'id_categorie' => $donnees['id_categorie'],
						   'adm_categorie'    =>$donnees['adm_categorie']
						  );
                $this->experience          = array(
						   'id_experience' => $donnees['id_experience'],
						   'experience'    =>$donnees['adm_experience']
						  );
		
		$this->date_reponse        = $donnees['mi_date_reponse'];
                $this->reference           = $donnees['mi_reference'];
                $this->titre               = $donnees['mi_titre'];
		$this->description         = $donnees['mi_description'];
		$this->tarif               = array(
							'tarif'=>$donnees['mi_tarif'],
							'monnaie'=>$donnees['adm_monnaie'],
							'contrat'=>$donnees['adm_contrat'],
							'id_monnaie'=>$donnees['id_monnaie'],
							'id_contrat'=>$donnees['id_contrat']
						);
		
		$this->debut   	           = $donnees['mi_debut'];
		$this->fin                 = array(
							'fin'       =>$donnees['mi_fin'],
							'int_fin'   =>$donnees['mi_int_fin'],
							'string_fin'=>$donnees['mi_string_fin']
						);
		
		$this->republication       = $donnees['mi_republication'];
		$this->notification_reponse_email  = $donnees['mi_reponse_mail'];
		$this->actif               = $donnees['actif'];
		
		$this->localite            = array(
							'lo_quartier'         =>$donnees['lo_quartier'],
							'lo_ville'            =>$donnees['lo_ville'],
							'lo_code_postal'      =>$donnees['lo_code_postal'],
							'lo_departement'      =>$donnees['lo_departement'],
							'lo_departement_short'=>$donnees['lo_departement_short'],
							'lo_region'           =>$donnees['lo_region'],
							'lo_region_short'     =>$donnees['lo_region_short'],
							'lo_pays'             =>$donnees['lo_pays'],
							'lo_pays_short'       =>$donnees['lo_pays_short']
						);
		
		$this->count_reponses      = $donnees['mi_reponse_count'];
		
		$this->new_mission         = $donnees['modifie_le'];
		
		//Permet de faire un peu d'heritage pour recuper Nom prenom et Email sans passer par la methode get client.
		parent::__construct($donnees);
		
		
	}
	
	public function __get($name){
		echo "<span class=\"error\">R�cup�ration de '$name' impossible dans  Mission.class.php</span>";
	}
	
	public function __call($name, $arguments){
		echo "<span class=\"error\">Appel de la m�thode '$name' inexistante avec les argument suivants Mission.class.php</span>"
			. implode(', ', $arguments). "\n";
	}

	public static function __callStatic($name, $arguments){
		echo "<span class=\"error\">Appel de la m�thode statique '$name' inexistante avec les argument suivants Mission.class.php</span>"
			. implode(', ', $arguments). "\n";
        }
	
	public function getId_mission(){
		return $this->id_mission;
	}
	
	public function getId_experience(){
		return $this->id_experience;
	}
	
	public function getStatut(){
            return (object) $this->statut;
        }
	
	public function getNew($nbJour =NOMBRE_DAY_NEW){
		//On recupere maintenant la date actuelle
		$var_day        = date('Y-m-d H:m:s');
		$var_day_tmp    = $this->new_mission;	
		
		//Difference
		$d1             = new DateTime($var_day);
		$d2             = new DateTime($var_day_tmp);
		$difDay         = $d1->diff($d2)->format('%d');
		
		if($difDay < $nbJour)
			return true;
		else
			return false;
        }
        
	
	
	public function getCategorie(){
		return (object )$this->categorie;
	}
	
	public function getExperience(){
		return (object )$this->experience;
	}
	
	public function getReference(){
		return $this->reference;
	}
	
	public function getTitre(){
		return $this->titre;
	}
	
	public function getDateReponse(){
		return Tools::shortConvertDBDateToLan($this->date_reponse);
	}
	public function getDescription(){
		return $this->description;
	}
	

	public function getTarif($string=false){
            
            $tarif = $this->tarif;
	   
            if($string)
                return $tarif["tarif"]." ".$tarif["monnaie"]. " ".$tarif["contrat"];
            else{
		
                return (object)$tarif;
            }
	}
	
	public function getDebut(){
		
		return Tools::shortConvertDBDateToLan($this->debut);

	}
	
	public function getFin($string=true){
		
		$fin = $this->fin;

		if(!$fin['int_fin'])//si pas de date de fin
			return '-';
		
		if($string){
			return Tools::shortConvertDBDateToLan($fin['fin']);
		}
		else
			return (object)$fin;
	}
	
	public function getRepublication(){
		return $this->republication;
	}
	
	public function getNotificationEmail(){
            return $this->notification_reponse_email;
        }
	
	//Pour acceder � cette methode
        //$mission = new Mission();
        //$missionClient = $mission->getLocalite();
	public function getLocalite(){
		return new Localite((object)$this->localite);
	}
        //Pour acceder � cette methode
        //$mission = new Mission();
        //$missionClient = $mission->getLangues();
	public function getLangues(){
		$complements = new DBMission();
		return (object) $complements->getListLangues(array('id_mission'=>$this->id_mission));
	}
	//Pour acceder � cette methode
        //$mission = new Mission();
        //$missionClient = $mission->getCompetences();
	public function getCompetences(){
		$complements = new DBMission();
		return (object) $complements->getListCompetences(array('id_mission'=>$this->id_mission));
	}
	//Pour acceder � cette methode
        //$mission = new Mission();
        //$missionClient = $mission->getPostes();
	public function getPostes(){
		$complements = new DBMission();
		return (object) $complements->getListPostes(array('id_mission'=>$this->id_mission));
	}
	//Pour acceder � cette methode
        //$mission = new Mission();
        //$missionClient = $mission->getEtudes();
	public function getEtudes(){
		$complements = new DBMission();
		return (object) $complements->getListEtudes(array('id_mission'=>$this->id_mission));
	}
	//Pour acceder � cette methode
        //$mission = new Mission();
        //$missionClient = $mission->getClient()->getEntreprise();
        public function getClient(){
		$client = new DBMembre();
		return (object) $client->getListClients($this->id_identifiant);
        }
	//Pour acceder � cette methode
        //$mission = new Mission();
        //$missionClient = $mission->getReponses(true);
        public function getReponses($count=false){
		
		if($count){
			return $this->count_reponses;
		}
		else{
			$listReponses = new DBReponse();
			return (object) $listReponses->getListReponses(array('id_mission'=>$this->id_mission), array(), array(0,NOMBRE_PER_PAGE));
		}
        }
        

}
