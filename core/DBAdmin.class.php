<?php

class DBAdmin extends DBConnect{
	
	//TRES IMPORTANT
	//Les m�thodes save doivent �tre priv�e et sonr uniquement appel�e via les m�thodes make
	//Les tests de saisie s'effectuent OBLIGATOIREMENT depuis les m�thodes make
	
	public function saveComplement($entry=array()){
		        
		if(empty($entry['entry']) || is_numeric($entry['entry']) || empty($entry['table']) || is_numeric($entry['table']))
			return false;
		
		$req = "
			  INSERT INTO adm_".$entry['table']."s SET
			  modifie_le = NOW(),
			  adm_".$entry['table']."_".$_SESSION['langue']." = ".$this->db->quote(trim($entry['entry']), PDO::PARAM_STR).",
			  actif = 1
			  ON DUPLICATE KEY UPDATE adm_".$entry['table']."_".$_SESSION['langue']." = adm_".$entry['table']."_".$_SESSION['langue'];
			  
			  if($debug)
				  throw new myException($req);
			  if(DEBUG)
				  parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
			
			
			if($id = $this->db->exec($req)){
			      //Tools::debugVar($this->db->lastInsertId());
				return $this->db->lastInsertId();
			}
			else{
			      switch($entry['table']){
				      case 'langue':
					      $getComp = $this->getListLangues(array($entry['entry']));
					      return $getComp[0]->id_langue;
				      
				      case 'competence':
					      $getComp = $this->getListCompetences(array($entry['entry']));
					      return $getComp[0]->id_competence;
			      }
			}
	}
	public function getListExperiences(){
		$experiences = array();
		$req = "SELECT id_experience, adm_experience_".$_SESSION["langue"]." AS adm_experience FROM adm_experiences WHERE actif = 1 ";
			
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'pink', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
		
		$resultats = $this->db->query($req);

		while ($experience = $resultats->fetch(PDO::FETCH_OBJ))
			 $experiences[] = $experience;	
		
		$resultats->closeCursor();
		
		if(count($experiences)){
			return $experiences;
		}
		else{
			return 'R�cuperation des experiences impossible !';
		}
		
	}
        
        public function getListCivilites(){
		$civilites = array();
		$req = "SELECT id_civilite, adm_civilite_".$_SESSION["langue"]." AS adm_civilite FROM adm_civilites WHERE actif = 1 ";
			
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'pink', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
		
		$resultats = $this->db->query($req);

		while ($civilite = $resultats->fetch(PDO::FETCH_OBJ))
			 $civilites[] = $civilite;	
		
		$resultats->closeCursor();
		
		if(count($civilites)){
			return $civilites;
		}
		else{
			return 'R�cuperation des civilites impossible !';
		}
		
	}
        
        public function getListMonnaies(){
		$monnaies = array();
		$req = "SELECT id_monnaie, adm_monnaie FROM adm_monnaies WHERE actif = 1 ";
			
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'pink', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
		
		$resultats = $this->db->query($req);

		while ($monnaie = $resultats->fetch(PDO::FETCH_OBJ))
			 $monnaies[] = $monnaie;	
		
		$resultats->closeCursor();
		
		if(!DEBUG)
			return $monnaies;
		
		if(count($monnaies)){
			return $monnaies;
		}
		else{
			return 'R�cuperation des monnaies impossible !';
		}
		
	}
	public function getListStructures(){
		$structures = array();
		$req = "SELECT id_structure, adm_structure_".$_SESSION["langue"]." AS adm_structure FROM adm_structures WHERE actif = 1 ";
			
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'pink', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
		
		$resultats = $this->db->query($req);

		while ($structure = $resultats->fetch(PDO::FETCH_OBJ))
			 $structures[] = $structure;	
		
		$resultats->closeCursor();
		
		return $structures;
		
	}
	public function getListContrats(){
		$contrats = array();
		$req = "SELECT id_contrat, adm_contrat_".$_SESSION["langue"]." AS adm_contrat FROM adm_contrats WHERE actif = 1 ";
			
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'pink', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
		
		$resultats = $this->db->query($req);

		while ($contrat = $resultats->fetch(PDO::FETCH_OBJ))
			 $contrats[] = $contrat;	
		
		$resultats->closeCursor();
		
		if(!DEBUG)
			return $contrats;

		if(count($contrats)){
			return $contrats;
		}
		else{
			return 'R�cuperation des contrats impossible !';
		}
		
	}
	
	public function getListPostes($search = array(), $id=array()){
		$postes = array();
		$req = "SELECT  id_poste, adm_poste_".$_SESSION["langue"]." AS adm_poste FROM adm_postes WHERE actif = 1 ";
		
		if(count($search))
			$req .= "AND MATCH (adm_poste_".$_SESSION["langue"].") AGAINST ('".implode("','", $search)."' IN BOOLEAN MODE)";
		
		if(count($id))
			$req .= "AND id_poste IN ('".implode("','", array_filter($id))."')";
			
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'pink', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
		
		$resultats = $this->db->query($req);

		while ($poste = $resultats->fetch(PDO::FETCH_OBJ))
			 $postes[] = $poste;	
		
		$resultats->closeCursor();
		
		return $postes;
		
	}
	
	public function getListCompetences($search = array(), $id=array()){
		
		$competences = array();
		$req = "SELECT  id_competence, adm_competence_".$_SESSION["langue"]." AS adm_competence FROM adm_competences WHERE actif = 1 ";
		
		if(count($search))
			$req .= "AND MATCH (adm_competence_".$_SESSION["langue"].") AGAINST ('".implode("','", $search)."' IN BOOLEAN MODE)";
		
		if(count($id))
			$req .= "AND id_competence IN ('".implode("','", array_filter($id))."')";
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'pink', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
		
		$resultats = $this->db->query($req);

		while ($competence = $resultats->fetch(PDO::FETCH_OBJ))
			 $competences[] = $competence;	
		
		$resultats->closeCursor();
		
		return $competences;
		
	}
	
	public function getListCategories($search = array(), $id=array()){
		$categories = array();
		$req = "SELECT id_categorie, adm_categorie_".$_SESSION["langue"]." AS adm_categorie FROM adm_categories WHERE actif = 1 ";
		
		if(count($search))
			$req .= "AND MATCH (adm_categorie_".$_SESSION["langue"].") AGAINST ('".implode("','", $search)."' IN BOOLEAN MODE)";
	
		if(count($id))
			$req .= "AND id_categorie IN ('".implode("','", array_filter($id))."')";
			
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'pink', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	

		$resultats = $this->db->query($req);

		while ($categorie = $resultats->fetch(PDO::FETCH_OBJ))
			 $categories[] = $categorie;	
		
		$resultats->closeCursor();
		
		return $categories;
		
	}
	public function getListEtudes($search = array(), $id=array()){
		$etudes = array();
		$req = "SELECT id_etude, adm_etude_".$_SESSION["langue"]." AS adm_etude FROM adm_etudes WHERE actif = 1 ";
		
		if(count($search))
			$req .= "AND MATCH (adm_etude_".$_SESSION["langue"].") AGAINST ('".implode("','", $search)."' IN BOOLEAN MODE)";
	
		if(count($id))
			$req .= "AND id_etude IN ('".implode("','", array_filter($id))."')";
		
			
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'pink', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
		
		$resultats = $this->db->query($req);

		while ($etude = $resultats->fetch(PDO::FETCH_OBJ))
			 $etudes[] = $etude;
			 
		$resultats->closeCursor();
		
		return $etudes;
		
	}
	public function getListLangues($search = array(), $id=array()){
		$langues = array();
		$req = "SELECT id_langue, adm_langue_".$_SESSION["langue"]." AS adm_langue FROM adm_langues WHERE actif = 1 ";
		
		if(count($search))
			$req .= "AND MATCH (adm_langue_".$_SESSION["langue"].") AGAINST ('".implode("','", $search)."' IN BOOLEAN MODE)";
	
		if(count($id))
			$req .= "AND id_langue IN ('".implode("','", array_filter($id))."')";
			
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'pink', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
		
		$resultats = $this->db->query($req);

		while ($langue = $resultats->fetch(PDO::FETCH_OBJ))
			 $langues[] = $langue;	
		
		$resultats->closeCursor();
		
		return $langues;
		
	}
	
	public function getListStatutsReponses(){
		$statuts = array();
		$req = "SELECT id_reponse_statut, adm_reponse_statut_".$_SESSION["langue"]." AS adm_statut FROM adm_reponses_statuts WHERE actif = 1 ";
			
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'pink', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
		
		$resultats = $this->db->query($req);

		while ($statut = $resultats->fetch(PDO::FETCH_OBJ))
			 $statuts[] = $statut;	
		
		$resultats->closeCursor();
		
		return $statuts;
		
	}
	public function getListStatutsMissions(){
		$statuts = array();
		$req = "SELECT id_mission_statut, adm_mission_statut_".$_SESSION["langue"]." AS adm_statut FROM adm_missions_statuts WHERE actif = 1 ";
			
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'pink', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
		
		$resultats = $this->db->query($req);

		while ($statut = $resultats->fetch(PDO::FETCH_OBJ))
			 $statuts[] = $statut;	
		
		$resultats->closeCursor();
		
		return $statuts;
		
	}
	//Uniquement pour les recherches autocomplete
	final function getListSelect($table, $string, $obj=false, $debug=false){
		$select = array();

		if($obj)
			$req .= "SELECT id_".$table." AS id,  adm_".$table."_".$_SESSION['langue']." AS value FROM adm_".$table."s  WHERE adm_".$table."_".$_SESSION['langue']." LIKE  '%".$string."%' LIMIT 10";
		else
			$req .= "SELECT id_".$table." ,adm_".$table."_".$_SESSION['langue']." AS value FROM adm_".$table."s WHERE adm_".$table."_".$_SESSION['langue']." LIKE  '%".$string."%'  LIMIT 10";
		
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'pink', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
			
		$resultats = $this->db->query($req);
		
		
		if(!$obj){
			while ($ligne = $resultats->fetch(PDO::FETCH_ASSOC)){
				 $select[] = array('id'=>$ligne["id_".$table], 'libelle'=>$ligne["adm_".$table]);	
			}
		}else{
		
			
			while ($ligne = $resultats->fetch(PDO::FETCH_ASSOC)){
				 $select[] =$ligne;	
			}
		}
		
		
		$resultats->closeCursor(); 
		return $select;
		
	}
    
}
