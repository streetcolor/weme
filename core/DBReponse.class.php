<?php
class DBReponse extends DBConnect{
	
	//TRES IMPORTANT
	//Les méthodes save doivent être privées et son uniquement appelées via les méthodes make
	//Les tests de saisie s'effectuent OBLIGATOIREMENT depuis les méthodes make
	
        const REPONSE_ETUDE    = 1;
        const REPONSE_RETENUE  = 2;
        const REPONSE_DECLINEE = 3;
	const REPONSE_ATTENTE  = 4;

	private function saveReponse($reponse=array(), $ignore=array(), $debug=false){
                
               
		$req = "INSERT INTO usr_missions_reponses SET
                        modifie_le = NOW(),
                        id_mission = ".$this->db->quote($reponse['id_mission'], PDO::PARAM_STR).",
			id_identifiant = ".$this->db->quote($reponse['id_identifiant'], PDO::PARAM_STR).",
                        id_prestataire_profil = ".$this->db->quote($reponse['id_prestataire_profil'], PDO::PARAM_STR).",
                        rp_reponse = ".$this->db->quote($reponse['rp_reponse'], PDO::PARAM_STR).",
                        rp_tarif = ".$this->db->quote($reponse['rp_tarif'], PDO::PARAM_STR).",
                        id_monnaie = ".$this->db->quote($reponse['id_monnaie'], PDO::PARAM_STR).",
                        id_contrat = ".$this->db->quote($reponse['id_contrat'], PDO::PARAM_STR).",
                        id_file = ".$this->db->quote($reponse['id_file'], PDO::PARAM_STR).",
                        id_pj = ".$this->db->quote($reponse['id_pj'], PDO::PARAM_STR).",
                        rp_commentaire = ".$this->db->quote($reponse['rp_commentaire'], PDO::PARAM_STR).",
                        rp_reponse_mail = ".$this->db->quote($reponse['rp_reponse_mail'], PDO::PARAM_STR).",
                        id_reponse_statut = ".$this->db->quote($reponse['id_reponse_statut'], PDO::PARAM_STR).",
                        rp_archiver = ".$this->db->quote($reponse['rp_archiver'], PDO::PARAM_STR);

		if($debug)
			throw new myException($req);
		//if(DBAdmin::$id_developper)
		//	DBAdmin::$error .=  '<span class="blue bold">'.__CLASS__.'</span> >>> <span class="pink bold"> '.  __FUNCTION__.'()</span><hr><br>'.$req.'<br><br>';
		
                if($this->db->exec($req)){
			
                        return  $this->db->lastInsertId();
        
		}

		
	}
	
	private function updReponse($reponse=array(), $ignore=array(), $debug=false){
                
               
		$req .= "UPDATE usr_missions_reponses UMR
			INNER JOIN usr_missions AS UM ON UMR.id_mission = UM.id_mission
			SET
                        UMR.modifie_le = NOW(),";
		
		if(isset($reponse['rp_archiver']))
			$req .= "UMR.rp_archiver = NOW() ";
			
		else
			$req .= "UMR.id_reponse_statut = ".$this->db->quote($reponse['id_reponse_statut'], PDO::PARAM_STR);
			
		$req .="WHERE id_mission_reponse IN (".implode(',', $reponse['id_mission_reponse']).")
			AND UM.id_identifiant = ".$this->db->quote($reponse['id_identifiant'], PDO::PARAM_STR)." AND	
                        UMR.id_mission = ".$this->db->quote($reponse['id_mission'], PDO::PARAM_STR);
                       
		if($debug)
			throw new myException($req);
		//if(DBAdmin::$id_developper)
		//	DBAdmin::$error .=  '<span class="blue bold">'.__CLASS__.'</span> >>> <span class="pink bold"> '.  __FUNCTION__.'()</span><hr><br>'.$req.'<br><br>';
		//Tools::debugVar($req);
                if($this->db->exec($req)){
			
                        return  true;
        
		}

		
	}
	
	
	public function getListReponses($filtre = array(), $searchFiltre=array(), $limit=array(0,NOMBRE_PER_PAGE), $justCount=false){
		
		$reponses = array();
		$statistique = array(self::REPONSE_ETUDE=>0,self::REPONSE_RETENUE=>0,self::REPONSE_DECLINEE=>0,self::REPONSE_ATTENTE=>0);
                $counter = array(
				'COUNT' => 'SELECT count(DISTINCT(UMR.id_mission_reponse)) AS total, ' ,
				'SELECT'=> 'SELECT DISTINCT(UMR.id_mission_reponse), '
				);
		$req = "UMR.id_mission, UMR.id_prestataire_profil,
			UM.id_identifiant/* id du client qui a deposé sa mission */, UM.mi_titre,
			UCR.cl_entreprise,
			AU.id_structure, AU.adm_structure_".$_SESSION["langue"]." AS adm_structure,
			UI.usr_email, /* Email du Prestaire ne pas confondre avec le client */
			UMR.rp_reponse, UMR.rp_tarif, UMR.id_monnaie, UMR.id_contrat, UMR.id_file, UMR.id_pj, UMR.rp_commentaire, UMR.rp_reponse_mail,  UMR.rp_archiver,
			UMR.cree_le, UMR.id_reponse_statut, AR.adm_reponse_statut_".$_SESSION['langue']." AS adm_statut,
                        UPP.pf_nom, UPP.pf_prenom,/*Nom et Prenom du contact Direct des agence ou alors des Freelan et Intermittent*/
			AM.adm_monnaie, AT.adm_contrat_".$_SESSION['langue']." AS adm_contrat,
			UF.fichier_reference, UF.id_file, UF.fichier_extension 	, UF.fichier_nom, UF.fichier_path, UF.fichier_type, UF.id_identifiant AS id_identifiant_prestataire, /*Exceptionellement ont va renommer id_identifiant pour eviter les confusions pour le ressortir dans la classe reponse*/
			UP.pr_standard_indicatif, UP.pr_standard
			
			FROM usr_missions_reponses UMR
		        INNER JOIN usr_prestataires_profils  AS UPP ON  UMR.id_identifiant  = UPP.id_identifiant
			INNER JOIN usr_prestataires          AS UP ON  UP.id_identifiant    = UPP.id_identifiant 
			INNER JOIN usr_missions              AS UM ON UMR.id_mission        = UM.id_mission
			INNER JOIN usr_clients               AS UCR ON UCR.id_client        = UM.id_client
			INNER JOIN usr_identifiants          AS UI ON UI.id_identifiant     = UMR.id_identifiant /* Pour recuperer le mail du prestataire Admin */ 
			LEFT OUTER JOIN adm_contrats         AS AT ON UMR.id_contrat        = AT.id_contrat
			LEFT OUTER JOIN adm_structures 	     AS AU ON UP.id_structure       = AU.id_structure /*On va recuperer la structure de notre prestataire*/
			LEFT OUTER JOIN adm_monnaies         AS AM ON UMR.id_monnaie        = AM.id_monnaie
			LEFT OUTER JOIN adm_reponses_statuts AS AR ON UMR.id_reponse_statut = AR.id_reponse_statut
			LEFT OUTER JOIN usr_files AS UF ON UF.id_file = UMR.id_pj
			
			WHERE UMR.modifie_le AND UPP.id_role= ".$this->db->quote(DBMembre::TYPE_ADMIN, PDO::PARAM_STR);
			
		foreach($filtre as $key=>$value){
				
				if(($key=='id_identifiant' && isset($filtre['id_client']) || $key=='id_client'))//Si Appel de missions en tant que client attention à bien indiquer un id_client
					$req .= " AND UM.".$key." = ".$this->db->quote($value, PDO::PARAM_STR);
					
				elseif($key=='id_identifiant')
					$req .= " AND UMR.".$key." = ".$this->db->quote($value, PDO::PARAM_STR);
					
				else{
					if(is_array($value)){
						if(count($value))
							$req .= " AND UMR.".$key." IN ('".implode("','", $value)."')";
						else
							$req .= " AND UMR.".$key." IN ('".implode(',', $value)."')";	
					}
					else
						$req .= " AND UMR.".$key." = ".$this->db->quote($value, PDO::PARAM_STR);
				}
		}		
                
		//Si on a des entrées dans la recherche
		if(is_array($searchFiltre) && count($searchFiltre)){
			
			if(!empty($searchFiltre['id_reponse_statut']) && is_numeric($searchFiltre['id_reponse_statut'])){
				
				$req .= "AND ( UMR.id_reponse_statut = ".$this->db->quote($searchFiltre['id_reponse_statut'], PDO::PARAM_STR)." ) ";
				
			}
			if(!empty($searchFiltre['id_mission']) && is_numeric($searchFiltre['id_mission'])){
				
				$req .= "AND ( UM.id_mission = ".$this->db->quote($searchFiltre['id_mission'], PDO::PARAM_STR)." ) ";
				
			}
			if(!empty($searchFiltre['rp_fin']) || !empty($searchFiltre['rp_fin'])){
				
				
				
				if(Tools::verifDate($searchFiltre['rp_fin']) && Tools::verifDate($searchFiltre['rp_debut'])){
					$debut = Tools::shortConvertFrDateToDb($searchFiltre['rp_debut']);
					$fin   = Tools::shortConvertFrDateToDb($searchFiltre['rp_fin']);
					
					$d1 = new DateTime($debut);
					$d2 = new DateTime($fin);
					
					if($d1 < $d2){
						$req .= "AND ( UMR.cree_le >= '$debut') AND ('$fin' >= UMR.cree_le)";
					}
					elseif($d1 == $d2){
						$req .= "AND ( CAST(UMR.cree_le AS DATE) = '$debut')";
					}
					
					
				}
				elseif(Tools::verifDate($searchFiltre['rp_debut'])){
					$debut = Tools::shortConvertFrDateToDb($searchFiltre['rp_debut']);
					$req .= "AND ( UMR.cree_le >= '$debut')";
				}
				elseif(Tools::verifDate($searchFiltre['rp_fin'])){
					$fin = Tools::shortConvertFrDateToDb($searchFiltre['rp_fin']);
					$req .= "AND (UMR.cree_le <= '$fin' )";
				}
				else{
					//nothing
				}
				
			}
			
			
		}
		
		
		$req .= " ORDER BY  UMR.id_reponse_statut DESC, UMR.cree_le DESC  ";
		
		if(is_array($limit)){
			$limit = array(
					'COUNT' => '' ,
					'SELECT'=> "LIMIT ".implode(",", $limit)
					);
		}
		
		if($debug)
			throw new myException($counter['COUNT'].$req.$limit['COUNT']);
		if(DEBUG)
			parent::$request[] =  array('color'=>'green', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$counter['COUNT'].$req.$limit['COUNT']);	

		//Le count avec la requete
		$resultats = $this->db->query($counter['COUNT'].$req.$limit['COUNT']);
		
		//si on un total superieur a zéro
		//on reexecute la requete afin de recuperer les elements
                if($count = $resultats->fetch(PDO::FETCH_OBJ)->total){
			
			if($justCount)
				return array('count'=>$count, 'reponses'=>$reponses, 'statistiques'=>$statistique);
			
			if($debug)
				throw new myException($counter['SELECT'].$req.$limit['SELECT']);
			if(DEBUG)
				parent::$request[] =  array('color'=>'green', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$counter['SELECT'].$req.$limit['SELECT']);
						
			$resultats = $this->db->query($counter['SELECT'].$req.$limit['SELECT']);
			
			while ($reponse = $resultats->fetch(PDO::FETCH_ASSOC)){
				switch($reponse['id_reponse_statut']){
					case self::REPONSE_ETUDE:
						$statistique[self::REPONSE_ETUDE] += 1;
						break;
					case self::REPONSE_RETENUE:
						$statistique[self::REPONSE_RETENUE] += 1;
						break;
					case self::REPONSE_DECLINEE:
						$statistique[self::REPONSE_DECLINEE]+= 1;
						break;
					case self::REPONSE_ATTENTE:
						$statistique[self::REPONSE_ATTENTE] += 1;
						break;
				}
				 $reponses[] = new Reponse($reponse);	
			}
			
			$resultats->closeCursor();
		}
		
		return array('count'=>$count, 'reponses'=>$reponses, 'statistiques'=>$statistique);
		
	}
        //Methode appelée pour procéder à l'ajout d'une mission
	public function makeReponse($array=array()){
		$error = array(); //on stock nos erreurs eventuelles
		// I. On commence par hydrater nos tableaux (cast des variable int, string...) unb pour la réponse et l'autre pour la PJ
		$reponse = array(
				'id_mission' =>              $array['id_mission'],
                                'id_prestataire_profil' =>   $array['id_prestataire_profil'],
				'id_identifiant'       =>    $array['id_identifiant'],
                                'rp_reponse' =>              $array['rp_reponse'],
                                'rp_tarif' =>                $array['rp_tarif'],
                                'id_monnaie' =>              $array['id_monnaie'],
                                'id_contrat' =>              $array['id_contrat'],
                                'id_pj' =>                   $array['id_pj'],
                                'rp_commentaire' =>          $array['rp_commentaire'],
                                'rp_reponse_mail' =>         $array['rp_reponse_mail'],
                                'id_reponse_statut' =>       4,
                                'rp_archiver' =>             $array['rp_archiver']
                        );
		
		$pj      =  array(
				'id_identifiant'=>           $array['id_identifiant'],
				'file' =>                    $_FILES,
				'file_type'=>                'pj'
			);
		
		//Si des profils ont été checkées
		if(count($array['id_prestataire_profil'])){
			$reponse['id_prestataire_profil'] = implode(',', $array['id_prestataire_profil']);
		}
		
		//Si des réalisation ont été checkées
		if(count($array['id_file'])){
			$reponse['id_file'] = implode(',', $array['id_file']);
		}
		//Si un PJ a été accompagnée
		if(!empty($pj['file']['file']['name'])){
			$DBfile = new DBFile;
			if($return = $DBfile->makeFile($pj)){//Cette methode s'occupe de la gestion de copie et d'enregistrement des fichiers
				if(is_array($return))
					return $return;
				else
					$reponse['id_pj'] = $return;
			}	
			 
		}

		//Tools::debugVar($reponse);
		// II. On effectue les tests sur les entrées saisies
		unset($reponse['crypt']);
		if(!isset($reponse['id_mission']) || empty($reponse['id_mission']))
		       $error['global'] = XMLEngine::$trad->global_trad->notifMissionInnaccessible;
		if(!isset($reponse['id_identifiant']) || empty($reponse['id_identifiant']))
		       $error['global'] = XMLEngine::$trad->global_trad->notifPrestataireInnaccessible;
		if(!isset($reponse['id_prestataire_profil']) || empty($reponse['id_prestataire_profil']))
		       $error['id_prestataire_profil'] =XMLEngine::$trad->global_trad->notifSelectProfil;
		if(!isset($reponse['rp_reponse']) || empty($reponse['rp_reponse']))
		       $error['rp_reponse'] = XMLEngine::$trad->global_trad->notifChampsObligatoire;
		if(!isset($reponse['rp_tarif']) || empty($reponse['rp_tarif']))
		       $error['rp_tarif'] = XMLEngine::$trad->global_trad->notifChampsObligatoire;
		if((!is_numeric($reponse['rp_tarif']) && !empty($reponse['rp_tarif'])) || (!is_numeric($reponse['id_monnaie']) && !empty($reponse['id_monnaie'])))
		       $error['rp_tarif'] =  XMLEngine::$trad->global_trad->notifMissionTarifInvalide;
		if(!is_numeric($reponse['id_contrat']))
		       $error['id_contrat'] = XMLEngine::$trad->global_trad->notifMissionContratObligatoire;

	        if(count($error)>0)//on a des erreurs on stop ici
			return $error;
			
                // III. On enregistre dans la base de donnée
		if($id_reponse = $this->saveReponse($reponse)){
			
			//ON INCREMENTE LA COLONNE DE LA TABLE MISSIONS 
			$DBmission = new DBMission();
			$DBmission->editMission(array('id_mission'=>$reponse['id_mission'], 'mi_reponse_count'=>1));
			// IV. On fait les traitements 	adequats envoie de mails et generation des messages de retour
			if($array['mission']->getNotificationEmail()){
				$reponse = $this->getListReponses(array('id_mission_reponse'=>$id_reponse, 'id_mission'=>$reponse['id_mission']));
				$reponse = $reponse['reponses'][0];
				$message["sujet"]   = XMLEngine::$trad->global_trad->mailClientReponseMissionTitre;
				$message["email"]   = $array['mission']->getEmail();
				
				$string = XMLEngine::$trad->global_trad->mailClientReponseMission;
				$patterns = array();
				$patterns[0] = '/{{nom}}/';
				$patterns[1] = '/{{prenom}}/';
				$patterns[2] = '/{{missionTitre}}/';
				$patterns[3] = '/{{reponse}}/';
				$patterns[4] = '/{{tarif}}/';
				$replacements = array();
				$replacements[0] = $array['mission']->getNom();
				$replacements[1] = $array['mission']->getPrenom();
				$replacements[2] = $array['mission']->getTitre();
				$replacements[3] = $reponse->getReponse();
				$replacements[4] = $reponse->getTarif(true);
				
				$message['message'] = preg_replace($patterns, $replacements, $string);
				Tools::sendEmail($message);
				
			}
			return $id_reponse; 
			
		}
	
	}
	//Methode appelée pour procéder à la mise à jour des reponse
	public function editReponse($array=array()){
		$error = array(); //on stock nos erreurs eventuelles
		// I. On commence par hydrater nos tableaux (cast des variable int, string...) unb pour la réponse et l'autre pour la PJ
		$reponse = array(
				'id_mission' =>              $array['id_mission'],
                                'id_mission_reponse' =>      $array['id_mission_reponse'],
                                'id_reponse_statut' =>       $array['id_reponse_statut'],
				'id_identifiant' =>          $array['id_identifiant']

                        );
				
		//Tools::debugVar(count($reponse['id_mission_reponse']));
		// II. On effectue les tests sur les entrées saisies
		unset($reponse['crypt']);
		if(!isset($reponse['id_mission']) || empty($reponse['id_mission']))
		       $error['id_mission'] = XMLEngine::$trad->global_trad->notifMissionInnaccessible;
		if(!isset($reponse['id_identifiant']) || empty($reponse['id_identifiant']))
		       $error['id_identifiant'] = XMLEngine::$trad->global_trad->notifClientInnaccessible;
		if(!isset($array['rp_archiver']) && (!isset($reponse['id_reponse_statut']) || empty($reponse['id_reponse_statut'])))
		       $error['id_reponse_statut'] = XMLEngine::$trad->global_trad->notifStatutInnaccessible;
		if(!is_array($reponse['id_mission_reponse']))
		       $error['id_mission_reponse'] = XMLEngine::$trad->global_trad->notifSelectReponses;
		       
	        if(count($error)>0)//on a des erreurs on stop ici
			return $error;

		//Si on souhaite uniquement archiver une réponse
		//on unset $reponse et on le redefini
		if($array['rp_archiver']){
			unset($reponse);
			$reponse = array(
				'id_mission' =>              $array['id_mission'],
                                'id_mission_reponse' =>      $array['id_mission_reponse'],
				'id_identifiant' =>          $array['id_identifiant'],
				'rp_archiver' =>             date('Y-m-d h:m:s')

                        );
			
		}
                // III. On enregistre dans la base de donnée

		if($id_reponse = $this->updReponse($reponse)){
			// IV. On effectue nos traitement mail et autres
			if(isset($reponse['rp_archiver']))
				return true;
			
			$getReponse = $this->getListReponses(array('id_mission_reponse'=>$reponse['id_mission_reponse'], 'id_mission'=>$reponse['id_mission']));
			foreach($getReponse['reponses'] as $reponse){
				if($reponse->getNotificationEmail()){
					$string = XMLEngine::$trad->global_trad->mailPrestaUpdateReponse;
					$patterns = array();
					$patterns[0] = '/{{reponseTitre}}/';
					$patterns[1] = '/{{statut}}/';
					
					$replacements = array();
					$replacements[0] = $reponse->getTitre();
					$replacements[1] = $reponse->getStatut()->statut;
					
					
					$message['message'] = preg_replace($patterns, $replacements, $string);
					$message['sujet']   = XMLEngine::$trad->global_trad->mailPrestaUpdateReponseSujet;
					$message['email']   = $reponse->getEmail();
					Tools::sendEmail($message);				
				}
	
			}
			return true; 
			
		}
	
	}
        
}
