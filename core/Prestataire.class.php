<?PHP
class Prestataire {

	protected $localisation; //langue sur laquelle le compte � �t� cr�� FR, EN, ES
        protected $id_identifiant;
        protected $id_prestataire;
	protected $id_prestataire_profil;//le profil qui possed un type role admin 
	
	protected $nom; 
        protected $prenom;
	protected $update;
	
	protected $civilite;  //array
	 
	protected $id_type; //type Client ou Prestataire
        protected $structure           = array(); //Intermittent, Freelance, Agence
	
	
	protected $pourcentage;//somme des 4 champs dans BDD
	protected $pourcentage_detail;//5 champs pourcentage en detail array();
	
	protected $entreprise;
	protected $presentation;
	
	protected $standard            = array();//Si agence numero de telephone de l'agence indicatif + numero
        protected $adresse_livraison   = array();
        protected $adresse_facturation = array();
	
        protected $email; //email d'identification
	
	protected $notification_mail; //Bool true or false
	
	protected $abonnement = false; //Un simple boolean que l'on va setter dans le contructeur et qui va faire office de passe droit dans cette classe et dans la class profil
	
	protected $jabber = array();//On va stocker les ID jabber de notre prestataire ici
		
	public function __construct( $donnees=array()){
		
				
                $this->localisation          = $donnees['usr_trad'];
		$this->id_identifiant        = $donnees['id_identifiant'];
                $this->id_prestataire        = $donnees['id_prestataire'];
		$this->id_prestataire_profil = $donnees['id_prestataire_profil'];
                $this->id_type               = $donnees['id_type'];
		
		$this->civilite              = array(
						   'id_civilite'=>$donnees['id_civilite'],
						   'civilite'=>$donnees['adm_civilite']
						  );
		$this->nom                   = $donnees['pf_nom'];
                $this->prenom                = $donnees['pf_prenom'];
		
		$this->update                = $donnees['modifie_le'];//Mise � jour du profil
		
		
		$this->structure       	     = array(
                                                   'id_structure' =>$donnees["id_structure"],
                                                   'structure'    =>$donnees["adm_structure"]
                                                );
		
		$this->entreprise            = $donnees['pr_entreprise'];
		$this->presentation          = $donnees['pr_presentation'];
	
		$this->standard              = array(
                                                   'indicatif' =>$donnees["pr_standard_indicatif"],
                                                   'numero'    =>$donnees["pr_standard"]
                                                );
                $this->adresse_livraison     = array(
                                                   'rue'       =>$donnees["pr_add_liv_rue"],
                                                   'codepostal'=>$donnees["pr_add_liv_codepostal"],
                                                   'ville'     =>$donnees["pr_add_liv_ville"],
                                                   'pays'      =>$donnees["pr_add_liv_pays"]
                                                );
                $this->adresse_facturation   = array(
                                                   'rue'       =>$donnees["pr_add_fac_rue"],
                                                   'codepostal'=>$donnees["pr_add_fac_codepostal"],
                                                   'ville'     =>$donnees["pr_add_fac_ville"],
                                                   'pays'      =>$donnees["pr_add_fac_pays"]
                                                );
		
		$this->notification_mail     = $donnees['pr_notification_mail'];
		
		$this->pourcentage           = $donnees["pf_pourcentage"];
		
		$this->pourcentage_detail    = array(
						     'experiences' =>$donnees['pf_pourcentage_experience'],
						     'formations'  =>$donnees['pf_pourcentage_formation'],
						     'identite'    =>$donnees['pf_pourcentage_identite'],
						     'competences' =>$donnees['pf_pourcentage_competence']
						);
		
		$this->email                 = $donnees['usr_email'];
		
		$this->jabber                = array(
                                                   'loggin'   =>$donnees["jbr_loggin"],
                                                   'password' =>$donnees["jbr_password"]
                                                );
  		
		$this->setAbonnement();//On va setter notre varaible par le biais d'une fonction pour eviter laisse le code respirer
		
		
	}
	
	public function __get($name){
		echo "<span class=\"error\">R�cup�ration de '$name' impossible dans  Prestataire.class.php</span>";
	}
	
	public function __call($name, $arguments){
		echo "<span class=\"error\">Appel de la m�thode '$name' inexistante avec les argument suivants Prestataire.class.php</span>"
			. implode(', ', $arguments). "\n";
	}

	public static function __callStatic($name, $arguments){
		echo "<span class=\"error\">Appel de la m�thode statique '$name' inexistante avec les argument suivants Prestataire.class.php</span>"
			. implode(', ', $arguments). "\n";
        }
	
       
	private function setAbonnement(){
		$DBmembre = new DBMembre();// en chargeant cette classe je charge aussi un singleton $id_type
		
		//SI on a configur� dans les define l'autorisation des abonnement gratuit
		//le site passe en mode gratuit pour tous les client enjoy!
		if(DBMembre::$id_type ==  DBMembre::TYPE_PRESTATAIRE)
			$this->abonnnement = true; // je suis prestataire donc RAS.
			
		elseif(DBMembre::$id_type ==  DBMembre::TYPE_CLIENT){
			$this->abonnnement = Client::$abonnement;
		}
		
		elseif(DBMembre::$id_type ==  DBMembre::TYPE_ADMINISTRATEUR){
			$this->abonnnement = true;
		}
		
		else
			$this->abonnnement = false; // je suis Hors Ligne.
	}
       
        public function getLocalisation(){
		return $this->localisation;
	}
        
	public function getCivilite(){
		
		return (object) $this->civilite;
	} 
	
	public function getId_prestataire(){
		return $this->id_prestataire;
	} 
        
	public function getId_profil(){
		return $this->id_prestataire_profil;
	}
	
	
	public function getId_identifiant(){
		return $this->id_identifiant;
	}
	
        public function getId_type(){
		return $this->id_type;
	} 
        
	public function getNom(){
		if($this->abonnnement)
			return $this->nom;
		else{
			return substr($this->nom, 0,1).'XXXXX';
		}
	} 
        
        public function getPrenom(){
		
		return $this->prenom;
	}
        
	public function getUpdate(){
		
		return Tools::shortConvertDBDateTimeToLan($this->update);
	}
	
        final public function getStructure(){
		return (object) $this->structure;
	}
        	
	public function getEntreprise(){
		return $this->entreprise;
	}
	

	
	public function getPresentation(){
		
		if(isset($this->presentation) && !empty($this->presentation))
			return stripcslashes($this->presentation);
		else
			return '-';
	}
	
	public function getStandard($string=false){
            if(!$this->abonnnement)
		return 'XXXXXXXXX';
	
            $numero = $this->standard;
	    
            if($string)
                return $numero["indicatif"]." - ".$numero["numero"];
            else
                return (object) $numero;
	}

	public function getAdresseFacturation($string=true){
		
		$adresse  = $this->adresse_facturation;

		if($string)
                    return $adresse["rue"]." ".$adresse["codepostal"]." ".$adresse["ville"]." - ".$adresse["pays"];
                else
                    return (object) $adresse;
	}	
	
        public function getAdresseLivraison($string=true){
		
		$adresse  = $this->adresse_livraison;

		if($string)
                    return $adresse["rue"]." ".$adresse["codepostal"]." ".$adresse["ville"]." - ".$adresse["pays"];
                else
                    return (object)  $adresse;
	}
        
	public function getNotificationEmail(){
            return $this->notification_mail;
        }
	
	public function getPourcentage(){
            return $this->pourcentage;
        }
	
	public function getPourcentageDetail(){
            return (object) $this->pourcentage_detail;
        }
	
	public function getEmail(){
		return $this->email;
	}
	
	public function getJabber(){
		return (object) $this->jabber;
	}
	
	//Pour acceder � cette methode
        //$prestataire = new $prestataire();
        //$avatar = $prestataire->getAvatar();
	public function getAvatar(){
		$DBfile = new DBFile();
		$avatar = $DBfile->getListFiles(array('id_identifiant'=>$this->id_identifiant, 'fichier_type'=>array(DBFile::TYPE_AVATAR)));

		if($avatar['count'])
			return $avatar['files'][0];
		else
			return new File(array('fichier_type'=>DBFile::TYPE_AVATAR));
			
		
	}
	//Pour acceder � cette methode
        //$prestataire = new $prestataire();
        //$prestataire = $prestataire->getCV();
	public function getCv(){
		$DBfile = new DBFile();
		$cv = $DBfile->getListFiles(array('id_prestataire_profil'=>$this->id_prestataire_profil, 'fichier_type'=>array(DBFile::TYPE_CV)));

		if($cv['count']){
			return $cv['files'][0];
		}
		else{
			return new File(array('fichier_type'=>DBFile::TYPE_CV));
		}
	}
	//Pour acceder � cette methode
        //$prestataire = new $prestataire();
        //$prestataire = $prestataire->getPlaquette();
	public function getPlaquette(){
		$DBfile = new DBFile();
		$plaquette = $DBfile->getListFiles(array('id_identifiant'=>$this->id_identifiant, 'fichier_type'=>array(DBFile::TYPE_PLAQUETTE)));

		if($plaquette['count']){
			return $plaquette['files'][0];
		}
		else{
			return new File(array('fichier_type'=>DBFile::TYPE_PLAQUETTE));
		}
	}
        //Cette fonction permet de r�cuperer les profil associ�s au prestataire en cours
	//Elle est separ�e pour �viter de surcharger inutilement les requetes SQL
	public function getProfils($agency = false){
		$profils = new DBMembre();
		//Le filtre agency permet de recuperer le profil principal dans le cadre d'un intermittent ou d'un freelance
		//Sinon si c'est une agence on recuper UNIQUEMENT ses profils et non pas le profil principal
		$filtre = $agency ? array('id_identifiant'=>$this->id_identifiant, 'id_role'=>DBMembre::TYPE_NORMAL) : array('id_identifiant'=>$this->id_identifiant);
		return (object) $profils->getListProfils($filtre, array(), false);
	}
	//Cette fonction permet de r�cuperer les reponses associ�s au prestataire en cours
	//Elle est separ�e pour �viter de surcharger inutilement les requetes SQL
	public function getReponses($limit=false, $justCount=false){
		$reponses = new DBReponse();
		return (object) $reponses->getListReponses(array('id_identifiant'=>$this->id_identifiant), array(), $limit, $jusCount);
	}
	//Cette fonction permet de r�cuperer les favoris mission
	//Elle est separ�e pour �viter de surcharger inutilement les requetes SQL
	public function getFavoris(){
		$favoris = new DBMembre();
		$listFavoris = $favoris->getListFavorisMissions($this->id_identifiant); 
	
		return (object) $listFavoris;
	}
	//Pour acceder � cette methode
        //$prestataire = new $prestataire();
        //$prestataire = $prestataire->getRealisations();
	//ATTENTION a la subtilite la methode est surcharg�e sur la Class profil
	//ATTENTION depuis cette classe on recupere TOUTES les r�alisation des comptes prestataire (id�al pour les agences)
	public function getRealisations(){
		$DBfile = new DBFile();
		$listRealisations = $DBfile->getListFiles(array('id_identifiant'=>$this->id_identifiant, 'fichier_type'=>array(DBFile::TYPE_TELECHARGEABLE, DBFile::TYPE_CONSULTABLE, DBFile::TYPE_VISUALISABLE)));
		return (object) $listRealisations;
	}
        


}
