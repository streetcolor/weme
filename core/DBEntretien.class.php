<?php
class DBEntretien extends DBConnect{
    
    const SESSION_EN_ATTENTE    = 1;
    const SESSION_CONFIRME      = 2;
    const SESSION_TERMINEE      = 3;
    const SESSION_DECLINEE      = 4;
    
    //TRES IMPORTANT
    //Les méthodes save doivent être privées et son uniquement appelées via les méthodes make
    //Les tests de saisie s'effectuent OBLIGATOIREMENT depuis les méthodes make
    
    private function saveVideoSession($session=array(), $ignore=array(), $debug=false){
        $req = "INSERT INTO usr_videosession  SET modifie_le = NOW() ";
        if(count($session)){
            foreach ($session as $champ=>$valeur){
                $req .= ", ".$champ." = ".$this->db->quote($valeur, PDO::PARAM_STR);
            }
        }
        
        if($debug)
                throw new myException($req);
        if(DEBUG)
                parent::$request[] =  array('color'=>'aqua', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);
                
        if($this->db->exec($req)){
            return $id_session = $this->db->lastInsertId();
        }
    }
    
    public function makeVideoSession($array=array()){
        
        //I. ON crée le tableau qui va servir à l'insertion dans la base de donnée
	$new_session = array('id_client'=>$array['id_client'],
			     'id_identifiant' => DBMembre::$id_identifiant,
                             'vs_id_mission' => $array['vs_id_mission'],
			     'id_prestataire_profil'=>$array['id_prestataire_profil'],
			     'vs_hostaccess' => Tools::randomString(),
			     'vs_horraires' => Tools::shortConvertLanDateToDb($array['vs_date'])." ".$array['hour'].":".$array['minute'].":00",
			     'vs_statut' => $this::SESSION_EN_ATTENTE
			     );//Tools::debugVar($array, false);
        
	
        // II. On effectue les traitements de formulaire avant de sauvegarder dans la base de donnée
        if(!isset($new_session['id_client']) || empty($new_session['id_client']))
            $error['id_client'] = XMLEngine::$trad->global_trad->notifChampsObligatoire;
            
        if(!isset($new_session['vs_id_mission']) || empty($new_session['vs_id_mission']))
            $error['vs_id_mission'] = XMLEngine::$trad->global_trad->notifChampsObligatoire;
            
        if(!isset($new_session['id_identifiant']) || empty($new_session['id_identifiant']))
            $error['id_identifiant'] = XMLEngine::$trad->global_trad->notifChampsObligatoire;
            
        if(!isset($new_session['id_prestataire_profil']) || empty($new_session['id_prestataire_profil']))
            $error['id_prestataire_profil'] = XMLEngine::$trad->global_trad->notifChampsObligatoire;
            
        if(!isset($new_session['vs_hostaccess']) || empty($new_session['vs_hostaccess']))
            $error['vs_hostaccess'] = XMLEngine::$trad->global_trad->notifChampsObligatoire;
            
        if(!Tools::verifDate($array['vs_date']))
            $error['vs_horraires'] = XMLEngine::$trad->global_trad->invalide_date;
            
        if(!isset($new_session['vs_statut']) || empty($new_session['vs_statut']))
            $error['vs_horraires'] = XMLEngine::$trad->global_trad->notifChampsObligatoire;

        if(count($error))
	    return $error;
                 
        // III. On enregistre dans la base de donnée
        if($id_session = $this->saveVideoSession($new_session)){
            // IV. On fait les traitements adequats envoie de mails et generation des messages de retour
	    //ON récupère le guest
	    $DBmembre = new DBMembre();
	    $profil = $DBmembre->getListProfils(array('id_prestataire_profil'=>$array['id_prestataire_profil']));//Tools::debugVar($profil, false);
	   $email = $profil['profils'][0]->getEmail();
	    $message["sujet"]   = XMLEngine::$trad->global_trad->mailPrestaPropositionEntretienSujet;
	    $message["email"]   = $email;
	    
	    $string = XMLEngine::$trad->global_trad->mailPrestaPropositionEntretien;
	    $patterns = array();
	    $patterns[0] = '/{{nom}}/';
	    $patterns[1] = '/{{prenom}}/';
	    $patterns[2] = '/{{appname}}/';
	    $replacements = array();
	    $replacements[0] = $profil['profils'][0]->getNom();
	    $replacements[1] = $profil['profils'][0]->getPrenom();
	    $replacements[2] = APPLI_NAME;
		
	    $message['message'] = preg_replace($patterns, $replacements, $string);
	    Tools::sendEmail($message);
	}
				
        return $id_session;
        
    }
    
    public function getListEntretiens($filtre=array()){
	$entretiens = array();
	$req_prefix = array(
			'COUNT' => 'SELECT count(DISTINCT(UVS.id_videosession)) AS total, ' ,
			'SELECT'=> 'SELECT DISTINCT(UVS.id_videosession), '
			    );
	$req = 'UI.usr_email, UVS.id_client, UVS.id_identifiant, UVS.vs_id_mission, UVS.id_prestataire_profil, UVS.vs_hostaccess, UVS.vs_guestaccess, UVS.vs_horraires, UVS.vs_statut, UVS.vs_host_commentaire, 
		UCM.mg_nom, UCM.mg_prenom, UC.cl_entreprise,  /* infos manager */
		UPP.pf_nom, UPP.pf_prenom, UPP.pf_cv_titre, UPP.pf_portable, UPP.pf_fixe,  /* infos profil */
		UM.mi_titre, UM.mi_description,  /* infos de la mission */
		AES.adm_entretien_statut_'.$_SESSION["langue"].' AS entretien_statut /* infos sur le statut */
		FROM usr_videosession AS UVS
		INNER JOIN usr_clients_managers AS UCM 
		ON UVS.id_identifiant = UCM.id_identifiant 
		INNER JOIN usr_clients AS UC 
		ON UC.id_client = UCM.id_client
		INNER JOIN usr_prestataires_profils AS UPP
		ON UVS.id_prestataire_profil = UPP.id_prestataire_profil 
		INNER JOIN usr_missions AS UM
		ON UVS.vs_id_mission = UM.id_mission
		INNER JOIN adm_entretiens_statuts AS AES
		ON UVS.vs_statut = AES.id_entretien_statut
		INNER JOIN usr_identifiants AS UI
		ON UCM.id_identifiant = UI.id_identifiant 
		WHERE UVS.modifie_le ';
	     
	if(count($filtre)){
	    foreach($filtre as $champ => $valeur){
		if(is_array($valeur)){
			if(count($valeur))
				$req .= " AND UVS.".$champ." IN ('".implode("','", $valeur)."')";
			else
				$req .= " AND UVS.".$champ." IN ('')";	
		}
		else
			$req .= " AND UVS.".$champ." = ".$this->db->quote($valeur, PDO::PARAM_STR);
	    }
	}
	
	$req .= " ORDER BY UVS.cree_le DESC  ";
	
	if($debug)
		throw new myException($req_prefix['COUNT'].$req.$limit['COUNT']);
	if(DEBUG)
		parent::$request[] =  array('color'=>'aqua', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req_prefix['COUNT'].$req);	
	    
		
	//Le count avec la requete
	$resultats = $this->db->query($req_prefix['COUNT'].$req);
	
	//si on un total superieur a zéro
	//on reexecute la requete afin de recuperer les elements
	if($count = $resultats->fetch(PDO::FETCH_OBJ)->total){
		
		if($debug)
			throw new myException($counter['SELECT'].$req.$limit['SELECT']);
		if(DEBUG)
			parent::$request[] =  array('color'=>'aqua', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req_prefix['SELECT'].$req.$limit['SELECT']);
					
		$resultats = $this->db->query($req_prefix['SELECT'].$req);
		
		while ($entretien = $resultats->fetch(PDO::FETCH_ASSOC)){
		    $entretiens[] = new Entretien($entretien);
		}
		
		$resultats->closeCursor();
	}
	return array('count'=>$count, 'entretiens'=>$entretiens);
    }
    
    public function updSessionVideo($upd_tab=array(), $where_tab=array(), $debug=false){
	$req = "UPDATE usr_videosession SET ";
	
	if(count($upd_tab)){
	    foreach ($upd_tab as $champ => $valeur){
		$req .= $champ." = ".$this->db->quote($valeur, PDO::PARAM_STR).', ';
	    }
	}
	$req .= " modifie_le = NOW() WHERE ";
	
	if(count($where_tab)){
	    foreach ($where_tab as $champ => $valeur){
		if(is_array($valeur))
		    $req .= $champ." IN ".implode(",", $valeur)." AND ";
		else
		    $req .= $champ." = ".$this->db->quote($valeur, PDO::PARAM_STR)." AND ";
	    }
	}else{
	    throw new myException("need $ where_tab to perform query");
	}
	
	$req.= "1 = 1 ";
	
	if($debug)
	    throw new myException($req);
	if(DEBUG)
	    parent::$request[] =  array('color'=>'aqua', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);
	    
	if($this->db->exec($req))
	    return $where_tab['id_videosession'];
    }
    
    private function updVideoSession($array=array(),$ignore=array('page', 'action', 'crypt', 'id_prestataire_profil'), $debug=false){
		
		$req = " UPDATE usr_videosession SET ";
		
		foreach($array as $key=>$value){
			if(!in_array($key, $ignore))
				$req .= $key." = ".$this->db->quote($value, PDO::PARAM_STR)." , ";
		}		
		
		$req .= " modifie_le = NOW()  WHERE id_videosession = ".$this->db->quote($array["id_videosession"], PDO::PARAM_STR);
			
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
		
		if($this->db->exec($req)){
			
			return $array["id_videosession"];
	
		}
		
	}
    
    public function generateEntretienXML($id_video_session){
	//Tools::debugVar($session, false);
	
	$session = $this->getListEntretiens(array('id_videosession'=>$id_video_session));
	$session = $session['entretiens'][0];
	
	$xml = new DOMDocument();
	$xml->encoding = 'UTF-8';
	//On le formate
	$xml->formatOutput = true;
	
	$xml_name = "entretien_".$session->getId_videosession()."-".Tools::shortConvertLanDateToDb($session->getSessionTime()->date);
	//Tools::debugVar($xml_name);
    
	$sess = $xml->createElement("session");
	$sess_id = $xml->createAttribute('id');
	$sess_id->value = $xml_name;
	$sess->appendChild($sess_id);
	$xml->appendChild($sess);
	
	//le noeud host
	$host_node = $xml->createElement("host");
	//les attributs
	$host_node_nom = $xml->createAttribute("nom");
	$host_node_prenom = $xml->createAttribute("prenom");
	$host_node_access = $xml->createAttribute("access");
	$host_node_nom->value = $session->getSessionHost()->host_nom;
	$host_node_prenom->value = $session->getSessionHost()->host_prenom;
	$host_node_access->value = $session->getSessionHost()->host_access;
	
	$host_node->appendChild($host_node_nom);
	$host_node->appendChild($host_node_prenom);
	$host_node->appendChild($host_node_access);
	
	$sess->appendChild($host_node);
	
	//Le noeud guest
	$guest_node = $xml->createElement('guest');
	//Les attributs
	$guest_node_nom = $xml->createAttribute('nom');
	$guest_node_prenom = $xml->createAttribute('prenom');
	$guest_node_access = $xml->createAttribute('access');
	$guest_node_nom->value = $session->getSessionGuest()->guest_nom;
	$guest_node_prenom->value = $session->getSessionGuest()->guest_prenom;
	$guest_node_access->value = $session->getSessionGuest()->guest_access;
	
	$guest_node->appendChild($guest_node_nom);
	$guest_node->appendChild($guest_node_prenom);
	$guest_node->appendChild($guest_node_access);
	
	$sess->appendChild($guest_node);
	
	//Le noeud session info
	$infos_node = $xml->createElement("infos");
	//Les attributs
	$infos_node_date = $xml->createAttribute("date");
	$infos_node_time = $xml->createAttribute("time");
	$infos_node_date->value = $session->getSessionTime()->date;
	$infos_node_time->value = $session->getSessionTime()->time;
	
	$infos_node->appendChild($infos_node_date);
	$infos_node->appendChild($infos_node_time);
	
	$sess->appendChild($infos_node);
	
	//ON sauvegarde le xml sous le format entretien_id-YYYYmmdd.xml
	
	$file_adresse = _SWF_ . 'entretiens/com/weme/data/' .$xml_name.".xml";//Tools::debugVar($file_adresse);
	if($xml->save($file_adresse))
	    return  $session;
	
	return false;
    }
    
    //Methode appelée pour modifier le CV d'un profil
    public function editEntretien($entretien=array()){
	$error = array();
	// I. On effectue les traitements de formulaire avant de sauvegarder dans la base de donnée
	if(DBMembre::$id_type==DBMembre::TYPE_CLIENT){
	    if(!isset($entretien["id_videosession"]) || empty($entretien["id_videosession"]))
		    $error['id_videosession'] = "Une session doit être définie";
	}
	
	elseif(DBMembre::$id_type==DBMembre::TYPE_PRESTATAIRE){
	    if(!isset($entretien["vs_statut"]) || empty($entretien["vs_statut"]))
		    $error['vs_statut'] = "Auncun statut de défini";
	    
	    if(!isset($entretien["id_videosession"]) || empty($entretien["id_videosession"]))
		    $error['id_videosession'] = "Une session doit être définie";
	    
	    if(!isset($entretien["vs_guestaccess"]) || empty($entretien["vs_guestaccess"]))
		    $error['vs_guestaccess'] = "Une reference invité doit etre définie";
	}	
	
	
	if(count($error))
		return $error;
	
	 
	// II. On enregistre notre profil dans la base de donnée
	//Ajout, edition et suppresion des formations
	if($id_video_session = $this->updVideoSession($entretien)){
	    
	    if($entretien["vs_statut"] == self::SESSION_CONFIRME){
		if($entretien= $this->generateEntretienXML($id_video_session)){
		    $DBmessage = new DBMessage();
		    $message['id_identifiant']   = true;
		    $message['mail_author']      = MAIL_REPLY_TO;
		    $message['usr_email']        = array($entretien->getSessionHost()->usr_email);
		    $message['mail_titre']       = $entretien->getSessionGuest()->guest_nom.' '.$entretien->getSessionGuest()->guest_prenom.' a accepté votre invitation';
		    $message['mail_message']     = $entretien->getSessionHost()->host_nom.' '.$entretien->getSessionHost()->host_prenom.',
							<br><br> '.$entretien->getSessionGuest()->guest_nom.' '.$entretien->getSessionGuest()->guest_prenom.' a accepté votre invitation
							<br><br>Pour commencer cet entretien , connectez-vous à votre compte et saisissez le code  '.$entretien->getSessionHost()->host_access;
		    
		    return $DBmessage->makeMail($message);

		}
	    }
		
	    return $id_video_session;
	}
    }
    
}