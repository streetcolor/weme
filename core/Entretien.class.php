<?php
class Entretien {
    protected $id_videosession;
    protected $vs_statut = array();
    
    protected $vs_host = array();
    
    protected $vs_guest = array();
    
    protected $vs_mission = array();
    
    protected $vs_horraires;
   
    protected $vs_host_commentaire;
    
    
    
    public function __construct ($donn�es=array()){
        $this->id_videosession = $donn�es["id_videosession"];
        
        $this->vs_statut = array(
                                 'id_statut' => $donn�es["vs_statut"],
                                 'entretien_statut' => $donn�es["entretien_statut"]
                                 );
        
        $this->vs_host = array(
                                'id_client' => $donn�es["id_client"],
                                'usr_email' => $donn�es["usr_email"],
                                'id_identifiant' => $donn�es["id_identifiant"],
                                'host_nom' => $donn�es["mg_nom"],
                                'host_prenom' => $donn�es["mg_prenom"],
                                'host_entreprise' => $donn�es["cl_entreprise"],
                                'host_access' => $donn�es["vs_hostaccess"]
                            );
        
        $this->vs_guest = array(
                                'id_prestataire_profil' => $donn�es["id_prestataire_profil"],
                                'guest_nom' => $donn�es["pf_nom"],
                                'guest_prenom' => $donn�es["pf_prenom"],
                                'guest_cv_titre' => $donn�es["pf_cv_titre"],
                                'guest_fixe' => $donn�es["pf_fixe"],
                                'guest_portable' => $donn�es["pf_portable"],
                                'guest_access' => $donn�es["vs_guestaccess"]
                            );
        
        $this->vs_mission = array(
                                'id_mission' => $donn�es["vs_id_mission"],
                                'mi_titre' => $donn�es["mi_titre"],
                                'mi_description' => $donn�es["mi_description"]
                            );
        
        
        $this->vs_horraires = $donn�es["vs_horraires"];
        
        $this->vs_host_commentaire = $donn�es["vs_host_commentaire"];
        
        
        
    }
    
    public function __get($name){
            echo "<span class=\"error\">R�cup�ration de '$name' impossible dans  Entretien.class.php</span>";
    }
    
    public function __call($name, $arguments){
            echo "<span class=\"error\">Appel de la m�thode '$name' inexistante avec les argument suivants Entretien.class.php</span>"
                    . implode(', ', $arguments). "\n";
    }

    public static function __callStatic($name, $arguments){
            echo "<span class=\"error\">Appel de la m�thode statique '$name' inexistante avec les argument suivants Entretien.class.php</span>"
                    . implode(', ', $arguments). "\n";
    }
    
    public function getId_videosession(){
        return $this->id_videosession;
    }
    public function getSessionHost(){
        return (object) $this->vs_host;
    }
    
    public function getSessionGuest(){
        return (object) $this->vs_guest;
    }
    
    public function getSessionMission(){
        return (object) $this->vs_mission;
    }
    
    public function getSessionTime(){
        
        $date_time = explode(" ",$this->vs_horraires);
        $date = Tools::shortConvertDBDateToLan($date_time[0]);
        $time = Tools::convertDBTimeToLan($date_time[1]);
        
        return (object) array("date"=>$date, "time"=>$time, "dbdate"=>$date_time[0]);
    }
    public function getSessionStatut(){
        return (object) $this->vs_statut;
    }
    public function getSessionHostComm(){
        return $this->vs_host_commentaire;
    }
}