<?PHP
class Reponse extends Profil {

        protected $id_reponse;
	protected $id_mission; 
        protected $id_profil;
        protected $statut = array(); //int : Accept�, D�clin�, En attente, en cours
        
        protected $id_pj;
        protected $id_file;
        
        protected $reponse;
        protected $commentaire;
        protected $archiver;//Date
        protected $tarif = array();//tarif, id_monnaie, id_contrat
	protected $date;//Date de la proposition
        
	
	protected $pj = array();//Uniquement les pj associ�es aux reponses

	
	protected $notification_reponse_email; //Boolean
	
	//On recupere quelques infos sur la mission pour eviter des surcharges inutiles
	protected $titre_mission; //titre de la missions
	protected $client_mission; //client de la missions
        
        
	public function __construct( $donnees=array()){
                $this->id_reponse          = $donnees['id_mission_reponse'];		
                $this->id_mission          = $donnees['id_mission'];
		
		$this->titre_mission       = $donnees['mi_titre'];
		$this->client_mission       = $donnees['cl_entreprise'];
		
                $this->id_profil           = $donnees['id_prestataire_profil'];
                $this->statut              = array(
						   'id_statut' => $donnees['id_reponse_statut'],
						   'statut'    =>$donnees['adm_statut']
						);
                
		$this->date                = $donnees['cree_le'];
                $this->id_pj               = $donnees['id_pj'];
                $this->id_file             = explode(',', $donnees['id_file']);
		$this->commentaire         = $donnees['rp_commentaire'];
                $this->reponse             = $donnees['rp_reponse'];
                $this->archiver            = $donnees['rp_archiver'];
		$this->tarif               = array(
							'tarif'=>$donnees['rp_tarif'],
							'monnaie'=>$donnees['adm_monnaie'],
							'contrat'=>$donnees['adm_contrat'],
							'id_monnaie'=>$donnees['id_monnaie'],
							'id_contrat'=>$donnees['id_contrat']
						);
		
		$this->pj                  = array(	'id_file'          =>$donnees['id_pj'],
							'id_identifiant'   =>$donnees['id_identifiant_prestataire'], //petit exception venant de DBReponse id_identifiant devient id_identifiant_prestataire
							'fichier_reference'=>$donnees['fichier_reference'],
							'fichier_nom'      =>$donnees['fichier_nom'],
							'fichier_path'     =>$donnees['fichier_path'],
							'fichier_extension'=>$donnees['fichier_extension'],
							'fichier_type'     =>DBFile::TYPE_PJ
						);
		
		$this->notification_reponse_email  = $donnees['rp_reponse_mail'];
		
                //Permet de remplir certaines infos presente la classe profil
		parent::__construct($donnees);
		
		$this->abonnnement = true;//On est dans la reponse on force la variable abonnement �  true pour afficher les contenu

	}
	
	public function __get($name){
		echo "<span class=\"error\">R�cup�ration de '$name' impossible dans  Reponse.class.php</span>";
	}
	
	public function __call($name, $arguments){
		echo "<span class=\"error\">Appel de la m�thode '$name' inexistante avec les argument suivants Reponse.class.php</span>"
			. implode(', ', $arguments). "\n";
	}

	public static function __callStatic($name, $arguments){
		echo "<span class=\"error\">Appel de la m�thode statique '$name' inexistante avec les argument suivants Reponse.class.php</span>"
			. implode(', ', $arguments). "\n";
        }
	
        public function getId_reponse(){
		return $this->id_reponse;
	}
        
	public function getId_mission(){
		return $this->id_mission;
	}
	
	public function getStatut(){
            return (object) $this->statut;
        }
        
	public function getId_profil(){
		return $this->id_profil;
	}
	
	public function getTitre(){
		return $this->titre_mission;
	}
	
	public function getClient(){
		return $this->client_mission;
	}
	
	public function getReponse(){
		
		return $this->reponse;
	}
	
	public function getCommentaire(){
		return $this->commentaire;
	}
	

	public function getTarif($string=false){
            
            $tarif = $this->tarif;
	   
            if($string)
                return $tarif["tarif"]." ".$tarif["monnaie"]. " ".$tarif["contrat"];
            else{
		
                return (object)$tarif;
            }
	}
	
	public function getDate(){
		
		return Tools::shortConvertDBDateToLan($this->date);

	}
	
	public function getArchiver(){
		if($this->archiver!='0000-00-00 00:00:00')
			return Tools::shortConvertDBDateToLan($this->archiver);
		else
			return false;
	}
	
	public function getNotificationEmail(){
            return $this->notification_reponse_email;
        }
	
        //Pour acceder � cette methode
        //$reponse = new Reponse();
        //$pieceJointe = $reponse->getPieceJointe();
        public function getPieceJointe(){

		if($this->id_pj){
			
			return new File($this->pj);
		}
		else
			return new File(array('fichier_type'=>DBFile::TYPE_PJ));
			
		
	}
        
        //Pour acceder � cette methode
        //$reponse = new Reponse();
        //$pieceJointe = $reponse->getRealisations();
	public function getRealisations(){
		$DBfile = new DBFile();
		$listRealisations = $DBfile->getListFiles(array('id_file'=>$this->id_file, 'fichier_type'=>array(DBFile::TYPE_TELECHARGEABLE, DBFile::TYPE_CONSULTABLE, DBFile::TYPE_VISUALISABLE)));
		return (object)  $listRealisations;
	}
        
	
        //Pour acceder � cette methode
        //$reponse = new Reponse();
        //$profil = $reponse->getProfil()->getNom();
        public function getProfils(){
            $DBmembre = new DBMembre();
	    $profils = explode(',' , $this->id_profil);
	    //Comme je suis dans la reponse je set le forcage de l'abonnement a true
	    //Ainsi on on acces au coordonn� de nos contacts 
            $profil = $DBmembre->getListProfils(array('id_prestataire_profil'=>$profils), $searchFiltre=array(), $limit=false, $matching=false, $forceAbonnement=$this->abonnnement);
            return (object) $profil['profils'];
        }
	
	 //Pour acceder � cette methode
        //$reponse = new Reponse();
        //$profil = $reponse->getProfil()->getNom();
        public function getMission(){
            
            $DBmission = new DBMission();
            $mission = $DBmission->getListMissions(array('id_mission'=>$this->id_mission));
           
	    return (object) $mission['missions'][0];
        }
        

}
