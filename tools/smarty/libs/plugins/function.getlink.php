<?php
/**
 * Smarty plugin
 *
 * This plugin is only for Smarty2 BC
 * @package Smarty
 * @subpackage PluginsFunction
 */

/**
 * Smarty {math} function plugin
 *
 * Type:     function<br>
 * Name:     math<br>
 * Purpose:  handle math computations in template
 *
 * @link http://www.smarty.net/manual/en/language.function.math.php {math}
 *          (Smarty online manual)
 * @author   Monte Ohrt <monte at ohrt dot com>
 * @param array                    $params   parameters
 * @param Smarty_Internal_Template $template template object
 * @return string|null
 */
function smarty_function_getlink($params)
{
    if(isset($params['action'])){
        $action = '&action='.$params['action'];
    }
    
    if(!$params['page']){
        if(!$_REQUEST['page']){
             $params['page'] = 'home';
        }
        else{
            $params['page'] = $_REQUEST['page'];
        }
    }
 
    if($params['crypt']){
        if(isset($params['params']))
            $crypt="&crypt=".Tools::encrypt($params['params']);
    }
    else{
         if(isset($params['params']))
            $crypt = '&'.$params['params'];
    }
  
    return  '?page='.$params['page'].$action.$crypt;
}

?>