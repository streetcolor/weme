
/*

Jappix - An open social platform
These are the temporary/persistent data store functions

-------------------------------------------------

License: dual-licensed under AGPL and MPLv2
Authors: Valérian Saliou, Maranda
Last revision: 20/07/13

*/

// Common: storage adapter
function storageAdapter(storage_native, storage_emulated) {
	var legacy = !storage_native;

	this.key = function(key) {
		if(legacy) {
			if(key >= this.length)
				return null;

			var c = 0;

			for(name in storage_emulated) {
				if(c++ == key)  return name;
			}

			return null;
		}

		return storage_native.key(key);
	};

	this.getItem = function(key) {
		if(legacy) {
			if(storage_emulated[key] !== undefined)
				return storage_emulated[key];

			return null;
		} else {
			return storage_native.getItem(key);
		}
	};

	this.setItem = function(key, data) {
		if(legacy) {
			if(!(key in storage_emulated))
				this.length++;

			storage_emulated[key] = (data + '');
		} else {
			storage_native.setItem(key, data);
			this.length = storage_native.length;
		}
	};

	this.removeItem = function(key) {
		if(legacy) {
			if(key in storage_emulated) {
				this.length--;
				delete storage_emulated[key];
			}
		} else {
			storage_native.removeItem(key);
			this.length = storage_native.length;
		}
	};

	this.clear = function() {
		if(legacy) {
			this.length = 0;
			storage_emulated = {};
		} else {
			storage_native.clear();
			this.length = storage_native.length;
		}
	};

	this.length = legacy ? 0 : storage_native.length;
}


// Temporary: sessionStorage emulation
var DATASTORE_DB_EMULATED = {};

// Temporary: sessionStorage class alias for direct access
var storageDB = new storageAdapter(
	(window.sessionStorage ? sessionStorage : null),
	DATASTORE_DB_EMULATED
);

// Temporary: returns whether it is available or not
function hasDB() {
	// Try to write something
	try {
		storageDB.setItem('hasdb_check', 'ok');
		storageDB.removeItem('hasdb_check');

		return true;
	}
	
	// Not available?
	catch(e) {
		return false;
	}
}

// Temporary: used to read a database entry
function getDB(type, id) {
	try {
		return storageDB.getItem(type + '_' + id);
	}
	
	catch(e) {
		logThis('Error while getting a temporary database entry (' + type + ' -> ' + id + '): ' + e, 1);
	}

	return null;
}

// Temporary: used to update a database entry
function setDB(type, id, value) {
	try {
		storageDB.setItem(type + '_' + id, value);

		return true;
	}
	
	catch(e) {
		logThis('Error while writing a temporary database entry (' + type + ' -> ' + id + '): ' + e, 1);
	}

	return false;
}

// Temporary: used to remove a database entry
function removeDB(type, id) {
	try {
		storageDB.removeItem(type + '_' + id);
		
		return true;
	}
	
	catch(e) {
		logThis('Error while removing a temporary database entry (' + type + ' -> ' + id + '): ' + e, 1);
	}

	return false;
}

// Temporary: used to check a database entry exists
function existDB(type, id) {
	return getDB(type, id) != null;
}

// Temporary: used to clear all the database
function resetDB() {
	try {
		storageDB.clear();
		
		logThis('Temporary database cleared.', 3);
		
		return true;
	}
	
	catch(e) {
		logThis('Error while clearing temporary database: ' + e, 1);
		
		return false;
	}
}


// Persistent: localStorage emulation
var DATASTORE_PERSISTENT_EMULATED = {};

// Persistent: localStorage class alias for direct access
var storagePersistent = new storageAdapter(
	(window.localStorage ? localStorage : null),
	DATASTORE_PERSISTENT_EMULATED
);

// Persistent: returns whether it is available or not
function hasPersistent() {
	// Try to write something
	try {
		storagePersistent.setItem('haspersistent_check', 'ok');
		storagePersistent.removeItem('haspersistent_check');
		
		return true;
	}
	
	// Not available?
	catch(e) {
		return false;
	}
}

// Persistent: used to read a database entry
function getPersistent(dbID, type, id) {
	try {
		return storagePersistent.getItem(dbID + '_' + type + '_' + id);
	}
	
	catch(e) {
		logThis('Error while getting a persistent database entry (' + dbID + ' -> ' + type + ' -> ' + id + '): ' + e, 1);
		
		return null;
	}
}

// Persistent: used to update a database entry
function setPersistent(dbID, type, id, value) {
	try {
		storagePersistent.setItem(dbID + '_' + type + '_' + id, value);
		
		return true;
	}
	
	// Database might be full
	catch(e) {
		logThis('Retrying: could not write a persistent database entry (' + dbID + ' -> ' + type + ' -> ' + id + '): ' + e, 2);
		
		// Flush it!
		flushPersistent();
		
		// Set the item again
		try {
			storagePersistent.setItem(dbID + ' -> ' + type + '_' + id, value);
			
			return true;
		}
		
		// New error!
		catch(e) {
			logThis('Aborted: error while writing a persistent database entry (' + dbID + ' -> ' + type + ' -> ' + id + '): ' + e, 1);
		}
	}

	return false;
}

// Persistent: used to remove a database entry
function removePersistent(dbID, type, id) {
	try {
		storagePersistent.removeItem(dbID + '_' + type + '_' + id);

		return true;
	}
	
	catch(e) {
		logThis('Error while removing a persistent database entry (' + dbID + ' -> ' + type + ' -> ' + id + '): ' + e, 1);
	}

	return false;
}

// Persistent: used to check a database entry exists
function existPersistent(dbID, type, id) {
	return getPersistent(dbID, type, id) != null;
}

// Persistent: used to clear all the database
function resetPersistent() {
	try {
		storagePersistent.clear();

		logThis('Persistent database cleared.', 3);
		
		return true;
	}
	
	catch(e) {
		logThis('Error while clearing persistent database: ' + e, 1);
	}

	return false;
}

// Persistent: used to flush the database
function flushPersistent() {
	try {
		// Get the stored session entry
		var session = getPersistent('global', 'session', 1);
		
		// Reset the persistent database
		resetPersistent();
		
		// Restaure the stored session entry
		if(session)
			setPersistent('global', 'session', 1, session);
		
		logThis('Persistent database flushed.', 3);
		
		return true;
	}
	
	catch(e) {
		logThis('Error while flushing persistent database: ' + e, 1);
	}

	return false;
}/* BROWSER DETECT
 * http://www.quirksmode.org/js/detect.html
 * License: dual-licensed under MPLv2 and the original license
 */

var BrowserDetect = {
	init: function () {
		this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
		this.version = this.searchVersion(navigator.userAgent)
			|| this.searchVersion(navigator.appVersion)
			|| "an unknown version";
		this.OS = this.searchString(this.dataOS) || "an unknown OS";
	},
	
	searchString: function (data) {
		for (var i=0;i<data.length;i++)	{
			var dataString = data[i].string;
			var dataProp = data[i].prop;
			this.versionSearchString = data[i].versionSearch || data[i].identity;
			if (dataString) {
				if (dataString.indexOf(data[i].subString) != -1)
					return data[i].identity;
			}
			else if (dataProp)
				return data[i].identity;
		}
	},
	
	searchVersion: function (dataString) {
		var index = dataString.indexOf(this.versionSearchString);
		if (index == -1) return;
		return parseFloat(dataString.substring(index+this.versionSearchString.length+1));
	},
	
	dataBrowser: [
		{
			string: navigator.userAgent,
			subString: "Chrome",
			identity: "Chrome"
		},
		{ 	string: navigator.userAgent,
			subString: "OmniWeb",
			versionSearch: "OmniWeb/",
			identity: "OmniWeb"
		},
		{
			string: navigator.vendor,
			subString: "Apple",
			identity: "Safari",
			versionSearch: "Version"
		},
		{
			prop: window.opera,
			identity: "Opera"
		},
		{
			string: navigator.vendor,
			subString: "iCab",
			identity: "iCab"
		},
		{
			string: navigator.vendor,
			subString: "KDE",
			identity: "Konqueror"
		},
		{
			string: navigator.userAgent,
			subString: "Firefox",
			identity: "Firefox"
		},
		{
			string: navigator.vendor,
			subString: "Camino",
			identity: "Camino"
		},
		{		// for newer Netscapes (6+)
			string: navigator.userAgent,
			subString: "Netscape",
			identity: "Netscape"
		},
		{
			string: navigator.userAgent,
			subString: "MSIE",
			identity: "Explorer",
			versionSearch: "MSIE"
		},
		{
			string: navigator.userAgent,
			subString: "Gecko",
			identity: "Mozilla",
			versionSearch: "rv"
		},
		{ 		// for older Netscapes (4-)
			string: navigator.userAgent,
			subString: "Mozilla",
			identity: "Netscape",
			versionSearch: "Mozilla"
		}
	],
	
	dataOS : [
		{
			string: navigator.platform,
			subString: "Win",
			identity: "Windows"
		},
		{
			string: navigator.platform,
			subString: "Mac",
			identity: "Mac"
		},
		{
			   string: navigator.userAgent,
			   subString: "iPhone",
			   identity: "iPhone/iPod"
	    },
		{
			string: navigator.platform,
			subString: "Linux",
			identity: "Linux"
		}
	]
};

BrowserDetect.init();
/*

Jappix - An open social platform
These are the common JS script for Jappix

-------------------------------------------------

License: dual-licensed under AGPL and MPLv2
Authors: Valérian Saliou, olivierm, regilero, Maranda
Last revision: 24/09/12

*/

// Checks if an element exists in the DOM
function exists(selector) {
	if(jQuery(selector).size() > 0)
		return true;
	else
		return false;
}

// Checks if Jappix is connected
function isConnected() {
	if((typeof con != 'undefined') && con && con.connected())
		return true;
	
	return false;
}

// Checks if Jappix has focus
function isFocused() {
	try {
		if(document.hasFocus())
			return true;
		
		return false;
	}
	
	catch(e) {
		return true;
	}
}

// Generates the good XID
function generateXID(xid, type) {
	// XID needs to be transformed
	// .. and made lowercase (uncertain though this is the right place...)
	xid = xid.toLowerCase();

	if(xid && (xid.indexOf('@') == -1)) {
		// Groupchat
		if(type == 'groupchat')
			return xid + '@' + HOST_MUC;
		
		// One-to-one chat
		if(xid.indexOf('.') == -1)
			return xid + '@' + HOST_MAIN;
		
		// It might be a gateway?
		return xid;
	}
	
	// Nothing special (yet bare XID)
	return xid;
}

// Gets the asked translated string
function _e(string) {
	return string;
}

// Replaces '%s' to a given value for a translated string
function printf(string, value) {
	return string.replace('%s', value);
}

// Returns the string after the last given char
function strAfterLast(given_char, str) {
	if(!given_char || !str)
		return '';
	
	var char_index = str.lastIndexOf(given_char);
	var str_return = str;
	
	if(char_index >= 0)
		str_return = str.substr(char_index + 1);
	
	return str_return;
}

// Properly explodes a string with a given character
function explodeThis(toEx, toStr, i) {
	// Get the index of our char to explode
	var index = toStr.indexOf(toEx);
	
	// We split if necessary the string
	if(index != -1) {
		if(i == 0)
			toStr = toStr.substr(0, index);
		else
			toStr = toStr.substr(index + 1);
	}
	
	// We return the value
	return toStr;
}

// Cuts the resource of a XID
function cutResource(aXID) {
	return explodeThis('/', aXID, 0);
}

// Gets the resource of a XID
function thisResource(aXID) {
	// Any resource?
	if(aXID.indexOf('/') != -1)
		return explodeThis('/', aXID, 1);
	
	// No resource
	return '';
}

// nodepreps an XMPP node
function nodeprep(node) {
	// Spec: http://tools.ietf.org/html/rfc6122#appendix-A

	if(!node)
		return node;

	// Remove prohibited chars
	var prohibited_chars = ['"', '&', '\'', '/', ':', '<', '>', '@'];

	for(j in prohibited_chars)
		node = node.replace(prohibited_chars[j], '');

	// Lower case
	node = node.toLowerCase();

	return node;
}

// Encodes quotes in a string
function encodeQuotes(str) {
	return (str + '').htmlEnc();
}

// Gets the bare XID from a XID
function bareXID(xid) {
	// Cut the resource
	xid = cutResource(xid);
	
	// Launch nodeprep
	if(xid.indexOf('@') != -1)
		xid = nodeprep(getXIDNick(xid)) + '@' + getXIDHost(xid);
	
	return xid;
}

// Gets the full XID from a XID
function fullXID(xid) {
	// Normalizes the XID
	var full = bareXID(xid);
	var resource = thisResource(xid);
	
	// Any resource?
	if(resource)
		full += '/' + resource;
	
	return full;
}

// Gets the nick from a XID
function getXIDNick(aXID) {
	// Gateway nick?
	if(aXID.match(/\\40/))
		return explodeThis('\\40', aXID, 0);
	
	return explodeThis('@', aXID, 0);
}

// Gets the host from a XID
function getXIDHost(aXID) {
	return explodeThis('@', aXID, 1);
}

// Checks if we are in developer mode
function isDeveloper() {
	return (DEVELOPER == 'on');
}

// Checks if we are RTL (Right-To-Left)
function isRTL() {
	return (_e("default:LTR") == 'default:RTL');
}

// Checks if anonymous mode is allowed
function allowedAnonymous() {
	return (ANONYMOUS == 'on');
}

// Checks if host is locked
function lockHost() {
	return (LOCK_HOST == 'on');
}

// Gets the full XID of the user
function getXID() {
	// Return the XID of the user
	if(con.username && con.domain)
		return con.username + '@' + con.domain;
	
	return '';
}

// Generates the colors for a given user XID
function generateColor(xid) {
	var colors = new Array(
			'ac0000',
			'a66200',
			'007703',
			'00705f',
			'00236b',
			'4e005c'
		     );
	
	var number = 0;
	
	for(var i = 0; i < xid.length; i++)
		number += xid.charCodeAt(i);
	
	var color = '#' + colors[number % (colors.length)];
	
	return color;
}

// Checks if the XID is a gateway
function isGateway(xid) {
	if(xid.indexOf('@') != -1)
		return false;
	
	return true;
}

// Gets the from attribute of a stanza (overrides some servers like Prosody missing from attributes)
function getStanzaFrom(stanza) {
	var from = stanza.getFrom();
	
	// No from, we assume this is our XID
	if(!from)
		from = getXID();
	
	return from;
}

// Logs a given data in the console
function logThis(data, level) {
	// Console not available
	if(!isDeveloper() || (typeof(console) == 'undefined'))
		return false;
	
	// Switch the log level
	switch(level) {
		// Debug
		case 0:
			console.debug(data);
			
			break;
		
		// Error
		case 1:
			console.error(data);
			
			break;
		
		// Warning
		case 2:
			console.warn(data);
			
			break;
		
		// Information
		case 3:
			console.info(data);
			
			break;
		
		// Default log level
		default:
			console.log(data);
			
			break;
	}
	
	return true;
}

// Gets the current Jappix app. location
function getJappixLocation() {
	var url = window.location.href;
	
	// If the URL has variables, remove them
	if(url.indexOf('?') != -1)
		url = url.split('?')[0];
	if(url.indexOf('#') != -1)
		url = url.split('#')[0];
	
	// No "/" at the end
	if(!url.match(/(.+)\/$/))
		url += '/';
	
	return url;
}

// Removes spaces at the beginning & the end of a string
function trim(str) {
	return str.replace(/^\s+/g,'').replace(/\s+$/g,'');
}

// Adds a zero to a date when needed
function padZero(i) {
	// Negative number (without first 0)
	if(i > -10 && i < 0)
		return '-0' + (i * -1);
	
	// Positive number (without first 0)
	if(i < 10 && i >= 0)
		return '0' + i;
	
	// All is okay
	return i;
}

// Escapes a string for a regex usage
function escapeRegex(query) {
	return query.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
}

// Returns a random array value
function randomArrayValue(arr) {
	return arr[Math.floor(Math.random() * arr.length)];
}

// Returns whether the browser is mobile or not
function isMobile() {
	try {
		return /Android|iPhone|iPod|iPad|Windows Phone|BlackBerry|Bada|Maemo|Meego|webOS/i.test(navigator.userAgent);
	} catch(e) {
		return false;
	}
}