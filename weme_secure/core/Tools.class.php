<?php
class Tools
{
	const SALT_KEY   = 'hJHE45g4fda41z5v4bnrt5kesr5454EE4';
    
	public static function encrypt($string){
		return strtr(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5(self::SALT_KEY), serialize($string), MCRYPT_MODE_CBC, md5(md5(self::SALT_KEY)))), '+/=', '-_,');
	}
	
	public static function decrypt($encrypted){
		return unserialize(rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5(self::SALT_KEY), base64_decode(strtr($encrypted, '-_,', '+/=')), MCRYPT_MODE_CBC, md5(md5(self::SALT_KEY))), "\0"));
	}
	
	public static function getLink($page="", $action="", $param=array(), $crypt=false){

		if(!$rewriting){
	
			if($page)
				$link .= '?page='.$page;
			
			if($action)
				$link .= '&action='.$action;
			
			
			if(count($param)){
				
				if(!$crypt){
					foreach($param as $key=>$value){
						$params .= '&'.$key."=".$value;
					}
				}
				
				else{
					$i=0;
					foreach($param as $key=>$value){
						$i++;
						$esp = count($param)==$i ? '' : '&';
						$parametres.=$key."=".$value.$esp;
						
					}
					$params="&crypt=".self::encrypt($parametres);
				}
				
			}
			
			return $link.$params;
		}
		else{
				
			if($modele=="#")
				return $link=$_SERVER['PHP_SELF'];
			
			if($modele){
	
					$link .= $page.'/';
			}
			
			if($vue){
					$link .=$action.'/';
			}
			
			
			if(count($param)){
				$iP=0;
				if(!$crypt){
					foreach($param as $key=>$value){
						$and =  ($iP==0) ? '?' : '&';
							
						$params .= $and.$key."=".$value;
						
						$iP++;
					}
					
				}
				
				else{
					$i=0;
					foreach($param as $key=>$value){
						$i++;
						$esp = count($param)==$i ? '' : '&';
						$parametres.=$key."=".$value.$esp;
						
					}
				
						$params="?crypt=".DBManager::encrypt($parametres);
				}
				
			}
			
			
			return '/'.$link.$params;
		}
	}
	
	public static function setFlashMessage($message='', $success=false, $flag = 'error',  $link=false){
		

		
		if(!($link))
			$link = self::getLink($request["page"], $request["action"]);
		
	
		$_SESSION[$flag] = array('message'=>$message, 'success'=>$success);
		
		
		header('Location:'.$link);
		exit;
		
	}
	
	public static function getFlashMessage($flag = 'error'){
		//DBManager::debugVar($_SESSION[$flag], false);
		if($_SESSION[$flag]){
			
			switch($_SESSION[$flag]['success']){
				
				case  0 :
					$note = 'error';
					break;
				case  1 :
					$note = 'success';
					break;
				case  2 :
					$note = 'warning';
					break;
				case  3 :
					$note = 'info';
					break;
				default :
					$note = 'error';
					break;
			}
			
			$mess = '<div class="alert alert-success-'.$note.'">
					'.$_SESSION[$flag]['message'].'
					
					</div>';
			
			unset($_SESSION[$flag]);
			return $mess;
		}

		
	}
	public static function resumeContent($content = "", $link=false, $max = 200, $rac = "[...]" ){
		
		$chaine=strip_tags($content);
		
		if(strlen($chaine)>=$max){
			// Met la portion de chaine dans $chaine
			$chaine=substr($chaine,0,$max); 
			// position du dernier espace
			$espace=strrpos($chaine," "); 
			// test si il ya un espace
			if($espace)
			// si ya 1 espace, coupe de nouveau la chaine
			$chaine=substr($chaine,0,$espace);
			// Ajoute ... à la chaine
			if(!$link)
				$chaine .= $rac;
			
			else
				$chaine .= '<a class="white" href="'.$link.'">'.$rac.'</a>';
		}
		
		return $chaine;
		
	}
	
	public static function stripslashes_deep($value) {
		if(is_array($value)) {
		    foreach($value as $k => $v) {
			$return[$k] = $this->stripslashes_deep($v);
		    }
		} elseif(isset($value)) {
		    $return = stripslashes($value);
		}
		return $return;
    
	}
	public static function randomString($taille=6){
		for ($i=0; $i<$taille; $i++) {
			$d=rand(1,30)%2;
			$string.= $d ? chr(rand(65,90)) : chr(rand(48,57));
		} 
		
		return $string;
	}	
	public static function randomPassword($length=5, $strength=0) {
		$numbers = '0123456789';
		$password = '';
		for ($i = 0; $i < $length; $i++) {
		  $password .= $numbers[(rand() % strlen($numbers))];
		}
		return $password;
	}
	public static function generateMd5($value=""){
		return md5($value);
	}
	
	public static function debugVar($var, $stop=true, $title=''){
		if ( $title ) echo "<h3>$title</h3>";
		echo "<pre style=\"text-align:left;\">";
		var_dump($var);
		echo '</pre>';
		if ( $stop ) exit;
	}
	
	
	public static function makeDir($path=array()){
	

		$arbo = '';
		foreach($path as $dir){
			$arbo .= '/'.$dir;
			$arbo = str_replace('//', '/', $arbo);
			if(!is_dir(_PATH_.$arbo))
				mkdir(_PATH_.$arbo);	
				@chmod(_PATH_.$arbo, 0777);
		}
		
		return $arbo;
	
	}
	
	public static function resizeAndCopyFile($file_name_source='', $file_name_destination='', $dir_destination= '/pictures', $largeur = '', $hauteur = '', $thumbnail=false, $posW=0,  $posH=0,  $posY=0, $posX=0){
		ini_set("memory_limit","20M");	
		if($file_name_source == '')
			return array('file'=>'Fichier source inexistant, la copie du fichier à échouée');
		
		if($file_name_destination['id_photo'] == '' or $file_name_destination['pho_extension'] == '')
			return array('file'=>'Fichier destination inexistant, la copie du fichier à échouée');
		else 
			$file_name_destination = strtr($file_name_destination,  "ÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ ","aaaaaaaaaaaaooooooooooooeeeeeeeecciiiiiiiiuuuuuuuuynn_");
		
		if($dir_destination == '')
			return array('file'=>'Répertoire destination inexistant, la copie du fichier à échouée...');
		else 
			$dir_destination = _PATH_.$dir_destination;
		
		if($largeur == '')
			return array('file'=>'Aucune largeur définie');
			
		if($hauteur == '')
			return array('file'=>'Aucune hauteur définie');
		
		
		$dimsImage = getimagesize($file_name_source);
		$typeImage = $dimsImage[2];
	
		
		if( $typeImage == 2 ) {
			$ImageChoisie = imagecreatefromjpeg($file_name_source);
			$pho_extension = ".jpg";
		}
		elseif( $typeImage == 1 ) {
			$ImageChoisie = imagecreatefromgif($file_name_source);
			$pho_extension = ".gif";
		} 
		elseif( $typeImage == 3 ) {
			$ImageChoisie = imagecreatefrompng($file_name_source);
			$pho_extension = ".png";
		}
		else return array('file'=>'Une erreur est survenue : type image inconnu');
	
	
		if($dimsImage[0]>$dimsImage[1]){
			$new_width   = $largeur;
			$new_height = ($new_width * $dimsImage[1])/$dimsImage[0];
		}
		
		elseif($dimsImage[0]<$dimsImage[1]){
			$new_height   = $hauteur;
			$new_width = ($new_height * $dimsImage[0])/$dimsImage[1];
		}
		
		else{
			$new_width   = $largeur;
			$new_height = $hauteur;
		}
				
		if($thumbnail)
			$new_image = imagecreatetruecolor($largeur, $hauteur) or die ("Erreur");
			
		else
			$new_image = imagecreatetruecolor($new_width, $new_height) or die ("Erreur");
		
		imagesavealpha($new_image, true); 
		$color = imagecolorallocatealpha($new_image,0x00,0x00,0x00,127);
		imagefill($new_image, 0, 0, $color); 
		
		
			$w = $posW ? $posW: $dimsImage[0];
			$h = $posH ? $posH: $dimsImage[1];
			
		imagecopyresampled($new_image, $ImageChoisie, 0, 0, $posX, $posY, $new_width, $new_height, $w, $h);
		
		if(is_dir($dir_destination.'/')){
			
			if( $typeImage == 2 ) {
				imagejpeg($new_image , "$dir_destination/$file_name_destination$pho_extension", 100);
			}
			elseif( $typeImage == 1 ) {
				imagegif($new_image , "$dir_destination/$file_name_destination$pho_extension", 100);
			} 
			elseif( $typeImage == 3 ) {
				
				imagepng($new_image , "$dir_destination/$file_name_destination$pho_extension");
			}
			else return array('file'=>'Une erreur est survenue : type image inconnu ');
			
			
			if(!file_exists("$dir_destination/$file_name_destination$pho_extension"))
				return array('file'=>'La copie du fichier à échouée');
			else return array('file_name_destination' => $file_name_destination, 'extension' => $pho_extension );
		}
		
		else return array('file'=>'une erreur est survenue le répertoire de destination ('.$dir_destination.'/) n\'existe pas');
		
	

	}
	
	public static function verifTypeImage($filename){
		/*
		  1 	IMAGETYPE_GIF
		  2 	IMAGETYPE_JPEG
		  3 	IMAGETYPE_PNG
		  4 	IMAGETYPE_SWF
		  5 	IMAGETYPE_PSD
		  6 	IMAGETYPE_BMP
		  7 	IMAGETYPE_TIFF_II (intel byte order)
		  8 	IMAGETYPE_TIFF_MM (motorola byte order)
		  9 	IMAGETYPE_JPC
		  10 	IMAGETYPE_JP2
		  11 	IMAGETYPE_JPX
		  12 	IMAGETYPE_JB2
		  13 	IMAGETYPE_SWC
		  14 	IMAGETYPE_IFF
		  15 	IMAGETYPE_WBMP
		  16 	IMAGETYPE_XBM
		  17 	IMAGETYPE_ICO
		  */
		$image = exif_imagetype($filename);
		
		if(!$image || ($image>3))
		  return false;
		 else return true;
		  
	 }
	public static function getFileData($fichier){

		$file=array();
		$path_parts    = pathinfo($fichier);	
		$file['extension']   = $path_parts['extension'];
		$file['name']  = $path_parts['filename'];
		
		return (object)$file; 
	}
	public static function copyFile($file_name_source='',  $file_name_destination = '', $dir_destination= ''){
		
		if($file_name_source == '')
			return array ('file'=>'Fichier source inexistant, la copie du fichier à échouée');
		
		if($file_name_destination == '')
			return array ('file'=>'Fichier destination inexistant, la copie du fichier à échouée');
					
		if($dir_destination == '')
			return array ('file'=>'Répertoire '.$dir_destination.' destination inexistant, la copie du fichier à échouée !');
		
					
				
		if(is_dir(_PATH_.$dir_destination.'/')){
	
			copy($file_name_source,_PATH_."$dir_destination/$file_name_destination"); 
			if(!file_exists(_PATH_."$dir_destination/$file_name_destination"))
				return array ('file'=>'La copie du fichier à échouée');
			else {
				chmod(_PATH_.$dir_destination.'/'.$file_name_destination, 0777);
				return $dir_destination.'/'.$file_name_destination;
			}
		}
		
		else
			return array ('file'=>'une erreur est survenue le répertoire de destination ('.$dir_destination.'/) n\'existe pas');
	}
	public static function removeFile($file=""){
		
	
		if(file_exists(_PATH_.$file) && !is_dir(_PATH_.$file)){
			if(unlink(_PATH_.$file))
				return true;
			else
				return false;
		}
		else
			return false;
		
	}
	public static function generate_jpeg_from_pdf($in ='', $out = "", $update = false){
			
			$in = _PATH_.$in;
			$out = _PATH_.$out;
			
			if($update){
				
				$this->removeFile($out);
				
			}
			
			if(file_exists($in)){
			   
				
				
				
					$quality=90;
					$res='40';
					
					set_time_limit(900);
					exec("'/usr/local/bin/gs' '-dNOPAUSE' '-sDEVICE=jpeg' '-dFirstPage=1' '-dUseCIEColor' '-dTextAlphaBits=4' '-dGraphicsAlphaBits=4' '-o$out' '-r$res' '-dJPEGQ=$quality' '$in'",$output);
					
					
					//DEBUG
					
					//for($i=0;$i<count($output);$i++)
					  //  echo($output[$i] .'<br/>');
					if(file_exists($out)){
					     
						return $out;
					}
					else
						return false;
				
			}
			else
				return false;//Pas de PDF
	}
	public static function exportPdftoText($chemin_destination){
			
			/* page-specific option list */
			$pageoptlist = "granularity=page";
			
			/* separator to emit after each chunk of text. This depends on the
			* application's needs; for granularity=word a space character may be useful
			*/
			$separator = "\n";
			
			$pageno = 0;
			$infilename = _PATH_.$chemin_destination;
			
			   
			$tet = new TET();

			// $tet->set_option($globaloptlist);
		      
			 $doc = $tet->open_document($infilename, $docoptlist);
			 if ($doc == -1) {

				switch ($tet->get_errnum()){
											

					case 8002:
						return array ('file_cv'=>'Votre document doit comporter moins de 10 pages');
						break;
					default:
						return array ('file_cv'=>$tet->get_errmsg());
						break;
				}
				
			     //die("Error ". $tet->get_errnum() . " in " . $tet->get_apiname()
				// . "(): " . $tet->get_errmsg() . "\n");
			 }
		      	
			

			/* get number of pages in the document */
			$n_pages = $tet->pcos_get_number($doc, "length:pages");
		     
			/* loop over pages in the document */
			for ($pageno = 1; $pageno <= $n_pages; ++$pageno) {
		     
			    $page = $tet->open_page($doc, $pageno, $pageoptlist);
		     
		     
			    /* Retrieve all text fragments; This is actually not required
			     * for granularity=page, but must be used for other granularities.
			     */
			    while (($text = $tet->get_text($page)) != "") {
			    $datatexte .= $text;
			    $datatexte .= $separator;
			    
			    }
		     
		     
			    $tet->close_page($page);
			}
		     
		
			   
			   $tet->close_document($doc);
			$tet = 0;
			
			return $datatexte; 
	}
	public static function sendEmail($filtre = array()){
		
	    //$DBadmin = new DBAdmin();
	    //$mail_trad = $DBadmin->getPageTraduction($page=array("modele"=>"mail", "vue"=>"abonnement"));
	    
	    $email = $filtre['email'];
	    
	    $mail = new PHPMailer();
	    $mail->setCharSet('UTF-8');
	    $mail->IsHTML(TRUE);
	    
	    $reply = ($filtre['author'])? $filtre['author'] : MAIL_REPLY_TO;
	    $entete = ($filtre['entete'])?$filtre['entete']:$filtre['sujet'];
	    
	    $mail->From =  $filtre['author'] ? $filtre['author'] : MAIL_FROM;
	    $mail->FromName =  $filtre["fromname"] ? $filtre["fromname"] : MAIL_FROM_NAME;

	    $mail->AddAddress($email);
	    $mail->AddBCC(MAIL_FROM);
	    $mail->Subject =  $filtre['sujet'];
	    $mail->AddReplyTo($reply);
	    $message = $filtre['message'];
	    $mail->Body = $message;
	    $mail->WordWrap = 72;
      
	    if($mail->Send())
		return true;
	    else
		throw new myException('Une erreur est survenue');
	}
	
	

	public static function verifDate($date = "", $lan='fr'){
		
		if($lan == "fr"){
			return preg_match('`^\d{1,2}/\d{1,2}/\d{4}$`', $date );
		}
		elseif($lan == "en"){
			return preg_match('`(\d{4})-(\d{1,2})-(\d{1,2})`', $date );
		}
			return false;
	}
	
	
	
	public static function shortConvertLanDateToDb($date){
		if($_SESSION['langue']=='fr')
			return self::shortConvertFrDateToDb($date);
		elseif($_SESSION['langue']=='en')
			return $date;
		elseif($_SESSION['langue']=='es')
			return$date;
	}
	
	public static function shortConvertDBDateToLan($date){
		if($_SESSION['langue']=='fr')
			return self::shortConvertDBDateToFr($date);
		elseif($_SESSION['langue']=='en')
			return $date;
		elseif($_SESSION['langue']=='es')
			return$date;
	}
	
	public static function shortConvertFrDateToDb($date){
		if($date){
			$date = explode('/', $date);
			return $date["2"].'-'.$date["1"].'-'.$date["0"];
		}
		
	}
	
	public static function shortConvertDBDateToFr($date){
		if($date=="0000-00-00" || empty($date))
			$date = date('Y-m-d');
			
		$dateformat = new DateTime($date);
		return $dateformat->format('d/m/Y');	
	}
	
	public static function paginate($currentpage= 1, $nbreElement=null ){
		// Numero de page (1 par défaut)
		if( isset($currentpage) && is_numeric($currentpage) )
		    $page = $currentpage;
		else
		    $page = 1;
	
		// Numéro du 1er enregistrement à lire
		$limit_start = ($page - 1) * NOMBRE_PER_PAGE;    
		    
		// Pagination
		 $nb_pages = ceil($nbreElement / NOMBRE_PER_PAGE);
		 
		return array('current_page'=>$currentpage, 'total_pages'=>$nb_pages, 'limit'=>$limit_start);
	}
	public static function generateThumbnail($file_name_source, $largeur=150, $hauteur=110) {
		
		$dimsImage = getimagesize($file_name_source);
		$typeImage = $dimsImage[2];
		
		
		if( $typeImage == 2 ) {
			$ImageChoisie = imagecreatefromjpeg($file_name_source);
			$pho_extension = ".jpeg";
		}
		elseif( $typeImage == 1 ) {
			$ImageChoisie = imagecreatefromgif($file_name_source);
			$pho_extension = ".gif";
		} 
		elseif( $typeImage == 3 ) {
			$ImageChoisie = imagecreatefrompng($file_name_source);
			$pho_extension = ".png";
		}
		else throw new myException('Une erreur est survenue : type image inconnu ('.$typeImage.')');
	
	
		if($dimsImage[0]>$dimsImage[1]){
	
			$new_width   = $largeur;
			$new_height = ($new_width * $dimsImage[1])/$dimsImage[0];
			
			if($new_height<$hauteur){
				$new_height   = $hauteur;
				$new_width = ($new_height * $dimsImage[0])/$dimsImage[1];
			}
			
		}
		elseif($dimsImage[0]<$dimsImage[1]){
			$new_height   = $hauteur;
			$new_width = ($new_height * $dimsImage[0])/$dimsImage[1];
			if($new_width<$largeur){
				$new_width   = $largeur;
				$new_height = ($new_width * $dimsImage[1])/$dimsImage[0];
			}
		}
		else{
			$new_width   = $largeur;
			$new_height = ($new_width * $dimsImage[1])/$dimsImage[0];
		}
		
	
		$new_image = imagecreatetruecolor($largeur, $hauteur) or die ("Erreur");

		imagesavealpha($new_image, true); 
		$color = imagecolorallocatealpha($new_image,0x00,0x00,0x00,127);
		imagefill($new_image, 0, 0, $color); 
		
		imagecopyresampled($new_image, $ImageChoisie, 0, 0, 0, 0, $new_width, $new_height, $dimsImage[0], $dimsImage[1]);

		
	
		
		if( $typeImage == 2 ) {
			return imagejpeg($new_image);
		}
		elseif( $typeImage == 1 ) {
			return imagegif($new_image);
		} 
		elseif( $typeImage == 3 ) {
			
			return imagepng($new_image);
		}
		else throw new myException('Une erreur est survenue : type image inconnu ('.$typeImage.')');
	}
	
	public static function replaceFlush($buffer){
		if($_REQUEST['search']['mots_cle']){
			$pattern      = '#(?!<.*?)(\b'.$_REQUEST['search']['mots_cle'].'\b)(?![^<>]*?>)#si';
			$replacement  = '<span class="bgOrange white">'.$_REQUEST['search']['mots_cle'].'</span>';
			    
			$buffer     = preg_replace( $pattern, $replacement, $buffer );  
		}
		return $buffer;
	}
	
	public static function  ScanDirectory($Directory){
		$files = array();
		$MyDirectory = opendir($Directory) or die('Erreur');
		$i=0;
		while($Entry = @readdir($MyDirectory)) {

			if($Entry != '.' && $Entry != '..') {
				$files[]= $Entry;
				$i++;
			}
			
		}
		closedir($MyDirectory);
		
		return (object) array('count'=>$i, 'templates'=>$files);
	}

}

?>