<?php
class DBMessage extends DBConnect{
	
	//TRES IMPORTANT
	//Les méthodes save doivent être privées et son uniquement appelées via les méthodes make
	//Les tests de saisie s'effectuent OBLIGATOIREMENT depuis les méthodes make

	private function saveMessage($message=array(), $ignore=array(), $debug=false){
               
		$req = "INSERT INTO usr_clients_messages SET
                        modifie_le = NOW(),
                        id_identifiant = ".$this->db->quote($message['id_identifiant'], PDO::PARAM_STR).",
                        mess_message = ".$this->db->quote($message['mess_message'], PDO::PARAM_STR).",
                        mess_titre = ".$this->db->quote($message['mess_titre'], PDO::PARAM_STR);
				
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'brown', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
		
		if($this->db->exec($req)){
				
				return $this->db->lastInsertId();
		}

	}
	private function updMessage($message=array(),  $ignore=array(), $debug=false){
             
		$req .= "UPDATE usr_clients_messages SET
			modifie_le              = NOW(),
			mess_message            = ".$this->db->quote($message['mess_message'], PDO::PARAM_STR).",
                        mess_titre              = ".$this->db->quote($message['mess_titre'], PDO::PARAM_STR)."
			WHERE id_client_message = ".$this->db->quote($message['id_client_message'], PDO::PARAM_STR)."
			AND id_identifiant      = ".$this->db->quote($message['id_identifiant'], PDO::PARAM_STR);
		
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'brown', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
		
		if($this->db->exec($req)){
			
			
			return $message['id_client_message'];
		}

		
	}
	private function rmvMessage($message=array(),  $debug=false){
             
		$req = "DELETE FROM usr_clients_messages
			WHERE id_client_message = ".$this->db->quote($message['id_client_message'], PDO::PARAM_STR)."
			AND id_identifiant      = ".$this->db->quote($message['id_identifiant'], PDO::PARAM_STR);
		
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'brown', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	

		if($this->db->exec($req)){
			
			
			return true;
		}

		
	}
	public function getListMessages($filtre = array(), $limit=array(0,NOMBRE_PER_PAGE), $matching=false){
		//$matching indique que la methode doit trouver des id_mission dans toutes les tables adm_ sinon el renvoi false
		$messages = array();
		
		$counter = array(
				'COUNT' => 'SELECT count(DISTINCT(id_client_message)) AS total, ' ,
				'SELECT'=> 'SELECT DISTINCT(id_client_message), '
				);

		$req = "id_identifiant, mess_titre, mess_message
			FROM usr_clients_messages
			WHERE modifie_le ";
			
		foreach($filtre as $key=>$value){
				if(is_array($value)){
					if(count($value))
						$req .= " AND ".$key." IN ('".implode("','", $value)."')";
					else
						$req .= " AND ".$key." IN ('')";	
				}
				else
					$req .= " AND ".$key." = ".$this->db->quote($value, PDO::PARAM_STR);
		}	
		
		$req .= " ORDER BY modifie_le DESC  ";
		
		if(is_array($limit)){
			$limit = array(
					'COUNT' => '' ,
					'SELECT'=> "LIMIT ".implode(",", $limit)
					);
		}

		
		if($debug)
			throw new myException($counter['COUNT'].$req.$limit['COUNT']);
		if(DEBUG)
			parent::$request[] =  array('color'=>'brown', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$counter['COUNT'].$req.$limit['COUNT']);	
		
		
		//Le count avec la requete
		$resultats = $this->db->query($counter['COUNT'].$req.$limit['COUNT']);
		
		//si on un total superieur a zéro
		//on reexecute la requete afin de recuperer les elements
                if($count = $resultats->fetch(PDO::FETCH_OBJ)->total){
			
			if($debug)
				throw new myException($counter['SELECT'].$req.$limit['SELECT']);
			if(DEBUG)
				parent::$request[] =  array('color'=>'brown', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$counter['SELECT'].$req.$limit['SELECT']);
						
			$resultats = $this->db->query($counter['SELECT'].$req.$limit['SELECT']);

			while ($message = $resultats->fetch(PDO::FETCH_ASSOC)){
				
				$messages[] = $message;	
			}
			
			$resultats->closeCursor();
			
                }
		return array('count'=>$count, 'messages'=>$messages);
		
	}
        //Methode appelée pour procéder à l'envoi d'un message
	public function makeMail($array=array()){
		$error = array(); //on stock nos erreurs eventuelles
		// I. On commence par hydrater nos tableaux (cast des variable int, string...) unb pour la réponse et l'autre pour la PJ
		//Ce que l'on enregistrea en BDD
		$mail = array(
				'usr_email' =>               $array['usr_email'],//aaray
				'id_identifiant' =>          $array['id_identifiant'],
                                'mail_message' =>            $array['mail_message'], 
                                'mail_titre' =>              $array['mail_titre'],
				'mail_author' =>             $array['mail_author']
                        
                        );
		//Tools::debugVar($reponse);
		// II. On effectue les tests sur les entrées saisies
		if(!isset($mail['id_identifiant']) || empty($mail['id_identifiant']))
		       $error['id_identifiant'] = 'Vous devez etre connecté';
		if(!is_array($mail['usr_email']) || !count($mail['usr_email']))
		       $error['usr_email'] = 'Aucun destinataire de séléctionné';
		if((!isset($mail['mess_message']) || empty($mail['mess_message'])) && !isset($array['mail_template']))
		       $error['mail_message'] = 'Vous devez saisir un message ou sélectionner un template';       
		if(!isset($mail['mail_titre']) || empty($mail['mail_titre']))
		       $error['mail_titre'] = 'Vous devez saisir un titre';
		if(!isset($mail['mail_author']) || empty($mail['mail_author']))
		       $error['mail_author'] = 'Vous devez saisir votre adresse email';    

	        if(count($error)>0)//on a des erreurs on stop ici
			return $error;
		
		if( isset($array['mail_template'])){
			$array['mail_message'] = $message = file_get_contents('web/mails/'.$array['mail_template']);
		}
		
		//CE que l'on envoie par email
		$mailSend = array(
				'email' =>              implode(",",$array['usr_email']),//aaray
                                'message' =>            $array['mail_message'], 
                                'sujet' =>              $array['mail_titre'],
				'author' =>             $array['mail_author']
                        
                        );

                // III. On enregistre dans la base de donnée
		if(Tools::sendEmail($mailSend)){
			
			return true; 
			
		}
	
	}
	//Methode appelée pour procéder à l'enregsitrement d'un message type
	public function makeMessage($array=array()){
		$error = array(); //on stock nos erreurs eventuelles
		// I. On commence par hydrater nos tableaux (cast des variable int, string...) unb pour la réponse et l'autre pour la PJ
		//Ce que l'on enregistrea en BDD
		$message = array(
				'id_identifiant' =>          $array['id_identifiant'],
                                'mess_message' =>            $array['mess_message'], 
                                'mess_titre' =>              $array['mess_titre']
                        
                        );
		//Tools::debugVar($reponse);
		// II. On effectue les tests sur les entrées saisies
		if(!isset($message['id_identifiant']) || empty($message['id_identifiant']))
		       $error['id_identifiant'] = 'Vous devez etre connecté';
		if(!isset($message['mess_message']) || empty($message['mess_message']))
		       $error['mess_message'] = 'Vous devez saisir un message';       
		if(!isset($message['mess_titre']) || empty($message['mess_titre']))
		       $error['mess_titre'] = 'Vous devez saisir un titre';    

	        if(count($error)>0)//on a des erreurs on stop ici
			return $error;
			
                // III. On enregistre dans la base de donnée
		if($id_client_message = $this->saveMessage($message)){
				
				// IV. On fait les traitements 	adequats envoie de mails et generation des messages de retour
				return $id_client_message; 
		}
	
	}
	//Methode appelée pour edition message type
	public function editMessage($array=array()){
		$error = array(); //on stock nos erreurs eventuelles
		// I. On commence par hydrater nos tableaux (cast des variable int, string...) unb pour la réponse et l'autre pour la PJ
		//Ce que l'on enregistrea en BDD
		$message = array(
				'id_identifiant' =>          $array['id_identifiant'],
                                'mess_message' =>            $array['mess_message'],
				'id_client_message' =>       $array['id_client_message'], 
                                'mess_titre' =>              $array['mess_titre']
                        
                        );
		//Tools::debugVar($reponse);
		// II. On effectue les tests sur les entrées saisies
		if(!isset($message['id_client_message']) || empty($message['id_client_message']))
		       $error['id_client_message'] = 'Vous devez sélectionner un message';
		if(!isset($message['id_identifiant']) || empty($message['id_identifiant']))
		       $error['id_identifiant'] = 'Vous devez etre connecté';
		if(!isset($message['mess_message']) || empty($message['mess_message']))
		       $error['mess_message'] = 'Vous devez saisir un message';       
		if(!isset($message['mess_titre']) || empty($message['mess_titre']))
		       $error['mess_titre'] = 'Vous devez saisir un titre';    

	        if(count($error)>0)//on a des erreurs on stop ici
			return $error;
			
                // III. On enregistre dans la base de donnée
		if($id_client_message = $this->updMessage($message)){
				
				// IV. On fait les traitements 	adequats envoie de mails et generation des messages de retour
				return $id_client_message; 
		}
	
	}
	//Methode appelée pour procéder à la suppression d'un message type
	public function removeMessage($array=array()){
		$error = array(); //on stock nos erreurs eventuelles
		// I. On commence par hydrater nos tableaux (cast des variable int, string...) unb pour la réponse et l'autre pour la PJ
		$message = array(
				'id_identifiant' =>          $array['id_identifiant'],
				'id_client_message' =>       $array['id_client_message']
                        
                        );
		//Tools::debugVar(count($reponse['id_mission_reponse']));
		// II. On effectue les tests sur les entrées saisies
		
		if(!isset($message['id_identifiant']) || empty($message['id_identifiant']))
		       $error['id_identifiant'] = 'Vous devez etre connecté';
		if(!isset($message['id_client_message']) || empty($message['id_client_message']))
		       $error['id_client_message'] = 'Vous devez sélectionner un message';
		       

	        if(count($error)>0)//on a des erreurs on stop ici
			return $error;
			
                // III. On enregistre dans la base de donnée
		if($id_reponse = $this->rmvMessage($message)){
			// IV. On effectue nos traitement mail et autres
			return true; 
			
		}
	
	}
}
