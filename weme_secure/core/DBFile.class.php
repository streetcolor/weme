<?php
class DBFile extends DBMembre{
	
	//TRES IMPORTANT
	//Les méthodes save doivent être privées et son uniquement appelées via les méthodes make
	//Les tests de saisie s'effectuent OBLIGATOIREMENT depuis les méthodes make
	
        const TYPE_VISUALISABLE    = 1;
        const TYPE_TELECHARGEABLE  = 2;
        const TYPE_CONSULTABLE     = 3;
        const TYPE_CV              = 4;
	const TYPE_PJ              = 5;
        const TYPE_AVATAR          = 6;
        const TYPE_PLAQUETTE       = 7;

	
	private function saveFile($file=array(), $ignore=array(), $debug=false){
                
		$req = "INSERT INTO usr_files SET
                        modifie_le = NOW(),
                        id_identifiant = ".$this->db->quote($file['id_identifiant'], PDO::PARAM_STR).",
                        id_prestataire_profil = ".$this->db->quote($file['id_prestataire_profil'], PDO::PARAM_STR).",
                        fichier_reference = ".$this->db->quote($file['fichier_reference'], PDO::PARAM_STR).",
                        fichier_nom = ".$this->db->quote($file['fichier_nom'], PDO::PARAM_STR).",
                        fichier_path = ".$this->db->quote($file['fichier_path'], PDO::PARAM_STR).",
                        fichier_extension = ".$this->db->quote($file['fichier_extension'], PDO::PARAM_STR).",
                        fichier_description = ".$this->db->quote($file['fichier_description'], PDO::PARAM_STR).",
                        fichier_type = ".$this->db->quote($file['fichier_type'], PDO::PARAM_STR)."
			ON DUPLICATE KEY UPDATE fichier_reference = fichier_reference";
		  
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'yellow', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);
		
		if($this->db->exec($req)){
			
                        return  $this->db->lastInsertId();
        
		}
		  else{
			if(is_numeric($file['id_file'])){
			      $req = "UPDATE usr_files SET
			      modifie_le = NOW(),
			      fichier_extension = ".$this->db->quote($file['fichier_extension'], PDO::PARAM_STR)."
			      WHERE id_identifiant = ".$this->db->quote($file['id_identifiant'], PDO::PARAM_STR)."
			      AND id_file = ".$this->db->quote($file['id_file'], PDO::PARAM_STR);
			    
				if($debug)
					throw new myException($req);
				if(DEBUG)
					parent::$request[] =  array('color'=>'yellow', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);    
			      if($this->db->exec($req)){
				      
				      return  true;
		      
			      }
			}
		  }
	    
		
	}
	private function updFile($file=array(), $ignore=array(), $debug=false){
                
		$req = " UPDATE usr_files SET ";
		
		foreach($file as $key=>$value){
			if(!in_array($key, $ignore))
				$req .= $key." = ".$this->db->quote($value, PDO::PARAM_STR)." , ";
		}		
		
		$req .= " modifie_le = NOW()  WHERE id_file = ".$this->db->quote($file["id_file"], PDO::PARAM_STR);
		
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'yellow', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);
		echo $req;

		if($this->db->exec($req)){
			
			return true;
	
		}

		
	}
	private function rmvFile($file=array(), $ignore=array(), $debug=false){
                
		$req = " DELETE FROM usr_files WHERE id_file  ";
		
		foreach($file as $key=>$value){
			if($key!='fichier_type')
				$req .= " AND ".$key." = ".$this->db->quote($value, PDO::PARAM_STR);
			else{
				//Uniquement pour les fichier type quand on veux en envoyer plusieurs ex : TYPE_VISUALISABLE et TYPE_TELECHARGEABLE
				if(is_array($value))
				    $req .= " AND ".$key." in (".implode(',', $value ).") ";
			}
		}		
		
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'yellow', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);

		if($this->db->exec($req)){
			
			return true;
	
		}

		
	}
	public function getListFiles($filtre = array(), $debug=false){
		$files = array();
		
		$counter = array(
				'COUNT' => 'SELECT count(id_file) AS total, ' ,
				'SELECT'=> 'SELECT id_file, '
				);
		$req = "id_identifiant, id_prestataire_profil,
			fichier_reference, fichier_nom, fichier_description, fichier_path, fichier_extension,  fichier_type 
			FROM usr_files
			WHERE modifie_le ";
			
		foreach($filtre as $key=>$value){
				if(is_array($value)){
					if(count($value))
						$req .= " AND ".$key." IN ('".implode("','", $value)."')";
					else
						$req .= " AND ".$key." IN ('".implode(',', $value)."')";	
				}
				else
					$req .= " AND ".$key." = ".$this->db->quote($value, PDO::PARAM_STR);
		}		
				
                
		if($debug)
			throw new myException($counter['COUNT'].$req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'yellow', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$counter['COUNT'].$req);

		$resultats = $this->db->query($counter['COUNT'].$req);
		
		if($count = $resultats->fetch(PDO::FETCH_OBJ)->total){
			
			if($debug)
				throw new myException($counter['SELECT'].$req);
			if(DEBUG)
				parent::$request[] =  array('color'=>'yellow', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$counter['SELECT'].$req);
			
			$resultats = $this->db->query($counter['SELECT'].$req);
			
			while ($file = $resultats->fetch(PDO::FETCH_ASSOC))
				 $files[] = new File($file);	
			
			$resultats->closeCursor();
			
		}
		return array('count'=>$count, 'files'=>$files);
		
	}
        //Methode appelée pour procéder à l'ajout d'un fichier
	public function makeFile($array=array()){
            $error = array(); //on stock nos erreurs eventuelles
	    
	    // I. On commence par hydrater notre tableau (cast des variable int, string...)
            $file_post =  $array['plaquette']['plaquette']['name'] ?  $array['plaquette']['plaquette'] : $array['file']['file'];//le fichier envoyé

            $flag      = $array['file_type']; //PJ, Avatar, CV, plaquette ou Realisation
            
            if(!in_array($flag, array('realisations', 'cv', 'avatar', 'pj', 'plaquette', 'template')))
                return array('file'=>'Une erreur est survenue aucun flag de défini');//error
            
	    if($file_post['size']>1000000)
		return array('file'=>'Votre fichier doit faire moins de 1mo');//error
	
	    if($file_post['error']!=UPLOAD_ERR_OK)
		return array('file'=>"L'importation du fichier à échouée");//error
	
            $file      = array(
                                    'id_identifiant' =>       $array['id_identifiant'],
                                    'id_prestataire_profil' =>$array['id_prestataire_profil'],
                                    'fichier_reference' =>    Tools::randomString(),
                                    'fichier_nom' =>          $file_post['name'],
                                    'fichier_path' =>         '/web/data/'.$array['id_identifiant'].'/'.$flag.'/'.$array['id_prestataire_profil'],
                                    'fichier_extension' =>    Tools::getFileData($file_post['name'])->extension,
                                    'fichier_description' =>  $array['fichier_description'],
                                    'fichier_type' =>         $array['fichier_type']
        
                              );
	    
	    	
	if($flag=='plaquette'){//Dans le cadre de l'importation d'une plaquette
		
		  $file['fichier_path']  =  '/web/data/'.$array['id_identifiant'].'/'.$flag;
		  $file['fichier_type']  =  self::TYPE_PLAQUETTE;
		  //si le repertoire CV est crée

		  if($chemin_destination = Tools::makeDir(array('web', 'data', $array['id_identifiant'],$flag))){

			//On vérifie l'extension du fichier
			//On vérifie l'extension du fichier
			$tab_doc_accepte = array("pdf");
			
			if(in_array($file['fichier_extension'], $tab_doc_accepte)){
				
			      //On récupere le detail du profil courant
			      $DBmembre                  = new DBMembre();
			
			     
			      //Si une plaquette exist déjà sur le profil on va recuperer le path et la reference existante
			      if(DBMembre::$id_type==DBMembre::TYPE_PRESTATAIRE){
					$prestataire              = $DBmembre->getPrestataire($file['id_identifiant']);//Renvoi un objet Prestataire()
					//Si un Avatar exist déjà sur le profil on va recuperer le path et la reference existante
					$existPlaquette           = $prestataire->getPlaquette();
			      }

			      if(is_object($existPlaquette)){
											     
					if($existPlaquette->getId_file()>0){

						$file['fichier_reference'] = $existPlaquette->getReference();
						$chemin_destination        = $existPlaquette->getPath(false)->path;
						$file['fichier_path']      = $chemin_destination;
						$file['id_file']           = $existPlaquette->getId_file();
						

					}
			      }
			    
			      //On ecrit un nom de fichier pour le stockage  
			      $plaquette_name = "PL_".$file['id_identifiant']."_".$file['fichier_reference'];
			      
			      if($error = Tools::copyFile($file_post['tmp_name'], $plaquette_name.".".$file['fichier_extension'] , $chemin_destination)){
				if(is_array($error))
					return $error;//error
			      }
			      //Generation de la miniature
			      $in  = $chemin_destination.'/'.$plaquette_name.".".$file['fichier_extension'];
			      $out = $chemin_destination.'/'.'GEN_'.$plaquette_name.'.jpg';
			      Tools::generate_jpeg_from_pdf($in, $out, false);
			      //Fin de la génération
			     
				      
			}
			else{
				return array('file_plaquette'=>'le fichier saisie doit etre un pdf');//error
			}
		  }
		  else{
			return array('file_plaquette'=>'Impossible de copier le  fichier');//error
		  }
		  
	    }
	    elseif($flag=='cv'){//Dans le cadre de l'importation d'un CV
		 
		  $file['fichier_path']  =  '/web/cv/'.date('Y').'/'.date('m');
		  $file['fichier_type']  =  self::TYPE_CV;
		  //si le repertoire CV est crée
		 
		  if($chemin_destination = Tools::makeDir(array('web', 'cv', date('Y'), date('m')))){

			//On vérifie l'extension du fichier
			$tab_doc_accepte = array("pdf");
			
			if(in_array($file['fichier_extension'], $tab_doc_accepte)){
				
			      //On récupere le detail du profil courant
			      $DBmembre                  = new DBMembre();
			      $profil                    = $DBmembre->getListProfils(array('id_prestataire_profil '=>$file['id_prestataire_profil'], 'id_identifiant'=>$file['id_identifiant']));//Renvoi un objet Profil()
			
				$file['fichier_reference'] = $profil["profils"][0]->getReference();
				
			     
			      //Si un CV exist déjà sur le profil on va recuperer le path et la reference existante
			      $existCv  = $profil["profils"][0]->getCv();
							     

				
			      if(is_object($existCv)){
					if($existCv->getId_file()>0){
						$chemin_destination        = $existCv->getPath(false)->path;
						$file['id_file']           = $existCv->getId_file();
						$file['fichier_path']      = $chemin_destination;
					}
			      }
			    
			      //On ecrit un nom de fichier pour le stockage  
			      $cv_name = "CV_".$file['id_prestataire_profil']."_".$file['fichier_reference'];
			      
			      if($error = Tools::copyFile($file_post['tmp_name'], $cv_name.".".$file['fichier_extension'] , $chemin_destination)){
				if(is_array($error))
					return $error;//error
			      }
			      //Generation de la miniature
			      $in  = $chemin_destination.'/'.$cv_name.".".$file['fichier_extension'];
			      $out = $chemin_destination.'/'.'GEN_'.$cv_name.'.jpg';
			      Tools::generate_jpeg_from_pdf($in, $out, false);
			      //Fin de la génération
			     
			      //echo Tools::exportPdftoText( $chemin_destination.'/'.$cv_name.".".$file['fichier_extension']);
			      //exit;
			      //Enregistrement du PDF en string
			      $DBmembre = new DBMembre;
			      $pdfTostring = Tools::exportPdftoText($chemin_destination.'/'.$cv_name.".".$file['fichier_extension']);
				
				 if(is_array($pdfTostring))
					return $pdfTostring;
				else{
					if(!$DBmembre->updProfil(array('pf_cv_pdf_string'=>$pdfTostring, 'id_prestataire_profil'=>$file['id_prestataire_profil'], 'id_identifiant'=>$file['id_identifiant']))){
						
							return array('file_cv'=>"L'indexation du CV à echoué car votre CV ne semble être applati");//error
						}
				}
				      
			}
			else{
				return array('file_cv'=>'le fichier saisie doit etre un pdf');//error
			}
		  }
		  else{
			return array('file'=>'Impossible de copier le  fichier');//error
		  }
		  
	    }
	    elseif($flag=='pj'){
		  
		  if($chemin_destination = Tools::makeDir(array('web', 'data', $file['id_identifiant'], $flag))){
			$file['fichier_type']      = self::TYPE_PJ;
			$file['fichier_path']      = $chemin_destination;

			if($error = Tools::copyFile($file_post['tmp_name'], $file['fichier_reference'].'.'.$file['fichier_extension'] , $chemin_destination)){
				if(is_array($error))
					return $error;//error
			}
		  }
		  
	    }
	    elseif($flag=='avatar'){
		
		  if($chemin_temporaire = Tools::makeDir(array('web', 'data', $file['id_identifiant'], $flag, 'tmp'))){
			
			if(isset($array["send_crop"])){//Si on a fait le crop en javascript
			      
			      $image                         = explode(".", $array['image']);
			      $file['fichier_type']          = self::TYPE_AVATAR;
			      $file['fichier_path']          = '/web/data/'.$file['id_identifiant'].'/avatar';
			      $file['fichier_reference']     = $image[0];
			      $file['fichier_extension']     = $image[1];
			      $file['fichier_nom']           = $array['image'];
			      $file['id_prestataire_profil'] = null;
			      
			      //On récupere le detail du profil courant
			      $DBmembre                  = new DBMembre();
			      if(DBMembre::$id_type==DBMembre::TYPE_PRESTATAIRE){
					$prestataire               = $DBmembre->getPrestataire($file['id_identifiant']);//Renvoi un objet Profil()
					//Si un Avatar exist déjà sur le profil on va recuperer le path et la reference existante
					$existAvatar               = $prestataire->getAvatar();
			      }
			      elseif(DBMembre::$id_type==DBMembre::TYPE_CLIENT){
					$client                    = $DBmembre->getClient($file['id_identifiant']);//Renvoi un objet Client()
					//Si un Avatar exist déjà sur le client on va recuperer le path et la reference existante
					$existAvatar               = $client->getAvatar();
			      }
			      
				
				if(is_object($existAvatar)){
					
					if($existAvatar->getReference() != ''){
					
					
						$file['fichier_reference'] = $existAvatar->getReference();
						$chemin_destination        = $existAvatar->getPath(false)->path;
						$file['fichier_path']      = $chemin_destination;
						$file['id_file']           = $existAvatar->getId_file();
					}
				 }
			      
			      
			

			      $targ_w = $array['w'];
			      $targ_h = $array['h'];
			      $jpeg_quality = 120;
		      
			      $src = _PATH_.$chemin_temporaire.'/'.$image[0].'.'.$image[1];
			      
			      $dimsImage = getimagesize($src);
			      $typeImage = $dimsImage[2];
			      
			      if( $typeImage == 2 ) {
				      $img_r = imagecreatefromjpeg($src);
				      $ext = "jpg";
			      }
			      elseif( $typeImage == 1 ) {
				      $img_r= imagecreatefromgif($src);
				      $ext = "gif";
			      } 
			      elseif( $typeImage == 3 ) {
				      $img_r = imagecreatefrompng($src);
				      $ext = "png";
			      }
			      
			     else return array('file_avatar'=>'Format invalide');//error
			      
			     
			     
			      $dst_r = ImageCreateTrueColor( $targ_w, $targ_h );
		      
			      imagecopyresampled($dst_r,$img_r,0,0,$array['x'],$array['y'], $targ_w,$targ_h,$array['w'],$array['h']);
	      
			      imagejpeg($dst_r,_PATH_.$file['fichier_path'].'/'.$file['fichier_reference'].'.'.$file['fichier_extension'],$jpeg_quality);
				      
			}
			else{//Si on envoie juste un fichier depuis le formulaire
			     
			      if($chemin_destination = Tools::resizeAndCopyFile($file_post['tmp_name'], $file['fichier_reference'] , $chemin_temporaire, 700, 700,false)){//DBManager::debugVar($chemin_destination, false);
					if(isset($chemin_destination["file"]))//on renvoi une erreur :)
						return array('file_avatar'=>$chemin_destination["file"]);//error
					else
						Tools::setFlashMessage('Update OK', true, 'success', Tools::getLink($array['page'],$array['action'], array('crop'=>'1', 'image'=>$chemin_destination["file_name_destination"].$chemin_destination["extension"])));

			      }
			      
			}
		  }
		   else{
			return array('file'=>'Impossible de copier le  fichier');//error
		  }
		  
	    }
	    elseif($flag=='realisations'){

		  //si le repertoire réalisation est crée
		  if($chemin_destination = Tools::makeDir(array('web', 'data', $file['id_identifiant'],$flag, $file['id_prestataire_profil']))){
				    
			if(Tools::verifTypeImage($file_post['tmp_name'])){
			      $chemin_destination = Tools::resizeAndCopyFile($file_post['tmp_name'], $file['fichier_reference'] , $chemin_destination, 800, 600,false);//DBManager::debugVar($chemin_destination, false);
			      $file['fichier_type'] = self::TYPE_VISUALISABLE;
			}
			else{
			      $chemin_destination = Tools::copyFile($file_post['tmp_name'], $file['fichier_reference'].'.'.$file['fichier_extension'] , $chemin_destination);//DBManager::debugVar($chemin_destination, false);
			      $file['fichier_type'] = self::TYPE_TELECHARGEABLE;
			}
			  
		  } 
	    }
	    
            elseif($flag=='template'){
		
		if(!$array['mess_titre'])
			return array('file'=>"Merci de saisir le titre de votre template");//error
		else{
			
			$unwanted_array = array(
				'Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
				'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U',
				'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c',
				'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o',
				'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y', ' '=> '_', '.'=> '_');
			
			$array['mess_titre'] = addslashes(strtr( $array['mess_titre'], $unwanted_array ));
			
		}
		$file   = array(
				'fichier_nom'           => $array['mess_titre'],
				'fichier_extension'     => Tools::getFileData($file_post['name'])->extension,
				'fichier_type'          => $array['fichier_type']
    
                        );
	    
		if($chemin_destination = Tools::makeDir(array('web', 'mails'))){
			//On vérifie l'extension du fichier
			$tab_doc_accepte = array("html");
			
			
			if(in_array($file['fichier_extension'], $tab_doc_accepte)){
				
			      if($error = Tools::copyFile($file_post['tmp_name'], $file['fichier_nom'].".".$file['fichier_extension'] , $chemin_destination)){
				if(is_array($error))
					return $error;//error
			      }
			    
				      
			}
			else{
				return array('file'=>'le fichier saisie doit etre un html');//error
			}
		  }
		  else{
			return array('file'=>'Impossible de copier le  fichier');//error
		  }
		  
	    }
	    
	    
	    // II. On effectue les tests sur les entrées saisies
            if(!isset($array['id_identifiant']))
                   return false;
            
            // III. On enregistre dans la base de donnée
            if($id_file = $this->saveFile($file)){
                    
                    // IV. On fait les traitements 	adequats envoie de mails et generation des messages de retour
                    return $id_file; 
                    
            }
	    
	    return true;
	
	}
	//Methode appelée pour procéder à l'edition d'un fichier
	public function editFile($array=array()){
             $error = array(); //on stock nos erreurs eventuelles
	    //I. On commence par récuperer l'ancien fichier pour le mettre en memoire
	    if(isset($array["id_file"]) && isset($array['id_prestataire_profil'])){
                $DFile    = new DBFile;
                $editFile = $DFile->getListFiles(array('id_file'=>$array["id_file"], 'id_prestataire_profil'=>$array['id_prestataire_profil']));
		
            }
	    else{
		return array('file'=>"Edition du fichier impossible");//error
	    }
	    
            // I. On commence par hydrater notre tableau (cast des variable int, string...)
            $file_post =   $array['file']['file'];//le fichier envoyé
          
            $flag      = $array['file_type']; //PJ, Avatar, CV ou Realisation

             if(!in_array($flag, array('realisations', 'cv', 'avatar', 'pj')))
                return array('file'=>'Une erreur est survenue aucun flag de défini');//error
           
	    if($file_post['size']>100000)
		return array('file'=>'Votre fichier doit faire moins de 1mo');//error

	    if($file_post['error']!=UPLOAD_ERR_OK){
		if($id_file = $this->updFile($file)){
		    return $id_file; 
		}
	    }
            $file      = array(
				'id_identifiant' =>       $array['id_identifiant'],
                                'id_prestataire_profil' =>$array['id_prestataire_profil'],
				'id_file'               =>  $array['id_file'],
                                'fichier_description'   =>  $array['fichier_description'],
				'id_prestataire_profil' =>$array['id_prestataire_profil']
                        );
	    
            // II. On effectue les tests sur les entrées saisies
            //si le repertoire réalisation est crée
	    if($file_post['error']==UPLOAD_ERR_OK){
		if($chemin_destination = Tools::makeDir(array('web', 'data', $file['id_identifiant'],$flag, $file['id_prestataire_profil']))){
			
			$file['fichier_reference'] = Tools::randomString();
			$file['fichier_nom']       = $file_post['name'];
			$file['fichier_path']      = '/web/data/'.$array['id_identifiant'].'/'.$flag.'/'.$array['id_prestataire_profil'];
			$file['fichier_extension'] = Tools::getFileData($file_post['name'])->extension;
			  
			if(Tools::verifTypeImage($file_post['tmp_name'])){
						
				$chemin_destination = Tools::resizeAndCopyFile($file_post['tmp_name'], $file['fichier_reference'] , $chemin_destination, 800, 600,false);//DBManager::debugVar($chemin_destination, false);
				$file['fichier_type'] = self::TYPE_VISUALISABLE;
			}
			else{
				
				$chemin_destination = Tools::copyFile($file_post['tmp_name'], $file['fichier_reference'].'.'.$file['fichier_extension'] , $chemin_destination);//DBManager::debugVar($chemin_destination, false);
				$file['fichier_type'] = self::TYPE_TELECHARGEABLE;
			};
		
			
		}
	    }
            if(!isset($array['id_identifiant']))
                   return array('file'=>'Une erreur est survenue aucun flag de défini');//error
            
            // III. On enregistre dans la base de donnée
            if($id_file = $this->updFile($file)){
                    
                    // IV. On fait les traitements 	adequats envoie de mails et generation des messages de retour
                    if($file_post['error']==UPLOAD_ERR_OK){
			Tools::removeFile($editFile['files'][0]->getPath());
		    }
		    return $id_file; 
                    
            }
	
	}
        //Methode appelée pour procéder à la suppresion d'un fichier
	public function removeFile($file=array()){
           
		//I. On commence par récuperer l'ancien fichier pour le mettre en memoire
		if(isset($file["id_file"]) && isset($file['id_prestataire_profil'])){
		    $DFile    = new DBFile;
		    $editFile = $DFile->getListFiles(array('id_file'=>$file["id_file"], 'id_prestataire_profil'=>$file['id_prestataire_profil'], 'fichier_type'=>$file['fichier_type']));
		    
		}
		else{
		   return array('file'=>"Edition du fichier impossible");//error
		}
		
		// III. On enregistre dans la base de donnée
		if($this->rmvFile(array('id_file'=>$file["id_file"], 'id_prestataire_profil'=>$file['id_prestataire_profil'], 'fichier_type'=>$file['fichier_type']))){
		
			    // IV. On fait les traitements 	adequats envoie de mails et generation des messages de retour
			    Tools::removeFile($editFile['files'][0]->getPath());
			    return $id_file; 
			
		}
	
	}
	
	//Transfert des fichiers
	public function saveFileTransfert($file=array()){
		$new_file_path = "/web/".$file['fichier_path'];
		$req = "INSERT INTO usr_files SET
                        id_file = ".$this->db->quote($file['id_file'], PDO::PARAM_STR).",
                        id_identifiant = ".$this->db->quote($file['id_identifiant'], PDO::PARAM_STR).",
                        id_prestataire_profil = ".$this->db->quote($file['id_prestataire_profil'], PDO::PARAM_STR).",
                        fichier_reference = ".$this->db->quote($file['fichier_reference'], PDO::PARAM_STR).",
                        fichier_nom = ".$this->db->quote($file['fichier_nom'], PDO::PARAM_STR).",
                        fichier_path = ".$this->db->quote($new_file_path, PDO::PARAM_STR).",
                        fichier_extension = ".$this->db->quote($file['fichier_extension'], PDO::PARAM_STR).",
                        fichier_description = ".$this->db->quote($file['fichier_description'], PDO::PARAM_STR).",
                        fichier_type = ".$this->db->quote($file['fichier_type'], PDO::PARAM_STR).",
			modifie_le = ".$this->db->quote($file['modifie_le'], PDO::PARAM_STR)."
			ON DUPLICATE KEY UPDATE fichier_reference = fichier_reference";
		  
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'yellow', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);
		
		//Tools::debugVar($req);
		if($this->db->exec($req)){
			
                        return  $this->db->lastInsertId();
        
		}
		  else{
			if(is_numeric($file['id_file'])){
			      $req = "UPDATE usr_files SET
			      modifie_le = NOW(),
			      fichier_extension = ".$this->db->quote($file['fichier_extension'], PDO::PARAM_STR)."
			      WHERE id_identifiant = ".$this->db->quote($file['id_identifiant'], PDO::PARAM_STR)."
			      AND id_file = ".$this->db->quote($file['id_file'], PDO::PARAM_STR);
			    
				if($debug)
					throw new myException($req);
				if(DEBUG)
					parent::$request[] =  array('color'=>'yellow', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);    
			      if($this->db->exec($req)){
				      
				      return  true;
		      
			      }
			}
		  }	
	}
}
