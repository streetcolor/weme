<?php
class DBMission extends DBConnect{
	
	//TRES IMPORTANT
	//Les méthodes save doivent être privées et son uniquement appelées via les méthodes make
	//Les tests de saisie s'effectuent OBLIGATOIREMENT depuis les méthodes make
	
        const COMPLEMENT_LANGUE     = 1;
        const COMPLEMENT_ETUDE      = 2;
        const COMPLEMENT_COMPETENCE = 3;
	const COMPLEMENT_POSTE      = 4;
	
	const MISSION_ATTENTE       = 1;
        const MISSION_EN_COURS      = 2;
        const MISSION_SUSPENDU      = 3;
	const MISSION_TERMINEE      = 4;

	private function saveMission($mission=array(), $complements=array(), $ignore=array(), $debug=false){
               
	       
		$req = "INSERT INTO usr_missions SET
                        modifie_le = NOW(),
                        id_identifiant = ".$this->db->quote($mission['id_identifiant'], PDO::PARAM_STR).",
                        id_client = ".$this->db->quote($mission['id_client'], PDO::PARAM_STR).",
                        id_categorie = ".$this->db->quote($mission['id_categorie'], PDO::PARAM_STR).",
			id_experience = ".$this->db->quote($mission['id_experience'], PDO::PARAM_STR).",
                        mi_reference = ".$this->db->quote($mission['mi_reference'], PDO::PARAM_STR).",
                        mi_titre = ".$this->db->quote($mission['mi_titre'], PDO::PARAM_STR).",
                        mi_description = ".$this->db->quote($mission['mi_description'], PDO::PARAM_STR).",
                        mi_tarif = ".$this->db->quote($mission['mi_tarif'], PDO::PARAM_STR).",
                        id_monnaie = ".$this->db->quote($mission['id_monnaie'], PDO::PARAM_STR).",
                        id_contrat = ".$this->db->quote($mission['id_contrat'], PDO::PARAM_STR).",
                        mi_date_reponse = ".$this->db->quote($mission['mi_date_reponse'], PDO::PARAM_STR).",
                        mi_republication = ".$this->db->quote($mission['mi_republication'], PDO::PARAM_STR).",
                        id_mission_statut = ".$this->db->quote($mission['id_mission_statut'], PDO::PARAM_STR).",
                        mi_reponse_mail = ".$this->db->quote($mission['mi_reponse_mail'], PDO::PARAM_STR).",
                        actif = ".$this->db->quote($mission['actif'], PDO::PARAM_STR).",
                        mi_debut = ".$this->db->quote($mission['mi_debut'], PDO::PARAM_STR).",
                        mi_int_fin = ".$this->db->quote($mission['mi_int_fin'], PDO::PARAM_STR).",
                        mi_string_fin = ".$this->db->quote($mission['mi_string_fin'], PDO::PARAM_STR).",
			lo_quartier = ".$this->db->quote($mission['lo_quartier'], PDO::PARAM_STR).",
			lo_ville = ".$this->db->quote($mission['lo_ville'], PDO::PARAM_STR).",
			lo_departement = ".$this->db->quote($mission['lo_departement'], PDO::PARAM_STR).",
			lo_departement_short = ".$this->db->quote($mission['lo_departement_short'], PDO::PARAM_STR).",
			lo_region = ".$this->db->quote($mission['lo_region'], PDO::PARAM_STR).",
			lo_region_short = ".$this->db->quote($mission['lo_region_short'], PDO::PARAM_STR).",
			lo_pays = ".$this->db->quote($mission['lo_pays'], PDO::PARAM_STR).",
			lo_pays_short = ".$this->db->quote($mission['lo_pays_short'], PDO::PARAM_STR);
                        
                if($mission['mi_int_fin'] && $mission['mi_string_fin'] && $mission['mi_debut'])
			$req .=",mi_fin = DATE_ADD('".$mission['mi_debut']."' , INTERVAL ".$mission['mi_int_fin']." ".$mission['mi_string_fin'].") ";
				
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'orange', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
		
		if($this->db->exec($req)){
			
                        $id_mission =  $this->db->lastInsertId();

			//Enregistrement des informations complementaires
			foreach($complements as $key=>$value){
			    
			    $entry = explode(',', str_replace('.', '', $value));
			   
			    $entries = array_unique($entry);
			    if(!empty($value)){
				switch ($key){
				    
				    case 'id_poste':
					$complement_type = self::COMPLEMENT_POSTE;
					break;
				    
				    case 'id_langue':
					$complement_type = self::COMPLEMENT_LANGUE;
					break;
				    
				    case 'id_competence':
					 
					$complement_type = self::COMPLEMENT_COMPETENCE;
					break;
				    
				    case 'id_etude':
					$complement_type = self::COMPLEMENT_ETUDE;
					break;
				    
				}
			    }
			    foreach($entries as $k=>$id_complement){
				
				if(is_numeric($id_complement) && isset($id_complement) && is_numeric($id_mission) && isset($id_mission)){

					$req = "INSERT INTO usr_missions_complements SET
					modifie_le = NOW(),
					id_mission = ".$this->db->quote($id_mission, PDO::PARAM_STR).",
					id_complement = ".$this->db->quote($id_complement, PDO::PARAM_STR).",
					complement = ".$this->db->quote($complement_type, PDO::PARAM_STR).",
					composante = ".$this->db->quote($id_mission."-".$id_complement."-".$complement_type, PDO::PARAM_STR)."
					ON DUPLICATE KEY UPDATE composante = composante";
				
					Tools::debugVar($req, false);
					if($debug)
						throw new myException($req);
					if(DEBUG)
						parent::$request[] =  array('color'=>'orange', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
					
					$this->db->exec($req);
					
				}else{
				    continue;
				}
			    }
			    
			}
			
			if(count($complements)==1){
				return $id_mission;
			}
			

		}

		return true;
	}
	private function updMission($mission=array(), $complements=array(), $ignore=array('page', 'action', 'id_langue', 'id_poste', 'id_competence', 'id_etude'), $debug=false){
             
		$req .= "UPDATE usr_missions SET " ;
		
		foreach($mission as $key=>$value){
			if(!in_array($key, $ignore)){
				
				if($value != "" && !is_array($value) ){
					
					if($key == 'mi_republication' && $mission['mi_republication']>0){
						$req .= "mi_republication = mi_republication+1, ";
					}
					elseif($key == 'mi_reponse_count' && $mission['mi_reponse_count']>0){
						$req .= "mi_reponse_count = mi_reponse_count+1, ";
					}
					elseif($key == "mi_debut" && !$value){
						$req.=" ".$key."= NOW() ,";
					}
					else{
						$req .= $key." = ".$this->db->quote($value, PDO::PARAM_STR)." , ";
					}
					
				}
			}
		}
		
		//on addition la date de fin au début
		if($mission['mi_int_fin'] && $mission['mi_string_fin'] && $mission['mi_debut'])
			$req .=" mi_fin = DATE_ADD('".Tools::shortConvertLanDateToDb($mission['mi_debut'])."' , INTERVAL ".$mission['mi_int_fin']." ".$mission['mi_string_fin'].") ,";
		
	
		if($mission['mi_republication'])
			$req .= " modifie_le = NOW() ,  cree_le = NOW() ";
		else
			$req .= " modifie_le = NOW() ";

		if(is_array($mission['id_mission']))
			$req .= "WHERE id_mission IN (".implode(',', $mission['id_mission']).") ";
		
		else
			$req .= "WHERE id_mission = ".$this->db->quote($mission['id_mission'], PDO::PARAM_STR);
			
		if(is_numeric($mission['id_mission_statut']))//On ne change pas le statut d'une mission qui est terminée !
			$req .= " AND id_mission_statut != ".$this->db->quote(self::MISSION_TERMINEE, PDO::PARAM_STR);

		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'orange', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
		
				
		if($this->db->exec($req)){
			//Tools::debugVar($complements, false, $mission['id_mission']);
			if(count($complements)){
				$id_mission =  $mission['id_mission'];
				 
				//Enregistrement des informations complementaires
				foreach($complements as $key=>$value){
					$entry = array();

					if(!is_array($value)){
						
						$entry = explode(',', str_replace('.', '', $value));
					}
					else{
						
						$entry = $value;

					}
					
			
					array_map(trim, $entry);			
					$entries = array_unique($entry);
				    
					if(!empty($value)){
					    switch ($key){
						
						case 'id_poste':
						    $complement_type = self::COMPLEMENT_POSTE;
						    $req = "DELETE FROM
							    usr_missions_complements WHERE
							    complement  = ".$this->db->quote( self::COMPLEMENT_POSTE, PDO::PARAM_STR)."
							    AND id_mission = ".$this->db->quote($mission['id_mission'], PDO::PARAM_STR);
					    
						    if($debug)
							    throw new myException($req);
						    if(DEBUG)
							    parent::$request[] =  array('color'=>'orange', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	

						    $this->db->exec($req);
							
						    break;
						
						case 'id_langue':
						    $complement_type = self::COMPLEMENT_LANGUE;
						    break;
						
						case 'id_competence':
						    $complement_type = self::COMPLEMENT_COMPETENCE;
						    break;
						
						case 'id_etude':
						    $complement_type = self::COMPLEMENT_ETUDE;
						    break;
						
					    }
					    
					}
				       
				     
					if(is_numeric($complement_type)){
															    
						foreach($entries as $k=>$id_complement){
							
							//si le complement existe déja dans les table adm_* on connait déja son id
							if(is_numeric($id_complement) && isset($id_complement) && is_numeric($id_mission) && isset($id_mission)){
								    $req = "INSERT INTO usr_missions_complements SET
									    modifie_le = NOW(),
									    id_mission = ".$this->db->quote($id_mission, PDO::PARAM_STR).",
									    id_complement = ".$this->db->quote($id_complement, PDO::PARAM_STR).",
									    complement = ".$this->db->quote($complement_type, PDO::PARAM_STR).",
									    composante = ".$this->db->quote(str_replace(' ', '', $id_mission."-".$id_complement."-".$complement_type), PDO::PARAM_STR)."
									    ON DUPLICATE KEY UPDATE composante = composante";
								
								    if($debug)
									    throw new myException($req);
								    if(DEBUG)
									    parent::$request[] =  array('color'=>'orange', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
								
							
								if($this->db->exec($req))
								    $lastId = $this->db->lastInsertId(); 
								
							
							}
							else{
								
								//si on ajoute un nouveau complement type adm_*
								if(!is_numeric($id_complement) && !empty($id_complement) && is_numeric($id_mission) && isset($id_mission)){
								    
									$DBadmin = new DBAdmin();
									switch ($complement_type){
							   
									   case self::COMPLEMENT_POSTE:
									       $complement = 'poste';
									       break;
									   
									   case self::COMPLEMENT_LANGUE:
									       $complement = 'langue';
									       break;
									   
									   case self::COMPLEMENT_COMPETENCE:
									       $complement = 'competence';
									       break;
									   
									   case self::COMPLEMENT_ETUDE:
									       $complement = 'etude';
									       break;
									   
								       }

								   
									if($id_complement = $DBadmin->saveComplement(array('table'=>$complement, 'entry'=>$id_complement))){
									 
									   $req = "INSERT INTO usr_missions_complements SET
										 modifie_le = NOW(),
										 id_mission = ".$this->db->quote($id_mission, PDO::PARAM_STR).",
										 id_complement = ".$this->db->quote($id_complement, PDO::PARAM_STR).",
										 complement = ".$this->db->quote($complement_type, PDO::PARAM_STR).",
										 composante = ".$this->db->quote(str_replace(' ', '', $id_mission."-".$id_complement."-".$complement_type), PDO::PARAM_STR)."
										 ON DUPLICATE KEY UPDATE composante = composante";
										 
										     if($debug)
											     throw new myException($req);
										     if(DEBUG)
											     parent::$request[] =  array('color'=>'orange', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
										
										 
										 if($this->db->exec($req))
											 return $this->db->lastInsertId(); 
										 
									}
							    
								    
								}
							}
							
						
						}
						continue;
					}
				    
				}

				if(!$lastId)
					return false;
				
				if(is_numeric($lastId) && $lastId>0)
					return $lastId;
					
				if(count($complements)==1){
					return $lastId;
				}
			}
			return true;
		}

		
	}
	private function rmvComplement($array = array(), $debug=false){
		
		$req .= "DELETE FROM
			usr_missions_complements WHERE
			id_mission_complement = ".$this->db->quote($array['id_mission_complement'], PDO::PARAM_STR)."
			AND id_mission = ".$this->db->quote($array['id_mission'], PDO::PARAM_STR);
	
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'orange', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
		
		if($this->db->exec($req))
		
			return true;
		
	}
	public function getListMissions($filtre = array(), $searchFiltre=array(), $limit=array(0,NOMBRE_PER_PAGE), $matching=false){
		//$matching indique que la methode doit trouver des id_mission dans toutes les tables adm_ sinon el renvoi false
		//petite securité pour evité l'envoie d'espaces blancs
		if(!is_array($searchFiltre['mots_cle']))
			$searchFiltre['mots_cle'] = trim($searchFiltre['mots_cle']);
		
		
	
		$missions       = array();
		$matchingResult = false;
		$statistique  = array(self::MISSION_ATTENTE=>0,self::MISSION_EN_COURS=>0,self::MISSION_SUSPENDU=>0,self::MISSION_TERMINEE=>0);
		$new_missions = 0;
		$counter = array(
				'COUNT' => 'SELECT count(DISTINCT(UM.id_mission)) AS total, ' ,
				'SELECT'=> 'SELECT DISTINCT(UM.id_mission), '
				);
		$req = "
			UM.id_identifiant, UM.id_client, UM.id_mission_statut, UM.id_categorie,UM.id_experience,
			UI.usr_email/*le mail du client bien sure*/,
			UCM.mg_nom, UCM.mg_prenom, UCM.mg_portable_indicatif, UCM.mg_portable, UCM.mg_fixe_indicatif, UCM.mg_fixe/*le coordonées du client emeteur*/,
			AMS.adm_mission_statut_".$_SESSION["langue"]." AS adm_statut,
			AC.adm_categorie_".$_SESSION["langue"]." AS adm_categorie,
			AE.adm_experience_".$_SESSION["langue"]." AS adm_experience,
			AT.adm_contrat_".$_SESSION["langue"]." AS adm_contrat,
			AM.adm_monnaie,
			UM.mi_reference, UM.mi_titre, UM.mi_description, UM.mi_tarif, UM.id_monnaie, UM.id_contrat,
			UM.mi_debut,
			UM.mi_fin, UM.mi_int_fin, UM.mi_string_fin, UM.mi_date_reponse, UM.mi_republication, UM.mi_reponse_mail,
			UM.lo_quartier, UM.lo_ville, UM.lo_code_postal, UM.lo_departement_short, UM.lo_departement, UM.lo_region_short,  UM.lo_region,  UM.lo_pays_short, UM.lo_pays,
			UM.mi_reponse_count, UM.modifie_le,
			UC.cl_entreprise
			FROM usr_missions AS UM
			LEFT OUTER JOIN usr_identifiants AS UI ON UI.id_identifiant=UM.id_identifiant
			LEFT OUTER JOIN usr_clients_managers AS UCM ON UCM.id_identifiant=UM.id_identifiant
			LEFT OUTER JOIN usr_clients AS UC ON UC.id_client=UCM.id_client
			LEFT OUTER JOIN adm_categories AS AC ON UM.id_categorie=AC.id_categorie
			LEFT OUTER JOIN adm_experiences AS AE ON UM.id_experience=AE.id_experience
			LEFT OUTER JOIN adm_contrats AS AT ON UM.id_contrat=AT.id_contrat
			LEFT OUTER JOIN adm_monnaies AS AM ON UM.id_monnaie=AM.id_monnaie
			LEFT OUTER JOIN adm_missions_statuts AS AMS ON UM.id_mission_statut=AMS.id_mission_statut
			WHERE UM.modifie_le ";
			
		foreach($filtre as $key=>$value){
				if(is_array($value)){
					if(count($value))
						$req .= " AND UM.".$key." IN ('".implode("','", $value)."')";
					else
						$req .= " AND UM.".$key." IN ('')";	
				}
				else
					$req .= " AND UM.".$key." = ".$this->db->quote($value, PDO::PARAM_STR);
		}	
		
		
		//Si on a des entrées dans la recherche
		if(is_array($searchFiltre) && count($searchFiltre)){
				
			if((count($searchFiltre['mots_cle']) && !empty($searchFiltre['mots_cle'])) || (count($searchFiltre['id_langue']) && !empty($searchFiltre['id_langue'])) ||(count($searchFiltre['id_poste']) && !empty($searchFiltre['id_poste'])) || (count($searchFiltre['id_competence']) && !empty($searchFiltre['id_competence']))){
				//Tools::debugVar($searchFiltre);
				/*ON VA BASCULER TOUTES NOS ENTREES RECU EN TABLEAU*/
				/*On sous entend que les valeur seront déparé par des virgules dans le moteur de recherche*/
				$keywords   = array();
				//Est ce que l'on a demandé une recherche sur mot_cle
				if(!is_array($searchFiltre['mots_cle']) &&  isset($searchFiltre['mots_cle'])){
					if(empty($searchFiltre['mots_cle']))
						$keywords = array();
					else
						$keywords = explode(',' , $searchFiltre['mots_cle']);
					
				}
				else
					$keywords= $searchFiltre['mots_cle'];
			
				$id_langues   = array();
				if(isset($searchFiltre['id_langue'])){
					if(!is_array($searchFiltre['id_langue']))
						$id_langues = explode(',' , $searchFiltre['id_langue']);
					else
						$id_langues= $searchFiltre['id_langue'];
				}
				$id_postes   = array();
				if(isset($searchFiltre['id_poste'])){
					if(!is_array($searchFiltre['id_poste']))
						$id_postes = explode(',' , $searchFiltre['id_poste']);
					else
						$id_postes= $searchFiltre['id_poste'];
				}
				$id_competences   = array();
				if(isset($searchFiltre['id_competence'])){
					if(!is_array($searchFiltre['id_competence']))
						$id_competences = explode(',' , $searchFiltre['id_competence']);
					else
						$id_competences= $searchFiltre['id_competence'];
				}	

				//On recupere les mission qui utilisent ces mots cle en competence ou poste ou langue ou formation ou experience
				if(count($keywords) || count($id_competences))
					$listCompetences = $this->getListCompetences(array(), $keywords, $id_competences);
				//Tools::debugVar($listCompetences, false, "Competences");
				if(count($keywords) || count($id_postes))
					$listPostes       = $this->getListPostes(array(), $keywords, $id_postes);
				//Tools::debugVar($listPostes, false, "Postes");
				if(count($keywords) || count($id_langues))
					$listLangues      = $this->getListLangues(array(), $keywords, $id_langues);
				//Tools::debugVar($listLangues, false, "Langues");
				$entryMatch      = array();
				
				$req .= "AND ( ";
				
				if(count($searchFiltre['mots_cle']) && !empty($searchFiltre['mots_cle'])){
					
					$string = addslashes(implode("* ", $keywords)); 
					$req  .=   "( MATCH (UM.mi_description, UM.mi_titre) AGAINST ('$string*' IN BOOLEAN MODE)";
					
			
					if(($listCompetences['count']>0 )|| ($listPostes['count']>0) ||($listLangues['count']>0)){
						$req .= " ) OR ";
					}
					else{
						
						$req .= " )  ";
					}
					
				}
				$competences = array();
				if($listCompetences['count']){
					
					foreach($listCompetences['competences'] as $key){
					
						$entryMatch[]=$key->id_mission;
						$competences[] = $key->id_mission;
					}
					$competences = array_unique($competences);
					//Tools::debugVar($competences, false, 'Competecnces id_mission');
				}
				
				$langues = array();
				if($listLangues['count']){
					
					foreach($listLangues['langues'] as $key){
					
						$entryMatch[]=$key->id_mission;
						$langues[] = $key->id_mission;
					}
					$langues = array_unique($langues);
					//Tools::debugVar($langues, false, 'langues id_mission');
				}
				
				$postes=array();
				if($listPostes['count']){
					
					foreach($listPostes['postes'] as $key){
					
						$entryMatch[]=$key->id_mission;
						$postes[] = $key->id_mission;
					}
					$postes = array_unique($postes);
					//Tools::debugVar($postes, false, 'postes id_mission');
				}
				
				
				
				if($matching){
					//Attention cette methode est categorique !
					//Elle ne gardera que les missions qui ont à la fois les competences et les langues demandés
					//Si une mission ne possède pas de langue elle ne resortira jamais
					//En cas de doute mieux vaut utiliser :  $implComplement = $competences;
					$implComplement = array_intersect($postes, $competences/*,$langues*/);//permet de definir les valeur qui sont presentes dans les deux tableaux
					//Tools::debugVar($implComplement, false, "matching Complements");
					//$implComplement = $competences;
					
					$matchingResult = true;
					
					if(count($searchFiltre['adm_competence']) || count($searchFiltre['adm_poste'])){
						
						$req  .=   "(";
					}
					
					if(count($searchFiltre['adm_competence'])){//Si on recupere des adm_competence dans getListMissionsMatchProfils
						
						$string = addslashes(implode("* ", $searchFiltre['adm_competence'])); 
						$req  .=   "  MATCH (UM.mi_description, UM.mi_titre) AGAINST ('$string*')/*On chereche les adm_competence du presta dans nos missions*/";
					}
					
					if(count($searchFiltre['adm_competence']) && count($searchFiltre['adm_poste'])){
						$req  .=   ' AND ';
					}
					
					if(count($searchFiltre['adm_poste'])){//Si on recupere des adm_competence dans getListMissionsMatchProfils
						$string = addslashes(implode("* ", $searchFiltre['adm_poste'])); 
						$req  .=   "  MATCH (UM.mi_description, UM.mi_titre) AGAINST ('$string*')/*On chereche les adm_poste du presta dans nos missions*/";
					}
					
					if(count($searchFiltre['adm_competence']) || count($searchFiltre['adm_poste'])){
						$req  .=   ')  ';
					}
				}
				else
					$implComplement = array_unique($entryMatch);
				
				
				//Tools::debugVar($implComplement, true, "ALl match");
				
				if(count($implComplement)>0){
					$matchingResult = true;
					if(count($searchFiltre['adm_competence']) || count($searchFiltre['adm_poste'])){
						$req .= "OR ";
					}
					$req .= "  (UM.id_mission IN (".implode(',',$implComplement)."))  ";			
				}
				
				
				elseif($matching)//si aucune entré trouvée on envoie le 1=1 pour eviter erreur sql et du coup afficher des missions...
					$req .= "AND 1 = 1";
				
				$req .= " ) ";
				
				
			}
			if(!empty($searchFiltre['mi_anciennete'])){
				
				$req .= "AND ( UM.modifie_le BETWEEN DATE_SUB( NOW( ) , INTERVAL ".$searchFiltre['mi_anciennete'].")  AND NOW( ) ) ";
				
			}
			if(!empty($searchFiltre['id_mission_statut'])){
				
				$req .= "AND ( UM.id_mission_statut = ".$searchFiltre['id_mission_statut']." ) ";
				
			}
			if(!empty($searchFiltre['mi_reponse_count'])){
				
				$req .= "AND ( UM.mi_reponse_count >0 ) ";
				
			}
			if(!empty($searchFiltre['mi_fin']) || !empty($searchFiltre['mi_debut']) ){
				
				
				if(Tools::verifDate($searchFiltre['mi_fin']) && Tools::verifDate($searchFiltre['mi_debut'])){
					
					$debut = Tools::shortConvertFrDateToDb($searchFiltre['mi_debut']);
					$fin   = Tools::shortConvertFrDateToDb($searchFiltre['mi_fin']);
					
					$d1 = new DateTime($debut);
					$d2 = new DateTime($fin);
					
					if($d1 < $d2){
						$req .= "AND ( /*Si la date est pas defini on retourne la date du jour sinon la définie*/ IF(DATE_FORMAT(UM.mi_debut, '%Y')>0, UM.mi_debut, CURDATE()) >= '$debut') AND ('$fin' >= UM.mi_fin)";
					}
				}
				elseif(Tools::verifDate($searchFiltre['mi_debut'])){
					$debut = Tools::shortConvertFrDateToDb($searchFiltre['mi_debut']);
					$req .= "AND ( /*Si la date est pas defini on retourne la date du jour sinon la définie*/ IF(DATE_FORMAT(UM.mi_debut, '%Y')>0, UM.mi_debut, CURDATE()) >= '$debut')";
				}
				elseif(Tools::verifDate($searchFiltre['mi_fin'])){
					$fin = Tools::shortConvertFrDateToDb($searchFiltre['mi_fin']);
					$req .= "AND (UM.mi_fin <= '$fin' )";
				}
				else{
					//nothing
				}
				
			}
			if(count($searchFiltre['lo_pays_short']) && !empty($searchFiltre['lo_pays_short'])){
				
				
				$listLoc['lo_departement_short']   = array();
				if(!is_array($searchFiltre['lo_departement_short'])){

					if(!empty($searchFiltre['lo_departement_short'])){
						
						$listLoc['lo_departement_short'] = explode(',' , $searchFiltre['lo_departement_short']);
					}
				}
				else{
					
					$listLoc['lo_departement_short'] = $searchFiltre['lo_departement_short'];
				}
				
				if(count($listLoc['lo_departement_short'])){
					$listLoc['lo_departement_short'] = array_unique($listLoc['lo_departement_short']);
				}
				
				$listLoc['lo_region_short']   = array();
				if(!is_array($searchFiltre['lo_region_short'])){
					if(!empty($searchFiltre['lo_region_short'])){
						$listLoc['lo_region_short'] = explode(',' , $searchFiltre['lo_region_short']);
					}
				}
				else{
					$listLoc['lo_region_short'] = $searchFiltre['lo_region_short'];
				}
				
				if(count($listLoc['lo_region_short'])){
					$listLoc['lo_region_short'] = array_unique($listLoc['lo_region_short']);
				}
				
				$listLoc['lo_pays_short']   = array();
				if(!is_array($searchFiltre['lo_pays_short'])){
					if(!empty($searchFiltre['lo_pays_short'])){
						$listLoc['lo_pays_short'] = explode(',' , $searchFiltre['lo_pays_short']);
					}
				}
				else{
					$listLoc['lo_pays_short'] = $searchFiltre['lo_pays_short'];
				}
				
				if(count($listLoc['lo_pays_short'])){
					$listLoc['lo_pays_short'] = array_unique($listLoc['lo_pays_short']);
				}
				
				if(count($listLoc)){
					$req .= "AND ("; 
					
					$i=0;
					foreach($listLoc as $k=>$v){
						$i++;
						if($v){
							
							if($implode = implode("','", $v))
								$req .= " UM.".$k."  IN ('".$implode."') ";
							else
								$req .= '1=1';
							if($i < count($listLoc)){
								$req .= " AND ";
							}
						}
						
					}
					
					
					$req .= " ) "; 
				}
				
				
				
			}
			if(isset($searchFiltre['id_mission']) && !empty($searchFiltre['id_mission'])){
				
				$req .= " AND ( UM.id_mission  IN (".$searchFiltre['id_mission'].")) ";			
			}
			
		}
		
		
		$req .= " ORDER BY UM.cree_le DESC  ";
	
		if(is_array($limit)){
			$limit = array(
					'COUNT' => '' ,
					'SELECT'=> "LIMIT ".implode(",", $limit)
					);
		}
		
		
		if($debug)
			throw new myException($counter['COUNT'].$req.$limit['COUNT']);
		if(DEBUG)
			parent::$request[] =  array('color'=>'orange', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$counter['COUNT'].$req.$limit['COUNT']);	
		
		
		//Le count avec la requete
		$resultats = $this->db->query($counter['COUNT'].$req.$limit['COUNT']);
		
		//si on un total superieur a zéro
		//on reexecute la requete afin de recuperer les elements
                if($count = $resultats->fetch(PDO::FETCH_OBJ)->total){
			
			if($debug)
				throw new myException($counter['SELECT'].$req.$limit['SELECT']);
			if(DEBUG)
				parent::$request[] =  array('color'=>'orange', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$counter['SELECT'].$req.$limit['SELECT']);
						
			$resultats = $this->db->query($counter['SELECT'].$req.$limit['SELECT']);

			while ($mission = $resultats->fetch(PDO::FETCH_ASSOC)){
				switch($mission['id_mission_statut']){
					case self::MISSION_ATTENTE:
						$statistique[self::MISSION_ATTENTE] += 1;
						break;
					case self::MISSION_EN_COURS:
						$statistique[self::MISSION_EN_COURS] += 1;
						break;
					case self::MISSION_SUSPENDU:
						$statistique[self::MISSION_SUSPENDU]+= 1;
						break;
					case self::MISSION_TERMINEE:
						$statistique[self::MISSION_TERMINEE] += 1;
						break;
				}
				
				//On recupere maintenant la date actuelle
				$var_day        = date('Y-m-d H:m:s');
				
				//Difference
				$d1             = new DateTime($var_day);
				$d2             = new DateTime($mission['modifie_le']);
				$difDay         = $d1->diff($d2)->format('%d');
				
				if($difDay<NOMBRE_DAY_NEW){
					$new_missions +=1;
				}
				$missions[] = new Mission($mission);	
			}
			
			$resultats->closeCursor();
			
                }
		return array('count'=>$count, 'missions'=>$missions, 'statistiques'=>$statistique, 'new_missions'=>$new_missions, 'matchingResult'=>$matchingResult);
		
	}
	public function getListPostes($filtre = array(), $search=array(), $id_postes=array()){
		$postes = array();
		$counter = array(
				'COUNT' => 'SELECT count(id_mission_complement) AS total, ' ,
				'SELECT'=> 'SELECT id_mission_complement, '
				);
		$req = " id_mission, id_complement,  COALESCE(AP.adm_poste_fr, AP.adm_poste_en, AP.adm_poste_es) AS adm_poste
			FROM usr_missions_complements AS UMC
			INNER JOIN adm_postes AS AP ON UMC.id_complement = AP.id_poste
			WHERE UMC.complement =   ".self::COMPLEMENT_POSTE;
			
		foreach($filtre as $key=>$value){
			if(is_array($value)){
				if(count($value))
					$req .= " AND ".$key." IN (".implode(',', $value).")";
				else
					$req .= " AND ".$key." IN ('".implode(',', $value)."')";	
			}
			else{
				$req .= " AND ".$key." = ".$this->db->quote($value, PDO::PARAM_STR);
			}
		}	
		
		if(count($search) || count($id_postes))
			$req .=" AND ( ";

		if(count($search)){
			$string = addslashes(implode("* ", $search)); 
			$req .= "  MATCH (AP.adm_poste_".$_SESSION["langue"].") AGAINST ('$string*' IN BOOLEAN MODE)";
		}

		if(count($search) && count($id_postes))
			$req .="OR ";
		
		
		if(count($id_postes))
			$req .= " AP.id_poste IN (".implode(',', $id_postes).")";
		
		if(count($search) || count($id_postes))
			$req .=" ) ";
		
		if($debug)
			throw new myException($counter['COUNT'].$req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'orange', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$counter['COUNT'].$req);
		
		$resultats = $this->db->query($counter['COUNT'].$req);
		
		if($count = $resultats->fetch(PDO::FETCH_OBJ)->total){
			if($justCount)
				return array('count'=>$count, 'postes'=>$postes);
			if($debug)
				throw new myException($counter['SELECT'].$req);
			if(DEBUG)
				parent::$request[] =  array('color'=>'orange', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$counter['SELECT'].$req);
			
			$resultats = $this->db->query($counter['SELECT'].$req);

			while ($poste = $resultats->fetch(PDO::FETCH_OBJ))
				 $postes[$poste->id_mission_complement] = $poste;	
			
			$resultats->closeCursor();
		}
		
		return array('count'=>$count, 'postes'=>$postes);
		
	}
	public function getListCompetences($filtre = array(), $search=array(), $id_competences=array()){
		$competences = array();
		$counter = array(
				'COUNT' => 'SELECT count(id_mission_complement) AS total, ' ,
				'SELECT'=> 'SELECT id_mission_complement, '
				);
		$req = "id_mission, id_complement, COALESCE(AC.adm_competence_fr, AC.adm_competence_en, AC.adm_competence_es) AS adm_competence
			FROM usr_missions_complements AS UMC
			INNER JOIN adm_competences AS AC ON UMC.id_complement = AC.id_competence
			WHERE UMC.complement =   ".self::COMPLEMENT_COMPETENCE;
			
		foreach($filtre as $key=>$value){
			if(is_array($value)){
				if(count($value))
					$req .= " AND ".$key." IN (".implode(',', $value).")";
				else
					$req .= " AND ".$key." IN ('".implode(',', $value)."')";	
			}
			else{
				$req .= " AND ".$key." = ".$this->db->quote($value, PDO::PARAM_STR);
			}
		}	
		
		if(count($search) || count($id_competences))
			$req .=" AND ( ";
		
		if(count($search)){
			$string = addslashes(implode("* ", $search)); 
			$req .= "  MATCH (AC.adm_competence_".$_SESSION["langue"].") AGAINST ('$string*' IN BOOLEAN MODE)";
		}
		if(count($search) && count($id_competences))
			$req .="OR ";
		
		if(count($id_competences))
			$req .= " AC.id_competence IN (".implode(',', $id_competences).")";
		
		if(count($search) || count($id_competences))
			$req .=" ) ";
		
		if($debug)
			throw new myException($counter['COUNT'].$req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'orange', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$counter['COUNT'].$req);
		
		$resultats = $this->db->query($counter['COUNT'].$req);
		
		if($count = $resultats->fetch(PDO::FETCH_OBJ)->total){
			if($justCount)
				return array('count'=>$count, 'competences'=>$competences);
			if($debug)
				throw new myException($counter['SELECT'].$req);
			if(DEBUG)
				parent::$request[] =  array('color'=>'orange', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$counter['SELECT'].$req);
			
			$resultats = $this->db->query($counter['SELECT'].$req);

			while ($competence = $resultats->fetch(PDO::FETCH_OBJ))
				 $competences[$competence->id_mission_complement] = $competence;	
			
			$resultats->closeCursor();
		}
		
		return array('count'=>$count, 'competences'=>$competences);
		
	}
	public function getListLangues($filtre = array(), $search=array(), $id_langues=array()){
	
		
		$langues = array();
		$counter = array(
				'COUNT' => 'SELECT count(id_mission_complement) AS total, ' ,
				'SELECT'=> 'SELECT id_mission_complement, '
				);
		$req = " id_mission, id_complement, COALESCE(AL.adm_langue_fr, AL.adm_langue_en, AL.adm_langue_es)  AS adm_langue
			FROM usr_missions_complements AS UMC
			INNER JOIN adm_langues AS AL ON UMC.id_complement = AL.id_langue
			WHERE UMC.complement =   ".self::COMPLEMENT_LANGUE;
			
		foreach($filtre as $key=>$value){
			if(is_array($value)){
				if(count($value))
					$req .= " AND ".$key." IN (".implode(',', $value).")";
				else
					$req .= " AND ".$key." IN ('".implode(',', $value)."')";	
			}
			else{
				$req .= " AND ".$key." = ".$this->db->quote($value, PDO::PARAM_STR);
			}
		}		
		
		if(count($search) || count($id_langues))
			$req .=" AND ( ";
	
		if(count($search)){
			$string = addslashes(implode("* ", $search)); 
			$req .= "  MATCH (AL.adm_langue_".$_SESSION["langue"].") AGAINST ('$string*' IN BOOLEAN MODE)";
		}
		if(count($search) && count($id_langues))
			$req .=" OR ";
		
		if(count($id_langues))
			$req .= " AL.id_langue IN (".implode(',', $id_langues).")";
		
		if(count($search) || count($id_langues))
			$req .=" ) ";
			
		if($debug)
			throw new myException($counter['COUNT'].$req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'orange', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$counter['COUNT'].$req);
		
		$resultats = $this->db->query($counter['COUNT'].$req);
		
		if($count = $resultats->fetch(PDO::FETCH_OBJ)->total){
			if($justCount)
				return array('count'=>$count, 'langues'=>$langues);
			if($debug)
				throw new myException($counter['SELECT'].$req);
			if(DEBUG)
				parent::$request[] =  array('color'=>'orange', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$counter['SELECT'].$req);
			
			$resultats = $this->db->query($counter['SELECT'].$req);

			while ($langue = $resultats->fetch(PDO::FETCH_OBJ))
				 $langues[] = $langue;	
			
			$resultats->closeCursor();
		}
		return array('count'=>$count, 'langues'=>$langues);
		
	}
	public function getListEtudes($filtre = array(), $search=array(), $id_etudes=array()){
		$etudes = array();
		$counter = array(
				'COUNT' => 'SELECT count(id_mission_complement) AS total, ' ,
				'SELECT'=> 'SELECT id_mission_complement, '
				);
		$req = "id_mission, id_complement, COALESCE(AE.adm_etude_fr, AE.adm_etude_en, AE.adm_etude_es)  AS adm_etude
			FROM usr_missions_complements AS UMC
			INNER JOIN adm_etudes AS AE ON UMC.id_complement = AE.id_etude
			WHERE UMC.complement =   ".self::COMPLEMENT_ETUDE;
			
		foreach($filtre as $key=>$value){
			if(is_array($value) && count($value))
				$req .= " AND ".$key." IN ('".implode("','", $value)."')";
			else
				$req .= " AND ".$key." = ".$this->db->quote($value, PDO::PARAM_STR);
		}		
		
		if(count($search) || count($id_etudes))
			$req .=" AND ( ";
		
		if(count($search)){
			$string = implode("* ", $search); 
			$req .= "  MATCH (AE.adm_etude_".$_SESSION["langue"].") AGAINST ('$string*' IN BOOLEAN MODE)";
		}

		
		
		if(count($search) && count($id_etudes))
			$req .="OR ";
		
		if(count($id_etudes))
			$req .= " AE.id_etude IN (".implode(',', $id_etudes).")";
		
		if(count($search) || count($id_etudes))
			$req .=" ) ";
		
		if($debug)
			throw new myException($counter['COUNT'].$req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'orange', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$counter['COUNT'].$req);
		
		$resultats = $this->db->query($counter['COUNT'].$req);
		
		if($count = $resultats->fetch(PDO::FETCH_OBJ)->total){
			if($justCount)
				return array('count'=>$count, 'etudes'=>$etudes);
			if($debug)
				throw new myException($counter['SELECT'].$req);
			if(DEBUG)
				parent::$request[] =  array('color'=>'orange', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$counter['SELECT'].$req);
			
			$resultats = $this->db->query($counter['SELECT'].$req);

			while ($etude = $resultats->fetch(PDO::FETCH_OBJ))
				 $etudes[$etude->id_mission_complement] = $etude;	
		
			$resultats->closeCursor();
		}
		
		return array('count'=>$count, 'etudes'=>$etudes);
		
	}
	//Methode permettant de recuperer toutes les missions qui matchent pour une loste de profil donnée array()
	public function getListMissionsMatchProfils($id_profils=array()){
		//recuperation de tous les poste recherché pour les profils
		$DBmembre = new DBMembre();
		$listCompetences       = array();
		$listPostes            = array();
		$listKeywordCompetences = array();
		$listKeywordPostes     = array();
		$listLangues           = array();
		$listLocalites         = array();
		
		if(count($id_profils)>0){

			$getListCompetences = $DBmembre->getListCvCompetences(array('id_prestataire_profil'=>$id_profils));
			if($getListCompetences['count']>0){
				foreach($getListCompetences['competences'] as $c){
					$listCompetences[]        = $c->id_complement;
					$listKeywordCompetences[] =   $c->adm_competence;
				}
			}
			//Tools::debugVar($listCompetences, false, 'Competnce');
			$getListPostes = $DBmembre->getListCvPostes(array('id_prestataire_profil'=>$id_profils));
			if($getListPostes['count']>0){
				foreach($getListPostes['postes'] as $p){
					$listPostes[]        = $p->id_complement;
					$listKeywordPostes[] =   $p->adm_poste;
				}
			}
			//Tools::debugVar($listPostes, false, 'poste');
			$getListLangues = $DBmembre->getListCvLangues(array('id_prestataire_profil'=>$id_profils));
			if($getListLangues['count']>0){
				foreach($getListLangues['langues'] as $l)
					$listLangues[] = $l->id_complement;
			}
			//Tools::debugVar($listLangues, false, 'langue');
			$getListLocalites = $DBmembre->getListCvLocalites(array('id_prestataire_profil'=>$id_profils));
			$listLocalites['lo_pays_short'] = array();
			$listLocalites['lo_region_short'] = array();
			$listLocalites['lo_departement_short'] = array();
			if($getListLocalites->count>0){
				foreach($getListLocalites->localites as $l){
					$listLocalites['lo_pays_short'][]        = $l->getLocalite()->lo_pays_short;
					$listLocalites['lo_region_short'][]      = $l->getLocalite()->lo_region_short;
					$listLocalites['lo_departement_short'][] = $l->getLocalite()->lo_departement_short;
				}
			}
			return $this->getListMissions(
								array(
								      'id_mission_statut'=>DBMission::MISSION_EN_COURS
								),
								array(
									  'id_langue'=>$listLangues,
									  'id_poste'=>$listPostes,
									  'id_competence'=>$listCompetences,
									  'adm_competence' =>$listKeywordCompetences,
									  'adm_poste' =>$listKeywordPostes
									  /*'lo_pays_short'=>array_filter($listLocalites['lo_pays_short']),
									  'lo_region_short'=>array_filter($listLocalites['lo_region_short']),
									  'lo_departement_short'=>array_filter($listLocalites['lo_departement_short'])*/
								),
								false,
								true
							);
		}
		else{
			if(!DEBUG)
				return 'Récuperation des misisons impossible !';
		
		}
	}
	//Methode appelée pour procéder à l'ajout d'une mission
	public function makeMission($array=array()){
	
		// I. On commence par hydrater notre tableau (cast des variable int, string...)
		$mission = array(
				'id_client'=>         $array["id_client"],
				'id_identifiant'=>    $array["id_identifiant"],
                                'id_categorie'=>      $array["id_categorie"],
				'id_experience'=>     $array["id_experience"],
				'mi_reference'=>      Tools::randomString(),
				'mi_titre'=>          $array["mi_titre"],
				'mi_description'=>    $array["mi_description"],
				'mi_tarif'=>          $array["mi_tarif"],
				'id_monnaie'=>        $array["id_monnaie"],
				'id_contrat'=>        $array["id_contrat"],
				'mi_debut'=>          $array["mi_debut"],
				'mi_fin'=>            $array["mi_fin"],
				'mi_int_fin'=>        $array["mi_int_fin"],
				'mi_string_fin'=>     $array["mi_string_fin"],
				'mi_date_reponse'=>   $array["mi_date_reponse"],
				'mi_republication'=>  0,
				'mi_reponse_mail'=>   $array["mi_reponse_mail"],
				'id_mission_statut'=> self::MISSION_ATTENTE,
				'actif'=>             1,
				
				'lo_quartier'=>           $array["lo_quartier"],
				'lo_ville'=>              $array["lo_ville"],
				'lo_departement'=>        $array["lo_departement"],
				'lo_departement_short'=>  $array["lo_departement_short"],
				'lo_region'=>             $array["lo_region"],
				'lo_region_short'=>       $array["lo_region_short"],
				'lo_pays'=>               $array["lo_pays"],
				'lo_pays_short'=>         $array["lo_pays_short"]
			);
		
                $complements = array(
                            'id_poste'=>              $array["id_poste"],
                            'id_competence'=>         $array["id_competence"],
                            'id_langue'=>             $array["id_langue"],
                            'id_etude'=>              $array["id_etude"]
                );
		   
                // II. On effectue les traitements de formulaire avant de sauvegarder dans la base de donnée
		if(!isset($mission['mi_titre']) || empty($mission['mi_titre']))
			$error['mi_titre'] = 'Titre obligatoire';
			
		if(!isset($mission['mi_description']) || empty($mission['mi_description']))
			$error['mi_description'] = 'Description obligatoire';
		
		if(!isset($mission['mi_description']) || empty($mission['mi_description']))
			$error['mi_description'] = 'Description obligatoire';
		
		if(!isset($mission['id_contrat']) || empty($mission['id_contrat']) || !is_numeric($mission['id_contrat']))
			$error['id_contrat'] = 'Type de contrat';
		
		if(!isset($mission['mi_tarif']) || empty($mission['mi_tarif']) || !is_numeric($mission['mi_tarif']))
			$error['mi_tarif'] = 'Tarif invalide';
			
		if(!isset($mission['id_monnaie']) || empty($mission['id_monnaie']) || !is_numeric($mission['id_monnaie']))
			$error['id_monnaie'] = 'Devise invalide';
		
		if(!Tools::verifDate($mission["mi_debut"]))
			$error['mi_debut'] = 'Date de debut invalide';
		else
			$mission['mi_debut'] = Tools::shortConvertLanDateToDb($mission['mi_debut']);
		
		
		if(!empty($mission["mi_date_reponse"]) && !Tools::verifDate($mission["mi_date_reponse"]))
			$error['mi_date_reponse'] = 'Date de reponse invalide';
		else
			$mission['mi_date_reponse'] = Tools::shortConvertLanDateToDb($mission['mi_date_reponse']);
		
		if(!isset($mission['mi_int_fin']) || empty($mission['mi_int_fin']) || !is_numeric($mission['mi_int_fin']))
			$error['mi_int_fin'] = 'Date de fib invalide';
		
		if(!isset($mission['id_experience']) || empty($mission['id_experience']) || !is_numeric($mission['id_experience']))
			$error['id_experience'] = 'Experience invalide';
		
		
		if(count($error))
			return $error;
		
              
                // III. On enregistre dans la base de donnée
		if($id_mission = $this->saveMission($mission, $complements)){
			
			// IV. On fait les traitements 	adequats envoie de mails et generation des messages de retour
			return $id_mission; 
			
		}
	
	}
	
	//Methode appelée pour procéder à l'édition d'une mission
	public function editMission($array=array(), $republication = false){
	
		// I. On commence par hydrater notre tableau (cast des variable int, string...)
		$mission = array(
				'id_mission'=>        $array["id_mission"],
				'id_identifiant'=>    $array["id_identifiant"],
				'id_categorie'=>      $array["id_categorie"],
				'id_experience'=>     $array["id_experience"],
				'mi_titre'=>          $array["mi_titre"],
				'mi_description'=>    $array["mi_description"],
				'mi_tarif'=>          $array["mi_tarif"],
				'id_monnaie'=>        $array["id_monnaie"],
				'id_contrat'=>        $array["id_contrat"],
				'mi_debut'=>          $array["mi_debut"],
				'mi_fin'=>            $array["mi_fin"],
				'mi_int_fin'=>        $array["mi_int_fin"],
				'mi_string_fin'=>     $array["mi_string_fin"],
				'mi_date_reponse'=>   $array["mi_date_reponse"],
				'mi_republication'=>  $array["mi_republication"],
				'mi_reponse_mail'=>   $array["mi_reponse_mail"],
				'id_mission_statut'=> self::MISSION_ATTENTE,
				'actif'=>             1,
				
				'lo_quartier'=>           $array["lo_quartier"],
				'lo_ville'=>              $array["lo_ville"],
				'lo_departement'=>        $array["lo_departement"],
				'lo_departement_short'=>  $array["lo_departement_short"],
				'lo_region'=>             $array["lo_region"],
				'lo_region_short'=>       $array["lo_region_short"],
				'lo_pays'=>               $array["lo_pays"],
				'lo_pays_short'=>         $array["lo_pays_short"]
			);
		
		$complements = array(
			    'id_poste'=>              $array["id_poste"],
			    'id_competence'=>         $array["id_competence"],
			    'id_langue'=>             $array["id_langue"],
			    'id_etude'=>              $array["id_etude"]
		);
		
		if($array['page'] == 'edit_mission'){
			
			unset($complements);	
		}
		// II. On effectue les traitements de formulaire avant de sauvegarder dans la base de donnée
		if(count($complements)<1 && !$republication && !isset($array['mi_reponse_count']) && !isset($array['id_mission_statut'])){
			//Si on traite des complements
			//si on fait juste une republication
			//si on fait une uodate deu competur de reponse
			//si on fait fait un changement de statut
			//ON NE PASSE PAS PAR CETTE PARTIE
		
			if(!isset($mission['mi_titre']) || empty($mission['mi_titre']))
				$error['mi_titre'] = 'Titre obligatoire';
				
			if(!isset($mission['mi_description']) || empty($mission['mi_description']))
				$error['mi_description'] = 'Description obligatoire';
			
			if(!isset($mission['mi_description']) || empty($mission['mi_description']))
				$error['mi_description'] = 'Description obligatoire';
			
			if(!isset($mission['id_contrat']) || empty($mission['id_contrat']) || !is_numeric($mission['id_contrat']))
				$error['id_contrat'] = 'Type de contrat';
			
			if(!isset($mission['mi_tarif']) || empty($mission['mi_tarif']) || !is_numeric($mission['mi_tarif']))
				$error['mi_tarif'] = 'Tarif invalide';
				
			if(!isset($mission['id_monnaie']) || empty($mission['id_monnaie']) || !is_numeric($mission['id_monnaie']))
				$error['id_monnaie'] = 'Devise invalide';
			
			if(!Tools::verifDate($mission["mi_debut"]))
				$error['mi_debut'] = 'Date de debut invalide';
			else
				$mission['mi_debut'] = Tools::shortConvertLanDateToDb($mission['mi_debut']);
			
			
			if(!empty($mission["mi_date_reponse"]) && !Tools::verifDate($mission["mi_date_reponse"]))
				$error['mi_date_reponse'] = 'Date de reponse invalide';
			else
				$mission['mi_date_reponse'] = Tools::shortConvertLanDateToDb($mission['mi_date_reponse']);
			
			if(!isset($mission['mi_int_fin']) || empty($mission['mi_int_fin']) || !is_numeric($mission['mi_int_fin']))
				$error['mi_int_fin'] = 'Date de fib invalide';
			
			if(!isset($mission['id_experience']) || empty($mission['id_experience']) || !is_numeric($mission['id_experience']))
				$error['id_experience'] = 'Experience invalide';
			
			
			if(count($error))
				return $error;
		}
		//Cette condition intervient uniquement lors de la sauvegarde d'une reponse
		//On souhaite juste incrementer le compteur de reponse mission
		//Donc on a pas besoin du gros tableau $mission...On efface et on change l"affectation
		//Au passage on fait pareil avec les complements
		if(isset($array['mi_reponse_count'])){
			unset($mission);
			unset($complements);
			$mission['mi_reponse_count'] = $array['mi_reponse_count'];
			$mission['id_mission']       = $array['id_mission'];
			$mission['id_identifiant']   = $array['id_identifiant'];
		}
		//Cette condition intervient uniquement lors de la republication d'une mission
		//On souhaite juste incrementer le compteur des republication
		//Donc on a pas besoin du gros tableau $mission...On efface et on change l"affectation
		//Au passage on fait pareil avec les complements
		if(isset($array['mi_republication'])){
			unset($mission);
			unset($complements);
			$mission['mi_republication'] = 1;
			$mission['id_mission']       = $array['id_mission'];
			$mission['id_identifiant']   = $array['id_identifiant'];
		}
		//Cette condition intervient uniquement lors de la mise a jour des notifcation email d'une mission
		//On souhaite juste activer ou desactiver la notification
		//Donc on a pas besoin du gros tableau $mission...On efface et on change l"affectation
		//Au passage on fait pareil avec les complements
		if(isset($array['mi_reponse_mail']) && !isset($array['mi_titre'])){
			unset($mission);
			unset($complements);
			$mission['mi_reponse_mail']  = $array['mi_reponse_mail'];
			$mission['id_mission']       = $array['id_mission'];
			$mission['id_identifiant']   = $array['id_identifiant'];
		}
		//Cette condition intervient uniquement lors du changement de statut de la mission
		//On souhaite juste changer le statut
		//Donc on a pas besoin du gros tableau $mission...On efface et on change l"affectation
		//Au passage on fait pareil avec les complements

		if(isset($array['id_mission_statut'])){
			unset($mission);
			unset($complements);
			$mission['id_mission_statut'] = $array['id_mission_statut'];
			$mission['id_mission']        = $array['id_mission'];
			$mission['id_identifiant']    = $array['id_identifiant'];
			
			if(!is_array($mission['id_mission']) || !count($mission['id_mission']))
			       $error['id_mission'] = 'Impossible d\'acceder à la mission demandée !';
	
			if(!isset($mission['id_mission_statut']) || empty($mission['id_mission_statut']))
			       $error['id_mission_statut'] = 'Merci de seectionner votre statut';
			
			if(count($error)>0)//on a des erreurs on stop ici
				return $error;
			
		}
	
		// III. On enregistre dans la base de donnée
		if(isset($array["rmv_id_mission_complement"]) && is_numeric($array["rmv_id_mission_complement"]) && isset($array["id_mission"])){
			return $this->rmvComplement(array('id_mission_complement'=>$array["rmv_id_mission_complement"], 'id_mission'=>$array["id_mission"]));
		}
		else{
			if($id_mission = $this->updMission($mission, $complements)){
				
				// IV. On fait les traitements 	adequats envoie de mails et generation des messages de retour
			
				if($array['action']=='changeStatus'){
					
					$messageSend['mail_author']    = MAIL_FROM;
					$messageSend['message']        = file_get_contents(_PATH_BACK_.'web/mails/validate_mission.html');
					
					
					foreach($array['id_mission'] as $id_mission){
						$getMission = $this->getListMissions(array('id_mission'=>$id_mission));
						$messageSend['sujet']  = 'Validation de votre mission : '. $getMission['missions'][0]->getTitre();
						$messageSend['email'] = $getMission['missions'][0]->getEmail();
						if(Tools::sendEmail($messageSend)){
							//On balance les mails aux profils qui matchent !
							if($getMission['missions'][0]->getStatut()->id_statut==self::MISSION_EN_COURS){
								$alreadySend = array();
								$DBmembre           = new DBMembre();
								$getMatchingProfils = $DBmembre->getListProfilsMatchMissions(array($id_mission));
								foreach($getMatchingProfils['profils'] as $profil){

									if($profil->getNotificationEmail()){
										$NotifmessageSend['mail_author']    = MAIL_FROM;
										
										$NotifmessageSend['message']        = 'Bonjour <br><br>Cette mission peut vous intéresser<br><br>'.$getMission['missions'][0]->getTitre().'<br><br>'.$getMission['missions'][0]->getDescription();
										$NotifmessageSend['sujet']          = 'Cette mission peux vous interresser';
										$NotifmessageSend['email']          = $profil->getEmail();
										if(!in_array($profil->getEmail(), $alreadySend)){
											Tools::sendEmail($NotifmessageSend);
											$alreadySend[] = $profil->getEmail();//evitons les doublons
										}
									}
									
								}
								$alreadySend =  array();
							}
						}
					}
					
					
				}
				return $id_mission; 
				
			}
		}
	
	}
	
	//Methode appelée pour dupliquer une mission
	public function duplicateMission($array=array()){
	
		//I on recupere notre mission
		$reqGet = "SELECT  * FROM usr_missions WHERE id_mission = ".$this->db->quote($array['id_mission'], PDO::PARAM_STR). "AND id_identifiant = ".$this->db->quote($array['id_identifiant'], PDO::PARAM_STR) ;
		$resultats = $this->db->query($reqGet);
		$mission = $resultats->fetch(PDO::FETCH_ASSOC);
		
		$getListCompetences = $this->getListCompetences(array('id_mission'=>$array['id_mission']));
		$listLangues = array();
		if($getListCompetences['count']){
			foreach($getListCompetences['competences'] as $c){
				$listCompetences[] = $c->id_complement;
			}
		}
		//Tools::debugVar($listCompetences, false, 'Competences');
		
		$getListPostes = $this->getListPostes(array('id_mission'=>$array['id_mission']));
		$listPostes = array();
		if($getListPostes['count']>0){
			foreach($getListPostes['postes'] as $p){
				$listPostes[] = $p->id_complement;
			}
		}
		//Tools::debugVar($listPostes, false, 'postes');

		$getListLangues = $this->getListLangues(array('id_mission'=>$array['id_mission']));
		$listLangues = array();
		if($getListLangues['count']>0){
			foreach($getListLangues['langues'] as $l){
				$listLangues[] = $l->id_complement;
			}
		}
		//Tools::debugVar($listLangues, false, 'langue');
		
		$getListEtudes = $this->getListEtudes(array('id_mission'=>$array['id_mission']));
		
		$listEtudes = array();
		if($getListEtudes['count']>0){
		
			foreach($getListEtudes['etudes'] as $e){
				$listEtudes[] = $e->id_complement;
			}
		}

		//Tools::debugVar($listEtudes, true, 'langue');
		
		$complements = array(
			'id_poste'=>$listPostes,
			'id_etude'=>$listEtudes,
			'id_langue'=>$listLangues,
			'id_competence'=>$listCompetences
		);
		
		//Tools::debugVar($complements);
		//II un peu de ménage s'impose
		unset($mission['id_mission']);
		unset($mission['mi_reponse_count']);
		unset($mission['cree_le']);
		
		$mission["mi_reference"] = Tools::randomString();
		
		//on peut meme modifier certaine entrées (mission statut par exemple)
		
		//III On immplode nos clé et nos valeur (simple et efficace)
		$keys   = implode(',', array_keys($mission));
		
		
		$mission["mi_description"] = addslashes($mission["mi_description"]);
		$values = '"'.implode('","', array_values($mission)).'"';
		
		
		//IV on enregistre notre nouvelle ligne
		$reqSave = "INSERT INTO usr_missions ({$keys}) VALUES ({$values})";

		if($this->db->exec($reqSave)){
		
			$id_mission =  $this->db->lastInsertId();
			if($this->updMission(array('id_mission'=>$id_mission), $complements)){
				return $id_mission;
			}
			
		}
	}
	
	//Methode appelée pour procéder à l'ajout d'une mission
	public function makeMissionTransfert($array=array()){
	
		// I. On commence par hydrater notre tableau (cast des variable int, string...)
		$mission = array(
				'id_mission'=>        $array["id_mission"],
				'id_client'=>         $array["id_client"],
				'id_identifiant'=>    $array["id_identifiant"],
                                'id_categorie'=>      $array["id_categorie"],
				'id_experience'=>     $array["id_experience"],
				'mi_reference'=>      $array["mi_reference"],
				'mi_titre'=>          $array["mi_titre"],
				'mi_description'=>    $array["mi_description"],
				'mi_tarif'=>          $array["mi_tarif"],
				'id_monnaie'=>        $array["id_monnaie"],
				'id_contrat'=>        $array["id_contrat"],
				'mi_debut'=>          $array["mi_debut"],
				'mi_fin'=>            $array["mi_fin"],
				'mi_int_fin'=>        $array["mi_int_fin"],
				'mi_string_fin'=>     $array["mi_string_fin"],
				'mi_date_reponse'=>   $array["mi_date_reponse"],
				'mi_republication'=>  $array["mi_republication"],
				'mi_reponse_mail'=>   $array["mi_reponse_mail"],
				'id_mission_statut'=> $array["id_mission_statut"],
				'actif'=>             1,
				
				'lo_quartier'=>           $array["lo_quartier"],
				'lo_ville'=>              $array["lo_ville"],
				'lo_departement'=>        $array["lo_departement"],
				'lo_departement_short'=>  $array["lo_departement_short"],
				'lo_region'=>             $array["lo_region"],
				'lo_region_short'=>       $array["lo_region_short"],
				'lo_pays'=>               $array["lo_pays"],
				'lo_pays_short'=>         $array["lo_pays_short"]
			);
		
                $complements = array(
                            'id_poste'=>              $array["id_poste"],
                            'id_competence'=>         $array["id_competence"],
                            'id_langue'=>             $array["id_langue"],
                            'id_etude'=>              $array["id_etude"]
                );
		   
                // II. On effectue les traitements de formulaire avant de sauvegarder dans la base de donnée
		/*if(!isset($mission['mi_titre']) || empty($mission['mi_titre']))
			$error['mi_titre'] = 'Titre obligatoire';
			
		if(!isset($mission['mi_description']) || empty($mission['mi_description']))
			$error['mi_description'] = 'Description obligatoire';
		
		if(!isset($mission['mi_description']) || empty($mission['mi_description']))
			$error['mi_description'] = 'Description obligatoire';
		
		if(!isset($mission['id_contrat']) || empty($mission['id_contrat']) || !is_numeric($mission['id_contrat']))
			$error['id_contrat'] = 'Type de contrat';
		
		if(!isset($mission['mi_tarif']) || empty($mission['mi_tarif']) || !is_numeric($mission['mi_tarif']))
			$error['mi_tarif'] = 'Tarif invalide';
			
		if(!isset($mission['id_monnaie']) || empty($mission['id_monnaie']) || !is_numeric($mission['id_monnaie']))
			$error['id_monnaie'] = 'Devise invalide';
		
		if(!Tools::verifDate($mission["mi_debut"]))
			$error['mi_debut'] = 'Date de debut invalide';
		else
			$mission['mi_debut'] = Tools::shortConvertLanDateToDb($mission['mi_debut']);
		
		
		if(!empty($mission["mi_date_reponse"]) && !Tools::verifDate($mission["mi_date_reponse"]))
			$error['mi_date_reponse'] = 'Date de reponse invalide';
		else
			$mission['mi_date_reponse'] = Tools::shortConvertLanDateToDb($mission['mi_date_reponse']);
		
		if(!isset($mission['mi_int_fin']) || empty($mission['mi_int_fin']) || !is_numeric($mission['mi_int_fin']))
			$error['mi_int_fin'] = 'Date de fib invalide';
		
		if(!isset($mission['id_experience']) || empty($mission['id_experience']) || !is_numeric($mission['id_experience']))
			$error['id_experience'] = 'Experience invalide';
		
		*/
		if(count($error)){
			Tools::debugVar($error);
			return $error;
		}
		
              
                // III. On enregistre dans la base de donnée
		if($id_mission = $this->saveMissionTransfert($mission, $complements)){
			
			// IV. On fait les traitements 	adequats envoie de mails et generation des messages de retour
			return $id_mission; 
			
		}
	
	}
	private function saveMissionTransfert($mission=array(), $complements=array(), $ignore=array(), $debug=false){
               
	       
		$req = "INSERT INTO usr_missions SET
                        modifie_le = NOW(),
			id_mission = ".$this->db->quote($mission['id_mission'], PDO::PARAM_STR).",
                        id_identifiant = ".$this->db->quote($mission['id_identifiant'], PDO::PARAM_STR).",
                        id_client = ".$this->db->quote($mission['id_client'], PDO::PARAM_STR).",
                        id_categorie = ".$this->db->quote($mission['id_categorie'], PDO::PARAM_STR).",
			id_experience = ".$this->db->quote($mission['id_experience'], PDO::PARAM_STR).",
                        mi_reference = ".$this->db->quote($mission['mi_reference'], PDO::PARAM_STR).",
                        mi_titre = ".$this->db->quote($mission['mi_titre'], PDO::PARAM_STR).",
                        mi_description = ".$this->db->quote($mission['mi_description'], PDO::PARAM_STR).",
                        mi_tarif = ".$this->db->quote($mission['mi_tarif'], PDO::PARAM_STR).",
                        id_monnaie = ".$this->db->quote($mission['id_monnaie'], PDO::PARAM_STR).",
                        id_contrat = ".$this->db->quote($mission['id_contrat'], PDO::PARAM_STR).",
                        mi_date_reponse = ".$this->db->quote($mission['mi_date_reponse'], PDO::PARAM_STR).",
                        mi_republication = ".$this->db->quote($mission['mi_republication'], PDO::PARAM_STR).",
                        id_mission_statut = ".$this->db->quote($mission['id_mission_statut'], PDO::PARAM_STR).",
                        mi_reponse_mail = ".$this->db->quote($mission['mi_reponse_mail'], PDO::PARAM_STR).",
                        actif = ".$this->db->quote($mission['actif'], PDO::PARAM_STR).",
                        mi_debut = ".$this->db->quote($mission['mi_debut'], PDO::PARAM_STR).",
                        mi_int_fin = ".$this->db->quote($mission['mi_int_fin'], PDO::PARAM_STR).",
                        mi_string_fin = ".$this->db->quote($mission['mi_string_fin'], PDO::PARAM_STR).",
			lo_quartier = ".$this->db->quote($mission['lo_quartier'], PDO::PARAM_STR).",
			lo_ville = ".$this->db->quote($mission['lo_ville'], PDO::PARAM_STR).",
			lo_departement = ".$this->db->quote($mission['lo_departement'], PDO::PARAM_STR).",
			lo_departement_short = ".$this->db->quote($mission['lo_departement_short'], PDO::PARAM_STR).",
			lo_region = ".$this->db->quote($mission['lo_region'], PDO::PARAM_STR).",
			lo_region_short = ".$this->db->quote($mission['lo_region_short'], PDO::PARAM_STR).",
			lo_pays = ".$this->db->quote($mission['lo_pays'], PDO::PARAM_STR).",
			lo_pays_short = ".$this->db->quote($mission['lo_pays_short'], PDO::PARAM_STR);
                        
                if($mission['mi_int_fin'] && $mission['mi_string_fin'] && $mission['mi_debut'])
			$req .=",mi_fin = DATE_ADD('".$mission['mi_debut']."' , INTERVAL ".$mission['mi_int_fin']." ".$mission['mi_string_fin'].") ";
				
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'orange', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
		
		if($this->db->exec($req)){
			
                        $id_mission =  $this->db->lastInsertId();

			//Enregistrement des informations complementaires
			foreach($complements as $key=>$value){
			    
			    $entry = explode(',', str_replace('.', '', $value));
			   
			    $entries = array_unique($entry);
			    if(!empty($value)){
				switch ($key){
				    
				    case 'id_poste':
					$complement_type = self::COMPLEMENT_POSTE;
					break;
				    
				    case 'id_langue':
					$complement_type = self::COMPLEMENT_LANGUE;
					break;
				    
				    case 'id_competence':
					 
					$complement_type = self::COMPLEMENT_COMPETENCE;
					break;
				    
				    case 'id_etude':
					$complement_type = self::COMPLEMENT_ETUDE;
					break;
				    
				}
			    }
			    foreach($entries as $k=>$id_complement){
				
				if(is_numeric($id_complement) && isset($id_complement) && is_numeric($id_mission) && isset($id_mission)){

					$req = "INSERT INTO usr_missions_complements SET
					modifie_le = NOW(),
					id_mission = ".$this->db->quote($id_mission, PDO::PARAM_STR).",
					id_complement = ".$this->db->quote($id_complement, PDO::PARAM_STR).",
					complement = ".$this->db->quote($complement_type, PDO::PARAM_STR).",
					composante = ".$this->db->quote($id_mission."-".$id_complement."-".$complement_type, PDO::PARAM_STR)."
					ON DUPLICATE KEY UPDATE composante = composante";
				
					Tools::debugVar($req, false);
					if($debug)
						throw new myException($req);
					if(DEBUG)
						parent::$request[] =  array('color'=>'orange', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
					
					$this->db->exec($req);
					
				}else{
				    continue;
				}
			    }
			    
			}
			
			if(count($complements)==1){
				return $id_mission;
			}
			

		}

		return true;
	}
        
	public function editMissionTransfert($array=array(), $republication = false){
	
		// I. On commence par hydrater notre tableau (cast des variable int, string...)
		$mission = array(
				'id_mission'=>        $array["id_mission"],
				'id_identifiant'=>    $array["id_identifiant"],
				'id_categorie'=>      $array["id_categorie"],
				'id_experience'=>     $array["id_experience"],
				'mi_titre'=>          $array["mi_titre"],
				'mi_description'=>    $array["mi_description"],
				'mi_tarif'=>          $array["mi_tarif"],
				'id_monnaie'=>        $array["id_monnaie"],
				'id_contrat'=>        $array["id_contrat"],
				'mi_debut'=>          $array["mi_debut"],
				'mi_fin'=>            $array["mi_fin"],
				'mi_int_fin'=>        $array["mi_int_fin"],
				'mi_string_fin'=>     $array["mi_string_fin"],
				'mi_date_reponse'=>   $array["mi_date_reponse"],
				'mi_republication'=>  $array["mi_republication"],
				'mi_reponse_mail'=>   $array["mi_reponse_mail"],
				'id_mission_statut'=> self::MISSION_ATTENTE,
				'actif'=>             1,
				
				'lo_quartier'=>           $array["lo_quartier"],
				'lo_ville'=>              $array["lo_ville"],
				'lo_departement'=>        $array["lo_departement"],
				'lo_departement_short'=>  $array["lo_departement_short"],
				'lo_region'=>             $array["lo_region"],
				'lo_region_short'=>       $array["lo_region_short"],
				'lo_pays'=>               $array["lo_pays"],
				'lo_pays_short'=>         $array["lo_pays_short"]
			);
		
		$complements = array(
			    'id_poste'=>              $array["id_poste"],
			    'id_competence'=>         $array["id_competence"],
			    'id_langue'=>             $array["id_langue"],
			    'id_etude'=>              $array["id_etude"]
		);
		
		if($array['page'] == 'edit_mission'){
			
			unset($complements);	
		}
		// II. On effectue les traitements de formulaire avant de sauvegarder dans la base de donnée
		if(count($complements)<1 && !$republication && !isset($array['mi_reponse_count']) && !isset($array['id_mission_statut'])){
			//Si on traite des complements
			//si on fait juste une republication
			//si on fait une uodate deu competur de reponse
			//si on fait fait un changement de statut
			//ON NE PASSE PAS PAR CETTE PARTIE
		
			if(!isset($mission['mi_titre']) || empty($mission['mi_titre']))
				$error['mi_titre'] = 'Titre obligatoire';
				
			if(!isset($mission['mi_description']) || empty($mission['mi_description']))
				$error['mi_description'] = 'Description obligatoire';
			
			if(!isset($mission['mi_description']) || empty($mission['mi_description']))
				$error['mi_description'] = 'Description obligatoire';
			
			if(!isset($mission['id_contrat']) || empty($mission['id_contrat']) || !is_numeric($mission['id_contrat']))
				$error['id_contrat'] = 'Type de contrat';
			
			if(!isset($mission['mi_tarif']) || empty($mission['mi_tarif']) || !is_numeric($mission['mi_tarif']))
				$error['mi_tarif'] = 'Tarif invalide';
				
			if(!isset($mission['id_monnaie']) || empty($mission['id_monnaie']) || !is_numeric($mission['id_monnaie']))
				$error['id_monnaie'] = 'Devise invalide';
			
			if(!Tools::verifDate($mission["mi_debut"]))
				$error['mi_debut'] = 'Date de debut invalide';
			else
				$mission['mi_debut'] = Tools::shortConvertLanDateToDb($mission['mi_debut']);
			
			
			if(!empty($mission["mi_date_reponse"]) && !Tools::verifDate($mission["mi_date_reponse"]))
				$error['mi_date_reponse'] = 'Date de reponse invalide';
			else
				$mission['mi_date_reponse'] = Tools::shortConvertLanDateToDb($mission['mi_date_reponse']);
			
			if(!isset($mission['mi_int_fin']) || empty($mission['mi_int_fin']) || !is_numeric($mission['mi_int_fin']))
				$error['mi_int_fin'] = 'Date de fib invalide';
			
			if(!isset($mission['id_experience']) || empty($mission['id_experience']) || !is_numeric($mission['id_experience']))
				$error['id_experience'] = 'Experience invalide';
			
			
			if(count($error))
				return $error;
		}
		//Cette condition intervient uniquement lors de la sauvegarde d'une reponse
		//On souhaite juste incrementer le compteur de reponse mission
		//Donc on a pas besoin du gros tableau $mission...On efface et on change l"affectation
		//Au passage on fait pareil avec les complements
		if(isset($array['mi_reponse_count'])){
			unset($mission);
			unset($complements);
			$mission['mi_reponse_count'] = $array['mi_reponse_count'];
			$mission['id_mission']       = $array['id_mission'];
			$mission['id_identifiant']   = $array['id_identifiant'];
		}
		//Cette condition intervient uniquement lors de la republication d'une mission
		//On souhaite juste incrementer le compteur des republication
		//Donc on a pas besoin du gros tableau $mission...On efface et on change l"affectation
		//Au passage on fait pareil avec les complements
		if(isset($array['mi_republication'])){
			unset($mission);
			unset($complements);
			$mission['mi_republication'] = 1;
			$mission['id_mission']       = $array['id_mission'];
			$mission['id_identifiant']   = $array['id_identifiant'];
		}
		//Cette condition intervient uniquement lors de la mise a jour des notifcation email d'une mission
		//On souhaite juste activer ou desactiver la notification
		//Donc on a pas besoin du gros tableau $mission...On efface et on change l"affectation
		//Au passage on fait pareil avec les complements
		if(isset($array['mi_reponse_mail']) && !isset($array['mi_titre'])){
			unset($mission);
			unset($complements);
			$mission['mi_reponse_mail']  = $array['mi_reponse_mail'];
			$mission['id_mission']       = $array['id_mission'];
			$mission['id_identifiant']   = $array['id_identifiant'];
		}
		//Cette condition intervient uniquement lors du changement de statut de la mission
		//On souhaite juste changer le statut
		//Donc on a pas besoin du gros tableau $mission...On efface et on change l"affectation
		//Au passage on fait pareil avec les complements

		if(isset($array['id_mission_statut'])){
			unset($mission);
			unset($complements);
			$mission['id_mission_statut'] = $array['id_mission_statut'];
			$mission['id_mission']        = $array['id_mission'];
			$mission['id_identifiant']    = $array['id_identifiant'];
			
			if(!is_array($mission['id_mission']) || !count($mission['id_mission']))
			       $error['id_mission'] = 'Impossible d\'acceder à la mission demandée !';
	
			if(!isset($mission['id_mission_statut']) || empty($mission['id_mission_statut']))
			       $error['id_mission_statut'] = 'Merci de seectionner votre statut';
			
			if(count($error)>0)//on a des erreurs on stop ici
				return $error;
			
		}
	
		// III. On enregistre dans la base de donnée
		if(isset($array["rmv_id_mission_complement"]) && is_numeric($array["rmv_id_mission_complement"]) && isset($array["id_mission"])){
			return $this->rmvComplement(array('id_mission_complement'=>$array["rmv_id_mission_complement"], 'id_mission'=>$array["id_mission"]));
		}
		else{
	
			if($id_mission = $this->updMissionTransfert($mission, $complements)){
				
			
				return $id_mission; 
				
			}
		}
	
	}
	private function updMissionTransfert($mission=array(), $complements=array(), $ignore=array('page', 'action', 'id_langue', 'id_poste', 'id_competence', 'id_etude'), $debug=false){
             
		$req .= "UPDATE usr_missions SET " ;
		
		foreach($mission as $key=>$value){
			if(!in_array($key, $ignore)){
				
				if($value != "" && !is_array($value) ){
					
					if($key == 'mi_republication' && $mission['mi_republication']>0){
						$req .= "mi_republication = mi_republication+1, ";
					}
					elseif($key == 'mi_reponse_count' && $mission['mi_reponse_count']>0){
						$req .= "mi_reponse_count = mi_reponse_count+1, ";
					}
					elseif($key == "mi_debut" && !$value){
						$req.=" ".$key."= NOW() ,";
					}
					else{
						$req .= $key." = ".$this->db->quote($value, PDO::PARAM_STR)." , ";
					}
					
				}
			}
		}
		
		//on addition la date de fin au début
		if($mission['mi_int_fin'] && $mission['mi_string_fin'] && $mission['mi_debut'])
			$req .=" mi_fin = DATE_ADD('".Tools::shortConvertLanDateToDb($mission['mi_debut'])."' , INTERVAL ".$mission['mi_int_fin']." ".$mission['mi_string_fin'].") ,";
		
	
		if($mission['mi_republication'])
			$req .= " modifie_le = NOW() ,  cree_le = NOW() ";
		else
			$req .= " modifie_le = NOW() ";

		if(is_array($mission['id_mission']))
			$req .= "WHERE id_mission IN (".implode(',', $mission['id_mission']).") ";
		
		else
			$req .= "WHERE id_mission = ".$this->db->quote($mission['id_mission'], PDO::PARAM_STR);
			
		if(is_numeric($mission['id_mission_statut']))//On ne change pas le statut d'une mission qui est terminée !
			$req .= " AND id_mission_statut != ".$this->db->quote(self::MISSION_TERMINEE, PDO::PARAM_STR);

		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'orange', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
		
				
		if($this->db->exec($req)){
			Tools::debugVar($complements, false, $mission['id_mission']);
			if(count($complements)){
				$id_mission =  $mission['id_mission'];
				 
				//Enregistrement des informations complementaires
				foreach($complements as $key=>$value){
					$entry = array();

					if(!is_array($value)){
						
						$entry = explode(',', str_replace('.', '', $value));
					}
					else{
						
						$entry = $value;

					}
					
			
					array_map(trim, $entry);			
					$entries = array_unique($entry);
				    
					if(!empty($value)){
					    switch ($key){
						
						case 'id_poste':
						    $complement_type = self::COMPLEMENT_POSTE;			
						    break;
						
						case 'id_langue':
						    $complement_type = self::COMPLEMENT_LANGUE;
						    break;
						
						case 'id_competence':
						    $complement_type = self::COMPLEMENT_COMPETENCE;
						    break;
						
						case 'id_etude':
						    $complement_type = self::COMPLEMENT_ETUDE;
						    break;
						
					    }
					    
					}
				       
				     
					if(is_numeric($complement_type)){
															    
						foreach($entries as $k=>$id_complement){
							
							//si le complement existe déja dans les table adm_* on connait déja son id
							if(is_numeric($id_complement) && isset($id_complement) && is_numeric($id_mission) && isset($id_mission)){
								    $req = "INSERT INTO usr_missions_complements SET
									    modifie_le = NOW(),
									    id_mission = ".$this->db->quote($id_mission, PDO::PARAM_STR).",
									    id_complement = ".$this->db->quote($id_complement, PDO::PARAM_STR).",
									    complement = ".$this->db->quote($complement_type, PDO::PARAM_STR).",
									    composante = ".$this->db->quote(str_replace(' ', '', $id_mission."-".$id_complement."-".$complement_type), PDO::PARAM_STR)."
									    ON DUPLICATE KEY UPDATE composante = composante";
								
								    if($debug)
									    throw new myException($req);
								    if(DEBUG)
									    parent::$request[] =  array('color'=>'orange', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
								
							
								if($this->db->exec($req))
								    $lastId = $this->db->lastInsertId(); 
								
							
							}
							else{
								
								//si on ajoute un nouveau complement type adm_*
								if(!is_numeric($id_complement) && !empty($id_complement) && is_numeric($id_mission) && isset($id_mission)){
								    
									$DBadmin = new DBAdmin();
									switch ($complement_type){
							   
									   case self::COMPLEMENT_POSTE:
									       $complement = 'poste';
									       break;
									   
									   case self::COMPLEMENT_LANGUE:
									       $complement = 'langue';
									       break;
									   
									   case self::COMPLEMENT_COMPETENCE:
									       $complement = 'competence';
									       break;
									   
									   case self::COMPLEMENT_ETUDE:
									       $complement = 'etude';
									       break;
									   
								       }

								   
									if($id_complement = $DBadmin->saveComplement(array('table'=>$complement, 'entry'=>$id_complement))){
									 
									   $req = "INSERT INTO usr_missions_complements SET
										 modifie_le = NOW(),
										 id_mission = ".$this->db->quote($id_mission, PDO::PARAM_STR).",
										 id_complement = ".$this->db->quote($id_complement, PDO::PARAM_STR).",
										 complement = ".$this->db->quote($complement_type, PDO::PARAM_STR).",
										 composante = ".$this->db->quote(str_replace(' ', '', $id_mission."-".$id_complement."-".$complement_type), PDO::PARAM_STR)."
										 ON DUPLICATE KEY UPDATE composante = composante";
										 
										     if($debug)
											     throw new myException($req);
										     if(DEBUG)
											     parent::$request[] =  array('color'=>'orange', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
										
										 
										 if($this->db->exec($req))
											 return $this->db->lastInsertId(); 
										 
									}
							    
								    
								}
							}
							
						
						}
						continue;
					}
				    
				}

				if(!$lastId)
					return false;
				
				if(is_numeric($lastId) && $lastId>0)
					return $lastId;
					
				if(count($complements)==1){
					return $lastId;
				}
			}
			return true;
		}

		
	}
}
