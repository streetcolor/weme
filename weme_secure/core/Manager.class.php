<?PHP
class Manager {

	protected $localisation; //langue sur laquelle le compte � �t� cr�� FR, EN, ES
        protected $id_identifiant;
	protected $id_client_manager;
        protected $id_role; //Boolean True il s'agit du Super admin et gere abonnement et manager False il s'agit d'un simple manager
        
	protected $civilite;  //array
        protected $nom;
        protected $prenom;
	
	protected $update; 
	
        protected $numero_fixe       = array();//indicatif + numero
        protected $numero_portable   = array();//indicatif + numero
        
        protected $email; //email d'identification
	
	protected $jabber = array();//On va stocker les ID jabber de notre prestataire ici
	
	public function __construct( $donnees=array()){
				
                $this->localisation       = $donnees['usr_trad'];
		$this->id_identifiant     = $donnees['id_identifiant'];
                $this->id_role            = $donnees['id_role'];
		$this->id_client_manager  = $donnees['id_client_manager'];
                $this->civilite           = array(
						   'id_civilite'=>$donnees['id_civilite'],
						   'civilite'=>$donnees['adm_civilite']
						  );
                
                $this->nom                = $donnees['mg_nom'];
                $this->prenom             = $donnees['mg_prenom'];
		
		$this->update            = $donnees['modifie_le'];
		
		$this->fixe               = array(
                                                'indicatif' =>$donnees["mg_fixe_indicatif"],
                                                'numero'    =>$donnees["mg_fixe"]
                                                );
                $this->portable           = array(
                                               'indicatif' =>$donnees["mg_portable_indicatif"],
                                               'numero'    =>$donnees["mg_portable"]
                                             );
		
		$this->email              = $donnees['usr_email'];
		
		$this->jabber                = array(
                                                   'loggin'   =>$donnees["jbr_loggin"],
                                                   'password' =>$donnees["jbr_password"]
                                                );
		
		
		
	}
	
	public function __get($name){
		echo "<span class=\"error\">R�cup�ration de '$name' impossible dans  Prestataire.class.php</span>";
	}
	
	public function __call($name, $arguments){
		echo "<span class=\"error\">Appel de la m�thode '$name' inexistante avec les argument suivants Prestataire.class.php</span>"
			. implode(', ', $arguments). "\n";
	}

	public static function __callStatic($name, $arguments){
		echo "<span class=\"error\">Appel de la m�thode statique '$name' inexistante avec les argument suivants Prestataire.class.php</span>"
			. implode(', ', $arguments). "\n";
        }
	
        public function getLocalisation(){
		return $this->localisation;
	}
        
	public function getId_identifiant(){
		return $this->id_identifiant;
	}
	
	
	public function getId_manager(){
		return $this->id_client_manager;
	}
	
	
        public function getId_role(){
		return $this->id_role;
	}
        
        public function getCivilite(){
		
		return (object) $this->civilite;
	} 
        
        public function getNom(){
		return $this->nom;
	} 
        
        public function getPrenom(){
		return $this->prenom;
	}
	
	public function getUpdate(){
		
		return Tools::shortConvertDBDateToLan($this->update);
	}
        
        public function getFixe($string=false){
            
            $numero = $this->fixe;
	    
            if($string)
                return $numero["indicatif"]." - ".$numero["numero"];
            else
                return (object)$numero;
	}
        
        public function getPortable($string=false){
            
            $numero = $this->portable;
	    
            if($string)
                return $numero["indicatif"]." - ".$numero["numero"];
            else
                return (object)$numero;
	}
        
	public function getEmail(){
		return $this->email;
	}
	
	public function getJabber(){
		return (object) $this->jabber;
	}
	
	//Pour acceder � cette methode
	//$client = new client();
	//$avatar = $client->getAvatar();
	public function getAvatar(){
		$DBfile = new DBFile();
		$avatar = $DBfile->getListFiles(array('id_identifiant'=>$this->id_identifiant, 'fichier_type'=>array(DBFile::TYPE_AVATAR)));
		
		if($avatar['count'])
			return $avatar['files'][0];
		else
			return new File(array('fichier_type'=>DBFile::TYPE_AVATAR));
			
		
	}
	
	//Cette fonction permet de r�cuperer les reponse envoy�e par les profils
	//Elle est separ�e pour �viter de surcharger inutilement les requetes SQL
	public function getReponses(){
		$reponse = new DBReponse();
		$listReponses = $reponse->getListReponses(array('id_client'=>$this->id_client)); 
	
		return (object) $listReponses;
	}
    
	//Cette fonction permet de r�cuperer les favoris profils
	//Elle est separ�e pour �viter de surcharger inutilement les requetes SQL
	public function getFavoris(){
		$favoris = new DBMembre();
		$listFavoris = $favoris->getListFavorisProfils($this->id_identifiant); 
	
		return (object) $listFavoris;
	}
        
	//Cette fonction permet de r�cuperer les reponses associ�s au manager en cours
	//Elle est separ�e pour �viter de surcharger inutilement les requetes SQL
	public function getMissions($limit=false){
		$missions = new DBMission();
		return (object) $missions->getListMissions(array('id_client'=>$this->id_client), array(), $limit);
	}
	
	//Cette fonction permet de r�cuperer les message type associ�s au manager en cours
	//Elle est separ�e pour �viter de surcharger inutilement les requetes SQL
	public function getMessages($limit=false){
		$messages = new DBMessage();
		return (object) $messages->getListMessages(array('id_identifiant'=>$this->id_identifiant), $limit);
	}
}
