<?php
//EXEMPLES D'UTILISATION DE RECUPERATION DE PRESTATAIRE ET DE PROFILS
//$prestataire = $DBmembre->getPrestataire($id_identifiant = 2);
//$profil      = $DBmembre->getListProfils(array('id_prestataire_profil '=>7));
//$entreprise  = $profil[0]->getPrestataire()->getEntreprise();
class DBMembre extends DBConnect{
	
	//TRES IMPORTANT
	//Les méthodes save et upd doivent être privée et sonr uniquement appelée via les méthodes make
	//Les tests de saisie s'effectuent OBLIGATOIREMENT depuis les méthodes make
	
	const TYPE_CLIENT           = 1;
	const TYPE_PRESTATAIRE      = 2;
	const TYPE_ADMINISTRATEUR   = 3;
	
	const COMPLEMENT_LANGUE     = 1;
        const COMPLEMENT_ETUDE      = 2;
        const COMPLEMENT_COMPETENCE = 3;
	const COMPLEMENT_POSTE      = 4;
	
	const TYPE_ADMIN            = 1;//Prestataire parent et Client Parent (Ceux qui crée le compte depuis la page inscription)
	const TYPE_NORMAL           = 0;//Profil et Manager
	
	const TYPE_FREELANCE        = 1;
	const TYPE_INTERMITTENT     = 2;
	const TYPE_AGENCE           = 3;//Seul a posséder le multiprofil

	public static $id_identifiant = null;
	public static $id_type  = null;
	
	private $quotaPourcentage = array();
	
	public function __construct(){
		//Tools::debugVar($_SESSION["user"]["id_identifiant"]);
		if($_SESSION["user"]["id_identifiant"] || $_COOKIE["user"]["id_identifiant"]){
			if(is_numeric($_COOKIE["user"]["id_identifiant"]) && ($_COOKIE["user"]["id_identifiant"]>=1))
				$id_identifiant = $_COOKIE["user"]["id_identifiant"];
			elseif(is_numeric($_SESSION["user"]["id_identifiant"]) && ($_SESSION["user"]["id_identifiant"]>=1))
				$id_identifiant = $_SESSION["user"]["id_identifiant"];
				
			$this->makeMembreSingleton($id_identifiant);
		}
		
		if($_SESSION["user"]["id_type"] || $_COOKIE["user"]["id_type"]){
			if(is_numeric($_COOKIE["user"]["id_type"]) && ($_COOKIE["user"]["id_type"]>=1))
				$id_type = $_COOKIE["user"]["id_type"];
			elseif(is_numeric($_SESSION["user"]["id_type"]) && ($_SESSION["user"]["id_type"]>=1))
				$id_type = $_SESSION["user"]["id_type"];
				
			$this->makeTypeSingleton($id_type);
		}
		
		parent::__construct();
	}
	
	
	public function makeMembreSingleton($id_identifiant=""){
		
		if($id_identifiant){
			if(is_null(self::$id_identifiant)) {
			   self::$id_identifiant = $id_identifiant;
			}	
		}
		
		else throw new myException('Création du singleton impossible pour ce membre');
	}
	public function makeTypeSingleton($id_type=""){
		
		if($id_type){	
			if(is_null(self::$id_type)) {
			   self::$id_type = $id_type;
			}	
		}
		
		else throw new myException('Création du singleton type membre impossible');
	}
	
	private function saveLog($identifiant, $log = array(), $debug = false){
		
		$req = "
		INSERT INTO usr_logs SET 
		cree_le=NOW(),
		modifie_le=NOW(),
		id_identifiant=".$this->db->quote($identifiant, PDO::PARAM_STR);
		
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
		
		if($this->db->exec($req))
			return true;	
	}
	private function saveIdentifiant($array=array(), $debug=false){
		
		$req = "
		INSERT INTO usr_identifiants SET  
		id_type=".$this->db->quote($array['id_type'], PDO::PARAM_STR).",
		usr_password= PASSWORD(".$this->db->quote($array['usr_password'], PDO::PARAM_STR)."),
		usr_email=".$this->db->quote($array['usr_email'], PDO::PARAM_STR).",
		usr_trad = ".$this->db->quote($array['usr_trad'], PDO::PARAM_STR).",
		actif = ".$this->db->quote($array['actif'], PDO::PARAM_STR).",
		usr_activate = ".$this->db->quote($array['usr_activate'], PDO::PARAM_STR).",
		modifie_le=NOW()";			
		
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
		
		if($this->db->exec($req)){
			
			return $this->db->lastInsertId();
	
		}
		
	}
	private function saveFavoriMission($favoris=array(), $ebug=false){
		$req = "INSERT INTO usr_prestataires_favoris SET
			modifie_le = NOW(),
			id_mission = ".$this->db->quote($favoris['id_mission'], PDO::PARAM_STR).",
			id_identifiant = ".$this->db->quote($favoris['id_identifiant'], PDO::PARAM_STR).",
			composante = ".$this->db->quote($favoris['composante'], PDO::PARAM_STR)."
			ON DUPLICATE KEY UPDATE composante = composante";
			
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
			
		if($this->db->exec($req))
			return $this->db->lastInsertId();
	
	}
	private function rmvFavoriMission($favoris = array(),  $debug=false){
		
		$req .= "DELETE FROM
			usr_prestataires_favoris WHERE cree_le
			AND id_identifiant = ".$this->db->quote($favoris['id_identifiant'], PDO::PARAM_STR)."
			AND id_mission = ".$this->db->quote($favoris['id_mission'], PDO::PARAM_STR);
		
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);
		if($this->db->exec($req))
		
			return true;
	}
	public function getListFavorisMissions($id_identifiant){
		$favoris = array();
		
		$counter = array(
				'COUNT' => 'SELECT count(DISTINCT(id_mission)) AS total ' ,
				'SELECT'=> 'SELECT DISTINCT(id_mission) '
				);
		
		$req = " FROM usr_prestataires_favoris
			WHERE id_identifiant =  ".$this->db->quote($id_identifiant, PDO::PARAM_STR);
		
		if($debug)
			throw new myException($counter['COUNT'].$req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$counter['COUNT'].$req);

		//Le count avec la requete
		$resultats = $this->db->query($counter['COUNT'].$req);
				
		//si on un total superieur a zéro
		//on reexecute la requete afin de recuperer les elements
                if($count = $resultats->fetch(PDO::FETCH_OBJ)->total){
			if($debug)
				throw new myException($counter['SELECT'].$req.$limit['SELECT']);
			if(DEBUG)
				parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$counter['SELECT'].$req.$limit['SELECT']);
			
 			$resultats = $this->db->query($counter['SELECT'].$req);
		
			while ($id_mission = $resultats->fetch(PDO::FETCH_OBJ))
				$favoris[] = $id_mission->id_mission;	
				
			$resultats->closeCursor();
		}
		return array('count'=>$count, 'favoris'=>$favoris);
		
	}
	private function saveFavoriProfil($favoris=array(), $ebug=false){
		$req = "INSERT INTO usr_managers_favoris SET
			modifie_le = NOW(),
			id_prestataire_profil = ".$this->db->quote($favoris['id_prestataire_profil'], PDO::PARAM_STR).",
			id_identifiant = ".$this->db->quote($favoris['id_identifiant'], PDO::PARAM_STR).",
			composante = ".$this->db->quote($favoris['composante'], PDO::PARAM_STR)."
			ON DUPLICATE KEY UPDATE composante = composante";

		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
			
		if($this->db->exec($req))
			return $this->db->lastInsertId();
	}
	private function rmvFavoriProfil($favoris = array(),  $debug=false){
		
		$req .= "DELETE FROM
			usr_managers_favoris WHERE cree_le
			AND id_identifiant = ".$this->db->quote($favoris['id_identifiant'], PDO::PARAM_STR)."
			AND id_prestataire_profil = ".$this->db->quote($favoris['id_prestataire_profil'], PDO::PARAM_STR);
		
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);

		if($this->db->exec($req))
		
			return true;
	}
	public function getListFavorisProfils($id_identifiant){
		$favoris = array();
	
		$counter = array(
				'COUNT' => 'SELECT count(DISTINCT(id_prestataire_profil)) AS total ' ,
				'SELECT'=> 'SELECT DISTINCT(id_prestataire_profil ) '
				);
		
		$req = " FROM usr_managers_favoris
			WHERE id_identifiant =  ".$this->db->quote($id_identifiant, PDO::PARAM_STR);
		
		if($debug)
			throw new myException($counter['COUNT'].$req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$counter['COUNT'].$req);

		//Le count avec la requete
		$resultats = $this->db->query($counter['COUNT'].$req);

		//si on un total superieur a zéro
		//on reexecute la requete afin de recuperer les elements
                if($count = $resultats->fetch(PDO::FETCH_OBJ)->total){
			if($debug)
				throw new myException($counter['SELECT'].$req.$limit['SELECT']);
			if(DEBUG)
				parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$counter['SELECT'].$req.$limit['SELECT']);
			
 			$resultats = $this->db->query($counter['SELECT'].$req);
		
			while ($id_profil = $resultats->fetch(PDO::FETCH_OBJ))
				$favoris[] = $id_profil->id_prestataire_profil;	
				
			$resultats->closeCursor();
		}
		return array('count'=>$count, 'favoris'=>$favoris);
	
		
	}
	public function updIdentifiant($filtre=array(), $debug=false){
	
		$req .= "UPDATE usr_identifiants SET modifie_le = NOW() " ;
		
		foreach($filtre as $key=>$value){
			if($key== 'usr_password')
				$req .= ", ".$key." = PASSWORD(".$this->db->quote($value, PDO::PARAM_STR).") ";
			else
				$req .= ", ".$key." = ".$this->db->quote($value, PDO::PARAM_STR);
		}
		
		$req .= " WHERE id_identifiant = ".$this->db->quote($filtre['id_identifiant'], PDO::PARAM_STR);
			
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
			
		if($this->db->exec($req))
			return true;	
	}
	private function saveManager($array=array(), $ignore=array(), $debug=false){
		
		$req = " INSERT INTO usr_clients_managers SET ";
		
		foreach($array as $key=>$value){
			if(!in_array($key, $ignore))
				$req .= $key." = ".$this->db->quote($value, PDO::PARAM_STR)." , ";
		}		
		
		$req .= " modifie_le = NOW() ";
		
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
		
		if($this->db->exec($req)){
			
			return $this->db->lastInsertId();
	
		}
		
	}
	
	private function saveProfil($array=array(), $ignore=array(), $debug=false){
		
		$req = " INSERT INTO usr_prestataires_profils SET ";
		
		foreach($array as $key=>$value){
			if(!in_array($key, $ignore))
				$req .= $key." = ".$this->db->quote($value, PDO::PARAM_STR)." , ";
		}		
		
		$req .= " modifie_le = NOW() ";
			
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
		
		if($this->db->exec($req)){
			
			return $this->db->lastInsertId();
	
		}
		
	}
	private function saveCvFormation($array=array(), $ignore=array('page', 'action', 'flag', 'id_profil_formation', 'id_identifiant', 'crypt'), $debug=false){
		
		$req = " INSERT INTO usr_profils_formations SET ";
		
		foreach($array as $key=>$value){
			if(!in_array($key, $ignore))
				$req .= $key." = ".$this->db->quote($value, PDO::PARAM_STR)." , ";
		}		
		
		$req .= " modifie_le = NOW() ";
			
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
		
		if($this->db->exec($req)){
			
			return $this->db->lastInsertId();
	
		}
		
	}
	private function updCvFormation($array=array(), $ignore=array('page', 'action', 'flag', 'id_identifiant', 'crypt'), $debug=false){
		
		$req = " UPDATE usr_profils_formations SET ";
		
		foreach($array as $key=>$value){
			if(!in_array($key, $ignore))
				$req .= $key." = ".$this->db->quote($value, PDO::PARAM_STR)." , ";
		}		
		
		$req .= " modifie_le = NOW()  WHERE id_profil_formation = ".$this->db->quote($array["id_profil_formation"], PDO::PARAM_STR);
			
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
		
		if($this->db->exec($req)){
			
			return true;
	
		}
		
	}
	private function rmvCvFormation($array = array(),  $debug=false){
		
		$req .= "DELETE FROM
			usr_profils_formations WHERE cree_le
			AND id_profil_formation = ".$this->db->quote($array['rmv_id_profil_formation'], PDO::PARAM_STR)."
			AND id_prestataire_profil = ".$this->db->quote($array['id_prestataire_profil'], PDO::PARAM_STR);
		
		if($debug)
			throw new myException($req);
		if(DEBUG)
		//		DBAdmin::$error .=  '<span class="blue bold">'.__CLASS__.'</span> >>> <span class="pink bold"> '.  __FUNCTION__.'()</span><hr><br>'.$req.'<br><br>';	
		if($this->db->exec($req))
		
			return true;
	}
	private function saveCvExperience($array=array(), $ignore=array('page', 'action', 'flag', 'id_profil_experience', 'id_identifiant', 'crypt'), $debug=false){
		
		$req = " INSERT INTO usr_profils_experiences SET ";
		
		foreach($array as $key=>$value){
			if(!in_array($key, $ignore))
				$req .= $key." = ".$this->db->quote($value, PDO::PARAM_STR)." , ";
		}		
		
		$req .= " modifie_le = NOW() ";
			
		
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
		
		if($this->db->exec($req)){
			
			return $this->db->lastInsertId();
	
		}
		
	}
	private function updCvExperience($array=array(), $ignore=array('page', 'action', 'flag', 'id_identifiant', 'crypt'), $debug=false){
		
		$req = " UPDATE usr_profils_experiences SET ";
		
		foreach($array as $key=>$value){
			if(!in_array($key, $ignore))
				$req .= $key." = ".$this->db->quote($value, PDO::PARAM_STR)." , ";
		}		
		
		$req .= " modifie_le = NOW()  WHERE id_profil_experience = ".$this->db->quote($array["id_profil_experience"], PDO::PARAM_STR);
			
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
		
		if($this->db->exec($req)){
			
			return true;
	
		}
		
	}
	private function rmvCvExperience($array = array(), $debug=false){
		
		$req .= "DELETE FROM
			usr_profils_experiences WHERE cree_le
			AND id_profil_experience = ".$this->db->quote($array['rmv_id_profil_experience'], PDO::PARAM_STR)."
			AND id_prestataire_profil = ".$this->db->quote($array['id_prestataire_profil'], PDO::PARAM_STR);
		
		
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
		
		if($this->db->exec($req))
		
			return true;
	}
	private function saveCvComplements($complements=array(),  $ignore=array(), $debug=false){
                        //Enregistrement des informations complementaires
                        foreach($complements as $key=>$value){
                            
                            $entry = explode(',', str_replace('.', '', $value));
			    array_map(trim, $entry);
			    
                            $entries = array_unique($entry);
                            
                            switch ($key){
                                
                                case 'id_poste':
                                    $complement_type = self::COMPLEMENT_POSTE;
                                    break;
                                
                                case 'id_langue':
                                    $complement_type = self::COMPLEMENT_LANGUE;
                                    break;
                                
                                case 'id_competence':
                                    $complement_type = self::COMPLEMENT_COMPETENCE;
                                    break;
                                
                                case 'id_etude':
                                    $complement_type = self::COMPLEMENT_ETUDE;
                                    break;
                                
                            }
                            if(is_numeric($complement_type) || (!empty($complements['id_niveau']) && !empty($complements['id_prestataire_profil']))){
				

				foreach($entries as $k=>$id_complement){
					//Si on update le niveau et seulement le niveau 
				    if(is_numeric($id_complement) && isset($id_complement) && is_numeric($complements['id_prestataire_profil']) && isset($complements['id_prestataire_profil']) && is_numeric($complements['id_niveau']) && isset($complements['id_niveau'])){
					    $req = "UPDATE usr_profils_complements SET
					    modifie_le = NOW(),
					    id_niveau = ".$this->db->quote($complements['id_niveau'], PDO::PARAM_STR)."
					    WHERE id_prestataire_profil = ".$this->db->quote($complements['id_prestataire_profil'], PDO::PARAM_STR)." AND  id_profil_complement  = ".$this->db->quote($id_complement, PDO::PARAM_STR);
					    
						if($debug)
							throw new myException($req);
						if(DEBUG)
							parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
						
					    
					    if($this->db->exec($req))
						$lastId = $id_complement;
						
				    }
					//Si on ajoute une nouvelle entrée dans la table des profil_complements 
				    elseif(is_numeric($id_complement) && isset($id_complement) && is_numeric($complements['id_prestataire_profil']) && isset($complements['id_prestataire_profil'])){
					    $req = "INSERT INTO usr_profils_complements SET
					    modifie_le = NOW(),
					    id_prestataire_profil = ".$this->db->quote($complements['id_prestataire_profil'], PDO::PARAM_STR).",
					    id_niveau = ".$this->db->quote(2, PDO::PARAM_STR).",
					    id_complement = ".$this->db->quote($id_complement, PDO::PARAM_STR).",
					    complement = ".$this->db->quote($complement_type, PDO::PARAM_STR).",
					    composante = ".$this->db->quote(str_replace(' ', '', $complements['id_prestataire_profil']."-".$id_complement."-".$complement_type), PDO::PARAM_STR)."
					    ON DUPLICATE KEY UPDATE composante = composante";
					    
						if($debug)
							throw new myException($req);
						if(DEBUG)
							parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
						
					    
					    if($this->db->exec($req))
						$lastId = $this->db->lastInsertId();
						
				    }
				    else{
					 //Si on veut inserer une nouvelle entrée qui n'exisait pas en BDD donc on string on l'enregitsre dabord dans la table ADM et ensuite dans la table profils_complements  
					if(!is_numeric($id_complement) && !empty($id_complement) && is_numeric($complements['id_prestataire_profil']) && isset($complements['id_prestataire_profil'])){

						$DBadmin = new DBAdmin();
						switch ($complement_type){
				   
						   case self::COMPLEMENT_POSTE:
						       $complement = 'poste';
						       break;
						   
						   case self::COMPLEMENT_LANGUE:
						       $complement = 'langue';
						       break;
						   
						   case self::COMPLEMENT_COMPETENCE:
						       $complement = 'competence';
						       break;
						   
						   case self::COMPLEMENT_ETUDE:
						       $complement = 'etude';
						       break;
						   
					       }

					       if($id_complement = $DBadmin->saveComplement(array('table'=>$complement, 'entry'=>$id_complement))){
						
						  $req = "INSERT INTO usr_profils_complements SET
							modifie_le = NOW(),
							id_prestataire_profil = ".$this->db->quote($complements['id_prestataire_profil'], PDO::PARAM_STR).",
							id_niveau = ".$this->db->quote($id_niveau, PDO::PARAM_STR).",
							id_complement = ".$this->db->quote($id_complement, PDO::PARAM_STR).",
							complement = ".$this->db->quote($complement_type, PDO::PARAM_STR).",
							composante = ".$this->db->quote($complements['id_prestataire_profil']."-".$id_complement."-".$complement_type, PDO::PARAM_STR)."
							ON DUPLICATE KEY UPDATE composante = composante";
							
							    if($debug)
								    throw new myException($req);
							    if(DEBUG)
								    parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
							    
							
							if($this->db->exec($req))
								return $this->db->lastInsertId();
							
					       }
					
						
					}
				    }
				    
				    
				}
				break;
			    }
                            
                        }
			
			return $lastId;
	}
	private function saveCvLocalites($array=array(),  $ignore=array(), $debug=false){

                 // I. On commence par hydrater notre tableau (cast des variable int, string...)
		$localite = array(
				
				'id_prestataire_profil' =>  $array['id_prestataire_profil'],
				'lo_quartier'=>             $array["lo_quartier"],
				'lo_ville'=>                $array["lo_ville"],
				'lo_departement'=>          $array["lo_departement"],
				'lo_departement_short'=>    $array["lo_departement_short"],
				'lo_region'=>               $array["lo_region"],
				'lo_region_short'=>         $array["lo_region_short"],
				'lo_pays'=>                 $array["lo_pays"],
				'lo_pays_short'=>           $array["lo_pays_short"]
			);
                        
		$req = "INSERT INTO usr_profils_localites SET
			modifie_le = NOW(),
			id_prestataire_profil = ".$this->db->quote($localite['id_prestataire_profil'], PDO::PARAM_STR).",
			lo_quartier = ".$this->db->quote($localite['lo_quartier'], PDO::PARAM_STR).",
			lo_ville = ".$this->db->quote($localite['lo_ville'], PDO::PARAM_STR).",
			lo_departement = ".$this->db->quote($localite['lo_departement'], PDO::PARAM_STR).",
			lo_departement_short = ".$this->db->quote($localite['lo_departement_short'], PDO::PARAM_STR).",
			lo_region = ".$this->db->quote($localite['lo_region'], PDO::PARAM_STR).",
			lo_region_short = ".$this->db->quote($localite['lo_region_short'], PDO::PARAM_STR).",
			lo_pays = ".$this->db->quote($localite['lo_pays'], PDO::PARAM_STR).",
			lo_pays_short = ".$this->db->quote($localite['lo_pays_short'], PDO::PARAM_STR);
		
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
				    
		if($id_profil_localite = $this->db->exec($req)){
			return $id_profil_localite; 
		}
	}
	private function rmvCvLocalite($array = array(), $debug=false){
		
		$req .= "DELETE FROM
			usr_profils_localites WHERE
			id_profil_localite = ".$this->db->quote($array['rmv_id_profil_localite'], PDO::PARAM_STR)."
			AND id_prestataire_profil = ".$this->db->quote($array['id_prestataire_profil'], PDO::PARAM_STR);

		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
		
		if($this->db->exec($req))
		
			return true;
	}
	private function rmvCvComplement($array = array(), $debug=false){
		
		$req .= "DELETE FROM
			usr_profils_complements WHERE
			id_profil_complement = ".$this->db->quote($array['rmv_id_profil_complement'], PDO::PARAM_STR)."
			AND id_prestataire_profil = ".$this->db->quote($array['id_prestataire_profil'], PDO::PARAM_STR);
	
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
		
		if($this->db->exec($req))
		
			return true;
	}
	protected function updProfil($array=array(), $ignore=array('page', 'action', 'crypt'), $debug=false){

		$req = " UPDATE usr_prestataires_profils SET ";
		
		foreach($array as $key=>$value){
			if(!in_array($key, $ignore)){
				
				$req .= $key." = ".$this->db->quote($value, PDO::PARAM_STR)." , ";
			}
		}		
		
		$req .= " modifie_le = NOW() WHERE id_identifiant=".$this->db->quote($array['id_identifiant'], PDO::PARAM_STR)." AND id_prestataire_profil = ".$this->db->quote($array['id_prestataire_profil'], PDO::PARAM_STR);

		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
		
				
		if($this->db->exec($req)){
			
			return $array['id_prestataire_profil'];
	
		}
				
	}
	
	protected function updManager($array=array(), $ignore=array('page', 'action', 'crypt'), $debug=false){

		$req = " UPDATE usr_clients_managers SET ";
		
		foreach($array as $key=>$value){
			if(!in_array($key, $ignore)){
				
				$req .= $key." = ".$this->db->quote($value, PDO::PARAM_STR)." , ";
			}
		}		
		
		$req .= " modifie_le = NOW() WHERE id_identifiant=".$this->db->quote($array['id_identifiant'], PDO::PARAM_STR)." AND id_client_manager = ".$this->db->quote($array['id_client_manager'], PDO::PARAM_STR);

		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
		

		if($this->db->exec($req)){
			return $array['id_client_manager'];
	
		}
		
	}
	
	private function saveClient($array=array(), $ignore=array(), $debug=false){
		
		$req .= "INSERT INTO usr_clients SET " ;
		
		foreach($array as $key=>$value){
			if(!in_array($key, $ignore))
				$req .= $key." = ".$this->db->quote($value, PDO::PARAM_STR)." , ";
		}		
		
		$req .= " modifie_le = NOW() ";
			
		
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
		
		if($this->db->exec($req)){
			
			return $this->db->lastInsertId();
	
		}
	}
	
	private function savePrestataire($array=array(), $ignore=array(), $debug=false){
		
		$req .= "INSERT INTO usr_prestataires SET " ;
		
		foreach($array as $key=>$value){
			if(!in_array($key, $ignore))
				$req .= $key." = ".$this->db->quote($value, PDO::PARAM_STR)." , ";
		}		
		
		$req .= " modifie_le = NOW() ";
				
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
		
		if($this->db->exec($req)){
			
			return $this->db->lastInsertId();
	
		}
	}
	private function updPrestataire($array=array(), $ignore=array(), $debug=false){
		
		$req .= "UPDATE usr_prestataires SET " ;
		
		foreach($array as $key=>$value){
			if(!in_array($key, $ignore))
				$req .= $key." = ".$this->db->quote($value, PDO::PARAM_STR)." , ";
		}		
		
		$req .= " modifie_le = NOW() WHERE id_identifiant= ".$this->db->quote($array['id_identifiant'], PDO::PARAM_STR);
		
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
		
		if($this->db->exec($req)){
			
			return $array['id_identifiant'];
	
		}
	}
	private function updClient($array=array(), $ignore=array(), $debug=false){
		
		$req .= "UPDATE usr_clients SET " ;
		
		foreach($array as $key=>$value){
			if(!in_array($key, $ignore))
				$req .= $key." = ".$this->db->quote($value, PDO::PARAM_STR)." , ";
		}		
		
		$req .= " modifie_le = NOW() WHERE id_client= ".$this->db->quote($array['id_client'], PDO::PARAM_STR);
		
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
		
		if($this->db->exec($req)){
			
			return $array['id_client'];
	
		}
	}
	public function getIdentifiant($filtre=array(), $debug=false){
		
		$req .= "SELECT * FROM usr_identifiants WHERE modifie_le ";
		
		foreach($filtre as $key=>$value){
			if($key== 'usr_password')
				$req .= "AND ".$key." = PASSWORD(".$this->db->quote($value, PDO::PARAM_STR).") ";
			else
				$req .= "AND ".$key." = ".$this->db->quote($value, PDO::PARAM_STR);
		}
		
		$req .= " ORDER BY cree_le DESC";
		
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
		
		$resultats = $this->db->query($req);
		$ligne = $resultats->fetch(PDO::FETCH_ASSOC);
			
		$resultats->closeCursor(); 
		
		
		
		if($ligne['id_identifiant'])
			return $ligne;
	
	}
	public function getClient($id_identifiant){
		
		$req = "SELECT
			UI.id_identifiant, UI.usr_email, UI.usr_trad,
			UC.cl_standard_indicatif, UC.cl_standard, UC.cl_entreprise, UC.cl_presentation, UC.cl_effectif, UC.cl_siret, UC.cl_siteweb, UC.cl_add_liv_rue, UC.cl_add_liv_codepostal,  UC.cl_add_liv_ville,  UC.cl_add_liv_pays, UC.cl_add_fac_rue, UC.cl_add_fac_codepostal,  UC.cl_add_fac_ville, UC.cl_add_fac_pays,
			UCM.id_client_manager, UCM.id_client, UCM.id_role, UCM.id_civilite, AC.adm_civilite_".$_SESSION['langue']." AS adm_civilite, UCM.mg_nom, UCM.mg_prenom, UCM.mg_portable_indicatif, UCM.mg_portable, UCM.mg_fixe_indicatif, UCM.mg_fixe,
			UJ.jbr_loggin, UJ.jbr_password
			FROM usr_clients_managers AS UCM
			INNER JOIN usr_clients AS UC ON UCM.id_client = UC.id_client
			LEFT OUTER JOIN adm_civilites AS AC ON  AC.id_civilite = UCM.id_civilite
			INNER JOIN usr_identifiants AS UI ON UI.id_identifiant = UCM.id_identifiant
			LEFT OUTER JOIN usr_jabbers AS UJ ON  UJ.id_identifiant = UI.id_identifiant
			WHERE UI.id_identifiant = ".$this->db->quote($id_identifiant, PDO::PARAM_STR);
		
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
		
		$resultats = $this->db->query($req);
		$client = $resultats->fetch(PDO::FETCH_ASSOC);
		
		$resultats->closeCursor();
		//print_r($manager);
		if(isset($client['id_client'])){
			return new Client($client);
		}
		else{
			return 'Récuperation du client impossible !';
		}
		
	}
	
	
	public function getListClients($filtre = array(), $searchFiltre=array(), $limit=array(0,NOMBRE_PER_PAGE), $matching=false){
		
		$clients = array();
		
		$counter = array(
				'COUNT' => 'SELECT count(DISTINCT(UI.id_identifiant)) AS total, ' ,
				'SELECT'=> 'SELECT DISTINCT(UI.id_identifiant), '
				);
		
		$req = " UI.usr_email, UI.usr_trad,
			UC.cl_standard_indicatif, UC.cl_standard, UC.cl_entreprise, UC.cl_presentation, UC.cl_effectif, UC.cl_siret, UC.cl_siteweb, UC.cl_add_liv_rue, UC.cl_add_liv_codepostal,  UC.cl_add_liv_ville,  UC.cl_add_liv_pays, UC.cl_add_fac_rue, UC.cl_add_fac_codepostal,  UC.cl_add_fac_ville, UC.cl_add_fac_pays,
			UCM.modifie_le, UCM.id_client_manager, UCM.id_client, UCM.id_role, UCM.id_civilite, AC.adm_civilite_".$_SESSION['langue']." AS adm_civilite, UCM.mg_nom, UCM.mg_prenom, UCM.mg_portable_indicatif, UCM.mg_portable, UCM.mg_fixe_indicatif, UCM.mg_fixe,
			UJ.jbr_loggin, UJ.jbr_password
			FROM usr_clients_managers AS UCM
			INNER JOIN usr_clients AS UC ON UCM.id_client = UC.id_client
			LEFT OUTER JOIN adm_civilites AS AC ON  AC.id_civilite = UCM.id_civilite
			INNER JOIN usr_identifiants AS UI ON UI.id_identifiant = UCM.id_identifiant
			LEFT OUTER JOIN usr_jabbers AS UJ ON  UJ.id_identifiant = UI.id_identifiant
			
			WHERE UCM.id_role =".$this->db->quote(self::TYPE_ADMIN, PDO::PARAM_STR);
		
		if(count($searchFiltre['mots_cle']) && !empty($searchFiltre['mots_cle'])){
				
				/*ON VA BASCULER TOUTES NOS ENTREES RECU EN TABLEAU*/
				/*On sous entend que les valeur seront déparé par des virgules dans le moteur de recherche*/
				$keywords   = array();
				if(!is_array($searchFiltre['mots_cle']))
					$keywords = explode(',' , $searchFiltre['mots_cle']);
				else
					$keywords = $searchFiltre['mots_cle'];
				
				$string = implode(" ", $keywords);

				$req  .=   "AND  MATCH (UCM.mg_nom, UCM.mg_prenom, UI.usr_email, UC.cl_entreprise, UC.cl_presentation) AGAINST ('$string*' IN BOOLEAN MODE)";
			}
			
		$req .= " ORDER BY UC.modifie_le DESC  ";
		
		if(is_array($limit)){

			$limit = array(
					'COUNT' => '' ,
					'SELECT'=> "LIMIT ".implode(",", $limit)
					);
		}

		//Tools::debugVar($counter['COUNT'].$req.$limit['COUNT']);
		if($debug)
			throw new myException($counter['COUNT'].$req.$limit['COUNT']);
		if(DEBUG)
			parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$counter['COUNT'].$req.$limit['COUNT']);	
		//Le count avec la requete
		$resultats = $this->db->query($counter['COUNT'].$req.$limit['COUNT']);
		
		//si on un total superieur a zéro
		//on reexecute la requete afin de recuperer les elements
                if($count = $resultats->fetch(PDO::FETCH_OBJ)->total){
			
			if($debug)
				throw new myException($counter['SELECT'].$req.$limit['SELECT']);
			if(DEBUG)
				parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$counter['SELECT'].$req.$limit['SELECT']);
			
 			$resultats = $this->db->query($counter['SELECT'].$req.$limit['SELECT']);
			
			while ($client = $resultats->fetch(PDO::FETCH_ASSOC))
				 $clients[] = new Client($client);	
			
			$resultats->closeCursor();
			
			
                }
		
		return array('count'=>$count, 'clients'=>$clients);
	}
	public function getAdministrateur($id_identifiant){
		
		$req = "SELECT
			UI.id_identifiant, UI.usr_email, UI.usr_trad
			FROM usr_identifiants AS UI
			WHERE UI.id_identifiant = ".$this->db->quote($id_identifiant, PDO::PARAM_STR);
		
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
		
		$resultats = $this->db->query($req);
		$client = $resultats->fetch(PDO::FETCH_ASSOC);
		
		$resultats->closeCursor();
		
		if(isset($client['id_identifiant'])){
			return new Manager($client);
		}
		else{
			return 'Récuperation du client impossible !';
		}
		
	}
 	public function getListProfils($filtre = array(), $searchFiltre=array(), $limit=array(0,NOMBRE_PER_PAGE), $matching=false, $forceAbonnement=false){
		//petite securité pour evité l'envoie d'espaces blancs
		if(!is_array($searchFiltre['mots_cle']))
			$searchFiltre['mots_cle'] = trim($searchFiltre['mots_cle']);
		$profils        = array();
		$matchingResult = false;
		$new_profils    = 0;
		$counter = array(
				'COUNT' => 'SELECT count(DISTINCT(UPP.id_prestataire_profil)) AS total, ' ,
				'SELECT'=> 'SELECT DISTINCT(UPP.id_prestataire_profil), '
				);
		$req = "UPP.id_prestataire, UPP.id_identifiant,  UP.id_structure, UP.pr_notification_mail, adm_structures.adm_structure_fr AS adm_structure,
			UPP.id_experience, UPP.id_prestataire_profil, UPP.pf_reference, UPP.id_role, UPP.id_civilite, UPP.pf_nom,  UPP.pf_active_pdf,   UPP.pf_profil_statut,  (UPP.pf_pourcentage_identite+UPP.pf_pourcentage_formation+UPP.pf_pourcentage_experience+UPP.pf_pourcentage_competence) AS pf_pourcentage,
			UPP.pf_pourcentage_identite, UPP.pf_pourcentage_formation, UPP.pf_pourcentage_experience, UPP.pf_pourcentage_competence,
			UPP.pf_prenom, UPP.pf_portable_indicatif, UPP.pf_portable, UPP.pf_fixe_indicatif, UPP.pf_fixe, UPP.pf_naissance, UPP.pf_disponibilite, UPP.pf_siteweb, UPP.pf_permis, UPP.pf_tjm, UPP.pf_cv_titre, UPP.pf_cv_description, UPP.id_monnaie, UPP.id_monnaie,
			adm_monnaies.adm_monnaie, UI.usr_email,
			UJ.jbr_loggin, UJ.jbr_password
			FROM usr_prestataires_profils AS UPP
			INNER JOIN usr_prestataires AS UP ON UP.id_identifiant = UPP.id_identifiant
			INNER JOIN usr_identifiants AS UI ON UI.id_identifiant = UP.id_identifiant
			LEFT OUTER JOIN adm_monnaies ON adm_monnaies.id_monnaie = UPP.id_monnaie
			LEFT OUTER JOIN adm_structures ON adm_structures.id_structure = UP.id_structure
			LEFT OUTER JOIN usr_profils_localites AS UPL ON UPL.id_prestataire_profil = UPP.id_prestataire_profil
			LEFT OUTER JOIN usr_jabbers AS UJ ON UJ.id_identifiant = UP.id_identifiant
			WHERE UPP.modifie_le
			/*ATTENTION : Il n'est pas question d'appeler les profils administrateurs des comptes AGENCE !*/
			AND ((UP.id_structure = ".$this->db->quote(self::TYPE_AGENCE, PDO::PARAM_STR)." AND UPP.id_role = ".$this->db->quote(self::TYPE_NORMAL, PDO::PARAM_STR).")
			OR UP.id_structure != ".$this->db->quote(self::TYPE_AGENCE, PDO::PARAM_STR).") ";
		
		foreach($filtre as $key=>$value){
				if(is_array($value)){
					if(count($value))
						$req .= " AND UPP.".$key." IN ('".implode("','", $value)."')";
					else
						$req .= " AND UPP.".$key." IN ('".implode(',', $value)."')";	
				}
				else{
					//On recupere les CV qio sont rempli un minum de 10%
					if($key=='pf_pourcentage')
						$req .= " AND  (UPP.pf_pourcentage_identite+UPP.pf_pourcentage_formation+UPP.pf_pourcentage_experience+UPP.pf_pourcentage_competence) >= ".$this->db->quote($value, PDO::PARAM_STR);
					
					else
						$req .= " AND UPP.".$key." = ".$this->db->quote($value, PDO::PARAM_STR);
				}
		}		
		
		//Si on a des entrées dans la recherche
		if(is_array($searchFiltre) && count($searchFiltre)){
			
				
			if((count($searchFiltre['mots_cle']) && !empty($searchFiltre['mots_cle'])) || (count($searchFiltre['id_langue']) && !empty($searchFiltre['id_langue'])) || (count($searchFiltre['id_poste']) && !empty($searchFiltre['id_poste'])) || (count($searchFiltre['id_competence']) && !empty($searchFiltre['id_competence']))){
				//Tools::debugVar($searchFiltre);
				/*ON VA BASCULER TOUTES NOS ENTREES RECU EN TABLEAU*/
				/*On sous entend que les valeur seront déparé par des virgules dans le moteur de recherche*/
				$keywords   = array();
				//Est ce que l'on a demandé une recherche sur mot_cle
				if(!is_array($searchFiltre['mots_cle']) &&  isset($searchFiltre['mots_cle'])){
					if(empty($searchFiltre['mots_cle']))
						$keywords = array();
					else
						$keywords = explode(',' , $searchFiltre['mots_cle']);
					
				}
				else
					$keywords= $searchFiltre['mots_cle'];
				
				$id_postes   = array();
				if(isset($searchFiltre['id_poste'])){
					if(!is_array($searchFiltre['id_poste']))
						$id_postes = explode(',' , $searchFiltre['id_poste']);
					else
						$id_postes= $searchFiltre['id_poste'];
				}
					
				$id_competences   = array();
				if(isset($searchFiltre['id_competence'])){
					if(!is_array($searchFiltre['id_competence']))
						$id_competences = explode(',' , $searchFiltre['id_competence']);
					else
						$id_competences= $searchFiltre['id_competence'];
				}
					
				$id_langues   = array();
				if(isset($searchFiltre['id_langue'])){
					if(!is_array($searchFiltre['id_langue']))
						$id_langues = explode(',' , $searchFiltre['id_langue']);
					else
						$id_langues= $searchFiltre['id_langue'];
				}	
				
				//On recupere les profil qui utilisent ces mots cle en competence ou poste ou langue ou formation ou experience
				if(count($keywords) || count($id_competences))
					$listCompetences = $this->getListCvCompetences(array(), $keywords, $id_competences);
				//Tools::debugVar($listCompetences, false, 'list competences');
				if(count($keywords) || count($id_postes))
					$listPostes      = $this->getListCvPostes(array(), $keywords, $id_postes);
				//Tools::debugVar($listPostes, false, 'list postes');
				if(count($keywords) || count($id_langues))
					$listLangues     = $this->getListCvLangues(array(), $keywords, $id_langues);
				//Tools::debugVar($listLangues, false, 'list langues');
				if(count($keywords))
					$listFormations  = $this->getListCvFormations(array(), $keywords);
				//Tools::debugVar($listFormations, false, 'list formatio,');
				if(count($keywords))
					$listExperiences = $this->getListCvExperiences(array(), $keywords);
				//Tools::debugVar($listExperiences, false, 'list experience');
				$entryMatch      = array();
				
				$req .= "AND ( ";
				$implComplement = array();
				if(count($searchFiltre['mots_cle']) && !empty($searchFiltre['mots_cle'])){
					$string = addslashes(implode("* ", $keywords)); 
					$req  .=   " ( MATCH (UPP.pf_cv_titre, UPP.pf_cv_description, UPP.pf_cv_pdf_string) AGAINST ('$string*' IN BOOLEAN MODE)";
					
					
					if($listCompetences['count'] || $listPostes['count'] || $listLangues['count'] || $listFormations['count'] || $listExperiences['count']){
						$req .= " ) OR ";
					}
					else{
						
						$req .= " )  ";
					}
				}
				
				$competences = array();
				if($listCompetences['count']>0){
					
					foreach($listCompetences['competences'] as $key){
					
						$entryMatch[]=$key->id_prestataire_profil;
						$competences[] = $key->id_prestataire_profil;
					}
					$competences = array_unique($competences);
					//Tools::debugVar($competences, false, 'Competecnces');
				}

				$postes = array();
				if($listPostes['count']>0){
					
					foreach($listPostes['postes'] as $key){
					
						$entryMatch[]=$key->id_prestataire_profil;
						$postes[] = $key->id_prestataire_profil;
					}
					$postes = array_unique($postes);
					//Tools::debugVar($postes, false, 'postes');
				}
				
				$langues = array();
				if($listLangues['count']>0){
					
					foreach($listLangues['langues'] as $key){
					
						$entryMatch[]=$key->id_prestataire_profil;
						$langues[] = $key->id_prestataire_profil;
						
					}
					$langues = array_unique($langues);
					//Tools::debugVar($langues, false, 'langues');
				}
				
				$formations = array();
				if($listFormations['count']>0){
					
					foreach($listFormations['formations'] as $key){
					
						$entryMatch[]=$key->id_prestataire_profil;
						$formations[] = $key->id_prestataire_profil;
						
					}
					$formations = array_unique($formations);
					//Tools::debugVar($formations, false, 'formations');
				}

				
				$experiences = array();
				if($listExperiences['count']>0){
					
					foreach($listExperiences['experiences'] as $key){
					
						$entryMatch[]=$key->id_prestataire_profil;
						$experiences[]=$key->id_prestataire_profil;
						
					}
					$experiences = array_unique($experiences);
					//Tools::debugVar($experiences, false, 'experiences');
				}

				
				if($matching){
					//Attention cette methode est categorique !
					//Elle ne gardera que les missions qui ont à la fois les competences et les langues demandés
					//Si une mission ne possède pas de langue elle ne resortira jamais
					//En cas de doute mieux vaut utiliser :  $implComplement = $competences;
					$implComplement = array_intersect($postes, $competences/*,$langues*/);//permet de definir les valeur qui sont presentes dans les deux tableaux
					//Tools::debugVar($implComplement, false, "matching Complements");
					//$implComplement = $competences;
					
					$matchingResult = true;
					
					if(count($searchFiltre['adm_competence']) || count($searchFiltre['adm_poste'])){
						
						$req  .=   "(";
					}
					
					if(count($searchFiltre['adm_competence'])){//Si on recupere des adm_competence dans getListMissionsMatchProfils
						
						$string = addslashes(implode("* ", $searchFiltre['adm_competence'])); 
						$req  .=   "  MATCH (UPP.pf_cv_titre, UPP.pf_cv_description, UPP.pf_cv_pdf_string) AGAINST ('$string*')/*On chereche les adm_competence des mission dans les profils*/";
					}
					
					if(count($searchFiltre['adm_competence']) && count($searchFiltre['adm_poste'])){
						$req  .=   ' AND ';
					}
					
					if(count($searchFiltre['adm_poste'])){//Si on recupere des adm_competence dans getListMissionsMatchProfils
						$string = addslashes(implode("* ", $searchFiltre['adm_poste'])); 
						$req  .=   "  MATCH (UPP.pf_cv_titre, UPP.pf_cv_description, UPP.pf_cv_pdf_string) AGAINST ('$string*')/*On chereche les adm_poste des mission dans les profils*/";
					}
					
					if(count($searchFiltre['adm_competence']) || count($searchFiltre['adm_poste'])){
						$req  .=   ')  ';
					}
				}
				else
					$implComplement = array_unique($entryMatch);
				
				//Tools::debugVar($implComplement, true, "ALl match");
				
				if(count($implComplement)>0){
					$matchingResult = true;
					if(count($searchFiltre['adm_competence']) || count($searchFiltre['adm_poste'])){
						$req .= "OR ";
					}
					$req .= " (UPP.id_prestataire_profil IN (".implode(',',$implComplement)."))  ";			
				}
				elseif($matching)//si aucune entré trouvée on envoie le 1=1 pour eviter erreur sql et du coup afficher des profils...
					$req .= "AND 1 = 1";
				
				$req .= " ) ";
				
			}
			if(count($searchFiltre['lo_pays_short']) && !empty($searchFiltre['lo_pays_short'])){
			
				$listLoc['lo_departement_short']   = array();
				if(!is_array($searchFiltre['lo_departement_short'])){

					if(!empty($searchFiltre['lo_departement_short'])){
						
						$listLoc['lo_departement_short'] = explode(',' , $searchFiltre['lo_departement_short']);
					}
				}
				else{
					
					$listLoc['lo_departement_short'] = $searchFiltre['lo_departement_short'];
				}
				
				if(count($listLoc['lo_departement_short'])){
					$listLoc['lo_departement_short'] = array_unique($listLoc['lo_departement_short']);
				}
				
				$listLoc['lo_region_short']   = array();
				if(!is_array($searchFiltre['lo_region_short'])){
					if(!empty($searchFiltre['lo_region_short'])){
						$listLoc['lo_region_short'] = explode(',' , $searchFiltre['lo_region_short']);
					}
				}
				else{
					$listLoc['lo_region_short'] = $searchFiltre['lo_region_short'];
				}
				
				if(count($listLoc['lo_region_short'])){
					$listLoc['lo_region_short'] = array_unique($listLoc['lo_region_short']);
				}
				
				$listLoc['lo_pays_short']   = array();
				if(!is_array($searchFiltre['lo_pays_short'])){
					if(!empty($searchFiltre['lo_pays_short'])){
						$listLoc['lo_pays_short'] = explode(',' , $searchFiltre['lo_pays_short']);
					}
				}
				else{
					$listLoc['lo_pays_short'] = $searchFiltre['lo_pays_short'];
				}
				
				if(count($listLoc['lo_pays_short'])){
					$listLoc['lo_pays_short'] = array_unique($listLoc['lo_pays_short']);
				}
				
				if(count($listLoc)){
					$req .= "AND ("; 
					
					$i=0;
					foreach($listLoc as $k=>$v){
						$i++;
						if($v){
							
							$req .= " UPL.".$k."  IN ('".implode("','", $v)."') ";
							
							if($i < count($listLoc)){
								$req .= " AND ";
							}
						}
						
					}
					
					
					$req .= " ) "; 
				}
				
				
				
			}
			if(is_numeric($searchFiltre['id_experience']) && !empty($searchFiltre['id_experience'])){
				
				$id_experience   = array();
				$id_experience[] = $searchFiltre['id_experience'];
				
				if(is_array($id_experience)){
					$imExperience = implode(',',$id_experience);
					if($imExperience>0)
						$req .= "  AND (UPP.id_experience >=" .$imExperience." ) ";
					else
						$req .= "  AND (UPP.id_experience < 1 ) ";
				}

			}
			
			if(isset($searchFiltre['disponibilite']) && !empty($searchFiltre['disponibilite'])){
				
				if(Tools::verifDate($searchFiltre['disponibilite'])){
					
					$disponibilite = Tools::shortConvertFrDateToDb($searchFiltre['disponibilite']);
					$req .= " AND ( /*Si la date est pas defini on retourne la date du jour sinon la définie*/ IF(DATE_FORMAT(UPP.pf_disponibilite , '%Y')>0, UPP.pf_disponibilite , CURDATE()) <= '$disponibilite')";
				}
			}
			
			if(!empty($searchFiltre['modifie_le'])){
				
				$req .= "AND ( UPP.modifie_le BETWEEN DATE_SUB( NOW( ) , INTERVAL ".$searchFiltre['modifie_le'].")  AND NOW( ) ) ";
				
			}		
						

			
			if(isset($searchFiltre['id_prestataire_profil']) && !empty($searchFiltre['id_prestataire_profil'])){
				
				$req .= " AND ( UPP.id_prestataire_profil  IN (".$searchFiltre['id_prestataire_profil'].")) ";			
			}
		}
		
		$req .= " ORDER BY UPP.modifie_le AND pf_pourcentage DESC  ";
		
		if(is_array($limit)){

			$limit = array(
					'COUNT' => '' ,
					'SELECT'=> "LIMIT ".implode(",", $limit)
					);
		}

		//Tools::debugVar($counter['COUNT'].$req.$limit['COUNT']);
		if($debug)
			throw new myException($counter['COUNT'].$req.$limit['COUNT']);
		if(DEBUG)
			parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$counter['COUNT'].$req.$limit['COUNT']);	
		//Le count avec la requete
		$resultats = $this->db->query($counter['COUNT'].$req.$limit['COUNT']);
		
		//si on un total superieur a zéro
		//on reexecute la requete afin de recuperer les elements
                if($count = $resultats->fetch(PDO::FETCH_OBJ)->total){
			
			if($debug)
				throw new myException($counter['SELECT'].$req.$limit['SELECT']);
			if(DEBUG)
				parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$counter['SELECT'].$req.$limit['SELECT']);
			
 			$resultats = $this->db->query($counter['SELECT'].$req.$limit['SELECT']);
			
			while ($profil = $resultats->fetch(PDO::FETCH_ASSOC)){
				
				//On recupere maintenant la date actuelle
				$var_day        = date('Y-m-d H:m:s');
				
				//Difference
				$d1             = new DateTime($var_day);
				$d2             = new DateTime($profil['modifie_le']);
				$difDay         = $d1->diff($d2)->format('%d');
				
				if($difDay<NOMBRE_DAY_NEW){
					$new_profils +=1;
				}
				
				 $profils[] = new Profil($profil, $forceAbonnement);	
			}
			
			$resultats->closeCursor();
			
			
                }
		
		return array('count'=>$count, 'profils'=>$profils, 'new_profils'=>$new_profils, 'matchingResult'=>$matchingResult);
		
		
	}
	public function getListManagers($filtre = array(), $searchFiltre=array(), $limit=array(0,NOMBRE_PER_PAGE), $matching=false){
		$managers = array();
		$counter = array(
				'COUNT' => 'SELECT count(DISTINCT(UM.id_client_manager)) AS total, ' ,
				'SELECT'=> 'SELECT DISTINCT(UM.id_client_manager), '
				);
		$req = " UI.id_identifiant, UI.usr_email, UI.usr_trad,
			UM.id_client, UM.id_role ,UM.id_civilite,UM.mg_nom, UM.mg_prenom,  UM.mg_portable_indicatif, UM.mg_portable, UM.mg_fixe_indicatif, UM.mg_fixe
			FROM usr_clients_managers AS UM
			INNER JOIN usr_identifiants AS UI ON UM.id_identifiant = UI.id_identifiant
			WHERE UM.modifie_le AND UI.usr_activate = 1 ";
			
		
		foreach($filtre as $key=>$value){
				if(is_array($value)){
					if(count($value))
						$req .= " AND UM.".$key." IN ('".implode("','", $value)."')";
					else
						$req .= " AND UM.".$key." IN ('')";	
				}
				else
					$req .= " AND UM.".$key." = ".$this->db->quote($value, PDO::PARAM_STR);
		}		
		
		
		$req .= " ORDER BY UM.modifie_le DESC  ";
		if(is_array($limit)){
			$limit = array(
					'COUNT' => '' ,
					'SELECT'=> "LIMIT ".implode(",", $limit)
					);
		}
	
		if($debug)
			throw new myException($counter['COUNT'].$req.$limit['COUNT']);
		if(DEBUG)
			parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$counter['COUNT'].$req.$limit['COUNT']);	
		//Le count avec la requete
		$resultats = $this->db->query($counter['COUNT'].$req.$limit['COUNT']);
		
		//si on un total superieur a zéro
		//on reexecute la requete afin de recuperer les elements
                if($count = $resultats->fetch(PDO::FETCH_OBJ)->total){
			if($debug)
				throw new myException($counter['SELECT'].$req.$limit['SELECT']);
			if(DEBUG)
				parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$counter['SELECT'].$req.$limit['SELECT']);
			
 			$resultats = $this->db->query($counter['SELECT'].$req.$limit['SELECT']);
			
			while ($manager = $resultats->fetch(PDO::FETCH_ASSOC))
				 $managers[] = new Manager($manager);	
			
			$resultats->closeCursor();
			
			
                }
		
		return array('count'=>$count, 'managers'=>$managers);
		
		
	}
	public function getPrestataire($id_identifiant){
		
		$req = "SELECT UI.id_identifiant, UI.usr_email, UI.usr_trad, UI.id_type, 
			UP.id_prestataire, UP.pr_notification_mail,UP.pr_standard_indicatif, UP.pr_standard, UP.id_prestataire, UP.id_structure, adm_structures.adm_structure_fr AS adm_structure, UP.pr_entreprise, UP.pr_presentation, UP.pr_add_liv_rue, UP.pr_add_liv_codepostal, UP.pr_add_liv_ville,  UP.pr_add_liv_pays, UP.pr_add_fac_rue, UP.pr_add_fac_codepostal, UP.pr_add_fac_ville, UP.pr_add_fac_pays,
			UPP.pf_nom, UPP.pf_prenom, UPP.id_prestataire_profil,
			(UPP.pf_pourcentage_identite+UPP.pf_pourcentage_formation+UPP.pf_pourcentage_experience+UPP.pf_pourcentage_competence) AS pf_pourcentage,
			UPP.id_civilite,  AC.adm_civilite_".$_SESSION['langue']." AS adm_civilite,
			UJ.jbr_loggin, UJ.jbr_password
			FROM usr_prestataires AS UP
			INNER JOIN usr_identifiants AS UI ON UP.id_identifiant = UI.id_identifiant
			LEFT OUTER JOIN adm_structures ON adm_structures.id_structure = UP.id_structure
			LEFT OUTER JOIN usr_prestataires_profils AS UPP ON UPP.id_identifiant = UP.id_identifiant
			LEFT OUTER JOIN adm_civilites AS AC ON  AC.id_civilite = UPP.id_civilite
			LEFT OUTER JOIN usr_jabbers AS UJ ON  UJ.id_identifiant = UI.id_identifiant
			
			WHERE UPP.id_role =".$this->db->quote(self::TYPE_ADMIN, PDO::PARAM_STR)." AND UP.id_identifiant = ".$this->db->quote($id_identifiant, PDO::PARAM_STR);
		
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
		
		$resultats = $this->db->query($req);
		
		$prestataire = $resultats->fetch(PDO::FETCH_ASSOC);
		
		$resultats->closeCursor();
		
		if(!DEBUG)
			return new Prestataire($prestataire);
		
		if(isset($prestataire['id_prestataire'])){
			return new Prestataire($prestataire);
		}
		else{
			return 'Récuperation du prestataire impossible !';
		}
	
		
	}
	
	public function getListPrestataires($filtre = array(), $searchFiltre=array(), $limit=array(0,NOMBRE_PER_PAGE), $matching=false){
		
		$prestataires = array();
		$counter = array(
				'COUNT' => 'SELECT count(DISTINCT(UI.id_identifiant)) AS total, ' ,
				'SELECT'=> 'SELECT DISTINCT(UI.id_identifiant), '
				);
		
		$req = " UI.usr_email, UI.usr_trad, UI.id_type, 
			UP.id_prestataire, UP.pr_notification_mail,UP.pr_standard_indicatif, UP.pr_standard, UP.id_prestataire, UP.id_structure, adm_structures.adm_structure_fr AS adm_structure, UP.pr_entreprise, UP.pr_presentation, UP.pr_add_liv_rue, UP.pr_add_liv_codepostal, UP.pr_add_liv_ville,  UP.pr_add_liv_pays, UP.pr_add_fac_rue, UP.pr_add_fac_codepostal, UP.pr_add_fac_ville, UP.pr_add_fac_pays,
			UPP.pf_nom, UPP.pf_prenom, UPP.id_prestataire_profil,
			(UPP.pf_pourcentage_identite+UPP.pf_pourcentage_formation+UPP.pf_pourcentage_experience+UPP.pf_pourcentage_competence) AS pf_pourcentage,
			UPP.modifie_le, UPP.id_civilite,  AC.adm_civilite_".$_SESSION['langue']." AS adm_civilite,
			UJ.jbr_loggin, UJ.jbr_password
			FROM usr_prestataires AS UP
			INNER JOIN usr_identifiants AS UI ON UP.id_identifiant = UI.id_identifiant
			LEFT OUTER JOIN adm_structures ON adm_structures.id_structure = UP.id_structure
			LEFT OUTER JOIN usr_prestataires_profils AS UPP ON UPP.id_identifiant = UP.id_identifiant
			LEFT OUTER JOIN adm_civilites AS AC ON  AC.id_civilite = UPP.id_civilite
			LEFT OUTER JOIN usr_jabbers AS UJ ON  UJ.id_identifiant = UI.id_identifiant
			
			WHERE UPP.id_role =".$this->db->quote(self::TYPE_ADMIN, PDO::PARAM_STR);
			
			if(count($searchFiltre['mots_cle']) && !empty($searchFiltre['mots_cle'])){
				
				/*ON VA BASCULER TOUTES NOS ENTREES RECU EN TABLEAU*/
				/*On sous entend que les valeur seront déparé par des virgules dans le moteur de recherche*/
				$keywords   = array();
				if(!is_array($searchFiltre['mots_cle']))
					$keywords = explode(',' , $searchFiltre['mots_cle']);
				else
					$keywords = $searchFiltre['mots_cle'];
				
				$string = implode(" ", $keywords);

				$req  .=   "AND  MATCH (UPP.pf_nom, UPP.pf_prenom, UI.usr_email, UP.pr_entreprise, UP.pr_presentation) AGAINST ('$string*' IN BOOLEAN MODE)";
			}
			
			if(is_numeric($searchFiltre['id_structure']) && !empty($searchFiltre['id_structure'])){
				
				$id_structure   = array();
				$id_structure[] = $searchFiltre['id_structure'];
				
				if(is_array($id_structure)){
					$imStructure = implode(',',$id_structure);
					$req .= " AND ( UP.id_structure  IN (".$imStructure.") )";
				}

			}
			
			
		$req .= " ORDER BY UPP.modifie_le DESC  ";
		
		if(is_array($limit)){

			$limit = array(
					'COUNT' => '' ,
					'SELECT'=> "LIMIT ".implode(",", $limit)
					);
		}

		//Tools::debugVar($counter['COUNT'].$req.$limit['COUNT']);
		if($debug)
			throw new myException($counter['COUNT'].$req.$limit['COUNT']);
		if(DEBUG)
			parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$counter['COUNT'].$req.$limit['COUNT']);	
		//Le count avec la requete
		$resultats = $this->db->query($counter['COUNT'].$req.$limit['COUNT']);
		
		//si on un total superieur a zéro
		//on reexecute la requete afin de recuperer les elements
                if($count = $resultats->fetch(PDO::FETCH_OBJ)->total){
			
			if($debug)
				throw new myException($counter['SELECT'].$req.$limit['SELECT']);
			if(DEBUG)
				parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$counter['SELECT'].$req.$limit['SELECT']);
			
 			$resultats = $this->db->query($counter['SELECT'].$req.$limit['SELECT']);
			
			while ($prestataire = $resultats->fetch(PDO::FETCH_ASSOC))
				 $prestataires[] = new Prestataire($prestataire);	
			
			$resultats->closeCursor();
			
			
                }
		
		return array('count'=>$count, 'prestataires'=>$prestataires);
	
		
	}
	
	public function getListCvFormations($filtre = array(), $search=array(), $justCount=false){
		
		$formations = array();
		$counter = array(
				'COUNT' => 'SELECT count(id_profil_formation) AS total, ' ,
				'SELECT'=> 'SELECT id_profil_formation, '
				);

		$req = "id_profil_formation, id_prestataire_profil,  form_debut, form_fin, form_description, form_diplome, form_ecole
			FROM usr_profils_formations 
			WHERE cree_le ";
		
		foreach($filtre as $key=>$value){
			
				$req .= " AND ".$key." = ".$this->db->quote($value, PDO::PARAM_STR);
		}

		if(count($search)){
			$string = addslashes(implode("* ", $search)); 
			$req .= " AND MATCH (form_description, form_diplome, form_ecole) AGAINST ('$string*' IN BOOLEAN MODE)";
		}
		if($debug)
			throw new myException($counter['COUNT'].$req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$counter['COUNT'].$req);
	
		$resultats = $this->db->query($counter['COUNT'].$req);
	
		if($count = $resultats->fetch(PDO::FETCH_OBJ)->total){
			if($justCount)
				return array('count'=>$count, 'formations'=>$formations);
			
			if($debug)
				throw new myException($counter['SELECT'].$req);
			if(DEBUG)
				parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$counter['SELECT'].$req);
			
			$resultats = $this->db->query($counter['SELECT'].$req);
	
			
			while ($formation = $resultats->fetch(PDO::FETCH_ASSOC)){
				$formation["form_debut"] = Tools::shortConvertDBDateToLan($formation["form_debut"]);
				$formation["form_fin"] = Tools::shortConvertDBDateToLan($formation["form_fin"]);
				$formations[] = (object)$formation;	
			}
			$resultats->closeCursor();
		}
		return array('count'=>$count, 'formations'=>$formations);
	}
	public function getListCvLocalites($filtre = array()){
		
		$localites = array();
		$counter = array(
				'COUNT' => 'SELECT count(id_profil_localite) AS total, ' ,
				'SELECT'=> 'SELECT id_profil_localite, '
				);
	
		$req = " id_prestataire_profil,  
			lo_quartier, lo_ville, lo_code_postal, lo_departement_short, lo_departement, lo_region_short,  lo_region,  lo_pays_short, lo_pays
			FROM usr_profils_localites 
			WHERE cree_le ";
		
		foreach($filtre as $key=>$value){
			if(is_array($value) && count($value))
				$req .= " AND ".$key." IN ('".implode("','", $value)."')";
			else
				$req .= " AND ".$key." = ".$this->db->quote($value, PDO::PARAM_STR);
		}
		
		if($debug)
			throw new myException($counter['COUNT'].$req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$counter['COUNT'].$req);
		
		$resultats = $this->db->query($counter['COUNT'].$req);
		
		if($count = $resultats->fetch(PDO::FETCH_OBJ)->total){
			
			if($debug)
				throw new myException($counter['SELECT'].$req);
			if(DEBUG)
				parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$counter['SELECT'].$req);
			
			$resultats = $this->db->query($counter['SELECT'].$req);
		
			while ($localite = $resultats->fetch(PDO::FETCH_OBJ))
				 $localites[] = new Localite($localite);	
			
			$resultats->closeCursor();
			
		}
		
		return array('count'=>$count, 'localites'=>$localites);
	}
	public function getListCvExperiences($filtre=array(), $search=array(), $justCount=false){
		
		$experiences = array();
		$counter = array(
				'COUNT' => 'SELECT count(id_profil_experience) AS total, ' ,
				'SELECT'=> 'SELECT id_profil_experience, '
				);

		$req = "id_profil_experience, id_prestataire_profil, exp_debut,exp_intitule, exp_fin, exp_entreprise, exp_description
			FROM usr_profils_experiences  WHERE cree_le ";
		
		foreach($filtre as $key=>$value){
			
				$req .= " AND ".$key." = ".$this->db->quote($value, PDO::PARAM_STR);
		}
		
		if(count($search)){
			$string = addslashes(implode("* ", $search)); 
			$req .= " AND MATCH (exp_entreprise, exp_description) AGAINST ('$string*' IN BOOLEAN MODE)";
		}
		if($debug)
			throw new myException($counter['COUNT'].$req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$counter['COUNT'].$req);
		
		$resultats = $this->db->query($counter['COUNT'].$req);
		
		if($count = $resultats->fetch(PDO::FETCH_OBJ)->total){
			if($justCount)
				return array('count'=>$count, 'experiences'=>$experiences);
			
			if($debug)
				throw new myException($counter['SELECT'].$req);
			if(DEBUG)
				parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$counter['SELECT'].$req);
			
			$resultats = $this->db->query($counter['SELECT'].$req);
			
			while ($experience = $resultats->fetch(PDO::FETCH_ASSOC)){
				$experience["exp_fin"] = Tools::shortConvertDBDateToLan($experience["exp_fin"]);
				$experience["exp_debut"] = Tools::shortConvertDBDateToLan($experience["exp_debut"]);
				
				$experiences[] = (object)$experience;	
			}
			$resultats->closeCursor();
		}
		
		return array('count'=>$count, 'experiences'=>$experiences);
	
	}
	public function getListCvPostes($filtre = array(), $search=array(), $id_postes=array(), $justCount=false){
		$postes = array();
		$counter = array(
				'COUNT' => 'SELECT count(UPC.id_profil_complement) AS total, ' ,
				'SELECT'=> 'SELECT UPC.id_profil_complement, '
				);
		$req = "UPC.id_prestataire_profil, UPC.id_complement, COALESCE(AP.adm_poste_fr, AP.adm_poste_en, AP.adm_poste_es) AS adm_poste
			FROM usr_profils_complements AS UPC
			INNER JOIN adm_postes AS AP ON UPC.id_complement = AP.id_poste
			WHERE UPC.complement =  ".self::COMPLEMENT_POSTE;
			
		foreach($filtre as $key=>$value){
			if(is_array($value)){
				if(count($value))
					$req .= " AND UPC.".$key." IN ('".implode("','", $value)."')";
				else
					$req .= " AND UPC.".$key." IN ('')";	
			}
			else{
				$req .= " AND UPC.".$key." = ".$this->db->quote($value, PDO::PARAM_STR);
			}
		}			
		
		if(count($search) || count($id_postes))
			$req .=" AND ( ";
		
		if(count($search)){
			$string = addslashes(implode("* ", $search)); 
			$req .= "  MATCH (AP.adm_poste_".$_SESSION["langue"].") AGAINST ('$string*' IN BOOLEAN MODE)";
		}
		
		if(count($search) && count($id_postes))
			$req .="OR ";
		
		if(count($id_postes))
			$req .= " AP.id_poste IN (".implode(',', $id_postes).")";
		
		if(count($search) || count($id_postes))
			$req .=" ) ";

		if($debug)
			throw new myException($counter['COUNT'].$req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$counter['COUNT'].$req);
		
		$resultats = $this->db->query($counter['COUNT'].$req);
		
		if($count = $resultats->fetch(PDO::FETCH_OBJ)->total){
			if($justCount)
				return array('count'=>$count, 'postes'=>$postes);
			
			if($debug)
				throw new myException($counter['SELECT'].$req);
			if(DEBUG)
				parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$counter['SELECT'].$req);
			
			$resultats = $this->db->query($counter['SELECT'].$req);
	
			while ($poste = $resultats->fetch(PDO::FETCH_OBJ))
				 $postes[] = $poste;	
		
			$resultats->closeCursor();
		}
		
		return array('count'=>$count, 'postes'=>$postes);
		
	}
	public function getListCvCompetences($filtre = array(), $search=array(), $id_competences=array(), $justCount=false){
		$competences = array();
		$counter = array(
				'COUNT' => 'SELECT count(UPC.id_profil_complement) AS total, ' ,
				'SELECT'=> 'SELECT UPC.id_profil_complement, '
				);
		$req = "UPC.id_prestataire_profil, UPC.id_niveau, UPC.id_complement, AN.adm_niveau_".$_SESSION["langue"]." AS adm_niveau, COALESCE( AC.adm_competence_fr, AC.adm_competence_en, AC.adm_competence_es) AS adm_competence
			FROM usr_profils_complements AS UPC
			INNER JOIN adm_competences AS AC ON UPC.id_complement = AC.id_competence
			LEFT OUTER JOIN adm_niveaux AS AN ON UPC.id_niveau = AN.id_niveau
			WHERE UPC.complement =  ".self::COMPLEMENT_COMPETENCE;
			
		foreach($filtre as $key=>$value){
			if(is_array($value)){
				if(count($value))
					$req .= " AND UPC.".$key." IN ('".implode("','", $value)."')";
				else
					$req .= " AND UPC.".$key." IN ('')";	
			}
			else{
				$req .= " AND UPC.".$key." = ".$this->db->quote($value, PDO::PARAM_STR);
			}
		}	
		
		if(count($search) || count($id_competences))
			$req .=" AND ( ";
		
		if(count($search)){
			$string = addslashes(implode("* ", $search)); 
			$req .= "  MATCH (AC.adm_competence_".$_SESSION["langue"].") AGAINST ('$string*' IN BOOLEAN MODE)";
		}
		
		if(count($search) && count($id_competences))
			$req .="OR ";
		
		if(count($id_competences))
			$req .= " AC.id_competence IN (".implode(',', $id_competences).")";
			
		if(count($search) || count($id_competences))
			$req .=" ) ";
 
		if($debug)
			throw new myException($counter['COUNT'].$req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$counter['COUNT'].$req);

		$resultats = $this->db->query($counter['COUNT'].$req);
		
		if($count = $resultats->fetch(PDO::FETCH_OBJ)->total){
			if($justCount)
				return array('count'=>$count, 'competences'=>$competences);
			
			if($debug)
				throw new myException($counter['SELECT'].$req);
			if(DEBUG)
				parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$counter['SELECT'].$req);
			
			$resultats = $this->db->query($counter['SELECT'].$req);

			while ($competence = $resultats->fetch(PDO::FETCH_OBJ))
				 $competences[] = $competence;	
			
			$resultats->closeCursor();
			
		}
		return array('count'=>$count, 'competences'=>$competences);
		
		
	}
	public function getListCvLangues($filtre = array(), $search=array(), $id_langues=array(), $justCount=false){
		$langues = array();
		$counter = array(
				'COUNT' => 'SELECT count(UPC.id_profil_complement) AS total, ' ,
				'SELECT'=> 'SELECT UPC.id_profil_complement, '
				);
		$req = "UPC.id_prestataire_profil, UPC.id_niveau, UPC.id_complement, AN.adm_niveau_".$_SESSION["langue"]." AS adm_niveau,  COALESCE(AL.adm_langue_fr, AL.adm_langue_en, AL.adm_langue_es)  AS adm_langue
			FROM usr_profils_complements AS UPC
			INNER JOIN adm_langues AS AL ON UPC.id_complement = AL.id_langue
			LEFT OUTER JOIN adm_niveaux AS AN ON UPC.id_niveau = AN.id_niveau
			WHERE UPC.complement =  ".self::COMPLEMENT_LANGUE;
			
		foreach($filtre as $key=>$value){
			if(is_array($value)){
				if(count($value))
					$req .= " AND UPC.".$key." IN ('".implode("','", $value)."')";
				else
					$req .= " AND UPC.".$key." IN ('')";	
			}
			else{
				$req .= " AND UPC.".$key." = ".$this->db->quote($value, PDO::PARAM_STR);
			}
		}	
		
		if(count($search) || count($id_langues))
			$req .=" AND ( ";
		
		
		if(count($search)){
			$string = addslashes(implode("* ", $search)); 
			$req .= "  MATCH (AL.adm_langue_".$_SESSION["langue"].") AGAINST ('$string*' IN BOOLEAN MODE)";
		}
		
		if(count($search) && count($id_langues))
			$req .="OR ";
		
		if(count($id_langues))
			$req .= " AL.id_langue IN (".implode(',', $id_langues).")";
		
		if(count($search) || count($id_langues))
			$req .=" ) ";
			
		if($debug)
			throw new myException($counter['COUNT'].$req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$counter['COUNT'].$req);
		
		$resultats = $this->db->query($counter['COUNT'].$req);
		
		if($count = $resultats->fetch(PDO::FETCH_OBJ)->total){
			if($justCount)
				return array('count'=>$count, 'langues'=>$langues);
			
			if($debug)
				throw new myException($counter['SELECT'].$req);
			if(DEBUG)
				parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$counter['SELECT'].$req);
			
			$resultats = $this->db->query($counter['SELECT'].$req);

			while ($langue = $resultats->fetch(PDO::FETCH_OBJ))
				 $langues[] = $langue;	
			
			$resultats->closeCursor();
		}
		return array('count'=>$count, 'langues'=>$langues);
		
	}
	public function getListCvEtudes($filtre = array(), $search=array(), $id_etudes=array(), $justCount=false){
		$etudes = array();
		$counter = array(
				'COUNT' => 'SELECT count(id_profil_complement) AS total, ' ,
				'SELECT'=> 'SELECT id_profil_complement, '
				);
		$req = "UPC.id_prestataire_profil, UPC.id_niveau, UPC.id_complement, COALESCE(AE.adm_etude_fr, AE.adm_etude_en, AE.adm_etude_es)  AS adm_etude
			FROM usr_profils_complements AS UPC
			INNER JOIN adm_etudes AS AE ON UPC.id_complement = AE.id_etude
			WHERE UPC.complement =  ".self::COMPLEMENT_ETUDE;
			
		foreach($filtre as $key=>$value){
			if(is_array($value)){
				if(count($value))
					$req .= " AND UPC.".$key." IN ('".implode("','", $value)."')";
				else
					$req .= " AND UPC.".$key." IN ('')";	
			}
			else{
				$req .= " AND UPC.".$key." = ".$this->db->quote($value, PDO::PARAM_STR);
			}
		}			
		
		if(count($search) || count($id_etudes))
			$req .=" AND ( ";

		if(count($search)){
			$string = implode("* ", $search); 
			$req .= "  MATCH (AE.adm_etude_".$_SESSION["langue"].") AGAINST ('$string*' IN BOOLEAN MODE)";
		}
		
		if(count($search) && count($id_etudes))
			$req .="OR ";
		
		if(count($id_etudes))
			$req .= " AE.id_etude IN (".implode(',', $id_etudes).")";
		
		if(count($search) || count($id_etudes))
			$req .=" ) ";
		
		if($debug)
			throw new myException($counter['COUNT'].$req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$counter['COUNT'].$req);
		
		$resultats = $this->db->query($counter['COUNT'].$req);
		
		if($count = $resultats->fetch(PDO::FETCH_OBJ)->total){
			if($justCount)
				return array('count'=>$count, 'etudes'=>$etudes);
			if($debug)
				throw new myException($counter['SELECT'].$req);
			if(DEBUG)
				parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$counter['SELECT'].$req);
			
			$resultats = $this->db->query($counter['SELECT'].$req);

			while ($etude = $resultats->fetch(PDO::FETCH_OBJ))
				 $etudes[] = $etude;	
			
			$resultats->closeCursor();
		}
		return array('count'=>$count, 'etudes'=>$etudes);
		
	}
	
	
	
	//Sauvegarde d'un compte jabber
	private function saveJabberUser($post=array()){
		
		if(!is_numeric($post['id_identifiant']) || !isset($post['id_identifiant']))
			return false;
		
		//On crée notre identifiant
		$prefixMail = $post['nom'].".".$post['prenom'];
		
		//On le nettoie
		$str = mb_strtolower($prefixMail, 'utf-8');
		mb_regex_encoding('utf-8');
		$str = strtr($str, array(' '=>'_', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'a'=>'a', 'a'=>'a', 'a'=>'a', 'ç'=>'c', 'c'=>'c', 'c'=>'c', 'c'=>'c', 'c'=>'c', 'd'=>'d', 'd'=>'d', 'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'e'=>'e', 'e'=>'e', 'e'=>'e', 'e'=>'e', 'e'=>'e', 'g'=>'g', 'g'=>'g', 'g'=>'g', 'h'=>'h', 'h'=>'h', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'i'=>'i', 'i'=>'i', 'i'=>'i', 'i'=>'i', 'i'=>'i', '?'=>'i', 'j'=>'j', 'k'=>'k', '?'=>'k', 'l'=>'l', 'l'=>'l', 'l'=>'l', '?'=>'l', 'l'=>'l', 'ñ'=>'n', 'n'=>'n', 'n'=>'n', 'n'=>'n', '?'=>'n', '?'=>'n', 'ð'=>'o', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'o'=>'o', 'o'=>'o', 'o'=>'o', 'œ'=>'o', 'ø'=>'o', 'r'=>'r', 'r'=>'r', 's'=>'s', 's'=>'s', 's'=>'s', 'š'=>'s', '?'=>'s', 't'=>'t', 't'=>'t', 't'=>'t', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ü'=>'u', 'u'=>'u', 'u'=>'u', 'u'=>'u', 'u'=>'u', 'u'=>'u', 'u'=>'u', 'w'=>'w', 'ý'=>'y', 'ÿ'=>'y', 'y'=>'y', 'z'=>'z', 'z'=>'z', 'ž'=>'z'));
		
		//On va verifier qu'il n'exsite pas dans la BDD	
		$req            = "SELECT jbr_loggin, jbr_password FROM  usr_jabbers WHERE  jbr_loggin = ".$this->db->quote($str, PDO::PARAM_STR);
		
		$resultats      = $this->db->query($req);

		$getJabberExist = $resultats->fetch(PDO::FETCH_ASSOC);

		//Si presence omonyme on incremente un compteur
		if($getJabberExist['jbr_loggin']){
			$i = 1;
			while(++$i){
				$user = $str.$i;
				$req            = "SELECT jbr_loggin, jbr_password FROM  usr_jabbers WHERE  jbr_loggin = ".$this->db->quote($user, PDO::PARAM_STR);
				$resultats      = $this->db->query($req);
				$getJabberExist = $resultats->fetch(PDO::FETCH_ASSOC);
				
				if(!$getJabberExist['jbr_loggin']){
					break 1;
				}
					
			}
			
			$str = $user;
		}
				
		$req = "
			INSERT INTO usr_jabbers SET  
			id_identifiant=".$this->db->quote($post['id_identifiant'], PDO::PARAM_STR).",
			jbr_loggin=".$this->db->quote($str, PDO::PARAM_STR).",
			jbr_password=".$this->db->quote($str, PDO::PARAM_STR);


		if($this->db->exec($req)){
		
			/*		
			$UserLogin = $str;
			$UserPass = $str;
			require_once(dirname(__FILE__).'/jabber/class_Jabber.php');
					
			
			$jab = new Jabber(false);
			$jab->AddMessenger($UserLogin,$UserPass);
				
			// set handlers for the events we wish to be notified about
			$jab->set_handler("connected",$jab,"_handle_connected");
			$jab->set_handler("authenticated",$jab,"_handle_authenticated");
			//$jab->set_handler("error",$addmsg,"handleError");
			
			// connect to the Jabber server
			if ($jab->connect('ks37615.kimsufi.com')){
				$AddUserErrorCode=12001;
				$jab->execute(CBK_FREQ,RUN_TIME);
			}
			
			$jab->disconnect();
			
			unset($jab,$addmsg);
			
			
			//Phase d'ajout au servcie client
			$password = 'contacto';
			$username = 'contacto';
			$server = 'ks37615.kimsufi.com';
			$port = $m[4] or $port = 5222;
			$params = $m[5];
		
			
		
			require_once(dirname(__FILE__).'/jabber/class.jabber.php');
			
		
			$connect_server = $server;
			
			$J = new jruJabber();
			$J->server = $server;
			$J->connect_server = $connect_server;
			$J->port = $port;
			$J->username = $username;
			$J->password = $password;
			$J->resource = 'JRUPHP';
			$J->use_tls = $_REQUEST['use_tls'] != 0;
		
			if (strpos($params, 'log') !== false)
				$J->enable_logging = true;
		
			if (!$J->Connect())
			{
				echo '<p><b class="error">' . $JRU_T['error_connect'] . '</b></p>';
				echo '</form>';
				$J->Disconnect();
				echo '</body></html>';
				exit();
			}
			if (!$J->SendAuth())
			{
				echo '<p><b class="error">' . $JRU_T['error_auth'] . '</b></p>';
				echo '</form>';
				$J->Disconnect();
				echo '</body></html>';
				exit();
			}
			
			$J->RosterUpdate();
			
			$new_jid = trim($UserLogin.'@ks37615.kimsufi.com');
			$new_name = trim('');
			$new_sub = trim('both');
			$new_group = trim('');
		
			
		
			$J->RosterAddUser($new_jid, NULL, $new_name, $new_group, false);
			if ($new_sub == 'none')
			{
				$J->Unsubscribe($new_jid);
				$J->SubscriptionDenyRequest($new_jid);
			}
			if ($new_sub == 'to')
			{
				$J->Subscribe($new_jid);
				$J->SubscriptionDenyRequest($new_jid);
			}
			if ($new_sub == 'from')
			{
				$J->Unsubscribe($new_jid);
				$J->SubscriptionAcceptRequest($new_jid);
			}
			if ($new_sub == 'both')
			{
				$J->Subscribe($new_jid);
				$J->SubscriptionAcceptRequest($new_jid);
			}
		
		
			$J->RosterUpdate();
			$J->Disconnect();
			*/
		
		
		}
		
		
		
		
	}
	//Methode permettant de recuperer tous les profils qui matchent pour une liste de missions données array()
	public function getListProfilsMatchMissions($id_missions=array(), $localites=array()){
		
		$DBmission = new DBMission();
		$listCompetences       = array();
		$listPostes            = array();
		$listKeywordCompetences = array();
		$listKeywordPostes     = array();
		$listLangues           = array();
		

		$getListCompetences = $DBmission->getListCompetences(array('id_mission'=>$id_missions));
		if($getListCompetences['count']){
			foreach($getListCompetences['competences'] as $c){
				$listCompetences[]        = $c->id_complement;
				$listKeywordCompetences[] = $c->adm_competence;
			}
		}
		//Tools::debugVar($listCompetences, false, 'Competences');
		
		$getListPostes = $DBmission->getListPostes(array('id_mission'=>$id_missions));
		if($getListPostes['count']){
			foreach($getListPostes['postes'] as $p){
				$listPostes[]        = $p->id_complement;
				$listKeywordPostes[] = $p->adm_poste;
			}
		}
		//Tools::debugVar($listPostes, false, 'postes');

		$getListlangues = $DBmission->getListLangues(array('id_mission'=>$id_missions));
		if($getListlangues['count']){
			foreach($getListlangues['langues'] as $l){
				$listIdLangues[]      = $l->id_complement;
				$listKeywords[]     = $l->adm_langue;
			}
		}
		//Tools::debugVar($listLangues, false, 'langues');
		return $this->getListProfils(
						array('pf_profil_statut'=>1, 'pf_pourcentage'=>'20'),
						array(
							'id_poste'=>array_unique($listPostes),
							'id_competence'=>array_unique($listCompetences),
							'id_langue'=>array_unique($listLangues),
							'adm_competence' =>array_unique($listKeywordCompetences),
							'adm_poste' =>array_unique($listKeywordPostes)
							/*
							'lo_pays_short'=>array_filter($localites['lo_pays_short']),
							'lo_region_short'=>array_filter($localites['lo_region_short']),
							'lo_departement_short'=>array_filter($localites['lo_departement_short'])*/
						),
						false,
						true
					);
		
	}
	//Methode appelée pour procéder à la récupération d'un mot de passe perdu
	public function makePassword($array=array()){
		$error = array();//on stock nos erreurs
		$identifiants = $this->getIdentifiant(array('usr_email'=>$array['usr_email']));

		if($identifiants["usr_email"]){
			//1. On génère un mot de passe aléatoire
			$new_mdp = Tools::randomString();
			
			//2. On update le mot de passe du mail donné
			if($this->updIdentifiant(array('id_identifiant'=>$identifiants["id_identifiant"], 'usr_password'=>$new_mdp))){
			
				//3.On Envoie le nouveau mot de passe
				$message['message'] = $mail_mdp_trad["msg_part_one"][$langue].$new_mdp.$mail_mdp_trad["msg_part_two"][$langue].'<a href="'.URL_SITE.'">'.URL_SITE.'</a><br/><br/>';
				$message['email']   = $_POST['usr_email'];
				$message['sujet']   = $mail_mdp_trad["sujet"][$langue];
											
				if(Tools::sendEmail($message)){
					
					return 'Un email vient de vous être envoyé';
				}
			}else{
				$error['password'] = 'Votre adresse email invalide';
			}
		}
		else{
			$error['password'] = 'Votre adresse email invalide';
		}
		
		return $error;

	}
	//Methode appelée pour procéder à l'inscription d'un membre
	public function makeInscription($array=array()){
		$error = array();//On stock nos erreurs dans ce tableau
		// I. On commence par hydrater tous nos tableaux (cast des variable int, string...)
		$identifiant = array(
				     'id_type'=>         $array["id_type"],
				     'usr_trad'=>        $_SESSION['langue'],
				     'usr_email'=>       $array["usr_email"],
				     'usr_password'=>    $array["usr_password"],
				     'usr_activate'=>    1,
				     'actif'=>           1
				     ) ;
		$manager = array(
				'id_identifiant'=>        $array["id_identifiant"],
				'id_client'=>             $array["id_client"],
				'id_role'=>               self::TYPE_ADMIN,
				'id_civilite'=>           $array["id_civilite"],
				'mg_nom'=>                $array["mg_nom"],
				'mg_prenom'=>             $array["mg_prenom"],
				'mg_portable_indicatif'=> $array["mg_portable_indicatif"],
				'mg_portable'=>           $array["mg_portable"],
				'mg_fixe_indicatif'=>     $array["mg_fixe_indicatif"],
				'mg_fixe'=>               $array["mg_fixe"]
			);
		$profil = array(
				'id_identifiant'=>        $array["id_identifiant"],
				'id_prestataire'=>        $array["id_prestataire"],
				'id_experience'=>         $array["id_experience"],
				'id_role'=>               self::TYPE_ADMIN,
				'id_civilite'=>           $array["id_civilite"],
				'pf_reference'=>          Tools::randomString(),
				'pf_nom'=>                $array["pf_nom"],
				'pf_prenom'=>             $array["pf_prenom"],
				'pf_naissance'=>          $array["pf_naissance"],
				'pf_disponibilite'=>      $array["pf_disponibilite"],
				'pf_siteweb'=>            $array["pf_siteweb"],
				'pf_permis'=>             $array["pf_permis"],
				'pf_tjm'=>                $array["pf_tjm"],
				'id_monnaie'=>            $array["id_monnaie"],
				'pf_portable_indicatif'=> $array["pf_portable_indicatif"],
				'pf_portable'=>           $array["pf_portable"],
				'pf_fixe_indicatif'=>     $array["pf_fixe_indicatif"],
				'pf_fixe'=>               $array["pf_fixe"],
				'pf_cv_titre'=>           $array["pf_cv_titre"],
				'pf_cv_description'=>     $array["pf_cv_description"],
				'pf_cv_pdf_string'=>      $array["pf_cv_pdf_string"],
				'pf_pourcentage_identite'=>2,
				'pf_profil_statut'=>      1,
				'actif'=>                 1
			);
		$client = array(
				'cl_standard'=>           $array["cl_standard"],
				'cl_standard_indicatif'=> $array["cl_standard_indicatif"],
				'cl_entreprise'=>         $array["cl_entreprise"],
				'cl_presentation'=>       $array["cl_presentation"],
				'cl_effectif'=>           $array["cl_effectif"],
				'cl_siret'=>              $array["cl_siret"],
				'cl_siteweb'=>            $array["cl_siteweb"],
				'cl_add_liv_rue'=>        $array["cl_add_liv_rue"],
				'cl_add_liv_codepostal'=> $array["cl_add_liv_codepostal"],
				'cl_add_liv_ville'=>      $array["cl_add_liv_ville"],
				'cl_add_liv_pays'=>       $array["cl_add_liv_pays"],
				'cl_add_fac_rue'=>        $array["cl_add_fac_rue"],
				'cl_add_fac_codepostal'=> $array["cl_add_fac_codepostal"],
				'cl_add_fac_ville'=>      $array["cl_add_fac_ville"],
				'cl_add_fac_pays'=>       $array["cl_add_fac_pays"]
			);
		$prestataire = array(
				'id_identifiant'=>        $array["id_identifiant"],
				'pr_notification_mail'=>  $array["pr_notification_mail"],
				'pr_standard'=>           $array["pr_standard"],
				'pr_standard_indicatif'=> $array["pr_standard_indicatif"],
				'pr_entreprise'=>         $array["pr_entreprise"],
				'pr_presentation'=>       $array["pr_presentation"],
				'id_structure'=>          $array["id_structure"],
				'pr_add_liv_rue'=>        $array["pr_add_liv_rue"],
				'pr_add_liv_codepostal'=> $array["pr_add_liv_codepostal"],
				'pr_add_liv_ville'=>      $array["pr_add_liv_ville"],
				'pr_add_liv_pays'=>       $array["pr_add_liv_pays"],
				'pr_add_fac_rue'=>        $array["pr_add_fac_rue"],
				'pr_add_fac_codepostal'=> $array["pr_add_fac_codepostal"],
				'pr_add_fac_ville'=>      $array["pr_add_fac_ville"],
				'pr_add_fac_pays'=>       $array["pr_add_fac_pays"]
			);
		
		$message['message'] = 'Bonjour Mr {{$prenom}} {{$nom}} <br> votre compte à bien été crée avec les identifiants suivants : <br>Email : '.$identifiant['usr_email'].'<br>Mot de passe : '.$identifiant['usr_password'].'<br>Merci';
		$message['sujet']   = 'Confirmation de votre inscription';
		$message['email']   = $identifiant['usr_email'];
		
		// II. On effectue les traiements de formualaire avant de sauvegarder dans la base de donnée
		//ATTENTION : Tout nos tests s'effectuent ici et nul part ailleur !
		if(!isset($identifiant['id_type']) || ($identifiant['id_type']>2 && $identifiant['id_type']<1)){
			$error['id_type'] = 'Inscription impossible, une erreur est survenue';
		}
		else{
			$securimage = new Securimage();
			
			if(!isset($array['ct_captcha']) || empty($array['ct_captcha']))
					$error['ct_captcha'] = 'Code invalide ';
			
			if ($securimage->check($array['ct_captcha']) == false) 
				$error['ct_captcha'] = 'Code invalide';
			
			if($this->getIdentifiant(array('usr_email'=>$identifiant['usr_email'])))
					$error['usr_email'] = 'Cette adresse email existe déja';
			
			if(!isset($array['valid_cgu']) || empty($array['valid_cgu']))
					$error['valid_cgu'] = 'Vous devez valider les CGU';
			
			if(!isset($identifiant['usr_password']) || empty($identifiant['usr_password']))
				$error['usr_password'] = 'ce champs est obligatoire';
					
			if($array['mdp_verif']!=$identifiant['usr_password'])
				$error['mdp_verif'] = 'Vos mots de passe sont differents';
				
			//je suis client
			if($identifiant["id_type"]==self::TYPE_CLIENT){
				
				if(!isset($manager['mg_nom']) || empty($manager['mg_nom']))
					$error['mg_nom'] = 'Nom obligatoire';
					
				if(!isset($manager['mg_prenom']) || empty($manager['mg_prenom']))
					$error['mg_prenom'] = 'Prenom obligatoire';
					
					
				if(!isset($client['cl_entreprise']) || empty($client['cl_entreprise']))
					$error['cl_entreprise'] = 'Entreprise obligatoire';
				
			}
			//je suis prestataire
			else{
				if(!isset($profil['pf_nom']) || empty($profil['pf_nom']))
					$error['pf_nom'] = 'Nom obligatoire';
					
				if(!isset($profil['pf_prenom']) || empty($profil['pf_prenom']))
					$error['pf_prenom'] = 'Prenom obligatoire';
				
			}
		}
		
		if(count($error))
			return $error;
		
		// III. On enregistre les membres dans la base de donnée
		//Ne pas modifier cette partie
		//je suis client
		if($identifiant["id_type"]==self::TYPE_CLIENT){
			
			if($manager['id_identifiant'] = $this->saveIdentifiant($identifiant)){
				if($manager['id_client'] = $this->saveClient($client)){
					if($id_manager = $this->saveManager($manager)){
						// IV. On fait les traitements 	adequats envoie de mails et generation des messages de retour
						$this->saveJabberUser(array("id_identifiant"=>$manager['id_identifiant'], 'nom'=>$manager['mg_nom'], 'prenom'=>$manager['mg_prenom']));
						Tools::sendEmail($message);
						return true;
					}
				}
			}
		}
		//je suis prestataire
		else{
			if($prestataire['id_identifiant'] = $this->saveIdentifiant($identifiant)){
				if($profil['id_prestataire'] = $this->savePrestataire($prestataire)){
					$profil['id_identifiant'] = $prestataire['id_identifiant'];
					if($id_profil = $this->saveProfil($profil)){
						// IV. On fait les traitements 	adequats envoie de mails et generation des messages de retour
						$this->saveJabberUser(array("id_identifiant"=>$prestataire['id_identifiant'], 'nom'=>$profil['pf_nom'], 'prenom'=>$profil['pf_prenom']));
						Tools::sendEmail($message);
						return true;
					}
				}
			}
			
		}
		
	}
        //Methode appelée pour procéder à la connexion d'un membre
	public function makeConnexion($array=array()){
		$error = array(); //Stockage des errors
		$identifiants = $this->getIdentifiant(array('usr_email'=>$array['usr_email'], 'usr_password'=>$array['usr_password'], 'id_type'=>self::TYPE_ADMINISTRATEUR));
		if(!$identifiants["usr_email"])
			$error['connexion'] = 'Vos identifiants sont incorrects';
		
		elseif(!$identifiants["actif"])
			$error['connexion'] = 'Votre compte à été suspendu';
		
		elseif(!$identifiants["usr_activate"])
			$error['connexion'] = 'Compte non activé';
		
		else{
			$this->saveLog($identifiants["id_identifiant"]);
			return  array(
					'id_identifiant'=>$identifiants["id_identifiant"],
					'id_type'       =>$identifiants["id_type"],
					'usr_trad'     =>$identifiants["usr_trad"]
				);
		}
		
		return $error;
	}
	//Methode appelée pour procéder à l'ajout d'un nouveau profil
	public function makeProfil($array=array()){

		// I. On commence par hydrater notre tableau (cast des variable int, string...)
		$profil = array(
				'id_prestataire'=>        $array["id_prestataire"],
				'id_identifiant'=>        $array["id_identifiant"],
				'id_experience'=>         $array["id_experience"],
				'id_role'=>               0,//Doit forecement être à zero car création du profil pour les agences
				'id_civilite'=>           $array["id_civilite"],
				'pf_reference'=>          Tools::randomString(),
				'pf_nom'=>                $array["pf_nom"],
				'pf_prenom'=>             $array["pf_prenom"],
				'pf_naissance'=>          $array["pf_naissance"],
				'pf_disponibilite'=>      $array["pf_disponibilite"],
				'pf_siteweb'=>            $array["pf_siteweb"],
				'pf_permis'=>             $array["pf_permis"],
				'pf_tjm'=>                $array["pf_tjm"],
				'id_monnaie'=>            $array["id_monnaie"],
				'pf_portable_indicatif'=> $array["pf_portable_indicatif"],
				'pf_portable'=>           $array["pf_portable"],
				'pf_fixe_indicatif'=>     $array["pf_fixe_indicatif"],
				'pf_fixe'=>               $array["pf_fixe"],
				'pf_cv_titre'=>           $array["pf_cv_titre"],
				'pf_cv_description'=>     $array["pf_cv_description"],
				'pf_cv_pdf_string'=>      $array["pf_cv_pdf_string"],
				'pf_profil_statut'=>         1,
				'pf_pourcentage_identite' => 2,
				'actif'=>                 1
			);
		
		// II. On effectue les traitements de formulaire avant de sauvegarder dans la base de donnée
		if(!isset($profil['pf_nom']) || empty($profil['pf_nom']))
			$error['pf_nom'] = 'Nom obligatoire';
			
		if(!isset($profil['pf_prenom']) || empty($profil['pf_prenom']))
			$error['pf_prenom'] = 'Prenom obligatoire';
		
		if(count($error))
			return $error;
		
		// III. On enregistre notre profil dans la base de donnée
		if(isset($profil["id_prestataire"])){
			
			if($id_profil = $this->saveProfil($profil)){
				// IV. On fait les traitements 	adequats envoie de mails et generation des messages de retour
				return $id_profil;		
			}
			
		}
		else{
			throw new myException('Attention aucun prestataire de défini');
		}
	}
	
	//Methode appelée pour procéder à l'ajout d'un nouveau manager dans l'espace client
	public function makeManager($array=array()){

		// I. On commence par hydrater notre tableau (cast des variable int, string...)
		$identifiant = array(
				     'id_type'=>         self::TYPE_CLIENT,
				     'usr_trad'=>        $_SESSION['langue'],
				     'usr_email'=>       $array["usr_email"],
				     'usr_password'=>    $array["usr_password"],
				     'usr_activate'=>    1,
				     'actif'=>           1
				     ) ;
		$manager = array(
				'id_identifiant'=>        $array["id_identifiant"],
				'id_client'=>             $array["id_client"],
				'id_role'=>               0,
				'id_civilite'=>           $array["id_civilite"],
				'mg_nom'=>                $array["mg_nom"],
				'mg_prenom'=>             $array["mg_prenom"],
				'mg_portable_indicatif'=> $array["mg_portable_indicatif"],
				'mg_portable'=>           $array["mg_portable"],
				'mg_fixe_indicatif'=>     $array["mg_fixe_indicatif"],
				'mg_fixe'=>               $array["mg_fixe"]
			);
		
		// II. On effectue les traiements de formualaire avant de sauvegarder dans la base de donnée
		//ATTENTION : Tout nos tests s'effectuent ici et nul part ailleur !
		if(!isset($identifiant['id_type']) || ($identifiant['id_type']>2 && $identifiant['id_type']<2)){
			$error['id_type'] = 'Inscription impossible, une erreur est survenue';
		}
		else{
			
			if(!isset($manager['id_client']) || empty($manager['id_client']))
				$error['id_client'] = 'Vous devez posséder un compte client';
			
			if(!isset($identifiant['usr_email']) || empty($identifiant['usr_email']))
				$error['usr_email'] = 'ce champs est obligatoire';
			
			if($this->getIdentifiant(array('usr_email'=>$identifiant['usr_email'])))
				$error['usr_email'] = 'Cette adresse email existe déja';
			
			if(!isset($identifiant['usr_password']) || empty($identifiant['usr_password']))
				$error['usr_password'] = 'ce champs est obligatoire';
					
			if($array['mdp_verif']!=$identifiant['usr_password'])
				$error['mdp_verif'] = 'Vos mots de passe sont differents';
				
			if(!isset($manager['mg_nom']) || empty($manager['mg_nom']))
				$error['mg_nom'] = 'Nom obligatoire';
				
			if(!isset($manager['mg_prenom']) || empty($manager['mg_prenom']))
				$error['mg_prenom'] = 'Prenom obligatoire';

		}
		
		if(count($error))
			return $error;

		// III. On enregistre notre profil dans la base de donnée
		if(isset($manager["id_client"])){
		
			if($manager['id_identifiant'] = $this->saveIdentifiant($identifiant)){
				if($id_manager = $this->saveManager($manager)){
					$this->saveJabberUser(array("id_identifiant"=>$manager['id_identifiant'], 'nom'=>$manager['mg_nom'], 'prenom'=>$manager['mg_prenom']));
					return true;
					// IV. On fait les traitements 	adequats envoie de mails et generation des messages de retour
				}
			}
			
		}
		else{
			throw new myException('Attention aucun client de défini');
		}
	}
	//Methode appelée pour modifier le CV d'un profil
	public function editProfil($profil=array()){
		
		if(isset($profil["id_prestataire_profil"])){
			
			// I. On effectue les traitements de formulaire avant de sauvegarder dans la base de donnée
			if($_POST && $profil["flag"]=="formation"){
			
				
				if(!Tools::verifDate($profil["form_debut"]))
					$error['form_debut'] = 'Date de formation invalide';
				else
					$profil['form_debut'] = Tools::shortConvertLanDateToDb($profil['form_debut']);
					
				if(!isset($profil["form_diplome"]) || empty($profil["form_diplome"]))
					$error['form_diplome'] = 'ce chmaps est obligatoire';
				
				if(!isset($profil["form_ecole"]) || empty($profil["form_ecole"]))
					$error['form_ecole'] = 'ce chmaps est obligatoire';
				
				if(!isset($profil["form_description"]) || empty($profil["form_description"]))
					$error['form_description'] = 'ce chmaps est obligatoire';
				
				
			}
			elseif($_POST && $profil["flag"]=="experience"){
				
			
				if(!Tools::verifDate($profil["exp_debut"]))
					$error['exp_debut'] = 'Date de debut invalide';
				else
					$profil['exp_debut'] = Tools::shortConvertLanDateToDb($profil['exp_debut']);
				
				if(!empty($profil["exp_fin"]) && !Tools::verifDate($profil["exp_fin"]))
					$error['exp_fin'] = 'Date de fin invalide';
				else
					$profil['exp_fin'] = Tools::shortConvertLanDateToDb($profil['exp_fin']);
				
				if(Tools::verifDate($profil["exp_debut"], 'en') && Tools::verifDate($profil["exp_fin"], 'en')){
					//Tester enteriorite des dates
					$date1 = new DateTime($profil["exp_debut"]);
					$date2 = new DateTime($profil["exp_fin"]);
					
					$diff = ($date1 < $date2);
					if(!$diff)
						$error['exp_debut'] = 'Date de fin invalide';
				}
				
				if(!isset($profil["exp_intitule"]) || empty($profil["exp_intitule"]))
					$error['exp_intitule'] = 'ce chmaps est obligatoire';
				
				if(!isset($profil["exp_entreprise"]) || empty($profil["exp_entreprise"]))
					$error['exp_entreprise'] = 'ce chmaps est obligatoire';
				
				if(!isset($profil["exp_description"]) || empty($profil["exp_description"]))
					$error['exp_description'] = 'ce chmaps est obligatoire';
				
		
				
			}
			
			elseif($_POST && $profil["flag"]=="localite"){
				
			}
			elseif(isset($profil["id_langue"]) || isset($profil["id_poste"]) || isset($profil["id_competence"]) || isset($profil["id_etude"]) || isset($profil['rmv_id_profil_complement']) || isset($profil['id_niveau'])) {
				
			}
			elseif($_POST){
				
				if(!Tools::verifDate($profil["pf_disponibilite"]))
					$error['pf_disponibilite'] = 'Disponibilite invalide';
					
				if(!is_numeric($profil["pf_naissance"]) || $profil["pf_naissance"]<5)
					$error['pf_naissance'] = 'Age invalide';
				
				$profil['pf_disponibilite'] = Tools::shortConvertLanDateToDb($profil['pf_disponibilite']);
				
				//Pour le pourcentage
				$count = 2;
				
				if(!empty($profil['pf_naissance']))
					$count ++;
				if(!empty($profil['pf_siteweb']))
					$count ++;
				if(!empty($profil['pf_fixe']))
					$count ++;
				if(!empty($profil['pf_portable']))
					$count ++;
				if(!empty($profil['pf_permis']))
					$count ++;
				if(!empty($profil['pf_disponibilite']))
					$count ++;
				if(!empty($profil['pf_cv_titre']))
					$count ++;
				if(!empty($profil['pf_tjm']))
					$count ++;
				
				$profil["pf_pourcentage_identite"] = ceil($count*100)/40;
				
				
				
			}

			if(count($error))
				return $error;
			
			// II. On enregistre notre profil dans la base de donnée
			//Ajout, edition et suppresion des formations
			if((($_POST && $profil["flag"]=="formation")) || is_numeric($profil["rmv_id_profil_formation"])){
								
				if(isset($profil["rmv_id_profil_formation"]) && is_numeric($profil["rmv_id_profil_formation"])){//REMOVE
					
					if($id_profil_formation = $this->rmvCvFormation($profil)){
						// III. On fait les traitements adequats envoie de mails et generation des messages de retour	
						$countLangue    =  $this->getListCvLangues(array('id_prestataire_profil'=>$profil["id_prestataire_profil"]), array(), array(), true);
						if($countLangue['count']<3)
							$countPerc = 0;
						else
							$countPerc = 10;
						
						$countFormation =  $this->getListCvFormations(array('id_prestataire_profil'=>$profil["id_prestataire_profil"]), array(), true);	

						if($countFormation['count']<1)
							$countPerc += 0;
						else
							$countPerc += 15;	
					
						$this->updProfil(array('id_identifiant'=>$profil['id_identifiant'], 'id_prestataire_profil'=>$profil["id_prestataire_profil"], 'pf_pourcentage_formation'=>$countPerc));
							
					
						return $id_profil_formation;
					}
				}
				elseif(isset($profil["id_profil_formation"]) && is_numeric($profil["id_profil_formation"])){//UPDATE
					if($id_profil_formation = $this->updCvFormation($profil)){
						// III. On fait les traitements adequats envoie de mails et generation des messages de retour
						return $id_profil_formation;
					}
				}
				else{
					if($id_profil_formation = $this->saveCvFormation($profil)){//INSERT
						// III. On fait les traitements adequats envoie de mails et generation des messages de retour
						$countLangue    =  $this->getListCvLangues(array('id_prestataire_profil'=>$profil["id_prestataire_profil"]), array(), array(), true);
						if($countLangue['count']<3)
							$countPerc = 0;
						else
							$countPerc = 10;
						
						$countPerc += 15;	
						
						$this->updProfil(array('id_identifiant'=>$profil['id_identifiant'], 'id_prestataire_profil'=>$profil["id_prestataire_profil"], 'pf_pourcentage_formation'=>$countPerc));
						
						return $id_profil_formation;
					}
				}
			}
			//Ajout, edition et suppresion des experiences
			elseif((($_POST && $profil["flag"]=="experience")) || is_numeric($profil["rmv_id_profil_experience"])){
				
				if(isset($profil["rmv_id_profil_experience"]) && is_numeric($profil["rmv_id_profil_experience"])){//REMOVE
					if($id_profil_experience = $this->rmvCvExperience($profil)){
						// III. On fait les traitements adequats envoie de mails et generation des messages de retour
						$countExperience = $this->getListCvExperiences(array('id_prestataire_profil'=>$profil["id_prestataire_profil"]), array(), true);	

						if($countExperience['count']<1)
							$countPerc += 0;
						else
							$countPerc += 25;	
					
						$this->updProfil(array('id_identifiant'=>$profil['id_identifiant'], 'id_prestataire_profil'=>$profil["id_prestataire_profil"], 'pf_pourcentage_experience'=>$countPerc));
						
						return $id_profil_experience;
					}
				}
				elseif(isset($profil["id_profil_experience"]) && is_numeric($profil["id_profil_experience"])){//UPDATE
					
					if($id_profil_experience = $this->updCvExperience($profil)){
						// III. On fait les traitements adequats envoie de mails et generation des messages de retour
						return $id_profil_experience;
					}
				}
				else{
					if($id_profil_experience = $this->saveCvExperience($profil)){//INSERT
						// III. On fait les traitements adequats envoie de mails et generation des messages de retour
						$this->updProfil(array('id_identifiant'=>$profil['id_identifiant'], 'id_prestataire_profil'=>$profil["id_prestataire_profil"], 'pf_pourcentage_experience'=>25));
						
						return $id_profil_experience;
					}
				}
			}
			//Ajout et suppresion des complements (poste, langue, etude, competences)
			elseif(($_POST && isset($profil["id_langue"])) || isset($profil["id_poste"]) || isset($profil["id_competence"]) || isset($profil["id_etude"]) || is_numeric($profil["rmv_id_profil_complement"]) || isset($profil['id_niveau'])){
				if(isset($profil["rmv_id_profil_complement"]) && is_numeric($profil["rmv_id_profil_complement"])){//REMOVE
					if($id_profil_complement = $this->rmvCvComplement($profil)){
						// III. On fait les traitements adequats envoie de mails et generation des messages de retour
						if(isset($profil["id_competence"])){
							
							$count =  $this->getListCvCompetences(array('id_prestataire_profil'=>$profil["id_prestataire_profil"]), array(), array(), true);
							if($count['count']<3)
								$this->updProfil(array('id_identifiant'=>$profil['id_identifiant'], 'id_prestataire_profil'=>$profil["id_prestataire_profil"], 'pf_pourcentage_competence'=>0));
						}
						elseif(isset($profil["id_langue"])){
							
							$countLangue    = $this->getListCvLangues(array('id_prestataire_profil'=>$profil["id_prestataire_profil"]), array(), array(), true);
							if($countLangue['count']<3)
								$countPerc = 0;
							else
								$countPerc = 10;
							
							$countFormation =  $this->getListCvFormations(array('id_prestataire_profil'=>$profil["id_prestataire_profil"]), array(), true);	
							
							if($countFormation['count']<1)
								$countPerc += 0;
							else
								$countPerc += 15;	
							
							if($countLangue['count']<3)
								$this->updProfil(array('id_identifiant'=>$profil['id_identifiant'], 'id_prestataire_profil'=>$profil["id_prestataire_profil"], 'pf_pourcentage_formation'=>$countPerc));
							
						}
						return $id_profil_complement;
					}
				}
				else{
				
					if($return = $this->saveCvComplements($profil)){
						if(isset($profil["id_competence"])){
							$count = $this->getListCvCompetences(array('id_prestataire_profil'=>$profil["id_prestataire_profil"]), array(), array(), true);
							
							if($count['count']>2 && $count['count']<4)
								$this->updProfil(array('id_identifiant'=>$profil['id_identifiant'], 'id_prestataire_profil'=>$profil["id_prestataire_profil"], 'pf_pourcentage_competence'=>25));
						}
						elseif(isset($profil["id_langue"])){
							
							$countLangue    =  $this->getListCvLangues(array('id_prestataire_profil'=>$profil["id_prestataire_profil"]), array(), array(), true);

							if($countLangue['count']>2 && $countLangue['count']<4)

								$countPerc = 10;

							$countFormation =  $this->getListCvFormations(array('id_prestataire_profil'=>$profil["id_prestataire_profil"]), array(), true);	
							if($countFormation['count']<1)
								$countPerc += 0;
							else
								$countPerc += 15;	
							
							if($countLangue['count']>2 && $countLangue['count']<4)
								$this->updProfil(array('id_identifiant'=>$profil['id_identifiant'], 'id_prestataire_profil'=>$profil["id_prestataire_profil"], 'pf_pourcentage_formation'=>$countPerc));
							
						}
						return $return;
					}
				}
			}
			//ajout et suppression des localites
			elseif((($_POST && $profil["flag"]=="localite")) || is_numeric($profil["rmv_id_profil_localite"])){
				
				if(isset($profil["rmv_id_profil_localite"]) && is_numeric($profil["rmv_id_profil_localite"])){//REMOVE
					if($id_profil_localite = $this->rmvCvLocalite($profil)){
						// III. On fait les traitements adequats envoie de mails et generation des messages de retour
						return $id_profil_localite;
					}
				}
				else{
					
					if($return = $this->saveCvLocalites($profil)){
						// III. On fait les traitements adequats envoie de mails et generation des messages de retour
						return $return;
					}
				}
			
			}
			//Edition du profil
			elseif($_POST){

				if($id_profil = $this->updProfil($profil)){
					// III. On fait les traitements adequats envoie de mails et generation des messages de retour
					return $id_profil;
				}
			}
			
		}
		else{
			throw new myException('Attention aucun profil de défini');
		}
	}
	//Methode appelée pour modifier le Prestataire dans mon compte
	public function editPrestataire($prestataire=array()){
		
		$error = array(); //on stock nos erreurs eventuelles
		$DBfile = new DBFile(); //Si on importe un avatar on aura besoin de la class DBfile

		if(isset($prestataire["id_identifiant"]) && isset($prestataire["id_prestataire_profil"])){
			//I. On hydrate nos tableau
			$profil   =array(
				//'pf_nom'                 =>$prestataire['pf_nom'],
				//'pf_prenom'              =>$prestataire['pf_prenom'],
				'id_identifiant'         =>$prestataire['id_identifiant'],
				'id_prestataire_profil'  =>$prestataire['id_prestataire_profil']
			);
			
			$presta   =array(
				'id_identifiant'         =>$prestataire['id_identifiant'],
				'pr_add_fac_rue'         =>$prestataire['pr_add_fac_rue'],
				'pr_add_fac_codepostal'  =>$prestataire['pr_add_fac_codepostal'],
				'pr_add_fac_ville'       =>$prestataire['pr_add_fac_ville'],
				'pr_add_fac_pays'        =>$prestataire['pr_add_fac_pays'],
				'pr_add_fac_pays'        =>$prestataire['pr_add_fac_pays'],
				'pr_notification_mail'   =>0
			);
			
			$file  =  array(
					'page'             => $prestataire['page'],
					'action'           => $prestataire['action'],
					'file'             => $_FILES,
					'id_identifiant'   => $prestataire['id_identifiant'],
					'file_type'        => 'avatar'//flag permettant de differencier le type de fichier CF $DBfile->makeFile();
			);
			unset ($_FILES['file']);
			$plaquette  =  array(
					'page'             => $prestataire['page'],
					'action'           => $prestataire['action'],
					'plaquette'        => $_FILES,
					'id_identifiant'   => $prestataire['id_identifiant'],
					'file_type'        => 'plaquette'//flag permettant de differencier le type de fichier CF $DBfile->makeFile();
			);

			$crop  =  array(
					'page'             => $prestataire['page'],
					'action'           => $prestataire['action'],
					'send_crop'        => $prestataire['send_crop'],
					'crop'             => $prestataire['crop'],
					'image'            => $prestataire['image'],
					'id_identifiant'   => $prestataire['id_identifiant'],
					'x'                => $prestataire['x'],
					'y'                => $prestataire['y'],
					'w'                => $prestataire['w'],
					'h'                => $prestataire['h'],
					'file_type'        => 'avatar'//flag permettant de differencier le type de fichier CF $DBfile->makeFile();
			);
			
			if(isset($prestataire['pr_notification_mail']))
				$presta['pr_notification_mail']= 1;
			
			// II. On effectue les traitements de formulaire avant de sauvegarder dans la base de donnée
			if($_POST && isset($prestataire["usr_password"])){
				//on lance une update des identifiants
				if($prestataire['mdp_verif']!=$prestataire['usr_password'])
					$error['mdp_verif'] = 'Vos mots de passe sont differents';
				if(!isset($prestataire['usr_password']) || empty($prestataire['usr_password']))
					$error['mdp_verif'] = 'Veuillez resaisir votre mot de passe';

			}

			//Cette condition intervient uniquement lors de la mise a jour des infos plaquette
			//On souhaite juste modifier la plaquette et presentation
			//Donc on a pas besoin du gros tableau $profil ou $prestataire...On efface et on change l"affectation
			if($_POST && isset($prestataire['pr_presentation'])){
				unset($profil);
				unset($presta);
				$profil   =array(
					'id_identifiant'         =>$prestataire['id_identifiant'],
					'id_prestataire_profil'  =>$prestataire['id_prestataire_profil']
				);
				$presta   =array(
					'id_identifiant'         =>$prestataire['id_identifiant'],
					'pr_presentation'        =>$prestataire['pr_presentation']
				);

			}

			if(count($error)>0)//on a des erreurs on stop ici
				return $error;
			
			// II. On enregistre l'edtion notre prestataire dans la base de donnée
			if($_POST && isset($prestataire["usr_password"])){//Traitement concernant les identifiants de connexion
				if($this->updIdentifiant(array("id_identifiant"=>$prestataire["id_identifiant"], 'usr_password'=>$prestataire['usr_password'])))
					return true;
			}
			elseif($_POST && empty($crop['send_crop'])){//Traitement concernant l'edition du presta
				
				if($this->updProfil($profil)){
					
					if($this->updPrestataire($presta)){

						if($file['file']['file']['error']==UPLOAD_ERR_OK && isset($file['file']['file']['name'])){
							if($error = $DBfile->makeFile($file)){
								return $error;
								// III. On fait les traitements adequats envoie de mails et generation des messages de retour
							}
							// III. On fait les traitements adequats envoie de mails et generation des messages de retour
						}

						if($plaquette['plaquette']['plaquette']['error']==UPLOAD_ERR_OK && isset($plaquette['plaquette']['plaquette']['name'])){
												
							if($error = $DBfile->makeFile($plaquette)){
								return $error;
								// III. On fait les traitements adequats envoie de mails et generation des messages de retour
							}
							// III. On fait les traitements adequats envoie de mails et generation des messages de retour
						}
						return true;
					}
					// III. On fait les traitements adequats envoie de mails et generation des messages de retour
					return true;
				}
			}
			else{//Traitement concernant le Crop
		
				if($DBfile->makeFile($crop)){
					// III. On fait les traitements adequats envoie de mails et generation des messages de retour
					return true;
				}
			}
			
			// III. On fait les traitements adequats envoie de mails et generation des messages de retour
			return true;
			
		}
		else{
			throw new myException('Attention aucun prestataire de défini');
		}
	}
	//Methode appelée pour modifier le client dans mon compte
	public function editClient($client=array()){
		
		$error = array(); //on stock nos erreurs eventuelles
		$DBfile = new DBFile(); //Si on importe un avatar on aura besoin de la class DBfile

		if(isset($client["id_identifiant"])){
			//I. On hydrate nos tableau
			$manager   =array(
					//'mg_nom'                 =>$client['mg_nom'],
					//'mg_prenom'              =>$client['mg_prenom'],
					'id_identifiant'         =>$client['id_identifiant'],
					'id_client_manager'      =>$client['id_client_manager']
			);
			
			$myClient  =array(
					'id_client'              =>$client['id_client'],
					'cl_add_fac_rue'         =>$client['cl_add_fac_rue'],
					'cl_add_fac_codepostal'  =>$client['cl_add_fac_codepostal'],
					'cl_add_fac_ville'       =>$client['cl_add_fac_ville'],
					'cl_add_fac_pays'        =>$client['cl_add_fac_pays'],
					'cl_add_fac_pays'        =>$client['cl_add_fac_pays']
			);
			
			$file  =  array(
					'page'             => $client['page'],
					'action'           => $client['action'],
					'file'             => $_FILES,
					'id_identifiant'   => $client['id_identifiant'],
					'file_type'        => 'avatar'//flag permettant de differencier le type de fichier CF $DBfile->makeFile();
			);

			$crop  =  array(
					'page'             => $client['page'],
					'action'           => $client['action'],
					'send_crop'        => $client['send_crop'],
					'crop'             => $client['crop'],
					'image'            => $client['image'],
					'id_identifiant'   => $client['id_identifiant'],
					'x'                => $client['x'],
					'y'                => $client['y'],
					'w'                => $client['w'],
					'h'                => $client['h'],
					'file_type'        => 'avatar'//flag permettant de differencier le type de fichier CF $DBfile->makeFile();
			);
			
			
			// II. On effectue les traitements de formulaire avant de sauvegarder dans la base de donnée
			if($_POST && isset($client["usr_password"])){
				//on lance une update des identifiants
				if($client['mdp_verif']!=$client['usr_password'])
					$error['mdp_verif'] = 'Vos mots de passe sont differents';
				if(!isset($client['usr_password']) || empty($client['usr_password']))
					$error['mdp_verif'] = 'Veuillez resaisir votre mot de passe';

			}
			
			
			//Cette condition intervient uniquement lors de la mise a jour des infos plaquette
			//On souhaite juste modifier la plaquette et presentation
			//Donc on a pas besoin du gros tableau $profil ou $prestataire...On efface et on change l"affectation
			//ATTENTION seul un client admin peut modfier la plaquette
			elseif($_POST && isset($client['cl_presentation']) && ($client['id_role']==self::TYPE_ADMIN)){
				unset($myClient);
	
				$myClient   =array(
					'id_client'         =>$client['id_client'],
					'cl_presentation'   =>$client['cl_presentation']
				);
			}
			
			elseif($_POST && isset($client['mg_nom'])){
				if(!isset($client['mg_nom']) || empty($client['mg_nom']))
					$error['mg_nom'] = 'Nom obligatoire';
					
				if(!isset($client['mg_prenom']) || empty($client['mg_prenom']))
					$error['mg_prenom'] = 'Prenom obligatoire';
			
			}
			
			else{
				//On utilise pas un compte client admin donc on va vider intentionellement la variable $myClient
				
				if($client['id_role']!=self::TYPE_ADMIN){
					unset($myClient);
					$myClient   =array('id_client'         =>$client['id_client']);
				}
				//On peut eventuellement effectuer des tests sur le formulaire de Crop envoyé
			}
	

			if(count($error)>0)//on a des erreurs on stop ici
				return $error;
									

			// II. On enregistre l'edtion notre prestataire dans la base de donnée
			if($_POST && isset($client["usr_password"])){//Traitement concernant les identifiants de connexion
				if($this->updIdentifiant(array("id_identifiant"=>$client["id_identifiant"], 'usr_password'=>$client['usr_password'])))
					return true;
			}
			elseif($_POST && empty($crop['send_crop'])){//Traitement concernant l'edition du presta
				
				if($this->updManager($manager)){

									
					if($this->updClient($myClient)){
						
						if($file['file']['file']['error']==UPLOAD_ERR_OK && isset($file['file']['file']['name'])){
							if($error = $DBfile->makeFile($file)){
							
								return $error;
								// III. On fait les traitements adequats envoie de mails et generation des messages de retour
							}
							// III. On fait les traitements adequats envoie de mails et generation des messages de retour
						}
						return true;
						
					}
					// III. On fait les traitements adequats envoie de mails et generation des messages de retour
					return true;
				}
			}
			else{//Traitement concernant le Crop

				if($DBfile->makeFile($crop)){
					// III. On fait les traitements adequats envoie de mails et generation des messages de retour
					return true;
				}
			}
			
			// III. On fait les traitements adequats envoie de mails et generation des messages de retour
			return true;
			
		}
		else{
			throw new myException('Attention aucun manager de défini');
		}
	}
	//Methode appelée pour procéder à la modification d'un manager
	public function editManager($client=array()){
		$error = array(); //on stock nos erreurs eventuelles
		$DBfile = new DBFile(); //Si on importe un avatar on aura besoin de la class DBfile

		if(isset($client["id_identifiant"])){
			//I. On hydrate nos tableau
			$manager   =array(
					'id_identifiant'         =>$client['id_identifiant'],
					'id_client_manager'      =>$client['id_client_manager'],
					'mg_fixe_indicatif'      =>$client['mg_fixe_indicatif'],
					'mg_fixe'                =>$client['mg_fixe'],
					'mg_portable_indicatif'  =>$client['mg_portable_indicatif'],
					'mg_portable'            =>$client['mg_portable']
			);
			unset($client['crypt']);
			
			// II. On effectue les traitements de formulaire avant de sauvegarder dans la base de donnée
			if($_POST && isset($client["usr_password"])){
				//on lance une update des identifiants
				if($client['mdp_verif']!=$client['usr_password'])
					$error['mdp_verif'] = 'Vos mots de passe sont differents';
				if(!isset($client['usr_password']) || empty($client['usr_password']))
					$error['mdp_verif'] = 'Veuillez saisir votre nouveau de passe';

			}
			elseif($_POST && isset($client['mg_nom'])){
				if(!isset($client['mg_nom']) || empty($client['mg_nom']))
					$error['mg_nom'] = 'Nom obligatoire';
					
				if(!isset($client['mg_prenom']) || empty($client['mg_prenom']))
					$error['mg_prenom'] = 'Prenom obligatoire';
			
			}
			//On souhaite supprimer un manager
			//On va vider $manager et le recréer simplement
			//On ira ensuite faire une modification de l'activation usr_actif uniquement si l'update du manager fonctionne
			//Aini on evite l'update d'un membre qui n'appartient pas au client (piratage)
			elseif(isset($client['rmv_id_client_manager'])){
				unset($manager);
				$manager   =array(
					'id_identifiant'         =>$client['id_identifiant'],
					'id_client_manager'      =>$client['rmv_id_client_manager']
				);
			
			}
			else{
				//On peut eventuellement effectuer des tests sur le formulaire de Crop envoyé
			}
			

			if(count($error)>0)//on a des erreurs on stop ici
				return $error;
		
			// II. On enregistre l'edtion notre manager dans la base de donnée
			if($this->updManager($manager)){
				
				if($_POST && isset($client["usr_password"])){//Traitement concernant les identifiants de connexion
					
					if($this->updIdentifiant(array("id_identifiant"=>$client["id_identifiant"], 'usr_password'=>$client['usr_password']))){
						// III. On fait les traitements adequats envoie de mails et generation des messages de retour
						return true;
					}
					
				}
				elseif(isset($client['rmv_id_client_manager'])){
					
					if($this->updIdentifiant(array("id_identifiant"=>$client["id_identifiant"], 'usr_activate'=>0))){
						// III. On fait les traitements adequats envoie de mails et generation des messages de retour
						return true;
					}
					
				}
				// III. On fait les traitements adequats envoie de mails et generation des messages de retour
				return true;
			}
		
			
		
			
		}
		else{
			throw new myException('Attention aucun manager de défini');
		}
		
	}
	//Methode appelée pour procéder à la sauvegarde d'un favori
	public function makeFavori($array=array()){
		if(is_numeric($array['id_identifiant']) && is_numeric($array['id_mission'])){
		
			$array['composante'] = $array['id_mission'].'-'.$array['id_identifiant'];
			if($this->saveFavoriMission($array)){
				return true;
			}
			else{
				return false;
			}
			
		}elseif(is_numeric($array['id_identifiant']) && is_numeric($array['id_prestataire_profil'])){
		
			$array['composante'] = $array['id_prestataire_profil'].'-'.$array['id_identifiant'];
			if($this->saveFavoriProfil($array)){
				return true;
			}
			else{
				return false;
			}
			
		}
		
	}
	//Methode appelée pour procéder à la suppresion d'un favori
	public function removeFavori($array=array()){
		
		if(is_numeric($array['id_identifiant']) && is_numeric($array['id_mission'])){
			if($this->rmvFavoriMission($array)){
				return true;
			}
			else{
				return false;
			}
			
		}elseif(is_numeric($array['id_identifiant']) && is_numeric($array['id_prestataire_profil'])){
		
			if($this->rmvFavoriProfil($array)){
				return true;
			}
			else{
				return false;
			}
			
		}
		
	}
	
	// --------------------------------------------------------------------------------//
	//Methodes pour le one shot du domino : transfert de la base neofreelo à la base Weme
	// --------------------------------------------------------------------------------//
	public function makeInscriptionTransfert($array=array()){
		$error = array();//On stock nos erreurs dans ce tableau
		// I. On commence par hydrater tous nos tableaux (cast des variable int, string...)
		$identifiant = array(
				'id_identifiant' => 	$array['id_identifiant'],
				     'id_type'=>         $array["id_type"],
				     'usr_trad'=>        'es',
				     'usr_email'=>       $array["usr_email"],
				     'usr_password'=>    $array["usr_password"],
				     'usr_activate'=>    1,
				     'actif'=>           1
				     ) ;
		$manager = array(
				'id_identifiant'=>        $array["id_identifiant"],
				'id_client'=>             $array["id_client"],
				'id_role'=>               self::TYPE_ADMIN,
				'id_civilite'=>           $array["id_civilite"],
				'mg_nom'=>                $array["mg_nom"],
				'mg_prenom'=>             $array["mg_prenom"],
				'mg_portable_indicatif'=> $array["mg_portable_indicatif"],
				'mg_portable'=>           $array["mg_portable"],
				'mg_fixe_indicatif'=>     $array["mg_fixe_indicatif"],
				'mg_fixe'=>               $array["mg_fixe"]
			);
		$profil = array(
				'id_identifiant'=>        $array["id_identifiant"],
				'id_prestataire'=>        $array["id_prestataire"],
				'id_experience'=>         $array["id_experience"],
				'id_role'=>               self::TYPE_ADMIN,
				'id_civilite'=>           $array["id_civilite"],
				'pf_reference'=>          Tools::randomString(),
				'pf_nom'=>                $array["pf_nom"],
				'pf_prenom'=>             $array["pf_prenom"],
				'pf_naissance'=>          $array["pf_naissance"],
				'pf_disponibilite'=>      $array["pf_disponibilite"],
				'pf_siteweb'=>            $array["pf_siteweb"],
				'pf_permis'=>             $array["pf_permis"],
				'pf_tjm'=>                $array["pf_tjm"],
				'id_monnaie'=>            $array["id_monnaie"],
				'pf_portable_indicatif'=> $array["pf_portable_indicatif"],
				'pf_portable'=>           $array["pf_portable"],
				'pf_fixe_indicatif'=>     $array["pf_fixe_indicatif"],
				'pf_fixe'=>               $array["pf_fixe"],
				'pf_cv_titre'=>           $array["pf_cv_titre"],
				'pf_cv_description'=>     $array["pf_cv_description"],
				'pf_cv_pdf_string'=>      $array["pf_cv_pdf_string"],
				'pf_pourcentage_identite'=>2,
				'pf_profil_statut'=>      1,
				'actif'=>                 1
			);
		$client = array(
				'id_client'=>             $array["id_client"],
				'cl_standard'=>           $array["cl_standard"],
				'cl_standard_indicatif'=> $array["cl_standard_indicatif"],
				'cl_entreprise'=>         $array["cl_entreprise"],
				'cl_presentation'=>       $array["cl_presentation"],
				'cl_effectif'=>           $array["cl_effectif"],
				'cl_siret'=>              $array["cl_siret"],
				'cl_siteweb'=>            $array["cl_siteweb"],
				'cl_add_liv_rue'=>        $array["cl_add_liv_rue"],
				'cl_add_liv_codepostal'=> $array["cl_add_liv_codepostal"],
				'cl_add_liv_ville'=>      $array["cl_add_liv_ville"],
				'cl_add_liv_pays'=>       $array["cl_add_liv_pays"],
				'cl_add_fac_rue'=>        $array["cl_add_fac_rue"],
				'cl_add_fac_codepostal'=> $array["cl_add_fac_codepostal"],
				'cl_add_fac_ville'=>      $array["cl_add_fac_ville"],
				'cl_add_fac_pays'=>       $array["cl_add_fac_pays"]
			);
		$prestataire = array(
				'id_identifiant'=>        $array["id_identifiant"],
				'id_prestataire'=>	  $array["id_prestataire"],
				'pr_notification_mail'=>  $array["pr_notification_mail"],
				'pr_standard'=>           $array["pr_standard"],
				'pr_standard_indicatif'=> $array["pr_standard_indicatif"],
				'pr_entreprise'=>         $array["pr_entreprise"],
				'pr_presentation'=>       $array["pr_presentation"],
				'id_structure'=>          $array["id_structure"],
				'pr_add_liv_rue'=>        $array["pr_add_liv_rue"],
				'pr_add_liv_codepostal'=> $array["pr_add_liv_codepostal"],
				'pr_add_liv_ville'=>      $array["pr_add_liv_ville"],
				'pr_add_liv_pays'=>       $array["pr_add_liv_pays"],
				'pr_add_fac_rue'=>        $array["pr_add_fac_rue"],
				'pr_add_fac_codepostal'=> $array["pr_add_fac_codepostal"],
				'pr_add_fac_ville'=>      $array["pr_add_fac_ville"],
				'pr_add_fac_pays'=>       $array["pr_add_fac_pays"]
			);
		
		
		
		// II. On effectue les traiements de formualaire avant de sauvegarder dans la base de donnée
		//ATTENTION : Tout nos tests s'effectuent ici et nul part ailleur !
		if(!isset($identifiant['id_type']) || ($identifiant['id_type']>2 && $identifiant['id_type']<1)){
			$error['id_type'] = XMLEngine::$trad->global_trad->notifInscriptionImpossible;
		}
		
		if(count($error))
			return $error;
		
		// III. On enregistre les membres dans la base de donnée
		//Ne pas modifier cette partie
		//je suis client
		if($identifiant["id_type"]==1){
			
			if($manager['id_identifiant'] = $this->saveIdentifiant($identifiant)){
				if($this->saveClientTransfert($client)){
					if($id_manager = $this->saveManager($manager)){
						// IV. On fait les traitements 	adequats envoie de mails et generation des messages de retour
						$this->saveJabberUser(array("id_identifiant"=>$manager['id_identifiant'], 'nom'=>$manager['mg_nom'], 'prenom'=>$manager['mg_prenom']));
						return true;
					}
				}
			}
		}
		//je suis prestataire
		else{
			if($prestataire['id_identifiant'] = $this->saveIdentifiantTransfert($identifiant)){
				if($profil['id_prestataire'] = $this->savePrestataire($prestataire)){
					$this->saveJabberUser(array("id_identifiant"=>$prestataire['id_identifiant'], 'nom'=>$profil['pf_nom'], 'prenom'=>$profil['pf_prenom']));
					//Je retourne un tableau contenant les nouveaux id_identifiant, id_prestataire  pour les prochains enregistrement (profils, complément, reponse, files ...)
					$new_idents = array('id_identifiant'=>$prestataire['id_identifiant'], 'id_prestataire'=>$profil['id_prestataire']);
					return $new_idents;
				}
			}
			
		}
		
	}
	
	//Methode appelée pour procéder à l'ajout d'un nouveau manager dans l'espace client
	public function makeManagerTransfert($array=array()){

		// I. On commence par hydrater notre tableau (cast des variable int, string...)
		$identifiant = array(
				     'id_type'=>         self::TYPE_CLIENT,
				     'usr_trad'=>        'es',
				     'usr_email'=>       $array["usr_email"],
				     'usr_password'=>    $array["usr_password"],
				     'usr_activate'=>    1,
				     'actif'=>           1
				     ) ;
		$manager = array(
				'id_identifiant'=>        $array["id_identifiant"],
				'id_client'=>             $array["id_client"],
				'id_role'=>               0,
				'id_civilite'=>           $array["id_civilite"],
				'mg_nom'=>                $array["mg_nom"],
				'mg_prenom'=>             $array["mg_prenom"],
				'mg_portable_indicatif'=> $array["mg_portable_indicatif"],
				'mg_portable'=>           $array["mg_portable"],
				'mg_fixe_indicatif'=>     $array["mg_fixe_indicatif"],
				'mg_fixe'=>               $array["mg_fixe"]
			);
		
		// II. On effectue les traiements de formualaire avant de sauvegarder dans la base de donnée
		//ATTENTION : Tout nos tests s'effectuent ici et nul part ailleur !
		if(!isset($identifiant['id_type']) || ($identifiant['id_type']>2 && $identifiant['id_type']<2)){
			$error['id_type'] = 'Inscription impossible, une erreur est survenue';
		}
		else{
			
			if(!isset($manager['id_client']) || empty($manager['id_client']))
				$error['id_client'] = 'Vous devez posséder un compte client';
			
			if(!isset($identifiant['usr_email']) || empty($identifiant['usr_email']))
				$error['usr_email'] = 'ce champs est obligatoire';
			
			if($this->getIdentifiant(array('usr_email'=>$identifiant['usr_email'])))
				$error['usr_email'] = 'Cette adresse email existe déja';
			
			if(!isset($identifiant['usr_password']) || empty($identifiant['usr_password']))
				$error['usr_password'] = 'ce champs est obligatoire';
					
		
			if(!isset($manager['mg_nom']) || empty($manager['mg_nom']))
				$error['mg_nom'] = 'Nom obligatoire';
				
			if(!isset($manager['mg_prenom']) || empty($manager['mg_prenom']))
				$error['mg_prenom'] = 'Prenom obligatoire';

		}
		
		if(count($error))
			return $error;

		// III. On enregistre notre profil dans la base de donnée
		if(isset($manager["id_client"])){
			
			if($manager['id_identifiant'] = $this->saveIdentifiantTransfert($identifiant)){
				if($id_manager = $this->saveManager($manager)){
					$this->saveJabberUser(array("id_identifiant"=>$manager['id_identifiant'], 'nom'=>$manager['mg_nom'], 'prenom'=>$manager['mg_prenom']));
					return true;
					// IV. On fait les traitements 	adequats envoie de mails et generation des messages de retour
				}
			}
			
		}
		else{
			throw new myException('Attention aucun client de défini');
		}
	}
	private function saveIdentifiantTransfert($array=array(), $debug=false){
		
		$req = "
		INSERT INTO usr_identifiants SET
		id_identifiant = ".$this->db->quote($array['id_identifiant'], PDO::PARAM_STR).",
		id_type=".$this->db->quote($array['id_type'], PDO::PARAM_STR).",
		usr_password= PASSWORD(".$this->db->quote($array['usr_password'], PDO::PARAM_STR)."),
		usr_email=".$this->db->quote($array['usr_email'], PDO::PARAM_STR).",
		usr_trad = ".$this->db->quote($array['usr_trad'], PDO::PARAM_STR).",
		actif = ".$this->db->quote($array['actif'], PDO::PARAM_STR).",
		usr_activate = ".$this->db->quote($array['usr_activate'], PDO::PARAM_STR).",
		modifie_le=NOW()";			
		
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
		
		if($this->db->exec($req)){
			
			return $this->db->lastInsertId();
	
		}
		
	}
	
	
	private function saveClientTransfert($array=array(), $ignore=array(), $debug=false){
		
		$req .= "INSERT INTO usr_clients SET " ;
		
		foreach($array as $key=>$value){
			if(!in_array($key, $ignore))
				$req .= $key." = ".$this->db->quote($value, PDO::PARAM_STR)." , ";
		}		
		
		$req .= " modifie_le = NOW() ";
			
		
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
		
		if($this->db->exec($req)){
			
			return $this->db->lastInsertId();
	
		}
	}
	public function saveProfilTransfert($array=array(), $ignore=array(), $debug=false){
		$profil = array(
				'id_prestataire'=>        $array["id_prestataire"],
				'id_identifiant'=>        $array["id_identifiant"],
				'id_prestataire_profil'=> $array['id_prestataire_profil'],
				'id_experience'=>         $array["id_experience"],
				'id_role'=>               $array["id_role"],
				'id_civilite'=>           $array["id_civilite"],
				'pf_reference'=>          $array["pf_reference"],
				'pf_nom'=>                $array["pf_nom"],
				'pf_prenom'=>             $array["pf_prenom"],
				'pf_naissance'=>          $array["pf_naissance"],
				'pf_disponibilite'=>      $array["pf_disponibilite"],
				'pf_siteweb'=>            $array["pf_siteweb"],
				'pf_permis'=>             $array["pf_permis"],
				'pf_tjm'=>                $array["pf_tjm"],
				'id_monnaie'=>            $array["id_monnaie"],
				'pf_portable_indicatif'=> $array["pf_portable_indicatif"],
				'pf_portable'=>           $array["pf_portable"],
				'pf_fixe_indicatif'=>     $array["pf_fixe_indicatif"],
				'pf_fixe'=>               $array["pf_fixe"],
				'pf_cv_titre'=>           $array["pf_cv_titre"],
				'pf_cv_description'=>     $array["pf_cv_description"],
				'pf_cv_pdf_string'=>      $array["pf_cv_pdf_string"],
				'pf_profil_statut'=>         1,
				'pf_pourcentage_identite' => 2,
				'actif'=>                 $array["actif"]
			);
		
		$req = " INSERT INTO usr_prestataires_profils SET ";
		
		foreach($profil as $key=>$value){
			if(!in_array($key, $ignore))
				$req .= $key." = ".$this->db->quote($value, PDO::PARAM_STR)." , ";
		}		
		
		$req .= " modifie_le = NOW() ON DUPLICATE KEY UPDATE modifie_le = NOW() ";
			
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
		
		if($this->db->exec($req)){
			return $this->db->lastInsertId();
		}
		
	}
	
	public function saveCvComplementsTransfert($complements=array(),  $ignore=array(), $debug=false){
                        //Enregistrement des informations complementaires
                        $key = array_keys($complements);
                            
			switch ($key[0]){
			    
			    case 'id_poste':
				$complement_type = self::COMPLEMENT_POSTE;
				break;
			    
			    case 'id_langue':
				$complement_type = self::COMPLEMENT_LANGUE;
				break;
			    
			    case 'id_competence':
				$complement_type = self::COMPLEMENT_COMPETENCE;
				break;
			    
			    case 'id_etude':
				$complement_type = self::COMPLEMENT_ETUDE;
				break;
			    
			}
			
			if(is_numeric($complement_type) || (!empty($complements['id_niveau']) && !empty($complements['id_prestataire_profil']))){
			    

			    foreach($complements as $value){
				$id_complement = $value['complement'];
				
				//Si on veut inserer une nouvelle entrée qui n'exisait pas en BDD donc on string on l'enregitsre dabord dans la table ADM et ensuite dans la table profils_complements  
				if(!is_numeric($id_complement) && !empty($id_complement) && is_numeric($complements['id_prestataire_profil']) && isset($complements['id_prestataire_profil'])){

					$DBadmin = new DBAdmin();
					switch ($complement_type){
			   
					   case self::COMPLEMENT_POSTE:
					       $complement = 'poste';
					       break;
					   
					   case self::COMPLEMENT_LANGUE:
					       $complement = 'langue';
					       break;
					   
					   case self::COMPLEMENT_COMPETENCE:
					       $complement = 'competence';
					       break;
					   
					   case self::COMPLEMENT_ETUDE:
					       $complement = 'etude';
					       break;
					   
				       }

				       if($id_complement = $DBadmin->saveComplement(array('table'=>$complement, 'entry'=>$id_complement))){
					
					  $req = "INSERT INTO usr_profils_complements SET
						modifie_le = NOW(),
						id_prestataire_profil = ".$this->db->quote($complements['id_prestataire_profil'], PDO::PARAM_STR).",
						id_niveau = ".$this->db->quote($value['id_niveau'], PDO::PARAM_STR).",
						id_complement = ".$this->db->quote($id_complement, PDO::PARAM_STR).",
						complement = ".$this->db->quote($complement_type, PDO::PARAM_STR).",
						composante = ".$this->db->quote($complements['id_prestataire_profil']."-".$id_complement."-".$complement_type, PDO::PARAM_STR)."
						ON DUPLICATE KEY UPDATE composante = composante";
						
						    if($debug)
							    throw new myException($req);
						    if(DEBUG)
							    parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
						    
						
						if($this->db->exec($req))
							return $complements['id_prestataire_profil']."-".$id_complement."-".$complement_type;
						else
							return $req;
						
				       }
				}
			    }
			}
                            
			
			return $lastId;
	}
	
	public function saveCvFormationTransfert($array=array(), $ignore=array('page', 'action', 'flag', 'id_profil_formation', 'id_identifiant', 'crypt'), $debug=false){
		
		$req = " INSERT INTO usr_profils_formations SET ";
		
		foreach($array as $key=>$value){
			if(!in_array($key, $ignore))
				$req .= $key." = ".$this->db->quote($value, PDO::PARAM_STR)." , ";
		}		
		
		$req .= " modifie_le = NOW() ON DUPLICATE KEY UPDATE composante = composante";
			
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
		
		if($this->db->exec($req)){
			
			return $this->db->lastInsertId();
	
		}
		
	}
	
	public function saveCvExperienceTransfert($array=array(), $ignore=array('page', 'action', 'flag', 'id_profil_experience', 'id_identifiant', 'crypt'), $debug=false){
		
		$req = " INSERT INTO usr_profils_experiences SET ";
		
		foreach($array as $key=>$value){
			if(!in_array($key, $ignore))
				$req .= $key." = ".$this->db->quote($value, PDO::PARAM_STR)." , ";
		}		
		
		$req .= " modifie_le = NOW() ON DUPLICATE KEY UPDATE composante = composante";
			
		
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
		
		if($this->db->exec($req)){
			
			return $this->db->lastInsertId();
	
		}
		
	}
	
	public function saveFavoriMissionTransfert($favoris=array(), $ebug=false){
		$req = "INSERT INTO usr_prestataires_favoris SET
			modifie_le = NOW(),
			id_mission = ".$this->db->quote($favoris['id_mission'], PDO::PARAM_STR).",
			id_identifiant = ".$this->db->quote($favoris['id_identifiant'], PDO::PARAM_STR).",
			composante = ".$this->db->quote($favoris['composante'], PDO::PARAM_STR)."
			ON DUPLICATE KEY UPDATE composante = composante";
			
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
			
		if($this->db->exec($req))
			return $this->db->lastInsertId();
	
	}
	
	public function saveCvLocalitesTransfert($array=array(),  $ignore=array(), $debug=false){

                 // I. On commence par hydrater notre tableau (cast des variable int, string...)
		$localite = array(
				
				'id_prestataire_profil' =>  $array['id_prestataire_profil'],
				'lo_quartier'=>             $array["lo_quartier"],
				'lo_ville'=>                $array["lo_ville"],
				'lo_departement'=>          $array["lo_departement"],
				'lo_departement_short'=>    $array["lo_departement_short"],
				'lo_region'=>               $array["lo_region"],
				'lo_region_short'=>         $array["lo_region_short"],
				'lo_pays'=>                 $array["lo_pays"],
				'lo_pays_short'=>           $array["lo_pays_short"]
			);
                        
		$req = "INSERT INTO usr_profils_localites SET
			modifie_le = NOW(),
			id_prestataire_profil = ".$this->db->quote($localite['id_prestataire_profil'], PDO::PARAM_STR).",
			lo_quartier = ".$this->db->quote($localite['lo_quartier'], PDO::PARAM_STR).",
			lo_ville = ".$this->db->quote($localite['lo_ville'], PDO::PARAM_STR).",
			lo_departement = ".$this->db->quote($localite['lo_departement'], PDO::PARAM_STR).",
			lo_departement_short = ".$this->db->quote($localite['lo_departement_short'], PDO::PARAM_STR).",
			lo_region = ".$this->db->quote($localite['lo_region'], PDO::PARAM_STR).",
			lo_region_short = ".$this->db->quote($localite['lo_region_short'], PDO::PARAM_STR).",
			lo_pays = ".$this->db->quote($localite['lo_pays'], PDO::PARAM_STR).",
			lo_pays_short = ".$this->db->quote($localite['lo_pays_short'], PDO::PARAM_STR);
		
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
				    
		if($id_profil_localite = $this->db->exec($req)){
			return $id_profil_localite; 
		}
	}
}
