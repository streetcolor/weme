<?PHP
class File {
    
    protected $id_file;
    protected $id_identifiant; //type Client ou Prestataire
    
    protected $id_profil;//Si le fichier correspond � un CV ou une Realisation ou meme une PJ
    protected $reference;
    protected $nom;
    protected $path;
    protected $extension;
    protected $description;
    protected $type;
    
    public function __construct( $donnees=array()){
          
                $this->id_file        = $donnees['id_file'];
                $this->id_identifiant = $donnees['id_identifiant'];
                $this->id_profil      = $donnees['id_prestataire_profil'];
               
		$this->reference      = $donnees['fichier_reference'];
		$this->nom            = $donnees['fichier_nom'];
		$this->path           = $donnees['fichier_path'];
		$this->extension      = strtolower($donnees['fichier_extension']);
		$this->description    = $donnees['fichier_description'];
                $this->type           = $donnees['fichier_type'];//cf reference definie dans DBfile
		
    }
    
    public function __toString(){//Methode appel�e quand on echo $profil->getFile()
	
	    return $this->getThumbnail();
    }
    
    public function getId_file(){
            return $this->id_file;
    }
    
    public function getId_identifiant(){
            return $this->id_identifiant;
    }
    
    public function getId_profil(){
            return $this->id_prestataire_profil;
    }
    
    public function getReference(){
            return $this->reference;
    }
    
    public function getDescription(){
            return $this->description;
    } 
    
    public function getNom(){
            return $this->nom;
    }
    
    public function getPath($full=true){
	  
	    if($this->path){
		if($this->type == DBFile::TYPE_CV)
		    $pref = 'CV_'.$this->id_profil.'_';
		elseif($this->type == DBFile::TYPE_PLAQUETTE)
		    $pref = 'PL_'.$this->id_identifiant.'_'; 
		if($full){
		    //$_SERVER["DOCUMENT_ROOT"].'/'.
		    if($this->type == DBFile::TYPE_PJ)
			return URL_SITE.$this->path.'/'.$pref.$this->reference.'.'.$this->extension;
		    
		    elseif($this->type == DBFile::TYPE_PLAQUETTE)
			return URL_SITE.$this->path.'/'.$pref.$this->reference.'.'.$this->extension;

		    else
			return _PATH_FRONT_.$this->path.'/'.$pref.$this->reference.'.'.$this->extension;
		}
		else{
		    return (object) array('server'=>URL_SITE, 'path'=>$this->path, 'reference'=>$this->reference, "extension"=>$this->extension);
		}
	    }
	    
	    else
		return false;
	    
    }
    
    public function getThumbnail($width=150, $height=110){
	
	    if($this->type == DBFile::TYPE_CV){

		if($this->path){
		    $pref = 'GEN_CV_'.$this->id_profil.'_';
		    return _PATH_FRONT_.$this->path.'/'.$pref.$this->reference.'.jpg';
		}
		else return "/web/scripts/thumbnail.php?path="._PATH_FRONT_."web/img/type_cv.png&width=$width&height=$height";
		    
	    }
	    elseif($this->type == DBFile::TYPE_PLAQUETTE){

		if($this->path){
		    $pref = 'GEN_PL_'.$this->id_identifiant.'_';
		    return URL_SITE.$this->path.'/'.$pref.$this->reference.'.jpg';
		}
		else return "/web/scripts/thumbnail.php?path="._PATH_FRONT_."web/img/type_plaquette.png&width=$width&height=$height";
		    
	    }
	    elseif($this->type == DBFile::TYPE_AVATAR){

		if($this->path){
		    return URL_SITE.$this->path.'/'.$pref.$this->reference.'.'.$this->extension;
		}
		else return "./web/scripts/thumbnail.php?path="._PATH_FRONT_."/web/img/type_avatar.png&width=$width&height=$height";

		    
	    }
	    elseif($this->type == DBFile::TYPE_PJ){

		return URL_SITE."/web/scripts/thumbnail.php?path="._PATH_FRONT_."web/img/type_pj.png&width=$width&height=$height";
		    
	    }
	    elseif($this->type == DBFile::TYPE_TELECHARGEABLE){

		return URL_SITE."/web/scripts/thumbnail.php?path=web/img/type_download.png&width=$width&height=$height";
		    
	    }
	    elseif($this->type == DBFile::TYPE_CONSULTABLE){

		return URL_SITE."/web/scripts/thumbnail.php?path="._PATH_FRONT_."web/img/type_open.png&width=$width&height=$height";
		    
	    }
	    elseif($this->type == DBFile::TYPE_VISUALISABLE){

		return '/web/scripts/thumbnail.php?path='.$this->getPath()."&width=$width&height=$height";
		    
	    }
	    else
		return false;
	    
    } 
    
    public function getExtension(){
            return $this->extension;
    }
    
    public function getType(){
        return $this->type;
    }
    
    public function getInformations(){
	$finfo = finfo_open(FILEINFO_MIME_TYPE);
	$file['date'] = Tools::shortConvertDBDateToLan(date('Y-m-d h:m:s', filemtime($this->getPath())));
	$file['size'] = $filesize = round(filesize($this->getPath()) * .0009765625);
	$file['type'] = finfo_file ($finfo, $this->getPath());
	if($this->type == DBFile::TYPE_VISUALISABLE){
	    $dimensions = getimagesize($this->getPath());
	    $file['width'] = $dimensions[0];
	    $file['height'] = $dimensions[1];
	}
	return (object) $file;
    }
    
    //Pour acceder � cette methode
    //$file = new File();
    //$file->getProfil()->getNom();
    public function getProfil(){
        
        $DBmembre = new DBMembre();
        $profil = $DBmembre->getListProfils(array('id_prestataire_profil'=>$this->id_profil));
        return $profil[0];
    }
        
}