<?PHP
class Profil extends Prestataire {
        
        protected $id_role;//Boolean True il s'agit du CV associ� directement au compte prestataire False il s'agit des CV qui entrent dans le cas d'une agence
	protected $id_experience; //Nombre ann�e Experience professionelle en ann�e 
       
        
        protected $reference;//permet de noter une reference lors de l'import de CV
        protected $naissance;//date en anglais
	
        protected $disponibilite;//date en anglais
	protected $siteweb;
        protected $permis; //Bool true or false
	
	protected $tjm               = array();//tarif + devise
        protected $numero_fixe       = array();//indicatif + numero
        protected $numero_portable   = array();//indicatif + numero
        
        protected $cv_titre;
        protected $cv_description;
        protected $cv_pdf_string;
        protected $cv_competences = array();
        protected $cv_langues     = array();
        protected $cv_localites   = array();
        protected $cv_postes      = array();//postes recherches
        protected $cv_experiences = array();
        protected $cv_formations  = array();
        
        protected $actif;//profil visible dans la CVteque ou non
	
	protected $active_pdf; //On utilise le PDF ou NON
	
	public function __construct( $donnees=array(), $forceAbonnement=false){
		
                $this->id_role               = $donnees['id_role'];
                $this->id_experience         = $donnees['id_experience'];
		
                $this->reference             = $donnees['pf_reference'];
               
                $this->naissance             = $donnees['pf_naissance'];
		
		$this->titre                 = $donnees['pf_cv_titre'];
		$this->description           = $donnees['pf_cv_description'];
                
                $this->disponibilite         = $donnees['pf_disponibilite'];
		$this->siteweb               = $donnees['pf_siteweb'];
                $this->permis                = $donnees['pf_permis'];
                
		$this->pourcentage           = $donnees['pf_pourcentage'];
		
		
		
                $this->tjm                   = array(
							'tarif'      =>$donnees['pf_tjm'],
							'id_monnaie' =>$donnees['id_monnaie'],
							'monnaie'    =>$donnees['adm_monnaie']
						);
		
		$this->fixe                  = array(
							'indicatif' =>$donnees["pf_fixe_indicatif"],
							'numero'    =>$donnees["pf_fixe"]
						);
		
                $this->portable              = array(
							'indicatif' =>$donnees["pf_portable_indicatif"],
							'numero'    =>$donnees["pf_portable"]
						);
		
		$this->active_pdf            = $donnees['pf_active_pdf'];
		
		//Permet de remplir id_deintifiant plus d'autre dans la classe prestataire
		parent::__construct($donnees);
		
		//Le $forceabonnement est une action manuelle envoy� depuis la partie reponse client
		//en effet les CV present dans les reponse ne sont doivent pas etre crypt�
		//On simule donc un abonnement valide et le tout est jou�
		//ATTENTION $this->abonnement est sett�� dans la classe Prestataire (Profil etend une extends)
		//Ce set priovent initualement de la classe client :)
		if($forceAbonnement)
			$this->abonnnement = true;
			
			

	}
	
	
        public function getId_role(){
		return $this->id_role;
	}
        
        public function getId_experience(){
		return $this->id_experience;
	}
        
	
        
	public function getReference(){
		return $this->reference;
	}
	
        
        
        public function getNaissance(){
		
		return $this->naissance;
	}
        
	public function getFixe($string=false){
            if(!$this->abonnnement)
		return 'XXXXXXXXX';
            $numero = $this->fixe;
	    
            if($string)
                return $numero["indicatif"]." - ".$numero["numero"];
            else
                return (object)$numero;
	}
        
        public function getPortable($string=false){
            if(!$this->abonnnement)
		return 'XXXXXXXXX';
            $numero = $this->portable;
	    
            if($string)
                return $numero["indicatif"]." - ".$numero["numero"];
            else
                return (object)$numero;
	}
	public function getTjm($string=false){
            
            $tjm = $this->tjm;
	    
            if($string){
		if($tjm["tarif"]>0)
			return $tjm["tarif"]." - ".$tjm["monnaie"];
		else
			return "-";
            }
            else
                return (object)$tjm;
	}
        public function getActif(){
            return $this->actif;
        }
	
	public function getSiteweb(){
		return $this->siteweb;
	}
	public function getTitre(){
		return $this->titre;
	}
	public function getPermis(){
		return $this->permis;
	}
	
	public function getDescription(){
		if($this->description)
			return $this->description;
		else
			return "-";
	}
	public function getDisponibilite(){
		return Tools::shortConvertDBDateToLan($this->disponibilite);
	}
	
	public function getActivePDF(){
		return $this->active_pdf;
	}
	
	//Cette fonction permet de r�cuperer les formations associ�s au profil en cours
	//Elle est separ�e pour �viter de surcharger inutilement les requetes SQL
	public function getFormations(){
		$formations = new DBMembre();
		return (object) $formations->getListCvFormations(array('id_prestataire_profil'=>$this->id_prestataire_profil), array());
	}
	//Cette fonction permet de r�cuperer le prestataire associ� au profil en cours
	//Elle est separ�e pour �viter de surcharger inutilement les requetes SQL
	public function getExperiences($count=false){
		$experiences = new DBMembre();
		return (object) $experiences->getListCvExperiences(array('id_prestataire_profil'=>$this->id_prestataire_profil), array(), $count);
	}
	//Pour acceder � cette methode
        //$profil = new $profil();
        //$profil = $profil->getLangues();
	public function getLangues(){
		$complements = new DBMembre();
		return (object) $complements->getListCvLangues(array('id_prestataire_profil'=>$this->id_prestataire_profil));
	}
	//Pour acceder � cette methode
        //$profil = new $profil();
        //$profil = $profil->getCompetences();
	public function getCompetences(){
		$complements = new DBMembre();
		return (object) $complements->getListCvCompetences(array('id_prestataire_profil'=>$this->id_prestataire_profil));
	}
	//Pour acceder � cette methode
        //$profil = new $profil();
        //$profil = $profil->getPostes();
	public function getPostes(){
		$complements = new DBMembre();
		return (object) $complements->getListCvPostes(array('id_prestataire_profil'=>$this->id_prestataire_profil));
	}
	//Pour acceder � cette methode
        //$profil = new $profil();
        //$missionClient = $profil->getEtudes();
	public function getEtudes(){
		$complements = new DBMembre();
		return (object) $complements->getListCvEtudes(array('id_prestataire_profil'=>$this->id_prestataire_profil));
	}
	//Pour acceder � cette methode
        //$profil = new $profil();
        //$profil = $profil->getLocalites();
	public function getLocalites(){
		$localites = new DBMembre();	
		return (object) $localites->getListCvLocalites(array('id_prestataire_profil'=>$this->id_prestataire_profil));
	}
	//Cette fonction permet de r�cuperer le prestataire associ� au profil en cours
	//Elle est separ�e pour �viter de surcharger inutilement les requetes SQL
	public function getPrestataire(){
		$prestataire = new DBMembre();
		return (object) $prestataire->getPrestataire($this->id_identifiant);
	}
	//Pour acceder � cette methode
        //$profil = new $profil();
        //$profil = $profil->getRealisations();
	//ATTENTION a la subtilite la methode est surcharg�e depuis la Class Prestataire
	//ATTENTION depuis cette classe on ne recupere QUE les r�alisation du compte profil (id�al pour les affichage dans les CV)
	public function getRealisations(){
		$DBfile = new DBFile();
		$listRealisations = $DBfile->getListFiles(array('id_prestataire_profil'=>$this->id_prestataire_profil, 'fichier_type'=>array(DBFile::TYPE_TELECHARGEABLE, DBFile::TYPE_CONSULTABLE, DBFile::TYPE_VISUALISABLE)));
		return (object)  $listRealisations;
	}
        

}
