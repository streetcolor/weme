<?php
class DBConnect{
	
	public static $connexion = null;
	public static $request   = null;
	
	public function __construct(){
		$this->db = self::$connexion;
		$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
	}
	
	public function __get($name){
		echo "<span class=\"error\">R�cup�ration de '$name' impossible</span>";
	}
	
	public function __call($name, $arguments){
		echo "<span class=\"error\">Appel de la m�thode '$name' inexistante avec les argument suivants DBMANAGER</span>"
			. implode(', ', $arguments). "\n";
	}

	public static function __callStatic($name, $arguments){
		echo "<span class=\"error\">Appel de la m�thode statique '$name' inexistante avec les argument suivants DBMANAGER</span>"
			. implode(', ', $arguments). "\n";
	}
	
        public function makeConnexionSingleton($connexion=""){
		
		if($connexion){
			if(is_null(self::$connexion)) {
			   self::$connexion = $connexion;
			}	
		}
		
		else throw new myException('Cr�ation du pattern factory impossible');
	}
        
}
