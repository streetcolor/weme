<?PHP
class Client extends Manager{
    
    protected $id_client;
    protected $id_type; //type Client ou Prestataire
    
    protected $entreprise;
    protected $presentation;
    protected $effectif;
    protected $siret;
    protected $siteweb;
    
    protected $standard            = array();//Si agence numero de telephone de l'agence indicatif + numero
    protected $adresse_livraison   = array();
    protected $adresse_facturation = array();
    
    public static $abonnement      = array(); //cette variable servira de reference pour stocker les abonnements du client array()

    
    public function __construct( $donnees=array()){
				
                $this->id_client          = $donnees['id_client'];
                $this->id_type            = $donnees['id_type'];
		
		$this->entreprise         = $donnees['cl_entreprise'];
		$this->presentation       = $donnees['cl_presentation'];
		$this->effectif           = $donnees['cl_effectif'];
		$this->siret              = $donnees['cl_siret'];
		$this->siteweb            = $donnees['cl_siteweb'];
	
		$this->standard           = array(
                                                   'indicatif' =>$donnees["cl_standard_indicatif"],
                                                   'numero'    =>$donnees["cl_standard"]
                                                );
                
                $this->adresse_livraison  = array(
                                                   'rue'       =>$donnees["cl_add_liv_rue"],
                                                   'codepostal'=>$donnees["cl_add_liv_codepostal"],
                                                   'ville'     =>$donnees["cl_add_liv_ville"],
                                                   'pays'      =>$donnees["cl_add_liv_pays"]
                                                );
                
                $this->adresse_facturation = array(
                                                   'rue'       =>$donnees["cl_add_fac_rue"],
                                                   'codepostal'=>$donnees["cl_add_fac_codepostal"],
                                                   'ville'     =>$donnees["cl_add_fac_ville"],
                                                   'pays'      =>$donnees["cl_add_fac_pays"]
                                                );
		$this->setAbonnement();//Je set ma variable abonnement via une methode priv�e
				
		parent::__construct($donnees);
		
    }
    
    //Toute la mecanique de l'abonnement se faira ici
    private function setAbonnement(){
	    $DBmembre=new DBMembre();
	    
	    //SI on a configur� dans les define l'autorisation des abonnement gratuit
	    //le site passe en mode gratuit pour tous les client enjoy!
	    if(FORCE_FREE_ABONNEMENT){
		Client::$abonnement = true;
		return true;
	    }
	    
	    if(DBMembre::$id_type==DBMembre::TYPE_CLIENT){
		//Si je suis de type client on va aller faire des requettes pour recupere les abonnement, les quotas et tout ce qui s'en suit
		//On va dailleurs setter cette variable statitque qui nous permettrea de l'appeler dans les autres classes et les autre partie du site via
		//$smarty->assign('abonnement') dans le controller parent controller/controller.php
		Client::$abonnement = false;
	    }
	    
    }
    
    public function getId_client(){
            return $this->id_client;
    }
    
    public function getId_type(){
            return $this->id_type;
    }
    
    public function getEntreprise(){
            return $this->entreprise;
    }
    
    public function getPresentation(){
            return $this->presentation;
    } 
    
    public function getEffectif(){
            return $this->effectif;
    }
    
    public function getSiret(){
            return $this->siret;
    } 
    
    public function getSiteweb(){
            return $this->siteweb;
    }
    
    public function getStandard($string=true){
            
	$numero = $this->standard;
	
	if($string)
	    return $numero["indicatif"]." - ".$numero["numero"];
	else
	    return $numero;
    }

    public function getAdresseFacturation($string=true){
	    
	    $adresse  = $this->adresse_facturation;

	    if($string)
		return $adresse["rue"]." ".$adresse["codepostal"]." ".$adresse["ville"]." - ".$adresse["pays"];
	    else
		return (object) $adresse;
    }	
    
    public function getAdresseLivraison($string=true){
	    
	    $adresse  = $this->adresse_livraison;

	    if($string)
		return $adresse["rue"]." ".$adresse["codepostal"]." ".$adresse["ville"]." - ".$adresse["pays"];
	    else
		return (object)  $adresse;
    }
    
     //Cette fonction permet de r�cuperer les managers associ�s au client en cours
    //Elle est separ�e pour �viter de surcharger inutilement les requetes SQL
    public function getManagers($agency = false){
	    $managers = new DBMembre();
	    return (object) $managers->getListManagers(array('id_client'=>$this->id_client, 'id_role'=>DBMembre::TYPE_NORMAL), array(), false );
    }
    
        
}