<?php

class DBAdmin extends DBConnect{
	
	//TRES IMPORTANT
	//Les méthodes save doivent être privée et sonr uniquement appelée via les méthodes make
	//Les tests de saisie s'effectuent OBLIGATOIREMENT depuis les méthodes make
	
	public function saveComplement($entry=array()){
		        
		if(empty($entry['entry']) || is_numeric($entry['entry']) || empty($entry['table']) || is_numeric($entry['table']))
			return false;
		
		$req = "
			  INSERT INTO adm_".$entry['table']."s SET
			  modifie_le = NOW(),
			  adm_".$entry['table']."_".$_SESSION['langue']." = ".$this->db->quote(trim($entry['entry']), PDO::PARAM_STR).",
			  actif = 1
			  ON DUPLICATE KEY UPDATE adm_".$entry['table']."_".$_SESSION['langue']." = adm_".$entry['table']."_".$_SESSION['langue'];
			  
			  if($debug)
				  throw new myException($req);
			  if(DEBUG)
				  parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
				
			//Tools::debugVar($req, false);
			
			  if($id = $this->db->exec($req)){
				//Tools::debugVar($this->db->lastInsertId());
				  return $this->db->lastInsertId();
			  }
			  else{
				switch($entry['table']){
					case 'langue':
						$getComp = $this->getListLangues(array($entry['entry']));
						return $getComp[0]->id_langue;
					
					case 'competence':
						$getComp = $this->getListCompetences(array($entry['entry']));
						return $getComp[0]->id_competence;
					
					case 'poste' :
						$getComp = $this->getListPostes(array($entry['entry']));
						return $getComp[0]->id_poste;
				}
			  }
	}
	public function getListExperiences(){
		$experiences = array();
		$req = "SELECT id_experience, adm_experience_".$_SESSION["langue"]." AS adm_experience FROM adm_experiences WHERE actif = 1 ";
			
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'pink', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
		
		$resultats = $this->db->query($req);

		while ($experience = $resultats->fetch(PDO::FETCH_OBJ))
			 $experiences[] = $experience;	
		
		$resultats->closeCursor();
		
		if(count($experiences)){
			return $experiences;
		}
		else{
			return 'Récuperation des experiences impossible !';
		}
		
	}
        
        public function getListCivilites(){
		$civilites = array();
		$req = "SELECT id_civilite, adm_civilite_".$_SESSION["langue"]." AS adm_civilite FROM adm_civilites WHERE actif = 1 ";
			
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'pink', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
		
		$resultats = $this->db->query($req);

		while ($civilite = $resultats->fetch(PDO::FETCH_OBJ))
			 $civilites[] = $civilite;	
		
		$resultats->closeCursor();
		
		if(count($civilites)){
			return $civilites;
		}
		else{
			return 'Récuperation des civilites impossible !';
		}
		
	}
        
        public function getListMonnaies(){
		$monnaies = array();
		$req = "SELECT id_monnaie, adm_monnaie FROM adm_monnaies WHERE actif = 1 ";
			
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'pink', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
		
		$resultats = $this->db->query($req);

		while ($monnaie = $resultats->fetch(PDO::FETCH_OBJ))
			 $monnaies[] = $monnaie;	
		
		$resultats->closeCursor();
		
		if(!DEBUG)
			return $monnaies;
		
		if(count($monnaies)){
			return $monnaies;
		}
		else{
			return 'Récuperation des monnaies impossible !';
		}
		
	}
	public function getListStructures(){
		$structures = array();
		$req = "SELECT id_structure, adm_structure_".$_SESSION["langue"]." AS adm_structure FROM adm_structures WHERE actif = 1 ";
			
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'pink', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
		
		$resultats = $this->db->query($req);

		while ($structure = $resultats->fetch(PDO::FETCH_OBJ))
			 $structures[] = $structure;	
		
		$resultats->closeCursor();
		
		return $structures;
		
	}
	public function getListContrats(){
		$contrats = array();
		$req = "SELECT id_contrat, adm_contrat_".$_SESSION["langue"]." AS adm_contrat FROM adm_contrats WHERE actif = 1 ";
			
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'pink', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
		
		$resultats = $this->db->query($req);

		while ($contrat = $resultats->fetch(PDO::FETCH_OBJ))
			 $contrats[] = $contrat;	
		
		$resultats->closeCursor();
		
		if(!DEBUG)
			return $contrats;

		if(count($contrats)){
			return $contrats;
		}
		else{
			return 'Récuperation des contrats impossible !';
		}
		
	}
	
	public function getListPostes($search = array(), $id=array()){
		$postes = array();
		$req = "SELECT  id_poste, adm_poste_".$_SESSION["langue"]." AS adm_poste FROM adm_postes WHERE actif = 1 ";
		
		if(count($search)){
			//On traite les string du tableau
			foreach ($search as $key => $non_escape_string){
				$search[$key] = addslashes($non_escape_string);
			}
			$req .= "AND MATCH (adm_poste_".$_SESSION["langue"].") AGAINST ('".implode("','", $search)."' IN BOOLEAN MODE)";
		}
		
		if(count($id))
			$req .= "AND id_poste IN ('".implode("','", array_filter($id))."')";
			
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'pink', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
		
		//Tools::debugVar($req, false);
		
		$resultats = $this->db->query($req);

		while ($poste = $resultats->fetch(PDO::FETCH_OBJ))
			 $postes[] = $poste;	
		
		$resultats->closeCursor();
		
		return $postes;
		
	}
	
	public function getListCompetences($search = array(), $id=array()){
		
		$competences = array();
		$req = "SELECT  id_competence, adm_competence_".$_SESSION["langue"]." AS adm_competence FROM adm_competences WHERE actif = 1 ";
		
		if(count($search)){
			//On traite les string du tableau
			foreach ($search as $key => $non_escape_string){
				$search[$key] = addslashes($non_escape_string);
			}
			
			$req .= "AND MATCH (adm_competence_".$_SESSION["langue"].") AGAINST ('".implode("','", $search)."' IN BOOLEAN MODE)";
		}
		
		if(count($id))
			$req .= "AND id_competence IN ('".implode("','", array_filter($id))."')";
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'pink', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
		
		$resultats = $this->db->query($req);

		while ($competence = $resultats->fetch(PDO::FETCH_OBJ))
			 $competences[] = $competence;	
		
		$resultats->closeCursor();
		
		return $competences;
		
	}
	
	public function getListCategories($search = array(), $id=array()){
		$categories = array();
		$req = "SELECT id_categorie, adm_categorie_".$_SESSION["langue"]." AS adm_categorie FROM adm_categories WHERE actif = 1 ";
		
		if(count($search))
			$req .= "AND MATCH (adm_categorie_".$_SESSION["langue"].") AGAINST ('".implode("','", $search)."' IN BOOLEAN MODE)";
	
		if(count($id))
			$req .= "AND id_categorie IN ('".implode("','", array_filter($id))."')";
			
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'pink', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	

		$resultats = $this->db->query($req);

		while ($categorie = $resultats->fetch(PDO::FETCH_OBJ))
			 $categories[] = $categorie;	
		
		$resultats->closeCursor();
		
		return $categories;
		
	}
	public function getListEtudes($search = array(), $id=array()){
		$etudes = array();
		$req = "SELECT id_etude, adm_etude_".$_SESSION["langue"]." AS adm_etude FROM adm_etudes WHERE actif = 1 ";
		
		if(count($search))
			$req .= "AND MATCH (adm_etude_".$_SESSION["langue"].") AGAINST ('".implode("','", $search)."' IN BOOLEAN MODE)";
	
		if(count($id))
			$req .= "AND id_etude IN ('".implode("','", array_filter($id))."')";
		
			
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'pink', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
		
		$resultats = $this->db->query($req);

		while ($etude = $resultats->fetch(PDO::FETCH_OBJ))
			 $etudes[] = $etude;
			 
		$resultats->closeCursor();
		
		return $etudes;
		
	}
	public function getListLangues($search = array(), $id=array()){
		$langues = array();
		$req = "SELECT id_langue, adm_langue_".$_SESSION["langue"]." AS adm_langue FROM adm_langues WHERE actif = 1 ";
		
		if(count($search)){
			//On traite les string du tableau
			foreach ($search as $key => $non_escape_string){
				$search[$key] = addslashes($non_escape_string);
			}
			
			$req .= "AND MATCH (adm_langue_".$_SESSION["langue"].") AGAINST ('".implode("','", $search)."' IN BOOLEAN MODE)";
		}
	
		if(count($id))
			$req .= "AND id_langue IN ('".implode("','", array_filter($id))."')";
			
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'pink', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
		
		$resultats = $this->db->query($req);

		while ($langue = $resultats->fetch(PDO::FETCH_OBJ))
			 $langues[] = $langue;	
		
		$resultats->closeCursor();
		
		return $langues;
		
	}
	
	public function getListStatutsReponses(){
		$statuts = array();
		$req = "SELECT id_reponse_statut, adm_reponse_statut_".$_SESSION["langue"]." AS adm_statut FROM adm_reponses_statuts WHERE actif = 1 ";
			
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'pink', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
		
		$resultats = $this->db->query($req);

		while ($statut = $resultats->fetch(PDO::FETCH_OBJ))
			 $statuts[] = $statut;	
		
		$resultats->closeCursor();
		
		return $statuts;
		
	}
	public function getListStatutsMissions(){
		$statuts = array();
		$req = "SELECT id_mission_statut, adm_mission_statut_".$_SESSION["langue"]." AS adm_statut FROM adm_missions_statuts WHERE actif = 1 ";
			
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'pink', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
		
		$resultats = $this->db->query($req);

		while ($statut = $resultats->fetch(PDO::FETCH_OBJ))
			 $statuts[] = $statut;	
		
		$resultats->closeCursor();
		
		return $statuts;
		
	}
	//Uniquement pour les recherches autocomplete
	final function getListSelect($table, $string, $obj=false, $debug=false){
		$select = array();

		if($obj)
			$req .= "SELECT id_".$table." AS id,  adm_".$table."_".$_SESSION['langue']." AS value FROM adm_".$table."s  WHERE adm_".$table."_".$_SESSION['langue']." LIKE  '%".$string."%' LIMIT 10";
		else
			$req .= "SELECT id_".$table." ,adm_".$table."_".$_SESSION['langue']." AS value FROM adm_".$table."s WHERE adm_".$table."_".$_SESSION['langue']." LIKE  '%".$string."%'  LIMIT 10";
		
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'pink', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
			
		$resultats = $this->db->query($req);
		
		
		if(!$obj){
			while ($ligne = $resultats->fetch(PDO::FETCH_ASSOC)){
				 $select[] = array('id'=>$ligne["id_".$table], 'libelle'=>$ligne["adm_".$table]);	
			}
		}else{
		
			
			while ($ligne = $resultats->fetch(PDO::FETCH_ASSOC)){
				 $select[] =$ligne;	
			}
		}
		
		
		$resultats->closeCursor(); 
		return $select;
		
	}
    
	// ----------------------------------------------------------------- //
	// ------------------------- Traductions -------------------------- //
	// ----------------------------------------------------------------- //
	
	//Génération du xml de traduction d'une page
	public function generateXmlTraduction($pageId){
		
			
		$req = "SELECT trad.id_traduction, tra_fr, tra_en, tra_es, libelle, pages.id_page, titre, nom, environnement FROM tra_page_traduction page_trad 
			 INNER JOIN tra_pages pages
			  ON pages.id_page = page_trad.id_page
			   INNER JOIN tra_traductions trad
			    ON trad.id_traduction = page_trad.id_traduction
			     WHERE page_trad.id_page = ".$this->db->quote($pageId, PDO::PARAM_STR)." ORDER BY trad.id_traduction ASC;";
		
		 
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'pink', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
			
		$resultats = $this->db->query($req);
		
		$tab_trad = array();
		
		while ($ligne = $resultats->fetch(PDO::FETCH_ASSOC)){
			$tab_trad[] = $ligne;	
	       }
	       
	      // return $tab_trad;
	
		//Transformation du tableau en xml
		//D'abbord on creer le doctype
		$implementation = new DOMImplementation();
 
		$dtd = $implementation->createDocumentType('language',
			'',
			'xmldoctype.dtd');
		 
		//Instanciation du nouveau xml en UTF8
		$xml = $implementation->createDocument('', '', $dtd);
		
		// Définition des autres propriétés
		$xml->encoding = 'UTF-8';
		
		//Creation du noeud de premier niveau language
		$lang = $xml->createElement("language");
		
		$lang_id_page =  $xml->createAttribute("page-id");
		$lang_id_page->value = $tab_trad[0]['id_page'];
		$lang->appendChild($lang_id_page);
		
		$lang_name_page  = $xml->createAttribute("page-name");
		$lang_name_page->value = $tab_trad[0]['nom'];
		$lang->appendChild($lang_name_page);
		
		$lang_titre_page  = $xml->createAttribute("page-titre");
		$lang_titre_page->value = $tab_trad[0]['titre'];
		$lang->appendChild($lang_titre_page);
		
		$xml->appendChild($lang);
		
		//On parcours notre tableau pour créer un nouvel element à chaque index.
		//D'aboord je crée un compteur pour les index qui n'ont pas de label
		$unknown_cpt = 1;
		
		foreach ($tab_trad as $key=>$trad){
			//Si la traduction française n'existe pas, on passe à l'incrémentation suivante
			if($trad['tra_fr'] == "" || $trad['tra_fr'] == NULL)
				continue;
			//On crée le noeud data
			$data = $xml->createElement("data");
			//On cree l'attribut id du noeud data
			$data_id = $xml->createAttribute("id");
				//Si la valeur du libelle est vide l'id = unknown+N et on incremente le compteur
			if($trad['libelle'] == "" || $trad['libelle'] == NULL){
				$data_id->value =  "unknown".$unknown_cpt;
				$unknown_cpt++;
			}else{
				$data_id->value = $trad['libelle'];
			}
			//On affect l'attribut id à son element
			$data->appendChild($data_id);
			//ON cree l'attribut dbId du noeud data
			$data_dbId = $xml->createAttribute("dbId");
			$data_dbId->value = $trad['id_traduction'];
			//On affect l'attribut dbId à son element
			$data->appendChild($data_dbId);
			//On ajoute le noeud data au xml
			$lang->appendChild($data);
			
			//LES NOEUDS DE TRADUCTION
			//fr
			$tradfr = $xml->createElement("trad", $trad['tra_fr']);
			$tradFrAttribut = $xml->createAttribute("lang");
			$tradFrAttribut->value="fr";
			$tradfr->appendChild($tradFrAttribut);
			//en
			$traden = $xml->createElement("trad", $trad['tra_en']);
			$tradEnAttribut = $xml->createAttribute("lang");
			$tradEnAttribut->value="en";
			$traden->appendChild($tradEnAttribut);
			//es
			$trades = $xml->createElement("trad", $trad['tra_es']);
			$tradEsAttribut = $xml->createAttribute("lang");
			$tradEsAttribut->value="es";
			$trades->appendChild($tradEsAttribut);
			
			//On implement les trad dans le noeud data
			$data->appendChild($tradfr);
			$data->appendChild($traden);
			$data->appendChild($trades);	
		}
		
		//ON sauvegarde le xml sous le format sitetrade_page.xml
		//On le formate
		$xml->formatOutput = true;
		$xml_name = "sitetrade_".$tab_trad[0]['nom'].".xml";
		
		//ON récupère l'environnement
		switch($tab_trad[0]['environnement']){
			
			case 1 :
			    $environnement = 'prestataire';
			    $suffix = '-';
			break;
			
			case 2:
			    $environnement = 'client';
			    $suffix = '-';
			break;
			
			case 3 :
			    $environnement = '';
			    $suffix = '-';
			    break;
			default;
			    $environnement = 'visiteur';
			    $suffix = '-';
			break;
		}
		
		if(!$xml->save( _XML_ . $environnement. '/' .$xml_name))
			return false;
		
		return true;
	
		
	}
	
	//Recupération de toute les pages d'un envieonnement
	public function getEnvironnementPages($filtre=array()){
		$req = "SELECT id_page, titre,nom, environnement FROM  tra_pages
			   WHERE ";
		if(count($filtre)){
			foreach ($filtre as $champ => $valeur){
				$req .= $champ." = ".$valeur." AND ";
			}
		}
		
		$req .= "1 = 1";
			   
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'pink', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	
			
		$resultats = $this->db->query($req);
		
		$liste_page = array();
		
		while ($ligne = $resultats->fetch(PDO::FETCH_OBJ)){
			$liste_page[] = $ligne;	
	       }
	       
	       return $liste_page;
	}

	public function getListEnvironnementPages($filtre = array(), $searchFiltre=array(), $limit=array(0,NOMBRE_PER_PAGE), $matching=false){
		//$matching indique que la methode doit trouver des id_mission dans toutes les tables adm_ sinon el renvoi false
		$pages = array();
		
		$counter = array(
				'COUNT' => 'SELECT count(DISTINCT(id_page)) AS total, ' ,
				'SELECT'=> 'SELECT DISTINCT(id_page), '
				);

		$req = " titre,nom, environnement
			FROM tra_pages
			WHERE modifie_le ";
			
		foreach($filtre as $key=>$value){
				if(is_array($value)){
					if(count($value))
						$req .= " AND ".$key." IN ('".implode("','", $value)."')";
					else
						$req .= " AND ".$key." IN ('')";	
				}
				else
					$req .= " AND ".$key." = ".$this->db->quote($value, PDO::PARAM_STR);
		}	
		
		
		//Si on a des entrées dans la recherche
		if(is_array($searchFiltre) && count($searchFiltre)){
			
			if((count($searchFiltre['mots_trad']) && !empty($searchFiltre['mots_trad'])) || (count($searchFiltre['id_langue']) && !empty($searchFiltre['id_langue'])) ||(count($searchFiltre['id_poste']) && !empty($searchFiltre['id_poste'])) || (count($searchFiltre['id_competence']) && !empty($searchFiltre['id_competence']))){
				//Tools::debugVar($searchFiltre);
				/*ON VA BASCULER TOUTES NOS ENTREES RECU EN TABLEAU*/
				/*On sous entend que les valeur seront déparé par des virgules dans le moteur de recherche*/
				$keywords   = array();
				//Est ce que l'on a demandé une recherche sur mot_cle
				if(!is_array($searchFiltre['mots_trad']) &&  isset($searchFiltre['mots_trad'])){
					if(empty($searchFiltre['mots_trad']))
						$keywords = array();
					else
						$keywords = explode(',' , $searchFiltre['mots_trad']);
					
				}
				
				
				if(count($searchFiltre['mots_trad']) && !empty($searchFiltre['mots_trad'])){
					
					$string = addslashes(implode("* ", $keywords)); 
					$req  .=   "AND MATCH (titre, nom) AGAINST ('$string*' IN BOOLEAN MODE)";
					
			
				
					
				}
				
			}
				
			if(isset($searchFiltre['environnement'])){
				
				$req .= "AND (environnement = ".$searchFiltre['environnement']." ) ";
				
			}
			
		}
		
		
		$req .= " ORDER BY id_page ASC ";
	
		if(is_array($limit)){
			$limit = array(
					'COUNT' => '' ,
					'SELECT'=> "LIMIT ".implode(",", $limit)
					);
		}
		
		//Tools::debugVar($counter['COUNT'].$req.$limit['COUNT']);
		if($debug)
			throw new myException($counter['COUNT'].$req.$limit['COUNT']);
		if(DEBUG)
			parent::$request[] =  array('color'=>'orange', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$counter['COUNT'].$req.$limit['COUNT']);	
		
		
		//Le count avec la requete
		$resultats = $this->db->query($counter['COUNT'].$req.$limit['COUNT']);
		
		//si on un total superieur a zéro
		//on reexecute la requete afin de recuperer les elements
                if($count = $resultats->fetch(PDO::FETCH_OBJ)->total){
			
			if($debug)
				throw new myException($counter['SELECT'].$req.$limit['SELECT']);
			if(DEBUG)
				parent::$request[] =  array('color'=>'brown', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$counter['SELECT'].$req.$limit['SELECT']);
						
			$resultats = $this->db->query($counter['SELECT'].$req.$limit['SELECT']);

			while ($page = $resultats->fetch(PDO::FETCH_ASSOC)){
				
				$pages[] = $page;	
			}
			
			$resultats->closeCursor();
			
                }
		return array('count'=>$count, 'pages'=>$pages);
		
	}
	
	public function saveXMLTrad($id_page="", $tab=array()){
		$req = "INSERT INTO tra_traductions SET ";
		
		if(count($tab)){
			foreach ($tab as $champ => $valeur){
				$req .=  $champ. " = " . $this->db->quote($valeur, PDO::PARAM_STR).", ";
			}
		}
		
		$req .= "trad_cle_unique =  ".$this->db->quote($tab['libelle']."-P".$id_page, PDO::PARAM_STR).", ";
		$req .=	" modifie_le = NOW()
			ON DUPLICATE KEY UPDATE tra_fr = ".$this->db->quote($tab['tra_fr'], PDO::PARAM_STR).", tra_en= ".$this->db->quote($tab['tra_en'], PDO::PARAM_STR).", tra_es= ".$this->db->quote($tab['tra_es'], PDO::PARAM_STR) ;
			  
		if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	

		if($id = $this->db->exec($req)){
		      //Tools::debugVar($this->db->lastInsertId());
			return $this->saveXMLTradPage($id_page, $this->db->lastInsertId());
		}
		return true; 
	}
	
	public function saveXMLTradPage($id_page="", $id_trad=""){
		$req .= "INSERT INTO tra_page_traduction SET
		 id_page = ".$this->db->quote($id_page, PDO::PARAM_STR).",
		 id_traduction = ".$this->db->quote($id_trad, PDO::PARAM_STR).",
		 modifie_le = NOW()";
		 
		 //Tools::debugVar($req);
		 if($debug)
			throw new myException($req);
		if(DEBUG)
			parent::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$req);	

		if($id = $this->db->exec($req)){
		      //Tools::debugVar($this->db->lastInsertId());
			return true; 
		}
		
		return false;
	}

}
