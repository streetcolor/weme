<?PHP
//On gere les routes dans ce fichier
//Fonctionnement : On appelle une page via le paremetre $_GET['page']
//Si cette page existe elle est forcement gérée par un controleur qui va récuperer ses contenus
//La page peut aussi appeler une action avec le parametre $_GET["action"].
//Dans tous les cas on appelle le controleur associée a la page via le gestionnaire de routes
//Les action associées aux controleurs seront gérées au sein meme du controleur dédié.
//Attention lors de la création d'une nouvelle page il faudra veiller à bien lui definir la route vers son controleur
//Si on souhaite utiliser un controleur directement on utilise $_REQUEST["controller"]
//Attention l'action doit exister sur un controlleur sinon on aura une belle 404 !
try{
    
    $page = $_REQUEST["page"] ? $_REQUEST["page"] : 'loggin';
    
    if(!$_REQUEST["controller"]){
	switch($page){
	    
	    case 'loggin':
		$controleur = 'home';
		break;
	    
	    case 'home':
		$controleur = 'home';
		break;
	 
	    case 'prestataires':
		$controleur = 'membre';
		break;
	    
	    case 'clients':
		$controleur = 'membre';
		break;	 
	 
	    case 'prestataire':
		$controleur = 'membre';
		break;
	    
	    case 'client':
		$controleur = 'membre';
		break;
	 
	    case 'cv':
		$controleur = 'cvteque';
		break;
	    
	    case 'cvteque':
		$controleur = 'cvteque';
		break;

	    case 'email':
		$controleur = 'message';
		break;

	    case 'mission':
		$controleur = 'mission';
		break;
	    
	    case 'missions':
		$controleur = 'mission';
		break;

	    case 'responses':
		$controleur = 'reponse';
		break;
	    
	    case 'response':
		$controleur = 'reponse';
		break;
	    
	     case 'messages':
		$controleur = 'message';
		break;
	    
	    case 'traduction':
		$controleur = 'traduction';
	    break;
	    
	    case '404':
		$controleur = 'notfound';
		break;
	    
	    
	    default:
		 //$controleur = 'notfound';
		break;
	
	}
    }
    else{
	$controleur = $_REQUEST["controller"];
    }
   
    include_once(_CTRL_.'controller.php');
    if (isset($controleur) && file_exists(_CTRL_.str_replace('.', '', $controleur).'.php')){
	

	include(_CTRL_.$controleur.'.php');
	$className  = $controleur.Controller;
        $controller = new $className($smarty,$_REQUEST);
	
	$funcname = $_REQUEST["action"] ? $_REQUEST["action"].'Action' : 'defaultAction';
	
        if (method_exists($className, $funcname)) {
	    
	    $controller->$funcname(); // Appelle la fonction action du controleur
        }
        else{
	    
	    include(_CTRL_.'notfound.php');
	    $className  = 'notfoundController';
	    $unfound = new $className($smarty,$_REQUEST);
	    if(DEBUG)
		$unfound->unfoundAction('Attention impossible de charger la fonction associée à l\'action suivante : '.$funcname); // Appelle la fonction action du controleur
	    else
		$unfound->unfoundAction('La page que vous tentez d\'afficher n\'existe pas'); // Appelle la fonction action du controleur
        }   
        
    }
    else{
	include(_CTRL_.'notfound.php');
	$className  = 'notfoundController';
	$unfound = new $className($smarty,$_REQUEST);
	$unfound->unfoundAction('Attention aucun controleur de défini pour la page suivante : '.$page); // Appelle la fonction action du controleur
    }    
    
}
catch(myException $e){
        if(DEBUG)
	    echo $e->getMessage();
        $e->traiteerror();
}
catch(PDOException $e){
	if(DEBUG)
	    echo $e->getMessage();
        //$e->traiteerror();
}

