<?PHP
//Définition de la time zone
date_default_timezone_set('Europe/Paris');
//On affiche toutes les erreurs sauf les notice
error_reporting(E_ALL ^ E_NOTICE);
//On active le log des erreur dans un fichier
ini_set('log_errors', 'On');
//Dans ce ficheir
ini_set('error_log', '../logs/error.log');
ini_set('memory_limit', '64M'); // 
set_time_limit(600); // en secondes
ini_set('upload_max_filesize', '80M');
ini_set('post_max_size', '80M');

ini_set('display_errors', 0);

// Initialisation de la session
session_name("back");

session_start();
header("Cache-Control: no-cache");


// Chargement des variables Defines
require('defines.inc.php');

//Chargement d'environnement smarty
require(_TOOLS_ . 'smarty/libs/Smarty.class.php');

// On charge toutes la classes du Coeur (core) a l'aide d'un auto load
function chargerClasse($classname){
    if (is_file(_CORE_.$classname.'.class.php')){
	require_once _CORE_.$classname.'.class.php';
    }
}

spl_autoload_register('chargerClasse');
include_once('connect.inc.php');

// Initialisation Smarty
//global $smarty ;
$smarty = new Smarty();

//Chargement de l'environnement de trad global (pour les traduction par page voir controlleur controller)
$global_trad = new XMLEngine( _PATH_BACK__.'/web/xml/sitetrade_globales.xml');
$smarty->assign('globale_trad', $global_trad);