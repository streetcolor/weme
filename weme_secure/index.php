<?php
//Initialisation de l'environnement
include('./config/config_init.php');
//Le routing permet de selectionner le controleur en fonction de la page sur la quelle on se trouve
//Le controleur determine ensuite l'action à accomplir sur le page en fonction du $_REQUEST["action"]
include('./config/routing.php');

/****************************************************************************************************************/
/********************************    ICI ON affiche nos templates  **********************************************/
/****************************************************************************************************************/
?>
<!DOCTYPE html>
<html lang="en">
 
  <head>
    <?PHP
        $smarty->display(_TPL_ .'template/meta.tpl');
        $smarty->display(_TPL_ .'template/css'.DBMembre::$id_type.'.tpl');
    ?>
  </head>

  <body>

<?PHP

if(DBMembre::$id_identifiant){
  $smarty->display(_TPL_ .'template/header.tpl');
}
//Plusieurs cas de figure
//Soit on affiche la page qui est définie manuellement à la sortie du controlleur via la variable $setTemplate
//Soit on affiche la variable passée dans l'url via $_GET[page]
//Si la page existe bien dans le réperoire des vue on l'affiche sinon on afficle le template 404.tpl

//On demarre la temporisation
ob_start('Tools::replaceFlush');

$page = isset($setTemplate) &&  !empty($setTemplate) ? $setTemplate : $_GET['page'];

if (isset($page) && file_exists(_TPL_.'pages/'.str_replace('.', '', $page).'.tpl')){
    $smarty->display(_TPL_.'pages/'.$page.'.tpl');
}

//Si le template que l'on tente d'appeler n'exsite pas
elseif(isset($page) && !file_exists(_TPL_.'pages/'.str_replace('.', '', $page).'.tpl')){
    $smarty->display(_TPL_.'pages/404.tpl');
}
//Sinon on maintien la home
else{
    $smarty->display(_TPL_ .'pages/home.tpl');
}

//On vide la temporisation
ob_end_flush();


if(DBMembre::$id_identifiant){
  $smarty->display(_TPL_ .'template/footer.tpl');
}

  $smarty->display(_TPL_ .'template/js.tpl');

if(DEBUG){

    $smarty->assign('debug', DBConnect::$request);
    $smarty->display(_TPL_ .'template/debug.tpl');
}
?>

  </body>
</html>
