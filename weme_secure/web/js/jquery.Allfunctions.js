$(document).ready(function() {
          
	  $(function () {
		 $('.checkall').on('click', function () {
		     $(this).closest('table').find(':checkbox').prop('checked', this.checked);
		 });
	  });
	       
	  
	  if ($('.homeDroite').height()>1000) {
		    $('.menuVertical').height($(".homeDroite").height());
	  }
	  
	  //Utilisation des ancres
	  if(location.hash){
		    $('html,body').animate({scrollTop: $(location.hash).offset().top-110},'slow');
	  }   
 
	  $(document).on('click', ".scroller", function (event, $hash) {
		
		    $('html,body').animate({scrollTop: $($(this).attr('href')).offset().top-110},'slow');
          });
	  
          //Google map
          $("#geocomplete").geocomplete().bind("geocode:result", function(event, result){
                    
                    $("#lo_quartier").val('');
                    $("#lo_ville").val('');
                    $("#lo_departement").val('');
                    $("#lo_departement_short").val('');
                    $("#lo_region").val('');
                    $("#lo_region_short").val('');
                    $("#lo_pays").val('');
                    $("#lo_pays_short").val('');
                    
                    
                    $localite = new Object();
                    
                    $.each(result.address_components, function(index, value){
                    
                             switch (value.types[0]) {
                                        
                                        case 'street_number'://numero de rue
                                                  break;
                                        
                                        case 'route'://rue
                                                  break;

                                        case 'sublocality'://quartier
                                                  $localite.lo_quartier = value.long_name;
                                                  break;
                                        
                                        case 'locality'://department
                                                  $localite.lo_ville = value.long_name;
                                                  break;
                                        
                                        case 'administrative_area_level_2'://department
                                                  $localite.lo_departement = value.long_name;
                                                  $localite.lo_departement_short = value.short_name;
                                                  break;
                                        
                                        case 'administrative_area_level_1'://region
                                                  $localite.lo_region = value.long_name;
                                                  $localite.lo_region_short = value.short_name;
                                                  break;
                                       
                                        case 'country'://pays
                                                  $localite.lo_pays = value.long_name;
                                                  $localite.lo_pays_short = value.short_name;
                                                  break;
                              
                             }
                             
                    })
                    
                    
                    if ($("#lo_pays_short").length) {//SI le champs lo_ville existe dans la page on le rempli aisni que les autres
                             $("#lo_quartier").val($localite.lo_quartier);
                             $("#lo_ville").val($localite.lo_ville);
                             $("#lo_departement").val($localite.lo_departement);
                             $("#lo_departement_short").val($localite.lo_departement_short);
                             $("#lo_region").val($localite.lo_region);
                             $("#lo_region_short").val($localite.lo_region_short);
                             $("#lo_pays").val($localite.lo_pays);
                             $("#lo_pays_short").val($localite.lo_pays_short);
 
                    }
                    else{
                              var dataString = $.toJSON($localite);
                              $.post('web/ajax/geocomplete.ajax.php', {data: dataString}, function(res){
                              
          
                                         $("#localiteReceive").val(res);
                              });       
                    }
                
          })
          
          //Date picker
          $('.date').datepicker();
          
          //Champs wysywyg
	  if ($('#redactor_content').length || $('.wysywyg').length) {
		    //code
	  
		    $('#redactor_content, .wysywyg').redactor({
				buttons: ['formatting', '|', 'bold', 'italic', 'underline', 'deleted', '|', 'unorderedlist', 'orderedlist', 'outdent', 'indent', '|', 'table', 'link', '|', '|', 'alignment', '|', 'horizontalrule'],
				lang: 'fr',
				toolbarFixed: true
				
		    });	
	  
	  }

	   
           
         $(".fancyHtml").fancybox({
		maxWidth	: 800,
		maxHeight	: 600,
		fitToView	: false,
		width		: '100%',
		height		: '100%',
		autoSize	: false,
		closeClick	: true,
		openEffect	: 'none',
		closeEffect	: 'none'
	});
           
	   //Fancy show CV
           $(document).on("click", ".showCv", function (e) {
                      
                    e.preventDefault(); 
                    $link = $(this).attr('href').split('?');

                    $.ajax({
                              type: "POST",
                              cache: false,
                              url: "web/ajax/ajax.cv.php",
                              data: $link[1],
                              success: function (data) {
                                        $.fancybox(data);                          
                              }
                    });
                    return false;
          })
	   //Partie permettant de masquer la colone menuVerticale   
    
    var $menuVerticalOpen = $('#header .span2').width();
    var $menuVerticalClose = 55;
    var $homeDroiteOpen = $(window).width() - $('#header .span2').width();
    var $homeDroiteClose = $(window).width() - $menuVerticalClose - 40;
    if ($.cookie('toggle') == 1) {
        $('.toggle').addClass('toggleClose');
        $('.menuVertical').width($menuVerticalClose);
        $('.homeDroite').width($homeDroiteClose);
    }
   
   
    $(document).on("click", ".toggle", function () {
        $(".menuVertical").animate({
            width: ($.cookie('toggle') == 1) ? $menuVerticalOpen : $menuVerticalClose
        }, {
            duration: 100,
            specialEasing: {
                width: "linear"
            },
            complete: function () {}
        });
        $(".homeDroite").animate({
            width: ($.cookie('toggle') == 1) ? $homeDroiteOpen : ($(window).width() - $menuVerticalClose)
        }, {
            duration: 100,
            specialEasing: {
                width: "linear"
            },
            complete: function () {
                if ($.cookie('toggle') == 1) {
                    $.cookie('toggle', null);
                    $('.toggle').removeClass('toggleClose');
                } else {
                    $.cookie('toggle', 1, {
                        expire: 7,
                        path: '/'
                    });
                    $('.toggle').addClass('toggleClose');
                }
            }
        });
    });
});