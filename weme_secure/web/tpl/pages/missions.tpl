<div class="container-fluid row-fluid corps">
    
    {include file='web/tpl/template/menu.tpl'}
    
    <div class="span10 homeDroite">
        
	<div class="search">
            
	    <form class="form-inline homeDroiteForm" method="POST" action="{getlink page=$page  action='search'}" >
                
		<input type="text" name="search[mots_cle]" value="{$smarty.session.search['mots_cle']}" placeholder="{$globale_trad->searchProjet}" class="span12 homeDroiteSearch"/><br/>
                
		<div class="row-fluid" id="filter">
		    
		    <div class="span12">
			
			<a href="{getlink page=$page action=''}">
			    <img class=" pull-left border-right-1 padding-0" src="web/img/ico/ico_reload.png" alt="reload" title="reload" />
			</a>
			
			<span class="pull-left label-form">{$globale_trad->label_lieu}</span>
	
			<div class="border-right-1 pull-left ">
			
			    <input  id="geocomplete" type="text" value="{$smarty.session.search['lo_region_short']}" class="span12 formModification"/>
			    <input type="hidden" name="search[lo_departement_short]" id="lo_departement_short" value="{$smarty.session.search['lo_departement_short']}">
			    <input type="hidden" name="search[lo_region_short]" id="lo_region_short" value="{$smarty.session.search['lo_region_short']}">
			    <input type="hidden" name="search[lo_pays_short]" id="lo_pays_short" value="{$smarty.session.search['lo_pays_short']}">
			  
			    <div class="clearfix"></div>
			</div>
			
	
				
			<div class="border-right-1 padding-left-10 span2 ">
			    <div class="span12 formModificationSelectDiv">
				<select class=" formModificationSelect" name="search[mi_anciennete]">
				    {foreach  from=$listPublications key=k item=v}
				    <option {if $k==$smarty.session.search.mi_anciennete}selected="selected"{/if} value="{$k}">{$v}</option>
				    {/foreach}
				</select>
				<div class="clearfix"></div>
			    </div>
			</div>
			
			<div class="border-right-1 padding-left-10  span2">
			    <div class="span12 formModificationSelectDiv">
				<select class=" formModificationSelect" name="search[id_mission_statut]">
				    <option value="">{$globale_trad->label_statut}</option>
				    {foreach $getListStatuts as $statut}
					<option {if $statut->id_mission_statut==$smarty.session.search.id_mission_statut}selected="selected"{/if} value="{$statut->id_mission_statut}">{$statut->adm_statut}</option>
				    {/foreach}
				</select>
				<div class="clearfix"></div>
			    </div>
			</div>
			
			<span class="pull-left label-form">{$globale_trad->label_date}</span>
			
			<div class="border-right-1 pull-left ">
			    
			    <div  data-date-format="{$datepicker.dateFormat}" data-date="{$smarty.request.search['mi_debut']}" class="  span6 input-append date">
				<input type="text" value="{if $smarty.request.search['mi_debut']}{$smarty.session.search['mi_debut']}{else}{$globale_trad->label_du}{/if}" size="16" name="search[mi_debut]" class="span12 ">
				<span class="add-on"><i class="icon-calendar"></i></span>
			    </div>
			    
			    <div  data-date-format="{$datepicker.dateFormat}" data-date="{$smarty.request.search['mi_fin']}" class="  span6 input-append date">
				<input type="text" value="{if $smarty.request.search['mi_fin']}{$smarty.session.search['mi_fin']}{else}{$globale_trad->label_au}{/if}" size="16" name="search[mi_fin]" class="span12">
				<span class="add-on"><i class="icon-calendar"></i></span>
			    </div>
			
			    <div class="clearfix"></div>
			    
			</div>
			
			
			<div class="border-right-1 pull-left padding-left-10">
		    
			    <label class="checkbox">
				<input type="checkbox" {if $smarty.session.search.mi_reponse_count}checked="checked"{/if} name="search[mi_reponse_count]"> {$globale_trad->label_reponse}
			    
			    </label>
			    
			    <div class="clearfix"></div>
			    
			</div>
			
			<input class=" pull-left last border-right-1 padding-0" type="image" src="web/img/ico/ico_search_form.png" alt="reload" title="reload" />
			
			<div class="clearfix"></div>
			
		    </div>
		
		</div>
		
            </form>
        </div>
	
	
	
	
	{*Si la requete getLitsMissions() renvoi des missions*}
	{if isset($count)}
	    <form method="post">
		<h1 class="result span6">
			Résultat de votre recherche : <b>{$count} Missions</b>
		</h1>
		 <button type="button" class="margin-0 margin-top-10 pull-right btn btn-primary" onclick="javascript: window.open('web/scripts/cronInfomProfil.php','Script emailing','menubar=no, status=no, scrollbars=yes, width=800');">
		    <span class=" icon-envelope icon-white"></span>
		    Informer la communauté des nouvelles missions postées
		</button>
		 
		<div class="clearfix"></div> 
		 
		<div class="">
		    {if isset($error['id_mission_statut']) || isset($error['id_identifiant']) || isset($error['id_mission_statut'])}
		    <div class="alert alert-error span12">
			{*On envoi nos variable de retour error ou success callBack*}
			<p>{$error['id_mission']}	</p>
			<p>{$error['id_identifiant']}	</p>
			<p>{$error['id_mission_statut']}	</p>	
		    </div>
		    {/if}
		</div>
		{$messageCallBack}

		<table width="100%" cellpadding="5" class=" mission">
		    <thead class="MissionListHeader">
			<tr>
				
				
				<td class=""><input type="checkbox" class="checkall"></td>
				
				<td align="center">Statut</td>
				
				<td  align="center"">Contact</td>
				
				<td  align="center">Titre de la mission</td>
		
				<td align="center" class="">Tarif</td>
	
				<td align="center" class="">Dates</td>
				
				<td align="center" class="">Réponses</td>
				
				<td align="center" class="">Modifié le</td>
				
				<td align="center" class="last">Action</td>
				
			</tr>
		    </thead>
		
		    <tbody>
			{foreach $listMissions as $mission}        
			    {include file='web/tpl/template/mission.tpl'}
			{/foreach}
		    </tbody>
		    
		    <tfoot>
		    
		    <tr class="MissionsListPagination pagination">
		
			<td colspan="9" class="last" >
				<ul>
				{if $nbrPages > 1 && $pageCourante != 1} <li><a href="{getlink page=$page  action='search' params="&paginate="|cat:($pageCourante-1)}" class="blue"><i class="icon-chevron-left"></i> {$globale_trad->precedant}</a></li> {/if}
				
				{section name=for start=1 loop=$nbrPages+1 step=1}
				    {if $smarty.section.for.index==$pageCourante}
					<li><span>{$smarty.section.for.index}</span></li>
				    {else}
					<li><a href="{getlink page=$page  action='search' params="&paginate="|cat:$smarty.section.for.index}">{$smarty.section.for.index}</a></li>
				    {/if}
				{/section}
				
				{if $nbrPages >1 && $pageCourante != $nbrPages} <li><a href="{getlink page=$page  action='search' params="&paginate="|cat:($pageCourante+1)}" class="blue">{$globale_trad->suivant} <i class="icon-chevron-right"></i></a></li> {/if}
				</ul>
			</td>

		    </tr>
		    
		</tfoot>
		
		</table>
		
		<div id="filter" class="row-fluid homeDroiteForm">
		    
		    <div class="span12">
			
			
			    <div class=" span12 margin-left-5">
								
				
				
				<div class="span4 formModificationSelectDiv">

				    <select name="id_mission_statut" class="span12 formModificationSelect">
					<option value="">Changer le statut</option>
					{foreach $getListStatuts as $statut}
					    <option value="{$statut->id_mission_statut}">{$statut->adm_statut}</option>
					{/foreach}
					
				    </select>
				    
				    <input type="hidden" name="mode" value="{$mode}">
				    <input type="hidden" name="paginate" value="{$pageCourante}">
				    
				    
				</div>
				
				<button class="margin-0 margin-left-10 pull-left btn btn-primary" type="submit" name="action" value="changeStatus"> 
				    <span class=" icon-ok icon-white "></span>
				    OK
				</button>
				<div class="clearfix"></div>
				
			    </div>
			    <div class="clearfix"></div>
			
			    
			
		    </div>
				    
		</div>
		
	    </form>

	{else}            
                <div class="bgWhite resultSearchNotFound">Aucune mission de diponible pour la recherche <b>{$post["mots_cle"]}</b></div>  
        {/if}
	
</div>
    
<div class="clearfix"></div>
