<div class="container row-fluid corps">
    {include file='web/tpl/template/menu.tpl'}

    
    <div class="span10 homeDroite">        
    
       
        <div class="span6" >
            
            <div class="monCompteDroite margin-top-20">
               
                
                <h1 class="margin-bottom-20">
                    {$trad->infos}
                </h1>

                <div class="span4">
                    <img src="{$avatar}" alt="photoMembre" class="img-polaroid span11 photoMonCompte">
                    
                </div>
                
                <div class="span8">
                    
                    
                    
                    
                    <h2 class=" compteTitleModificationh2">{$globale_trad->label_etat_civil}</h4>
                    
                    
                    <div class="span4 padding-right-10">
                       
                        <div class="span12">
                            {$globale_trad->label_civilite}
                        </div>
                        
                         <div class="span12">
                            <input  disabled="disabled" class="span12 formModification" type="text" value="{$getClient->getCivilite()->civilite}">
                        </div>
                        
                        <div class="clearfix"></div>
                        
                    </div>
                    
                    <div class="span4 padding-right-10">
                        
                        <div class="span12">
                            {$globale_trad->label_nom}
                        </div>
                        
                        <div class="span12">
                            <input  disabled="disabled" class="span12 formModification" type="text" value="{$getClient->getNom()}"placeholder="{$globale_trad->label_nom}">
                        </div>
                        
                        <div class="clearfix"></div>
                        
                    </div>
                    
                    <div class="span4">
                        
                        <div class="span12">
                            {$globale_trad->label_prenom}
                        </div>
                        
                        <div class="span12">
                            <input  disabled="disabled" class="span12 formModification" type="text" value="{$getClient->getPrenom()}"  placeholder="{$globale_trad->label_prenom}">
                        </div>
                        
                        <div class="clearfix"></div>
                        
                    </div>
                    
                    <div class="clearfix"></div>
                        
                    <h2 class=" compteTitleModificationh2 margin-top-20">{$globale_trad->label_adresse}</h4>

                    <div class="span12">{$globale_trad->label_rue}</div>
                    <input name="pr_add_fac_rue" disabled="disabled"  class="span12 formModification" type="text" value="{$getClient->getAdresseFacturation(false)->rue}" placeholder="{$globale_trad->label_rue}"/>
                            
                    <div class="span4 padding-right-10">
                        <div class="span12">{$globale_trad->label_cp}</div>
                        <input name="pr_add_fac_codepostal" disabled="disabled"  class="span12 formModification" type="text" value="{$getClient->getAdresseFacturation(false)->codepostal}" placeholder="{$globale_trad->label_cp}"/>
                        <div class="clearfix"></div>
                    </div>
                    
                    <div class="span4 padding-right-10">
                        <div class="span12">{$globale_trad->label_ville}</div>
                        <input name="pr_add_fac_ville" disabled="disabled"  class="span12 formModification" type="text" value="{$getClient->getAdresseFacturation(false)->ville}" placeholder="{$globale_trad->label_ville}"/>
                        <div class="clearfix"></div>
                    </div>
                            
                    <div class="span4">
                        <div class="span12">{$globale_trad->label_pays}</div>
                        <input name="pr_add_fac_pays" disabled="disabled"  class="span12 formModification" type="text" value="{$getClient->getAdresseFacturation(false)->pays}" placeholder="{$globale_trad->label_pays}"/>
                        <div class="clearfix"></div>
                    </div>
                            
                    <div class="clearfix"></div>
                    
                    <h2 class=" compteTitleModificationh2 margin-top-20">Contacts</h4>
                    
                    <div class="span12">Email</div>
                    <input disabled="disabled"  class="span12 formModification" type="text" value="{$getClient->getEmail()}" placeholder="{$globale_trad->label_rue}"/>
                    
                    <div class="span12">Standard</div>
                    <input disabled="disabled"  class="span12 formModification" type="text" value="{$getClient->getStandard(true)}" placeholder="{$globale_trad->label_rue}"/>
                    
                    <div class="clearfix"></div>
                    
                </div>
                
                <div class="clearfix"></div>


            </div>
        
        </div>
        
       
        <div class="span6 padding-left-20">
            {if $role >0}   
            <div class="monCompteDroite margin-top-20">
                
                <h1>Liste des managers</h1>
                
                <p>Le Lorem Ipsum est simplement du faux texte employ� dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l'imprimerie depuis les ann�es 1500, quand un peintre anonyme assembla ensemble des morceaux de texte pour r�aliser un livre sp�cimen de polices de texte. Il n'a pas fait que survivre cinq si�cles, mais s'est aussi adapt� � la bureautique informatique, sans que son contenu n'en soit modifi�. Il a �t� popularis� dans les ann�es 1960 gr�ce � la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus r�cemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker. </p>
                    
        
            </div>
                
            <div class="monCompteDroite margin-top-20">

                <table width="100%" cellpadding="4" class=" mission">
		    <thead class="MissionListHeader">
			<tr>
		     
			    <td class="">Nom</td>
	    
			    <td align="center" class="">Prenom</td>
    
			    <td align="center" class="">T�l�phone</td>
			    
			    <td class="" align="center" class="">Email</td>

				
			</tr>
		    </thead>
		
		    <tbody>
			{if isset($listManagers->count)}
			    {foreach $listManagers->managers as $manager}           
				<tr style="border-top:1px solid #D8D8D8">

				    
				    <td class="">{$manager->getNom()}</td>
		    
				    <td align="center" class="">{$manager->getPrenom()}</td>
	    
				    <td align="" class="">
					Fixe : {$manager->getFixe(true)}
					<br>
					Port : {$manager->getPortable(true)}
				    </td>
				    
				    <td align="center" class="">{$manager->getEmail()}</td>
				    
	
					
				</tr>
				   
			    {/foreach}
			{else}
			    <tr>
				<td colspan="4" class=" Aucune profil de diponible  "></td>
			    </tr>
				
			{/if}
		    </tbody>
		
		
		</table>
                    
            </div>
            {/if}
            
            <div class="monCompteDroite margin-top-20 text-center">
                {foreach $getListStatuts as $statut}
                    <div class="span3">
                        <div class="padding-10">
                        
                        <div class="square red">
                            <p class="text padding-10">Missions <br>{$statut->adm_statut}</p>
                            <p>
                                {$statistiques[$statut->id_mission_statut]}
                            </p>
                        </div>
                        </div>
                    </div>
		{/foreach}
                <div class="clearfix"></div>
            </div>
            
            <div class="monCompteDroite margin-top-20 text-center">
                {foreach $getListStatutsReponses as $statut}
                    <div class="span3">
                        <div class="padding-10">
                        
                        <div class="square red">
                            <p class="text padding-10">{$statut->adm_statut}</p>
                            <p>
                                {$statistiquesReponses[$statut->id_reponse_statut]}
                            </p>
                        </div>
                        </div>
                    </div>
		{/foreach}
                <div class="clearfix"></div>
            </div>
            
        </div>
        
        
        
        
    </div>
</div>

