<div class="container-fluid row-fluid corps">
    
    {include file='web/tpl/template/menu.tpl'}
    
    <div class="span10 homeDroite">
        
	<div class="search margin-bottom-20">
            
	    <form class="form-inline homeDroiteForm" method="POST" action="{getlink page="missions"  action='search'}" >
                
		<input type="text" name="search[mots_cle]" value="{$smarty.session.search['mots_cle']}" placeholder="{$globale_trad->searchProjet}" class="span12 homeDroiteSearch"/><br/>
                
		<div class="row-fluid" id="filter">
		    
		    <div class="span12">
			
			<a href="{getlink page='missions'}">
			    <img  container="true"  rel="tooltip" class=" pull-left border-right-1 padding-0" src="web/img/ico/ico_reload.png" alt="Réinitailiser le formulaire" title="Réinitailiser le formulaire" />
			</a>
			
			<span class="pull-left label-form">{$globale_trad->label_lieu}</span>
	
			<div class="border-right-1 pull-left ">
			
				<input  id="geocomplete" type="text" value="{$smarty.session.search['lo_region_short']}" class="span12 formModification"/>
				<input type="hidden" name="search[lo_departement_short]" id="lo_departement_short" value="{$smarty.session.search['lo_departement_short']}">
				<input type="hidden" name="search[lo_region_short]" id="lo_region_short" value="{$smarty.session.search['lo_region_short']}">
				<input type="hidden" name="search[lo_pays_short]" id="lo_pays_short" value="{$smarty.session.search['lo_pays_short']}">
			  
			    <div class="clearfix"></div>
			</div>
			
			<div class="border-right-1 padding-left-10 pull-left ">
			    <div class="span12 formModificationSelectDiv">
				<select class=" formModificationSelect" name="search[mi_anciennete]">
				    {foreach  from=$listPublications key=k item=v}
				    <option {if $k==$smarty.session.search.mi_anciennete}selected="selected"{/if} value="{$k}">{$v}</option>
				    {/foreach}
				</select>
			    </div>
			</div>
			
			<span class="pull-left label-form">{$globale_trad->label_date}</span>
			
			<div class="border-right-1 pull-left ">
			    
			    <div  data-date-format="{$datepicker.dateFormat}" data-date="{$smarty.request.search['mi_debut']}" class=" span6 input-append date">
				<input type="text" value="{if $smarty.request.search['mi_debut']}{$smarty.session.search['mi_debut']}{else}{$globale_trad->label_du}{/if}" size="16" name="search[mi_debut]" class="span12 ">
				<span class="add-on"><i class="icon-calendar"></i></span>
			    </div>
			    
			    <div  data-date-format="{$datepicker.dateFormat}" data-date="{$smarty.request.search['mi_fin']}" class="  span6 input-append date">
				<input type="text" value="{if $smarty.request.search['mi_fin']}{$smarty.session.search['mi_fin']}{else}{$globale_trad->label_au}{/if}" size="16" name="search[mi_fin]" class="span12">
				<span class="add-on"><i class="icon-calendar"></i></span>
			    </div>
			
			    <div class="clearfix"></div>
			    
			</div>
			
			<input  container="true"  rel="tooltip" class=" pull-left last border-right-1 padding-0" type="image" src="web/img/ico/ico_search_form.png" alt="Rechercher" title="Rechercher" />
			
			<div class="clearfix"></div>
			
		    </div>
		
		</div>
		
            </form>
	    
        </div>
	    
		
    <div class="span9 ">
	
	{if isset($error['file']) || isset($error['file_cv']) || isset($error['file_plaquette'])}
	<div class="alert alert-error">
	    {$error['file']}
	    {$error['file_cv']}
	    {$error['file_plaquette']}
	</div>
	{/if}
	{$messageCallBack}
        <div class="accordion" id="accordion2">
            <div class="accordion-group">
                <div class="accordion-heading">
                    <a class="accordion-toggle white" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwoo">
		      <div class="span5">
			<span class="pull-left margin-right-10  ico35 project"></span>
			<h1 lass="span10">Les derniers profils publiés</h1>
			<div class="clearfix"></div>
		      </div>
		      <div class="clearfix"></div>
		      <span class=" ico35 button"></span>
                    </a>
                </div>
                <div id="collapseTwoo" class="accordion-body in collapse" style="height: auto;">
                    <div class="accordion-inner">
			<table width="100%" cellpadding="5" class="mission">
				<tbody>
                        {if $listProfils['count']>0}
			    
				    {foreach $listProfils['profils'] as $profil}   
					{if $reponse@iteration > 3}{break}{/if} 
					{include file='web/tpl/template/profil.tpl'}
				    {/foreach}
				
			{else}            
				<tr align="center">
				    <td class="last MissionsListActions" colspan="5">
				    Aucun profil de diponible  
				    </td>
			    </tr>
			{/if}	
			</tbody>
			    </table>
                    </div>
                    <div class="accordionMore"><a href="{getlink page='cvteque'}"><button class="buttonCollapseMore"><b>></b> Voir tous les profils</button></a></div>
                </div>
            </div>
        </div>
       <div class="accordion" id="accordion3">
            <div class="accordion-group">
                <div class="accordion-heading">
                    <a class="accordion-toggle white" data-toggle="collapse" data-parent="#accordion3" href="#collapseTwo">
		      <div class="span5">
			<span class="pull-left margin-right-10  ico35 project"></span>
			<h1 lass="span10">Projets en attente de validation</h1>
			<div class="clearfix"></div>
		      </div>
		      <div class="clearfix"></div>
		      <span class=" ico35 button"></span>
                    </a>
                </div>
                <div id="collapseTwo" class="accordion-body in collapse" style="height: auto;">
                    <div class="accordion-inner">
			<table width="100%" class="mission">
			<tbody>
                        {if $listMissions['count'] >0}
			   
				{foreach $listMissions['missions'] as $mission}
				    {if $mission@iteration > 3}{break}{/if} 
				    {include file='web/tpl/template/mission.tpl'}
				{/foreach}
			    
			{else}
			    <tr align="center">
				    <td class="last MissionsListActions" colspan="5">
				    Aucun profil de diponible  
				    </td>
			    </tr>
			{/if}
			</tbody>
			</table>
                    </div>
                    <div class="accordionMore"><a href="{getlink page='missions' action="search" params='id_mission_statut=1'}"><button class="buttonCollapseMore"><b>></b>Afficher les projet en attente</button></a></div>
                </div>
            </div>
        </div>
       
       {$chart}
    </div>
    <!-- fin partie droite -->
    <div class="span3 pull-right">
	
	<div class="padding-left-20">
	
	    <!-- suivi proposition -->
	    <div class="DashboardDroitInfoProposition">
		<h1 class="margin-bottom-20">Tout les missions</h1>
		{foreach $getListStatutsReponse as $statut}
		<div class="DashboardDroitInfoPropositionP"><div class="PText span10"><a href="{getLink page='missions' action='search' params="id_mission_statut="|cat:$statut->id_mission_statut}">{$statut->adm_statut}</a></div><div class="PCounter span2">{$statistiques[$statut->id_mission_statut]}</div></div>
		{/foreach}
		<a href="{getLink  page="my_profils"}" class="span12 buttonProjetSaved">{$globale_trad->missions}</a>
		<div class="clearfix"></div>
	    </div>
	    <!-- Reponses -->
	     <div class="DashboardDroitInfoProposition">
		<h1 class="margin-bottom-20">Toutes les réponses</h1>
		{foreach $getListStatuts as $statut}
		<div class="DashboardDroitInfoPropositionP"><div class="PText span10">{$statut->adm_statut}</div><div class="PCounter span2">{$reponses.statistiques[$statut->id_reponse_statut]}</div></div>
		{/foreach}
		<div class="clearfix"></div>
	    </div>
	     
	      <div class="monCompteDroite margin-top-20 text-center">
		<div class='span6 padding-5'>
		    <div class="square red">
			<p class="text padding-10">Missions</p>
			<p>
			    {$countMissions}
			</p>
		    </div>
		</div>
		<div class='span6 padding-5'>
		    <div class="square red">
			<p class="text padding-10">Prestataires</p>
			<p>
			    {$countPrestataires.count}
			</p>
		    </div>
		</div>
		<div class='span6 padding-5'>
		    <div class="square red ">
			<p class="text padding-10">Clients</p>
			<p>
			    {$countClients.count}
			</p>
		    </div>
		</div>
		<div class='span6 padding-5'>
		    <div class="square red">
			<p class="text padding-10">CV</p>
			<p>
			    {$countProfils.count}
			</p>
		    </div>
		</div>
                <div class="clearfix"></div>
            </div>
            
	    
	</div>
    </div>
</div>

<div class="clearfix"></div>