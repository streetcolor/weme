<div class="container-fluid row-fluid corps">
    
    {include file='web/tpl/template/menu.tpl'}
    
    <div class="span10 homeDroite">
        
	<div class="search">
            
	    <form class="form-inline homeDroiteForm" method="POST" action="{getlink page='clients'  action='search'}" >
                
		<input type="text" name="search[mots_cle]" placeholder="RECHERCHER UN CLIENT..." class="span12 homeDroiteSearch"/><br/>
                
	    
		   
			
			<input class="submit" type="image" src="web/img/ico/ico_reload.png" alt="reload" title="reload" />
			
		    
    
		
            </form>
        </div>
	
	
	
	
	{*Si la requete getLitsMissions() renvoi des missions*}
	{if isset($count)}
	<form method="post" action="{getLink page="email" action="sendEmail" params="from=clients"}">
	    <h1 class="result">
		Résultat de votre recherche : <b>{$count} clients</b>
	    </h1>
	    {$messageCallBack}

	    <table width="100%" cellpadding="5" class=" mission">
		<thead class="MissionListHeader">
		    <tr>
			    
			    
			    <td class=""><input type="checkbox" class="checkall"></td>
			    
			    <td class="">Nom</td>
	    
			    <td align="center" class="">Prénom</td>
    
			    <td align="center" class="">Entreprise</td>
			    
			    <td align="center" class="">Contact</td>
			    
			    <td align="center" class="">Modifie le</td>
			    
			    <td align="center" class="last">Action</td>
			    
		    </tr>
		</thead>
	    
		<tbody>
		    {foreach $listClients as $client}        
			{include file='web/tpl/template/client.tpl'}
		    {/foreach}
		</tbody>
		
		<tfoot>
		    
		    <tr class="MissionsListPagination">

			<td colspan="7" class="last" >
			    {if $nbrPages > 1 && $pageCourante != 1} <a href="?page=clients&action=search&paginate={$pageCourante-1}">precedent</a> {/if}
			    
			    {section name=for start=1 loop=$nbrPages+1 step=1}
				{if $smarty.section.for.index==$pageCourante}
				    {$smarty.section.for.index}
				{else}
				    <a href="?page=clients&action=search&paginate={$smarty.section.for.index}">{$smarty.section.for.index}</a>
				{/if}
			    {/section}
			    
			    {if $nbrPages >1 && $pageCourante != $nbrPages} <a href="?page=clients&action=search&paginate={$pageCourante+1}">suivant</a> {/if}
			</td>

		    </tr>
		
		</tfoot>
	    
	    </table>
	    
	    <div id="filter" class="row-fluid homeDroiteForm">
		    
		    <div class="span12">
			<button class="margin-0 margin-left-10 pull-left btn btn-primary" type="submit" name="action" value="sendEmail"> 
			    <span class=" icon-ok icon-white "></span>
			    Envoyer un email aux entrées séléctionnées
			</button>
			
		    </div>
				    
	    </div>
	    
	    
	</form>

	{else}            
                <div class="bgWhite resultSearchNotFound">Aucun client de diponible pour la recherche <b>{$post["mots_cle"]}</b></div>  
        {/if}
	
</div>
    
<div class="clearfix"></div>
