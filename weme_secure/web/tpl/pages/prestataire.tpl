<div class="container row-fluid corps">
    {include file='web/tpl/template/menu.tpl'}

    
    <div class="span10 homeDroite">        
    
       
        <div class="span6" >
            
            <div class="monCompteDroite margin-top-20">
               
                
                <h1 class="margin-bottom-20">
                    {$trad->infos}
                </h1>

                <div class="span4">
                    <img src="{$avatar}" alt="{$avatar}"  title="{$avatar}" class="img-polaroid span11 photoMonCompte">
                    
                </div>
                
                <div class="span8">
                    
                    
                    
                    
                    <h2 class=" compteTitleModificationh2">{$globale_trad->label_etat_civil}</h4>
                    
                    
                    <div class="span4 padding-right-10">
                       
                        <div class="span12">
                            {$globale_trad->label_civilite}
                        </div>
                        
                         <div class="span12">
                            <input  disabled="disabled" class="span12 formModification" type="text" value="{$prestataire->getCivilite()->civilite}">
                        </div>
                        
                        <div class="clearfix"></div>
                        
                    </div>
                    
                    <div class="span4 padding-right-10">
                        
                        <div class="span12">
                            {$globale_trad->label_nom}
                        </div>
                        
                        <div class="span12">
                            <input  disabled="disabled" class="span12 formModification" type="text" value="{$prestataire->getNom()}"placeholder="{$globale_trad->label_nom}">
                        </div>
                        
                        <div class="clearfix"></div>
                        
                    </div>
                    
                    <div class="span4">
                        
                        <div class="span12">
                            {$globale_trad->label_prenom}
                        </div>
                        
                        <div class="span12">
                            <input  disabled="disabled" class="span12 formModification" type="text" value="{$prestataire->getPrenom()}"  placeholder="{$globale_trad->label_prenom}">
                        </div>
                        
                        <div class="clearfix"></div>
                        
                    </div>
                    
                    <div class="clearfix"></div>
                        
                    <h2 class=" compteTitleModificationh2 margin-top-20">{$globale_trad->label_adresse}</h4>

                    <div class="span12">{$globale_trad->label_rue}</div>
                    <input name="pr_add_fac_rue" disabled="disabled"  class="span12 formModification" type="text" value="{$prestataire->getAdresseFacturation(false)->rue}" placeholder="{$globale_trad->label_rue}"/>
                            
                    <div class="span4 padding-right-10">
                        <div class="span12">{$globale_trad->label_cp}</div>
                        <input name="pr_add_fac_codepostal" disabled="disabled"  class="span12 formModification" type="text" value="{$prestataire->getAdresseFacturation(false)->codepostal}" placeholder="{$globale_trad->label_cp}"/>
                        <div class="clearfix"></div>
                    </div>
                    
                    <div class="span4 padding-right-10">
                        <div class="span12">{$globale_trad->label_ville}</div>
                        <input name="pr_add_fac_ville" disabled="disabled"  class="span12 formModification" type="text" value="{$prestataire->getAdresseFacturation(false)->ville}" placeholder="{$globale_trad->label_ville}"/>
                        <div class="clearfix"></div>
                    </div>
                            
                    <div class="span4">
                        <div class="span12">{$globale_trad->label_pays}</div>
                        <input name="pr_add_fac_pays" disabled="disabled"  class="span12 formModification" type="text" value="{$prestataire->getAdresseFacturation(false)->pays}" placeholder="{$globale_trad->label_pays}"/>
                        <div class="clearfix"></div>
                    </div>
                            
                    <div class="clearfix"></div>
                    
                    <h2 class=" compteTitleModificationh2 margin-top-20">Contacts</h4>
                    
                    <div class="span12">Email</div>
                    <input disabled="disabled"  class="span12 formModification" type="text" value="{$prestataire->getEmail()}" placeholder="{$globale_trad->label_rue}"/>
                    
                    <div class="span12">Standard</div>
                    <input disabled="disabled"  class="span12 formModification" type="text" value="{$prestataire->getStandard(true)}" placeholder="{$globale_trad->label_rue}"/>
                    
                    <div class="clearfix"></div>

                </div>
                
                <div class="clearfix"></div>


            </div>


        </div>
        
        
        <div class="span6 padding-left-20">
               
            {if $agence ==true}
            
                <div class="monCompteDroite margin-top-20">
                        <h1 class= margin-bottom-20">{$trad->agenceDescription}</h1>
                     
                        <textarea class="margin-top-20 wysywyg formModificationTxtArea">{$prestataire->getPresentation()}</textarea>
                        
                        <div class="span5">
                            
                            <div class="padding-10">
                    
                                <p>
                                    <span class="span4"><img class="border-1" src="{$plaquette}" alt="{$plaquette}" title="{$plaquette}"></span>
                                </p>
            
                                
                            </div>
                            
                        </div>
            
                        <div class="clearfix"></div>
    
                </div>
            {/if}
        

            <div id="realisation" class="monCompteDroite margin-top-20">
                {if $countRealisations >0}
                    
                    {foreach $listRealisations as $realisation}
                        
                        <li class="span2">
                            
                            <div class="inner">
                               <a href="#rea{$realisation@iteration}" role="button" data-toggle="modal"><img src="{$realisation->getThumbnail(168, 100)}" alt="pj" title="pj"></a>
                   
                            </div>
                            
                            <div id="rea{$realisation@iteration}" class="modal hide fade " tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                   
                                <div class="modal-header">
                                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">x</button>
                                    <h3 id="myModalLabel">Editer une réalisation</h3>
                                </div>
                                
                                <div class="mediaWrapper">
                                    
                                    <div class="row-fluid">
                                        
                                        <div class="span imginfo">
                                            
                                            <div class="span12">
                                                
                                                <img class="imgpreview img-polaroid " alt="" src="{$realisation->getThumbnail(200, 135)}">
                                
                                                <p>
        
                                                        <strong>Type de fichier : </strong>{$realisation->getInformations()->type}<br>
                                                        <strong>Date upload : </strong>{$realisation->getInformations()->date}<br>
                                                        <strong>Taille : </strong>{$realisation->getInformations()->size} ko<br>
                                                        <strong>Resolution : </strong>{if $realisation->getInformations()->height}{$realisation->getInformations()->width}X{$realisation->getInformations()->height}{/if}<br>
                                                        
                                                </p>
                                                
                                            </div>
                                            
                                            <div class="clearfix"></div>
                                            
                                        </div><!--span3-->
                                        
                                        <div class=" span7 imgdetails pull-right">
                                            
                                           <div>
                                                <p>Description:</p>
                                                <p >{$realisation->getDescription()}</p>   
                                            </div>
                                            
                                        </div><!--span3-->
                                        
                                        <div class="clearfix"></div>
                                        
                                    </div>
                                    
                                </div>                                  
                                
                            </div>
                        </li>
                    {/foreach}
                    <div class="clearfix"></div>
                
                {else}
                
                    <div class="padding-10">
                        Vous n'avez actullement aucune réalisations
                    </div>
                
                {/if}
                
            </div>
            
            <div class="monCompteDroite margin-top-20 text-center">
                {foreach $getListStatutsReponses as $statut}
                    <div class="span3">
                        <div class="padding-10">
                        
                        <div class="square red">
                            <p class="text padding-10">{$statut->adm_statut}</p>
                            <p>
                                {$statistiquesReponses[$statut->id_reponse_statut]}
                            </p>
                        </div>
                        </div>
                    </div>
		{/foreach}
                <div class="clearfix"></div>
            </div>
            
        </div>
        
        
        <div class="clearfix"></div>
        
               
        {if $agence ==true}
        
            <div class="monCompteDroite margin-top-20">
                    
                    <h1>Liste des profils</h1>
                    
                    <p>Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l'imprimerie depuis les années 1500, quand un peintre anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n'a pas fait que survivre cinq siècles, mais s'est aussi adapté à la bureautique informatique, sans que son contenu n'en soit modifié. Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker. </p>
                        
            
                </div>
                    
                <div class="monCompteDroite margin-top-20">
    
                    <table width="100%" cellpadding="5" class=" mission">
                        <thead class="MissionListHeader">
                            <tr>
                         
                                <td class="">Nom</td>
                
                                <td align="center" class="">Prenom</td>
        
                                <td align="center" class="">Téléphone</td>
                                
                                <td align="center" class="">Disponibilité</td>
                                                        
                                <td align="center" class="last" width="75">Action</td>
                                    
                            </tr>
                        </thead>
                    
                        <tbody>
                            {if $listProfils->count > 0}
                                {foreach $listProfils->profils as $profil}           
                                    <tr style="border-top:1px solid #D8D8D8">
    
                                        
                                        <td class="">{$profil->getNom()}</td>
                        
                                        <td align="center" class="">{$profil->getPrenom()}</td>
                
                                        <td align="" class="">
                                            Fixe : {$profil->getFixe(true)}
                                            <br>
                                            Port : {$profil->getPortable(true)}
                                        </td>
                                        
                                        <td align="" class="">
                                             {$profil->getDisponibilite()}
                                        </td>
                                        <td align="center" class="last">
                                          
                                            
                                        </td>
                                            
                                    </tr>
                                       
                                {/foreach}
                            {else}
                                <tr>
                                    <td colspan="4" class=""></td> Aucune profil de diponible </td>
                                </tr>
                                    
                            {/if}
                        </tbody>
                    
                    
                    </table>
                        
                </div>
                
            </div>
            
        {/if}
        
    </div>
</div>

