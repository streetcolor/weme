<div class="container-fluid row-fluid corps">
    {include file='web/tpl/template/menu.tpl'}

    <div class="span10 row-fluid homeDroite">
    
	<div class="monCompteDroite  margin-top-20">
		
	    <h1>Vos utilisateurs</h1>
	   
	    <p>
		Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l'imprimerie depuis les années 1500, quand un peintre anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n'a pas fait que survivre cinq siècles, mais s'est aussi adapté à la bureautique informatique, sans que son contenu n'en soit modifié. Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker.
	    </p>
	    
	    <form method="post" action="{getlink page='email'  action='sendEmail' params="from="|cat:$from}" >
	    
		<h2 class="margin-top-20">Détail des utilisateurs selectionnés (les emails seront dedoublonnés à l'envoi)</h2>
	    
		<ul>
		    
		    {foreach $listProfils as $profil}
		    
			<li><b>{$profil->getNom()}  {$profil->getPrenom()}</b>
			<br>{$profil->getEmail()}
			{if $id_type == 2}
			<input type="hidden" name="id_prestataire_profil[]" value="{$profil->getId_profil()}">
			{else}
			<input type="hidden" name="id_client_manager[]" value="{$profil->getId_manager()}">
			{/if}
			</li>
		    
		    {/foreach}
		    
		</ul>
		
	    
		<h1>Vous pouvez séléctionner un message pre défini si vous le souhaitez</h1>
		
		{if isset($countMessages)}
		<h2 class="margin-top-20"></h2>
		    
		<div class="span6">
			<div class="formModificationSelectDiv span12">
			    <select class="formModificationSelect span12" name="id_client_message">
				{foreach $listMessages as $message}     
				    <option value=" {$message['id_client_message']}">{$message['mess_titre']}</option>
				{/foreach}
			    </select>
			</div>
		</div>
		    
	
		<input type="hidden" name="crypt" value="{$id_prestataire_profil}">
		<button class="margin-left-20 btn btn-primary  pull-left" type="submit" value="flag" name="chooseMessage">
		    <span class="icon-refresh icon-white"></span>
		    Choisir ce mail predefini
		</button>
		{/if}
		<div class="clearfix"></div>
		<h1 class="margin-top-20">Ou alors rédiger votre message</h1>

		
		{if isset($error['id_identifiant']) ||  isset($error['mail_author']) ||  isset($error['usr_email']) || isset($error['mail_titre']) || isset($error['mess_titre'])}
		<div class="span12 ">
		    <div class="alert alert-error span12">
			{*On envoi nos variable de retour error ou success callBack*}
			<p>{$error['id_identifiant']}</p>
			<p>{$error['mail_author']}</p>
			<p>{$error['usr_email']}</p>
			<p>{$error['mess_titre']}{$error['mail_titre']}</p>
		    </div>	
		</div>
		<div class="clearfix"></div>
		{/if}
		
		<div class="span6">
		    <div class="span12">Objet du message</div>
		    <input type="text" name="mail_titre"  class="formModification span12" value="{if $post['mail_titre'] || $post['mess_titre']}{$post['mail_titre']}{$post['mess_titre']}{/if}" />
		    <div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>

		
				
		<h2 class="margin-top-20">Soit vous utilisez directement un template HTML dejà existant</h2>
		
		
		{if isset($error['mail_message']) ||  isset($error['mess_message'])}
		<div class="span12 ">
		    <div class="alert alert-error span12">
			{*On envoi nos variable de retour error ou success callBack*}
			<p>{$error['mail_message']}</p>
			<p>{$error['mess_message']}</p>
		    </div>	
		</div>
		<div class="clearfix"></div>
		{/if}
		<div class="clearfix"></div>
		{if $listTemplates->count>0}
		    {foreach $listTemplates->templates as $template}
		       <label class="checkbox inline">
		      <input type="checkbox" value="{$template}" name="mail_template"> {$template} <a data-fancybox-type="iframe"  class='fancyHtml' href="web/mails/{$template}" target="_blank"><span class=" icon-eye-open margin-0 icon-black"></span></a>
		       </label>
		       <!--<img src="{thumbweb url="http://www.google.fr" }">-->
		    {/foreach}
		{/if}
		
		
		
		
		<h2 class="margin-top-20">Soit vous redigez votre propre message</h2>
		<div class="span12">
		    
		    <div class="clearfix"></div>
		    <textarea class="wysywyg" name="mail_message">
			{if $post['mail_message'] || $post['mess_message']}
			    {$post['mail_message']}{$post['mess_message']}
			{else}
			    Bonjour voici un message qui vous interree peut etre 
			<br>
			{/if}
		    </textarea>
		</div>
		
		<div class="clearfix"></div>
		
		<button class="btn btn-primary  margin-top-20 pull-right" type="submit" name="sendMessage" value="1">
		    <span class="icon-pencil icon-white"></span>
		    Envoyer le message
		</button>
		<div class="clearfix"></div>
		
		
		
	    </form>	
	    
	</div>
	
    </div>
    
<div class="clearfix"></div>
