<div class="container-fluid row-fluid corps">
    
    {include file='web/tpl/template/menu.tpl'}
    
    
    <div class="span10 homeDroite">
    <!-- Le filtre -->
      <div class="search">
        <form class="form-inline homeDroiteForm" method="POST" action="{getlink page='traduction'  action='search' params="crypt="|cat:$smarty.get.crypt}" >
                    
	    <input type="text" name="search[mots_trad]" placeholder="RECHERCHER UNE PAGE..." value="{$smarty.session.search.mots_trad}"class="span12 homeDroiteSearch"/><br/>		    
            <div class="row-fluid" id="filter">
                
                <div class="span12">
                {if !empty($smarty.get.crypt)}
                <a href="{getlink page='traduction'  action='editXML' params="crypt="|cat:$smarty.get.crypt}">
                    <img class=" pull-left border-right-1 padding-0" src="web/img/ico/ico_reload.png" alt="reload" title="reload" />
                </a>
		
		<div class="pull-left border-right-1">
		    
		    <label class="padding-left-20 padding-right-20 checkbox inline">
			<input type="checkbox" name="search[langue][]" {if in_array("en", $smarty.session.search.langue)}checked="checked"{/if}value="en">EN
		    </label>
		    <label class="padding-right-20 checkbox inline">
			<input type="checkbox" name="search[langue][]" {if in_array("fr", $smarty.session.search.langue)}checked="checked"{/if} value="fr">FR
		    </label>
		    <label class="padding-right-20 checkbox inline">
			<input type="checkbox" name="search[langue][]" {if in_array("es", $smarty.session.search.langue)}checked="checked"{/if} value="es">ES
		    </label>
		    
		</div>
		
                {else}
                
                <span class="pull-left label-form">Environnement</span>
                
                <div class="span2 border-right-1 pull-left ">
                    <div class="span12 formModificationSelectDiv">
                      <select class="span12 formModificationSelect" name="search[environnement]">
			<option value="">Choisissez</option>
                        <option value="0" {if $smarty.session.search.environnement == 0}selected="selected"{/if}>Visiteur</option>
                        <option value="1" {if $smarty.session.search.environnement == 1}selected="selected"{/if}>Prestataire</option>
                        <option value="2" {if $smarty.session.search.environnement == 2}selected="selected"{/if}>Client</option>
						<option value="3" {if $smarty.session.search.environnement == 2}selected="selected"{/if}>Variables globales</option>
                      </select>
                    <div class="clearfix"></div>
                    </div>
                </div>
		{/if}
                <input class=" pull-left border-right-1 padding-0" type="image" src="web/img/ico/ico_search_form.png" alt="reload" title="reload" />
                
                <div class="clearfix"></div>
                
                </div>
            
            </div>
            
        </form>
      </div>
	  
	  {$messageCallBack}
      <!-- La liste  -->
       
	{if isset($liste_page)}
		 <table width="100%" cellpadding="5" class=" mission">
			<thead class="MissionListHeader">
				<tr>
					<td class="" align="center">ID Page</td>
					
					<td align="center" class="">Titre </td>
			
					<td  align="center" class="">Nom</td>
		
					<td align="center" class="">Environnement</td>
					
					<td align="center" class="last">Actions</td>
					
				</tr>
			</thead>
			<tbody>
			{if ($liste_page.count>0)}
			    {foreach $liste_page.pages as $page}
			    <tr class="MissionsListBackground">
			      <td align="center">{$page.id_page}</td>
			      <td>{$page.titre}</td>
			      <td>{$page.nom}</td>
			      <td>{$environnements[$page.environnement]}</td>
			      <td align="center" class="span2 last">
				     
				    <div class="btn-group group-link ">
					<a class="btn btn-primary dropdown-toggle white" href="#" data-toggle="dropdown"><i class="icon-tasks icon-white"></i> Actions</a>
					<a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
					<ul class="dropdown-menu">
					    <li><a href="{getlink page="traduction" action='editXML' params="xml=sitetrade_"|cat:$page.nom|cat:"&env="|cat:{$environnements[$page.environnement]|cat:"&titre="|cat:{$page.titre}} crypt=true}"><i class="icon-pencil"></i> Modifier</a></li>
					    <li><a href="{getlink page="traduction" action='updBaseTrad' params="xml=sitetrade_"|cat:$page.nom|cat:"&env="|cat:{$environnements[$page.environnement]|cat:"&titre="|cat:{$page.titre}} crypt=true}" class=""><i class="icon-refresh"></i> mettre à jour la BDD</a></li>
						<li><a href="{getlink page="traduction" action='generateXML' params="pageId="|cat:$page.id_page crypt=true}" class=""><i class="icon-refresh"></i> regenerer le XML </a></li>
					</ul>
				    </div>
			      </td>
			    </tr>
			    <tr >
					<td style="height: 1px;" class="last MissionsListActions" colspan="5"></td>
				</tr>
			    {/foreach}
			    
			  {else}
			    
			    <tr class="MissionsListBackground">
			      <td class="last" align="center" colspan="5">
				Aucune page en cours actuellement
			      </td>
			    </tr>
			  
			  {/if}
			</tbody>
			
		    
			
				
	  
	  </table>
	{/if}
		
	<!-- LE formulaire d'edition -->
	{if isset($xml)}
		<h1>{$page_titre}</h1>
		<form method="POST" action="{getlink page='traduction'  action='trad'}" >
			<table width="100%" cellpadding="5" class=" mission">
			<thead class="MissionListHeader">
				<tr>
					<td align="center" class="">ID</td>
					
					<td class="span2">Trad ID </td>
					{if in_array("fr", $smarty.session.search.langue)}
					<td align="center" align="center" class="">FR</td>
					{/if}
					{if in_array("en", $smarty.session.search.langue)}
					<td align="center" class="">EN</td>
					{/if}
					{if in_array("es", $smarty.session.search.langue)}
					<td align="center" class="">ES</td>
					{/if}
					
				</tr>
			</thead>
			<tbody>
			{if count($nodeList)}
			    {foreach $nodeList as $id => $value}
				<tr class="MissionsListBackground">
				    <td align="center" class="">{$value['dbId']}</td>
				    <td align="center" class=""><input class="span12 formModification" value="{$id}" readonly="readonly"/></td>
				    {foreach $value as $lan=>$trad}
						{if $lan != "dbId"}
							<td class="last"><textarea class="span12 formModification margin-0" name="{'trad['|cat:$id|cat:']['|cat:$lan|cat:']'}">{$trad}</textarea></td>
						{/if}
				    {/foreach}	
				</tr>
				<tr >
					<td style="height: 1px;" class="last MissionsListActions" colspan="5"></td>
				</tr>
			    {/foreach}
			{else}
			    <tr class="MissionsListBackground">
			      <td class="last" align="center" colspan="5">
				Aucune traduction en cours actuellement
			      </td>
			    </tr>
			{/if}
			</tbody>
			
			<tfoot>
			
			    <tr class="MissionsListPagination pagination">
			
				<td colspan="9" class="last" >
					<ul>
					{if $nbrPages > 1 && $pageCourante != 1} <li><a href="{getlink page=$page  action='search' params="crypt="|cat:$smarty.get.crypt|cat:"&paginate="|cat:($pageCourante-1)}" class="blue"><i class="icon-chevron-left"></i> {$globale_trad->precedant}</a></li> {/if}
					
					{section name=for start=1 loop=$nbrPages+1 step=1}
					    {if $smarty.section.for.index==$pageCourante}
						<li><span>{$smarty.section.for.index}</span></li>
					    {else}
						<li><a href="{getlink page=$page  action='search' params="crypt="|cat:$smarty.get.crypt|cat:"&paginate="|cat:$smarty.section.for.index}">{$smarty.section.for.index}</a></li>
					    {/if}
					{/section}
					
					{if $nbrPages >1 && $pageCourante != $nbrPages} <li><a href="{getlink page=$page  action='search' params="crypt="|cat:$smarty.get.crypt|cat:"&paginate="|cat:($pageCourante+1)}" class="blue">{$globale_trad->suivant} <i class="icon-chevron-right"></i></a></li> {/if}
					</ul>
				</td>
	
			    </tr>
			    
			</tfoot>
			
			</table>
			<div class="padding-top-20 margin-bottom-20">
			    <button  class="pull-right btn btn-primary" type="submit">
				<span class=" icon-pencil icon-white"></span>
				sauvegarder les traduction de cette page
			    </button>
			    <div class="clearfix"></div>
			</div>
			<input type="hidden" name="xml_name" value="{$xml_name}" />
		</form>
	{/if}
    </div>
    <div class="clearfix"></div>
</div>