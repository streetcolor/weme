<div class="container-fluid row-fluid corps">
    
    {include file='web/tpl/template/menu.tpl'}
    
    <div class="span10 homeDroite">
        
	<div class="search">
                             
	    <div class="row-fluid" id="filter">
		
		<div class="form-inline homeDroiteForm">
		    
		    <div class="border-right-1  pull-left margin-left-5 ">
			
			<a class="scroller" href="{getLink page='responses' params="id_mission="|cat:$mission->getId_mission() crypt=true}">
			    <button type="button" class="btn btn-secondary pull-left margin-right-5 ">
				<span class="icon-refresh icon-white "></span>
				Révenir aux réponses
			    </button>
			</a>
			
		    </div>
		    {if $mission->getStatut()->id_statut<2}
		    <div class="border-right-1 pull-left padding-left-5 ">
			
			<a href="{getLink page='missions' action="changeStatus" params="id_mission_statut=2&id_mission="|cat:$mission->getId_mission() crypt="true"}">
			    <button class="btn btn-secondary pull-left margin-right-5 " type="button">
				<span class="icon-user icon-white "></span>
				Valider la mission
			    </button>
			</a>
	    
		    </div>
		    {/if}
		    <div class="clearfix"></div>
		    
		</div>
		
	    </div>
		
        </div>
	
	<div class="span12 row-fluid">
	    <div class="span10">
		<div class="monCompteDroite">
		    
		    <h1>Annonce : {$mission->getTitre()}</h1>
		    <div class="span9">
			<p>{$mission->getDescription()}</p>
		    </div>
		    
		    <div class="span3 padding-left-20">
			<h2>Localité :</h2>
			<p class="span12">
			    {$mission->getLocalite()}
			</p>
			<h2>Tarif :</h2>
			<p class="span12">
			    {$mission->getTarif(true)}
			</p>
			<h2>Date :</h2>
			<p class="span12">
			    du {$mission->getDebut()} Au {$mission->getFin()}
			</p>
			{if $missionPostes->count}
			<h2>Poste :</h2>
			<ul class="span12">
			   {foreach $missionPostes->postes as $poste}
			    <li>{$poste->adm_poste}</li>
			   {/foreach}
			</ul>
			{/if}
			
			{if  $missionCategorie->id_categorie}
			<h2>Secteur :</h2>
			<ul class="span12">
			    <li>{$missionCategorie->adm_categorie}</li>
			</ul>
			{/if}
			
			
			{if $missionCompetences->count}
			<h2>Competences :</h2>
			<ul class="span12">
			   {foreach $missionCompetences->competences as $competence}
			    <li>{$competence->adm_competence}</li>
			   {/foreach}
			</ul>
			{/if}
			
			{if $missionLangues->count}
			<h2>Langues :</h2>
			<ul class="span12">
			   {foreach $missionLangues->langues as $langue}
			    <li>{$langue->adm_langue}</li>
			   {/foreach}
			</ul>
			{/if}
			
			{if $missionEtudes->count}
			<h2>Langues :</h2>
			<ul class="span12">
			   {foreach $missionEtudes->etudes as $etude}
			    <li>{$etude->adm_etude}</li>
			   {/foreach}
			</ul>
			{/if}
		    </div>
		    <div class="clearfix"></div>
		    
		</div>
	    </div>
	    <div class="span2 text-center">
		{foreach $getListStatutsReponses as $statut}
                    <div class="span12">
                        
                        <div class="square red margin-bottom-5">
                            <p class="text padding-10">{$statut->adm_statut}</p>
                            <p>
                                <a href="{getLink page='responses' action="search" params="id_mission="|cat:$mission->getId_mission() crypt=true}&id_reponse_statut={$statut->id_reponse_statut}">
				    {$statistiquesReponses[$statut->id_reponse_statut]}
				</a>
                            </p>
                        </div>
                    </div>
		{/foreach}
	    </div>
	    <div class="clearfix"></div>
	    
	    
	</div>
    </div>
</div>


