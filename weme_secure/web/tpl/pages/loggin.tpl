<div id="identification" class="modal " tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
     
<form action="?page=home" method="POST">
	<div class="modal-header">
	  <button aria-hidden="true" data-dismiss="modal" class="close" type="button">x</button>
	  <h3 id="myModalLabel">connexion à votre compte</h3>
	</div>
	<div class="modal-body row-fluid">
	  <div class="span3 offset1">
	    Email de connexion
	  </div>
	  <div class="span6 offset1">
	    <input type="text" name="usr_email" placeholder="login" class="modalconnect span12">
	  </div>
	  <div class="clearfix"></div>
	  <div class="span3 offset1">
	    Mot de passe
	  </div>
	  <div class="span6 offset1">
	    <input type="password" name="usr_password" placeholder="password" class="modalconnect span12">
	    <input type="hidden" value="connexion" name="action">
	    <input type="hidden" value="home" name="page">
	  </div>
	  <div class="clearfix"></div>
	  <div class="span9 offset2">
	    <button type="submit" class="btn btn-primary span4  margin-top-20 pull-right"><span class="icon-ok icon-white"></span> Connection </button>
	  </div>
	  <div class="clearfix"></div>
	</div>
      </form>
      <div class="modal-footer">
	<a id="password" class="toggle passwordLost" href="#password">mot de passe oublié ?</a>
	<form class="none" action="?page=home" id="password_result" method="post">
	  <div class="row-fluid">
	    <p>
		  NULL
	    </p>
	    <div class="span2 offset2">
	      Email
	    </div>
	    <div class="span6">
	      <input type="text" name="usr_email" placeholder="e-mail" class="modalconnect span12">
	      <input type="hidden" name="action" value="password">
	    </div>
	    <div class="span8 offset2">
	      <button type="submit" class="btn btn-primary span4  margin-top-20 pull-right"><span class="icon-ok icon-white"></span>Envoyer </button>
	    </div>
	  </div>
	</form>
      </div>
    </div>

