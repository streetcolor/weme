<div class="container-fluid row-fluid corps">
    
    {include file='web/tpl/template/menu.tpl'}
    
    <div class="span10 homeDroite">
        
	<div class="search">
            
	    <form class="form-inline homeDroiteForm" method="POST" action="{getlink page=$page  action='search' params="id_mission="|cat:$id_mission crypt=true}" >
                                
		<div class="row-fluid" id="filter">
		    
		    <div class="span12">
			
			<a href="{getlink page=$page  action='' params="id_mission="|cat:$id_mission crypt=true}">
			    <img class=" pull-left border-right-1 padding-0" src="web/img/ico/ico_reload.png" alt="reload" title="reload" />
			</a>
			
			<span class="pull-left label-form">Publication</span>
				
			<div class="border-right-1 pull-left ">
			    <div class="span12 formModificationSelectDiv">
				<select class="formModificationSelect" name="search[id_reponse_statut]">
				    <option value="">Indiférrent</option>
				    {foreach $getListStatuts as $statut}
					<option {if $statut->id_reponse_statut==$smarty.session.search.id_reponse_statut}selected="selected"{/if}  value="{$statut->id_reponse_statut}">{$statut->adm_statut}</option>
				    {/foreach}
				</select>
			    </div>
			</div>
			
			<span class="pull-left label-form">Date</span>
			
			<div class="border-right-1 pull-left ">
			    
			    <div  data-date-format="dd/mm/yyyy" data-date="{$smarty.session.search['mi_debut']}" class=" span6 input-append date">
				<input type="text" value="{if $smarty.session.search['rp_fin']}{$smarty.session.search['rp_debut']}{else}{$globale_trad->label_du}{/if}" size="16" name="search[rp_debut]" class=" span12">
				<span class="add-on"><i class="icon-calendar"></i></span>
			    </div>
			    
			    <div  data-date-format="dd/mm/yyyy" data-date="{$smarty.request.search['rp_fin']}" class="  span6 input-append date">
				<input type="text" value="{if $smarty.session.search['rp_fin']}{$smarty.session.search['rp_fin']}{else}{$globale_trad->label_au}{/if}" size="16" name="search[rp_fin]" class=" span12">
				<span class="add-on"><i class="icon-calendar"></i></span>
			    </div>
		    
			    <div class="clearfix"></div>
			    
			</div>
			
			<input class=" pull-left last border-right-1 padding-0" type="image" src="web/img/ico/ico_search_form.png" alt="reload" title="reload" />
			
			<div class="clearfix"></div>
			
		    </div>
		
		</div>
		
            </form>
	    
        </div>
	

	{*Si la requete getLitsReponses() renvoi des response*}
	{if isset($count)}
	    <div class="form-inline " >
		
		<h1 class="result">
		    Résultat de votre recherche : <b>{$count} Propositions</b>
		</h1>
		
		{if isset($error['id_mission_reponse'])}
		    <div class="alert alert-error">
		    {$error['id_mission_reponse']}
		    </div>
		{/if}
		
		{$messageCallBack}
		
		<table width="100%" cellpadding="5" class=" mission">
		 
		    <thead class="MissionListHeader">
			
			<tr>
				
				<td class="first">Réponse</td>
		
				<td align="center" class="">Tarif</td>
				
				<td align="center" class="">Date</td>
				
				<td align="center" class="">Profil proposé</td>
				
				<td align="center" class="">Mission</td>
				
				<td align="center" class=" last">Action</td>
				
			</tr>
			
		    </thead>
		
		    <tbody>
			{foreach $listReponses as $reponse}
			    
			    {*Process qui permet de n'appeler qu'une seule fois les statuts reponses*}
			    {foreach $getListEntete as $entete}
				{if $entete ==  $reponse->getStatut()->id_statut}
				    
				    <tr class="MissionsListEntete">
					    
					    <td colspan="6" class="last ">{$reponse->getStatut()->statut}</td>
					    
				    </tr>
				    
				    {$getListEntete[$entete]=false}
				{/if}
			    {/foreach}
			    {*Fin du process*}
	    
			    {include file='web/tpl/template/reponse.tpl'}
    
			{/foreach}
		    </tbody>
		    
		    <tfoot>
			
			<tr class="MissionsListPagination">
    
			    <td colspan="6" class="last" >
				{if $nbrPages > 1 && $pageCourante != 1} <a href="{getlink page=$page  action='search' params="&id_mission="|cat:$id_mission crypt=true}&paginate={$pageCourante-1}">precedent</a> {/if}
				
				{section name=for start=1 loop=$nbrPages+1 step=1}
				    {if $smarty.section.for.index==$pageCourante}
					{$smarty.section.for.index}
				    {else}
					<a href="{getlink page=$page  action='search' params="&id_mission="|cat:$id_mission crypt=true}&paginate={$smarty.section.for.index}">{$smarty.section.for.index}</a>
					
				    {/if}
				{/section}
				
				{if $nbrPages >1 && $pageCourante != $nbrPages} <a href="{getlink page=$page  action='search' params="&id_mission="|cat:$id_mission crypt=true}&paginate={$pageCourante+1}">suivant</a> {/if}
			    </td>
    
			</tr>
    
		    </tfoot>
		
		</table>
		
		
	    </div>

	{else}            
                <div class="bgWhite resultSearchNotFound">Aucune proposition de diponible pour la recherche <b>{$post["mots_cle"]}</b></div>  
        {/if}


</div>
<div class="clearfix"></div>
