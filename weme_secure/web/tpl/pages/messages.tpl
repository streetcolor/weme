<div class="container row-fluid corps">
    {include file='web/tpl/template/menu.tpl'}

    
    <div class="span10 homeDroite margin-top-20">        
        <div class="span8">
            <form  method="POST" class="monCompteDroite"  enctype="multipart/form-data">
                <h1>Créez des messages types</h1>
                
    
                <div class="span12">
                    
                    <div class="span12">
                        Titre de votre message
                    </div>
                    
                    {if isset($error['id_client_message']) || isset($error['mess_titre'])}
                        <div class="span12">
                            <div class="alert alert-error span12 margin-bottom-20">
                               {*On envoi nos variable de retour error ou success callBack*}
                               <p>{$error['id_client_message']}</p>
                               <p>{$error['mess_titre']}</p>
                       
                            </div>
                        </div>
                    {/if}
                    
                    <div class="span12">
                        <input  class="span12 formModification"  name="mess_titre"  type="text" value="{if $post['mess_titre']}{$post['mess_titre']}{else}{if isset($id_client_message)}{$editMessage['mess_titre']}{/if}{/if}"  placeholder="Titre de votre message">
                    </div>
                    
                    <div class="clearfix"></div>
                    
                </div>
                
             
                <div class="span12">
                    
                    <div class="span12">
                        Message type
                    </div>
                    
                    {if isset($error['mess_message'])}
                        <div class="span12">
                            <div class="alert alert-error span12 margin-bottom-20">
                               {*On envoi nos variable de retour error ou success callBack*}
                               <p>{$error['mess_message']}</p>
                       
                            </div>
                        </div>
                    {/if}
                    
                    
                    <div class="span12">
                        <textarea  name="mess_message" class="wysywyg">{if $post['mess_message']}{$post['mess_message']}{else}{if isset($id_client_message)}{$editMessage['mess_message']}{/if}{/if}</textarea>
                    </div>
                    
                    <div class="clearfix"></div>
                    
                </div>
                
                <div class="clearfix"></div>
                
                {if isset($id_client_message)}
                    <a href="{getLink page="messages"}">
                        <button type="button" class="btn btn-primary margin-top-20 pull-left" >
                            <span class=" icon-repeat icon-white"></span>
                            Annuler la modification
                        </button>
                    </a>
                {/if}
                
                <button value="{if isset($id_client_message)}editMessage{else}addMessage{/if}" name="action" class="btn btn-primary margin-top-20 pull-right" type="submit">
                    <span class=" icon-pencil icon-white"></span>
                    Enregistrer le message
                </button>
                
                <div class="clearfix"></div>
    
            </form>
            
            <div class="clearfix"></div>
            
            <form  method="POST" class="margin-top-10 monCompteDroite" enctype="multipart/form-data">
                <h1>Ou importez votre propre template HTML</h1>
                
                <div class="span12">
                    
                    <div class="span12">
                        Titre de votre template
                    </div>
                    
                    {if isset($error['id_client_message']) || isset($error['mess_titre']) ||  isset($error['file'])}
                        <div class="span12">
                            <div class="alert alert-error span12 margin-bottom-20">
                               {*On envoi nos variable de retour error ou success callBack*}
                               <p>{$error['id_client_message']}</p>
                               <p>{$error['mess_titre']}</p>
                               <p>{$error['file']}</p>
                       
                            </div>
                        </div>
                    {/if}
                    
                    <div class="span12">
                        <input  class="span12 formModification"  name="mess_titre"  type="text" value="{if $post['mess_titre']}{$post['mess_titre']}{else}{if isset($id_client_message)}{$editMessage['mess_titre']}{/if}{/if}"  placeholder="Titre de votre message">
                    </div>
                    
                    <div class="span12 formModification">
                        <input  name="file"  type="file" >
                    </div>
                    
                    <div class="clearfix"></div>
                    
                </div>
                
                <div class="clearfix"></div>
                
                <button value="addTemplate" name="action" class="btn btn-primary margin-top-20 pull-right" type="submit">
                    <span class=" icon-pencil icon-white"></span>
                    Enregistrer le template
                </button>
                
                <div class="clearfix"></div>
    
            </form>
        </div>
        
	<div class="span4  pull-right padding-left-20">
	    
	    {$messageCallBack}
	    
	    <div class="monCompteDroite ">
	    
		<h1>Liste de vos mail type</h1>
		
		<p>Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l'imprimerie depuis les années 1500, quand un peintre anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n'a pas fait que survivre cinq siècles, mais s'est aussi adapté à la bureautique informatique, sans que son contenu n'en soit modifié. Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker. </p>
		
	    </div>
	    
	    <div class="monCompteDroite">
	    <table width="100%" cellpadding="5" class=" mission">
		    <thead class="MissionListHeader">
			<tr>

			    <td align="center" class="">Mail</td>
		    
			    <td align="center" class="last" width="75">Action</td>
				
			</tr>
		    </thead>
		
		    <tbody>
			{if $countMessages>0}
			    {foreach $listMessages as $message}
                           
				<tr style="border-top:1px solid #D8D8D8">

				    <td class="">{$message['mess_titre']}</td>
		    
                            
				    <td align="center" class="last">
				
                                        
                                        <div class="btn-group group-link ">
                                            <a class="btn btn-primary dropdown-toggle white" href="#" data-toggle="dropdown"><i class="icon-tasks icon-white"></i> Actions</a>
                                            <a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
                                            <ul class="dropdown-menu">
                                                    <li>
                                                            <a href="{getLink page="messages" action="editMessage" params="id_client_message="|cat:$message['id_client_message'] crypt=true}">
                                                                    
                                                                        <i class="icon-eye-open margin-0 icon-white "></i>
                                                                       Editer
                                                            
                                                            </a>
                                                    </li>
                                                    <li>
                                                            <a class="delEntry" href="{getLink page="messages" action="removeMessage"  params="id_client_message="|cat:$message['id_client_message'] crypt="true"}">
                                                                    
                                                                        <i class="icon-trash margin-0 icon-white "></i>
                                                                       Supprimer
                                                            
                                                            </a>
                                                    </li>
                                            </ul>
                                        </div>
				    </td>
					
				</tr>
				   
			    {/foreach}
			{else}
			    <tr>
				<td colspan="5" class="" >Aucune message de diponible</td>
			    </tr>
				
			{/if}
		    </tbody>
		
		
		</table>
	    
	    </div>
            <div class="monCompteDroite">
                
	    <table width="100%" cellpadding="5" class=" mission">
		    <thead class="MissionListHeader">
			<tr>

			    <td align="center" class="">Templates</td>
		    
			    <td align="center" class="last">Action</td>
				
			</tr>
		    </thead>
		
		    <tbody>
			{if $listTemplates->count>0}
			    {foreach $listTemplates->templates as $template}
                           
				<tr class="MissionsListBackground">

				    <td class="" title="{$template}">{$template|truncate:30}</td>
		    
		    
				    <td align="center" class="last">
                                        
                                        <div class="btn-group group-link ">
                                            <a class="btn btn-primary dropdown-toggle white" href="#" data-toggle="dropdown"><i class="icon-tasks icon-white"></i> Actions</a>
                                            <a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
                                            <ul class="dropdown-menu">
                                                    <li>
                                                            <a data-fancybox-type="iframe"  class='fancyHtml'  href="web/mails/{$template}" target="_blank">
                                                                    
                                                                        <i class="icon-eye-open margin-0 icon-white "></i>
                                                                       Voir
                                                            
                                                            </a>
                                                    </li>
                                                    <li>
                                                            <a class="delEntry" href="{getLink page="messages" action="removeTemplate"  params="rmvTemplate="|cat:$template}">
                                                                    
                                                                        <i class="icon-trash margin-0 icon-white "></i>
                                                                       Supprimer
                                                            
                                                            </a>
                                                    </li>
                                            </ul>
                                        </div>
                                        
					
				    </td>
					
				</tr>
				   
			    {/foreach}
			{else}
			    <tr class="MissionsListBackground">
				<td colspan="2" class="last" >Aucun template de diponible</td>
			    </tr>
				
			{/if}
		    </tbody>
		
		
		</table>
	    
	    </div>
	</div>
    </div>
</div>
<div class="clearfix"></div>