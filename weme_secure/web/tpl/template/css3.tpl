<link href='http://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,900,800,700,600' rel='stylesheet' type='text/css'>

<link href="web/css/global.css" rel="stylesheet">
<link href="web/css/bootstrap2.css" rel="stylesheet">
<link href="web/css/weme_prestataire.css" rel="stylesheet">
<link href="web/css/bootstrap-responsive2.css" rel="stylesheet">
<link href="web/css/bootstrap-datepicker.css" rel="stylesheet">
<link href="web/css/jquery-ui-1.9.0.custom_1.css" rel="stylesheet">
<link href="web/css/jquery.fancybox.css" rel="stylesheet">
<link href="web/css/redactor.css" rel="stylesheet">
