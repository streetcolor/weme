<div id="header" class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid row-fluid">

          <div class="span2"><a class="brand" href="#">Administration</a></div>
          <div class="span10">

	    <ul class="nav">
              
		  <li class="span4">
			<span class="round-count">{$smarty.session.compteur.all_missions}</span>
			<a href="{getLink page="missions"}">
			      <span class="ico projectIco"></span>
			      <span class="text">Projets</span>
			</a>
		  </li>
              
		  <li class="span4">
			<span class="round-count">{$smarty.session.compteur.new_missions}</span>
			<a class="active" href="{getLink page="missions"}">
			      <span class="ico projectIco"></span>
			      <span class="text">New Projets</span>
			</a>
		  </li>
		  
		  <li class="span4">
			<span class="round-count">{$smarty.session.compteur.all_cv}</span>
			<a class="active" href="{getLink page="cvteque"}">
			      <span class="ico propositionIco"></span>
			      <span class="text">CVs</span>
			</a>
		  </li>
	
       
	      <div class="clearfix"></div>
            </ul>
	    <div class="span4 offset1 row-fluid headerProfil">
    
	      <div class="span12 headerProfil2 padding-10">
		  <b>Bienvenue, vous êtes connecté en tant que</b>
		  <br>
		  <span class="headerProfilEmail">{$admin->getEmail()}</span>
	      </div>
	      <div class=" headerProfilDeco">
		<a href="{getlink page='home' action='loggout'}"><img src="web/img/headerProfilDeco.png" alt="deconnexion"/></a>
	      </div>
            </div>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>