<div class="span2 menuVertical">
        <div class="toggle"></div>       
        <ul class="span12 nav nav-tabs nav-stacked row-fluid">
                
            <li {if $smarty.get.page == 'home'}class="actif"{/if}>
                <a href="{getLink page="home"}">
                        <span class="margin-right-10 ico35 calendar pull-left"></span>
                        <span class="text">Tableau de bord</span>
                        <span class="clearfix"></span>
                </a>
            </li>

            <li {if $smarty.get.page == 'cvteque'}class="actif"{/if}>
                <a href="{getLink page="cvteque"}">
                        <span class="margin-right-10 ico35 paper pull-left"></span>
                        <span class="text">CVTeque</span>
                        <span class="clearfix"></span>
                </a>
            </li>
            
            <li {if $smarty.get.page == 'missions'}class="actif"{/if}>
                <a href="{getLink page="missions"}">
                        <span class="margin-right-10 ico35 eye pull-left"></span>
                        <span class="text">Mission</span>
                        <span class="clearfix"></span>
                </a>
            </li>
           
            <li {if $smarty.get.page == 'prestataires'}class="actif"{/if}>
                <a href="{getLink page="prestataires"}">
                        <span class="margin-right-10 ico35 buddy pull-left"></span>
                        <span class="text">Prestataires</span>
                        <span class="clearfix"></span>
                </a>
            </li>
           
            <li {if $smarty.get.page == 'clients' }class="actif"{/if}>
                <a href="{getLink page="clients"}">
                        <span class="margin-right-10 ico35 buddy pull-left"></span>
                        <span class="text">Clients</span>
                        <span class="clearfix"></span>
                </a>
            </li>
            
            <li {if $smarty.get.page == 'messages' }class="actif"{/if}>
                <a href="{getLink page="messages"}">
                        <span class="margin-right-10 ico35 mail pull-left"></span>
                        <span class="text">Mails types</span>
                        <span class="clearfix"></span>
                </a>
            </li>
            <li {if $smarty.get.page == 'traduction' }class="actif"{/if}>
                <a href="{getLink page="traduction"}">
                        <span class="margin-right-10 ico35 eye pull-left"></span>
                        <span class="text">Traductions</span>
                        <span class="clearfix"></span>
                </a>
            </li>
            <li>
            </li>
            
            
        </ul>
    </div>