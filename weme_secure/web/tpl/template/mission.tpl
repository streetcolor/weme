<tr class="MissionsListBackground">
	<td height="20" class=" last" colspan="9"><a class="linkCell" href="{getLink page='mission' action="mission" params="id_mission="|cat:$mission->getId_mission() crypt="true"}"></a></td>
</tr>

<tr class="MissionsListBackground">
	
	<td class="">
	    <input type="checkbox" value="{$mission->getId_mission()}" name="id_mission[]">
	</td>
	
	<td  align="center">
		<a class="linkCell" href="{getLink page='mission' action="mission" params="id_mission="|cat:$mission->getId_mission() crypt="true"}">
		<p>
		<span class="flag missionS{$mission->getStatut()->id_statut} "></span>
		</p>
		</a>
	</td>
	
	<td class="" align="">
		<a class="linkCell" href="{getLink page='mission' action="mission" params="id_mission="|cat:$mission->getId_mission() crypt="true"}">
		<p>
		<b>Contact : </b> {$mission->getNom()} {$mission->getPrenom()}
		<br>
		<b>Société : </b>  {$mission->getEntreprise()}
		<br>
		<b>Fixe : </b>  {$mission->getFixe(true)}
		<br>
		<b>Portable : </b>  {$mission->getPortable(true)}
		<br>
		<b>Email : </b>  {$mission->getEmail()}
		</p>
		</a>
	</td>

	<td>
		<a class="linkCell" href="{getLink page='mission' action="mission" params="id_mission="|cat:$mission->getId_mission() crypt="true"}">
		<p>
			{$mission->getTitre()}
			</p>
		</a>
	</td>
	
	<td class="" align="center">
		<a class="linkCell" href="{getLink page='mission' action="mission" params="id_mission="|cat:$mission->getId_mission() crypt="true"}">
		<p>
			{$mission->getTarif(true)}
			</p>
		</a>
	</td>

	<td class="" align="center">
		<a class="linkCell" href="{getLink page='mission' action="mission" params="id_mission="|cat:$mission->getId_mission() crypt="true"}">
		<p>
			Du {$mission->getDebut()}<br/>Au {$mission->getFin()}
			</p>
		</a>
	</td>
		
	<td align="center">
		<a class="linkCell" href="{getLink page='mission' action="mission" params="id_mission="|cat:$mission->getId_mission() crypt="true"}">
		<p>
			{$mission->getReponses(true)} réponse(s)
			</p>
		</a>
	</td>
	
	<td >
		<a class="linkCell" href="{getLink page='mission' action="mission" params="id_mission="|cat:$mission->getId_mission() crypt="true"}">
		<p>
		{$mission->getUpdate()}
		</p>
		</a>
	</td>
	
	<td  align="center" class="last">
		<div class="btn-group group-link ">
			<a class="btn btn-primary dropdown-toggle white" href="#" data-toggle="dropdown"><i class="icon-tasks icon-white"></i> Actions</a>
			<a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
			<ul class="dropdown-menu">
			    {if $mission->getReponses(true)>0}	
		<li><a href="{getLink page='responses' params="id_mission="|cat:$mission->getId_mission() crypt=true}">
			    <i class="icon-user icon-white "></i>
			    Voir les reponses
		</a></li>
		 {/if}
		<li><a href="{getLink page='mission' action="mission" params="id_mission="|cat:$mission->getId_mission() crypt="true"}">
			
			    <i class="icon-user icon-white "></i>
			    Accéder à la fiche de la mission
		</a></li>
		<li><a href="{getLink page='client' action="client" params="id_identifiant="|cat:$mission->getId_identifiant() crypt="true"}">
			    <i class="icon-user icon-white "></i>
			    Accéder à la fiche du client
		</a>
		{if $mission->getStatut()->id_statut<2}
		<li><a href="{getLink page='missions' action="changeStatus" params="id_mission_statut=2&id_mission="|cat:$mission->getId_mission() crypt="true"}">
			    <i class="icon-user icon-white "></i>
			    Valider la mission
		</a></li>
		{/if}
			</ul>
		    </div>
	</td>
</tr>
<tr class="MissionsListBackground">
	<td height="20" class=" last" colspan="9"><a class="linkCell" href="{getLink page='mission' action="mission" params="id_mission="|cat:$mission->getId_mission() crypt="true"}"></a></td>
</tr>
<tr>
	<td colspan="9" style="height:1px"class="last MissionsListActions"></td>
</tr>
