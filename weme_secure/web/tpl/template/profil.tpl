<tr class="MissionsListBackground">
	<td height="20" class=" last" colspan="9"></td>
</tr>

<tr class="MissionsListBackground">
	
	<td>
		<input type="checkbox" value="{$profil->getId_profil()}" name="id_prestataire_profil[]">
	</td>
	
	<td class=""> {$profil->getStructure()->structure}</td>
	
	<td class=""> {$profil->getNom()}</td>
	
	<td class=""> {$profil->getPrenom()}</td>
	
	<td class="">
		</b>Email :<b> {$profil->getEmail()}
		<br>
		</b>Fixe :<b> {$profil->getFixe(true)}
		<br>
		</b>Port :<b> {$profil->getPortable(true)}	
	</td>
	
	<td>
		</b>TJM :<b> {$profil->getTJM(true)}
		<br>
		</b>Disponibilité :<b> {$profil->getDisponibilite()}
	</td>
	
	<td class="" align="">
		
		<b>Identité</b> : {$profil->getPourcentageDetail()->identite}%
		<br>
		<b>Competences</b> :  {$profil->getPourcentageDetail()->competences}%
		<br>
		<b>Experiences</b> :  {$profil->getPourcentageDetail()->experiences}%
		<br>
		<b>Formations</b> :  {$profil->getPourcentageDetail()->formations}%

	</td>
	
	<td> {$profil->getUpdate()}</td>
	
	<td class="last" align="center">
		<div class="btn-group group-link ">
			<a class="btn btn-primary dropdown-toggle white" href="#" data-toggle="dropdown"><i class="icon-tasks icon-white"></i> Actions</a>
			<a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
			<ul class="dropdown-menu">
				{if !$profil->getActivePDF()}
					<li>
					<a href="web/scripts/cv.php?page=cv&action=cv&id_prestataire_profil={$profil->getId_profil()}">
						    <i class="icon-download icon-white "></i>
						    Télecharger PDF
					</a></li>
				{else}
					{assign var='cv' value=$profil->getCV()}
		
					<li><a href="{if !$cv->getId_file()}./web/scripts/cv.php?id_prestataire_profil={$profil->getId_profil()}{else}{$cv->getPath()}{/if}">
						    <i class="icon-download icon-white "></i>
						    Télecharger PDF
					</a></li>
				{/if}
				<li><a  class="showCv"href="{getLink page='cv' action="cv" params="id_prestataire_profil="|cat:$profil->getId_profil()}{if isset($smarty.request.search['mots_cle'])}&mots_cle={$smarty.request.search['mots_cle']}{/if}">
					    <i class=" icon-eye-open icon-white "></i>
					    Accéder à la fiche CV
				</a></li>
				
				<li><a href="{getLink page='prestataire' action="prestataire" params="id_identifiant="|cat:$profil->getId_identifiant() crypt="true"}">
					    <i class="icon-user icon-white "></i>
					    Accéder à la fiche du prestataire
				</a></li>
			</ul>
		    </div>
	</td>
</tr>
<tr class="MissionsListBackground">
	<td height="20" class=" last" colspan="9"></td>
</tr>
<tr>
	
	<td colspan="9"  style="height: 1px;"class="last MissionsListActions"></td>
</tr>

