<?php

$documentRoot = $_SERVER['DOCUMENT_ROOT'].'/weme_secure';
@ini_set('zlib.output_compression',0);
@ini_set('implicit_flush',1);
@ob_end_clean();
set_time_limit(0);
header( 'Content-type: text/html; charset=utf-8' );
ob_implicit_flush(1);

//Initialisation de l'environnement
include($documentRoot.'/config/config_init.php');