<?PHP

class DBManager{
	
	//Stoquage des requetes pour le debug
	public static $request   = null;
	
	public function __construct($PARAM_hote='localhost', $PARAM_port='3306', $PARAM_nom_bd='neo_V2', $PARAM_utilisateur='root', $PARAM_mot_passe='AbN38jyQ'){

		$connexion = new PDO('mysql:host='.$PARAM_hote.';port='.$PARAM_port.';dbname='.$PARAM_nom_bd, $PARAM_utilisateur, $PARAM_mot_passe, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
	    
		$this->db = $connexion;
		$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$connexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING); // On émet une alerte à chaque fois qu'une requête a échoué
		
	}
	
	public function getListIdentifiants(){
		
		$identifiants = array();
		
		$counter = array(
				'COUNT' => 'SELECT count(DISTINCT(id_identifiant)) AS total ' ,
				'SELECT'=> 'SELECT DISTINCT(id_identifiant) ,  id_type, usr_email'
				);
		
		$req = " FROM usr_identifiants  WHERE cree_le ORDER BY id_identifiant ASC ";
		
		
		if(is_array($limit)){

			$limit = array(
					'COUNT' => '' ,
					'SELECT'=> "LIMIT ".implode(",", $limit)
					);
		}

		//Tools::debugVar($counter['COUNT'].$req.$limit['COUNT']);
		if($debug)
			throw new myException($counter['COUNT'].$req.$limit['COUNT']);
		if(DEBUG)
			self::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$counter['COUNT'].$req.$limit['COUNT']);	
		//Le count avec la requete
		$resultats = $this->db->query($counter['COUNT'].$req.$limit['COUNT']);
		
		//si on un total superieur a zéro
		//on reexecute la requete afin de recuperer les elements
                if($count = $resultats->fetch(PDO::FETCH_OBJ)->total){
			
			if($debug)
				throw new myException($counter['SELECT'].$req.$limit['SELECT']);
			if(DEBUG)
				self::$request[] =  array('color'=>'blue', 'class'=>__CLASS__, 'function'=>__FUNCTION__, 'req'=>$counter['SELECT'].$req.$limit['SELECT']);
			
 			$resultats = $this->db->query($counter['SELECT'].$req.$limit['SELECT']);
			
			while ($identifiant = $resultats->fetch(PDO::FETCH_ASSOC))
				 $identifiants[] = $identifiant;	
			
			$resultats->closeCursor();
			
			
                }
		
		return array('count'=>$count, 'identifiants'=>$identifiants);
	}
	
	
	
	public function getClientFull($id_identifiant){
	    
	    if(is_numeric($id_identifiant)){
		
				
		$req = "/*PARTIE usr_client_managers*/
			SELECT UCM.id_identifiant, UCM.id_client, UCM.id_role, UCM.id_civilite, UCM.mg_nom, UCM.mg_prenom, UCM.modifie_le,
			SUBSTRING_INDEX(SUBSTRING_INDEX(UCM.mg_portable, '-', 1), '-', -1) AS mg_portable_indicatif,
			SUBSTRING_INDEX(SUBSTRING_INDEX(UCM.mg_portable, '-', 2), '-', -1) AS mg_portable,
			SUBSTRING_INDEX(SUBSTRING_INDEX(UCM.mg_fixe, '-', 1), '-', -1) AS mg_fixe_indicatif,
			SUBSTRING_INDEX(SUBSTRING_INDEX(UCM.mg_fixe, '-', 2), '-', -1) AS mg_fixe,
			/*PARTIE usr_identifiant*/
			UI.usr_email, UI.id_type, UI.usr_trad_localisation AS usr_trad, UI.usr_email, UI.usr_password,
			/*PARTIE usr_clients*/
			UC.cl_indicatif AS cl_standard_indicatif, UC.cl_telephone AS cl_standard, UC.cl_entreprise,  UC.cl_siret, UC.cl_effectif, UC.cl_description AS cl_presentation, UC.cl_entreprise, 
			UAC.cl_adresse AS cl_add_liv_rue, UAC.cl_codepostal  AS cl_add_liv_codepostal, UAC.cl_ville AS cl_add_liv_ville, UAC.cl_pays AS cl_add_liv_pays,
			UAC.cl_adresse AS cl_add_fac_rue, UAC.cl_codepostal  AS cl_add_fac_codepostal, UAC.cl_ville AS cl_add_fac_ville, UAC.cl_pays AS cl_add_fac_pays

			
			FROM usr_clients_managers AS UCM
			INNER JOIN usr_identifiants AS UI ON UI.id_identifiant =  UCM.id_identifiant
			INNER JOIN usr_clients AS UC ON UC.id_client = UCM.id_client
			LEFT OUTER JOIN usr_adresses_client AS UAC ON UAC.id_identifiant = UCM.id_identifiant
			WHERE UCM.id_role = 1 AND UCM.id_identifiant= ".$this->db->quote($id_identifiant, PDO::PARAM_STR);
		
		$resultats   = $this->db->query($req);
			
		$client       = $resultats->fetch(PDO::FETCH_ASSOC);
		
		$resultats->closeCursor();
		
		return $client;
		
	    }
	    
	}
	
	public function getListManagers($id_client, $debug=false){
		$managers = array();
		
		if(is_numeric($id_client)){
		
			$req .= "/*PARTIE usr_client_managers*/
				SELECT UCM.id_identifiant, UCM.id_client , UCM.id_role, UCM.id_civilite, UCM.mg_nom, UCM.mg_prenom, UCM.modifie_le,
				SUBSTRING_INDEX(SUBSTRING_INDEX(UCM.mg_portable, '-', 1), '-', -1) AS mg_portable_indicatif,
				SUBSTRING_INDEX(SUBSTRING_INDEX(UCM.mg_portable, '-', 2), '-', -1) AS mg_portable,
				SUBSTRING_INDEX(SUBSTRING_INDEX(UCM.mg_fixe, '-', 1), '-', -1) AS mg_fixe_indicatif,
				SUBSTRING_INDEX(SUBSTRING_INDEX(UCM.mg_fixe, '-', 2), '-', -1) AS mg_fixe,
				/*PARTIE usr_identifiant*/
				UI.usr_email, UI.id_type, UI.usr_trad_localisation AS usr_trad, UI.usr_email, UI.usr_password
				FROM usr_clients_managers AS UCM
				INNER JOIN usr_identifiants AS UI ON UI.id_identifiant =  UCM.id_identifiant
				WHERE UCM.id_role=2 AND UCM.id_client = ".$this->db->quote($id_client, PDO::PARAM_STR);
			
			
			$resultats = $this->db->query($req);
			
			while ($manager = $resultats->fetch(PDO::FETCH_ASSOC))
				 $managers[] = $manager;	
			
			$resultats->closeCursor(); 
			return $managers;
	
		}
		
	}
	

	public function getPrestataireFull($id_identifiant){
		
		if(is_numeric($id_identifiant)){	
			$req = "SELECT
				/*les donnee identifiants */
				i.id_identifiant, id_type, usr_email, usr_password, usr_trad_localisation as usr_trad, i.cree_le, i.modifie_le,
				/* les donnees prestataires */
				p.id_prestataire, pr_entreprise, pr_indicatif as pr_standard_indicatif, pr_telephone as pr_standard, id_structure, pr_presentation, pr_adresse as pr_add_liv_rue, pr_adresse as pr_add_fac_rue, pr_codepostal as pr_add_liv_codepostal, pr_codepostal as pr_add_fac_codepostal, pr_ville as pr_add_liv_ville,  pr_ville as pr_add_fac_ville, pr_pays as pr_add_liv_pays, pr_pays as pr_add_fac_pays,
				/* les donnees du profil */
				pf.pf_nom, pf.pf_prenom 
				FROM usr_identifiants as i
				INNER JOIN usr_prestataires as p ON  i.id_identifiant = p.id_identifiant
				 LEFT OUTER JOIN usr_adresses_prestataire as uap ON i.id_identifiant = uap.id_identifiant AND pr_type = 1
				  INNER JOIN usr_prestataires_profils as upp ON p.id_prestataire = upp.id_prestataire
				   INNER JOIN usr_profils as pf ON upp.id_profil = pf.id_profil 
				    WHERE i.id_identifiant = ".$this->db->quote($id_identifiant, PDO::PARAM_STR);
			
			$resultats   = $this->db->query($req);//Tools::debugVar($req);
				
			$prestataire       = $resultats->fetch(PDO::FETCH_ASSOC);
			
			$resultats->closeCursor();
			
			return $prestataire;
			
		    }
	}
	
	public function getListProfils($id_prestataire){
		
		$profils = array();
		
		$req = "SELECT
		/* l'id du prestataire  */
		id_prestataire,
		/* les donnees du profil principal */
		up.id_profil AS id_prestataire_profil, id_experience, id_civilite, pf_nom, pf_prenom,
		SUBSTRING_INDEX(SUBSTRING_INDEX(pf_fixe, '-', 1), '-', -1) AS pf_fixe_indicatif,
		SUBSTRING_INDEX(SUBSTRING_INDEX(pf_fixe, '-', 2), '-', -1) AS pf_fixe,
		SUBSTRING_INDEX(SUBSTRING_INDEX(pf_portable, '-', 1), '-', -1) AS pf_portable_indicatif,
		SUBSTRING_INDEX(SUBSTRING_INDEX(pf_portable, '-', 2), '-', -1) AS pf_portable,
		id_role, pf_naissance, pf_book as pf_siteweb, pf_permis, pf_disponibilite, pf_tjm, id_monnaie, pf_accept_mail as pr_notification_mail, pf_reference, pf_titre as pf_cv_titre, pf_cv_string as pf_cv_pdf_string, pf_actif as actif,
		/* les localités jointure avec usr_profil_localites */
		lo_departement,  lo_ville, lo_pays, lo_code_postal, lo_region
		
		 FROM usr_prestataires_profils as upp
		  INNER JOIN usr_profils as up ON upp.id_profil = up.id_profil
		   LEFT OUTER JOIN usr_profils_localites upl ON up.id_profil = upl.id_profil  
		   WHERE id_prestataire = ".$this->db->quote($id_prestataire, PDO::PARAM_STR);
		  
		 //Tools::debugVar($req);
		  $resultats = $this->db->query($req);
			
		while ($profil = $resultats->fetch(PDO::FETCH_ASSOC))
			 $profils[] = $profil;	
		
		$resultats->closeCursor(); 
		return $profils;
		
		
	}
	
	public function getListProfilComplements($id_profil){
		
		$competences = array();
		$langues = array();
		$postes = array();
		
		$req = "SELECT upc.id_niveau, ac.adm_competence	as complement
			 FROM usr_profils_competances  upc 
			  INNER JOIN adm_competences  ac ON upc.id_competance = ac.id_competence 	
			   WHERE upc.id_profil = ".$this->db->quote($id_profil, PDO::PARAM_STR);
		  
		 // Tools::debugVar($req);
		  $resultats = $this->db->query($req);
			
		while ($competence = $resultats->fetch(PDO::FETCH_ASSOC))
			 $competences[] = array('id_competence'=>$competence);	
		
		$resultats->closeCursor();
		
		$req = "SELECT upl.id_niveau, al.adm_langue as complement
			 FROM usr_profils_langues upl 
			  INNER JOIN adm_langues al ON upl.id_langue = al.id_langue 	
			  WHERE upl.id_profil = ".$this->db->quote($id_profil, PDO::PARAM_STR);
		  
		 // Tools::debugVar($req);
		  $resultats = $this->db->query($req);
			
		while ($langue = $resultats->fetch(PDO::FETCH_ASSOC))
			 $langues[] = array('id_langue'=>$langue);	
		
		$resultats->closeCursor();
		
		$req = "SELECT  ap.adm_poste as complement
			 FROM usr_profils_postes upp
			 INNER JOIN adm_postes ap ON upp.id_poste = ap.id_poste 	
			 WHERE upp.id_profil = ".$this->db->quote($id_profil, PDO::PARAM_STR);
		  
		 // Tools::debugVar($req);
		  $resultats = $this->db->query($req);
			
		while ($poste = $resultats->fetch(PDO::FETCH_ASSOC))
			 $postes[] = array('id_poste'=>$poste);	
		
		$resultats->closeCursor();
		
		return array_merge($postes, $langues, $competences);

	}
	
	public function getListProfilExperiences($id_profil){
		$experiences = array();
		
		$req = "SELECT exp_pro_debut as exp_debut, exp_pro_fin as exp_fin, exp_pro_poste as exp_intitule, exp_pro_societe as exp_entreprise, exp_pro_description as exp_description
			 FROM usr_profils_experiences_pro
			 WHERE id_profil = ".$this->db->quote($id_profil, PDO::PARAM_STR);
			 
		$resultats = $this->db->query($req);
		
		while ($experience = $resultats->fetch(PDO::FETCH_ASSOC)){
			$experiences[] = $experience;
		}
		
		$resultats->closeCursor();
		
		return $experiences;
	}
	
	public function getListProfilFormations($id_profil){
		$formations = array();
		
		$req = "SELECT pf_formation_debut as form_debut, pf_formation_fin as form_fin, pf_formation_diplome as form_diplome, pf_formation_ecole as form_ecole, pf_formation_description as form_description
			 FROM usr_profils_formations
			 WHERE id_profil = ".$this->db->quote($id_profil, PDO::PARAM_STR);
			 
		$resultats = $this->db->query($req);
		
		while ($formation = $resultats->fetch(PDO::FETCH_ASSOC)){
			$formations[] = $formation;
		}
		
		$resultats->closeCursor();
		
		return $formations;
	}
	
	public function getListProfilLocalite($id_profil){
		$localites = array();
		
		$req = "SELECT id_profil as id_prestataire_profil, lo_departement, lo_adresse as lo_quartier, lo_ville, lo_pays, lo_region, lo_pays_short, lo_code_postal, lo_region_short
			 FROM usr_profils_localites
			 WHERE id_profil = ".$this->db->quote($id_profil, PDO::PARAM_STR);
			 
		$resultats = $this->db->query($req);
		
		while ($localite = $resultats->fetch(PDO::FETCH_ASSOC)){
			$localites[] = $localite;
		}
		
		$resultats->closeCursor();
		
		return $localites;
	}
	
	public function getListMissions($id_identifiant){
		
		$missions = array();
		
		$req = "SELECT
		UCM.id_client, UM.id_mission, UM.id_identifiant, UM.id_experience, UMC.id_categorie, UM.id_mission_statut,UM.mi_reference, UM.mi_titre,
		UM.mi_description, UM.id_budget, UM.mi_tarif, UM.id_monnaie, UM.id_tarif AS id_contrat, 
		UM.mi_debut, UM.mi_fin, UM.mi_int_fin, UM.mi_string_fin, UM.mi_date_reponse,UM.mi_republication,  UM.mi_active_mail AS mi_reponse_mail,UM.cree_le, UM.modifie_le,
		/*Gestion des lacalité*/
		UML.lo_ville, UML.lo_pays, UML.lo_region, UML.lo_departement, UML.lo_pays_short, UML.lo_code_postal, UML.lo_region_short, UML.lo_departement_short
		
		FROM usr_missions AS UM 
		INNER JOIN usr_clients_managers AS UCM ON UCM.id_identifiant = UM.id_identifiant
		LEFT OUTER JOIN usr_missions_categories AS UMC ON UM.id_mission = UMC.id_mission
		LEFT OUTER JOIN usr_mission_localite as UML ON UM.id_mission = UML.id_mission
		WHERE UCM.id_identifiant = ".$this->db->quote($id_identifiant, PDO::PARAM_STR);
		  
		 // Tools::debugVar($req);
		  $resultats = $this->db->query($req);
			
		while ($mission = $resultats->fetch(PDO::FETCH_ASSOC))
			 $missions[] = $mission;	
		
		$resultats->closeCursor(); 
		return $missions;

	}
	public function getListMissionComplements($id_mission){
		$etudes = array();
		$competences = array();
		$langues = array();
		$postes = array();
		$req = "SELECT UME.id_mission, AE.adm_etude	
		FROM usr_missions_etudes AS UME 
		INNER JOIN adm_etudes AS AE ON UME.id_etude = AE.id_etude 	
		WHERE UME.id_mission = ".$this->db->quote($id_mission, PDO::PARAM_STR);
		  
		 // Tools::debugVar($req);
		  $resultats = $this->db->query($req);
			
		while ($etude = $resultats->fetch(PDO::FETCH_ASSOC))
			 $etudes[] = array('id_etude'=>$etude['adm_etude']);			
		
		$resultats->closeCursor();
		
		$req = "SELECT UMC.id_mission, AC.adm_competence	
		FROM usr_missions_competences AS UMC 
		INNER JOIN adm_competences AS AC ON UMC.id_competence = AC.id_competence 	
		WHERE UMC.id_mission = ".$this->db->quote($id_mission, PDO::PARAM_STR);
		  
		 // Tools::debugVar($req);
		  $resultats = $this->db->query($req);
			
		while ($competence = $resultats->fetch(PDO::FETCH_ASSOC))
			 $competences[] = array('id_competence'=>$competence['adm_competence']);		
		
		$resultats->closeCursor();
		
		$req = "SELECT UML.id_mission, AL.adm_langue
		FROM usr_missions_langues AS UML 
		INNER JOIN adm_langues AS AL ON UML.id_langue = AL.id_langue 	
		WHERE UML.id_mission = ".$this->db->quote($id_mission, PDO::PARAM_STR);
		  
		 // Tools::debugVar($req);
		  $resultats = $this->db->query($req);
			
		while ($langue = $resultats->fetch(PDO::FETCH_ASSOC))
			 $langues[] = array('id_langue'=>$langue['adm_langue']);	
		
		$resultats->closeCursor();
		
		$req = "SELECT UMP.id_mission, AP.adm_poste
		FROM usr_missions_postes AS UMP 
		INNER JOIN adm_postes AS AP ON UMP.id_poste = AP.id_poste 	
		WHERE UMP.id_mission = ".$this->db->quote($id_mission, PDO::PARAM_STR);
		  
		 // Tools::debugVar($req);
		  $resultats = $this->db->query($req);
			
		while ($poste = $resultats->fetch(PDO::FETCH_ASSOC))
			 $postes[] = array('id_poste'=>$poste['adm_poste']);	
		
		$resultats->closeCursor();
		
		return array('etudes'=>$etudes, 'postes'=>$postes, 'langues'=>$langues, 'competences'=>$competences);

	}
	
	public function getListMissionReponses($id_identifiant){
		$reponses = array();
		
		$req = "SELECT id_mission, /* id_identifiant du prestataire */  up.id_identifiant,  mr.id_profil as id_prestataire_profil, rp_reponse, rp_tarif, id_monnaie, id_tarif as id_contrat, id_file, id_pj, rp_comm_realisation as rp_commentaire, rp_get_rep_by_email as rp_reponse_mail, rp_statut as id_reponse_statut, rp_archiver
			 FROM usr_missions_reponses mr
			  INNER JOIN usr_prestataires_profils upp ON mr.id_profil = upp.id_profil
			   INNER JOIN usr_prestataires up ON upp.id_prestataire = up.id_prestataire
			   WHERE up.id_identifiant = ".$this->db->quote($id_identifiant, PDO::PARAM_STR);
			  
		$resultats = $this->db->query($req);
		
		while ($reponse = $resultats->fetch(PDO::FETCH_ASSOC)){
			//ON test si pour chaque identifiant, il y a dejà un profil de proposé. Si un profil est déjà proposé, on concataire sa valeur 'id_prestataire_profil' avec celui du noeud actuel
			if(array_key_exists($reponse['id_mission'], $reponses))
				$reponses[$reponse['id_mission']]['id_prestataire_profil'] = $reponses[$reponse['id_mission']]['id_prestataire_profil'].','.$reponse['id_prestataire_profil'];
			//Sinon on ajoute une nouvelle entrée au tableau réponses
			else
				$reponses[$reponse['id_mission']] = $reponse;	
		}
		
		$resultats->closeCursor();
		
		return $reponses;
	}
	
	public function getListFavorisMission($id_identifiant){
		$favoris = array();
		
		$req = "SELECT id_identifiant, id_mission
			 FROM usr_prestataires_sauvegardes ups
			  INNER JOIN usr_prestataires up ON up.id_prestataire = ups.id_prestataire
			   WHERE id_identifiant = ".$this->db->quote($id_identifiant, PDO::PARAM_STR);
		
		 $resultats = $this->db->query($req);
		 
		while ($favori = $resultats->fetch(PDO::FETCH_ASSOC))
			 $favoris[] = $favori;	
		
		$resultats->closeCursor();
		
		return $favoris;
	}
	
	public function getListFavorisProfils($id_identifiant){
	
		$favoris = array();
		
		$req = "SELECT UCF.id_profil AS id_prestataire_profil, UCF.id_identifiant
		FROM usr_clients_favoris AS UCF 
		WHERE UCF.id_identifiant = ".$this->db->quote($id_identifiant, PDO::PARAM_STR);
		  
		 // Tools::debugVar($req);
		  $resultats = $this->db->query($req);
			
		while ($favori = $resultats->fetch(PDO::FETCH_ASSOC))
			 $favoris[] = $favori;	
		
		$resultats->closeCursor(); 
		return $favoris;
	
	}

	public function getListMessages($id_identifiant){
		
		$messages = array();
		
		$req = "SELECT UCM.id_identifiant, UCM.ml_titre  AS mess_titre, UCM.ml_contenu  AS mess_message
		FROM usr_clients_mails AS UCM
		WHERE UCM.id_identifiant = ".$this->db->quote($id_identifiant, PDO::PARAM_STR);
		  
		 // Tools::debugVar($req);
		  $resultats = $this->db->query($req);
			
		while ($message = $resultats->fetch(PDO::FETCH_ASSOC))
			 $messages[] = $message;	
		
		$resultats->closeCursor(); 
		return $messages;
	
	}
	
	public function getListFiles(){
		
		$files = array();
		
		$req = "SELECT id_files as id_file, id_identifiant, id_profil as id_prestataire_profil, fichier_reference, fichier_nom, fichier_path, fichier_extension, fichier_type, cree_le, modifie_le
		FROM usr_files";
		  
		 // Tools::debugVar($req);
		  $resultats = $this->db->query($req);
			
		while ($file = $resultats->fetch(PDO::FETCH_ASSOC))
			 $files[] = $file;	
		
		$resultats->closeCursor(); 
		return $files;
	
	}
	
	public function getListAdmCompetences(){
		$comps = array();
		
		$req = "SELECT adm_competence, adm_trad FROM adm_competences";
		
		// Tools::debugVar($req);
		  $resultats = $this->db->query($req);
			
		while ($comp = $resultats->fetch(PDO::FETCH_ASSOC))
			 $comps[] = $comp;	
		
		$resultats->closeCursor(); 
		return $comps;
	}
	
	public function getListAdmLangue(){
		$langues = array();
		
		$req = "SELECT adm_langue, adm_trad FROM adm_langues";
		
		// Tools::debugVar($req);
		  $resultats = $this->db->query($req);
			
		while ($lan = $resultats->fetch(PDO::FETCH_ASSOC))
			 $langues[] = $lan;	
		
		$resultats->closeCursor(); 
		return $langues;
	}

}



$documentRoot = $_SERVER['DOCUMENT_ROOT'].'/weme_secure';
@ini_set('zlib.output_compression',0);
@ini_set('implicit_flush',1);
@ob_end_clean();
set_time_limit(0);
header( 'Content-type: text/html; charset=utf-8' );
ob_implicit_flush(1);

//Initialisation de l'environnement
include($documentRoot.'/config/config_init.php');


//Cette classe recupere les contenus des anciennes bases de données
$DBManager       = new DBManager();
$DBmembre 	 = new DBMembre();
$DBreponse	 = new DBReponse();
$DBmessage	 = new DBMessage();
$DBmission	 = new DBMission();
$DBFile		 = new DBFile();
$DBAdmin	 = new DBAdmin();

$getListIdentifiants =  $DBManager->getListIdentifiants();
//ETAPE 1 ON LISTE TOUS NOS IDENTIFIANTS
echo "<ul>";
$listChunk = array_chunk($getListIdentifiants["identifiants"],15, true);//500

//ON régénère les base adm_compétence et adm_langue
if(isset($_GET['generateGoAdm'])){
	if($getListAdmCompetence = $DBManager->getListAdmCompetences()){
		//Tools::debugVar($getListAdmCompetence, false);
		$array = array('table'=>'competence');
		foreach($getListAdmCompetence as $comp){
			$array['entry'] = $comp['adm_competence'];
			$lang_save = $_SESSION['langue'];
			$_SESSION['langue'] = $comp['adm_trad'];
			if($result = $DBAdmin->saveComplement($array)){
				echo "enregistrement adm compétence path ok : ".$comp['adm_competence']."<br/>";
			}else{
				echo "erreur lors de l'enregistrement de la competence : ".$comp['adm_competence']." - ".$result."<br/>";
			}
		}
		$_SESSION['langue'] = $lang_save;
	}
	
	if($getListAdmLangue = $DBManager->getListAdmLangue()){
		//Tools::debugVar($getListAdmCompetence, false);
		$array = array('table'=>'langue');
		foreach($getListAdmLangue as $lan){
			$array['entry'] = $lan['adm_langue'];
			$lang_save = $_SESSION['langue'];
			$_SESSION['langue'] = $lan['adm_trad'];
			if($result = $DBAdmin->saveComplement($array)){
				echo "enregistrement adm langue path ok : ".$lan['adm_langue']."<br/>";
			}else{
				echo "erreur lors de l'enregistrement de la langue : ".$lan['adm_langue']." - ".$result."<br/>";
			}
		}
		$_SESSION['langue'] = $lang_save;
	}
}

/*$getListFiles = $DBManager->getListFiles();Tools::debugVar($getListFiles[2], false);
$return = $DBFile->saveFileTransfert($getListFiles[2]);
echo $return ;*/

if(isset($_GET['generateGoFiles'])){
	//On récupère les path des fichiers
	if($getListFiles = $DBManager->getListFiles()){
		foreach($getListFiles as $file){
			if($result = $DBFile->saveFileTransfert($file)){
				echo "enregistrement fichier path ok : ".$file['id_file']."<br/>";
			}else{
				echo "erreur lors de l'enregistrement du path file : ".$file['id_file']."<br/>";
			}
		}
	}
}

foreach($getListIdentifiants["identifiants"] as $identifiant){//$listChunk[337] $getListIdentifiants["identifiants"]

    echo '<li>';
    
    echo '<p class="id_identifiant">['.$identifiant['id_identifiant'].'] '.$identifiant["usr_email"].'</p>';
    switch ($identifiant['id_type']){
	
	case 1 :
	    echo '<p class="id_type">Prestataire</p>';
	    if(isset($_GET['generateGo'])){
		$saveLang = $_SESSION['langue'];
		$_SESSION['langue'] = "es";
		if($prestataireFull = $DBManager->getPrestataireFull($identifiant['id_identifiant'])){
			$prestataireFull["id_type"] = DBMembre::TYPE_PRESTATAIRE;
			if($new_idents = $DBmembre->makeInscriptionTransfert($prestataireFull)){
				echo 'Enregistrement identifiant ok : identifiant'.$new_idents['id_identifiant']."- prestataire : ".$new_idents['id_prestataire']."<br/>";
				if($getListProfils = $DBManager->getListProfils($prestataireFull['id_prestataire'])){
					//Tools::debugVar($getListProfils);
					foreach ($getListProfils as $profil){
						//Make profil
						$profil['id_identifiant'] = $new_idents['id_identifiant'];
						$profil['id_prestataire'] = $new_idents['id_prestataire'];
						
						if($new_id_profil_prestataire = $DBmembre->saveProfilTransfert($profil)){
							echo "Enregistrement Profil ok : ".$new_id_profil_prestataire."<br/>";
							
							if($getListProfilComplement = $DBManager->getListProfilComplements($profil['id_prestataire_profil'])){
								foreach ($getListProfilComplement as $flag => $complement){
									$complement['id_prestataire_profil'] = $new_id_profil_prestataire;
									//sauvegarde des complément
									if($result = $DBmembre->saveCvComplementsTransfert($complement)){
										echo "enregistrement complement profil ok ! composante : ".$result."<br/>";
									}else{
										echo "Une erreure est survenu lors de saveCvComplementsTransfert ->".$result."<br/>";
										//Tools::debugVar($complement, false);
										
									}
								}
							}
							
							if($getListExpProfil = $DBManager->getListProfilExperiences($profil['id_prestataire_profil'])){
								$cpt=0;
								foreach ($getListExpProfil as $exp){
									$exp['id_prestataire_profil'] = $new_id_profil_prestataire;
									//On vérifie si la date de débbut n'est pas nul. Si oui, on régénère un champ composante pour ne pas avoir de duplicate entry
									if($exp['exp_debut'] == '0000-00-00'){
										$exp['composante']= $exp['id_prestataire_profil']."-".$cpt;
										$cpt++;
									}else{
										$exp['composante'] = $exp['id_prestataire_profil']."-".$exp['exp_debut'];
									}
									//save experience profil
									if($resultId = $DBmembre->saveCvExperienceTransfert($exp)){
										echo "enregistrement de l'experience pro ok : ".$resultId."<br/>";
									}else{
										echo "Une erreure est survenu lors de saveCvExperienceTransfert <br/> : ";
										Tools::debugVar($exp, false);
									}
								}
							}
							
							if($getListFormationProfil = $DBManager->getListProfilFormations($profil['id_prestataire_profil'])){
								$cpt = 0;
								
								foreach ($getListFormationProfil as $form){
									//save formation
									$form['id_prestataire_profil'] = $new_id_profil_prestataire;
									//On vérifie si la date de débbut n'est pas nul. Si oui, on régénère un champ composante pour ne pas avoir de duplicate entry
									if($form['form_debut'] == '0000-00-00'){
										$form['composante']= $form['id_prestataire_profil']."-".$cpt;
										$cpt++;
									}else{
										$form['composante'] = $form['id_prestataire_profil']."-".$form['form_debut'];
									}
									if($resultId = $DBmembre->saveCvFormationTransfert($form)){
										echo "enregistrement de la formation  ok : ".$resultId."<br/>"; 
									}else{
										echo "Une erreure est survenu lors de saveCvFormationTransfert <br/>";
										Tools::debugVar($form, false);
									}
								}
							}
							
							if($getListLocaliteProfil = $DBManager->getListProfilLocalite($profil['id_prestataire_profil'])){
								foreach ($getListLocaliteProfil as $loc){
									if($result = $DBmembre->saveCvLocalitesTransfert($loc)){
										echo "enregistrement de la localite ok : ".$result."<br/>";
									}else{
										echo "Une erreure est survenu lors de saveCvLocalites <br/>";
									}
								}
							}
						}else{
							echo "une erreure est survenu lors de saveProfilTransfert <br/>";
							//Tools::debugVar($profil);
						}
					}
				}
			}else{
				echo "une erreure est survenu lors ddu makeInscriptionTransfert";
			}
				
			if($getListMissionsReponse = $DBManager->getListMissionReponses($identifiant['id_identifiant'])){
				foreach ($getListMissionsReponse as $reponse){
					if($result = $DBreponse->saveReponseTransfert($reponse)){
						echo "enregistrement de la reponse ok : ".$result."<br/>";
					}else{
						echo "une erreure est survenu lors de saveReponseTransfert <br/>";	
					}
				}	
			}
			
			if($getListMissionsFavoris = $DBManager->getListFavorisMission($identifiant['id_identifiant'])){
				foreach ($getListMissionsFavoris as $favori){
					$favori['composante'] =$favori['id_mission']."-".$favori['id_identifiant'];
					if($result = $DBmembre->saveFavoriMissionTransfert($favori)){
						echo "enregistrement de la favori ok : composante -> ".$favori['composante']."<br/>";
					}else{
						echo "une erreure est survenu lors de saveFavoriMissionTransfert <br/>";		
					}
				}
			}
		}
		$_SESSION['langue'] = $saveLang;
	    }
	    
	break;

	case 2 :
		echo '<p class="id_type">Client</p>';
		if(isset($_GET['generateGoC'])){
			if($clientFull = $DBManager->getClientFull($identifiant['id_identifiant'])){
				
				//ON enregistre notre client
				$clientFull["id_type"]   = DBMembre::TYPE_CLIENT;
				$DBmembre->makeInscriptionTransfert($clientFull);
				
				$getListMissionsAdmin = $DBManager->getListMissions($identifiant['id_identifiant']);
				foreach($getListMissionsAdmin as $mission){
					
					//On enregistre les mission du compte principal
					$DBmission->makeMissionTransfert($mission);
				
				}		 
		
				$getListFavorisAdmin = $DBManager->getListFavorisProfils($identifiant['id_identifiant']);
				foreach($getListFavorisAdmin as $favoris){
					//On enregistre les favoris du compte principal
					$DBmembre->makeFavori($favoris);
	
				}
				 
				$getListMessagesAdmin = $DBManager->getListMessages($identifiant['id_identifiant']);
				foreach($getListMessagesAdmin as $message){
					//On enregistre les mail du compte principal
					$DBmessage->makeMessage($message);
	
				}
		
	
				//on recupere les managers
				$getListManagers = $DBManager->getListManagers($clientFull['id_client']);
				foreach($getListManagers as $manager){
	
					$manager["id_type"] = DBMembre::TYPE_CLIENT;	
					$getListFavorisManager = $DBManager->getListFavorisProfils($manager['id_identifiant']);
					foreach($getListFavorisManager as $favoris){
						//On enregistre les favoris du compte manager
						$DBmembre->makeFavori($favoris);
					
					}
	
					$getListMessagesManager = $DBManager->getListMessages($manager['id_identifiant']);
					foreach($getListMessagesManager as $message){
						//On enregistre les mail du compte manager
						$DBmessage->makeMessage($message);
					
					}
								
					$getListMissionsManagers = $DBManager->getListMissions($manager['id_identifiant']);
					foreach($getListMissionsManagers as $mission){
						//On enregistre les mission du compte manager
						$DBmission->makeMissionTransfert($mission);
						
					}
					
				}
							
			}
		    
		}
	
		break;
	
    }
    
    echo '</li>';
    
    
}
echo "</ul>";
