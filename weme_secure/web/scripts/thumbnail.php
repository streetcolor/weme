<?php
$documentRoot = $_SERVER['HTTP_HOST']=="5.39.77.136" ?  '/home/ejobber/www/weme_secure/' : $_SERVER['DOCUMENT_ROOT'];

require_once $documentRoot.'/core/Tools.class.php';
require_once $documentRoot.'/core/myException.class.php';


$file_name_source   = $_GET['path'];

$height = $_GET['height'] ? $_GET['height'] : 110;
$width  = $_GET['width'] ? $_GET['width'] : 150;

Tools::generateThumbnail($file_name_source, $width, $height);


