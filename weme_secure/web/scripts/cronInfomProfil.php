<?PHP
$documentRoot = $_SERVER['DOCUMENT_ROOT'];

@ini_set('zlib.output_compression',0);
@ini_set('implicit_flush',1);
@ob_end_clean();
set_time_limit(0);
header( 'Content-type: text/html; charset=utf-8' );
ob_implicit_flush(1);

session_start();
//Initialisation de l'environnement
include($documentRoot.'/config/config_init.php');
//Le routing permet de selectionner le controleur en fonction de la page sur la quelle on se trouve
//Le controleur determine ensuite l'action à accomplir sur le page en fonction du $_REQUEST["action"]
include(_PATH_BACK_.'/config/routing.php');

$BaseFichier    = _PATH_BACK_.'/logs/alert-missions';
$DBmission      = new DBMission();
$DBmembre       = new DBMembre();
$getListPresta  = $DBmembre->getListPrestataires(array(), array(), false);
$emails         = array();


//On recupere les infos contenu dans le fichier sur le serveur (T-24H).
$file           = file_get_contents("$BaseFichier.txt");
$var_count_tmp  =  json_decode($file)->var_count;
$var_day_tmp    =  json_decode($file)->var_day;

//On recupere maintenant les infos recuperées par le cron T+0H
$var_count      = $DBmission->getListMissions(array(), array(), false);
$var_count      = $var_count['count'];
$var_day        = date('Y-m-d H:m:s');

//On compare en fonction des differents Use cases
//Difference en T-24H et T+0H
$diffTotal      = $var_count-$var_count_tmp;

//Difference
$d1             = new DateTime($var_day);
$d2             = new DateTime($var_day_tmp);
$difDay         = $d1->diff($d2)->format('%d');

//configurtaion de notre email
$messageSend['mail_author']    = 'alberto.martinez@neofreelo.com';
if($messageSend['message'] = @file_get_contents(_PATH_BACK_.'web/mails/Descubri_los_nuevos_proyectos_freelances_publicados.html')){
    
}
else{
    die('Attention le ficher :<br><br>'._PATH_BACK_.'web/mails/Descubri_los_nuevos_proyectos_freelances_publicados.html <br><br>est inexistant sur le serveur le cron ne peux pas se lancer!');
}
$messageSend['sujet']          = '[TEST BO] NeoFreelo - Descubrí los nuevos proyectos freelances publicados';	

echo '<h1>Veille</h1>';
echo $var_count_tmp;
echo '<br>';
echo $var_day_tmp;

echo '<hr><h1>Maintenant</h1>';
echo $var_count;
echo '<br>';
echo $var_day;
echo "<br>";
echo "Nouvelles missions : $diffTotal";
echo "<br>";
echo "Nouveau compteur : $difDay";
echo "<hr>";


if($diffTotal>=5){
    //Plus de 5 missions ont été postée depuis la derniere execution du cron
    //on envoi un mail 
    //les tests s'arrete là
    //{...}
    echo "<p>Envoie mail demandé car le compteur affiche 5 nouvelle mission soit '.$var_count.' dans la base</p>";
    
    foreach($getListPresta['prestataires'] as $email){
	$emails[] = $email->getEmail();
    }
    
    //On met nos valeurs à jour
    $arraytToFile = array('var_count'=>$var_count, 'var_day'=>$var_day);
}
elseif(($difDay>= 2) && ($diffTotal>0)){
    //Plus de 48 heures se sont écolées depuis la derniere execution du cron et au moins une mission à été postée
    //on envoi un mail 
    //les tests s'arrete là
    //{...}
    echo "<p>Envoie mail demandé car <b>'.$diffTotal.'</b> projet(s) en plus depuis '.$difDay.' jours</p>";
    foreach($getListPresta['prestataires'] as $email){
	$emails[] = $email->getEmail();
    }
    //On met nos valeurs à jour
    $arraytToFile = array('var_count'=>$var_count, 'var_day'=>$var_day);
}
else{
    //Rien de neuf
    //Pas de mail pour cette fois ci
    //On met nos valeurs à jour sauf la date que l'on va reporter
    echo "<p style=\"color:red\">Aucune nouvelle mission depuis le dernier cron ni depuis ces 48 dernieres heures</p>";
    $arraytToFile = array('var_count'=>$var_count, 'var_day'=>$var_day_tmp);
}


$emails[] = 'sebastien@keyrium.com';

$i        = 0;
$compteur = 0;
foreach($emails as $messageSend['email']){
	
	if(Tools::sendEmail($messageSend))
		echo '<b'.$i.'</b> '.$messageSend['email']. '<br />';
	$compteur ++;
	$i ++;
	if($compteur==2){
		$compteur = 0;
		echo '<hr>';
		flush();
		ob_flush();
		sleep(1);
	}
}
echo '<p>End ... le mail à été envoyé à '.$i.' personnes</p>';
	


//On supprime le fichier de reference
if(unlink("$BaseFichier.txt")){
   echo "<p>Suppression du fichier ok</p>";
}else{
    echo "<p>impossible de supprimer le fichier</p>";
}


//on edite un nouveau Fichier de reference
if($fp = fopen("$BaseFichier.txt","a+")){ // ouverture du fichier en écriture
    echo '<p>Création du nouveau fichier de référence.</p>';
    fputs($fp, json_encode($arraytToFile)); // on écrit le nom et email dans le fichier
    fclose($fp);
    echo '<p>La ligne '.json_encode($arraytToFile).' vient d\'être ajoutée dans le fichier de référence.</p>';
}

//On update notre Fichier de log
$fp = fopen("$BaseFichier-log.txt","a"); // ouverture du fichier en écriture
fputs($fp, json_encode($arraytToFile)); //on log nos statistiques
fputs($fp, "\n"); // on va a la ligne
fclose($fp);