<?PHP
//Initialisation de l'environnement

$documentRoot = $_SERVER['HTTP_HOST']=="5.39.77.136" ?  '/home/ejobber/www/weme_secure/' : $_SERVER['DOCUMENT_ROOT'];

include($documentRoot.'/config/config_init.php');
//Le routing permet de selectionner le controleur en fonction de la page sur la quelle on se trouve
//Le controleur determine ensuite l'action à accomplir sur le page en fonction du $_REQUEST["action"]
include($documentRoot.'/config/routing.php');

$_REQUEST['search']['mots_cle'] = $_REQUEST['mots_cle'];

ob_start('Tools::replaceFlush');

$smarty->display(_TPL_.'pages/cv.tpl');


ob_end_flush();
