<?php
//Controleur pour la gestion de la CVteque
//pour la gestion des missions
class cvtequeController extends controller {


   
   function defaultAction($search = array(), $limitForPagine = true){
	 unset($_SESSION['search']);
            $smarty  = $this->smarty;
            $request =$this->request;
            $DBmembre= new DBMembre();
            $DBadmin = new DBAdmin();
    
            $filtreDefault = array(); //Le filtre de recheche de base obligtatoire

            $_SESSION['search'] = $search;//si on passe par la methode searchAction on passe la parametre dans la session
            
            // Numero de page (1 par d�faut)
            $paginate['current_page'] = (isset($_GET['paginate']) && is_numeric($_GET['paginate'])) ? $_GET['paginate'] : 1;
            // Num�ro du 1er enregistrement � lire
            $paginate['limit']        = ($paginate['current_page'] - 1) * NOMBRE_PER_PAGE;    
            //On affiche une limite ou pas
            $limit = $limitForPagine ? array($paginate['limit'], NOMBRE_PER_PAGE) : false;

            //on recupere la liste de nos CV pour le comptage
            $listCv   = $DBmembre->getListProfils($filtreDefault, $search, $limit, false, true);
            
            // Pagination
            $paginate['total_pages']  = ceil($listCv['count'] / NOMBRE_PER_PAGE);
            
            //Si on du monde en sortie on les recupere
            if($listCv['count']){
                
                //Puis on les envoie dans la vue
                $smarty->assign('nbrPages', $paginate['total_pages']);
                $smarty->assign('pageCourante', $paginate['current_page']);
                $smarty->assign('listCv', $listCv['profils']);
                $smarty->assign('count', $listCv['count']);
                $smarty->assign('mode', 'normal');//permet d'ajouter un paramtrer dans lien favoris des profil
                
                

            }
	 //Pour le moteur de recherche    
	 $DBadmin = new DBAdmin();
	 $smarty->assign('listExperiences', $DBadmin->getListExperiences());
 
	  //Pour le moteur de recherche    
	 $smarty->assign('listStructures', $DBadmin->getListStructures());
	 
	 //Pour le moteur de recherche
	 $publications = array(
	    "1 DAY"    => "Aujourd'hui",
	    "7 DAY"    => "Il y a moins d'une semaine",
	    "1 MONTH"  => "Il y a moins d'un mois",
	    "6 MONTH"  => "Il y a moins de 6 mois"
	 );
	 $smarty->assign('listPublications', $publications);
            
    }
    
        
    function cvAction(){
        

                $DBmembre = new DBMembre();
               
                $filtreDefault = array('pf_profil_statut'=>1, 'id_prestataire_profil'=>array($this->request['id_prestataire_profil'])); //Le filtre de recheche de base obligtatoire
                $cv   = $DBmembre->getListProfils($filtreDefault,  $searchFiltre=array(), $limit=false, $matching=false, true);

                if($cv['count']){
                     $cv = $cv['profils'][0];
                     $this->smarty->assign('cv', $cv);//permet d'ajouter un paramtrer dans lien favoris des profil
                     $this->smarty->assign('cvComp', $cv->getCompetences());
                     $this->smarty->assign('cvExp', $cv->getExperiences());
                     $this->smarty->assign('cvForm',$cv->getFormations());
                     $this->smarty->assign('cvLang',$cv->getLangues());
                     $this->smarty->assign('cvPostes',$cv->getPostes());
                }
    }          
    function searchAction(){
        
        if($this->request['search'])
            $_SESSION['search'] = $this->request['search'];
        
        $this->defaultAction($_SESSION['search'] );
        $this->smarty->assign('mode', 'search');//permet d'ajouter un paramtrer dans lien des profil
        $this->smarty->assign('post', $_SESSION['search']);//tr�s pratique puisque il permet de ressortir le post dans la vue


    }
     
   
}
?>