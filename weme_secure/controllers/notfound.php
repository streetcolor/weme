<?php
class notfoundController extends controller {
    
    
    function defaultAction(){

    }
    
    function unfoundAction($message){
        $this->defaultAction();
        //Si on souhaite envoyer notre propre template
        global $setTemplate;
        $setTemplate = '404';
        $this->smarty->assign('message', $message);
        
    }
}
?>
