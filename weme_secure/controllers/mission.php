<?php
//Controleur pour la gestion de la home
//pour la gestion des missions
class missionController extends controller {


    
    //par default on liste les missions
    function defaultAction($search=array()){
        unset($_SESSION['search']);
        $DBmembre = new DBMembre();
       
        if(DBMembre::$id_type==DBMembre::TYPE_ADMINISTRATEUR){
	    $_SESSION['search'] = $search;//si on passe par la methode searchAction on passe la parametre dans la session
	    
            $DBmission= new DBMission();
            
            // Numero de page (1 par défaut)
            $paginate['current_page'] = (isset($_GET['paginate']) && is_numeric($_GET['paginate'])) ? $_GET['paginate'] : 1;
            // Numéro du 1er enregistrement à lire
            $paginate['limit']        = ($paginate['current_page'] - 1) * NOMBRE_PER_PAGE;    
            //on recupere la liste de nos CV pour le comptage
            $listMissions             = $DBmission->getListMissions(array(), $search, array($paginate['limit'], NOMBRE_PER_PAGE));
            
	    // Pagination
            $paginate['total_pages']  = ceil($listMissions['count'] / NOMBRE_PER_PAGE);
            
            //Si on du monde en sortie on les recupere
            if($listMissions['count']){
                
                //Puis on les envoie dans la vue
                $this->smarty->assign('nbrPages', $paginate['total_pages']);
                $this->smarty->assign('pageCourante', $paginate['current_page']);
                $this->smarty->assign('listMissions', $listMissions['missions']);
                $this->smarty->assign('count', $listMissions['count']);
                $this->smarty->assign('mode', 'normal');//permet d'ajouter un paramtrer dans lien favoris des mission et updtae statut
            }
	    
	    $DBadmin = new DBAdmin();
	    $this->smarty->assign('getListStatuts', $DBadmin->getListStatutsMissions());
        
        }else{
            header('location:'.Tools::getLink("loggin"));
        }
	
	  //Pour le moteur de recherche
	$publications = array(
	   ""         => "Publication",
	   "1 DAY"    => "Aujourd'hui",
	   "7 DAY"    => "Il y a moins d'une semaine",
	   "1 MONTH"  => "Il y a moins d'un mois",
	   "6 MONTH"  => "Il y a moins de 6 mois"
	);
	$this->smarty->assign('listPublications', $publications);
    }
    
    //Afficher une mission
    function missionAction(){
            
            $DBmission  = new DBMission();
            $DBmembre   = new DBMembre();
	    $DBreponse  = new DBReponse();
	    $DBadmin    = new DBAdmin();
       
	    if(DBMembre::$id_type==DBMembre::TYPE_ADMINISTRATEUR){
	    
		//on recupere la liste de nos mission pour le comptage
		$mission         = $DBmission->getListMissions(array('id_mission'=>array($this->crypt->id_mission)));
    
		//Si on du monde en sortie on les recupere
		if($mission['count']){
			$mission             = $mission['missions'][0];
			$this->smarty->assign('missionCompetences', $mission->getCompetences());
			$this->smarty->assign('missionLangues',     $mission->getLangues());
			$this->smarty->assign('missionEtudes',      $mission->getEtudes());
			$this->smarty->assign('missionPostes',      $mission->getPostes());
			$this->smarty->assign('missionCategorie',   $mission->getCategorie());
			$this->smarty->assign('mission',            $mission);
			
			/*Pour afficher nos statut reponse */
			$this->smarty->assign('getListStatutsReponses', $DBadmin->getListStatutsReponses());
			$this->smarty->assign('statistiquesReponses', $mission->getReponses(false, false)->statistiques);
			/*Fin de la partie*/
			
			//Pour recuperer les profils qui matchent pour une mlission donnée
			//$DBmembre->getListProfilsMatchMissions(array($this->crypt->id_mission));
			
		}
		else{
		    header('location:'.Tools::getLink('missions'));
		}
	    
	    }else{
		header('location:'.Tools::getLink("loggin"));
	    }
	    
             
    }
  
    
    function searchAction(){
        
	if($this->request['id_mission_statut'])
                    $this->request['search']['id_mission_statut']=$this->request['id_mission_statut'];
            
        if($this->request['search'])
            $_SESSION['search'] = $this->request['search'];
        
        $this->defaultAction($_SESSION['search'] );
        
	$this->smarty->assign('mode', 'search');//permet d'ajouter un paramtrer dans lien favoris des mission et updtae statut
	$this->smarty->assign('post', $_SESSION['search']);//très pratique puisque il permet de ressortir le post dans la vue


    }
    //Changer le statut d'une mission
    function changeStatusAction(){
                      
        if(!is_numeric($this->crypt->id_mission) && !is_numeric($this->request['id_mission_statut'])){
	    header('location:'.Tools::getLink('missions'));
	}
 
        if(($this->crypt->mode=='search') || ($this->request['mode']=='search')){
            $page = is_numeric($this->crypt->paginate) ? $this->crypt->paginate : $this->request['paginate'];
            $link =  Tools::getLink($this->request['page'], 'search', array('paginate'=> $page));
        }
        else if($this->crypt->mode=='home'){
            $link =  Tools::getLink('home');
        }
        else if($this->crypt->mode=='normal'){
             $link =  Tools::getLink('missions', '', array('paginate'=> $this->crypt->paginate));
        }
        else{
             $link =  Tools::getLink('missions', '', array('paginate'=> $this->request['paginate']));
        }
  
	$DBmission = new DBMission();

	if($_POST){
	    $this->request['id_mission']        = $_POST['id_mission'];
	    $this->request['id_mission_statut'] = $_POST['id_mission_statut'];
	}
	else{
	    $this->request['id_mission']        = array($this->crypt->id_mission);
	    $this->request['id_mission_statut'] = $this->crypt->id_mission_statut;
	}

	if($error = $DBmission->editMission($this->request)){

		if(is_array($error) && count($error)>0){
		    $this->smarty->assign('post', $_POST);
		    $this->smarty->assign('error', $error);
		    $this->defaultAction();
		}
		else{
		    Tools::setFlashMessage('Statut modifié avec succes', true, 'success-update-statut', $link);
		}  
	}
	else{
	    Tools::setFlashMessage('Update OK', true, 'success', $link);
	} 

         
    }
    //Permet de rediriger la page vers my_profil afin d'appeler le controller CVteque
    //Uniqument quand onsubmit le formulaire avec le bouton proposer au favori
    //Attention donc, cette methode n'est pas appeler quand on clique sur le lien proposer sous la mission...
    //Moyen simple pour eviter les confusions
    function proposeMissionAction(){
        header('location:'.Tools::getLink('my_profils', 'proposeMission', array("id_mission"=>implode(',',$this->request['id_mission'])),true));
    }
    
}
?>