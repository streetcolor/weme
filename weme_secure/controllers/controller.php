<?php

class controller {
    
    protected $smarty;
    protected $request;
    protected $crypt;
    protected  $membre; //On va stocker notre client ou notre prestataire dans cet attribut et le recuperer dans les autres controllers via $this->membre
    public static $singleton_membre_call = array('admin'=>0); // ce singleton permet de conserver les appel au methode getPresta et getClient, on evite ainsi de rappeler deux fois la meme methode
    public static $singleton = array();
    
    public function __construct($smarty, $request){
                                
        $this->smarty          = $smarty;
        $this->request         = $request;
        
	$this->smarty->assign('URL_SITE', URL_SITE);
	
        if(isset($request['crypt'])){
            $query = Tools::decrypt($_REQUEST['crypt']);
            parse_str($query, $output);//DBManager::debugVar($output, false);
            $this->crypt = (object) $output;
        }

        //On va grace � ce code r�cuperer les controller qui sont app�l� dans la page
        $calledClass = get_called_class();
        if(!in_array($calledClass, self::$singleton)){
            self::$singleton[] =$calledClass;
        }
        
        //Si je suis conn�ct� on va loader automatiquement des infos de base
        $DBmembre       = new DBMembre();
   
	$this->membre = $this->administrateurAction();
	
	if(!DBMembre::$id_identifiant && $this->request['page']!='loggin'){
	    header('location:'.Tools::getLink('loggin'));
	}
	//Succes message commun a tous les controller
	if($messageCallBack=Tools::getFlashMessage('success')){
	    $this->smarty->assign('messageCallBack', $messageCallBack);
	}
	
	//gestion des callback
	if($messageCallBack=Tools::getFlashMessage('success-send-message')){
                $this->smarty->assign('messageCallBack', $messageCallBack);
        }
                                
        if($messageCallBack=Tools::getFlashMessage('success-send-favoris-message')){
                $this->smarty->assign('messageCallBack', $messageCallBack);
        }
	if($messageCallBack=Tools::getFlashMessage('success-update-statut')){
		$this->smarty->assign('messageCallBack', $messageCallBack);
	}
 
    }
    
   //Pour recuperer un administrateur 
    function administrateurAction(){
	$DBmembre       = new DBMembre();

	if(DBmembre::$id_type==DBMembre::TYPE_ADMINISTRATEUR){
	    if(!self::$singleton_membre_call['admin']){
	      
	       $id_identifiant = DBMembre::$id_identifiant;
	       
	       //On recupere le prestataire par default afin de d'affciher des infos dans le Header
	       $getAdmin = $DBmembre->getAdministrateur($id_identifiant);
   
	       $this->smarty->assign('admin', $getAdmin);
	       
	       self::$singleton_membre_call['admin'] ++;
	       
	       return $getAdmin;
	   }
	}
	
	else{
	    global $setTemplate;
	    $setTemplate = 'loggin';
	}
    }
    
}

?>
