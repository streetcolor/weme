<?php
//Controleur pour la gestion des membres
//pour la gestion des clients et prestataires 
class membreController extends controller{
    
    //par default on liste les missions
    function defaultAction($search=array()){
        unset($_SESSION['search']);
        $DBmembre = new DBMembre();
       
        if(DBMembre::$id_type==DBMembre::TYPE_ADMINISTRATEUR){
	    
	    
	    $_SESSION['search'] = $search;//si on passe par la methode searchAction on passe la parametre dans la session
	    
            $DBmembre= new DBMembre();
            
            // Numero de page (1 par défaut)
            $paginate['current_page'] = (isset($_GET['paginate']) && is_numeric($_GET['paginate'])) ? $_GET['paginate'] : 1;
            // Numéro du 1er enregistrement à lire
            $paginate['limit']        = ($paginate['current_page'] - 1) * NOMBRE_PER_PAGE;    
            
	    //on recupere la liste de nos CV pour le comptage    
	    if($this->request['page']=='clients'){
		$listMembre          = $DBmembre->getListClients(array(), $search, array($paginate['limit'], NOMBRE_PER_PAGE));
	    }
	    elseif($this->request['page']=='prestataires'){
		$listMembre          = $DBmembre->getListPrestataires(array(), $search, array($paginate['limit'], NOMBRE_PER_PAGE));
	    }
	    // Pagination
            $paginate['total_pages']  = ceil($listMembre['count'] / NOMBRE_PER_PAGE);

            //Si on du monde en sortie on les recupere
            if($listMembre['count']){
                
                //Puis on les envoie dans la vue
                $this->smarty->assign('nbrPages', $paginate['total_pages']);
                $this->smarty->assign('pageCourante', $paginate['current_page']);
                $this->smarty->assign('listClients', $listMembre['clients']);
		$this->smarty->assign('listPrestaires', $listMembre['prestataires']);
                $this->smarty->assign('count', $listMembre['count']);
                $this->smarty->assign('mode', 'normal');//permet d'ajouter un paramtrer dans lien favoris des mission et updtae statut
		
		//Pour le moteur de recherche    
		$DBadmin = new DBAdmin();
		$this->smarty->assign('listStructures', $DBadmin->getListStructures());
            }
        
        }else{
            header('location:'.Tools::getLink("loggin"));
        }

    }
    //Quand on modifie le prestataire depuis la page edit_moncompte
    function prestataireAction(){
        $DBmembre       = new DBMembre();
	$DBadmin        = new DBAdmin(); 
	$id_identifiant = $this->crypt->id_identifiant;
	
    	//On recupere le prestataire par default afin de d'affciher des infos dans le Header
	$getPrestataire = $DBmembre->getListPrestataires(array('id_identifiant'=>$id_identifiant));
	$getPrestataire = $getPrestataire["prestataires"][0];
	
	$string = substr($getPrestataire->getAvatar()->getPath(), 1);
	
	$this->smarty->assign('avatar', URL_SITE.$string);
    
	//Si il s'agit d'un Freelance ou Intermmittent il ne possede que UN profil !
	if($getPrestataire->getStructure()->id_structure != DBMembre::TYPE_AGENCE){
		  
	    $this->smarty->assign('agence', false);	  
	    
	    /*Affichage des réalisations*/	  
	    $listRealisations      = $getPrestataire->getRealisations();
	    $this->smarty->assign('countRealisations', $listRealisations->count);
	    if($listRealisations->count > 0)
		$this->smarty->assign('listRealisations', $listRealisations->files);
	    /*fin Affichage des réalisations*/
	    
	    
	}
	else{
	    
	    $this->smarty->assign('agence', true);	  
	    
	    /*Affichage des réalisations*/	  
	    $listRealisations      = $getPrestataire->getRealisations();
	    $this->smarty->assign('countRealisations', $listRealisations->count);
	    if($listRealisations->count > 0)
		$this->smarty->assign('listRealisations', $listRealisations->files);
	    /*fin Affichage des réalisations*/
	    
	    /*Affichage des profil agence*/	
	    $listProfils = $getPrestataire->getProfils(true);
	    
	    $this->smarty->assign('listProfils', $listProfils);
	    /*fin Affichage des profil agence*/
	    
	    /*Affichage de la plaquette agence*/	
	    $plaquette = $getPrestataire->getPlaquette();
	    $this->smarty->assign('plaquette', $plaquette);
	    /*fin Affichage de la plaquette agence*/	
	}
	
	/*Pour afficher nos statut reponse */
	$this->smarty->assign('getListStatutsReponses', $DBadmin->getListStatutsReponses());
	$this->smarty->assign('statistiquesReponses', $getPrestataire->getReponses()->statistiques);
	/*Fin de la partie*/
	
	
	$this->smarty->assign('prestataire', $getPrestataire);
  
    }
    //Quand on modifie le client depuis la page edit_moncompte
    function clientAction(){
        $DBmembre       = new DBMembre();
	$DBadmin        = new DBAdmin(); 
	$id_identifiant = $this->crypt->id_identifiant;

	//On recupere le client
	$getClient = $DBmembre->getClient($id_identifiant);
	
	/*SI il est admin on recupere ses managers*/
	if($getClient->getId_role()==DBMembre::TYPE_ADMIN)
	    $listManagers = $getClient->getManagers();
	/*Fin recuperation*/
	
	/*Pour afficher nos statut mission*/
	$this->smarty->assign('getListStatuts', $DBadmin->getListStatutsMissions());
	$this->smarty->assign('statistiques', $getClient->getMissions()->statistiques);
	/*Fin de la partie*/
	
	/*Pour afficher nos statut reponse */
	$this->smarty->assign('getListStatutsReponses', $DBadmin->getListStatutsReponses());
	$this->smarty->assign('statistiquesReponses', $getClient->getReponses()->statistiques);
	/*Fin de la partie*/
	
	//$this->smarty->assign('listReponses', $getClient->getReponses()->statistiques);
	$this->smarty->assign('listManagers', $listManagers);
	$this->smarty->assign('role', $getClient->getId_role());
	// Tools::debugVar($getClient->getFavoris());
	$this->smarty->assign('avatar', $getClient->getAvatar());
	//Tools::debugVar($getClient->getFavoris());
	$this->smarty->assign('getClient', $getClient);

    }
    
    function searchAction(){
        
	if($this->request['id_mission_statut'])
                    $this->request['search']['id_mission_statut']=$this->request['id_mission_statut'];
            
        if($this->request['search'])
            $_SESSION['search'] = $this->request['search'];
        
        $this->defaultAction($_SESSION['search'] );
        
	$this->smarty->assign('mode', 'search');//permet d'ajouter un paramtrer dans lien favoris des mission et updtae statut
	$this->smarty->assign('post', $_SESSION['search']);//très pratique puisque il permet de ressortir le post dans la vue


    }


   
}

?>
