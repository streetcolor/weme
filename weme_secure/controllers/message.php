<?php
//Controleur pour la gestion de la message et mail et contact
class messageController extends controller {
    
    function defaultAction(){
	$getListMessages = $this->membre->getMessages();
	    
	$this->smarty->assign('countMessages', $getListMessages->count);
	if($getListMessages->count>0)
	    $this->smarty->assign('listMessages', $getListMessages->messages);
	
	$getListTemplates = Tools::ScanDirectory('web/mails');
	$this->smarty->assign('listTemplates', $getListTemplates);

	return array('templates'=>$getListTemplates, 'messages'=>$getListMessages);
    }
    
    //ajouter un message type
    function addMessageAction(){
	    $DBmemebre=new DBMembre();
	    $DBmembre  = new DBMembre();
	    $DBmessage = new DBMessage();
            $this->defaultAction();
            
            if($_POST){
                
                $this->request['id_identifiant'] = DBMembre::$id_identifiant; 
		
                if($error = $DBmessage->makeMessage($this->request)){
                    if(is_array($error) && count($error)>0){
                        $this->smarty->assign('post', $_POST);
                        $this->smarty->assign('error', $error);
                    }
                    else{
                        Tools::setFlashMessage('Update OK', true, 'success', Tools::getLink($request["page"]));
                    }  
                }
            }
    
    }
    //ajouter un template type
    function addTemplateAction(){
	    
        $DBmembre = new DBMembre();
        $DBfile   = new DBFile;
         $this->defaultAction();
        
        if(DBMembre::$id_type==DBMembre::TYPE_ADMINISTRATEUR){
            $this->request['file']           = $_FILES;
            $this->request['id_identifiant'] = DBMembre::$id_identifiant;
	    $this->request['file_type']      = 'template'; 
            
            if($error = $DBfile->makeFile( $this->request)){
                //Tools::debugVar($error);
                if(is_array($error) && count($error)>0){
                   
            
                     $this->smarty->assign('error', $error);
                }
                else{
                    Tools::setFlashMessage('Update OK', true, 'success', Tools::getLink('messages'));
                }
            }
        } 
    
    }
     //supprimer un template type
    function removeTemplateAction(){
        
	if($this->request['rmvTemplate']){
	
	    if(Tools::removeFile('web/mails/'.$this->request['rmvTemplate'])){
	    
		Tools::setFlashMessage('Update OK', true, 'success', Tools::getLink($this->request["page"]));
		
	    }
	
	}
	
	$this->defaultAction();

    }
    //editer un message type
    function editMessageAction(){
           
	    $DBmembre  = new DBMembre();
	    $DBmessage = new DBMessage();
            $this->defaultAction();
	     
            $editMessage = $DBmessage->getListMessages(array('id_client_message'=>$this->crypt->id_client_message, 'id_identifiant'=>DBMembre::$id_identifiant));
            
            if($editMessage['count']>0){
                $message = $editMessage['messages'][0];
                $this->smarty->assign('editMessage', $message);
                $this->smarty->assign('id_client_message', $this->crypt->id_client_message);//Pour tester si on est bien sur l'edition d'un manager
            }
	    
            if($_POST){
                
                $this->request['id_identifiant']    = DBMembre::$id_identifiant; 
		$this->request['id_client_message'] = $this->crypt->id_client_message;
		
                if($error = $DBmessage->editMessage($this->request)){
                    if(is_array($error) && count($error)>0){
                        $this->smarty->assign('post', $_POST);
                        $this->smarty->assign('error', $error);
                    }
                    else{
                        Tools::setFlashMessage('Update OK', true, 'success', Tools::getLink($request["page"]));
                    }  
                }
            }
    
    }
    //supprimer un message type
    function removeMessageAction(){
            $DBmembre  = new DBMembre();
	    $DBmessage = new DBMessage();
            $this->defaultAction();
            
	    if(!is_numeric($this->crypt->id_client_message)){
		
		header('location:'.Tools::getLink('new_message'));
	     
	    }
	    
	    $this->request['id_identifiant'] = DBMembre::$id_identifiant;
	    $this->request['id_client_message'] = $this->crypt->id_client_message;
	    
	    if($error = $DBmessage->removeMessage($this->request)){
		if(is_array($error) && count($error)>0){
		    $this->smarty->assign('error', $error);
		}
		else{
		    Tools::setFlashMessage('Update OK', true, 'success', Tools::getLink($request["page"]));
		}  
	    }
    
    }
    //envoyer un mail au profil selctionné depuis la CVteque
    function sendEmailAction(){
	
	$DBmembre = new DBMembre();
	$DBmessage = new DBMessage();
	
	$getListEmails = $this->defaultAction();//Recuration des TPL mail et message
	
	//Si on bien de la page CVteque ou Prestataires on recupere des id_prestataire_profil
	if(is_array($this->request['id_prestataire_profil']) ){
	    $id_prestataire_profil = $this->request['id_prestataire_profil'];
	    $users       = $DBmembre->getListProfils(array('id_prestataire_profil'=>$id_prestataire_profil));
	    $listProfils = $users['profils'];
	    $id_type     = DBMembre::TYPE_PRESTATAIRE;
	}
	//Si on bien de la page Misssion ou Clients on recupere des id_client_manager
	elseif(is_array($this->request['id_client_manager']) ){
	    $id_client_manager = $this->request['id_client_manager'];
	    $users       = $DBmembre->getListManagers(array('id_client_manager'=>$id_client_manager));
	    $listProfils = $users['managers'];
	    $id_type     = DBMembre::TYPE_CLIENT;
	    
	}
	//On a un probleme :)
	else{
	    header('location:'.Tools::getLink('home'));
	}
        
	if(!$users['count'])
	    header('location:'.Tools::getLink('home'));
	    
	/*
	//Recuperation de nos mails types
	$getListMessages          = $this->membre->getMessages();
	*/
	//Si on appel un mail type
	if($_POST['id_client_message'] && isset($this->request['chooseMessage'])){
	    $getListMessages = $getListEmails["messages"];
	    $id_client_messages       = array();
	    foreach($getListMessages->messages as $message){
		$id_client_messages[] = $message['id_client_message'];
	    }	
	    
	    if(in_array($_POST['id_client_message'], $id_client_messages)){

		$callMessage = $DBmessage->getListMessages(array('id_client_message'=>$_POST['id_client_message'], 'id_identifiant'=>DBMembre::$id_identifiant));
            
		if($callMessage['count']>0){
		    $message = $callMessage['messages'][0];
		    $this->smarty->assign('post', $message);
		}
		
	    }
	    
	}
	else if(isset($this->request['sendMessage'])){
	    
	    //On va recuperer les email afin de pouvoir envoyer des message 
	    $emails = array();
	    foreach($listProfils as $profil){
		$emails[] = $profil->getEmail();
	    }
	    $this->request['mail_author']    = MAIL_REPLY_TO;
	    $this->request['usr_email']      = array_unique($emails);
	    $this->request['id_identifiant'] = $this->membre->getId_identifiant();
	    if($error=$DBmessage->makeMail($this->request)){
		if(is_array($error) && count($error)>0){
		    $this->smarty->assign('post', $_POST);
		    $this->smarty->assign('error', $error);
		}
		else{
		    
		    Tools::setFlashMessage('Le message à bien été envoyé', true, 'success-send-message', Tools::getLink($this->request['from']));//le from vient de l'url definie dans le form
		}  
	    }
	    
	}
	
	
	//$this->smarty->assign('id_prestataire_profil', Tools::encrypt('id_prestataire_profil='.$this->crypt->id_prestataire_profil));//Utile pour le chargement d'un message type
	$this->smarty->assign('listProfils', $listProfils);
	$this->smarty->assign('id_type', $id_type);
	$this->smarty->assign('from', $this->request['from']);//Provient de l'url
	
    }
    

}
?>
