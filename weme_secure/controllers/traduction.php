<?php
//Controleur pour la gestion de la home
//pour la gestion des dashboard client et prestataire
class traductionController extends controller{
    
    function defaultAction($search=array()){
        $DBadmin = new DBAdmin();
        unset($_SESSION['search']);
    
	$filtreDefault = array();

	if(is_array($filtreDefault)){
	    
	    $_SESSION['search'] = $search;//si on passe par la methode searchAction on passe la parametre dans la session
	            
            // Numero de page (1 par défaut)
            $paginate['current_page'] = (isset($_GET['paginate']) && is_numeric($_GET['paginate'])) ? $_GET['paginate'] : 1;
            // Numéro du 1er enregistrement à lire
            $paginate['limit']        = ($paginate['current_page'] - 1) * NOMBRE_PER_PAGE;    
            //on recupere la liste de nos page pour le comptage
            $liste_page               = $DBadmin->getListEnvironnementPages($filtreDefault, $search, false);//Tools::debugVar($liste_page, false);
            
	    // Pagination
            $paginate['total_pages']  = ceil($listMissions['count'] / NOMBRE_PER_PAGE);
            
            //Si on du monde en sortie on les recupere
            $this->smarty->assign('liste_page',$liste_page);
        }
	        
        //Tableau pour avoir concordance id_environnement -> environnement (passé par une table environnement à l'avenir)
        $environnement_tab = array('visiteur', 'prestataire', 'client', 'globales');
        $this->smarty->assign('environnements', $environnement_tab);
    }
    
    //Recherche de traductions
    function searchAction(){
        
        if($this->request['search'])
            $_SESSION['search'] = $this->request['search'];
        
	if(!empty($this->request['crypt'])){
	    $this->editXMLAction($_SESSION['search']);
	}
	else
	    $this->defaultAction($_SESSION['search'] );
        
	$this->smarty->assign('mode', 'search');//permet d'ajouter un paramtrer dans lien favoris des mission et updtae statut
	$this->smarty->assign('post', $_SESSION['search']);//très pratique puisque il permet de ressortir le post dans la vue


    }
    
    function editXMLAction($search = array()){
	
	if(!count($_SESSION['search']['langue'])){
	    $_SESSION['search']["langue"]=array('fr', 'en', 'es');
	}
	
        $xml_name =  _XML_. (($this->crypt->env == "globales")?"":$this->crypt->env. '/'). $this->crypt->xml. '.xml';
        $this->smarty->assign('xml_name', $xml_name);
        
        //On créer un nouvel objet xml pour la langue à traduire
	$xml = new XMLEngine( $xml_name);
        $this->smarty->assign('xml', $xml);
        
        //le titre de la page à traduire
        $titre_page = $this->crypt->titre;
        $this->smarty->assign('page_titre',$titre_page );
        
        //On récupère les identifiants des balises :
	if(count($search['langue']))
	    $langue = $search['langue'];
	else
	    $langue = $_SESSION['search']["langue"];
	    
	$idTab  = $xml->getItemNodeListFull($langue, $search["mots_trad"]);//Tools::debugVar($idTab);
	
	// Numero de page (1 par défaut)
	$paginate['current_page'] = (isset($_GET['paginate']) && is_numeric($_GET['paginate'])) ? $_GET['paginate'] : 1;
	// Numéro du 1er enregistrement à lire
	$paginate['limit']        = ($paginate['current_page'] - 1) * NOMBRE_PER_PAGE;    
	//on recupere la liste de nos page pour le comptage
	$liste_page               = array_chunk($idTab, NOMBRE_PER_PAGE, true);//Tools::debugVar($search, false);
	// Pagination
	$paginate['total_pages']  = count($liste_page);
    
	 $this->smarty->assign('nbrPages', $paginate['total_pages']);
	$this->smarty->assign('pageCourante', $paginate['current_page']);
	$this->smarty->assign('count', $listMissions['count']);	
	$this->smarty->assign('nodeList', $liste_page[$paginate['current_page']-1]);
    }
    
    function tradAction(){
        //Tools::debugVar($_POST, false);
        $tabATraduire = $_POST['trad'];
        $saveOk = true;
        
        $xml_name = $_POST['xml_name'];//Tools::debugVar($xml_name, false);
        //On créer un nouvel objet xml pour la langue à traduire
	$xml = new XMLEngine( $xml_name);
        
        foreach($tabATraduire as $nodeId => $aValues){
        
            if(count($aValues)){
                foreach($aValues as $lang => $newTrad){
                    if($newTrad != ""){
                        if ( !$xml->modifyXMLElement( $nodeId, $lang, $newTrad, false ) )
                             $saveOk = false;
                    }
                }
            }
        }
        if($saveOk){
            Tools::setFlashMessage('Les traductions ont été enregistrées avec succès', true, 'success', Tools::getLink('traduction'));
        }else{
            Tools::setFlashMessage('Une erreure s\'est produite lors de la modification du fichier xml de traduction', false, 'success', Tools::getLink('traduction'));
        }
        Tools::debugVar($saveOk, false);
    }
    
    //Pour mettre à jour la base de donnée à partir du xml de la page choisi
    function updBaseTradAction(){
        $xml_name =  _XML_. (($this->crypt->env == "globales")?"":$this->crypt->env. '/'). $this->crypt->xml. '.xml';
        $this->smarty->assign('xml_name', $xml_name);//Tools::debugVar($xml_name);
        
        //On créer un nouvel objet xml pour la langue à traduire
	$xml = new XMLEngine( $xml_name);
        $this->smarty->assign('xml', $xml);
        
        
        if(!$result = $xml->saveXMLInDB())
            Tools::setFlashMessage('Un problème c\'est produit lors d l\'enregistrement dans la base de données', true, 'success', Tools::getLink('traduction'));
        else
            Tools::setFlashMessage('La page selectionné a été mise à jour dans la base de données', true, 'success', Tools::getLink('traduction'));
    }
    
    //Opération inverse : pour mettre à jour le xml à partir de la base de donnée (ne doit être utilisé qu'en cas d'éxtrême urgence genre si on a perdu les xml)
    function generateXMLAction(){
        $dbAdmin = new DBAdmin();
        $pageId = $this->crypt->pageId;//Tools::debugVar($pageId);
        
        if($pageId>0){
            if(!$result = $dbAdmin->generateXmlTraduction($pageId))
                Tools::setFlashMessage('Un problème c\'est produit lors d a génération du xml à partir de la base de donnée', true, 'success', Tools::getLink('traduction'));
            else
                Tools::setFlashMessage('Le xml de la page choisi a été régénéré à partir de la base de données', true, 'success', Tools::getLink('traduction'));        
        } 
    }
}