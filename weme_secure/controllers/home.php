<?php
//Controleur pour la gestion de la home
//pour la gestion des dashboard client et prestataire
class homeController extends controller{
        
    function defaultAction($membre=false){
        $DBmembre   = new DBMembre();
        $DBmission  = new DBMission();
        $DBReponse  = new DBReponse();
        
        if(DBMembre::$id_identifiant){
            /*On va créer une mini session qui stock nos compteurs de projet et reponse pour les afficher dans le header*/
             /*ATTENTION la session doit être setté ici même puisque c'est la premiere page qui s'affiche quand on se connecte !*/
            /*L'assignation de la variable compteur se fait ici pour quon puisse l'afficher en home mais aussi dans la methode getPrestataire()*/
            /*A noter que la methode prestataire recuperera que la session dejé existante autrement dit inutile de recompter les entrées avec les Classes DBmission et DBreponse*/
            $listMissions                         = $DBmission->getListMissions(array(), array(), false);
            $listCv                               = $DBmembre->getListProfils(array(), array(), false);
            $_SESSION['compteur']["all_cv"]       = $listCv['count']; //Toutes nos reponses
            $_SESSION['compteur']["all_missions"] = $listMissions['count']; //Les nouvelle missions de 10 jours CF:methode $DBmission->getListMissions()
            $_SESSION['compteur']["new_missions"] = $listMissions['new_missions']; //Les nouvelle missions de 10 jours CF:methode $DBmission->getListMissions()
            //assignation pas utile on reccupere la session via {$smarty.session.compteur.all_missions} directement dans le header
            /*Fin de la partie*/
            
            /**Preparons nos statistique de la home/
            
            /*Partie qui recupere les Missions en attente*/
            $listMissions = $DBmission->getListMissions(array('id_mission_statut'=>DBMission::MISSION_ATTENTE), array(), false);
            $this->smarty->assign('listMissions', $listMissions);
            /*Fin de la partie*/
            
            /*Partie qui recupere les Profils nouveau*/
            $listProfils = $DBmembre->getListProfils(array(), array("modifie_le"=>"1 DAY"), false);
            $this->smarty->assign('listProfils', $listProfils);
            /*Fin de la partie*/
            
            /*Partie qui recupere des stats de mission*/
            $DBadmin      = new DBAdmin();
            $listMissions = $DBmission->getListMissions(array(), array(), false);
            $this->smarty->assign('getListStatutsReponse', $DBadmin->getListStatutsMissions(array(), array(), false));
            $this->smarty->assign('statistiques', $listMissions['statistiques']);
            /*Fin de la partie*/
            
            /*Partie qui recupere les reponses aux missions*/
            $listReponses = $DBReponse->getListReponses(array(), array(), false);
            $this->smarty->assign('reponses', $listReponses);
            /*Fin de la partie*/
            
            /*Pour afficher nos statut reponse (sert dans pour le menu Mon siuvi de proposition a droire du dashboard)*/
            $DBadmin      = new DBAdmin(); 
            $this->smarty->assign('getListStatuts', $DBadmin->getListStatutsReponses());
            /*Fin de la partie*/
            

            $this->smarty->assign('countMissions',    $listMissions['count']);
            $this->smarty->assign('countPrestataires', $DBmembre->getListPrestataires());
            $this->smarty->assign('countClients',     $DBmembre->getListClients(array(), array(),false));
            $this->smarty->assign('countProfils',     $DBmembre->getListProfils(array(), array(),false));
        }
        
        //Pour le moteur de recherche
	$publications = array(
	   ""         => "Publication",
	   "1 DAY"    => "Aujourd'hui",
	   "7 DAY"    => "Il y a moins d'une semaine",
	   "1 MONTH"  => "Il y a moins d'un mois",
	   "6 MONTH"  => "Il y a moins de 6 mois"
	);
	$this->smarty->assign('listPublications', $publications);
        
    }
    //Pour faire une connexion
    function connexionAction(){
        
        $DBmembre  = new DBMembre();
        
        if($error =  $DBmembre->makeConnexion($this->request)){
            if(is_array($error) && count($error)>0 && count($error)<2){
                $this->smarty->assign('error', $error);
                header('location:'.Tools::getLink("loggin"));
            }
            else{
                $_SESSION["user"]=$error;
                header('location:index.php');
            }
        }
        
        
       
    }
    //Pour faire une deconnexion
    function loggoutAction(){
        $smarty  = $this->smarty;
        $request =$this->request;
        
        unset($_SESSION['user']);
        header('location:'.Tools::getLink('loggin'));
    }
    //Pour un mot de passe perdu
    function passwordAction(){
        
        
        $DBmembre  = new DBMembre();
        if($error = $DBmembre->makePassword($this->request)){
    
            if(is_array($error) && count($error)>0){
        
                $this->smarty->assign('error', $error);
            }
            else{
                Tools::setFlashMessage('Votre mot de passe vient de vous être envoyé', true, 'success-password', Tools::getLink('home'));
            }
        }
    
       
    }
       
    
  

 
}
?>
