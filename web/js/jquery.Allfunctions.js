$(document).ready(function () {
   
   
 
   if($("p[id*='anchor']").length){
     var $error  =  $("p[id*='anchor']");
     $error  = $error[0];
      $('html,body').animate({
            scrollTop: $("#"+$error.id).offset().top - 130
      }, 'slow');
   }
    //AFFICHAGE DES TOOLTIPS

   $("[rel='tooltip']").tooltip({html:true});
   
   $('#downloadAvatarCrop').modal('show')
   $(function () {
	$('.checkall').on('click', function () {
	    $(this).closest('table').find(':checkbox').prop('checked', this.checked);
	});
    });

    //REDIMMENSIONNER LA COLONNE DE GAUCHE
    if ($('.homeDroite').height() > 1000) {
        $('.menuVertical').height($(".homeDroite").height());
    }
    
    //Utilisation des ancres
    if (location.hash) {
        $('html,body').animate({
            scrollTop: $(location.hash).offset().top - 110
        }, 'slow');
    }
    
    $(document).on('click', ".scroller", function (event, $hash) {
        if ($('.CVHeader').length) {
            $('html,body').animate({
                scrollTop: $('.CVHeader').offset().top - 110
            }, 'slow');
        } else {
            $('html,body').animate({
                scrollTop: $($(this).attr('href')).offset().top - 110
            }, 'slow');
        }
    });
    
    //SUPPRESSION DES ENTREES TELLES QUE EXPERIECE CV
    $('.delEntry').on('click', function (e) {
        e.preventDefault();
        if ($(this).attr('title'))
            $message = $(this).attr('title');
        else
            $message = 'Vous etes sur le point de supprimer cette entrée, souhaitez vous continuer ?';
        $confirmation = confirm($message);
        if ($confirmation) {
            document.location.href = $(this).attr('href');
        }
    });
    
    $(".toggle").click(function () {
        $id = $(this).attr('id');
        $result = "#" + $id + "_result";
        $($result).slideToggle("slow");
    });
    
    //Slide de la home
    if ($("#iview").length) {
        $('#iview').iView({
            pauseTime: 3000,
            pauseOnHover: true,
            directionNav: true,
            directionNavHide: false,
            controlNav: true,
            controlNavNextPrev: false,
            controlNavTooltip: false,
            nextLabel: "Próximo",
            previousLabel: "Anterior",
            playLabel: "Jugada",
            pauseLabel: "Pausa",
            timer: "pie",
            timerBg: "#EEE",
            timerColor: "#ff786a",
            timerDiameter: 20,
            timerPadding: 2,
            timerStroke: 1
        });
        $('#iview2').iView({
            pauseTime: 3000,
            pauseOnHover: true,
            directionNav: true,
            directionNavHide: false,
            controlNav: true,
            controlNavNextPrev: false,
            controlNavTooltip: false,
            nextLabel: "Próximo",
            previousLabel: "Anterior",
            playLabel: "Jugada",
            pauseLabel: "Pausa",
            timer: "pie",
            timerBg: "#EEE",
            timerColor: "#00cccc",
            timerDiameter: 20,
            timerPadding: 2,
            timerStroke: 1
        });
    }
    //Autocompletion
    var $complements = new Object();
    if ($("#id_poste").length)
        $complements['poste'] = $("#id_poste").val().split(',');
    if ($("#id_competence").length)
        $complements['competence'] = $("#id_competence").val().split(',');
    if ($("#id_langue").length)
        $complements['langue'] = $("#id_langue").val().split(',');
    if ($("#id_etude").length)
        $complements['etude'] = $("#id_etude").val().split(',');
    if ($("#id_categorie").length)
        $complements['categorie'] = $("#id_categorie").val().split(',');

    function split(val) {
        return val.split(/,\s*/);
    }

    function extractLast(term) {
        return split(term).pop();
    }

    function arrayUnset(array, value) {
        array.splice(array.indexOf(value), 1);
    }

    function in_array(needle, haystack, argStrict) {
        // http://kevin.vanzonneveld.net
        // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
        // +   improved by: vlado houba
        // +   input by: Billy
        // +   bugfixed by: Brett Zamir (http://brett-zamir.me)
        // *     example 1: in_array('van', ['Kevin', 'van', 'Zonneveld']);
        // *     returns 1: true
        // *     example 2: in_array('vlado', {0: 'Kevin', vlado: 'van', 1: 'Zonneveld'});
        // *     returns 2: false
        // *     example 3: in_array(1, ['1', '2', '3']);
        // *     returns 3: true
        // *     example 3: in_array(1, ['1', '2', '3'], false);
        // *     returns 3: true
        // *     example 4: in_array(1, ['1', '2', '3'], true);
        // *     returns 4: false
        var key = '',
            strict = !! argStrict;
        if (strict) {
            for (key in haystack) {
                if (haystack[key] === needle) {
                    return true;
                }
            }
        } else {
            for (key in haystack) {
                if (haystack[key] == needle) {
                    return true;
                }
            }
        }
        return false;
    }

    function loadAutocompletion() {
        function loadData(event, ui) {
            event.preventDefault();
            var $dom = null;
            if (!in_array(ui.item.id, $complements[$table])) {
                //Si notre input souhaite utiliser un systeme de notation
                $eval = $('#' + $table).hasClass('entryEval') ? true : false;
                //Si notre input souhaite utiliser limiter la saisie à 1 element
                $limit = $('#' + $table).hasClass('limit') ? true : false;
                //Si notre input autorise l'enregistrement de nouvelles entrées
                $new = $('#' + $table).hasClass('entryNew') ? true : false;
                //SI on indique à l'input de sauver les information dans la BDD
                if ($new) {
                    $.ajax({
                        type: "POST",
                        url: "web/ajax/ajax.select.php",
                        data: "saveEntry=" + ui.item.id + "&table=" + $table + "&crypt=" + $("#crypt").val(),
                        success: function (cont) {
                            if (cont > 0) {
                                if (!$limit) {
                                    if (!in_array(cont, $complements[$table])) {
                                        $complements[$table].push(cont);
                                    } else
                                        return false;
                                } else {
                                    $complements[$table] = new Array(cont);
                                }
                                $('#id_' + $table).val($complements[$table].join(","));
                                $dom = '<div class="span12 listeCompetence ligneEntry">' +
                                    '<a class="removeItem ico35" href="rmv_id_complement=' + cont + '"></a>' + ui.item.value;
                                if ($eval) {
                                    $dom +=
                                        '<div class="pull-right span6">' +
                                        '<div class="span6">' +
                                        'Niveau intermediaire' +
                                        '</div>' +
                                        '<div class="span6 GraphNiveauCompetence">' +
                                        '<div class="rating_box ">' +
                                        '<div class="span1" style="position:relative; top: -10px"><a href="">-</a></div>' +
                                        '<p data-level="1"  class="rate  PointCompetenceActive" style="float:left; margin-left: 5px;"> </p>  ' +
                                        '<p data-level="2" class="rate  PointCompetenceActive" style="float:left; margin-left: 5px;"> </p>  ' +
                                        '<p data-level="3"  class="rate active  PointCompetenceActive" style="float:left; margin-left: 5px;"> </p>  ' +
                                        '<p data-level="4"  class="rate  PointCompetence" style="float:left; margin-left: 5px;"> </p>' +
                                        '<p data-level="5" class="rate  PointCompetence" style="float:left; margin-left: 5px;"> </p>' +
                                        '<div class="span1" style="position:relative; top: -10px"><a href="">+</a></div>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>'
                                    '<div class="clearfix"></div>';
                                }
                                $dom += '</div>';
                                if (!$limit) {
                                    if ($dom) {
                                        $("#result_" + $table).prepend($dom);
                                    }
                                } else {
                                    $("#result_" + $table).html($dom);
                                }
                            }
                        }
                    });
                } else {
                    if (!$limit) {
                        $complements[$table].push(ui.item.id);
                    } else {
                        $complements[$table] = new Array(ui.item.id);
                    }
                    $('#id_' + $table).val($complements[$table].join(","));
                    $dom = '<div class=" listeCompetence ligneEntry">' +
                        '<a class="removeItem ico35" href="rmv_id_complement=' + ui.item.id + '"></a>' + ui.item.value +
                        '</div>';
                    if (!$limit) {
                        $("#result_" + $table).prepend($dom);
                    } else {
                        $("#result_" + $table).html($dom);
                    }
                }
            }
            return false;
        }
        $(".autocompletion").bind("keydown", function (event) {
            $table = $(this).attr("id");
            if (event.keyCode === $.ui.keyCode.TAB && $(this).data("ui-autocomplete").menu.active) {
                event.preventDefault();
            }
            //Sauvegarde la donnée quand on appuie sur Enter
            else if (event.keyCode === $.ui.keyCode.ENTER && !$(this).data("ui-autocomplete").menu.active) {
                $(".manual-insert").trigger('click', [$table]);
                event.preventDefault();
            }
        });
        //Sauvegarde la donnée quand on appuie sur le boutou ajouter
        $(document).on('click', ".manual-insert", function (event, $tableSet) {
            //si on passe pas un declenchment trigger
            if ($tableSet) {
                $table = $tableSet;
            }
            //si on pas le clique direct
            else {
                $table = $(this).attr('id').split('Button');
                $table = $table[0];
            }
            $val = $('#' + $table).val();
            var ui = {
                item: {
                    value: $val,
                    id: $val
                }
            }
            if (ui.item.value != '') {
                loadData(event, ui);
            }
            ui = null;
        });
        $(".autocompletion").autocomplete({
            minLength: 0,
            source: function (request, response) {
                request.table = $table;
                request.newentry = $("#" + $table).hasClass('entryNew') ? "true" : "false";
                $.getJSON("web/ajax/ajax.autocomplete.php", request, function (data, status, xhr) {
                    response($.map(data, function (item) {
                        return {
                            newentry: item.newentry, // si l'element possede un new entry à TRUE il devra etre enregistré dans la bdd
                            label: item.value, // le label s'affiche dans la liste autocomplete
                            value: item.value, // la value correspond a l'element selectionné
                            id: item.id // le id correspond à l'id_element et sera envoyer dans le hidden
                        }
                    }));
                });
            },
            search: function () {
                // custom minLength
                var term = extractLast(this.value);
                if (term.length < 2) {
                    return false;
                }
            },
            focus: function () {
                // prevent value inserted on focus
                return false;
            },
            select: function (event, ui) {
                loadData(event, ui);
            }
        });
    }
    $(document).on("click", ".removeItem", function (event) {
        $table = $(this).parents('.result').eq(0).attr("id").split('result_');
        $table = $table[1];
        event.preventDefault();
        $this = $(this);
        $href = $this.attr("href");
        //Si on a un crypt on peut alors supprimer un element dans la BDD
        //la categorie est géré differement
        if ($("#crypt").val() && ($table != 'categorie')) {
            $.ajax({
                type: "POST",
                url: "web/ajax/ajax.select.php",
                data: $href + "&crypt=" + $("#crypt").val(),
                success: function (cont) {
                    //arrayUnset($complements[$table], cont)
                    //console.log($complements[$table]);
                    if (cont > 0) {
                        $this.parents('.ligneEntry').eq(0).remove();
                    }
                }
            });
        }
        //Sinon la suppression nse faira que depuis le client WEB
        else {
            console.log($table);
            $id = $href.split("rmv_id_complement=");
            cont = $id[1];
            console.log(cont);
            arrayUnset($complements[$table], cont);
            $('#id_' + $table).val($complements[$table].join(","));
            console.log($('#id_' + $table).val());
            $this.parents('.ligneEntry').eq(0).remove();
        }
    })
    //Affectation des niveaux
    $(document).on('mouseover', ".rate", function () {
        // On passe les notes supérieures à l'état inactif (par défaut)
        $(this).nextAll("p").addClass("PointCompetence");
        $(this).nextAll("p").removeClass("PointCompetenceActive");
        // On passe les notes inférieures à l'état actif
        $(this).prevAll("p").addClass("PointCompetenceActive");
        // On passe la note survolée à l'état actif (par défaut)
        $(this).addClass("PointCompetenceActive");
    });
    $(document).on('mouseout', ".rating_box", function () {
        // On passe les notes supérieures à l'état inactif (par défaut)
        $(this).children("p").removeClass("PointCompetenceActive");
        // On simule (trigger) un mouseover sur la note cochée s'il y a lieu
        $(this).find("p.active").trigger("mouseover");
    });
    $(document).on("click", '.rate', function () {
        $this = $(this);
        $this.parents(".rating_box").eq(0).children('p').removeClass('active');
        $this.addClass("active");
        //Recuperer le nouveau niveau
        var niveau = $this.attr('data-level');
        //Recuperation du type et de l id à modifier
        var href = $this.parents(".ligneEntry").eq(0).find('.removeItem').attr('href');
        //Enregistrement en Ajax
        $.ajax({
            type: "POST",
            cache: false,
            url: "web/ajax/ajax.niveau.php",
            data: href + "&niveau=" + niveau + "&crypt=" + $("#crypt").val(),
            success: function (data) {
                $this.addClass('rate PointCompetenceActive');
            }
        });
    })
    loadAutocompletion();
    //Crop avatar
    $('#cropbox').Jcrop({
        //aspectRatio: 1,
        onSelect: updateCoords
    });

    function updateCoords(c) {
        $('#x').val(c.x);
        $('#y').val(c.y);
        $('#w').val(c.w);
        $('#h').val(c.h);
    };
    $("#checkCoords").bind('submit', function () {
        if (parseInt($('#w').val()))
            return true;
        alert(error_crop);
        return false;
    });
    //Home
    /*
          $activePanel = $("#accordion div.panel:first");
	  $($activePanel).addClass('active');
	  $("#accordion").delegate('.move', 'click', function(e){
	    var $id = $(this).attr('id');
                    if( ! $(this.parentElement).is('.active') ){
			      $($activePanel).animate({width: "256px"}, 300);
			      $(this.parentElement).animate({width: "512px"}, 300);
			      $('#accordion .panel').removeClass('active');
			    
			      $(this.parentElement).addClass('active');
			     
			      $activePanel = this.parentElement;
		    };
	  });
  */
    //Google map
    $("#geocomplete").geocomplete().bind("geocode:result", function (event, result) {
        $("#lo_quartier").val('');
        $("#lo_ville").val('');
        $("#lo_departement").val('');
        $("#lo_departement_short").val('');
        $("#lo_region").val('');
        $("#lo_region_short").val('');
        $("#lo_pays").val('');
        $("#lo_pays_short").val('');
        $localite = new Object();
        $.each(result.address_components, function (index, value) {
            switch (value.types[0]) {
            case 'street_number': //numero de rue
                break;
            case 'route': //rue
                break;
            case 'sublocality': //quartier
                $localite.lo_quartier = value.long_name;
                break;
            case 'locality': //department
                $localite.lo_ville = value.long_name;
                break;
            case 'administrative_area_level_2': //department
                $localite.lo_departement = value.long_name;
                $localite.lo_departement_short = value.short_name;
                break;
            case 'administrative_area_level_1': //region
                $localite.lo_region = value.long_name;
                $localite.lo_region_short = value.short_name;
                break;
            case 'country': //pays
                $localite.lo_pays = value.long_name;
                $localite.lo_pays_short = value.short_name;
                break;
            }
        })
        if ($("#lo_pays_short").length) { //SI le champs lo_ville existe dans la page on le rempli aisni que les autres
            $("#lo_quartier").val($localite.lo_quartier);
            $("#lo_ville").val($localite.lo_ville);
            $("#lo_departement").val($localite.lo_departement);
            $("#lo_departement_short").val($localite.lo_departement_short);
            $("#lo_region").val($localite.lo_region);
            $("#lo_region_short").val($localite.lo_region_short);
            $("#lo_pays").val($localite.lo_pays);
            $("#lo_pays_short").val($localite.lo_pays_short);
        } else {
            var dataString = $.toJSON($localite);
            $.post('web/ajax/geocomplete.ajax.php', {
                data: dataString
            }, function (res) {
                $("#localiteReceive").val(res);
            });
        }
    })
    /**
 * French translation for bootstrap-datepicker
 * Nico Mollet <nico.mollet@gmail.com>
 */


    //Date picker
    if ($(".date").length) {
        $('.date').datepicker();
	
    }
    //Time picker
    if ($(".timepicker").length) {
        $('.timepicker').timepicker();
    }
    //Champs wysywyg
    if ($('#redactor_content').length || $('.wysywyg').length) {
        //code
        $('#redactor_content, .wysywyg').redactor({
            buttons: ['formatting', '|', 'bold', 'italic', 'underline', 'deleted', '|', 'unorderedlist', 'orderedlist', 'outdent', 'indent', '|', 'table', 'link', '|', '|', 'alignment', '|', 'horizontalrule'],
            lang: 'fr',
            toolbarFixed: true
        });
    }
    $(".fancy-image").fancybox({
        helpers: {
            title: {
                type: 'over'
            }
        }
    });
    //Fancy show CV
    $(document).on("click", ".showCv", function (e) {
        e.preventDefault();
        $link = $(this).attr('href').split('?');
        $.ajax({
            type: "POST",
            cache: false,
            url: "web/ajax/ajax.cv.php",
            data: $link[1],
            success: function (data) {
                $.fancybox(data);
            }
        });
        return false;
    })
    $(document).on('click', '.sendFriend', function (e) {
        e.preventDefault();
        $link = $(this).attr('href');
        $('#sendFriend').modal('show');
        $(document).on('submit', '#sendFriendForm', function (e) {
            e.preventDefault();
            $.getJSON("web/ajax/ajax.sendfriend.php", $(this).serialize() + $link, function (data, status, xhr) {
                console.log(data);
                if (data.success) {
                    $('#sendFriendForm').append('<p class="padding-20">' + data.message + '</p>');
                } else {
                    $('#sendFriendForm').append('<p class="padding-20">' + data.message + '</p>');
                }
            });
        })
    })
    //Page editCV
    $("#tabCV a[href='" + location.hash + "']").tab('show');
    $(document).on('click', ".CVNav", function (event) {
        // event.preventDefault();
        location.hash = $(this).attr('href');
    })
    if ($('#linkedin').length) {
        //Linkedin
        IN.init({
            api_key: "4z8bnda0ck5f",
            onLoad: onLinkedInLoad(),
            scope: "r_fullprofile r_emailaddress r_contactinfo rw_nus",
            authorize: true
        });
    }
    // 2. Runs when the JavaScript framework is loaded

    function onLinkedInLoad() {
        IN.Event.on(IN, "auth", onLinkedInAuth);
    }
    // 3. Runs when the viewer has authenticated

    function onLinkedInAuth() {
        console.log('you are connected');
        IN.API.Profile("me").fields("positions", "languages", "skills", "educations").result(saveCv);
        IN.User.logout(autoriserSave);
    }

    function autoriserSave() {
        console.log("je deconnecte la session")
    }

    function saveCv(me) {
        $.fancybox.showLoading();
        var education = new Object();
        education.sn = me.values[0].educations;
        education.type = 'educations';
        var positions = new Object();
        positions.sn = me.values[0].positions;
        positions.type = 'positions';
        var languages = new Object();
        languages.sn = me.values[0].languages;
        languages.type = 'languages';
        var skills = new Object();
        skills.sn = me.values[0].skills;
        skills.type = 'skills';
        var file = "web/ajax/ajax.socialnetwork.php";
        $.getJSON(file, education, function (data, status, xhr) {
            if (data == 1) {
                $.getJSON(file, positions, function (data2, status, xhr) {
                    if (data2 == 1) {
                        $.getJSON(file, languages, function (data3, status, xhr) {
                            if (data3 == 1) {
                                $.getJSON(file, skills, function (data4, status, xhr) {
                                    if (data4 == 1) {
                                        $(location).attr('href', 'index.php?page=edit_cv');
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }
    //-------------------------------- VIADEO ------------------------------------------//
    if ($('#viadeo').length) {
        VD.init({
            apiKey: 'wemeggLepN',
            status: true,
            cookie: false
        });
    }
    $contentApi = null;
    /* The calls() function is called when a click event is fired on our trigger */
    $(document).on("click", "#viadeo", function () {
        /* VD.getLoginStatus() check wheter the user is logged or not and then executes our callback function. */
        VD.getLoginStatus(function (r) {
            if (!r.session) {
                VD.login(function (r) {
                    if (r.session) {
                        calls();
                    }
                });
            } else {
                calls();
            }
        });
    });

    function calls() {
        $.fancybox.showLoading();
        VD.api('/me', {
            user_detail: 'full'
        }, function (r1) {
            var file = "web/ajax/ajax.socialnetwork.php";
            var user = {
                'VD': '1',
                'headline': r1.headline,
                'introduction': r1.introduction,
                'type': r1.type
            };
            
            $.post(file, user, function (data, status, xhr) {
                if (data == 1) {
                  
                    VD.api('/me/career', {
                        limit: 12
                    }, function (r2) {
                        var career = {
                            'VD': '1',
                            'data': r2.data
                        };
                        
                        $.post(file, career, function (data2, status, xhr) {
                            if (data2 == 1) {
                                VD.api('/me/education', {
                                    limit: 12
                                }, function (r3) {
                                    var education = {
                                        'VD': '1',
                                        'data': r3.data
                                    };
                                    $.post(file, education, function (data3, status, xhr) {
                                        if (data3 == 1) {
                                            VD.api('/me/skills', {
                                                limit: 100
                                            }, function (r4) {
                                                var skills = {
                                                    'VD': '1',
                                                    'data': r4.data
                                                };
                                                $.post(file, skills, function (data4, status, xhr) {
                                                    if (data4 == 1) {
                                                        VD.api('/me/spoken_languages', {
                                                            limit: 100
                                                        }, function (r5) {
                                                            var langues = {
                                                                'VD': '1',
                                                                'data': r5.data
                                                            };
                                                            $.post(file, langues, function (data5, status, xhr) {
								
                                                                if (data5 == 1) {
                                                                     $(location).attr('href', 'index.php?page=edit_cv');
                                                                }
                                                            }, "json");
                                                        });
                                                    }
                                                }, "json");
                                            });
                                        }
                                    }, "json");
                                });
                            }
                        }, "json" );
                    });
                }
            },"json");
        });
    }

   
    //SET UP de la dialog pour le video entretien
    $(function () {
        $(".dialog").dialog({
            autoOpen: false,
            show: {
                effect: "blind",
                duration: 1000
            },
            hide: {
                effect: "explode",
                duration: 1000
            }
        });
    });
    //Lancement des entretiens conférences
    $(document).on("click", ".launchEntretien",
        function () {
            var session = $(this).attr('data-src');
            var id = $(this).attr('data-IDSession');
            //alert(session);
            $.ajax({
                type: "POST",
                cache: false,
                url: "web/ajax/ajax.entretien.php",
                data: "page=conference&action=entretien&session=" + session,
                success: function (data) {
                    $(".corps").append(data);
                    $("#video_entretien").css('width', 720)
                    $("#video_entretien").draggable({
                        handle: "p.move"
                    });
                    $(".closeCam").attr("href", self.location.href + "&end=1&idSession="+id);
                    $("#video_entretien").dialog("open");
                }
            });
        })
   
   
   
   //Home slide cote visteur
          $activePanel = $("#accordion div.panel:first");
	  $($activePanel).addClass('active');
	  $("#accordion").delegate('.move', 'click', function(e){
	    var $id = $(this).attr('id');
                    if( ! $(this.parentElement).is('.active') ){
			      $($activePanel).animate({width: "250px"}, 300);
			      $(this.parentElement).animate({width: "512px"}, 300);
			      $('#accordion .panel').removeClass('active');
			   
			      $(this.parentElement).addClass('active');
			     
			      $activePanel = this.parentElement;
		    };
	  });
   
   
    //Partie permettant de masquer la colone menuVerticale   
    if ( $('.menuVertical').length) {
    
    var $menuVerticalOpen = $('#header .span2').width();
    var $menuVerticalClose = 55;
    var $homeDroiteOpen = $(window).width() - $('#header .span2').width();
    var $homeDroiteClose = $(window).width() - $menuVerticalClose - 40;
    if ($.cookie('toggle') == 1) {
        $('.toggle').addClass('toggleClose');
        $('.menuVertical').width($menuVerticalClose);
        $('.homeDroite').width($homeDroiteClose);
    }
   
   
    $(document).on("click", ".toggle", function () {
        $(".menuVertical").animate({
            width: ($.cookie('toggle') == 1) ? $menuVerticalOpen : $menuVerticalClose
        }, {
            duration: 100,
            specialEasing: {
                width: "linear"
            },
            complete: function () {}
        });
        $(".homeDroite").animate({
            width: ($.cookie('toggle') == 1) ? $homeDroiteOpen : ($(window).width() - $menuVerticalClose)
        }, {
            duration: 100,
            specialEasing: {
                width: "linear"
            },
            complete: function () {
                if ($.cookie('toggle') == 1) {
                    $.cookie('toggle', null);
                    $('.toggle').removeClass('toggleClose');
                } else {
                    $.cookie('toggle', 1, {
                        expire: 7,
                        path: '/'
                    });
                    $('.toggle').addClass('toggleClose');
                }
            }
        });
    });
    
    }
});