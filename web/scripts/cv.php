<?PHP

require_once($_SERVER['DOCUMENT_ROOT'].'/tools/html2pdf/html2pdf.class.php');
//Initialisation de l'environnement
include($_SERVER['DOCUMENT_ROOT'].'/config/config_init.php');
//Le routing permet de selectionner le controleur en fonction de la page sur la quelle on se trouve
//Le controleur determine ensuite l'action à accomplir sur le page en fonction du $_REQUEST["action"]
include($_SERVER['DOCUMENT_ROOT'].'/config/routing.php');

switch($_SESSION['user']['id_type']){
    case DBMembre::TYPE_CLIENT:
        $environnement = 'client/';
        break;
    case DBMembre::TYPE_PRESTATAIRE :
        $environnement = 'prestataire/';
        break;
    default;
        $environnement = 'visiteur/';
        break;
}

/**
 * HTML2PDF Librairy - example
 *
 * HTML => PDF convertor
 * distributed under the LGPL License
 *
 * @author      Laurent MINGUET <webmaster@html2pdf.fr>
 *
 * isset($_GET['vuehtml']) is not mandatory
 * it allow to display the result in the HTML format
 */

    // get the HTML
    ob_start();
?>
<style type="text/css" media="all">
	
	
body, html {
    color: #333333;
    font-size: 11pt;
    margin: 0;
    padding: 0;
    text-align: justify;
    font-family: verdana;
    }

    h4, h2, p{margin: 0;padding: 0; font-weight:normal;}
    h2.red{ color: #ff786a; margin: 3mm; font-size: 15px;font-weight: 500;line-height: normal;border:none;  padding: 0; }

#pageContent{width: 200mm;}

.text-center{text-align: center;}
.blue{color:#00cccc;}
.red{color: #ff786a;}
.white{color:#FFF;}

.span4{width: 60mm;}
.span8{width: 120mm;}

.cv-entete{ }

.pull-left{float: left;}
.pull-right{float: right;}

.text-right {text-align: right;}
.clearfix{clear: both;}
.margin-left-10{margin-left: 10px;}
.margin-right-10{margin-right: 10px;}
.margin-top-10{margin-top: 10px;}
.margin-bottom-10{margin-bottom: 10px;}

.padding-10{padding: 10px!important;}
.padding-left-5mm{padding-left:3mm;}

hr{border: none; border-top:1px solid #FFF; border-bottom: 1px solid #d0d6d8;}
table.cv td{border-right:1px solid #d8d8d8;padding-right: 5px;}
table.cv td.last{border-right:none;}


</style>
<?PHP
    
    $smarty->display(_TPL_.$environnement.'pages/cv.tpl');
   
?>

<?PHP
    $content = ob_get_clean();
    
    
   $content = preg_replace('/src="web/', 'src="web', $content);
    //echo $content;
    
    // convert to PDF
    try
    {
        $html2pdf = new HTML2PDF('P', 'A4', 'fr');
        $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
        $html2pdf->Output('cv.pdf');
    }
    catch(HTML2PDF_exception $e) {
        echo $e;
        exit;
    }
