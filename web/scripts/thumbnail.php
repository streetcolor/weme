<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/core/Tools.class.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/core/myException.class.php';


$file_name_source   = $_SERVER['DOCUMENT_ROOT'].'/'.$_GET['path'];

$height = $_GET['height'] ? $_GET['height'] : 110;
$width  = $_GET['width'] ? $_GET['width'] : 150;

Tools::generateThumbnail($file_name_source, $width, $height);


