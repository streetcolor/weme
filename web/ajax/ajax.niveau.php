<?PHP

//Initialisation de l'environnement
require_once($_SERVER['DOCUMENT_ROOT'].'/config/config_init.php');

try{
	$query = Tools::decrypt($_POST['crypt']);
	parse_str($query , $output);//DBManager::debugVar($output, false);
	
	$DBmembre  = new DBMembre();
	$DBmission = new DBMission();
	
	$identifiant           = DBMembre::$id_identifiant; 
	$id_prestataire_profil = $output['id_prestataire_profil'];
	$idNiveau              = (int) $_POST['niveau'];
	$idComplement          = $_POST['rmv_id_complement'];
	
	$idNiveau = ($idNiveau < 1) && ($idNiveau > 5) ? 3 : $idNiveau;
	
	if((is_numeric($identifiant)) && (DBMembre::$id_type==DBMembre::TYPE_PRESTATAIRE) && (is_numeric($id_prestataire_profil)) && ($id_prestataire_profil>0) &&  ($idComplement>0) && ($idNiveau>0)){
		
		if($id_complement = $DBmembre->editProfil(array('id_profil_complement' =>$idComplement, 'id_prestataire_profil'=>$id_prestataire_profil, 'id_niveau'=>$idNiveau))){
			echo $id_complement;
		}
		else{
			
			echo 0;
		}
	}
	else{
			
		echo 0;
	}
	

}
catch(myException $e){
	echo $e->getMessage();
}
catch(PDOException $e){
	echo $e->getMessage();
}