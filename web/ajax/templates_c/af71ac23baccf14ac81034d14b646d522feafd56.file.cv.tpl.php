<?php /* Smarty version Smarty-3.1.12, created on 2016-01-15 22:30:11
         compiled from "/var/www/vhosts/streetcolor.com/weme.streetcolor.com/web/tpl/visiteur/pages/cv.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1454930409569964e3aa09e6-53709760%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'af71ac23baccf14ac81034d14b646d522feafd56' => 
    array (
      0 => '/var/www/vhosts/streetcolor.com/weme.streetcolor.com/web/tpl/visiteur/pages/cv.tpl',
      1 => 1425415239,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1454930409569964e3aa09e6-53709760',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'cv' => 0,
    'globale_trad' => 0,
    'cvComp' => 0,
    'competence' => 0,
    'cvExp' => 0,
    'exp' => 0,
    'cvForm' => 0,
    'form' => 0,
    'cvLang' => 0,
    'langue' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_569964e3b9c283_21730637',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_569964e3b9c283_21730637')) {function content_569964e3b9c283_21730637($_smarty_tpl) {?><div class="span12">
	 <table id="pageContent" width="100% "cellpadding="0" cellspacing=0>
	    <tr bgcolor="#333333">
		<td id="pageContent">
		    <img src="web/img/logo.png" alt="logo weme" class="pull-left margin-left-10 margin-top-10"/>
		    <p class="pull-right text-right margin-right-10 margin-top-10 white">
		      <?php echo $_smarty_tpl->tpl_vars['cv']->value->getPrenom();?>

		      <br />
		      <?php echo $_smarty_tpl->tpl_vars['cv']->value->getNom();?>
<br/>
		      <?php echo $_smarty_tpl->tpl_vars['cv']->value->getFixe();?>
 / <?php echo $_smarty_tpl->tpl_vars['cv']->value->getPortable();?>
<br/>
		      <?php echo $_smarty_tpl->tpl_vars['cv']->value->getNaissance();?>
 ans
		    </p>
		    <div class="clearfix"></div>
		 
		   <h2 class="blue text-center"><?php echo $_smarty_tpl->tpl_vars['cv']->value->getTitre();?>
</h2>
		</td>
	    </tr>
	    <tr>
		<td colspan="2" height="10"></td>
	    </tr>
	    <!-- descritpion -->
	    <?php if ($_smarty_tpl->tpl_vars['cv']->value->getDescription()!=''){?>
		
		<tr bgcolor="#333333">
		   <td colspan="2" class="padding-left-5mm ">
		       <h2 class="padding-10 red"><?php echo $_smarty_tpl->tpl_vars['globale_trad']->value->label_desc;?>
</h2>
		   </td>
	       </tr>
		<tr>
		    <td colspan="2" height="10"></td>
		</tr>
		<tr>
		    <td colspan="2">
			<p class="description"><?php echo $_smarty_tpl->tpl_vars['cv']->value->getDescription();?>
</p>
		    </td>
		</tr>
		<tr>
		    <td colspan="2" height="10"></td>
		</tr>
	    <?php }?>
	    <!-- compétences -->
	    <?php if ($_smarty_tpl->tpl_vars['cvComp']->value->count>0){?>
		<tr bgcolor="#333333">
		    <td colspan="2" class="padding-left-5mm ">
			 <h2 class="padding-10 red"><?php echo $_smarty_tpl->tpl_vars['globale_trad']->value->label_comp;?>
</h2>
		    </td>
		</tr>
		<tr>
		    <td colspan="2" height="10"></td>
		</tr>
		<tr>
		    <td colspan="2">
		       <?php  $_smarty_tpl->tpl_vars['competence'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['competence']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['cvComp']->value->competences; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['competence']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['competence']->key => $_smarty_tpl->tpl_vars['competence']->value){
$_smarty_tpl->tpl_vars['competence']->_loop = true;
 $_smarty_tpl->tpl_vars['competence']->iteration++;
?>
			   <?php echo $_smarty_tpl->tpl_vars['competence']->value->adm_competence;?>

			   <?php if ($_smarty_tpl->tpl_vars['competence']->iteration<$_smarty_tpl->tpl_vars['cvComp']->value->count){?>
			   •
			   <?php }?>
		       <?php } ?>
		   </td>
	       </tr>
		<tr>
		    <td colspan="2" height="10"></td>
		</tr>
	    <?php }?>
	    <!-- exp pro -->
	    <?php if ($_smarty_tpl->tpl_vars['cvExp']->value->count>0){?>
		<tr bgcolor="#333333">
		    <td colspan="2" class="padding-left-5mm ">
			 <h2 class="padding-10 red"><?php echo $_smarty_tpl->tpl_vars['globale_trad']->value->label_exp;?>
</h2>
		    </td>
		</tr>
		<tr>
		    <td colspan="2" height="10"></td>
		</tr>
		<?php  $_smarty_tpl->tpl_vars['exp'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['exp']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['cvExp']->value->experiences; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['exp']->key => $_smarty_tpl->tpl_vars['exp']->value){
$_smarty_tpl->tpl_vars['exp']->_loop = true;
?>
		<tr>
		   <td colspan="2" valign="top">
			
			<table  width="100% "cellpadding=0 cellspacing=0>
			   
			    <tr>
				<td valign="top" class="span4"><?php echo $_smarty_tpl->tpl_vars['exp']->value->exp_debut;?>
 - <?php echo $_smarty_tpl->tpl_vars['exp']->value->exp_fin;?>
</td>
				<td valign="top"  class="span8">
				    <p><?php echo $_smarty_tpl->tpl_vars['exp']->value->exp_entreprise;?>
</p>
				    <?php if (isset($_smarty_tpl->tpl_vars['exp']->value->exp_intitule)){?>
				    <h4 class="blue"><?php echo $_smarty_tpl->tpl_vars['exp']->value->exp_intitule;?>
</h4>
				    <?php }?>
				    <br/>
				    <?php echo $_smarty_tpl->tpl_vars['exp']->value->exp_description;?>

				</td>
			    </tr>
			    
			</table>
			
		    </td>
		</tr>
		<tr>
			<td colspan="2">
			    <hr>
			</td>
		</tr>
		<?php } ?>
		<tr>
		    <td colspan="2" height="10"></td>
		</tr>
	    <?php }?>
	    
	    <!-- formations -->
	    <?php if ($_smarty_tpl->tpl_vars['cvForm']->value->count>0){?>
		<tr bgcolor="#333333">
		    <td colspan="2" class="padding-left-5mm ">
			 <h2 class="padding-10 red"><?php echo $_smarty_tpl->tpl_vars['globale_trad']->value->label_formation;?>
</h2>
		    </td>
		</tr>
		<tr>
		    <td colspan="2" height="10"></td>
		</tr>
		<?php  $_smarty_tpl->tpl_vars['form'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['form']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['cvForm']->value->formations; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['form']->key => $_smarty_tpl->tpl_vars['form']->value){
$_smarty_tpl->tpl_vars['form']->_loop = true;
?>
		<tr>
		   <td colspan="2" valign="top">
			
			<table  width="100% "cellpadding=0 cellspacing=0>
			   
			    <tr>
				<td valign="top" class="span4"><?php echo $_smarty_tpl->tpl_vars['form']->value->form_debut;?>
 - <?php echo $_smarty_tpl->tpl_vars['form']->value->form_fin;?>
</td>
				<td valign="top"  class="span8">
				    <h4 class="blue"><?php echo $_smarty_tpl->tpl_vars['form']->value->form_diplome;?>
 <b><?php echo $_smarty_tpl->tpl_vars['form']->value->form_ecole;?>
</b></h4>
				    <b><?php echo $_smarty_tpl->tpl_vars['exp']->value->exp_entreprise;?>
<br/></b>
				    <br/>
				     <?php echo $_smarty_tpl->tpl_vars['form']->value->form_description;?>

				</td>
			    </tr>
			    
			</table>
			
		    </td>
		</tr>
		<tr>
			<td colspan="2">
			    <hr>
			</td>
		</tr>
		<?php } ?>
		<tr>
		    <td colspan="2" height="10"></td>
		</tr>
	    <?php }?>
	    
	    <!-- Langues -->
	    <?php if ($_smarty_tpl->tpl_vars['cvLang']->value->count>0){?>
		<tr bgcolor="#333333">
		    <td colspan="2" class="padding-left-5mm ">
			 <h2 class="padding-10 red"><?php echo $_smarty_tpl->tpl_vars['globale_trad']->value->label_langue;?>
</h2>
		    </td>
		</tr>
		<tr>
		    <td colspan="2" height="10"></td>
		</tr>
		<tr>
		    <td colspan="2">
	    
			<?php  $_smarty_tpl->tpl_vars['langue'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['langue']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['cvLang']->value->langues; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['langue']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['langue']->key => $_smarty_tpl->tpl_vars['langue']->value){
$_smarty_tpl->tpl_vars['langue']->_loop = true;
 $_smarty_tpl->tpl_vars['langue']->iteration++;
?>
			    <?php echo $_smarty_tpl->tpl_vars['langue']->value->adm_langue;?>

			    <?php if ($_smarty_tpl->tpl_vars['langue']->iteration<$_smarty_tpl->tpl_vars['cvLang']->value->count){?>
			    •
			    <?php }?>
			<?php } ?>
		    </td>
		</tr>
	    <?php }?>
	    
	    
	
	</table>
</div><?php }} ?>