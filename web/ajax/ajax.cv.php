<?PHP
//Initialisation de l'environnement
include($_SERVER['DOCUMENT_ROOT'].'/config/config_init.php');
//Le routing permet de selectionner le controleur en fonction de la page sur la quelle on se trouve
//Le controleur determine ensuite l'action à accomplir sur le page en fonction du $_REQUEST["action"]
include($_SERVER['DOCUMENT_ROOT'].'/config/routing.php');

$_REQUEST['search']['mots_cle'] = $_REQUEST['mots_cle'];

ob_start('Tools::replaceFlush');

$smarty->display(_TPL_.$environnement.'/pages/cv.tpl');

ob_end_flush();