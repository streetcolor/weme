<?PHP
//Initialisation de l'environnement
require_once($_SERVER['DOCUMENT_ROOT'].'/config/config_init.php');


try{
    $DBadmin = new DBAdmin();
    $table = $_GET['table']; 
    $getListSelect = $DBadmin->getListSelect($table, $_GET['term'], true);
    
    if($_GET['newentry']=="true"){//SI on envoie un newentru depuis le JS
        array_unshift($getListSelect, array('id'=>$_GET['term'], 'value'=>$_GET['term'], 'newentry'=>"true"));
    }
    echo json_encode($getListSelect);
            
}
catch(myException $e){
	echo $e->getMessage();
}
catch(PDOException $e){
	echo $e->getMessage();
}