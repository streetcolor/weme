<?PHP

require_once($_SERVER['DOCUMENT_ROOT'].'/config/config_init.php');

$DBmembre = new DBMembre;
$prestataire = $DBmembre->getListPrestataires(array('id_identifiant'=>DBMembre::$id_identifiant));


$prestataire = $prestataire['prestataires'][0];

if($prestataire->getStructure()->id_structure != DBMembre::TYPE_AGENCE){
	if($_POST['VD'] && !empty($_POST['VD'])){
			
			     
			if($_POST['type']=='USER'){
				$profil = $prestataire->getProfils()->profils;
				$profil = $profil[0];

				//$viadeo->headline
				//$viadeo->introduction
				$presentation['pf_naissance']          = $profil->getNaissance();
				$presentation['pf_siteweb']            = $profil->getSiteweb();
				$presentation['pf_fixe']               = $profil->getFixe()->numero;
				$presentation['pf_portable']           = $profil->getPortable()->numero;
				$presentation['pf_permis']             = $profil->getPermis();
				$presentation['pf_tjm']                = $profil->getTjm()->tarif;
				$presentation['pf_disponibilite']      = $profil->getDisponibilite();
				$presentation['pf_cv_titre']           = $_POST['headline'] ? $_POST['headline'] : $profil->getTitre();
				$presentation['id_prestataire_profil'] = $prestataire->getId_profil();
				$presentation['id_identifiant']        = $prestataire->getId_identifiant();
				$presentation['pf_cv_description']     = $_POST['introduction'] ? $_POST['introduction'] : $prestataire->getPresentation();
				$DBmembre->editProfil($presentation);

				echo 1;
			}
			else{
			
				foreach($_POST["data"] as $obj){
				
					if($obj['type']=='CAREER'){
						
						$experience = array();
					       
						$experience['exp_fin']               = !$obj['end'] ? Tools::shortConvertDBDateToLan(date('Y')."-".date('m')."-".date('d')) : Tools::shortConvertDBDateToLan($obj['end']."-01-01");
						$experience['exp_debut']             = Tools::shortConvertDBDateToLan($obj['begin']."-01-01");
						$experience['exp_description']       = $obj['description'] ? $obj['description'] : '-';
						$experience['exp_entreprise']        = $obj['company_name'];
						$experience['exp_intitule']          = $obj['position'];
						$experience['id_prestataire_profil'] = $prestataire->getId_profil();
						$experience['flag']                  = 'experience';
						$experience['id_identifiant']	     = $prestataire->getId_identifiant();

						$DBmembre->editProfil($experience);
												
					
					}
					elseif($obj['type']=='EDUCATION'){
						
						$formation = array();
						
						$formation['form_debut']            = !$obj['begin']? Tools::shortConvertDBDateToLan(date('Y')."-".date('m')."-".date('d')) : Tools::shortConvertDBDateToLan($obj['begin']."-01-01");
						$formation['form_description']      = $obj['comment'] ? $obj['comment'] : '-';
						$formation['form_diplome']          = $obj['school']['department'] ? $obj['school']['department']." (".$obj['degree'].")" : '-';
						$formation['form_ecole']            = $obj['school']['name'] ? $obj['school']['name'] : '-'; 
						$formation['id_prestataire_profil'] = $prestataire->getId_profil();
						$formation['flag']                  = 'formation';
						$formation['id_identifiant']	    = $prestataire->getId_identifiant();
						
						$DBmembre->editProfil($formation);
						
					}
					elseif($obj['type']=='SKILL'){

						$competence['id_competence']         = $obj['skill'];
						$competence['id_prestataire_profil'] = $prestataire->getId_profil();
						$competence['id_identifiant']	 = $prestataire->getId_identifiant();	
						$DBmembre->editProfil($competence);
					}
					elseif($obj["type"]=='SPOKEN LANGUAGE'){
						
						$langue['id_langue']             = $obj['language'];
						$langue['id_prestataire_profil'] = $prestataire->getId_profil();
						$langue['id_identifiant']	 = $prestataire->getId_identifiant();	
						$DBmembre->editProfil($langue);
					}
				}
				echo 1;
			}

	
		
	 
	}
	
	elseif($_GET){

		$_POST=true;
		switch ($_GET['type']){
		     
			case "positions" :
	
				if($_GET["sn"]["_total"]){
					 foreach($_GET["sn"]['values'] as $index => $pos_obj){
						 $experience = array();
					       
						 $experience['exp_fin']               = ($pos_obj['isCurrent'] == "true") ? Tools::shortConvertDBDateToLan(date('Y')."-".date('m')."-".date('d')) : Tools::shortConvertDBDateToLan($pos_obj['endDate']['year']."-01-01");
						 $experience['exp_debut']             = '01/01/'.$pos_obj['startDate']['year'];
						 $experience['exp_description']       = $pos_obj['summary'] ? $pos_obj['summary'] : '-';
						 $experience['exp_intitule']          = $pos_obj['title'];
						 $experience['exp_entreprise']        = $pos_obj['company']['name'];
						 $experience['id_prestataire_profil'] = $prestataire->getId_profil();
						 $experience['flag']                  = 'experience';
						 $experience['id_identifiant']	      = $prestataire->getId_identifiant();				    
						 
						
						 
						 $DBmembre->editProfil($experience);
					}

					echo 1;
					 
				}
				else
					echo 1;
				 
			break;
		     
			case "educations" :
				
				if($_GET["sn"]["_total"]){
				
					foreach($_GET["sn"]['values'] as $index => $pos_obj){
						$formation = array();
						
						$formation['form_debut']            = ($pos_obj['isCurrent'] == "true")? Tools::shortConvertDBDateToLan(date('Y')."-".date('m')."-".date('d')) : Tools::shortConvertDBDateToLan($pos_obj['startDate']['year']."-01-01");
						$formation['form_description']      = $pos_obj['fieldOfStudy'] ? $pos_obj['fieldOfStudy'] : '-';
						$formation['form_diplome']          = $pos_obj['degree'] ? $pos_obj['degree'] : '-';
						$formation['form_ecole']            = $pos_obj['schoolName'] ? $pos_obj['schoolName'] : '-'; 
						$formation['id_prestataire_profil'] = $prestataire->getId_profil();
						$formation['flag']                  = 'formation';
						$experience['id_identifiant']	    = $prestataire->getId_identifiant();	
											
						$DBmembre->editProfil($formation);

					}
					echo 1;
				}
				
				else
				    echo 1;
				
			break;
		  
			 
			case "skills" :
			     
				if($_GET["sn"]["_total"]){

					foreach($_GET["sn"]['values'] as $index => $skill_obj){
					       
						$competence['id_competence']         = $skill_obj['skill']['name'];
						$competence['id_prestataire_profil'] = $prestataire->getId_profil();
						$competence['id_identifiant']	 = $prestataire->getId_identifiant();	
						$DBmembre->editProfil($competence);
					}
					echo 1;
				}
				else
				     echo 1;
				 
			break;
		
			case "languages" :
						    
       
				if($_GET["sn"]["_total"]){
				
					foreach($_GET["sn"]['values'] as $index => $lan_obj){
						  
						$langue['id_langue']             = $lan_obj['language']['name'];
						$langue['id_prestataire_profil'] = $prestataire->getId_profil();
						$langue['id_identifiant']	     = $prestataire->getId_identifiant();	
						$DBmembre->editProfil($langue);
						 
					}
					echo 1;
				}
				else
					echo 1;
			break;
		
			default:
			break;
			 
		}
	}
}

?>