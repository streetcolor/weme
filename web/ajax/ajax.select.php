<?PHP

//Initialisation de l'environnement
require_once($_SERVER['DOCUMENT_ROOT'].'/config/config_init.php');

try{
	$query = Tools::decrypt($_POST['crypt']);
	parse_str($query , $output);//DBManager::debugVar($output, false);
	
	$DBmembre  = new DBMembre();
	$DBmission = new DBMission();
	$DBadmin   = new DBAdmin();

	$identifiant           = DBMembre::$id_identifiant; 
	$id_prestataire_profil = $output['id_prestataire_profil'];
	$id_mission            = $output['id_mission'];
	$table                 = $_POST['table'];
	$saveEntry             = $_POST['saveEntry'];
	$rmvComplement         = $_POST['rmv_id_complement'];
	$type                  = $_POST['type'] ? $_POST['type'] : 'null';
	
		
	if(is_numeric($identifiant)){
		
		//Si on accepte l'ajout des entrée depuis input en BDD dans les tables complements
		if($saveEntry){
			
			$autorize = array('competence', 'etude', 'langue', 'poste', 'categorie');
			//Ajouter une entrée en BDD
			if(in_array($table,$autorize)){

				if(DBMembre::$id_type==DBMembre::TYPE_PRESTATAIRE && ($id_prestataire_profil>0)){		
					
					if($id_complement = $DBmembre->editProfil(array('id_'.$table=>$saveEntry, 'id_prestataire_profil'=>$id_prestataire_profil, 'id_identifiant'=>$identifiant))){
						echo $id_complement;
					}
					else{
						
						echo 0;
					}
				}
				elseif(DBMembre::$id_type==DBMembre::TYPE_CLIENT && ($id_mission>0)){

					if($id_complement = $DBmission->editMission(array('id_'.$table=>$saveEntry, 'id_mission'=>$id_mission, 'ajax'=>true))){
						echo $id_complement;
					}
					else{
						
						echo 0;
					}
					
				}
				//Si on ne possede pas de id_mission ou de id_prestataire_profil et que l'on créé une mission
				elseif((DBMembre::$id_type==DBMembre::TYPE_CLIENT) || (DBMembre::$id_type==DBMembre::TYPE_PRESTATAIRE)){

					if(is_numeric($saveEntry)){
						echo $saveEntry;
					}
					else{
						if($id_complement = $DBadmin->saveComplement(array('table'=>$table, 'entry'=>$saveEntry))){
							echo $id_complement;
						}
						else{
							
							echo 0;
						}
					}
				}
			}
			
	
		}
		//Supprimer une entrée en BDD
		else{
			
			if(DBMembre::$id_type==DBMembre::TYPE_PRESTATAIRE && ($rmvComplement>0)){		
				
				if($id_complement = $DBmembre->editProfil(array('id_prestataire_profil'=>$id_prestataire_profil, 'rmv_id_profil_complement'=>$rmvComplement, 'id_identifiant'=>$identifiant, $type=>'true'))){
					echo $id_complement;
				}
				else{
					
					echo 0;
				}
			}
			elseif(DBMembre::$id_type==DBMembre::TYPE_CLIENT && ($rmvComplement>0)){
				if($id_complement = $DBmission->editMission(array('id_mission'=>$id_mission, 'rmv_id_mission_complement'=>$rmvComplement))){
					echo $id_complement;
				}
				else{
					
					echo 0;
				}
				
				
			}
		}
	}
	

}
catch(myException $e){
	echo $e->getMessage();
}
catch(PDOException $e){
	echo $e->getMessage();
}