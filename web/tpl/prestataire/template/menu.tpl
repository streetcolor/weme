<div class="span2 menuVertical">
        <div class="toggle"></div>       
        <ul class="span12 nav nav-tabs nav-stacked row-fluid">
                
            <li {if $smarty.get.page == 'home' || !$smarty.get.page}class="actif"{/if}>
                <a href="{getLink page="home"}">
                        <span class="margin-right-10 ico35 calendar pull-left"></span>
                        <span class="text">{$header_trad->menu_tableau_bord}</span>
                        <span class="clearfix"></span>
                </a>
            </li>
            <li {if $smarty.get.page == 'edit_moncompte'}class="actif"{/if}>
                <a href="{getLink page="edit_moncompte"}">
                        <span class="margin-right-10 ico35 buddy pull-left"></span>
                        <span class="text">{$header_trad->menu_mon_compte}</span>
                        <span class="clearfix"></span>
                </a>
            </li>
            <li {if $smarty.get.page == 'edit_cv'}class="actif"{/if}>
                <a href="{getLink page="edit_cv"}">
                        <span class="margin-right-10 ico35 paper pull-left"></span>
                        {if $prestataire->getStructure()->id_structure!=3}
                        <span class="text">{$header_trad->menu_mon_cv}</span>
                        {else}
                        <span class="text">Mes profils</span>
                        {/if}
                        <span class="clearfix"></span>
                </a>
            </li>
            <li {if $smarty.get.page == 'realisations'}class="actif"{/if}>
                <a href="{getLink page="realisations"}">
                        <span class="margin-right-10 ico35 eye pull-left"></span>
                        {if $prestataire->getStructure()->id_structure!=3}
                        <span class="text">{$header_trad->menu_mon_portfolio}</span>
                        {else}
                        <span class="text">Réalisations de l'agence</span>
                        {/if}
                        <span class="clearfix"></span>
                </a>
            </li>
            <li {if $smarty.get.page == 'missions'}class="actif"{/if}>
                <a href="{getLink page="missions"}">
                        <span class="margin-right-10 ico35 zoom pull-left"></span>
                        <span class="text">{$header_trad->menu_rechercher_projets}</span>
                        <span class="clearfix"></span>
                </a>
            </li>
            <li {if $smarty.get.page == 'my_missions'}class="actif"{/if}>
                <a href="{getLink page="my_missions"}">
                        <span class="margin-right-10 ico35 project pull-left"></span>
                        <span class="text">{$header_trad->menu_saved_project}</span>
                        <span class="clearfix"></span>
                </a>
            </li>
            <li {if $smarty.get.page == "new_response" or $smarty.get.page == 'responses'}class="actif"{/if}>
                <a href="{getLink page="responses"}">
                        <span class="margin-right-10 ico35 responses pull-left"></span>
                        {if $prestataire->getStructure()->id_structure!=3}
                        <span class="text">{$header_trad->menu_mes_propositions}</span>
                        {else}
                        <span class="text">Nos propositions</span>
                        {/if}
                        <span class="clearfix"></span>
                </a>
            </li>
            <li {if $smarty.get.page == 'entretiens'}class="actif"{/if}>
                <a href="{getLink page="entretiens"}">
                        <span class="margin-right-10 ico35 webcam pull-left"></span>
                        <span class="text">{$header_trad->menu_mes_entretiens}</span>
                        <span class="clearfix"></span>
                </a>
            </li>
            <li>
                <div class="text-center">
                <a href=""><img alt="twitter" src="web/img/pictoTwitter.png"/></a>
                <a href=""><img alt="facebook" src="web/img/pictoFb.png"/></a>
                <a href=""><img alt="linkedin" src="web/img/pictoLinkedin.png"/></a>
                <a href=""><img alt="googleplus" src="web/img/pictoGoogle.png"/></a>
                </div>
                <div class="clearfix"></div>
            </li>
        </ul>
    </div>