
	<tr class="MissionsListBackground">
		<td height="20" class="last" colspan="5"><a href="{getLink page='new_response' params="id_mission="|cat:$mission->getId_mission() crypt=true}" class="linkCell"></a></td>
	</tr>	
	<tr class="MissionsListBackground">

		<td class="first">
			<a href="{getLink page='new_response' params="id_mission="|cat:$mission->getId_mission() crypt=true}" class="linkCell">
			{if $mission->getNew()==0}
				<span class="flag missionS{$mission->getStatut()->id_statut}  pull-left"></span>
			{else}
				<span class="flag missionNew  pull-left"></span>
			{/if}
			
			<h4 class="blue">{$mission->getTitre()}</h4>
			<p class="padding-right-10">{$mission->getDescription()|truncate:300|@strip_tags}<span class="blue">(+)</span></p>
			</a>
		</td>

		<td class="" align="center">
			<a href="{getLink page='new_response' params="id_mission="|cat:$mission->getId_mission() crypt=true}" class="linkCell">
				<p>{$mission->getLocalite()}</p>
			</a>
		</td>

		<td class="" align="center">
			<a href="{getLink page='new_response' params="id_mission="|cat:$mission->getId_mission() crypt=true}" class="linkCell">
				<p>{$mission->getTarif(true)}</p>
		</td>

		<td class="" align="center">
			<a href="{getLink page='new_response' params="id_mission="|cat:$mission->getId_mission() crypt=true}" class="linkCell">
				<p>{$globale_trad->label_du} {$mission->getDebut()}<br/>{$globale_trad->label_au} {$mission->getFin()}</p>
				</a>
		</td>

		<td class=" last" align="center">
			<a href="{getLink page='new_response' params="id_mission="|cat:$mission->getId_mission() crypt=true}" class="linkCell">
				<p>{$mission->getReponses(true)} {$globale_trad->missionRepRecu}</p>
			</a>
		</td>

	</tr>
	
	<tr class="MissionsListBackground">
		<td height="20" class="last" colspan="5"><a href="{getLink page='new_response' params="id_mission="|cat:$mission->getId_mission() crypt=true}" class="linkCell"></a></td>
	</tr>
	
	<tr>

		<td colspan="5" class="last MissionsListActions">
			<a class='pull-left' href="{getLink page='new_response' params="id_mission="|cat:$mission->getId_mission() crypt=true}">
					<span class="ico35 detail pull-left"></span>
					<span class="pull-left">{$globale_trad->missionDetail}</span>
					
			</a>
			
			<a class='pull-left' href="{getLink page='new_response' params="id_mission="|cat:$mission->getId_mission() crypt=true}#reponseAnchor">
					<span class="ico35 detail pull-left"></span>
					<span class="pull-left">{$globale_trad->bt_reponse_offre}</span>
					
			</a>
			
			
			{if in_array($mission->getId_mission(), $favorisMission)}
			<a  class='pull-left'  href="{getLink page="missions" action="removeFavori" params="id_mission="|cat:$mission->getId_mission()|cat:"&mode="|cat:$mode|cat:"&paginate="|cat:$pageCourante crypt=true}">
					<span class="ico35 delFav pull-left"></span>
					<span class="pull-left">{$globale_trad->select_supp_fav}</span>
			</a>
			{else}
			<a   class='pull-left'  href="{getLink page="missions" action="addFavori" params="id_mission="|cat:$mission->getId_mission()|cat:"&mode="|cat:$mode|cat:"&paginate="|cat:$pageCourante crypt=true}">
					<span class="ico35 addFav pull-left"></span>
					<span class="pull-left">{$globale_trad->saveProjet}</span>
			</a>
			{/if}
			<a class="pull-left sendFriend" href="&id_mission={$mission->getId_mission()}">
					<span class="ico35 friend pull-left"></span>
					<span class="pull-left">{$globale_trad->bt_transfere}</span>
			</a>
		</td>

	</tr>
