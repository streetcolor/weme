<div id="header" class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid row-fluid">

          <div class="span2"><a class="brand" href="{getLink page="home"}"><img src="web/img/logo/logo_connected.png" alt="weme dashboard" title="weme dashboard"></a></div>
          <div class="span9">
				  <ul class="nav">
						
							  <li class="span6">
									<span class="round-count">{$smarty.session.compteur.new_missions}</span>
									<a class="{if $smarty.request.page == 'missions' }active{/if}" href="{getLink page="missions" action="search" params="id_mission="|cat:$smarty.session.compteur.match_missions crypt=true}"}">
										  <span class="ico projectIco"></span>
										  <span class="text">ProjectMatch!</span>
									</a>
							  </li>
						
							  <li class="span6">
									<span class="round-count">{$smarty.session.compteur.all_reponses}</span>
									<a class="{if $smarty.request.page == 'responses' }active{/if}" href="{getLink page="responses"}">
										<span class="ico propositionIco"></span>
										<span class="text">{$header_trad->menu_mes_propositions}</span>
									</a>
							  </li>
						
						
				  </ul>
				  <div class="span4 offset1 row-fluid headerProfil">
						<div class="pull-left">
							  <div class="padding-right-10">
									<img class="headerPhoto img-polaroid" src="{$avatar}" alt=""/>
							  </div>
						</div>
						<div class="span7 headerProfil2">
							  <h1 class="headerProfilNom">{$prestataire->getPrenom()} {$prestataire->getNom()}</h1>
									<span class="headerProfilEmail">{$prestataire->getEmail()}</span>
									<br/>
									{$prestataire->getStructure()->structure}

									<p class="">
									<a class="white" href="{getlink page='edit_moncompte'}">> {$header_trad->bt_edit_profil}</a>
							  </p>
						</div>
						<div class=" headerProfilDeco">
						<a href="{getlink page='home' action='loggout'}"><img src="web/img/headerProfilDeco.png" alt="deconnexion"/></a>
						</div>
				  </div>
          </div><!--/.nav-collapse -->
        </div>
      </div>
</div>

<div id="sendFriend" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-header">
	    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">x</button>
	    <h3 id="myModalLabel">Envoyer à un ami</h3>
      </div>  
      <div class="mediaWrapper">
      <form method="post" id="sendFriendForm" class="row-fluid">
	    <div class="padding-20">
	    <p>
		  Veuillez saisir l'addresse de votre ami et cliquez sur envoyer
	    </p>
	    <div class="span12">
		  <input class="span12 formModification" type="text" placeholder="e-mail" name="usr_email"/>
	    </div>
	    
		      <input type="hidden" name="controller" value="message">
		      <input type="hidden"  value="sendFriend" name="action"> 
			<button class="btn btn-primary margin-top-20 pull-right" type="submit">
			      <span class=" icon-pencil icon-white"></span>
			      {$globale_trad->bt_enregistrer}
			</button>
	    </div>
       </form>
      </div>
</div>