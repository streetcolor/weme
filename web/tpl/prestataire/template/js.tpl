<script type="text/javascript">
    
    var $langue = "{$smarty.session.langue}";
</script>
<!-- Le javascript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="http://cdn.viadeo.com/javascript/sdk.js"></script>
<script type="text/javascript" src="http://platform.linkedin.com/in.js?async=true"></script>
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script src="http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places&language={$smarty.session.langue}"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script src="web/js/jquery.json-2.3.min.js"></script>
<script src="web/js/jquery.geocomplete.js"></script>
<script src="web/js/jquery.Jcrop.js"></script>
<script src="web/js/bootstrap.js"></script>
<script src="web/js/bootstrap-datepicker.js"></script>
<script src="web/js/bootstrap-timepicker.min.js"></script>
<script src="web/js/redactor.js"></script>
<script src="web/js/jquery.fancybox.js"></script>
<script src="web/js/jquery.Cookie.js"></script>
<script src="web/js/jquery.Allfunctions.js"></script>


{if !empty($prestataire->getJabber()->loggin)}
    {include file=$_TOOLS_|cat:'jabber/jabber.tpl'}
    
    <script type="text/javascript">
	
	 $(document).ready(function() {
	    launchMini(true, true, "ks37615.kimsufi.com", "{$prestataire->getJabber()->loggin}", "{$prestataire->getJabber()->password}");
	});
    </script>
{/if}