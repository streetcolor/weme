<tr class="MissionsListBackground">
	<td height="20" class="last" colspan="4"><a class="linkCell" href="{getLink page="response" params="id_mission_reponse="|cat:$reponse->getId_reponse()}"></a></td>
</tr>

<tr class="MissionsListBackground">
	
	<td class="first">
		<a class="linkCell" href="{getLink page="response" params="id_mission_reponse="|cat:$reponse->getId_reponse()}">
		<h4 class="blue">{$reponse->getStatut()->statut}</h4>
		<p>
			{$reponse->getReponse()|truncate:300|@strip_tags}
			
		</p>
		</a>
		
	</td>

	<td class="" align="center">
		<a class="linkCell" href="{getLink page="response" params="id_mission_reponse="|cat:$reponse->getId_reponse()}">
			<p>{$reponse->getTarif(true)}</p>
		</a>
	</td>
	
	<td class="" align="center">
		<a class="linkCell" href="{getLink page="response" params="id_mission_reponse="|cat:$reponse->getId_reponse()}">
			<p>{$reponse->getDate()}</p>
		</a>
	</td>

	<td class=" last" align="center">
		<a class="linkCell" href="{getLink page="response" params="id_mission_reponse="|cat:$reponse->getId_reponse()}">
		<p class="padding-left-10 padding-right-10">
		{$reponse->getTitre()}
		</p>
		</a>
	</td>

	
</tr>

<tr class="MissionsListBackground">
	<td height="20" class="last" colspan="4"><a class="linkCell" href="{getLink page="response" params="id_mission_reponse="|cat:$reponse->getId_reponse()}"></a></td>
</tr>

<tr>
	<td colspan="4" class="last MissionsListActions">
		
		<a class="pull-left" href="{getLink page="response" params="id_mission_reponse="|cat:$reponse->getId_reponse()}">
			<span class="ico35 detail pull-lef"></span>
			<span class="pull-left">{$globale_trad->detail_devis}</span>
		</a>
		
		
		<a class="pull-left" href="{getLink page="responses" action='search' params="id_mission="|cat:$reponse->getId_mission()}">
			<span class="ico35 filter pull-left"></span>
			<span class="pull-left">Filtrer sur cette misison</span>
		</a>
		
		<a class="pull-left" href="{getLink page='new_response' params="id_mission="|cat:$reponse->getId_mission() crypt="true"}">
			<span class="ico35 return pull-lef"></span>
			<span class="pull-left">{$globale_trad->bt_nouvelle_prop}</span>
		</a>
	</td>
</tr>
