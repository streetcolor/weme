<div class="container row-fluid corps">
   
    {include file='web/tpl/prestataire/template/menu.tpl'}
    
    <div class="span10 homeDroite">
	
	
	<div class="span6 margin-top-20">
	    
	    {if isset($entretien)}
	    
		<form method="post" class="monCompteDroite">
		    
		    <h1 class="margin-bottom-20 ">{$trad->prestaEntretienTitre1}</h1>
		    
		    <h4>{$globale_trad->label_session}</h4>
		    
		    <div class="span6 padding-right-10">
			<div class="span6 padding-right-10">
				<div>{$globale_trad->label_date}</div>
				<div data-date-format="{$datepicker.dateFormat}" data-date="{$smarty.now|date_format:$datepicker.dateDate}" id="" class="newMission datepicker input-append date ">
					<input type="text" readonly="" disabled="disabled" value="{$entretien->getSessionTime()->date}" size="16" name="vs_date" class="formModification span12">
				</div>
			</div>
			<div class="span6">
				<div>{$globale_trad->label_heure}</div>
				<div class="input-append bootstrap-timepicker">
				  <input type="text" disabled="disabled" class="timepicker span12" data-show-meridian="false" value="{$entretien->getSessionTime()->time}">
				</div>
			</div>
			<div class="clearfix"></div>
		    </div>
		    
		    <div class="clearfix"></div>
		    
		    <div>{$trad->prestaEntretienCode}</div>
		    <div  class="span12" >
			    <input  class="span12 formModification" readonly="" type="text" value="{$entretien->getSessionGuest()->guest_access}"/>
		    </div>
		    <div class="clearfix"></div>
		    <h4>{$trad->prestaEntretienLabelHote} </h4>
		    <div class="span6 padding-right-10">
		    
			    <div class="span12">
				    {$globale_trad->label_nom}
			    </div>
			    
			    <div class="span12">
				    <input  class="span12 formModification"  name="vs_nom"  type="text"  disabled="disabled" value="{$entretien->getSessionHost()->host_nom}"  placeholder="{$globale_trad->label_nom}">
			    </div>
			    
			    <div class="clearfix"></div>
		    
		    </div>
		    
		    <div class="span6">
	    
			    <div class="span12">
				    {$globale_trad->label_prenom}
			    </div>
			    
			    <div class="span12">
				    <input class="span12 formModification" name="vs_prenom" type="text" disabled="disabled" type="text" value="{$entretien->getSessionHost()->host_prenom}"  placeholder="{$globale_trad->label_prenom}">
			    </div>
			    
			    <div class="clearfix"></div>
		    
		    </div>
		    
		    <div class="clearfix"></div>
		    
		    <div class="span12">
	    
			    <div class="span12">
				   {$globale_trad->label_societe}
			    </div>
		    
			    <div  class="span12" >
				    <input name="vs_poste" class="span12 formModification" disabled="disabled" type="text" value="{$entretien->getSessionHost()->host_entreprise}"/>
			    </div>
	    
			    <div class="clearfix"></div>
	
		    </div>
		    
		    <div class="clearfix"></div>
		    
		</form>
		
		<div class="monCompteDroite ">
		    <h1>{$globale_trad->mission}</h1>
		    <h4>{$entretien->getSessionMission()->mi_titre}</h4>
		    <hr/>
		    <p>
			    {$entretien->getSessionMission()->mi_description|truncate:300}	
		    </p>
		    {if $entretien->getSessionStatut()->id_statut == 1}
		    <a href="{getLink page=$page action="editSession" params="id_videosession="|cat:$entretien->getId_videosession()}">
		    <button class="btn btn-primary  pull-right margin-top-20" type="button">
			    <span class="icon-ok icon-white"></span>
			    {$trad->prestaEntretienBtConfirmer}
		    </button>
		    </a>
		    <div class="clearfix"></div>
		    {else if $entretien->getSessionStatut()->id_statut == 2}
		    <hr/>
		    <button class="btn btn-primary pull-right launchEntretien" type="button" data-src="{$session_name}">
			    <span class="icon-ok icon-white"></span>
			    {$globale_trad->bt_rejoindre_session}
		    </button>
		    <div class="clearfix"></div>
		    {/if}
		    <input type="hidden" name="id_videosession" value="{$entretien->getId_videosession()}" />
		</div>
		
	    {else}
	    
		<div class="monCompteDroite ">
		    
		    <p class="padding-30"></p>    
		    Vous n'avez actuellement aucun entretien vidéo en cours
		    </p>   
		    
		</div>
	    
	    {/if}
	
	</div>
		
        <div class="span6 padding-left-20 margin-top-20">
          
	    {$messageCallBack}
	    
	    <div class="monCompteDroite ">
		  <h1>{$trad->prestaEntretienTitre2}</h1>
		  <p>
		    {$trad->prestaEntretienIntro}
		  </p>
	    </div>
	    
	    <div class="monCompteDroite ">
          
		<h1>{$trad->prestaEntretienTitre3}</h1>
		
		<table width="100%" cellpadding="5" class="mission margin-bottom-20 margin-top-20">
		  
		    <thead class="MissionListHeader">
			
			<tr>
	    
			    <td align="center" class="">{$globale_trad->label_statut}</td>
						
			    <td align="center">{$trad->prestaEntretienLabelHote}</td>
			    
			    <td align="center" class=" ">{$globale_trad->label_date_heure}</td>
		
			    <td align="center" class=" last">{$globale_trad->label_action}</td>
			    
			</tr>
			
		    </thead>
			     
		    <tbody>
			
			{if $listEntretiens.count > 0 }
			
			    {foreach $listEntretiens.entretiens as $entretien}
			    
			    <tr>
				<td align="center">{$entretien->getSessionStatut()->entretien_statut}</td>
				<td align="center">{$entretien->getSessionHost()->host_nom} {$entretien->getSessionHost()->host_prenom}</td>
				<td align="center">le {$entretien->getSessionTime()->date}</td>
				<td align="center" class="last">
				    <div class="btn-group group-link ">
					<a class="btn btn-primary dropdown-toggle white"  data-toggle="dropdown"  href="#"><i class="icon-tasks icon-white"></i> {$globale_trad->label_action}</a>
					<a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
					<ul class="dropdown-menu">
						{if $entretien->getSessionStatut()->id_statut == 1}
						<li>
						    
						    <a href="{getLink page=$page action="editSession" params="id_videosession="|cat:$entretien->getId_videosession()|cat:"&vs_statut=2" crypt=true}"><i class="icon-pencil"></i> {$trad->prestaEntretienBtConfirmer}</a>
						    
						</li>
						<li>
						    
						    <a href="{getLink page=$page action="editSession" params="id_videosession="|cat:$entretien->getId_videosession()|cat:"&vs_statut=4" crypt=true}"><i class="icon-pencil"></i>Décliner</a>
						    
						</li>
    						{else if $entretien->getSessionStatut()->id_statut == 2}
						<li>
							<a class="launchEntretien" data-src="entretien_{$entretien->getId_videosession()}-{$entretien->getSessionTime()->dbdate}" >
								<span class=" icon-play-circle margin-0"></span> {$globale_trad->bt_rejoindre_session}
							</a>
						</li>
						{/if}
						<li>
							<a href="{getLink page=$page action="showDetailSession" params="id_videosession="|cat:$entretien->getId_videosession() crypt=true}"><i class="icon-pencil"></i> {$globale_trad->bt_detail}</a>
						</li>
					</ul>
				    </div>
				</td>
				    
			    </tr>
    
			    {/foreach}
			
			{else}
			
			    <tr>
				
				<td colspan="4" align="center"> Vous n'avez actuellement aucun entretien vidéo</td>
			      
			    </tr>
			    
			
			{/if}
			
		    </tbody>
		    
		</table>
          
          </div>
        </div>
    </div>
</div>