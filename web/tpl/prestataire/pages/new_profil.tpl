<div class="container row-fluid corps">
    {include file='web/tpl/prestataire/template/menu.tpl'}

    
    <div class="span10 homeDroite margin-top-20">        
        
	<form class="monCompteDroite span4" method="POST"  enctype="multipart/form-data">
            <h1>Ajouter un profil à votre compte</h1>
	                
	    {if isset($error['id_type']) || isset($error['id_client'])}
                <div class="span12">
                <div class="alert alert-error span12 margin-bottom-20">
                   {*On envoi nos variable de retour error ou success callBack*}
                   <p>{$error['id_type']}</p>
                   <p>{$error['id_client']}</p>
           
                </div>
                </div>
            {/if}
	               
	    <div class="span5 padding-right-10">
	       
		<div class="span12">
		    Civilité
		</div>
		
		<div class="span12 formModificationSelectDiv margin-bottom-10">
		    <select name="id_civilite"   class="formModificationSelect">
			{foreach $getListCivilites as $civilite}
			    <option {if {$post['id_civilite']}==$civilite->id_civilite} selected="selected"{/if} value="{$civilite->id_civilite}">{$civilite->adm_civilite}</option>
			{/foreach}
		    </select>
		</div>
		
		<div class="clearfix"></div>
		
	    </div>
	    
	    <div class="clearfix"></div>

	    {if isset($error['pf_nom']) || isset($error['pf_prenom'])}
                <div class="span12">
                <div class="alert alert-error span12 margin-bottom-20">
                   {*On envoi nos variable de retour error ou success callBack*}
                   <p>{$error['pf_nom']}</p>
                   <p>{$error['pf_prenom']}</p>
           
                </div>
                </div>
            {/if}
	    
	    <div class="span12">
		
		<div class="span12">
		    Nom
		</div>
		
		<div class="span12">
		    <input  class="span12 formModification"  name="pf_nom"  type="text"  value="{if $post['pf_nom']}{$post['pf_nom']}{/if}"  placeholder="Nom">
		</div>
		
		<div class="clearfix"></div>
		
	    </div>
	    
	    <div class="span12">
		
		<div class="span12">
		    Prenom
		</div>
		
		<div class="span12">
		    <input class="span12 formModification" name="pf_prenom" type="text" type="text" value="{if $post['pf_prenom']}{$post['pf_prenom']}{/if}"  placeholder="Prenom">
		</div>
		
		<div class="clearfix"></div>
		
	    </div>
	    
	    <div class="clearfix"></div>
	
	    
	    <button value="addProfil" name="action" class="btn btn-primary margin-top-20 pull-right" type="submit">
		<span class=" icon-pencil icon-white"></span>
		Enregistrer le profil
	    </button>
	    
	    <div class="clearfix"></div>

	</form>
	
	<div class="span8  pull-right padding-left-20">
	    
	    {$messageCallBack}
	    
	    <div class="monCompteDroite ">
	    
		<h1>Liste de vos profils</h1>
		
		<p>Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l'imprimerie depuis les années 1500, quand un peintre anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n'a pas fait que survivre cinq siècles, mais s'est aussi adapté à la bureautique informatique, sans que son contenu n'en soit modifié. Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker. </p>
		
	    </div>
	    
	    <div class="monCompteDroite">
	    <table width="100%" cellpadding="5" class=" mission">
		    <thead class="MissionListHeader">
			<tr>
		     
			    <td class="">Nom</td>
	    
			    <td align="center" class="">Prenom</td>
    
			    <td align="center" class="">Téléphone</td>
			    
			    <td align="center" class="">Disponibilité</td>
			    			    
			    <td align="center" class="last" width="75">Action</td>
				
			</tr>
		    </thead>
		
		    <tbody>
			{if isset($count)}
			    {foreach $listProfils as $profil}           
				<tr style="border-top:1px solid #D8D8D8">

				    
				    <td class="">{$profil->getNom()}</td>
		    
				    <td align="center" class="">{$profil->getPrenom()}</td>
	    
				    <td align="" class="">
					Fixe : {$profil->getFixe(true)}
					<br>
					Port : {$profil->getPortable(true)}
				    </td>
				    
				    <td align="" class="">
					 {$profil->getDisponibilite()}
				    </td>
				    <td align="center" class="last">
			
				    			    
					    <div class="btn-group group-link ">
						<a class="btn btn-primary dropdown-toggle white" href="#" data-toggle="dropdown"><i class="icon-tasks icon-white"></i> Actions</a>
						<a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
						<ul class="dropdown-menu">
						    <li><a href="{getLink page="edit_cv" params="id_prestataire_profil="|cat:$profil->getId_profil() crypt=true}"><i class="icon-pencil"></i> Modifier</a></li>
						    {if $profil->getActif()}
						    <li><a href="{getLink page=$page  action="hideProfil" params="id_prestataire_profil="|cat:$profil->getId_profil() crypt=true}"><i class="icon-eye-open "></i> Désactiver</a></li>
						    {else}
						    <li><a href="{getLink page=$page  action="showProfil" params="id_prestataire_profil="|cat:$profil->getId_profil() crypt=true}"><i class="icon-eye-close "></i> Activer</a></li>
						    {/if}
						</ul>
					    </div>
		    
					
				    </td>
					
				</tr>
				   
			    {/foreach}
			{else}
			    <tr>
				<td colspan="4" class=" Aucune profil de diponible  "></td>
			    </tr>
				
			{/if}
		    </tbody>
		
		
		</table>
	    
	    </div>
	</div>
    </div>
</div>
<div class="clearfix"></div>