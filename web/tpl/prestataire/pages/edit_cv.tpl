<div class="container-fluid row-fluid corps">
	{include file='web/tpl/prestataire/template/menu.tpl'}
	
	<div class="span10 homeDroite">
	
	    <div class="CVHeader span12 margin-bottom-20">
		
		<div class="pull-left CVHeaderPourcent margin-right-10">
			 {$profil->getPourcentage()}%
		</div>
		
		<div class="span3 CVHeaderBar blue">
			 {$trad->prestaCvCompleteprc}
			<div class="progress">
				<div class="bar" style="width: {$profil->getPourcentage()}%;"></div>
			</div>
		</div>
		
		<span id="tooltipGris" rel="tooltip" data-placement="bottom" data-original-title="<div>
		    <h1>Comment optimiser son CV</h1>
		    <hr/>
		    <ul class='tooltipList'>
			<li><img src='web/img/picto-tooltip_profil.png' class='floatLeft margin-right-5' alt='profil'/><span>pour plus de visibilit&eacute;, pensez &agrave; renseigner votre profil WeMe !</span></li><span class='clearfix'/>
			<li><img src='web/img/picto-tooltip_voiture.png' class='floatLeft margin-right-5' alt='voiture'/><span class='padding-top-10' style='display: inline-block;'>Avez vous le permis de conduire ?</span></li><span class='clearfix'/>
			<li><img src='web/img/picto-tooltip_mobile.png' class='floatLeft margin-right-5' alt='mobile'/><span class='padding-top-10' style='display: inline-block;'>Pr&eacute;cisez un num&eacute;ro de t&eacute;l&eacute;phone portable</span></li><span class='clearfix'/>
		    </ul>
		    <div class='tooltipInner'>
			<img src='web/img/picto-tooltip_interro.png' class='margin-right-5 floatLeft' alt='question'/><span class='padding-top-10'>Un probl&egrave;me technique ? Une question d'ordre administratif ou commercial ? <br/>CONTACTEZ NOUS</span><span class='clearfix'/>
		    </div>
	    </div>" class="ico35 question pull-left margin-top-30 margin-right-10">&nbsp;</span>
		
		<div class="padding-right-10 border pull-left">
			<a href="#downloadCV" role="button" data-toggle="modal" class="">
			<button class="btn btn-primary" type="submit">
			<span class=" icon-download icon-white"></span> {$trad->ajouter_cv} </button>
			</a>
		</div>
		
		<form method="post" class="border pull-left" action="{getLink page=$page action="" params="crypt="|cat:$smarty.get.crypt}">
			
			<button  rel="tooltip"  data-original-title="Si vous souhaitez que votre cv <br> PDF apparaissent dans la CVTeque"  class="btn btn-primary" type="submit" name="pf_active_pdf" value="{if $profil->getActivePDF()}0{else}1{/if}" >
			<span class=" icon-eye-{if $profil->getActivePDF()}open{else}close{/if} icon-white"></span> {if $profil->getActivePDF()}Desactiver PDF{else}Activer PDF{/if}
			</button>
			
		</form>
		
		{if !isset($agence)}
		    
		    <div class="  pull-left margin-top-20 margin-left-20">
			<div class="blue padding-right-10">{$trad->cv_complete_linked}</div>
			
			<div class="pull-left">
				<script id="linkedin" data-size="medium" type="IN/Login" data-label="linkedin" data-themeColor="#158585"></script>
			</div>
			
			<div class="pull-left">
				<img id="viadeo" src="web/img/pictoViadeoConnection.png" alt="linkedin" class=""/>
			</div>
		    
			<div class="clearfix"></div>
		    </div>
		    
		{/if}
		    
		<!-- modale dl CV -->
		<div id="downloadCV" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		    
		    <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
			<h3 id="myModalLabel">{$trad->prestaImporterCvTitre}</h3>
		    </div>
		    
		    <div class="mediaWrapper">
			
			<div class="row-fluid">
			    <div class="span4">
				<div class="padding-10">
					{$trad->prestaImporterCvRules}
				</div>
			    </div>
			    
			    <div class="span3 padding-5">
				<img src="{$cv->getThumbnail()}" alt="" title="">
			    </div>
			    
			    <form enctype="multipart/form-data" method="post" class=" imgdetails pull-right span5">
				
				<p>
				    <label>{$trad->votre_cv}</label>
				    <div class="span12 formModification">
					    <input type="file" value="votre CV" class="" name="file">
				    </div>
				</p>
				
				<div class="margin-bottom-0">
				    <button class="btn btn-primary margin-top-20 pull-right" type="submit"><span class="icon-ok icon-white"></span> Telecharger</button>
				    <input type="hidden" value="membre" name="controller">
				    <input type="hidden" value="addCv" name="action">
				    <input type="hidden" value="cv" name="file_type">
				    <div class="clearfix"></div>
				</div>
				
			    </form>
			    <!--span3-->
			    <div class="clearfix"></div>
			</div>
		    
		    </div>
		</div>
		<!-- fin modale CV -->
		<div class="clearfix"></div>
	</div>
	
	{if isset($error['file']) || isset($error['file_cv'])}
	<div class="span12 alert alert-error " id="anchor-file_cv">
		 {$error['file']} {$error['file_cv']}
	</div>
	{/if}
	
	<div class="CV">
	    
	    <ul class="nav nav-tabs CVTab span8" id="tabCV">
		<li class="active span3">
		    <a href="#idendite" class="scroller CVNav" data-toggle="tab">
			<img class=" margin-right-10" src="web/img/pictoIdendite.png" alt=""/>
			<span class="libelle">{if !isset($agence)}Mon identité{else}Identité{/if}
			<!--{$trad->infos_persos}-->
			</span>
		    </a>
		</li>
		<li class="span3">
		    <a href="#competences" class="scroller CVNav" data-toggle="tab">
			<img class=" margin-right-10" src="web/img/pictoCompetence.png" alt=""/>
			<span class="libelle">{$trad->comp}</span>
		    </a>
		</li>
		<li class="span3">
		    <a href="#experiences" class="scroller CVNav" data-toggle="tab">
			<img class=" margin-right-10" src="web/img/pictoExperience.png" alt=""/>
			<span class="libelle">{$trad->exp_pro_short}</span>
		    </a>
		</li>
		<li class="span3">
		    <a href="#formations" class="scroller CVNav" data-toggle="tab">
			<img class=" margin-right-10" src="web/img/pictoFormation.png" alt=""/>
			<span class="libelle">{$trad->form}</span>
		    </a>
		</li>
	    </ul>
	    
	    <div class="tab-content span9">
		<!-- PAGE 1 Mon identité-->
		<div class="tab-pane CVTabPane active" id="idendite">
		   
		    <h1 class="span12 CVTitre">{if !isset($agence)}{$trad->infos_persosTitre} {else}Informations personelles{/if}</h1>
		    
		    <h2>{$globale_trad->label_etat_civil}</h2>
		    
		    <form name="" method="POST" class="inline-form" action="{getLink page=$page action="" params="crypt="|cat:$smarty.get.crypt}">
			<!-- debut etat civil -->
			
			{if isset($error['pf_nom']) || isset($error['pf_prenom'])}
			    <div class="span12 alert alert-error ">
				<p id="anchor-pf_nom">{$error['pf_nom']}</p>
				<p  id="anchor-pf_prenom">{$error['pf_prenom']}</p>
			    </div>
			{/if}
			<div class="span12 margin-bottom-10">
			    <div class="span2  padding-right-10">
				<div class="span12">
				    {$globale_trad->label_civilite}
				</div>
				<div class="span12 styled-select margin-bottom-10">
				    <select name="id_civilite" disabled="disabled" class="">
					{foreach $getListCivilites as $civilite}
					<option {if $profil->getCivilite()->id_civilite==$civilite->id_civilite} selected="selected"{/if} value="{$civilite->id_civilite}">{$civilite->adm_civilite}</option>
					{/foreach}
				    </select>
				</div>
			    </div>
			    <div class="span10">
				<div class="span6  padding-right-10">
				    <div class="span12">
					    {$globale_trad->label_nom}
				    </div>
				    <input name="pf_nom" class="span12 formModification" disabled type="text" value="{$profil->getNom()}" placeholder="{$globale_trad->label_nom}"/>
				</div>
				<div class="span6">
				    <div class="span12">
					    {$globale_trad->label_prenom}
				    </div>
				    <input name="pf_prenom" class="span12 formModification" disabled type="text" value="{$profil->getPrenom()}" placeholder="{$globale_trad->label_prenom}"/>
				</div>
			    </div>
			    
			    <div class="span2 padding-right-10">
				<div class="span12">
					{$globale_trad->label_permis}
				</div>
				<div class="span12 styled-select">
				    <select name="pf_permis" class="" placeholder="{$globale_trad->label_permis}">
					{foreach from=$getListPermis key=k item=v}
					    <option {if $profil->getPermis()=={$k}} selected="selected"{/if} value="{$k}">{$v}</option>
					{/foreach}
				    </select>
				</div>
			    </div>
			    
			    <div class="span10">
				<div class="span12">
					{$globale_trad->label_age}
				</div>
				
				<div class="span2">
				    <input name="pf_naissance" class="span12 formModification" type="text" value="{if $post['pf_naissance']}{$post['pf_naissance']}{else}{$profil->getNaissance()}{/if}" placeholder="{$globale_trad->label_age}"/>
				</div>
			    
				{if isset($error['pf_naissance'])}
				    <div class="span10 padding-left-10">
					<div class="span12 alert alert-error " >
					    <p id="anchor-pf_naissance">{$error['pf_naissance']}</p>
					</div>
				    </div>
				{/if}
			    </div>
			</div>
			<!-- fin etat civil -->
			<!-- debut contact -->
			<h2>{$globale_trad->label_contact}</h2>
			
			<div class="span4 padding-right-10">
			    <div class="span12">
				    {$globale_trad->label_site}
			    </div>
			    <input name="pf_siteweb" class="span12 formModification" type="text" value="{if $post['pf_siteweb']}{$post['pf_siteweb']}{else}{$profil->getSiteweb()}{/if}" placeholder="{$globale_trad->label_site}"/>
			</div>
			
			<div class="span4 padding-right-10">
			    <div class="span12">{$globale_trad->label_fixe}</div>
			    <div class="span12">
				    <div class="span2 ">
					    <input maxlength="6" name="pf_fixe_indicatif" class="span12 formModification" type="text" value="{if $post['pf_fixe_indicatif']}{$post['pf_fixe_indicatif']}{else}{$profil->getFixe()->indicatif}{/if}"/>
				    </div>
				    <div class="span10">
					    <input maxlength="10" name="pf_fixe" class="span12 formModification" type="text" value="{if $post['pf_fixe']}{$post['pf_fixe']}{else}{$profil->getFixe()->numero}{/if}"/>
				    </div>
				    <div class="clearfix">
				    </div>
			    </div>
			    <div class="clearfix">
			    </div>
			</div>
			
			<div class="span4">
			    <div class="span12">{$globale_trad->label_portable}</div>
			    <div class="span12">
				    <div class="span2">
					    <input maxlength="6" name="pf_portable_indicatif" class="span12 formModification" type="text" value="{if $post['pf_portable_indicatif']}{$post['pf_portable_indicatif']}{else}{$profil->getPortable()->indicatif}{/if}"/>
				    </div>
				    <div class="span10">
					    <input maxlength="10" name="pf_portable" class="span12 formModification" type="text" value="{if $post['pf_portable']}{$post['pf_portable']}{else}{$profil->getPortable()->numero}{/if}"/>
				    </div>
				    <div class="clearfix">
				    </div>
			    </div>
			    <div class="clearfix">
			    </div>
			</div>
			<!-- fin contact -->
			<!-- Situation professionnelle -->
			<h2 class="span12 compteTitleModificationh2 margin-bottom-20">{$trad->prestaCvSituationPro}</h2>
			
			<div class="span12">
				{$globale_trad->label_titre_cv}
			</div>
			
			<div class="span12">
			    <input name="pf_cv_titre" class="span12 formModification" type="text" value="{if $post['pf_cv_titre']}{$post['pf_cv_titre']}{else}{$profil->getTitre()}{/if}" placeholder="{$globale_trad->label_titre_cv}"/>
			</div>
			
			<div class="span12">
				{$trad->prestaDescProfil}
			</div>
			
			<div class="span12">
			    <textarea name="pf_cv_description" class="span12 wysywyg">{if $post['pf_cv_description']}{$post['pf_cv_description']}{else}{$profil->getDescription()}{/if}</textarea>
			</div>
			
			<div class="span12 margin-top-10">
			    
			    <div class="span4 padding-right-10">
				
				<div class="span12">
					{$globale_trad->label_tjm}
				</div>
				
				<div class="span8 ">
					<input name="pf_tjm" class=" span12 formModification" type="text" value="{if $post['pf_tjm']}{$post['pf_tjm']}{else}{$profil->getTjm()->tarif}{/if}" placeholder="Tarif"/>
				</div>
				
				<div class="styled-select span4">
				    <select class="" placeholder="Monnaie" name="id_monnaie">
					{foreach $getListMonnaies as $monnaie}
					<option {if $monnaie->id_monnaie==$profil->getTjm()->id_monnaie} selected="selected"{/if} value="{$monnaie->id_monnaie}">{$monnaie->adm_monnaie}</option>
					{/foreach}
				    </select>
				</div>
				
			    </div>
			    
			    <div class="span4">

				<div class="span12">
				    {$globale_trad->label_dispo}
				</div>
				<div data-date-format="{$datepicker.dateFormat}" data-date="{$smarty.now|date_format:$datepicker.dateDate}" id="" class="span12 input-append date">
				    <input class="span12" type="text" readonly="" value="{if $post['pf_disponibilite']}{$post['pf_disponibilite']}{else}{$profil->getDisponibilite()}{/if}" size="16" name="pf_disponibilite">
				    <span class="add-on"><i class="icon-calendar"></i></span>
				</div>
				
			    </div>
			    
			    {if isset($error['pf_tjm']) || isset($error['pf_disponibilite'])}
				<div class="span4 padding-left-10"></div>
				<div class="span4">
				    <div class="span12 alert alert-error ">
					<p  id="anchor-pf_tjm">{$error['pf_tjm']}</p>
					<p  id="anchor-pf_disponibilite">{$error['pf_disponibilite']}</p>
				    </div>
				</div>
			    {/if}
			    
			    <div class="clearfix"></div>
			    
			</div>
			<!-- fin situation professionnelle -->
			<div class="pull-right monCompteButton">
				<button class="btn btn-primary" type="submit">
				    <span class=" icon-pencil icon-white"></span>
				    {$globale_trad->bt_enregistrer}
				</button>
			</div>
			
			<div class="clearfix"></div>
			    
		    </form>
			
			
		
		    <form class="span6 inline-form">
			
			<h2 class="span12 compteTitleModificationh2">{$globale_trad->label_poste}</h2>
			
			<div class="span12">
			    
			    <div class="input-append span10">
				<input class="margin-0 autocompletion entryNew span12 " type="text" name="" id="poste" placeholder="{$globale_trad->label_add_poste}"/> <input type="hidden" name="id_poste" id="id_poste">
				<a id="posteButton" class="manual-insert btn btn-third">
				    <span class=" icon-plus icon-white margin-0"></span>
				</a>
			    </div>
			    
			    <div class="clearfix"></div>
			    
			    <div id="result_poste" class="result">
				{foreach $profil->getPostes()->postes as $poste}
				<div class="span12 listeCompetence ligneEntry">
				    <div class=" listeCompetence ligneEntry">
					    <a href="?page=edit_profil&type=id_poste&action=editProfil&rmv_id_complement={$poste->id_profil_complement}" class="removeItem ico35"></a>{$poste->adm_poste}
				    </div>
				</div>
				{/foreach}
			    </div>
			    
			</div>
			
		    </form>

		    <form method="post" class="span6 inline-form">
			<h2 class="span12 compteTitleModificationh2">{$globale_trad->label_mob}</h2>
			<div class="span12">
				<div class="input-append span10">
					<input type="text" id="geocomplete" class="span12 margin-0" value="{$post['lo_quartier']} {$post['lo_ville']} {$post['lo_region']} {$post['lo_pays']}">
					<input type="hidden" name="lo_quartier" id="lo_quartier" value="{$post['lo_quartier']}">
					<input type="hidden" name="lo_ville" id="lo_ville" value="{$post['lo_ville']}">
					<input type="hidden" name="lo_departement" id="lo_departement" value="{$post['lo_departement']}">
					<input type="hidden" name="lo_departement_short" id="lo_departement_short" value="{$post['lo_departement_short']}">
					<input type="hidden" name="lo_region" id="lo_region" value="{$post['lo_region']}">
					<input type="hidden" name="lo_region_short" id="lo_region_short" value="{$post['lo_region_short']}">
					<input type="hidden" name="lo_pays" id="lo_pays" value="{$post['lo_pays']}">
					<input type="hidden" name="lo_pays_short" id="lo_pays_short" value="{$post['lo_pays_short']}">
					<input type="hidden" value="localite" name="flag">
					<button class="manual-insert btn btn-third" type="submit">
					<span class=" margin-0 icon-plus icon-white "></span>
					</button>
				</div>
			</div>
			<div class="clearfix">
			</div>
			<div id="" class="result">
				 {if $localites->count >0} {foreach $localites->localites as $localite}
				<div class="span12 listeCompetence ligneEntry">
					<div class=" listeCompetence ligneEntry">
							<a href="?page=edit_profil&action=editProfil&rmv_id_profil_localite={$localite->getLocalite()->id_profil_localite}"class="delEntry removeLoc ico35"></a>
						
							{$localite}
					</div>
				</div>
				 {/foreach} {/if}
			</div>
		    </form>
	    
		</div>
		<!--FIN mon ientité-->
		<!--PAGE 2 competences-->
		<div class="tab-pane CVTabPane" id="competences">
    
    		    <h1 class="span12 CVTitre">{if !isset($agence)}{$trad->compTitre} {else}Competences {/if}</h1>
		    
		    <h2 class="span12">{$trad->comp}</h2>

		    <form class="inline-form">
			
			<div class="input-append span6">
				<input class="autocompletion entryNew entryEval margin-0 span12" type="text" name="" id="competence" placeholder="{$globale_trad->add_comp}"/> <input type="hidden" name="id_competence" id="id_competence">
				<a id="competenceButton" class="manual-insert btn btn-third">
				    <span class=" icon-plus icon-white margin-0"></span>
				</a>
			</div>
			
			<div class="clearfix"></div>
			
			<div id="result_competence" class="result margin-top-10">
			    {foreach $profil->getCompetences()->competences as $competence}
				<div class="span12 listeCompetence ligneEntry">
					
				    <a class="ico35 removeItem" href="?page=edit_profil&action=editProfil&type=id_competence&rmv_id_complement={$competence->id_profil_complement}"></a>
				    {$competence->adm_competence}
				    
				    <div class="pull-right span6">
					<div class="span6">
						 {$competence->adm_niveau}
					</div>
					<div class="span6 GraphNiveauCompetence">
						<div class="rating_box ">
							<div class="span1" style="position:relative; top: -10px">
								<a href="#">-</a>
							</div>
							 {for $i=1 to 5}
							<p data-level="{$i}" class="rate {if $i==$competence->
								id_niveau}active{/if} {if $i<=$competence->id_niveau}PointCompetenceActive{else}PointCompetence{/if}" style="float:left; margin-left: 5px;">
							</p>
							 {/for}
							<div class="span1 margin-left-5" style="position:relative; top: -10px">
								<a href="#">+</a>
							</div>
						</div>
					</div>
				    </div>
				    
				</div>
				
				<div class="clearfix"></div>
			    {/foreach}
			</div>
			<div class="clearfix"></div>
			
		    </form>
		    <hr class="pointille"/>
		    <form class="inline-form">
			
			<h2 class="span12 ">{$trad->langues}</h2>
			
			<div class="input-append span6">
			    <input class="autocompletion entryNew entryEval span12 margin-0" type="text" name="" id="langue" placeholder="{$globale_trad->add_langue}"/> <input type="hidden" name="id_langue" id="id_langue">
			    
			    <a id="langueButton" class="manual-insert btn btn-third">
				<span class=" icon-plus icon-white margin-0"></span>
			    </a>
			    
			</div>
			
			<div class="clearfix"></div>
			   
			<div id="result_langue" class="result margin-top-10">
			    
			    {foreach $profil->getLangues()->langues as $langue}
			    
				<div class="span12 listeCompetence ligneEntry">
				   
				    <a class="ico35 removeItem" href="?page=edit_profil&type=id_langue&action=editProfil&rmv_id_complement={$langue->id_profil_complement}"></a>
				    {$langue->adm_langue}
				    
				    <div class="pull-right span6">
					
					<div class="span6">
						 {$langue->adm_niveau}
					</div>
					
					<div class="span6 GraphNiveauCompetence">
						<div class="rating_box ">
							<div class="span1" style="position:relative; top: -10px">
								<a href="#">-</a>
							</div>
							 {for $i=1 to 5}
							<p data-level="{$i}" class="rate {if $i==$langue->
								id_niveau}active{/if} {if $i<=$langue->id_niveau}PointCompetenceActive{else}PointCompetence{/if}" style="float:left; margin-left: 5px;">
							</p>
							 {/for}
							<div class="span1 margin-left-5" style="position:relative; top: -10px">
								<a href="#">+</a>
							</div>
						</div>
					</div>
					
				    </div>	
					
				</div>
			    {/foreach}
			    
			</div>
			
		    </form>
		    
		</div>
		<!--FIN competences-->
		<!-- PAGE 3 experience-->
		<div class="tab-pane CVTabPane" id="experiences">

		    <h1 class="span12 CVTitre">{if !isset($agence)}{$trad->exp_proTitre} {else}Experiences personelles{/if}</h1>

		    <h2 class="span12">{if $smarty.get.id_profil_experience}{$trad->edit_exp_pro}{else}{$trad->add_exp_pro}{/if}</h2>

		    <form class="inline-form" method="post">
			
			<div class="span12">
			    
			    <div class="span6">
				
				<div class="span6 padding-right-10">
					
					<div class="span12">
						{$globale_trad->label_debut}<em class="red">*</em>
					</div>
					<div data-date-format="dd/mm/yyyy" data-date="{if $post['exp_debut']}{$post['exp_debut']}{else}{if $editExperience->exp_debut}{$editExperience->exp_debut}{else}{$smarty.now|date_format:'%d/%m/%Y'}{/if}{/if}" id="" class="span12 input-append date">
					    <input type="text" readonly="" value="{if $post['exp_debut']}{$post['exp_debut']}{else}{$editExperience->exp_debut}{/if}" size="16" name="exp_debut" class="span12">
					    <span class="add-on"><i class="icon-calendar"></i></span>
					</div>
				
				</div>
				
				<div class="span6">

					<div class="span12">
						{$globale_trad->label_fin}<em class="red">*</em>
					</div>
					<div data-date-format="dd/mm/yyyy" data-date="{if $post['exp_fin']}{$post['exp_fin']}{else}{if $editExperience->exp_fin}{$editExperience->exp_fin}{else}{$smarty.now|date_format:'%d/%m/%Y'}{/if}{/if}" id="" class="span12 input-append date">
					    <input type="text" readonly="" value="{if $post['exp_fin']}{$post['exp_fin']}{else}{$editExperience->exp_fin}{/if}" size="16" name="exp_fin" class="span12">
					    <span class="add-on"><i class="icon-calendar"></i></span>
					</div>
				
				</div>
				
			    </div>
			    
			    {if isset($error['exp_debut']) || isset($error['exp_fin'])}
				<div class="span6 padding-left-10">
				    <div class="span12"></div>
				    <div class="span12 alert alert-error ">
					<p id="anchor-exp_debut">{$error['exp_debut']}</p>
					<p id="anchor-exp_fin">{$error['exp_fin']}</p>
				    </div>
				</div>
			    {/if}
			
			</div>
			
			<div class="span12">
			    <div class="span12">
				    {$globale_trad->label_societe}<em class="red">*</em>
			    </div>
			    <div class="span6">
				<input name="exp_entreprise" class="span12 formModification" type="text" value="{if $post['exp_entreprise']}{$post['exp_entreprise']}{else}{$editExperience->exp_entreprise}{/if}"/>
			    </div>
			    {if isset($error['exp_entreprise'])}
				<div class="span6 padding-left-10">
				    <div class="span12 alert alert-error ">
					<p id="anchor-exp_entreprise">{$error['exp_entreprise']}</p>
				    </div>
				</div>
			    {/if}
			</div>
			<div class="span12">

			    <div class="span12">
				    {$globale_trad->label_poste}<em class="red">*</em>
			    </div>
			    <div class="span6">
				<input name="exp_intitule" class="span12 formModification" type="text" value="{if $smarty.request['exp_intitule']}{$post['exp_intitule']}{else}{$editExperience->exp_intitule}{/if}"/>
			    </div>
			    {if isset($error['exp_intitule'])}
				<div class="span6 padding-left-10">
				    <div class="span12 alert alert-error ">
					<p id="anchor-exp_intitule">{$error['exp_intitule']}</p>
				    </div>
				</div>
			    {/if}
			</div>
			
			<div class="span12">
				
				<div class="span12">
					{$globale_trad->desc_poste}<em class="red">*</em>
				</div>
				
				{if isset($error['exp_description'])}
				    <div class="span12">
					<div class="span12 alert alert-error ">
					    <p id="anchor-exp_description">{$error['exp_description']}</p>
					</div>
				    </div>
				{/if}
				
				<div class="span12">
				    <textarea name="exp_description" class="span11 wysywyg formModificationTxtArea">{if $post['exp_description']}{$post['exp_description']}{else}{$editExperience->exp_description}{/if}{$editexperience->exp_intitule}</textarea>
				</div>
			</div>
			<div class="monCompteButton pull-right margin-top-10">
				<input type="hidden" value="experience" name="flag">
				<button class="btn btn-primary" type="submit">
				{if $smarty.get.id_profil_experience} <span class=" icon-pencil icon-white"></span>
				{$globale_trad->bt_modifier} {else} <span class=" icon-pencil icon-white"></span>
				{$globale_trad->bt_ajouter} {/if} </button>
			</div>
			<div class="clearfix"></div>
		    </form>
		    
		    <hr class="pointille"/>
			    
		    <h2>{$trad->exp_proTitre}</h2>
			
		    {if $listExperiences->count > 0}
		    
			{foreach $listExperiences->experiences as $experience}
			    <div class="pull-right">
			    
				<div class="btn-group group-link margin-bottom-5">
				    <a class="btn btn-primary dropdown-toggle white" href="#" data-toggle="dropdown"><i class="icon-tasks icon-white"></i> Actions</a>
				    <a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
				    <ul class="dropdown-menu">
					<li><a href="?page=edit_cv&id_profil_experience={$experience->id_profil_experience}&crypt={$smarty.get.crypt}#experiences"><i class="icon-pencil"></i> Modifier</a></li>
					<li><a class="delEntry" href="?page=edit_cv&rmv_id_profil_experience={$experience->id_profil_experience}{if isset($smarty.get.crypt)}&crypt={$smarty.get.crypt}{/if}#experiences"><i class="icon-trash"></i> Supprimer</a></li>
					<li><a href="#expDetails" role="button" data-toggle="modal" data-expid="{$experience->id_profil_experience}" class="showExp"><i class="icon-eye-open"></i>Affficher</a></li>
				    </ul>
				</div>
	
			    </div>
			    
			    <div class="clearfix"></div>
			    
			    <div class=" listFormation">
    
				    <div class="padding-10">
			
					    <p>{$experience->exp_entreprise} - {$experience->exp_debut} - {$experience->exp_fin}</p>
					    <h2>{$experience->exp_intitule}</h2>
					    <div>
						{$experience->exp_description|truncate:220|@strip_tags}
					    </div>
				    </div>
				    <div class="clearfix"></div>
			    </div>
			    			    
			{/foreach} 
			<!-- modal pour le detail des experience -->
			<div id="expDetails" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			    
			    <div class="modal-header">
				    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				    <h3 class="intitule">{$experience->exp_entreprise} - {$experience->exp_debut} - {$experience->exp_fin}</h3>
			    </div>
			    <div class="mediaWrapper padding-10">
				    
				    <div class="description">
					 <h2>{$experience->exp_intitule}</h2>
					{$experience->exp_description}
				    </div>
			    </div>
			    
			</div>
			<!-- fin modal detail experience -->
			
		    {else}
		    
			<div class="padding-10 text-center">Aucune experience</div>
		    
		    {/if}
    		</div>
		<!--FIN experience-->
		<!--PAGE 4 formation-->
		<div class="tab-pane CVTabPane" id="formations">
		  
		    <h1 class="span12 CVTitre">{if !isset($agence)}{$trad->formTitre} {else}Formations professioneels{/if}</h1>
		    
		    <h2 class="span12">{if $smarty.get.id_profil_formation}{$trad->edit_form}{else}{$trad->add_form}{/if}</h2>
		    
		    <hr/>
				
		    <form class="inline-form" method="post">
					
			<div class="span12">
			    <div class="span12">
				    {$globale_trad->label_date_obtention} <em class="red">*</em>
			    </div>
			
			    <div data-date-format="dd/mm/yyyy" data-date="{if $post['form_debut']}{$post['form_debut']}{else}{if $editFormation->form_debut}{$editFormation->form_debut}{else}{$smarty.now|date_format:'%d/%m/%Y'}{/if}{/if}" id="" class="span3 input-append date">
				<input type="text" readonly="" value="{if $post['form_debut']}{$post['form_debut']}{else}{$editFormation->form_debut}{/if}" size="16" name="form_debut" class="span12">
				<span class="add-on"><i class="icon-calendar"></i></span>
			    </div>
			
			  
			    {if isset($error['form_debut'])}
			    <div class="span9 pull-right padding-left-10">
				<div class="span12 alert alert-error ">
				    <p id="anchor-form_debut">{$error['form_debut']}</p>
				</div>
			    </div>
			    {/if}
			 
		    
			</div>
					
			<div class="span12">
				<div class="span12">
					{$globale_trad->label_diplome} <em class="red">*</em>
				</div>
				
				<div class="span6">
				    <input name="form_diplome" class="span12 formModification" type="text" value="{if $post['form_diplome']}{$post['form_diplome']}{else}{$editFormation->form_diplome}{/if}" placeholder="Diplome Obtenu"/>
				</div>
				
				{if isset($error['form_diplome'])}
				<div class="span6 padding-left-10">
				    <div class="span12 alert alert-error ">
					<p id="anchor-form_diplome">{$error['form_diplome']}</p>
				    </div>
				</div>
				{/if}
			</div>
			
			<div class="span12">
			    <div class="span12">
				{$globale_trad->label_ecole} <em class="red">*</em>
			    </div>

			    <div class="span6">
				<input name="form_ecole" class="span12 formModification" type="text" value="{if $post['form_ecole']}{$post['form_ecole']}{else}{$editFormation->form_ecole}{/if}" placeholder="Etablissement"/>
			    </div>
			    
			    {if isset($error['form_ecole'])}
				<div class="span6 padding-left-10">
				    <div class="span12 alert alert-error ">
					<p id="anchor-form_ecole">{$error['form_ecole']}</p>
				    </div>
				</div>
			    {/if}
			</div>
			
			<div class="span12">
			    
			       
			    <div class="span12">
				{$trad->prestaDescForm} <em class="red">*</em>
			    </div>
			    
			    {if isset($error['form_description'])}
			    
				<div class="span12 alert alert-error " id="anchor-form_description">
				    <p id="anchor-form_description">{$error['form_description']}</p>
				</div>
		    
			    {/if}
			    
			    <div class="span12">
				<textarea name="form_description" class=" wysywyg formModificationTxtArea">{if $post['form_description']}{$post['form_description']}{else}{$editFormation->form_description}{/if}</textarea>
			    </div>
			    
			    <div class="monCompteButton pull-right margin-top-10">
				    <input type="hidden" value="formation" name="flag">
				    <button class="btn btn-primary" type="submit">{$globale_trad->bt_enregistrer}</button>
			    </div>
			    
			    <div class="clearfix"></div>
				
			</div>
		    </form>
		    
		    <hr class="pointille"/>
		    
		    <h2>{$trad->diplomes}</h2>
		    {if $listFormations->count > 0}
		    
			{foreach $listFormations->formations as $formation}
		    
			<div class="pull-right">
			    
			    <div class="btn-group group-link margin-bottom-5">
				<a class="btn btn-primary dropdown-toggle white" href="#" data-toggle="dropdown"><i class="icon-tasks icon-white"></i> Actions</a>
				    <a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
				<ul class="dropdown-menu">
				    <li><a href="?page=edit_cv&id_profil_formation={$formation->id_profil_formation}&crypt={$smarty.get.crypt}#formations"><i class="icon-pencil"></i> Modifier</a></li>
				    <li><a class="delEntry" href="?page=edit_cv&rmv_id_profil_formation={$formation->id_profil_formation}{if isset($smarty.get.crypt)}&crypt={$smarty.get.crypt}{/if}#formations"><i class="icon-trash"></i> Supprimer</a></li>
				    <li><a href="#formationDetails" role="button" data-toggle="modal" data-expid="{$formation->id_profil_formation}" class="showFormation"><i class="icon-eye-open"></i>Affficher</a></li>
				</ul>
			    </div>
    
			</div>
			
			<div class="clearfix"></div>
			
			<div class=" listFormation">

				<div class="padding-10">
				
					<p>{$formation->form_ecole} -  {$formation->form_debut}</p>
					<h2>{$formation->form_diplome} </h2>
					<div>
					    {$formation->form_description|truncate:220|@strip_tags}
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			
			
			{/foreach}
		    <!-- modal pour le detail des experience -->
		    <div id="formationDetails" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h3 class="intitule">{$formation->form_ecole} -  {$formation->form_debut}</h3>
			</div>
			
			<div class="mediaWrapper padding-10">
				<div class="description">
					<h2>{$formation->form_diplome} </h2>
					 {$formation->form_description}
				</div>
			</div>
			
		    </div>
		    
		    {else}
		    
			<div class="padding-10 text-center">Aucune Formation</div>
		    
		    {/if}
		    
		</div>    
		<!--FIN formation-->    
	    </div>
	    
	</div>
	
    </div>
    <input type="hidden" name="crypt" value="{$crypt}" id="crypt" autocomplete="off">

</div>