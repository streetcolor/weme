<div class="container-fluid row-fluid corps">
    
    {include file='web/tpl/prestataire/template/menu.tpl'}
    
    <div class="span10 homeDroite">
        <h1 class="result  pull-left">
            {$trad->realisationTitre}
        </h1>
        <button class="btn btn-primary pull-right span2 margin-top-20" data-toggle="modal" role="button" href="#addRea">
                                                   
                <span class="icon-plus icon-white"></span> {$globale_trad->bt_ajouter}
            
        </button>
        
        <div class="clearfix"></div>
        
        <div id="addRea" class="modal hide fade " tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-header">
		      <button aria-hidden="true" data-dismiss="modal" class="close" type="button">x</button>
                     <h3 id="myModalLabel">{$trad->importationReaTitre}</h3>
            </div>            
            <div class="mediaWrapper">
                
                <div class="row-fluid">
                    
                    <div class="span5">
                        <div class="padding-10">
                            {$trad->importationReaConditions}
                        </div>                        
                    </div>
                    <form enctype="multipart/form-data" method="post" class=" imgdetails pull-right span7">
                        
                        <p>
                            <label>{$globale_trad->label_rea}</label>
                            <input type="file" value="" class="file"  name="file">
                        </p>
                    
                        <p>
                            <label>{$globale_trad->label_desc}</label>
                            <textarea name="fichier_description"></textarea>   
                        </p>
            
                        <div class="margin-bottom-0">
                                
                                <button type="submit"class="btn btn-primary span5 pull-right"><span class="icon-plus icon-white"></span>{$globale_trad->bt_ajouter}</button>
                           
                                <div class="clearfix"></div>
                                <input type="hidden" value="realisations" name="file_type">  
                        </div>
                        
                    </form><!--span3-->
                    
                    <div class="clearfix"></div>
                    
                </div>
            </div>                                  
        
    </div>
        {if isset($error['file'])}
            <div class="alert alert-error">
            {$error['file']}
            </div>
        {/if}
        {$messageCallBack}
        <div class="span12 margin-top-20">
            <div id="realisation" class="span12">
                {if $countRealisations >0}
                    
                    {foreach $listRealisations as $realisation}
                        <li class="span2">
                            <div class="inner">
                               <a href="#rea{$realisation@iteration}" role="button" data-toggle="modal"><img src="{$realisation->getThumbnail(168, 100)}" alt="pj" title="pj"></a>
                   
                            </div>
                            <div id="rea{$realisation@iteration}" class="modal hide fade " tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-header">
                                                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">x</button>
                                               <h3 id="myModalLabel">{$trad->modificationRealisation}</h3>
                                    </div>  
                                    <div class="mediaWrapper">
                                        
                                        <div class="row-fluid">
                                            
                                            <div class="span5 imginfo">
                                                
                                                <div class="span12">
                                                    
                                                    <img class="imgpreview img-polaroid " alt="" src="{$realisation->getThumbnail(200, 135)}">
                                    
                                                    <p>
            
                                                            <strong>{$trad->realisationType} </strong>{$realisation->getInformations()->type}<br>
                                                            <strong>{$trad->realisationDate}</strong>{$realisation->getInformations()->date}<br>
                                                            <strong>{$trad->realisationTaille}</strong>{$realisation->getInformations()->size} ko<br>
                                                            <strong>{$trad->realisationResolution}</strong>{if $realisation->getInformations()->height}{$realisation->getInformations()->width}X{$realisation->getInformations()->height}{/if}<br>
                                                            
                                                    </p>
                                                    
                                                </div>
                                                
                                                <div class="clearfix"></div>
                                                
                                            </div><!--span3-->
                                            
                                            <form enctype="multipart/form-data" method="post" class=" span7 imgdetails pull-right">
                                                
                                                <p>
                                                    <label>{$globale_trad->label_rea}</label>
                                                    <input type="file" value="" class="file"  name="file">
                                                </p>
                                            
                                                <p>
                                                    <label>{$globale_trad->label_desc}</label>
                                                    <textarea name="fichier_description">{$realisation->getDescription()}</textarea>   
                                                </p>
                                    
                                                <div class="margin-bottom-0">
                                                        
                                                        <button type="submit"class="btn btn-primary span5"><span class="icon-pencil icon-white"></span> {$globale_trad->bt_enregistrer}</button>
                                                        <a class="delEntry" href="{getLink page=$page action="editRealisations" params="rmv_id_file="|cat:$realisation->getId_file()}">
							    
							    <button class=" btn btn-primary pull-right span5" type="button">
                                                           
                                                                <span class="icon-trash icon-white"></span> {$globale_trad->bt_supprimer}
                                                            
                                                        </button>
							    
							</a>
							
                                                        <div class="clearfix"></div>
                                                        <input type="hidden" value="{$realisation->getId_file()}" name="id_file">  
                                                        <input type="hidden" value="realisations" name="file_type">  
                                                </div>
                                                
                                            </form><!--span3-->
                                            
                                            <div class="clearfix"></div>
                                            
                                        </div>
                                </div>                                  
                                
                            </div>
                        </li>
                    {/foreach}
                    <div class="clearfix"></div>
                
                {else}
                
                    <div class="padding-10">
                        Vous n'avez actullement aucune réalisations
                        <img class="pull-right" src="web/img/ill/arrow.gif" alt="" title="">
                    </div>
                
                {/if}
                
            </div>
        </div>
    </div>
</div>