<div class="container row-fluid corps">
    {include file='web/tpl/prestataire/template/menu.tpl'}

    
    <div class="span10 homeDroite">        
        
        
       {if isset($pathCrop)}
             <form name="" method="POST" id="downloadAvatarCrop" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                          <h3 id="myModalLabel">{$globale_trad->explicationCropTitre}</h3>
                        </div>
                                  
                         <div  id="checkCoords" class="monCompteDroite margin-bottom-0" enctype="multipart/form-data">
                                
                                    <img src="{$pathCrop}" id="cropbox" />
                                
                                    <input type="hidden" name="send_crop" value="{$pathCrop}" />
                                    <input type="hidden" id="x" name="x" />
                                    <input type="hidden" id="y" name="y" />
                                    <input type="hidden" id="w" name="w" />
                                    <input type="hidden" id="h" name="h" />
                                    
                                    <div id="infoCropAvatar">
                                        <p>
                                        <img src="web/img/ico/ico_recadrage.png" alt="{$globale_trad->explicationCropTitre}">
                                        </p>
                                           <p>
                                            {$global_globale_tradtrad->explicationCrop}
                                        </p>
                                    </div>
                                    <div class="clearfix"></div>
                                  
                        
                                    
                         
                         </div>
                         
                         <div class="modal-footer">
                            <button class="btn btn-primary  pull-right" type="submit"><span class=" icon-download icon-white"></span>{$globale_trad->bt_envoyer}</button>
                        </div>
                        
                    </form>  
         
        {/if}
       
       
        
         <form class="span6 margin-top-20" method="POST"  enctype="multipart/form-data">
            {$messageCallBack}
            <div class="monCompteDroite ">
               
                
                    
                <h1 class="margin-bottom-20">
                    {$trad->infos}
                </h1>
               
                {if isset($error['pf_nom']) || isset($error['pf_prenom']) || isset($error['file']) || isset($error['file_avatar'])}
                <div class="span12">
                <div class="alert alert-error span12 margin-bottom-20">
                   {*On envoi nos variable de retour error ou success callBack*}
                   <p>{$error['pf_nom']}</p>
                   <p>{$error['pf_prenom']}</p>
                   <p>{$error['file']}</p>
                   <p> {$error['file_avatar']}</p>
                </div>
                </div>
                {/if}
               
                <div class="span4">
                    <img src="{$avatar}" alt="photoMembre" class="img-polaroid span11 photoMonCompte">
                        
                    <a href="#downloadAvatar" role="button" data-toggle="modal" class="padding-left-20">
                            <button class="btn btn-primary span11   " type="submit"><span class=" icon-download icon-white"></span>{$trad->photo_modif}</button>
                    </a>        
                                                        
                    <div id="downloadAvatar" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                          <h3 id="myModalLabel">{$trad->photo_modif}</h3>
                        </div>
                                  
                        <div class="mediaWrapper">
                            
                            <div class="row-fluid">
                                
                                <div class="span5">
                                    
                                    <div class="padding-10">
                            
                                        <p>
                                             {$trad->prestaImporterPhotoRules} 
                                        </p>
                    
                                        
                                    </div>
                                    
                                </div>
                               
					 
                                <div class=" imgdetails pull-right span7">

                                        <p>
                                            <label>{$trad->photo}</label>
                                            <div class="span12 formModification">
                                                    <input type="file" value="votre CV" class="" name="file">
                                            </div>
                                        </p>
                                
    
                                    
                                </div><!--span3-->
                                
                                <div class="clearfix"></div>
                                
                            </div>
                            
                            
                        </div>
                    
                        <div class="modal-footer">
                            <button class="btn btn-primary  " type="submit"><span class="icon-ok icon-white"></span> {$globale_trad->bt_dl}</button>
                        </div>
                        
                    </div>  
                    
                </div>
                
                <div class="span8">
                    
                    <h2 class=" compteTitleModificationh2">{$globale_trad->label_etat_civil}</h4>
                    
                    
                    <div class="span8">
                       
                        <div class="span6 padding-right-10">
                       
                            <div class="span12">
                                {$globale_trad->label_civilite}
                            </div>
                            
                            <div class="span12 formModificationSelectDiv">
                                <select class="formModificationSelect ">
                                    {foreach $getListCivilites as $civilite}
                                        <option {if $prestataire->getCivilite()->id_civilite==$civilite->id_civilite} selected="selected"{/if} value="{$civilite->id_civilite}">{$civilite->adm_civilite}</option>
                                    {/foreach}
                                </select>
                            </div>
                        
                        </div>
                        
                        <div class="span6 padding-right-10 ">
                            
                            <div class="span12">
                                {$globale_trad->label_nom}
                            </div>
                            
                            <div class="span12">
                                <input  disabled="disabled" class="span12 formModification" type="text" value="{$prestataire->getNom()}"placeholder="{$globale_trad->label_nom}">
                            </div>
                            
                            <div class="clearfix"></div>
                            
                        </div>
                        
                    </div>
                    
                    <div class="span4 pull-right ">
                        
                        <div class="span12">
                            {$globale_trad->label_prenom}
                        </div>
                        
                        <div class="span12">
                            <input  disabled="disabled" class="span12 formModification" type="text" value="{$prestataire->getPrenom()}"  placeholder="{$globale_trad->label_prenom}">
                        </div>
                        
                        <div class="clearfix"></div>
                        
                    </div>
                    
                    
                    
                    <div class="clearfix"></div>
                        
                    <h2 class=" compteTitleModificationh2 margin-top-20">{$globale_trad->label_adresse}</h4>

                            
                            <div class="span12">
                                {$globale_trad->label_societe}
                            </div>
                            
                            <div class="span12">
                                <input  class="span12 formModification" name="pr_entreprise" type="text" value="{$prestataire->getEntreprise()}"placeholder="{$globale_trad->label_societe}">
                            </div>
                            
                            <div class="clearfix"></div>
                            
                    <div class="span12">{$globale_trad->label_rue}</div>
                    <input name="pr_add_fac_rue" class="span12 formModification" type="text" value="{$prestataire->getAdresseFacturation(false)->rue}" placeholder="{$globale_trad->label_rue}"/>
                            
                    <div class="span8 ">
                    
                        <div class="span6">
                            <div class="span12">{$globale_trad->label_cp}</div>
                            <input name="pr_add_fac_codepostal" class="span12 formModification" type="text" value="{$prestataire->getAdresseFacturation(false)->codepostal}" placeholder="{$globale_trad->label_cp}"/>
                            <div class="clearfix"></div>
                        </div>
                    
                        <div class="span6 padding-left-10">
                            <div class="span12">{$globale_trad->label_ville}</div>
                            <input name="pr_add_fac_ville" class="span12 formModification" type="text" value="{$prestataire->getAdresseFacturation(false)->ville}" placeholder="{$globale_trad->label_ville}"/>
                            <div class="clearfix"></div>
                        </div>
                    
                    </div>
                    
                    <div class="span4 pull-right padding-left-10">
                        <div class="span12">{$globale_trad->label_pays}</div>
                        <input name="pr_add_fac_pays" class="span12 formModification" type="text" value="{$prestataire->getAdresseFacturation(false)->pays}" placeholder="{$globale_trad->label_pays}"/>
                        <div class="clearfix"></div>
                    </div>
                            
                    <div class="clearfix"></div>
                                                                        
                    <div class="span12 margin-top-20">
                        
                        <label class="span12"   for="cl_telephone">{$globale_trad->label_telephone}</label>
                             
                        <div class="span8">
                                
                            <div  class="span2 " >
                                <input maxlength="6"  name="pr_standard_indicatif" class="span12 formModification"  type="text" value="{$prestataire->getStandard()->indicatif}"/>
                            </div>
                            
                            <div class="span10 " >
                                <input maxlength="10" name="pr_standard" class="span12 formModification"  type="text"  value="{$prestataire->getStandard()->numero}"/>
                            </div>
                           
                           <div class="clearfix"></div>
                           
                        </div>   
                        
                        <button class="btn btn-primary   pull-right" type="submit">
                            <span class=" icon-pencil icon-white"></span>
                            {$globale_trad->bt_enregistrer}
                        </button>
                    </div>
                </div>
                
                <div class="clearfix"></div>


            </div>
            <!--
            <div class="monCompteDroite margin-top-20">
                <h1>{$trad->config_notif}</h1>
                <h2 class=" compteTitleModificationh2 margin-top-20">{$trad->config_notif}</h4>
                <label class="checkbox inline">
                <input name="pr_notification_mail" type="checkbox" value="1" {if $prestataire->getNotificationEmail()>0} checked="checked" {/if}> {$globale_trad->label_informe_par_mail}
                </label>   
                
                <div class="span12 margin-top-20">
                    <button class="btn btn-primary   pull-right" type="submit">
                        <span class=" icon-pencil icon-white"></span>
                        {$globale_trad->bt_enregistrer}
                    </button>
                </div>
       
                <div class="clearfix"></div>
                
            </div>
            -->
        
        </form>
        
        <div class="span6 padding-left-20">
            
            
            {if isset($agence)}
                <form name="" method="POST" class="monCompteDroite margin-top-20" enctype="multipart/form-data">
                        <h1 class="span7 margin-bottom-20">{$trad->agenceDescription}</h1>
                        <a href="#downloadPlaquette" role="button" data-toggle="modal" class="pull-right">
                            <button class="btn btn-primary  " type="submit"><span class=" icon-plus icon-white"></span>{$trad->agenceImporterPlaquetteTitre}</button>
                        </a>
                        
                        {if isset($error['file_plaquette'])}
                        <div class="span12">
                        <div class="alert alert-error span12 margin-bottom-20">
                           {*On envoi nos variable de retour error ou success callBack*}
                           <p>{$error['file_plaquette']}</p>

                        </div>
                        </div>
                        {/if}
                        
                        <div class="clearfix"></div>
                        <textarea name="pr_presentation" class="margin-top-20 wysywyg formModificationTxtArea">{$prestataire->getPresentation()}</textarea>
                             
                                                            
                        <div id="downloadPlaquette" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                                <h3 id="myModalLabel">{$globale_trad->label_agence_plaq}</h3>
                            </div>
                                      
                            <div class="mediaWrapper">
                                
                                <div class="row-fluid">
                                    
                                    <div class="span5">
                                        
                                        <div class="padding-10">
                                
                                            <p>
                                                <a href="{$plaquette->getPath()}" target="_blank">
                                                <span class="span4">
                                                    <img class="border-1" src="{$plaquette}" alt="" title="">
                                                </span>
                                                </a>
                                            </p>
                        
                                            
                                        </div>
                                        
                                    </div>
                                    
                                    <div class=" imgdetails pull-right span7">
                                        
                                        <p>
                                            <label>{$globale_trad->label_agence_plaq}</label>
                                            <input type="file" value=""  class="pull-left file" name="plaquette">
                                        </p>
                                    
                                        <div class="margin-bottom-0">
                                            
                                                <button class="btn btn-primary  margin-top-20 pull-right" type="submit"><span class="icon-ok icon-white"></span>{$globale_trad->bt_dl}</button>
                                                <div class="clearfix"></div>
                                        </div>
                                        
                                    </div><!--span3-->
                                    
                                    <div class="clearfix"></div>
                                    
                                </div>
                                
                            </div> 
                            
                        </div>  
                                               
                        
                       
                        <div class="clearfix"></div>
                        
                        <div class="span12 margin-top-20">
                            <button type="submit" class="btn btn-primary   pull-right">
                                <span class=" icon-pencil icon-white"></span>
                                {$globale_trad->bt_enregistrer}
                            </button>
                        </div>
                        
                        <div class="clearfix"></div>
                        
                </form>
            {/if}
            
            <form name="" method="POST" class="monCompteDroite margin-top-20">
                
                <h1 class="margin-bottom-20">
                    {$trad->ident}
                </h1>

                <div class="span4">{$globale_trad->label_connexion}</div>
                <div class="span8">
                    <input  class="span12 formModification" type="email" value="{$prestataire->getEmail()}" placeholder="{$globale_trad->label_connexion}" disabled="disabled"/>
                </div>
                <div class="clearfix"></div>
                
                
                <div class="span4">{$globale_trad->label_mdp}</div>
                <div class="span8">
                    <input name="usr_password" class="span12 formModification" type="password" value="" placeholder="{$globale_trad->label_mdp}"/>
                </div>
                <div class="clearfix"></div>
                
                
                <div class="span4">{$globale_trad->label_mdp_conf}</div>
                <div class="span8">
                    <input name="mdp_verif" class="span12 formModification" type="password" value="" placeholder="{$globale_trad->label_mdp_conf}"/>
                </div>
              
                <div class="clearfix"></div>
                {if isset($error['mdp_verif'])}
                     <div class="span12 padding-left-10">
                     <div class="alert alert-error  margin-0">
                        {*On envoi nos variable de retour error ou success callBack*}
                        <p>{$error['mdp_verif']}</p>	
                     </div>
                     </div>
                {/if}
               
                <button class="btn btn-primary margin-top-20 pull-right" type="submit">
                    <span class=" icon-pencil icon-white"></span>
                    {$globale_trad->bt_enregistrer}
                </button>
                
                <div class="clearfix"></div>
            </form> 
        
        </div>
        
    </div>
</div>

