<div class="container-fluid row-fluid corps">
    
    {include file='web/tpl/prestataire/template/menu.tpl'}
    
    <div class="span10 homeDroite">
        
	<div class="search margin-bottom-20">
            
	    <form class="form-inline homeDroiteForm" method="POST" action="{getlink page="missions"  action='search'}" >
                
		<input type="text" name="search[mots_cle]" value="{$smarty.session.search['mots_cle']}" placeholder="{$globale_trad->searchProjet}" class="span12 homeDroiteSearch"/><br/>
                
		<div class="row-fluid" id="filter">
		    
		    <div class="span12">
			
			<a href="{getlink page='missions'}">
			    <img  container="true"  class=" pull-left border-right-1 padding-0" src="web/img/ico/ico_reload.png"  alt="#" title="#" />
			</a>
			
			<span class="pull-left label-form">{$globale_trad->label_lieu}</span>
	
			<div class="border-right-1 pull-left ">
			
				<input  id="geocomplete" type="text" value="{$smarty.session.search['lo_region_short']}" class="span12 formModification"/>
				<input type="hidden" name="search[lo_departement_short]" id="lo_departement_short" value="{$smarty.session.search['lo_departement_short']}">
				<input type="hidden" name="search[lo_region_short]" id="lo_region_short" value="{$smarty.session.search['lo_region_short']}">
				<input type="hidden" name="search[lo_pays_short]" id="lo_pays_short" value="{$smarty.session.search['lo_pays_short']}">
			  
			    <div class="clearfix"></div>
			</div>
			
			<div class="border-right-1 padding-left-10 pull-left ">
			    <div class="span12 formModificationSelectDiv">
				<select class=" formModificationSelect" name="search[mi_anciennete]">
				    {foreach  from=$listPublications key=k item=v}
				    <option {if $k==$smarty.session.search.mi_anciennete}selected="selected"{/if} value="{$k}">{$v}</option>
				    {/foreach}
				</select>
			    </div>
			</div>
			
			<span class="pull-left label-form">{$globale_trad->label_date}</span>
			
			<div class="border-right-1 pull-left ">
			    
			    <div  data-date-format="{$datepicker.dateFormat}" data-date="{$smarty.request.search['mi_debut']}" class=" span6 input-append date">
				<input type="text" value="{if $smarty.request.search['mi_debut']}{$smarty.session.search['mi_debut']}{else}{$globale_trad->label_du}{/if}" size="16" name="search[mi_debut]" class="span12 ">
				<span class="add-on"><i class="icon-calendar"></i></span>
			    </div>
			    
			    <div  data-date-format="{$datepicker.dateFormat}" data-date="{$smarty.request.search['mi_fin']}" class="  span6 input-append date">
				<input type="text" value="{if $smarty.request.search['mi_fin']}{$smarty.session.search['mi_fin']}{else}{$globale_trad->label_au}{/if}" size="16" name="search[mi_fin]" class="span12">
				<span class="add-on"><i class="icon-calendar"></i></span>
			    </div>
			
			    <div class="clearfix"></div>
			    
			</div>
			
			<input  container="true"  rel="tooltip" class=" pull-left last border-right-1 padding-0" type="image" src="web/img/ico/ico_search_form.png" alt="Rechercher" title="Rechercher" />
			
			<div class="clearfix"></div>
			
		    </div>
		
		</div>
		
            </form>
	    
        </div>
	    	
	<div class="span9 ">
	
	{if isset($error['file']) || isset($error['file_cv']) || isset($error['file_plaquette'])}
	<div class="alert alert-error">
	    {$error['file']}
	    {$error['file_cv']}
	    {$error['file_plaquette']}
	</div>
	{/if}
	{$messageCallBack}
	
        <div class="accordion" id="accordion1">
	    <div class="accordion-group">
		<div class="accordion-heading">
		    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne">
			<div class="span5">
			    <span class="pull-left margin-right-10  ico35 project"></span>
			    <h1 lass="span10">{$trad->derniers_projets}</h1>
			    <div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
			<span class=" ico35 button"></span>
		    </a>
		</div>
		<div id="collapseOne" class="accordion-body collapse in">
		    <div class="accordion-inner">
			<table width="100%" class="mission">
			    <tbody>
				{if $listMissions.count >0}
				    
					{if !$listMissions.matchingResult}
					<tr align="center">
					    <td class="last MissionsListActions" colspan="5">
						Attention aucune mission ne match avec vos criteres de CV. Nous vous affichons par conséquents toutes les missions disponibles
					    </td>
				
					</tr>
					{/if}
					{foreach $listMissions.missions as $mission}
					    {if $mission@iteration > 3}{break}{/if} 
					    {include file='web/tpl/prestataire/template/mission.tpl'}
					{/foreach}
				    
				{else}
				    <tr align="center">
					    <td class="last MissionsListActions" colspan="5">
					    {$trad->prestaNoMission}
					    </td>
				    </tr>
				{/if}
			    </tbody>
			</table>
		    </div>
		</div>
	    </div>
	</div>
        <div class="accordion" id="accordion3">
	    <div class="accordion-group">
		<div class="accordion-heading">
		    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapseTree">
			<div class="span5">
			    <span class="pull-left margin-right-10 ico35 responses"></span>
			    <h1 lass="span10">{$trad->prop_en_cours}</h1>
			    <div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
			<span class=" ico35 button"></span>
		    </a>
		</div>
		<div id="collapseTree" class="accordion-body collapse in">
		    <div class="accordion-inner">
			
			<table width="100%" class="mission">
			    <tbody>
			    {if $countReponses >0}
				
					{foreach $reponses->reponses as $reponse}
					    {if $reponse@iteration > 3}{break}{/if} 
					    {include file='web/tpl/prestataire/template/reponse.tpl'}
					{/foreach}
				    
			    {else}
				<tr align="center">
					<td class="last MissionsListActions" colspan="5">
					{$trad->no_prop}
					</td>
				</tr>
			    {/if}
			    </tbody>
			</table>
			
		    </div>
		    <div class="accordionMore"><a href="{getlink page='responses'}"><button class="buttonCollapseMore"><b>></b> {$trad->prestaSeeAllProp}</button></a></div>
		</div>
	    </div>	
	</div>
    </div>
    <!-- fin partie droite -->
    <div class="span3 pull-right">
	
	<div class="padding-left-20">
	
	    <!--CV -->
	    {if $agence==false}
	    <div class="DashboardDroitInfoCVJpeg">
		{$trad->profilIsCompleteTo}
		
		<p class="DashboardDroitInfoCVPourcentage">{$prestataire->getPourcentage()}%</p>
		
		<p class="CVJpeg ">
		    
		    <a href="{$cv->getPath()}" target="_blank">
			<img src="{$cv->getThumbnail()}" alt="" class="img-polaroid span12"/>
			<span class="clearfix"></span>
		    </a>
		    
		</p>
		
		<p class="buttonImportCV margin-top-20" >
		    <a href="#uploadCV" role="button" data-toggle="modal">
		       <span class="ico50 cv pull-left margin-right-10 margin-top-5"></span>
		       <span class="white">
				{$trad->update}
			    <!--<span class="higher">Importez</span>
			    <br>
			    rapidement votre CV au format PDF-->
		       </span>
		       <span class="clearfix"></span>
		    </a>
		</p>
		 
		<p class="astuceCV">
		    <span class="higher">{$trad->prestaCvAstuceTitre}</span>
		    <br>
		    {$trad->prestaCvAstuce1}
		</p>
		 
		 
		<div id="uploadCV" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		    
		    <div class="modal-header">
		      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		      <h3 id="myModalLabel">{$trad->prestaImporterCvTitre}</h3>
		    </div>
		              
		    <div class="mediaWrapper">
			
			<div class="row-fluid">
			    
			    <div class="span5">
				
				<div class="padding-10">
			
				    {$trad->prestaImporterCvRules}
				</div>
				
			    </div>
			    
			    <form enctype="multipart/form-data" method="post" class=" imgdetails pull-right span7">
				
				<p>
				    <label>{$trad->votre_cv} </label>
				    <input type="file" value=""  class="pull-left file" name="file">
				</p>
			    
				<div class="margin-bottom-0">
				    
					<button class="btn btn-primary  margin-top-20 pull-right" type="submit"><span class=" icon-download icon-white"></span> {$globale_trad->bt_dl}</button>
					
					<input type="hidden" value="membre" name="controller">
					<input type="hidden" value="addCv" name="action">
					<input type="hidden" value="cv" name="file_type">
					
					<div class="clearfix"></div>
				</div>
				
			    </form><!--span3-->
			    
			    <div class="clearfix"></div>
			    
			</div>
			
		    </div> 
		    
		</div>
		 
		<div class="clearfix"></div>
	    </div>
	    {else}
	    <div class="DashboardDroitInfoCVJpeg">
		
		<h1>{$globale_trad->label_agence_plaq}</h1>
		
		<p class="CVJpeg">
		    
		    <a href="{$plaquette->getPath()}" target="_blank">
			<img src="{$plaquette->getThumbnail()}" alt="" class="img-polaroid span12"/>
			<span class="clearfix"></span>
		    </a>
		    
		</p>
		
		<p class="buttonImportCV margin-top-20" container="true"  rel="tooltip" title="Importez maintenant votre CV et augmentez votre visibilité sur le site " >
		    <a href="#uploadCV" role="button" data-toggle="modal">
		       <span class="ico50 cv pull-left margin-right-10 margin-top-5"></span>
		       <span class="white">
			   {$trad->agenceImporterPlaquette}</span>
		       <span class="clearfix"></span>
		    </a>
		</p>
		
		 <div id="uploadCV" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		    <div class="modal-header">
		      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		      <h3 id="myModalLabel">{$trad->agenceImporterPlaquetteTitre}</h3>
		    </div>
		    <div class="mediaWrapper">
			
			<div class="row-fluid">
			    
			    <div class="span5">
				
				<div class="padding-10">
			
				    {$trad->prestaImporterCvRules}
				    
				</div>
				
			    </div>
			    
			    <form enctype="multipart/form-data" method="post" class=" imgdetails pull-right span7">
				
				<p>
				    <label>{$globale_trad->label_agence_plaq}</label>
				    <input type="file" value=""  class="pull-left file" name="plaquette">
				</p>
			    
				<div class="margin-bottom-0">
				    
					<button class="btn btn-primary  margin-top-20 pull-right" type="submit"><span class=" icon-download icon-white"></span> {$globale_trad->bt_dl}</button>
					
					<input type="hidden" value="membre" name="controller">
					<input type="hidden" value="addPlaquette" name="action">
					<input type="hidden" value="plaquette" name="file_type">
					
					<div class="clearfix"></div>
				</div>
				
			    </form><!--span3-->
			    
			    <div class="clearfix"></div>
			    
			</div>
			
		    </div> 
		</div>
		 
	    </div>    
	    {/if}
	    <!-- suivi proposition -->
	    <div class="DashboardDroitInfoProposition">
		<h1 class="margin-bottom-20">{$trad->titre}</h1>
		{foreach $getListStatuts as $statut}
		<div class="DashboardDroitInfoPropositionP"><div class="PText span10"><a  href="{getLink page='responses' action='search' params="id_reponse_statut="|cat:$statut->id_reponse_statut}">{$statut->adm_statut}</a></div><a  href="{getLink page='responses' action='search' params="id_reponse_statut="|cat:$statut->id_reponse_statut}" class="PCounter span2">{$reponses->statistiques[$statut->id_reponse_statut]}</a></div>
		{/foreach}
		<a href="{getLink  page="my_missions"}" class="span12 buttonProjetSaved">{$globale_trad->saved}</a>
		<div class="clearfix"></div>
	    </div>
	    <!-- contactez nous -->
	    <div class="DashboardDroitInfoContact">
		<span class="ico35 contact margin-right-10"></span>
		<h1>{$trad->contactTitre}</h1><p>{$trad->contactIntro}</p>
		<a href="{getLink  page="contact"}" class="span12 buttonProjetSaved">{$trad->contact_bt}</a>
		<div class="clearfix"></div>
	    </div>
	    
	</div>
    </div>
</div>

<div class="clearfix"></div>