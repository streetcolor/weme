<div class="container-fluid row-fluid corps">
    
    {include file='web/tpl/prestataire/template/menu.tpl'}
    
    <div class="span10 homeDroite">
        
	<div class="search">
            
	    <form class="form-inline homeDroiteForm" method="POST" action="{getlink page=$page  action='search' params='id_mission='|cat:$smarty.request.id_mission}" >
                                
		<div class="row-fluid" id="filter">
		    
		    <div class="span12">
			
			<a href="{getlink page=$page}">
			    <img class=" pull-left border-right-1 padding-0" src="web/img/ico/ico_reload.png" alt="reload" title="reload" />
			</a>
			
			<span class="pull-left label-form">{$globale_trad->label_publication}</span>
				
			<div class="border-right-1 pull-left ">
			    <div class="span12 formModificationSelectDiv">
				<select class=" formModificationSelect" name="search[id_reponse_statut]">
				    <option value="">{$globale_trad->indifferent}</option>
				    {foreach $getListStatuts as $statut}
					<option {if $statut->id_reponse_statut==$smarty.session.search.id_reponse_statut}selected="selected"{/if}  value="{$statut->id_reponse_statut}">{$statut->adm_statut}</option>
				    {/foreach}
				</select>
			    </div>
			</div>
			
			<span class="pull-left label-form">{$globale_trad->label_date}</span>
			
			<div class="border-right-1 pull-left ">
			    
			    <div  data-date-format="{$datepicker.dateFormat}" data-date="{$smarty.session.search['mi_debut']}" class=" span6 input-append date">
				<input type="text" value="{if $smarty.session.search['rp_fin']}{$smarty.session.search['rp_debut']}{else}{$globale_trad->label_du}{/if}" size="16" name="search[rp_debut]" class=" span12">
				<span class="add-on"><i class="icon-calendar"></i></span>
			    </div>
			    
			    <div  data-date-format="{$datepicker.dateFormat}" data-date="{$smarty.request.search['rp_fin']}" class="  span6 input-append date">
				<input type="text" value="{if $smarty.session.search['rp_fin']}{$smarty.session.search['rp_fin']}{else}{$globale_trad->label_au}{/if}" size="16" name="search[rp_fin]" class=" span12">
				<span class="add-on"><i class="icon-calendar"></i></span>
			    </div>
		    
			    <div class="clearfix"></div>
			    
			</div>
			
			<input class="last pull-left border-right-1 padding-0" type="image" src="web/img/ico/ico_search_form.png" alt="reload" title="reload" />
			
			<div class="clearfix"></div>
			
		    </div>
		
		</div>
		
            </form>
        </div>
	
	
	{*Si la requete getLitsReponses() renvoi des response*}
	{if isset($count)}
	
	    <h1 class="result">
		{$globale_trad->searchResult}<b>{$count} {$globale_trad->label_proposition}</b>
	    </h1>
	    
	    
	    {$messageCallBack}
	    
	    <table width="100%" cellpadding="4" class=" mission">
		<thead class="MissionListHeader">
		    <tr>
			    <td class=" first">{$globale_trad->label_reponse}</td>
	    
			    <td align="center" class="">{$globale_trad->label_tarif}</td>
			    
			    <td align="center" class="">{$globale_trad->label_date}</td>
			    			    
			    <td align="center" class=" last">{$globale_trad->mission}</td>
			    
		    </tr>
		    
		</thead>
	    
		<tbody>
		    {foreach $listReponses as $reponse}
			
			{*Process qui permet de n'appeler qu'une seule fois les statuts reponses*}
			{foreach $getListEntete as $entete}
			    {if $entete ==  $reponse->getStatut()->id_statut}
				
				<tr class="MissionsListEntete">
					
					<td colspan="5" class="last ">{$reponse->getStatut()->statut}</td>
					
				</tr>
				
				{$getListEntete[$entete]=false}
			    {/if}
			{/foreach}
			{*Fin du process*}
	
			{include file='web/tpl/prestataire/template/reponse.tpl'}

		    {/foreach}
		</tbody>
		
		<tfoot>
		    
		    <tr class="MissionsListPagination pagination">
		
			<td colspan="4" class="last" >
				<ul>
				{if $nbrPages > 1 && $pageCourante != 1} <li><a href="{getlink page=$page  action='search' params="paginate="|cat:($pageCourante-1)|cat:'&id_mission='|cat:$smarty.request.id_mission}" class="blue"><i class="icon-chevron-left"></i> {$globale_trad->precedent}</a></li> {/if}
				
				{section name=for start=1 loop=$nbrPages+1 step=1}
				    {if $smarty.section.for.index==$pageCourante}
					<li><span>{$smarty.section.for.index}</span></li>
				    {else}
					<li><a href="{getlink page=$page  action='search' params="paginate="|cat:$smarty.section.for.index|cat:'&id_mission='|cat:$smarty.request.id_mission}">{$smarty.section.for.index}</a></li>
				    {/if}
				{/section}
				
				{if $nbrPages >1 && $pageCourante != $nbrPages} <li><a href="{getlink page=$page  action='search' params="paginate="|cat:($pageCourante+1)|cat:'&id_mission='|cat:$smarty.request.id_mission}" class="blue">{$globale_trad->suivant} <i class="icon-chevron-right"></i></a></li> {/if}
				</ul>
			</td>

		    </tr>
		    
		</tfoot>
	    
	    </table>

	{else}            
                <div class="bgWhite resultSearchNotFound">{$globale_trad->result_no_propositions} <b>{$post["mots_cle"]}</b></div>  
        {/if}


</div>
<div class="clearfix"></div>
