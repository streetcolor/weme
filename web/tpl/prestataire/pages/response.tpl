<div class="container-fluid row-fluid corps">
    
    {include file='web/tpl/prestataire/template/menu.tpl'}
    
    <div class="span10 homeDroite">
        	
	<div class="span12 row-fluid">
	    <div class="monCompteDroite">
		
		<h1>Annonce : {$mission->getTitre()}</h1>
		<div class="span9">
		    <p>{$mission->getDescription()}</p>
		</div>
		
		<div class="span3 padding-left-20">
		    <h2>Localité :</h2>
		    <p class="span12">
			{$mission->getLocalite()}
		    </p>
		    <h2>Tarif :</h2>
		    <p class="span12">
			{$mission->getTarif(true)}
		    </p>
		    <h2>Date :</h2>
		    <p class="span12">
			du {$mission->getDebut()} Au {$mission->getFin()}
		    </p>
		    
		    {if $missionPostes->count}
		    <h2>Poste :</h2>
		    <ul class="span12">
		       {foreach $missionPostes->postes as $poste}
			<li>{$poste->adm_poste}</li>
		       {/foreach}
		    </ul>
		    {/if}
		    
		    {if  $missionCategorie->id_categorie}
		    <h2>Secteur :</h2>
		    <ul class="span12">
			<li>{$missionCategorie->adm_categorie}</li>
		    </ul>
		    {/if}
		    
		    {if $missionCompetences->count}
		    <h2>Competences :</h2>
		    <ul class="span12">
		       {foreach $missionCompetences->competences as $competence}
			<li>{$competence->adm_competence}</li>
		       {/foreach}
		    </ul>
		    {/if}
		    
		    {if $missionLangues->count}
		    <h2>Langues :</h2>
		    <ul class="span12">
		       {foreach $missionLangues->langues as $langue}
			<li>{$langue->adm_langue}</li>
		       {/foreach}
		    </ul>
		    {/if}
		    
		    {if $missionEtudes->count}
		    <h2>Langues :</h2>
		    <ul class="span12">
		       {foreach $missionEtudes->etudes as $etude}
			<li>{$etude->adm_etude}</li>
		       {/foreach}
		    </ul>
		    {/if}
		</div>
		<div class="clearfix"></div>
		
	    </div>
	  	
	    <div class="span12 row-fluid">
		
		<div class="monCompteDroite">
		    
		    <h1 class="">Votre réponse</h1>
		    
		    <div class="span9">
			<div>{$reponse->getReponse()}</div>
		
		    </div>
		    
		    <div class="span3 padding-left-20">
			
			<h2>Votre contact direct:</h2>
			<p class="span12">
			    <b>Structure :</b> {$reponse->getStructure()->structure}
			    <br>
			    <b>Contact : </b>{$reponse->getNom()} {$reponse->getPrenom()}
			    <br>
			    <b>Email :</b> {$reponse->getEmail()}
			    
			</p>
			
			{if $reponse->getStructure()->id_structure == 3}
			<h2>Les profil prosés:</h2>
			<ul>
			{foreach $profils as $profil}
			    <li>
				{$profil->getNom()} {$profil->getPrenom()}
				
			    </li>
			   
			{/foreach}
			</ul>
			{/if}
			<h2>Tarif :</h2>
			<p class="span12">
			    {$reponse->getTarif(true)}
			</p>
			
			{if $reponse->getCommentaire()}
			    <h2>Commentaire :</h2>
			    <p>
				{$reponse->getCommentaire()}
			    </p>
			{/if}
			
			{if $pj->getId_file()}
			    <h2>Piece Jointe :</h2>
			    <p>
				<a target="_blank" href="{$pj->getPath()}">
				    <button class="btn btn-primary " type="button">
					<span class="icon-pencil icon-white"></span>
					Voir la pièce jointe
				    </button>
				</a>
			    </p>
			{/if}
			
			{if $realisations->count>0}
			    <h2>Réalisation :</h2>
			    {foreach $realisations->files as $realisation}
				<a href="{$realisation->getPath()}" target="_blank" class="{if $realisation->getType()==1}fancy-image{/if}">
				<img  src="{$realisation->getThumbnail(50, 50)}" alt="pj" title="pj">
				</a>
				 
			    {/foreach}
			{/if}
		    
			
		    </div>
		    
		    <div class="clearfix"></div>
		    
		      
		</div>
	    
	    </div>
	    
	    
	    
	</div>
    </div>
</div>


