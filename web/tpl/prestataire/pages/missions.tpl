<div class="container-fluid row-fluid corps">
    
    {include file='web/tpl/prestataire/template/menu.tpl'}
    
    <div class="span10 homeDroite">
        
	<div class="search">
            
	    <form class="form-inline homeDroiteForm" method="POST" action="{getlink page=$page  action='search'}" >
                
		<input type="text" name="search[mots_cle]" value="{$smarty.session.search['mots_cle']}" placeholder="{$globale_trad->searchProjet}" class="span12 homeDroiteSearch"/>
                
		<div class="row-fluid" id="filter">
		    
		    <div class="span12">
			
			<a href="{getlink page='missions'}">
			    <img class=" pull-left border-right-1 padding-0" src="web/img/ico/ico_reload.png" alt="reload" title="reload" />
			</a>
			
			<span class="pull-left label-form">{$globale_trad->label_lieu}</span>
	
			<div class="border-right-1 pull-left ">
			
				<input  id="geocomplete" type="text" value="{$smarty.session.search['lo_region_short']}" class="span12 formModification"/>
				<input type="hidden" name="search[lo_departement_short]" id="lo_departement_short" value="{$smarty.session.search['lo_departement_short']}">
				<input type="hidden" name="search[lo_region_short]" id="lo_region_short" value="{$smarty.session.search['lo_region_short']}">
				<input type="hidden" name="search[lo_pays_short]" id="lo_pays_short" value="{$smarty.session.search['lo_pays_short']}">
			  
			    <div class="clearfix"></div>
			</div>
			
			<div class="border-right-1 padding-left-10 pull-left ">
			    <div class="span12 formModificationSelectDiv">
				<select class=" formModificationSelect" name="search[mi_anciennete]">
				    {foreach  from=$listPublications key=k item=v}
				    <option {if $k==$smarty.session.search.mi_anciennete}selected="selected"{/if} value="{$k}">{$v}</option>
				    {/foreach}
				</select>
			    </div>
			</div>
			
			<span class="pull-left label-form">{$globale_trad->label_date}</span>
			
			<div class="border-right-1 pull-left ">
			    
			    <div  data-date-format="{$datepicker.dateFormat}" data-date="{$smarty.request.search['mi_debut']}" class="  span6 input-append date">
				<input type="text" value="{if $smarty.request.search['mi_debut']}{$smarty.session.search['mi_debut']}{else}{$globale_trad->label_du}{/if}" size="16" name="search[mi_debut]" class="span12 ">
				<span class="add-on"><i class="icon-calendar"></i></span>
			    </div>
			    
			    <div  data-date-format="{$datepicker.dateFormat}" data-date="{$smarty.request.search['mi_fin']}" class="  span6 input-append date">
				<input type="text" value="{if $smarty.request.search['mi_fin']}{$smarty.session.search['mi_fin']}{else}{$globale_trad->label_au}{/if}" size="16" name="search[mi_fin]" class="span12">
				<span class="add-on"><i class="icon-calendar"></i></span>
			    </div>
			
			    <div class="clearfix"></div>
			    
			</div>
			
			<input class=" pull-left last border-right-1 padding-0" type="image" src="web/img/ico/ico_search_form.png" alt="reload" title="reload" />
			
			<div class="clearfix"></div>
			
		    </div>
		
		</div>
		
            </form>
	    
        </div>
	
	
	
	
	{*Si la requete getLitsMissions() renvoi des missions*}
	{if isset($count)}
	
	    <h1 class="result">
		{$globale_trad->searchResult} <b>{$count} {$globale_trad->missions}</b>
	    </h1>
	    
	    <table width="100%" cellpadding="5" class="mission">
		<thead class="MissionListHeader">
		    <tr>

			    <td class="span6 first">{$globale_trad->label_projet}</td>
	    
			    <td align="center" class="">{$globale_trad->label_mob}</td>
    
			    <td align="center" class="">{$globale_trad->label_tarif}</td>
			    
			    <td align="center" class="">{$globale_trad->label_date}</td>
			    
			    <td align="center" class=" last">{$trad->missionNbReponses}</td>

		    </tr>
		</thead>
	    
		<tbody>
		    {foreach $listMissions as $mission}        
			{include file='web/tpl/prestataire/template/mission.tpl'}
		    {/foreach}
		</tbody>
		
		<tfoot>
		    
		    <tr class="MissionsListPagination pagination">
		
			<td colspan="5" class="last" >
				<ul>
				{if $nbrPages > 1 && $pageCourante != 1} <li><a href="{getlink page=$page  action='search' params="&paginate="|cat:($pageCourante-1)}" class="blue"><i class="icon-chevron-left"></i> {$globale_trad->precedent}</a></li> {/if}
				
				{section name=for start=1 loop=$nbrPages+1 step=1}
				    {if $smarty.section.for.index==$pageCourante}
					<li><span>{$smarty.section.for.index}</span></li>
				    {else}
					<li><a href="{getlink page=$page  action='search' params="&paginate="|cat:$smarty.section.for.index}">{$smarty.section.for.index}</a></li>
				    {/if}
				{/section}
				
				{if $nbrPages >1 && $pageCourante != $nbrPages} <li><a href="{getlink page=$page  action='search' params="&paginate="|cat:($pageCourante+1)}" class="blue">{$globale_trad->suivant} <i class="icon-chevron-right"></i></a></li> {/if}
				</ul>
			</td>

		    </tr>
		    
		</tfoot>
	    
	    </table>

	{else}            
                <div class="bgWhite resultSearchNotFound">{$globale_trad->no_mission_match} <b>{$post["mots_cle"]}</b></div>  
        {/if}
	
</div>
    
<div class="clearfix"></div>
