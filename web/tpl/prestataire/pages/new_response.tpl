<div class="container-fluid row-fluid corps">
    
    {include file='web/tpl/prestataire/template/menu.tpl'}
    
    <div class="span10 homeDroite">
        
	<div class="search">
            
	    <form class="form-inline homeDroiteForm" method="POST" action="{getlink page=$page  action='search'}" >
                                
		<div class="row-fluid" id="filter">
				
			     <div class="pull-left padding-left-5 margin-top-5 margin-bottom-5 margin-right-15">
				
				<a class="scroller"  href="javascript:history.back()">
				    <button type="button" class="btn client btn-fourth pull-left padding-left-10 padding-right-15 border-radius-6">
					<span class="ico20 backresponse"></span>
					{$globale_trad->bt_retour}
				    </button>
				</a>
		    
			    </div>	
				
			    <div class="pull-left padding-left-5 margin-top-5 margin-bottom-5 margin-right-15 margin-left-5">
				
		
				{if in_array($mission->getId_mission(), $favorisMission)}
				    <a href="{getLink page="missions" action="removeFavori" params="id_mission="|cat:$mission->getId_mission()|cat:"&mode=reponse" crypt=true}">
					<button type="button" class="btn btn-fourth pull-left padding-left-10 padding-right-15 border-radius-6">
					    <span class="ico20 supfav"></span>
					    {$globale_trad->select_supp_fav}
					</button>
					<div class="clearfix"></div>
				    </a>
				{else}
				    <a href="{getLink page="missions" action="addFavori" params="id_mission="|cat:$mission->getId_mission()|cat:"&mode=reponse" crypt=true}">
					<button type="button" class="btn btn-fourth pull-left padding-left-10 padding-right-15 border-radius-6">
					    <span class="ico20 addfav"></span>
					    {$globale_trad->select_add_favoris}
					</button>
					<div class="clearfix"></div>
				    </a>
				{/if}
				<div class="clearfix"></div>
				
			    </div>
			    
			    <div class="pull-left padding-left-5 margin-top-5 margin-bottom-5 margin-right-15">
				
				<a class="scroller" href="#reponseAnchor">
				    <button type="button" class="btn btn-fourth pull-left padding-left-10 padding-right-15 border-radius-6">
					<span class="ico20 edit"></span>
					{$globale_trad->bt_reponse_offre}
				    </button>
				</a>
		    
			    </div>
			    
			    <div class="pull-left padding-left-5 margin-top-5 margin-bottom-5 margin-right-15">

				
				<a class="sendFriend" href="&id_mission={$mission->getId_mission()}">
				    <button type="button" class="btn btn-fourth pull-left padding-left-10 padding-right-15 border-radius-6">
					<span class="ico20 transfert"></span>
					{$globale_trad->bt_transfere}
				    </button>
				</a>
				
			    </div>
			
		
		</div>
		
            </form>
        </div>
	
	<div class="span12 row-fluid">
	    <div class="monCompteDroite">
		<h1>{$mission->getTitre()}</h1>
		<div class="span9">
		    <p>{$mission->getDescription()}</p>
		</div>
		
		<div class="span3 padding-left-20">
		    <h2>{$globale_trad->label_mob} :</h2>
		    <p class="span12">
			{$mission->getLocalite()}
		    </p>
		    <h2>{$globale_trad->label_tarif} :</h2>
		    <p class="span12">
			{$mission->getTarif(true)}
		    </p>
		    <h2>{$globale_trad->label_date} :</h2>
		    <p class="span12">
			du {$mission->getDebut()} Au {$mission->getFin()}
		    </p>
		   
		    {if $missionPostes->count}
		    <h2>{$globale_trad->label_poste} :</h2>
		    <ul class="span12">
		       {foreach $missionPostes->postes as $poste}
			<li>{$poste->adm_poste}</li>
		       {/foreach}
		    </ul>
		    {/if}
		    
		    {if  $missionCategorie->id_categorie}
		    <h2>{$globale_trad->label_secteur_activite} :</h2>
		    <ul class="span12">
			<li>{$missionCategorie->adm_categorie}</li>
		    </ul>
		    {/if}
		   
		    {if $missionCompetences->count}
		    <h2>{$globale_trad->label_comp} :</h2>
		    <ul class="span12">
		       {foreach $missionCompetences->competences as $competence}
			<li>{$competence->adm_competence}</li>
		       {/foreach}
		    </ul>
		    {/if}
		    
		    {if $missionLangues->count}
		    <h2>{$globale_trad->label_langue} :</h2>
		    <ul class="span12">
		       {foreach $missionLangues->langues as $langue}
			<li>{$langue->adm_langue}</li>
		       {/foreach}
		    </ul>
		    {/if}
		    
		    {if $missionEtudes->count}
		    <h2>{$globale_trad->label_formation} :</h2>
		    <ul class="span12">
		       {foreach $missionEtudes->etudes as $etude}
			<li>{$etude->adm_etude}</li>
		       {/foreach}
		    </ul>
		    {/if}
		</div>
		<div class="clearfix"></div>
		
	    </div>
	    
	    <form method="post" id="reponseAnchor" enctype="multipart/form-data" class="monCompteDroite span12">
		
		<h1 >{$trad->detailMissionReponseTitre}</h1>

		{if isset($error['rp_reponse'])}
		    <div class="alert alert-error">
			{*On envoi nos variable de retour error ou success callBack*}
			{$error['rp_reponse']}			
		    </div>
		{/if}
		<textarea  name="rp_reponse" class="wysywyg formModificationTxtArea">{if $post['rp_reponse']}{$post['rp_reponse']}{/if}</textarea>
		
		
		<div class="span12 margin-top-20">
		    
		    <div class="span2">
			<h4>{$globale_trad->label_tarif}</h4>
		    </div>
		
		    <div class="span7 padding-5">
    
			<div class="span4">
			
			    <div class="span4">
				<input type="text" name="rp_tarif" id="rp_tarif" class="formModification span12" value="{if $post['rp_tarif']}{$post['rp_tarif']}{/if}" />
			    </div>
			    
			    <div class="span8">
				<div class="span12 formModificationSelectDiv">
				    <select class=" formModificationSelect" name="id_monnaie">
					{foreach $getListMonnaies as $monnaie}
					    <option {if $monnaie->id_monnaie=={$post['id_monnaie']}} selected="selected"{/if} value="{$monnaie->id_monnaie}">{$monnaie->adm_monnaie}</option>
					{/foreach}
				    </select>
				</div>
			    </div>
			    
			</div>
			
			<div class="span7 offset1 pull-right">
			    <div class="span12 formModificationSelectDiv">
				<select class=" formModificationSelect" name="id_contrat">
				    {foreach $getListContrats as $contrat}
				    <option {if $contrat->id_contrat=={$post['id_contrat']}} selected="selected"{/if} value="{$contrat->id_contrat}">{$contrat->adm_contrat}</option>
				{/foreach}
				</select>
			    </div>
			</div>
			
			<div class="clearfix"></div>
    
		    </div>
		    
		    <div class="span3">
			
			{if isset($error['rp_tarif'])}
			    <div class="alert alert-error span12">
				{*On envoi nos variable de retour error ou success callBack*}
				{$error['rp_tarif']}			
			    </div>
			{/if}
			
		    </div>
		    
		    <div class="clearfix"></div>
		
		</div>	
		
		<div class="span12 margin-top-20">
		    
		    <div class="span2">
			<h4>{$globale_trad->label_pj}</h4>
		    </div>
		
		    <p class="imgdetails span7">
		    	
			<input type="file" value="" class="file"  name="file">
	    
		    </p>
		    
		    <div class="span3">
			
			{if isset($error['file'])}
			    <div class="alert alert-error span12">
				{*On envoi nos variable de retour error ou success callBack*}
				{$error['file']}			
			    </div>
			{/if}
		    
		    </div>
		    
		    <div class="clearfix"></div>
		    
		</div>
		
		{if $getListFiles->count > 0}
		<div class="span12 margin-top-20">
		    
		    <div class="span2">
			<h4>{$globale_trad->label_rea}</h4>
		    </div>
		    
		    <ul id="realisation" class="padding-0 span7">
		    {foreach $getListFiles->files as $realisation}
			    <li class="span3">
				
				<div class="inner">

				    <a href="{$realisation->getPath()}" target="_blank" class="{if $realisation->getType()==1}fancy-image{/if}">
					<img src="{$realisation}" alt="pj" class="span12" title="pj">
				    </a>
				    <p class="span12 margin-0">
					<label class="checkbox inline">
					<input type="checkbox" name="id_file[]" value="{$realisation->getId_file()}">
					Choisir
					</label>
				    </p>
				    
				    <div class="clearfix"></div>
		   
				</div>
				
			    </li>  
		    {/foreach}
		    
		    </ul>
		    
		    
		    <div class="clearfix"></div>
		    
		</div>
		{/if}
		{if $prestataire->getStructure()->id_structure == $typeAgence}
		    <div class="span12 margin-top-20"> 
			
			<div class="span2">
			    <h4>{$globale_trad->label_profils} </h4>
			</div>
			
			<ul class="span7">  
			    {foreach $getListProfils as $profil}
				<li>
				    <label class="checkbox inline">
					<input type="checkbox" name="id_prestataire_profil[]" value="{$profil->getId_profil()}">{$profil->getNom()} {$profil->getPrenom()} 
				    </label>
				    
				</li>
			    {/foreach}
			</ul>
			
			<div class="span3">
			    
			    {if isset($error['id_prestataire_profil'])}
				<div class="alert alert-error span12">
				    {*On envoi nos variable de retour error ou success callBack*}
				    {$error['id_prestataire_profil']}			
				</div>
			    {/if}
			
			</div>
			
		    <div class="clearfix"></div>
			
		    </div>
		
		{/if}
		   
		<div class="span12 margin-top-20">
		    
		    <div class="span2">
			<h4>{$globale_trad->label_comm}</h4>
		    </div>
		    
		    <div class="span7 padding-5">
			<textarea  name="rp_commentaire" class="span12 formModificationTxtArea" placeholder="{$trad->detailMissionCommDefaut}"></textarea>    
			<div class="clearfix"></div>
		    </div>
		    
		    <div class="clearfix"></div>
		    
		</div>
		
		<div class="span12 margin-top-20">
		    
		    <div class="span2"></div>
		    
		    <div class="span7 padding-5">
			<label class="rose_fonce " for="rp_reponse_mail"> {$globale_trad->label_rep_mail}</label>
			<input type="radio" name="rp_reponse_mail" checked="checked" value="1"> {$globale_trad->label_oui}
			<input type="radio" name="rp_reponse_mail" value="0"> {$globale_trad->label_non}	
		    </div>
		    
		    <div class="clearfix"></div>
		    
		</div>
		
		<button class="btn btn-primary  pull-right" type="submit">
			<span class="icon-pencil icon-white"></span>
                        {$trad->detailMissionSaveReponse}
		</button>

		
	    </form>
	    
	</div>
    </div>
</div>


