<!-- debut footer -->
      <!-- debut de pied de page -->
      <div class="row-fluid zonefooter padding-top-20">
        <div class="span2 footerlogo">
          <a class="brand " href="{getLink page="home"}"><img src="web/img/logo/logo_connected.png" alt="logo weme"/></a>
        </div>
        <div class="span2  footerlist">
	  <img src="web/img/pictoEntrepreneurFooter.png" alt="picto Entrepreneur" class="pull-left"/>
          <h2>{$footer_trad->footerEntrepreneur}</h2>
	 
	  <ul class="decalage">
	    <li><a href="{getLink page="new_mission"}">{$footer_trad->footerClientProjet}</a></li>
	    <li><a href="{getLink page="cvteque"}">{$footer_trad->footerClientCvtheque}</a></li>
	    <li><a href="{getLink page="services"}">{$footer_trad->footerClientSourcing}</a></li>
	  </ul>
       </div>
       
	<div class="span2 footerlist">
	  <img src="web/img/ico/ico_chaine.png" alt="picto Prestataire" class="pull-left"/>
          <h2>{$footer_trad->footerLiensUtiles}</h2>
		  <ul>
			<li><a href="{getLink page="about"}">{$footer_trad->footerAPropos}</a></li>
			<li><a href="{getLink page="mentions"}">{$footer_trad->footerMentions}</a></li>
			<li><a href="{getLink page="contact"}">{$footer_trad->footerContact}</a></li>
		  </ul>
        </div>
      </div>
      <!-- fin du pied de page -->
      <footer class="row-fluid">
        <div class="span5 copyright"><p>{$footer_trad->footerCopyright}</p></div><div class="span2 offset5 socialfooter"><a href=""><img alt="twitter" src="web/img/pictoTwitter.png"/></a> <a href=""><img alt="facebook" src="web/img/pictoFb.png"/></a> <a href=""><img alt="linkedin" src="web/img/pictoLinkedin.png"/></a> <a href=""><img alt="googleplus" src="web/img/pictoGoogle.png"/></a></div>
      </footer>
      <!-- fin du footer -->

    </div> <!-- /container -->