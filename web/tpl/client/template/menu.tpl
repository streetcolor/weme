<div class="span2 menuVertical">
        <div class="toggle"></div>       
        <ul class="span12 nav nav-tabs nav-stacked row-fluid">
                
            <li {if $smarty.get.page == 'home' || !$smarty.get.page}class="actif"{/if}>
                <a href="{getLink page="home"}">
                        <span class="margin-right-10 ico35 calendar pull-left"></span>
                        <span class="text">{$header_trad->menu_tableau_bord}</span>
                        <span class="clearfix"></span>
                </a>
            </li>
            <li {if $smarty.get.page == 'moncompte' || $smarty.get.page == 'edit_moncompte'}class="actif"{/if}>
                <a href="{getLink page="edit_moncompte"}">
                        <span class="margin-right-10 ico35 buddy pull-left"></span>
                        <span class="text">{$header_trad->menu_mon_compte}</span>
                        <span class="clearfix"></span>
                </a>
            </li>
            <li {if $smarty.get.page == 'cvteque'}class="actif"{/if}>
                <a href="{getLink page="cvteque"}">
                        <span class="margin-right-10 ico35 paper pull-left"></span>
                        <span class="text">{$header_trad->menu_cvteque}</span>
                        <span class="clearfix"></span>
                </a>
            </li>
            <li {if $smarty.get.page == 'missions'}class="actif"{/if}>
                <a href="{getLink page="missions"}">
                        <span class="margin-right-10 ico35 eye pull-left"></span>
                        <span class="text">{$header_trad->menu_gerer_projets}</span>
                        <span class="clearfix"></span>
                </a>
            </li>
            <li {if $smarty.get.page == "edit_mission" or $smarty.get.page == "new_mission"}class="actif"{/if}>
                <a href="{getLink page="new_mission"}">
                       <span class="margin-right-10 ico35 zoom pull-left"></span>
                        <span class="text">{$header_trad->menu_publier_projet}</span>
                        <span class="clearfix"></span>
                </a>
            </li>
            <li {if $smarty.get.page == 'new_manager'}class="actif"{/if}>
                <a href="{getLink page="new_manager"}">
                        <span class="margin-right-10 ico35 buddy pull-left"></span>
                        <span class="text">{$header_trad->menu_managers}</span>
                        <span class="clearfix"></span>
                </a>
            </li>
            <li {if $smarty.get.page == 'my_profils'}class="actif"{/if}>
                <a href="{getLink page="my_profils"}">
                        <span class="margin-right-10 ico35 buddy pull-left"></span>
                        <span class="text">{$header_trad->menu_favoris}</span>
                        <span class="clearfix"></span>
                </a>
            </li>
            <li {if $smarty.get.page == 'entretiens'}class="actif"{/if}>
                <a href="{getLink page="new_entretien"}">
                        <span class="margin-right-10 ico35 webcam pull-left"></span>
                        <span class="text">{$header_trad->menu_mes_entretiens}</span>
                        <span class="clearfix"></span>
                </a>
            </li>
            <li {if $smarty.get.page == 'new_message' || $smarty.get.page == 'new_message'}class="actif"{/if}>
                <a href="{getLink page="new_message"}">
                        <span class="margin-right-10 ico35 mail pull-left"></span>
                        <span class="text">{$header_trad->menu_messages_modele}</span>
                        <span class="clearfix"></span>
                </a>
            </li>
            <li>
                <div class="text-center">
                <a href=""><img alt="twitter" src="web/img/pictoTwitter.png"/></a>
                <a href=""><img alt="facebook" src="web/img/pictoFb.png"/></a>
                <a href=""><img alt="linkedin" src="web/img/pictoLinkedin.png"/></a>
                <a href=""><img alt="googleplus" src="web/img/pictoGoogle.png"/></a>
                </div>
                <div class="clearfix"></div>
            </li>
        </ul>
    </div>