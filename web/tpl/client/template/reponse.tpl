{*Si $id_reponse nus somme sur la page reponse cote client et donc on affiche pas la mission courante dans les archives...*}
{if $reponse->getId_reponse() != $id_reponse}
	
	<tr class="MissionsListBackground ">
		<td height="20" colspan="{if  $page ==  "responses" or $page == "response"  }6{else}{if $page == "home"}5{else}4{/if}{/if}" class="last">
			<a href="{getLink page="response" params="id_mission_reponse="|cat:$reponse->getId_reponse()|cat:"&id_mission="|cat:$reponse->getId_mission() crypt="true"}" class="linkCell"></a>
		</td>
	</tr>
	
	<tr class="MissionsListBackground">
		
		{if $page != "home"  && $page != "response" }
			<td>
				
				<input type="checkbox" name="id_mission_reponse[]" value="{$reponse->getId_reponse()}">
				
			</td>
		{/if}
		<td class="">
			<a href="{getLink page="response" params="id_mission_reponse="|cat:$reponse->getId_reponse()|cat:"&id_mission="|cat:$reponse->getId_mission() crypt="true"}" class="linkCell">
				<h4 class="blue">{$reponse->getStatut()->statut}</h4>
				<p class="black">{$reponse->getReponse()|truncate:300|@strip_tags}</p>
			</a>
			
		</td>
		
		<td class="" align="center">
			<a href="{getLink page="response" params="id_mission_reponse="|cat:$reponse->getId_reponse()|cat:"&id_mission="|cat:$reponse->getId_mission() crypt="true"}" class="linkCell">
			<p class="black">{$reponse->getTarif(true)}</p>
			</a>
		</td>
		
		<td class="" align="center">
			<a href="{getLink page="response" params="id_mission_reponse="|cat:$reponse->getId_reponse()|cat:"&id_mission="|cat:$reponse->getId_mission() crypt="true"}" class="linkCell">
			<p>{$reponse->getDate()}</p>
			</a>
		</td>
	
		<td class="{if  $page == "response" }last{/if}" align="center">
			<a href="{getLink page="response" params="id_mission_reponse="|cat:$reponse->getId_reponse()|cat:"&id_mission="|cat:$reponse->getId_mission() crypt="true"}" class="linkCell">
			<p class="black">{$reponse->getNom()} {$reponse->getPrenom()}</p>
			</a>
		</td>
		{if  $page != "response" and $page != "responses" }
		<td class=" last" align="center">
			<a href="{getLink page="response" params="id_mission_reponse="|cat:$reponse->getId_reponse()|cat:"&id_mission="|cat:$reponse->getId_mission() crypt="true"}" class="linkCell">

			<p class="padding-left-10 padding-right-10">
			{$reponse->getTitre()}
			</p>
			</a>
		</td>
		{/if}
		{if  $page ==  "responses" or $page == "response"  }
		<td class=" last" align="center">
			<p>
			{if $reponse->getPieceJointe()->getId_file()}
			    
				<a target="_blank" href="{$reponse->getPieceJointe()->getPath()}">
				    <button class="btn btn-primary " type="button">
					<span class="icon-eye-open icon-white"></span>
					{$globale_trad->bt_detail}
				    </button>
				</a>
			    
			{else}
				-
			{/if}
			</p>
			
		</td>
		{/if}
	
	
	</tr>
	
	<tr class="MissionsListBackground ">
		<td height="20" colspan="{if  $page ==  "responses" or $page == "response"  }6{else}{if $page == "home"}5{else}4{/if}{/if}"class="last">
			<a href="{getLink page="response" params="id_mission_reponse="|cat:$reponse->getId_reponse()|cat:"&id_mission="|cat:$reponse->getId_mission() crypt="true"}" class="linkCell"></a>
		</td>
	</tr>
	
	<tr>
		<td colspan="{if  $page ==  "responses" or $page == "response"  }6{else}{if $page == "home"}5{else}4{/if}{/if}" class="last MissionsListActions">
			
			<a class="pull-left" href="{getLink page="response" params="id_mission_reponse="|cat:$reponse->getId_reponse()|cat:"&id_mission="|cat:$reponse->getId_mission() crypt="true"}">
				<span class="ico35 detail pull-left"></span>
				<span class="pull-left">{$globale_trad->propositionDetailBt}</span>
			</span>
			<a class="pull-left" href="{getLink page="responses" action="sendCopy" params="id_mission_reponse="|cat:$reponse->getId_reponse()|cat:"&id_mission="|cat:$reponse->getId_mission()|cat:"&mode="|cat:$mode|cat:"&paginate="|cat:$pageCourante crypt="true"}">
				<span class="ico35 envelop pull-left"></span>
				<span class="pull-left">{$globale_trad->send_me}</span>
			</span>
			{if !$reponse->getArchiver()}
				<a class="pull-left" href="{getLink page="responses" action="archiver" params="id_mission_reponse="|cat:$reponse->getId_reponse()|cat:"&id_mission="|cat:$reponse->getId_mission()|cat:"&mode="|cat:$mode|cat:"&paginate="|cat:$pageCourante crypt="true"}">
					<span class="ico35 archive pull-left"></span>
					<span class="pull-left">{$globale_trad->bt_archiver}</span>	
				</a>
			{else}
				<a class="pull-left" href="#">
					<span class="ico35 archive pull-left"></span>
					<span class="pull-left"></span>{$globale_trad->label_archiver} {$reponse->getArchiver()}</span>
				</a>
			{/if}	
			
			<a  class=" pull-left" href="{getLink page='new_entretien'  params="id_prestataire_profil="|cat:$reponse->getId_profil()}">
				<span class="ico35 cam pull-left"></span>
				<span class="pull-left">{$globale_trad->select_entretien}</span>
		</span>
		
			
		</td>
	</tr>
{/if}