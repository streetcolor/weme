<div id="header" class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid row-fluid">

          <div class="span2"><a class="brand" href="{getLink page="home"}"><img src="web/img/logo/logo_connected.png" alt="weme dashboard" title="weme dashboard"></a></div>
          <div class="span10">
            <ul class="nav">
          
		  <li class="span6">
			<span class="round-count">{$smarty.session.compteur.all_profils}</span>
			<a class="{if $smarty.request.page == 'cvteque' }active{/if}" href="{getLink page="cvteque" action="search" params="id_prestataire_profil="|cat:$smarty.session.compteur.match_profils crypt=true}">
			      <span class="ico projectIco"></span>
			      <span class="text">CVMatch!</span>
			</a>
		  </li>
	
		  <li class="span6">
			<span class="round-count">{$smarty.session.compteur.all_projets}</span>
			<a class="{if $smarty.request.page == 'missions' }active{/if}" href="{getLink page="missions"}">
			      <span class="ico projectIco"></span>
			      <span class="text">{$header_trad->HpClientVosMissions}</span>
			</a>
		  </li>
	
           
            </ul>
	    <div class="span4 offset1 row-fluid headerProfil">
              <div class="pull-left">
		  <div class="padding-right-10">
			<img class="headerPhoto img-polaroid" src="{$avatar}" alt=""/>
		  </div>
	      </div>
	      <div class="span7 headerProfil2">
		<h1 class="headerProfilNom">{$client->getPrenom()} {$client->getNom()}</h1>
		<span class="headerProfilEmail">{$client->getEmail()}</span>
		<br/>
		<p class="padding-top-10">
		  <a class="white" href="{getlink page='edit_moncompte'}">> {$header_trad->bt_edit_profil}</a>
		</p>
	      </div>
	      <div class=" headerProfilDeco">
		<a href="{getlink page='home' action='loggout'}"><img src="web/img/headerProfilDeco.png" alt="deconnexion"/></a>
	      </div>
            </div>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>