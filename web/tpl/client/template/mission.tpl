<tr class="MissionsListBackground">
	<td height="20" class=" last" colspan="6"><a href="{getLink page='edit_mission' action='editMission' params="id_mission="|cat:$mission->getId_mission() crypt=true}" class="linkCell"></a></td>
</tr>

<tr class="MissionsListBackground">
	
	<td class="">
	    {if $mission->getStatut()->id_statut >1 && $mission->getStatut()->id_statut <4 }
	    <input type="checkbox" value="{$mission->getId_mission()}" name="id_mission[]">
	    {/if}
	</td>
	
	<td class="">
		<a href="{getLink page='edit_mission' action='editMission' params="id_mission="|cat:$mission->getId_mission() crypt=true}" class="linkCell">

		<span class="flag missionS{$mission->getStatut()->id_statut}  pull-left"></span>

		<h4 class="blue">{$mission->getTitre()}</h4>
		<p class="padding-right-10">{$mission->getDescription()|truncate:300|@strip_tags}</p>
		</a>
	</td>

	<td class="" align="center">
			<a href="{getLink page='edit_mission' action='editMission' params="id_mission="|cat:$mission->getId_mission() crypt=true}" class="linkCell">

			<p>{$mission->getLocalite()}</p>
			
			</a>
			
	</td>

	<td class="" align="center">
		<a href="{getLink page='edit_mission' action='editMission' params="id_mission="|cat:$mission->getId_mission() crypt=true}" class="linkCell">

			<p>{$mission->getTarif(true)}</p>
			</a>
	</td>

	<td class="" align="center">
		<a href="{getLink page='edit_mission' action='editMission' params="id_mission="|cat:$mission->getId_mission() crypt=true}" class="linkCell">

			<p>{$globale_trad->label_du} {$mission->getDebut()}<br/>{$globale_trad->label_au} {$mission->getFin()}</p>
			
		</a>
	</td>

	<td class=" last" align="center">
		<a href="{getLink page='responses' params="id_mission="|cat:$mission->getId_mission() crypt=true}" class="linkCell">

			<p>{$mission->getReponses(true)} {$globale_trad->label_reponse}</p>
			
		</a>
	</td>
</tr>

<tr class="MissionsListBackground">
	<td height="20" class=" last" colspan="6"><a href="{getLink page='edit_mission' action='editMission' params="id_mission="|cat:$mission->getId_mission() crypt=true}" class="linkCell"></a></td>
</tr>

<tr>
	<td colspan="6" class="last MissionsListActions">
	
	<a class='pull-left' href="{getLink page='mission' params="id_mission="|cat:$mission->getId_mission() crypt=true}">
		<span class="ico35 detail pull-left"></span>
		<span class="pull-left">{$globale_trad->missionDetail}</span>
	</a>
	{if $mission->getStatut()->id_statut != 4 && $mission->getStatut()->id_statut != 1}
	    <a class="pull-left" href="{getLink page='edit_mission' action='editMission' params="id_mission="|cat:$mission->getId_mission() crypt=true}">
		<span class="ico35 edition pull-left"></span>
		<span class="pull-left">{$globale_trad->projetApercuModifier}</span>
	    </a>
	    <a  class="pull-left" href="{getLink page='edit_mission' action="duplicateMission" params="id_mission="|cat:$mission->getId_mission()|cat:"&mode="|cat:$mode|cat:"&paginate="|cat:$pageCourante  crypt=true}">
		<span class="ico35 duplicate pull-left"></span>
		<span class="pull-left">{$globale_trad->projetApercuDupliquer}</span>
	    </a>
	    <a  class="pull-left" href="{getLink page='missions' action="changeStatus" params="id_mission="|cat:$mission->getId_mission()|cat:"&id_mission_statut=4"|cat:"&mode="|cat:$mode|cat:"&paginate="|cat:$pageCourante  crypt=true}">
		<span class="ico35 cloturer pull-left"></span>
		<span class="pull-left">{$globale_trad->projetApercuCloturer}
	    </a>
	    {if  $mission->getStatut()->id_statut == 2}
	    <a  class="pull-left" href="{getLink page='my_profils' action="proposeMission" params="id_mission="|cat:$mission->getId_mission() crypt=true}">
		<span class="ico35 return pull-left"></span>
		<span class="pull-left">{$globale_trad->bt_proposer} </span>
	    </a>
	    {/if}
	    {if $mission->getNotificationEmail()>0}
		<a  class="pull-left" href="{getLink page='missions' action="stopNotifications" params="id_mission="|cat:$mission->getId_mission()|cat:"&mi_reponse_mail=0"|cat:"&mode="|cat:$mode|cat:"&paginate="|cat:$pageCourante  crypt=true}">
		    <span class="ico35 startMail pull-left"></span>
		    <span class="pull-left">{$globale_trad->bt_desactiverMail}</span>
		</a>
	    {else}
		<a  class="pull-left" href="{getLink page='missions' action="stopNotifications" params="id_mission="|cat:$mission->getId_mission()|cat:"&mi_reponse_mail=1"|cat:"&mode="|cat:$mode|cat:"&paginate="|cat:$pageCourante  crypt=true}"">
		    <span class="ico35 stopMail  pull-left"></span>
		    <span class="pull-left">{$globale_trad->bt_activerMail}</span>
		</a>
	    {/if}
	{/if}
	{if ($mission->getRepublication()<=2)  && ($mission->getStatut()->id_statut != 1)  && ($mission->getStatut()->id_statut != 4)}
	<a  class="pull-left" href="{getLink page='edit_mission' action="republishMission" params="id_mission="|cat:$mission->getId_mission()|cat:"&mode="|cat:$mode|cat:"&paginate="|cat:$pageCourante  crypt=true}">
	    <span class="ico35 republish pull-left"></span>
	    <span class="pull-left">{$globale_trad->projetApercuRepublier}</span>
	</a>
	{/if}
	{if $mission->getStatut()->id_statut == 3}
	    <a  class="pull-left" href="{getLink page='missions' action="changeStatus" params="id_mission="|cat:$mission->getId_mission()|cat:"&id_mission_statut=2"|cat:"&mode="|cat:$mode|cat:"&paginate="|cat:$pageCourante crypt=true}"">
		<span class="ico35 return pull-left"></span>
		<span class="pull-left">{$globale_trad->projetApercuReprendre}</span>
	    </a>
	{else if $mission->getStatut()->id_statut == 2}
	    <a  class="pull-left" href="{getLink page='missions' action="changeStatus" params="id_mission="|cat:$mission->getId_mission()|cat:"&id_mission_statut=3"|cat:"&mode="|cat:$mode|cat:"&paginate="|cat:$pageCourante crypt=true}"">
		<span class="ico35 pause pull-left"></span>
		<span class="pull-left">{$globale_trad->projetApercuSuspendre}</span>
	    </a>
	{/if}
	
	{if $mission->getReponses(true)>0}
	    <a  class="pull-left" href="{getLink page='responses' params="id_mission="|cat:$mission->getId_mission() crypt=true}">
		<span class="ico35 detail pull-left"></span>
		<span class="pull-left">{$globale_trad->projetApercuVoirReponses}</span>
	    </a>
	{/if}
	
	</td>
</tr>
