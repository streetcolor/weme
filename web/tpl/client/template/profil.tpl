<tr class="MissionsListBackground">
	<td class=" last" height="20" colspan="7"></td>
</tr>

<tr class="MissionsListBackground">
	{*hideSerach est appeler dans le cadre d'une propositoion mission aux favoris*}
	{if isset($hideSearch)}
	<td class="">
	    <input type="checkbox" value="{$profil->getId_profil()}" name="id_prestataire_profil[]">
	</td>
	{/if}
	<td class="{if !isset($hideSearch)}first{/if} ">
		
		{$globale_trad->label_nom} : {$profil->getNom()} {$profil->getPrenom()}
		<br> 
		{$globale_trad->label_tel_short} : {$profil->getFixe(true)}
		<br>
		{$globale_trad->label_dispo} : {$profil->getDisponibilite()}
		<br>
		{$globale_trad->label_tjm} : {$profil->getTjm(true)}
		
	</td>
	
	<td class="span5" align="">
		<h4 class="blue">{$profil->getTitre()}</h4>
		<p>
			{$profil->getDescription()|truncate:200}
		</p>
	</td>
	
	<td class="" align="">
		<ul>
		{foreach $profil->getLocalites()->localites as $localite}
				{if $localite@iteration > 5}<li>...</li>{break}{/if}
				<li>{$localite}</li>
		{/foreach}
		</ul>
	</td>
	
	<td class=" last" align="">
		<ul>
		{foreach $profil->getCompetences()->competences as $competence}
				{if $competence@iteration > 5}<li>...</li>{break}{/if}
				<li>{$competence->adm_competence}</li>
		{/foreach}
		</ul>
	</td>


</tr>
{if $page != "new_entretien"}
<tr class="MissionsListBackground">
	<td class=" last" height="20" colspan="7"></td>
</tr>

<tr>
	
	<td colspan="5" class="last MissionsListActions">
		

		{if in_array($profil->getId_profil(), $favorisProfil)}
		<a class="pull-left delEntry"  href="{getLink page='cvteque' action="removeFavori" params="id_prestataire_profil="|cat:$profil->getId_profil()|cat:"&mode="|cat:$mode|cat:"&paginate="|cat:$pageCourante crypt=true}">
				<span class="ico35 delFav pull-left"></span>
				<span class="pull-left">{$globale_trad->select_supp_fav}</span>
		</span>
		{else}
		<a class="pull-left " href="{getLink page='cvteque' action="addFavori" params="id_prestataire_profil="|cat:$profil->getId_profil()|cat:"&mode="|cat:$mode|cat:"&paginate="|cat:$pageCourante crypt=true}">
				<span class="ico35 addFav pull-left"></span>
				<span class="pull-left">{$globale_trad->btSave}
		</span>
		{/if}
		<a class="pull-left " href="{getLink page='message' action="send" params="id_prestataire_profil="|cat:$profil->getId_profil() crypt=true}">
				<span class="ico35 envelop  pull-left"></span>
				<span class="pull-left">{$globale_trad->select_send_msg}</span>
		</span>

		
		{if !$abonnement || !$profil->getActivePDF()}
				<a class="pull-left" href="web/scripts/cv.php?page=cv&action=cv&id_prestataire_profil={$profil->getId_profil()}">
					<span class="ico35 pdf pull-left"></span>
					<span class="pull-left">{$globale_trad->bt_dl_pdf}</span>
				</a>
			{else}
				{assign var='cv' value=$profil->getCV()}
				
				{if !$cv->getId_file()}
					<a class="pull-left" href="web/scripts/cv.php?id_prestataire_profil={$profil->getId_profil()}">
						<span class="ico35 pdf pull-left"></span>
						<span class="pull-left">{$globale_trad->bt_dl_pdf}</span>
					</a>
				{else}
					<a class="pull-left" href="{$cv->getPath()}">
						<span class="ico35 pdf pull-left"></span>
						<span class="pull-left">{$globale_trad->bt_dl_pdf}</span>
					</a>
				{/if}
			{/if}
		
		<a class="pull-left showCv" href="{getLink page='cv' action="cv" params="id_prestataire_profil="|cat:$profil->getId_profil()}{if isset($smarty.request.search['mots_cle'])}&mots_cle={$smarty.request.search['mots_cle']}{/if}">
				<span class="ico35 detail pull-left"></span>
				<span class="pull-left">{$globale_trad->voir_cv}</a>
		</span>
		<a  class=" pull-left" href="{getLink page='new_entretien'  params="id_prestataire_profil="|cat:$profil->getId_profil()}">
				<span class="ico35 cam pull-left"></span>
				<span class="pull-left">{$globale_trad->select_entretien}</span>
		</span>
		{if $profil->getJabber()->loggin && $client->getJabber()->loggin}
		<a class="tchat pull-left" data-loggin="{$profil->getJabber()->loggin}" href="#{$profil->getJabber()->loggin}">
			<span class="ico35 tchat pull-left"></span>
			<span class="pull-left">{$globale_trad->btTchater}</span>
		</span>
		{/if}
		
	</td>
</tr>
{/if}

