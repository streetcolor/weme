<div class="container-fluid row-fluid corps">
    
    {include file='web/tpl/client/template/menu.tpl'}
    
    <div class="span10 homeDroite">
        
	<div class="search">
            
	    <form class="form-inline homeDroiteForm" method="POST" action="{getlink page=$page  action='search' params="id_mission="|cat:$id_mission crypt=true}" >
                                
		<div class="row-fluid" id="filter">
		    
		    <div class="span12">
			
			<a href="{getlink page=$page  action='' params="id_mission="|cat:$id_mission crypt=true}">
			    <img class=" pull-left border-right-1 padding-0" src="web/img/ico/ico_reload.png" alt="reload" title="reload" />
			</a>
			
			<span class="pull-left label-form">{$globale_trad->label_publication}</span>
				
			<div class="border-right-1 pull-left ">
			    <div class="span12 formModificationSelectDiv">
				<select class="formModificationSelect" name="search[id_reponse_statut]">
				    <option value="">{$globale_trad->indifferent}</option>
				    {foreach $getListStatuts as $statut}
					<option {if $statut->id_reponse_statut==$smarty.session.search.id_reponse_statut}selected="selected"{/if}  value="{$statut->id_reponse_statut}">{$statut->adm_statut}</option>
				    {/foreach}
				</select>
			    </div>
			</div>
			
			<span class="pull-left label-form">{$globale_trad->label_date}</span>
			
			<div class="border-right-1 pull-left ">
			    
			    <div  data-date-format="dd/mm/yyyy" data-date="{$smarty.session.search['mi_debut']}" class=" span6 input-append date">
				<input type="text" value="{if $smarty.session.search['rp_fin']}{$smarty.session.search['rp_debut']}{else}{$globale_trad->label_du}{/if}" size="16" name="search[rp_debut]" class=" span12">
				<span class="add-on"><i class="icon-calendar"></i></span>
			    </div>
			    
			    <div  data-date-format="dd/mm/yyyy" data-date="{$smarty.request.search['rp_fin']}" class="  span6 input-append date">
				<input type="text" value="{if $smarty.session.search['rp_fin']}{$smarty.session.search['rp_fin']}{else}{$globale_trad->label_au}{/if}" size="16" name="search[rp_fin]" class=" span12">
				<span class="add-on"><i class="icon-calendar"></i></span>
			    </div>
		    
			    <div class="clearfix"></div>
			    
			</div>
			
			<input class=" pull-left last border-right-1 padding-0" type="image" src="web/img/ico/ico_search_form.png" alt="reload" title="reload" />
			
			<div class="clearfix"></div>
			
		    </div>
		
		</div>
		
            </form>
	    
        </div>
	

	{*Si la requete getLitsReponses() renvoi des response*}
	{if isset($count)}
	    <form class="form-inline " method="POST" action="{getLink page='responses' action="search" params="id_mission="|cat:$id_mission crypt=true}" >
		
		<h1 class="result">
		   Il y a <b> {$count}  {$globale_trad->label_proposition}</b> pour la mission {$listReponses[0]->getTitre()}
		</h1>
		
		{if isset($error['id_mission_reponse'])}
		    <div class="alert alert-error">
		    {$error['id_mission_reponse']}
		    </div>
		{/if}
		
		{$messageCallBack}
		
		<table width="100%" cellpadding="6" class=" mission">
		 
		    <thead class="MissionListHeader">
			
			<tr>
				<td class=""><input type="checkbox" class="checkall"></td>
				
				<td class="">{$globale_trad->label_reponse}</td>
		
				<td align="center" class="">{$globale_trad->label_tarif}</td>
				
				<td align="center" class="">{$globale_trad->label_date}</td>
				
				<td align="center" class="">{$globale_trad->profils}</td>
				
				<td align="center" class="last">{$globale_trad->label_pj}</td>
				
				
			</tr>
			
		    </thead>
		
		    <tbody>
			{foreach $listReponses as $reponse}
			    
			    {*Process qui permet de n'appeler qu'une seule fois les statuts reponses*}
			    {foreach $getListEntete as $entete}
				{if $entete ==  $reponse->getStatut()->id_statut}
				    
				    <tr class="MissionsListEntete">
					    
					    <td colspan="6" class="last ">{$reponse->getStatut()->statut}</td>
					    
				    </tr>
				    
				    {$getListEntete[$entete]=false}
				{/if}
			    {/foreach}
			    {*Fin du process*}
	    
			    {include file='web/tpl/client/template/reponse.tpl'}
    
			{/foreach}
		    </tbody>
		    
		    <tfoot>
		    
			<tr class="MissionsListPagination pagination">
		    
			    <td colspan="6" class="last" >
				    <ul>
				    {if $nbrPages > 1 && $pageCourante != 1} <li><a href="{getlink page=$page  action='search' params="&id_mission="|cat:$id_mission crypt=true}&paginate={$pageCourante-1}" class="blue"><i class="icon-chevron-left"></i> precedent</a></li> {/if}
				    
				    {section name=for start=1 loop=$nbrPages+1 step=1}
					{if $smarty.section.for.index==$pageCourante}
					    <li><span>{$smarty.section.for.index}</span></li>
					{else}
					    <li><a href="{getlink page=$page  action='search' params="&id_mission="|cat:$id_mission crypt=true}&paginate={$smarty.section.for.index}">{$smarty.section.for.index}</a></li>
					{/if}
				    {/section}
				    
				    {if $nbrPages >1 && $pageCourante != $nbrPages} <li><a href="{getlink page=$page  action='search' params="&id_mission="|cat:$id_mission crypt=true}&paginate={$pageCourante+1}" class="blue">suivant <i class="icon-chevron-right"></i></a></li> {/if}
				    </ul>
			    </td>
    
			</tr>
			
		    </tfoot>
		
		</table>
		
		<div id="filter" class="row-fluid homeDroiteForm">
		    
		    <div class="span12">
				
			    <div class="span3 margin-left-5 formModificationSelectDiv">

				<select name="id_reponse_statut" class="formModificationSelect">
					
				    <option name="">{$globale_trad->changeStatut}</option>
				    
				    {foreach $getListStatuts as $statut}
					<option value="{$statut->id_reponse_statut}">{$statut->adm_statut}</option>
				    {/foreach}
				    
				</select>
				    
				<input  type="hidden"  value="{$mode}" name="mode">
				<input  type="hidden"  value="{$paginate}" name="paginate">
				    
			    </div>
			    
			    <button type="submit"  name="action" value="changeStatus"  class="btn btn-secondary margin-left-10  pull-left">
				<span class=" icon-ok icon-white "></span>
				{$globale_trad->bt_ok}
			    </button>
				
		    
			    <button class="btn btn-secondary margin-left-10 "  name="action" value="sendCopy" type="submit">
				<span class="icon-envelope icon-white "></span>
				{$globale_trad->send_copy}
			    </button>
			    
		    </div>
				    
		</div>
	    </form>

	{else}            
                <div class="bgWhite resultSearchNotFound">{$globale_trad->result_no_propositions} <b>{$post["mots_cle"]}</b></div>  
        {/if}


</div>
<div class="clearfix"></div>
