<div class="container row-fluid corps">
    {include file='web/tpl/client/template/menu.tpl'}

    
    <div class="span10 homeDroite margin-top-20">        
        
	<form class="monCompteDroite span4" method="POST"  enctype="multipart/form-data">
            <h1>{$trad->clientMessageTitre1}</h1>
	    

	    <div class="span12">
		
		<div class="span12">
		    {$trad->clientMessageLabelTitre}
		</div>
		
		{if isset($error['id_client_message']) || isset($error['mess_titre'])}
		    <div class="span12">
			<div class="alert alert-error span12 margin-bottom-20">
			   {*On envoi nos variable de retour error ou success callBack*}
			   <p>{$error['id_client_message']}</p>
			   <p>{$error['mess_titre']}</p>
		   
			</div>
		    </div>
		{/if}
		
		<div class="span12">
		    <input  class="span12 formModification"  name="mess_titre"  type="text" value="{if $post['mess_titre']}{$post['mess_titre']}{else}{if isset($id_client_message)}{$editMessage['mess_titre']}{/if}{/if}"  placeholder="{$trad->clientMessageLabelTitre}">
		</div>
		
		<div class="clearfix"></div>
		
	    </div>
	    
	 
	    <div class="span12">
		
		<div class="span12">
		   {$trad->clientMessageLabelMessage}
		</div>
		
		{if isset($error['mess_message'])}
		    <div class="span12">
			<div class="alert alert-error span12 margin-bottom-20">
			   {*On envoi nos variable de retour error ou success callBack*}
			   <p>{$error['mess_message']}</p>
		   
			</div>
		    </div>
		{/if}
		
		
		<div class="span12">
		    <textarea  name="mess_message" class="wysywyg">{if $post['mess_message']}{$post['mess_message']}{else}{if isset($id_client_message)}{$editMessage['mess_message']}{/if}{/if}</textarea>
		</div>
		
		<div class="clearfix"></div>
		
	    </div>
	    
	    <div class="clearfix"></div>
	    
	    {if isset($id_client_message)}
		<a href="{getLink page="new_message"}">
		    <button type="button" class="btn btn-primary margin-top-20 pull-left" >
			<span class=" icon-repeat icon-white"></span>
			{$globale_trad->bt_retour}
		    </button>
		</a>
	    {/if}
	    
	    <button value="{if isset($id_client_message)}editMessage{else}addMessage{/if}" name="action" class="btn btn-primary margin-top-20 pull-right" type="submit">
		<span class=" icon-pencil icon-white"></span>
		{$globale_trad->bt_enregistrer}
	    </button>
	    
	    <div class="clearfix"></div>

	</form>
	
	<div class="span8  pull-right padding-left-20">
	    
	    {$messageCallBack}
	    
	    <div class="monCompteDroite ">
	    
		<h1>{$trad->clientMessageTitre2}</h1>
		
		<p>{$trad->clientMessageIntro} </p>
		
	    </div>
	    
	    <div class="monCompteDroite">
	    <table width="100%" cellpadding="5" class=" mission">
		    <thead class="MissionListHeader">
			<tr>

			    <td align="center" class="">{$globale_trad->label_email}</td>
		    
			    <td align="center" class="last" width="75">{$globale_trad->label_action}</td>
				
			</tr>
		    </thead>
		
		    <tbody>
			{if $countMessages>0}
			    {foreach $listMessages as $message}            
				<tr style="border-top:1px solid #D8D8D8">

				    <td class="">{$message['mess_titre']}</td>
		    
		    
				    <td align="center" class="last">
					
					
					 <div class="btn-group group-link ">
					    <a class="btn btn-primary dropdown-toggle white"  data-toggle="dropdown"  href="#"><i class="icon-tasks icon-white"></i> {$globale_trad->label_action}</a>
					    <a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
					    <ul class="dropdown-menu">
						<li><a href="{getLink page="new_message" action="editMessage" params="id_client_message="|cat:$message['id_client_message'] crypt=true}"><i class="icon-pencil"></i> {$globale_trad->bt_modifier}</a></li>
						<li><a class="delEntry" href="{getLink page="new_message" action="removeMessage"  params="id_client_message="|cat:$message['id_client_message'] crypt="true"}"><i class="icon-trash "></i> {$globale_trad->bt_supprimer}</a></li>
					    </ul>
					</div>
					
				
				    </td>
					
				</tr>
				   
			    {/foreach}
			
			  
				
			  {else}            
					<tr align="center">
					    
					    <td class="last MissionsListActions" colspan="5">
						{$globale_trad->notifAucunMailType}
					    </td>
				
					</tr>
				{/if}	
		    </tbody>
		
		
		</table>
	    
	    </div>
	</div>
    </div>
</div>
<div class="clearfix"></div>