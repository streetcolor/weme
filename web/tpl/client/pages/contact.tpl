<div class="container-fluid row-fluid corps">
    {include file='web/tpl/client/template/menu.tpl'}
    
<div class="span10 row-fluid homeDroite">
   
   
     <form enctype="multipart/form-data" method="POST" class="span12">
            
            <div class="monCompteDroite  margin-top-20">
               
                
                <h1 class="result">{$trad->titreContact}</h1>
		
		

		               
		    <p class="padding-bottom-30">
			{$trad->descriptionCorps}

		    </p>
		    
		    <p>
			<label>{$globale_trad->label_nom}  </label>
		    </p>
		    <p class="span5">
			<input value="{$getMembre->getNom()}" disabled="disabled" type ="text" class="span12 formModification" >
		    </p>
		    <div class="clearfix"></div>
		    
		    <p>
			<label>{$globale_trad->label_email}  </label>
		    </p>
		    <p class="span5">
			<input value="{$getMembre->getEmail()}" disabled="disabled" type ="text" class=" span12 formModification" >
		    </p>
		    <div class="clearfix"></div>
	
		    
		    <p>
			<label>{$globale_trad->label_sujet}   <em class="smallText red ">*</em></label>
		    </p>
		    {if isset($error['mail_titre'])}
			<div class="span5 padding-bottom-10">
			    <div class="alert alert-error  margin-0">
				{*On envoi nos variable de retour error ou success callBack*}
				<p>{$error['mail_titre']}</p>
			    </div>
			</div>
			<div class="clearfix"></div>
		    {/if}
		    <p class="span5">
			<input type ="text" value="{if $post['mail_titre']}{$post['mail_titre']}{/if}" class="span12 formModification" name="mail_titre">
			<br class="clear">
		    </p>
		    
		    <div class="clearfix"></div>
		    

		    <p>
			 <label>{$globale_trad->label_message}  <em class="smallText red ">*</em> </label>
		    </p>
		    {if isset($error['mail_message'])}
			<div class=" padding-bottom-10">
			    <div class="alert alert-error  margin-0">
			       {*On envoi nos variable de retour error ou success callBack*}
			       <p>{$error['mail_message']}</p>	
			    </div>
			</div>
		    {/if}
		    <div>
			<textarea  name="mail_message" class="wysywyg">{if $post['mail_message']}{$post['mail_message']}{/if}</textarea>
		    </div>
		    <div class="clear"></div>
		    <p class="smallText red alignLeft  padding-top-10">
			<i><em class="smallText red ">*</em>&nbsp; {$globale_trad->label_obligatoire} </i>
		    </p>
    
		    <button class="btn btn-primary margin-top-20 pull-right" type="submit">
			<span class=" icon-pencil icon-white"></span>
			{$globale_trad->bt_envoyer} 

		    </button>
		    
		    <div class="clearfix"></div>
		    
	
		
	    </div>
		
    </form>
     
</div>

<div class="clearfix"></div>
