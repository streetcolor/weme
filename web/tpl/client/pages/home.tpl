<div class="container-fluid row-fluid corps">
    
    {include file='web/tpl/client/template/menu.tpl'}
    
    <div class="span10 homeDroite">
        
	<div class="search margin-bottom-20">
            
	    <form class="form-inline homeDroiteForm" method="POST" action="{getlink page='cvteque'  action='search'}" >
                
		<input type="text" name="search[mots_cle]" value="{$smarty.request.search['mots_cle']}"placeholder="{$globale_trad->searchCv}" class="span12 homeDroiteSearch"/>
	    
		<div class="row-fluid" id="filter">
		    
		    <div class="span12">
			<a href="{getlink page='cvteque'  action=''}">
			    <img class=" pull-left border-right-1 padding-0" src="web/img/ico/ico_reload.png" alt="reload" title="reload" />
			</a>
			<span class="pull-left label-form">{$globale_trad->label_lieu}</span>
		    
			<div class="border-right-1 pull-left ">
			
			    <input  id="geocomplete" type="text" value="{$smarty.session.search['lo_region_short']}" class="span12 formModification "/>
			    <input type="hidden" name="search[lo_departement_short]" id="lo_departement_short" value="{$smarty.session.search['lo_departement_short']}">
			    <input type="hidden" name="search[lo_region_short]" id="lo_region_short" value="{$smarty.session.search['lo_region_short']}">
			    <input type="hidden" name="search[lo_pays_short]" id="lo_pays_short" value="{$smarty.session.search['lo_pays_short']}">
			  
			    <div class="clearfix"></div>
			</div>
			
			
			<div class="border-right-1 padding-left-10 pull-left ">
			    <div class="span12 formModificationSelectDiv">
				<select class="formModificationSelect" name="search[id_experience]">
				    <option value="">{$globale_trad->label_exp}</option>
				    {foreach  $listExperiences as $experience}
				       <option {if $experience->id_experience==$smarty.session.search.id_experience}selected="selected"{/if} value="{$experience->id_experience}">{$experience->adm_experience}</option>
				    {/foreach}
				 </select>
			    </div>
			    <div class="clearfix"></div>
			</div>

					    
			<div class="border-right-1 padding-left-10  pull-left ">
			    <div class="span12 formModificationSelectDiv">
				<select class="formModificationSelect" name="search[modifie_le]">
				    {foreach  from=$listPublications key=k item=v}
				    <option {if $k==$smarty.session.search.modifie_le}selected="selected"{/if} value="{$k}">{$v}</option>
				    {/foreach}
				</select>
			    </div>
			    <div class="clearfix"></div>
			</div>
		    
			<span class="pull-left label-form">{$globale_trad->label_dispo}</span>
			
			<div class="border-right-1 pull-left ">
			    <div  data-date-format="{$datepicker.dateFormat}" data-date="{$smarty.request.search['mi_debut']}" class=" input-append date">
				<input type="text" value="{if $smarty.request.search['disponibilite']}{$smarty.session.search['disponibilite']}{else}{$globale_trad->label_du}{/if}" size="16" name="search[disponibilite]" class=" span12">
				<span class="add-on"><i class="icon-calendar"></i></span>
			    </div>
			</div>
			
			<input class=" pull-left last border-right-1 padding-0" type="image" src="web/img/ico/ico_search_form.png" alt="reload" title="reload" />
		    
		    </div>
		
		</div>
		
            </form>
        </div>
	    
		
    <div class="span9 ">
	
	{if isset($error['file']) || isset($error['file_cv']) || isset($error['file_plaquette'])}
	<div class="alert alert-error">
	    {$error['file']}
	    {$error['file_cv']}
	    {$error['file_plaquette']}
	</div>
	{/if}
	{$messageCallBack}
        <div class="accordion" id="accordion2">
            <div class="accordion-group">
                <div class="accordion-heading">
                    <a class="accordion-toggle white" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
		      <div class="span5">
			<span class="pull-left margin-right-10  ico35 project"></span>
			<h1 lass="span10">{$trad->HpClientDerniersProfils}</h1>
			<div class="clearfix"></div>
		      </div>
		      <div class="clearfix"></div>
		      <span class=" ico35 button"></span>
                    </a>
                </div>
                <div id="collapseTwo" class="accordion-body in collapse" style="height: auto;">
                    <div class="accordion-inner">
			<table width="100%" cellpadding="5" class="mission">
			    <tbody>
				{if isset($listProfils.count)}
				    
					    {if !$listProfils.matchingResult}
					    <tr align="center">
						<td class="last MissionsListActions" colspan="5">
						    {$trad->HpClientNoProfilMatch}
						</td>
				    
					    </tr>
					    {/if}
					    {foreach $listProfils.profils as $profil}   
						{if $profil@iteration > 3}{break}{/if} 
						{include file='web/tpl/client/template/profil.tpl'}
					    {/foreach}
					
				{else}            
					<tr align="center">
					    
					    <td class="last MissionsListActions" colspan="5">
						{$trad->HpClientNoProfil}
					    </td>
				
					</tr>
				{/if}	
			    </tbody>
			</table>
                    </div>
                    <div class="accordionMore"><a href="{getlink page='cvteque'}"><button class="buttonCollapseMore"><b>></b> {$trad->HpClientAllProfil}</button></a></div>
                </div>
            </div>
        </div>
       <div class="accordion" id="accordion3">
            <div class="accordion-group">
                <div class="accordion-heading">
                    <a class="accordion-toggle white" data-toggle="collapse" data-parent="#accordion3" href="#collapseTree">
		      <div class="span5">
			<span class="pull-left margin-right-10 ico35 responses"></span>
			<h1 lass="span10">{$trad->HpClientPropositionEnCours}</h1>
			<div class="clearfix"></div>
		      </div>
		      <div class="clearfix"></div>
		      <span class=" ico35 button"></span>
                    </a>
                </div>
                <div id="collapseTree" class="accordion-body in collapse" style="height: auto;">
                    <div class="accordion-inner">
			<table width="100%" cellpadding="5"  class="mission">
			    <tbody>
				{if $countReponses >0}
				    
					    {foreach $reponses->reponses as $reponse}
						{if $reponse@iteration > 3}{break}{/if} 
						{include file='web/tpl/client/template/reponse.tpl'}
					    {/foreach}
					
				{else}
				    <tr align="center">
					<td class="last MissionsListActions" colspan="5">
					    {$trad->HpClientNoReponse} 
					</td>
			    
				    </tr>
				{/if}
			    </tbody>
			</table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- fin partie droite -->
    <div class="span3 pull-right">
	
	<div class="padding-left-20">
	
	    <!-- suivi proposition -->
	    <div class="DashboardDroitInfoProposition">
		<h1 class="margin-bottom-20">{$trad->HpClientSuiviMission}</h1>
		{foreach $getListStatuts as $statut}
		<div class="DashboardDroitInfoPropositionP"><div class="PText span10"><a href="{getLink page='missions' action='search' params="id_mission_statut="|cat:$statut->id_mission_statut}">{$statut->adm_statut}</a></div><a href="{getLink page='missions' action='search' params="id_mission_statut="|cat:$statut->id_mission_statut}" class="PCounter span2">{$statistiques[$statut->id_mission_statut]}</a></div>
		{/foreach}
		<a href="{getLink  page="my_profils"}" class="span12 buttonProjetSaved">{$trad->HpClientProfilSauvegarde}</a>
		<div class="clearfix"></div>
	    </div>
	    <!-- contactez nous -->
	    <div class="DashboardDroitInfoContact">
		<span class="ico35 contact margin-right-10"></span>
		<h1>{$globale_trad->label_contact}</h1>
		{$trad->HpClientContact}
		<a href="{getLink  page="contact"}" class="span12 buttonProjetSaved">{$globale_trad->bt_contact}</a>
		<div class="clearfix"></div>
	    </div>
	    
	</div>
    </div>
</div>

<div class="clearfix"></div>