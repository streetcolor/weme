<div class="container-fluid row-fluid corps">
    
    {include file='web/tpl/client/template/menu.tpl'}
    
    <div class="span10 homeDroite">
        
	<div class="search">
	
	{if !isset($hideSearch)}
	<form class="form-inline homeDroiteForm" method="POST" action="{getlink page=$page  action='search'}" >
                
		<input type="text" name="search[mots_cle]" value="{$smarty.request.search['mots_cle']}" placeholder="{$globale_trad->searchCv}" class="span12 homeDroiteSearch"/>
	    
		<div class="row-fluid" id="filter">
		    
		    <div class="span12">
			<a href="{getlink page='cvteque'  action=''}">
			    <img class=" pull-left border-right-1 padding-0" src="web/img/ico/ico_reload.png" alt="reload" title="reload" />
			</a>
			<span class="pull-left label-form">{$globale_trad->label_lieu}</span>
		    
			<div class="border-right-1 pull-left ">
			
			    <input  id="geocomplete" type="text" value="{$smarty.session.search['lo_region_short']}" class=" span12 formModification"/>
			    <input type="hidden" name="search[lo_departement_short]" id="lo_departement_short" value="{$smarty.session.search['lo_departement_short']}">
			    <input type="hidden" name="search[lo_region_short]" id="lo_region_short" value="{$smarty.session.search['lo_region_short']}">
			    <input type="hidden" name="search[lo_pays_short]" id="lo_pays_short" value="{$smarty.session.search['lo_pays_short']}">
			  
			    <div class="clearfix"></div>
			</div>
			
			
			<div class="border-right-1 padding-left-10 pull-left ">
			    <div class="span12 formModificationSelectDiv">
				<select class="formModificationSelect" name="search[id_experience]">
				    <option value="">{$globale_trad->label_exp}</option>
				    {foreach  $listExperiences as $experience}
				       <option {if $experience->id_experience==$smarty.session.search.id_experience}selected="selected"{/if} value="{$experience->id_experience}">{$experience->adm_experience}</option>
				    {/foreach}
				 </select>
			    </div>
			    <div class="clearfix"></div>
			</div>

					    
			<div class="border-right-1 padding-left-10  pull-left ">
			    <div class="span12 formModificationSelectDiv">
				<select class="formModificationSelect" name="search[modifie_le]">
				    {foreach  from=$listPublications key=k item=v}
				    <option {if $k==$smarty.session.search.modifie_le}selected="selected"{/if} value="{$k}">{$v}</option>
				    {/foreach}
				</select>
			    </div>
			    <div class="clearfix"></div>
			</div>
		    
			<span class="pull-left label-form">{$globale_trad->label_dispo}</span>
			
			<div class="border-right-1 pull-left ">
			    <div  data-date-format="{$datepicker.dateFormat}" data-date="{$smarty.request.search['mi_debut']}" class=" input-append date">
				<input type="text" value="{if $smarty.request.search['disponibilite']}{$smarty.session.search['disponibilite']}{else}{$globale_trad->label_du}{/if}" size="16" name="search[disponibilite]" class=" span12">
				<span class="add-on"><i class="icon-calendar"></i></span>
			    </div>
			</div>
			
			<input class=" pull-left last border-right-1 padding-0" type="image" src="web/img/ico/ico_search_form.png" alt="reload" title="reload" />
		    
		    </div>
		
		</div>
		
            </form>
	{/if}
	
    </div>
	
    {*Si la requete getLitsMissions() renvoi des missions*}
    {if isset($count)}
	<form method="post">

	    <h1 class="result">
		{$globale_trad->searchResult} <b>{$count} {$globale_trad->profils}</b>
	    </h1>
	    {$messageCallBack}
	    <table width="100%" cellpadding="5" class="mission">
		<thead class="MissionListHeader">
		    <tr>

			    
			    <td class="span2">{$globale_trad->identite}</td>
	    
			    <td align="center" class="">{$globale_trad->presentation}</td>
			    
			    <td align="center" class=" ">{$globale_trad->label_mob}</td>
    
			    <td align="center" class=" last">{$globale_trad->label_comp}</td>
			    
		    </tr>
		</thead>
	    
		<tbody>
		    {foreach $listCv as $profil}       
			{include file='web/tpl/client/template/profil.tpl'}
		    {/foreach}
		</tbody>
		
		<tfoot>
		    
		    <tr class="MissionsListPagination pagination">
		
			<td colspan="4" class="last" >
				<ul>
				{if $nbrPages > 1 && $pageCourante != 1} <li><a href="{getlink page=$page  action='search' params="&paginate="|cat:($pageCourante-1)}" class="blue"><i class="icon-chevron-left"></i> {$globale_trad->precedant}</a></li> {/if}
				
				{section name=for start=1 loop=$nbrPages+1 step=1}
				    {if $smarty.section.for.index==$pageCourante}
					<li><span>{$smarty.section.for.index}</span></li>
				    {else}
					<li><a href="{getlink page=$page  action='search' params="&paginate="|cat:$smarty.section.for.index}">{$smarty.section.for.index}</a></li>
				    {/if}
				{/section}
				
				{if $nbrPages >1 && $pageCourante != $nbrPages} <li><a href="{getlink page=$page  action='search' params="&paginate="|cat:($pageCourante+1)}" class="blue">{$globale_trad->suivant} <i class="icon-chevron-right"></i></a></li> {/if}
				</ul>
			</td>

		    </tr>
		    
		</tfoot>
	    
	    </table>
	    
	</form>

    {else}            
	    <div class="bgWhite resultSearchNotFound">{$globale_trad->no_profil_match} <b>{$post["mots_cle"]}</b></div>  
    {/if}
	
</div>
    
<div class="clearfix"></div>
