<div class="container row-fluid corps">
    {include file='web/tpl/client/template/menu.tpl'}

    <div class="span10 homeDroite">        
        
	    <form method="post" class="monCompteDroite margin-top-20">
	
		<h1 class="margin-bottom-20 ">
		   {$trad->clientPublierMissionTitre}
		</h1>
	    
	       {if isset($error['mi_titre'])}
	       <div class="alert alert-error span12">
		   {*On envoi nos variable de retour error ou success callBack*}
		   <p id="anachor-mi_titre">{$error['mi_titre']}</p>		
	       </div>
	       {/if}
	    
		<div class="span2">
		    <label class="" for="mi_titre"><h4>{$globale_trad->label_titre_projet}<em class="red">*</em></h4></label>
		</div>
		
		<div class="span10">
		    <input type="text" name="mi_titre" id="mi_titre" class="span12 formModification" value="{if isset($post['mi_titre'])}{$post['mi_titre']}{/if}" />
		</div>
		
		<div class="clearfix"></div>
    
    
		<div class="span2">
		    <label class="" for="cl_entreprise"><h4>{$globale_trad->label_mob}</h4></label>
		</div>
		
		<div class="span10">
		    <input type="text" class="span6 formModification" id="geocomplete" value="{$post['lo_quartier']} {$post['lo_ville']} {$post['lo_region']} {$post['lo_pays']}">
		    <input type="hidden" name="lo_quartier" id="lo_quartier" value="{$post['lo_quartier']}">
		    <input type="hidden" name="lo_ville" id="lo_ville" value="{$post['lo_ville']}">
		    <input type="hidden" name="lo_departement" id="lo_departement" value="{$post['lo_departement']}">
		    <input type="hidden" name="lo_departement_short" id="lo_departement_short" value="{$post['lo_departement_short']}">
		    <input type="hidden" name="lo_region" id="lo_region" value="{$post['lo_region']}">
		    <input type="hidden" name="lo_region_short" id="lo_region_short" value="{$post['lo_region_short']}">
		    <input type="hidden" name="lo_pays" id="lo_pays" value="{$post['lo_pays']}">
		    <input type="hidden" name="lo_pays_short" id="lo_pays_short" value="{$post['lo_pays_short']}">
		</div>
		
		<div class="clearfix"></div>

		{if isset($error['mi_description'])}
		<div class="alert alert-error span12">
		    {*On envoi nos variable de retour error ou success callBack*}
		    <p  id="anachor-mi_description">{$error['mi_description']}	</p>		
		</div>
		{/if}
			     
		<div class="span2">
		    <h4>{$globale_trad->label_desc}<em class="red">*</em></h4>
		</div>
		
		<div class="margin-bottom-10 span10">
		    <textarea name="mi_description" id="redactor_content" name="content">{if isset($post['mi_description'])}{$post['mi_description']}{/if}</textarea>
		</div>
    
		<div class="clearfix"></div>
		
		
		<div class="span2">
		    <h4>{$globale_trad->label_poste}</h4>
		</div>
		
		<div class="span10">
		    <div class="span6">
			
			<input class="autocompletion limit entryNew formModification span12" type="text" id="poste" placeholder="{$trad->clientAjouterProfil}"/>
			<input type="hidden" name="id_poste" id="id_poste" value="{foreach $getListPostes as $poste},{$poste->id_poste}{/foreach}">
		    </div>
		    <div id="result_poste" class="result span6">
			{foreach $getListPostes as $poste}
			    <div class="listeCompetence ligneEntry">
				<a href="rmv_id_complement={$poste->id_poste}" class="removeItem ico35"></a>{$poste->adm_poste}
			    </div>
			{/foreach}
		    </div>
		</div>
		
		<div class="clearfix"></div>

		<div class="span2">
		    <h4>{$globale_trad->label_secteur_activite}</h4>
		</div>
		   
		<div class="span10">		
		    <div class="span6">
			<input class="autocompletion limit formModification span12" type="text" name="" id="categorie" placeholder="{$trad->clientAjouterSecteur}"/>
			<input type="hidden" name="id_categorie" id="id_categorie" value="{if $post['id_categorie']}{$post['id_categorie']}{/if}">
		    </div>
			
		    <div id="result_categorie" class="result span6">
			{if $post['id_categorie']}
			    {foreach $getListCategories as $categorie}
			    <div class="listeCompetence ligneEntry">
				<a href="rmv_id_complement={$categorie->id_categorie}" class="removeItem ico35"></a>{$categorie->adm_categorie}
			    </div>
			    {/foreach}
			{/if}
		    </div>
		</div>
		
		<div class="clearfix"></div>	
		
		{if isset($error['id_experience'])}
		<div class="alert alert-error span12">
		    {*On envoi nos variable de retour error ou success callBack*}
		    <p id="anchor-id_experience">{$error['id_experience']}	</p>		
		</div>
		{/if}
		
		<div class="span2">
		    <h4>{$globale_trad->label_exp}</h4>
		</div>
		   
		<div class="span10">
		   
		    <div class="span6 margin-bottom-10">
			<div class="span12 formModificationSelectDiv ">
				<select class="formModificationSelect" name="id_experience">
				    {foreach $getListExperiences as $experience}
				      <option {if $experience->id_experience==$post['id_experience']} selected="selected"{/if} value="{$experience->id_experience}">{$experience->adm_experience}</option>
				    {/foreach}
			    </select>
			</div>
		    </div>
		
		</div>

		<div class="clearfix"></div>
		
		{if isset($error['mi_tarif']) || isset($error['id_monnaie']) || isset($error['id_contrat'])}
		<div class="alert alert-error span12">
		    {*On envoi nos variable de retour error ou success callBack*}
		    <p id="anchor-mi_tarif">{$error['mi_tarif']}</p>
		    <p id="anchor-id_monnaie">{$error['id_monnaie']}</p>	
		    <p id="anchor-id_contrat">{$error['id_contrat']}</p>
		</div>
		{/if}
		
		<div class="span2">
		    <h4>{$globale_trad->label_tarif}<em class="red">*</em></h4>
		</div>
		
		<div class="span10">
			
			<div class="span6">
			
			    <div class="span6 padding-right-10">
			    
				<div class="span4">
				    <input type="text" name="mi_tarif" id="mi_tarif" class="formModification span12" value="{if isset($post['mi_tarif'])}{$post['mi_tarif']}{/if}" />
				</div>
				
				<div class="span8">
				    <div class="span12 formModificationSelectDiv">
					<select class="formModificationSelect" name="id_monnaie">
					    {foreach $getListMonnaies as $monnaie}
						<option {if $monnaie->id_monnaie==$post['id_monnaie']} selected="selected"{/if} value="{$monnaie->id_monnaie}">{$monnaie->adm_monnaie}</option>
					    {/foreach}
					</select>
				    </div>
				</div>
				
			    </div>
			    
			    <div class="span6 pull-right">
				<div class="span12 formModificationSelectDiv">
				    <select class="formModificationSelect" name="id_contrat">
					{foreach $getListContrats as $contrat}
					    <option {if $contrat->id_contrat==$post['id_contrat']} selected="selected"{/if} value="{$contrat->id_contrat}">{$contrat->adm_contrat}</option>
					{/foreach}
				    </select>
				</div>
			    </div>
			
			</div>
			
			<div class="clearfix"></div>

		</div>
	    
		<div class="clearfix"></div>
		
		{if isset($error['mi_debut']) || isset($error['mi_int_fin'])}
		<div class="alert alert-error span12">
		    {*On envoi nos variable de retour error ou success callBack*}
		    <p id="anchor-mi_debut">{$error['mi_debut']}	</p>
		   <p id="anchor-mi_int_fin">{$error['mi_int_fin']}</p>	
		</div>
		{/if}
		
		<div class="span2">
		    <h4>{$globale_trad->label_debut_projet}</h4>
		</div>
		
		<div class="span10">
		    
		    <div class="span6">
			
			<div class="span6 padding-right-10">
			    <div data-date-format="{$datepicker.dateFormat}" data-date="{$smarty.now|date_format:$datepicker.dateDate}" id="" class=" input-append date span12">
				<input type="text" readonly="" value="{if isset($post['mi_debut'])}{$post['mi_debut']}{else}{$smarty.now|date_format:$datepicker.dateDate}{/if}" size="16" name="mi_debut" class="span12">
				<span class="add-on"><i class="icon-calendar"></i></span>
			    </div>
			</div>
     
			<div class="span6 pull-right">
			    
			    <div class="span4">
				<input name="mi_int_fin" id="mi_int_fin" class="formModification span12" type="text" value="{if isset($post['mi_int_fin'])}{$post['mi_int_fin']}{/if}" />
			    </div>
			    
			    <div class="span8 pull-right">
				<div class="span12 formModificationSelectDiv">
				    <select class="formModificationSelect" name="mi_string_fin">
					{foreach from=$getListFinString key=k item=v}
					    <option {if $post['mi_string_fin']==$k} selected="selected"{/if} value="{$k}">{$v}</option>
					{/foreach}
				    </select>
				</div>
			    </div>
			</div>
		    
		    </div>	
		    <div class="clearfix"></div>
		    
		</div>
		
		
		
		<div class="clearfix"></div>
		
		{if isset($error['mi_date_reponse'])}
		<div class="alert alert-error span12">
		    {*On envoi nos variable de retour error ou success callBack*}
		    {$error['mi_date_reponse']}			
		</div>
		{/if}
			
		<div class="span2">
		    <h4>{$globale_trad->limite_reponse}</h4>
		</div>		
		
		<div class="span10">
		    <div class="span6">
			<div class="span6 padding-right-10">
			    <div data-date-format="{$datepicker.dateFormat}" data-date="{$smarty.now|date_format:$datepicker.dateDate}" id="" class=" input-append date span12">
				<input type="text" readonly="" value="{if isset($post['mi_date_reponse'])}{$post['mi_date_reponse']}{else}{$smarty.now|date_format:$datepicker.dateDate}{/if}" size="16" name="mi_date_reponse" class=" span12">
				<span class="add-on"><i class="icon-calendar"></i></span>
			    </div>
			</div>
		    </div>
		</div>
		
		<div class="clearfix"></div>
		
		<div class="span2">
		    <h4>{$globale_trad->label_comp}</h4>
		</div>
		
		<div class="span10">    
		    <div class="span6">
			<div class="span6 padding-right-10">
			    <div class="span12 input-append ">
				<input class="margin-0 autocompletion entryNew span12  ui-autocomplete-input" type="text" name="" id="competence" placeholder="{$trad->clientAjouterCompetence}"/>
				<input type="hidden" name="id_competence" id="id_competence" value="{foreach $getListCompetences as $competence},{$competence->id_competence}{/foreach}">
				<a id="competenceButton" class="manual-insert btn btn-third" >
				    <span class=" icon-plus icon-white margin-0"></span>
				</a>
			    </div>
			</div>
		    </div>
	
		</div>
		
		<div class="clearfix"></div>
		
		<div id="result_competence" class="result offset2 result_competence">
			{foreach $getListCompetences as $competence}
			  <div class="listeCompetence ligneEntry">
				<a href="rmv_id_complement={$competence->id_competence}" class="removeItem ico35"></a>{$competence->adm_competence}
			  </div>
			{/foreach}
		  </div>		
		
		
		<div class="span2">
		    <h4>{$globale_trad->label_niveau_etude}</h4>
		</div>
		
		<div class="span10">
		    <div class="span6">
			<div class="span6 padding-right-10">
			    <div class="span12 input-append ">
				    <input class="margin-0 autocompletion entryNew span12  ui-autocomplete-input" type="text" id="etude" placeholder="{$trad->clientAjouterEtude}"/>
				    <input type="hidden" name="id_etude" id="id_etude" value="{foreach $getListEtudes as $etude},{$etude->id_etude}{/foreach}">
				    <a id="etudeButton" class="manual-insert btn btn-third" >
					    <span class=" icon-plus icon-white margin-0"></span>
				    </a>
			    </div> 
			    
			</div>
		    </div>
		</div>
		
		<div class="clearfix"></div>
		
		<div id="result_etude" class="result offset2 result_etude">
			{foreach $getListEtudes as $etude}
			  <div class="listeCompetence ligneEntry">
				<a href="rmv_id_complement={$etude->id_etude}" class="removeItem ico35"></a>{$etude->adm_etude}
			  </div>
			{/foreach}
		 </div>
		
		<div class="span2">
		    <h4>{$globale_trad->label_langue}</h4>
		</div>
		
		<div class="span10">    
		    <div class="span6">
			<div class="span6 padding-right-10">
			    <div class="span12 input-append">
				    <input class="margin-0 autocompletion entryNew span12  ui-autocomplete-input" type="text" id="langue" placeholder="{$trad->clientAjouterLangue}"/>
				    <input type="hidden" name="id_langue" id="id_langue" value="{foreach $getListLangues as $langue},{$langue->id_langue}{/foreach}">
				    <a id="langueButton" class="manual-insert btn btn-third" >
					<span class=" icon-plus icon-white margin-0"></span>
				    </a>
			    </div>
			</div>
			
		    </div>
			
		    <div class="clearfix"></div>
		    
		</div>
		
		<div class="clearfix"></div>
		
		
		<div id="result_langue" class="result offset2 result_langue">
			{foreach $getListLangues as $langue}
			  <div class="listeCompetence ligneEntry">
				<a href="rmv_id_complement={$langue->id_langue}" class="removeItem ico35"></a>{$langue->adm_langue}
			  </div>
			{/foreach}
		</div>
		
		<div class="span2"></div>
		
		<div class="span7">
		    
		       {$globale_trad->label_accept_mail}
		      <input type="radio" name="mi_reponse_mail" checked="checked" value="1"> {$globale_trad->label_oui}
		      <input type="radio" name="mi_reponse_mail" value="0"> {$globale_trad->label_non}	                       
		    
		</div>
		
		<div class="span3 padding-left-10">
		    {*no error*}
		</div>
		
		 <div class="clearfix"></div>
		 
		       <button class="btn btn-primary  pull-right" type="submit"  value="addMission" name="action">
			<span class="icon-ok icon-white"></span>
                        {$globale_trad->bt_enregistrer}
			</button>
		 <div class="clearfix"></div>

		
	  </form>
  </div>
<div class="clearfix"></div>