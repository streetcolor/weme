<div class="container-fluid row-fluid corps">
    {include file='web/tpl/client/template/menu.tpl'}

    <div class="span10 row-fluid homeDroite">
    
	<div class="monCompteDroite  margin-top-20">
		
		
	    {if $result==true}
		{$globale_trad->notifMessageEnvoye}
		  <p>
		    {$globale_trad->profils} 
		    <br>
		    {foreach $listProfils as $profil}
		    
			{$profil->getNom()}  {$profil->getPrenom()}
			<input type="hidden" name="id_prestataire_profil[]" value="{$profil->getId_profil()}">
			<br>
		    {/foreach}
		</p>
		
		<p>
		<br>
		{$post['mail_titre']}
		<br>
		{$post['mail_message']}
		</p>
		
	    {elseif isset($id_mission)}
	    

		<form method="post" action="{getlink page='message'  action='sendFavoris' params="id_mission="|cat:$id_mission crypt="true"}" >
		    
		    <h1>{$globale_trad->select_send_msg}</h1>
	   
		    <p>
			{$trad->clientMessageIntro}
		    </p>
		    
		    <h2 class="margin-top-20">{$globale_trad->profils}</h2>
	    
		    <p>
			
			{foreach $listProfils as $profil}
			
			    {$profil->getNom()}  {$profil->getPrenom()}
			    <input type="hidden" name="id_prestataire_profil[]" value="{$profil->getId_profil()}">
			    <br>
			{/foreach}
		    </p>
		    
		    
		    <div class="clearfix"></div>
		    <h2 class="margin-top-20">{$trad->clientMessageRedigerLabel}</h2>
    
		    
		    {if isset($error['id_identifiant']) ||  isset($error['mail_author']) ||  isset($error['usr_email']) || isset($error['mail_titre']) || isset($error['mess_titre'])}
		    <div class="span12 ">
			<div class="alert alert-error span12">
			    {*On envoi nos variable de retour error ou success callBack*}
			    <p>{$error['id_identifiant']}</p>
			    <p>{$error['mail_author']}</p>
			    <p>{$error['usr_email']}</p>
			    <p>{$error['mess_titre']}{$error['mail_titre']}</p>
			</div>	
		    </div>
		    <div class="clearfix"></div>
		    {/if}
		    
		    <div class="span6">
			<div class="span12">{$globale_trad->label_objet}</div>
			<input type="text" name="mail_titre"  class="formModification span12" value="{if $post['mail_titre'] || $post['mess_titre']}{$post['mail_titre']}{$post['mess_titre']}{/if}" />
			<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
    
    
		    {if isset($error['mail_message']) ||  isset($error['mess_message'])}
		    <div class="span12 ">
			<div class="alert alert-error span12">
			    {*On envoi nos variable de retour error ou success callBack*}
			    <p>{$error['mail_message']}</p>
			    <p>{$error['mess_message']}</p>
			</div>	
		    </div>
		    <div class="clearfix"></div>
		    {/if}
		   
		   <div class="span12">
			<div class="span12">{$trad->clientMessageMessageLabel}</div>
			<div class="clearfix"></div>
			<textarea class="wysywyg" name="mail_message">
			    {if $post['mail_message'] || $post['mess_message']}
				{$post['mail_message']}{$post['mess_message']}
			    {else}
				{if $missions['count']}
				    {$trad->clientMessageMessageIntro} 
				    <br/>
				    {foreach $missions['missions'] as $mission}
					{$mission->getTitre()}
					<br/>
					{$mission->getDescription()}
					<hr/>
				    {/foreach}
				{else}
				    {$trad->clientMessageMessageNonDispo} 
				    <br/>
				{/if}
			    {/if}
			</textarea>
		    </div>
		       
		   <div class="clearfix"></div>
		

		    <button class="btn btn-primary  margin-top-20 pull-right" type="submit">
			<span class="icon-pencil icon-white"></span>
			{$globale_trad->bt_envoyer}
		    </button>
		    
		    <div class="clearfix"></div>
		    
		</form>
		
	    {else}
	    
	    <h1>{$globale_trad->select_send_msg}</h1>
	   
	    <p>
		{$trad->clientMessageIntro}
	    </p>
	    
	    <form method="post" action="{getlink page='message'  action='send' crypt="true"}" >
	    
		<h2 class="margin-top-20">{$globale_trad->profils}</h2>
	    
		<p>
		    
		    {foreach $listProfils as $profil}
		    
			{$profil->getNom()}  {$profil->getPrenom()}
			<input type="hidden" name="id_prestataire_profil[]" value="{$profil->getId_profil()}">
			<br>
		    {/foreach}
		</p>
		
		
		{if $countMessages>0}
		<h2 class="margin-top-20">{$trad->clientMessageChoosePrefefini}</h2>
		    
		<div class="span6">
			<div class="span12 formModificationSelectDiv">
				<select class="formModificationSelect" name="id_client_message">
				{foreach $listMessages as $message}     
				    <option value=" {$message['id_client_message']}">{$message['mess_titre']}</option>
				{/foreach}
			    </select>
			</div>
		</div>
		    
	
		<input type="hidden" name="crypt" value="{$id_prestataire_profil}">
		<button class="margin-left-20 btn btn-primary  pull-left" type="submit" value="flag" name="chooseMessage">
		    <span class="icon-refresh icon-white"></span>
		    {$trad->clientMessageChoosePrefefiniBt}
		</button>
		{/if}
		<div class="clearfix"></div>
		<h2 class="margin-top-20">{$trad->clientMessageRedigerLabel}</h2>

		
		{if isset($error['id_identifiant']) ||  isset($error['mail_author']) ||  isset($error['usr_email']) || isset($error['mail_titre']) || isset($error['mess_titre'])}
		<div class="span12 ">
		    <div class="alert alert-error span12">
			{*On envoi nos variable de retour error ou success callBack*}
			<p>{$error['id_identifiant']}</p>
			<p>{$error['mail_author']}</p>
			<p>{$error['usr_email']}</p>
			<p>{$error['mess_titre']}{$error['mail_titre']}</p>
		    </div>	
		</div>
		<div class="clearfix"></div>
		{/if}
		
		<div class="span6">
		    <div class="span12">{$globale_trad->label_objet}</div>
		    <input type="text" name="mail_titre"  class="formModification span12" value="{if $post['mail_titre'] || $post['mess_titre']}{$post['mail_titre']}{$post['mess_titre']}{/if}" />
		    <div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>


		{if isset($error['mail_message']) ||  isset($error['mess_message'])}
		<div class="span12 ">
		    <div class="alert alert-error span12">
			{*On envoi nos variable de retour error ou success callBack*}
			<p>{$error['mail_message']}</p>
			<p>{$error['mess_message']}</p>
		    </div>	
		</div>
		<div class="clearfix"></div>
		{/if}
		
		<div class="span12">
		    <div class="span12">{$trad->clientMessageMessageLabel}</div>
		    <div class="clearfix"></div>
		    <textarea class="wysywyg" name="mail_message">
			{if $post['mail_message'] || $post['mess_message']}
			    {$post['mail_message']}{$post['mess_message']}
			{else}
			    {$trad->clientMessageMessageIntro}  
			<br/>
			{/if}
		    </textarea>
		</div>
		
		<div class="clearfix"></div>
		

		<button class="btn btn-primary  margin-top-20 pull-right" type="submit">
		    <span class="icon-pencil icon-white"></span>
		    {$globale_trad->bt_envoyer}
		</button>
		
		<div class="clearfix"></div>
		
	    </form>
		
	    
	    {/if}
	</div>
	
    </div>
    
<div class="clearfix"></div>
