<div class="container-fluid row-fluid corps">
    {include file='web/tpl/client/template/menu.tpl'}
    
<div class="span10 row-fluid homeDroite">
   
    <div class="monCompteDroite  margin-top-20">
	    
	    <h1>{$trad->titreService}</h1>
	    
	    <div>
		
		{$trad->descriptionCorps}
		
	    </div>
	    
	    <p>
		
		
	    <a class="blue" href="{getLink page="new_mission" }">
                {$trad->linkNewProjet}
            </a>
            &nbsp;
             <a class="blue" href="{getLink page="missions" }">
               {$trad->linkAllProjets}
            </a>
            &nbsp;
            <a class="blue" href="{getLink page="contact" }">
                 {$trad->linkContact}
            </a>
		
	    </p>
    </div>
</div>

<div class="clearfix"></div>
