<div class="container row-fluid corps">
    {include file='web/tpl/client/template/menu.tpl'}

    <div class="span10 homeDroite">        
         
	<form method="post" class="monCompteDroite margin-top-20">
	    
	    <h1 class=" margin-bottom-20">
	       {$trad->clientEditMissionsTitre}
	    </h1>
	    
	    {if isset($error['mi_titre'])}
	    <div class="alert alert-error span12">
		{*On envoi nos variable de retour error ou success callBack*}
		{$error['mi_titre']}			
	    </div>
	    {/if}
	    
	    <div class="span2">
		<label class="" for="mi_titre"><h4>{$globale_trad->label_titre_projet}t</h4></label>
	    </div>
	    
	    <div class="span10">
		<input type="text" name="mi_titre" id="mi_titre" class="span12 formModification" value="{if $post['mi_titre']}{$post['mi_titre']}{else}{$editMission->getTitre()}{/if}" />
	    </div>
	    
	    <div class="clearfix"></div>

	    <div class="span2">
		<label class="" for="cl_entreprise"><h4>{$globale_trad->label_mob}</h4></label>
	    </div>
	    
	    <div class="span10">
		<input type="text" class="span6 formModification" id="geocomplete" value="{if $post['lo_pays']}{$post['lo_quartier']} {$post['lo_ville']} {$post['lo_region']} {$post['lo_pays']}{else}{$localiteMission}{/if}">
		<input type="hidden" name="lo_quartier" id="lo_quartier" value="{if $post['lo_quartier']}{$post['lo_quartier']}{else}{$localiteMission->getLocalite()->lo_quartier}{/if}">
		<input type="hidden" name="lo_ville" id="lo_ville" value="{if $post['lo_ville']}{$post['lo_ville']}{else}{$localiteMission->getLocalite()->lo_ville}{/if}">
		<input type="hidden" name="lo_departement" id="lo_departement" value="{if $post['lo_departement']}{$post['lo_departement']}{else}{$localiteMission->getLocalite()->lo_departement}{/if}">
		<input type="hidden" name="lo_departement_short" id="lo_departement_short" value="{if $post['lo_departement_short']}{$post['lo_departement_short']}{else}{$localiteMission->getLocalite()->lo_departement_short}{/if}">
		<input type="hidden" name="lo_region" id="lo_region" value="{if $post['lo_region']}{$post['lo_region']}{else}{$localiteMission->getLocalite()->lo_region}{/if}">
		<input type="hidden" name="lo_region_short" id="lo_region_short" value="{if $post['lo_region_short']}{$post['lo_region_short']}{else}{$localiteMission->getLocalite()->lo_region_short}{/if}">
		<input type="hidden" name="lo_pays" id="lo_pays" value="{if $post['lo_pays']}{$post['lo_pays']}{else}{$localiteMission->getLocalite()->lo_pays}{/if}">
		<input type="hidden" name="lo_pays_short" id="lo_pays_short" value="{if $post['lo_pays_short']}{$post['lo_pays_short']}{else}{$localiteMission->getLocalite()->lo_pays_short}{/if}">
	    </div>
	    
	    <div class="clearfix"></div>
    
	    {if isset($error['mi_description'])}
	    <div class="alert alert-error span12">
		{*On envoi nos variable de retour error ou success callBack*}
		{$error['mi_description']}			
	    </div>
	    {/if}
	    
	    <div class="span2">
		<h4>{$globale_trad->label_desc}</h4>
	    </div>
	    
	    <div class="margin-bottom-10 span10">
		<textarea name="mi_description" id="redactor_content" name="content">{if $post['mi_description']}{$post['mi_description']}{else}{$editMission->getDescription()}{/if}</textarea>
	    </div>
	    
	    <div class="clearfix"></div>
	    
	    <div class="span2">
		<h4>{$globale_trad->label_poste}</h4>
	    </div>
	    
	    <div class="span10">
		<div class="span6">
		    <input class="autocompletion limit entryNew formModification span12" type="text" id="poste" placeholder="{$globale_trad->label_add_poste}"/>
		    <input type="hidden" name="id_poste" id="id_poste" value="">

		</div>
		<div id="result_poste" class="result span6">
		    {foreach $editMission->getPostes()->postes as $poste}
			<div class="ligneEntry">
			    {$poste->adm_poste} 
			    <a class="removeItem ico35" href="?page=edit_mission&action=editMission&rmv_id_complement={$poste->id_mission_complement}"></a>
			</div>
		    {/foreach}
		</div>
		
		<div class="clearfix"></div>
		
	    </div>

	    <div class="clearfix"></div>

	    <div class="span2">
		<h4>{$globale_trad->label_secteur_activite}</h4>
	    </div>
	       
	    <div class="span10">		
		
		<div class="span6">
		    <input class="autocompletion limit formModification span12" type="text" name="" id="categorie" placeholder=""/>
		    <input type="hidden" name="id_categorie" id="id_categorie" value="{if $post['id_categorie'] || $post}{foreach $getListCategories as $categorie}{$categorie->id_categorie}{/foreach}{else}{$getCategorie->id_categorie}{/if}">
		</div>
		    
		<div id="result_categorie" class="result span6">
		    {if $post['id_categorie'] || $post}
			{foreach $getListCategories as $categorie}
			<div class=" ligneEntry">
			    <a href="rmv_id_complement={$categorie->id_categorie}" class="removeItem ico35"></a>{$categorie->adm_categorie}
			</div>
			{/foreach}
		    {else}
			<div class="ligneEntry">
			    {if $getCategorie->id_categorie > 0}
				{$getCategorie->adm_categorie}<a class="removeItem ico35" href="?page=edit_mission&action=editMission&id_mission={$editMission->getId_mission()}&rmv_id_categorie={$getCategorie->id_categorie}"></a>
			    {/if}
			</div>
		    {/if}
		</div>
		
		<div class="clearfix"></div>
		
	    </div>
	    
	    <div class="clearfix"></div>
	    
	    {if isset($error['id_experience'])}
	    <div class="alert alert-error span12">
		{*On envoi nos variable de retour error ou success callBack*}
		{$error['id_experience']}			
	    </div>
	    {/if}
	    
	    <div class="span2">
		<h4>{$globale_trad->label_exp}</h4>
	    </div>
	       
	    <div class="span10">
	       
		<div class="span6">
		    <div class="span12 formModificationSelectDiv margin-bottom-10">
			<select class="formModificationSelect " name="id_experience">
				{foreach $getListExperiences as $experience}
				    <option {if $experience->id_experience==$editMission->getExperience()->id_experience} selected="selected"{/if} value="{$experience->id_experience}">{$experience->adm_experience}</option>
				{/foreach}
			</select>
		    </div>
		</div>
	    
	    </div>
    
	    <div class="clearfix"></div>
	    
	    <div class="span2">
		<h4>{$globale_trad->label_tarif}</h4>
	    </div>
	    
	    {if isset($error['mi_tarif']) || isset($error['id_monnaie']) || isset($error['id_contrat'])}
	    <div class="alert alert-error span12">
		{*On envoi nos variable de retour error ou success callBack*}
		{$error['mi_tarif']}	
		{$error['id_monnaie']}	
		{$error['id_contrat']}			
	    </div>
	    {/if}
	    
	    <div class="span10">
		    
		    <div class="span6">
		    
			<div class="span6 padding-right-10">
			
			    <div class="span4">
				<input type="text" name="mi_tarif" id="mi_tarif" class="formModification span12" value="{if $post['mi_tarif']}{$post['mi_tarif']}{else}{$editMission->getTarif()->tarif}{/if}" />
			    </div>
			    
			    <div class="span8">
			    
				<div class="span12 formModificationSelectDiv">
				    <select class="formModificationSelect" name="id_monnaie">
					{foreach $getListMonnaies as $monnaie}
					    <option {if $monnaie->id_monnaie==$editMission->getTarif()->id_monnaie} selected="selected"{/if} value="{$monnaie->id_monnaie}">{$monnaie->adm_monnaie}</option>
					{/foreach}
				    </select>
				</div>
			    </div>
			    
			</div>
			
			<div class="span6 pull-right">
			    <div class="span12 formModificationSelectDiv">
				<select class="formModificationSelect" name="id_contrat">
				    {foreach $getListContrats as $contrat}
					<option {if $contrat->id_contrat==$editMission->getTarif()->id_contrat} selected="selected"{/if} value="{$contrat->id_contrat}">{$contrat->adm_contrat}</option>
				    {/foreach}
				</select>
			    </div>
			</div>
		    </div>
		    
		    <div class="clearfix"></div>

	    </div>
	
	    <div class="clearfix"></div>
	    
	    {if isset($error['mi_debut']) || isset($error['mi_int_fin'])}
	    <div class="alert alert-error span12">
		{*On envoi nos variable de retour error ou success callBack*}
		{$error['mi_debut']}	
		{$error['mi_int_fin']}	
	    </div>
	    {/if}
	    
	    <div class="span2">
		<h4>{$globale_trad->label_debut_projet}</h4>
	    </div>
	    
	    <div class="span10">
		
		<div class="span6">
		    
		    <div class="span6 padding-right-10">
			<div data-date-format="{$datepicker.dateFormat}" data-date="{if $post['mi_debut']}{$post['mi_debut']}{else}{if $editMission->getDebut()}{$editMission->getDebut()}{else}{$smarty.now|date_format:$datepicker.dateDate}{/if}{/if}" id="" class="span12 input-append date ">
			    <input type="text" readonly="" value="{if $post['mi_debut']}{$post['mi_debut']}{else}{$editMission->getDebut()}{/if}" size="16" name="mi_debut" class=" span12">
			    <span class="add-on"><i class="icon-calendar"></i></span>
			</div>
		    </div>
 
		    <div class="span6 pull-right">
			
			<div class="span4">
			    <input name="mi_int_fin" id="mi_int_fin" class="formModification span12" type="text" value="{if $post['mi_int_fin']}{$post['mi_int_fin']}{else}{$editMission->getFin(false)->int_fin}{/if}" />
			</div>
			
			<div class="span8 pull-right">
			    <div class="span12 formModificationSelectDiv">
				<select class="formModificationSelect" name="mi_string_fin">
				{foreach from=$getListFinString key=k item=v}
				    <option {if $editMission->getFin(false)->string_fin==$k} selected="selected"{/if} value="{$k}">{$v}</option>
				{/foreach}
				</select>
			    </div>
			</div>
		    </div>
		</div>
		    
		<div class="clearfix"></div>
		
	    </div>
	    
	    <div class="clearfix"></div>
		    
	    {if isset($error['mi_date_reponse'])}
		<div class="alert alert-error span12">
		    {*On envoi nos variable de retour error ou success callBack*}
		    {$error['mi_date_reponse']}			
		</div>
		{/if}
		    
	    <div class="span2">
		<h4>{$globale_trad->date_reponse}</h4>
	    </div>		
	    
	    <div class="span10">
		<div class="span6">
		    <div class="span6 padding-right-10">
			<div data-date-format="{$datepicker.dateFormat}" data-date="{if $post['mi_date_reponse']}{$post['mi_date_reponse']}{else}{if $editMission->getDateReponse()}{$editMission->getDateReponse()}{else}{$smarty.now|date_format:$datepicker.dateDate}{/if}{/if}"  id="" class="input-append date span12">
			    <input type="text" readonly="" value="{if $post['mi_date_reponse']}{$post['mi_date_reponse']}{else}{$editMission->getDateReponse()}{/if}" size="16" name="mi_date_reponse" class=" span12">
			    <span class="add-on"><i class="icon-calendar"></i></span>
			</div>
		    </div>
		</div>
	    </div>
	    
	    <div class="clearfix"></div>
	    
	    <div class="span2">
		<h4>{$globale_trad->label_comp}</h4>
	    </div>
	    
	    <div class="span10">
		
		<div class="span6">
		    <div class="span6 padding-right-10">
			<div class="span12 input-append">
			    <input class="margin-0 autocompletion entryNew span12  ui-autocomplete-input" type="text" name="" id="competence" placeholder="Ajouter une competence"/>
			    <input type="hidden" id="id_competence" value="{foreach $editMission->getCompetences()->competences as $competence},{$competence->id_mission_complement}{/foreach}">
			    <a id="competenceButton" class="manual-insert btn btn-third">
				<span class=" icon-plus icon-white margin-0"></span>
			    </a>
			</div>
		    </div>
		    <div id="result_competence" class="result result_competence">
			    {foreach $editMission->getCompetences()->competences as $competence}
				<div class="ligneEntry">
				    <a class="removeItem ico35" href="?page=edit_mission&action=editMission&id_mission={$editMission->getId_mission()}&rmv_id_complement={$competence->id_mission_complement}"></a>{$competence->adm_competence}
			       </div>
			    {/foreach}
		    </div>
		</div>
	    </div>    
	    <div class="clearfix"></div>
	    
	    <div class="span2">
		<h4>{$globale_trad->label_formation}</h4>
	    </div>
	    
	    <div class="span10">
		
		<div class="span6">
		    <div class="span6 padding-right-10">
			<div class="span12 input-append">
			    <input class="margin-0 autocompletion entryNew span12  ui-autocomplete-input" type="text" id="etude" placeholder="Ajouter une etude"/>
			    <input type="hidden" id="id_etude" value="">
			    <a id="etudeButton" class="manual-insert btn btn-third">
				<span class=" icon-plus icon-white margin-0"></span>
			    </a>
		    
			</div>
		    </div>
			
		    <div id="result_etude" class="result result_etude">
			    {foreach $editMission->getEtudes()->etudes as $etude}
				<div class="ligneEntry">
				    {$etude->adm_etude}   
				    <a class="removeItem ico35" href="?page=edit_mission&action=editMission&id_mission={$editMission->getId_mission()}&rmv_id_complement={$etude->id_mission_complement}"></a>
				</div>
			    {/foreach}
		     </div>
		</div>
	    </div>    
	    <div class="clearfix"></div>
	    
	    <div class="span2">
		<h4>{$globale_trad->label_langue}</h4>
	    </div>
	    
	    <div class="span10">
		
		<div class="span6">
		    <div class="span6 padding-right-10">
			<div class="span12 input-append">
				<input class="margin-0 autocompletion entryNew span12  ui-autocomplete-input" type="text" id="langue" placeholder="Ajouter une langue"/>
				<input type="hidden"  id="id_langue" value="">
				<a id="langueButton" class="manual-insert btn btn-third" >
				    <span class=" icon-plus icon-white margin-0"></span>
				</a>
			</div>
		    </div>
		    <div id="result_langue" class="result result_langue">
			    {foreach $editMission->getLangues()->langues as $langue}
				<div class="ligneEntry">
				    {$langue->adm_langue}   
				    <a class="removeItem ico35" href="?page=edit_mission&action=editMission&id_mission={$editMission->getId_mission()}&rmv_id_complement={$langue->id_mission_complement}"></a>
				</div>
			    {/foreach}
		    </div>
		    
		</div>
		    
		<div class="clearfix"></div>
		
	    </div>
	    
	    <div class="clearfix"></div>
	    
	    <div class="span2"></div>
	    
	    <div class="span7">
		 
		   {$globale_trad->label_accept_mail} 
		  <input type="radio" name="mi_reponse_mail" checked="checked" value="1"> {$globale_trad->label_oui}
		  <input type="radio" name="mi_reponse_mail" value="0"> {$globale_trad->label_non}	                       
		
	    </div>
	
	    
	     <div class="clearfix"></div>
	    
	    <button class="btn btn-primary  pull-right" type="submit"  value="editMission" name="action">
		    <span class="icon-pencil icon-white"></span>
		    {$globale_trad->bt_modifier}
	    </button>
	    
	    <input type="hidden" name="crypt" value="{$crypt}" id="crypt" autocomplete="off">
	    
	    <div class="clearfix"></div>
    
	</form>
	    
  </div>
<div class="clearfix"></div>