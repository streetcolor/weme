<div class="container row-fluid corps">
    {include file='web/tpl/client/template/menu.tpl'}

    
    <div class="span10 homeDroite margin-top-20">        
        
	<form class="monCompteDroite span4" method="POST"  enctype="multipart/form-data">
            <h1>{$trad->clientNewManagerTitreUn}</h1>
	    
	    <h2 class=" compteTitleModificationh2">{$globale_trad->label_etat_civil}</h4>
            
	    {if isset($error['id_type']) || isset($error['id_client'])}
                <div class="span12">
                <div class="alert alert-error span12 margin-bottom-20">
                   {*On envoi nos variable de retour error ou success callBack*}
                   <p>{$error['id_type']}</p>
                   <p>{$error['id_client']}</p>
           
                </div>
                </div>
            {/if}
	               
	    <div class="span4 padding-right-10">
	       
		<div class="span12">
		    {$globale_trad->label_civilite}
		</div>
		
		<div class="span12 formModificationSelectDiv margin-bottom-10">
		    <select name="id_civilite"  {if isset($id_client_manager)}disabled="disabled"{/if}  class="formModificationSelect span12">
			{foreach $getListCivilites as $civilite}
			    <option {if {$post['id_civilite']}==$civilite->id_civilite} selected="selected"{/if} value="{$civilite->id_civilite}">{$civilite->adm_civilite}</option>
			{/foreach}
		    </select>
		</div>
		
		<div class="clearfix"></div>
		
	    </div>
	    
	    <div class="clearfix"></div>

	    {if isset($error['mg_nom']) || isset($error['mg_prenom'])}
                <div class="span12">
                <div class="alert alert-error span12 margin-bottom-20">
                   {*On envoi nos variable de retour error ou success callBack*}
                   <p>{$error['mg_prenom']}</p>
                   <p>{$error['mg_nom']}</p>
           
                </div>
                </div>
            {/if}
	    
	    <div class="span6 padding-right-10">
		
		<div class="span12">
		    {$globale_trad->label_nom}
		</div>
		
		<div class="span12">
		    <input  class="span12 formModification"  name="mg_nom"  type="text"  {if isset($id_client_manager)}disabled="disabled"{/if}  value="{if $post['mg_nom']}{$post['mg_nom']}{else}{if isset($id_client_manager)}{$manager->getNom()}{/if}{/if}"  placeholder="{$globale_trad->label_nom}">
		</div>
		
		<div class="clearfix"></div>
		
	    </div>
	    
	    <div class="span6">
		
		<div class="span12">
		    {$globale_trad->label_prenom}
		</div>
		
		<div class="span12">
		    <input class="span12 formModification" name="mg_prenom" type="text" {if isset($id_client_manager)}disabled="disabled"{/if} type="text" value="{if $post['mg_prenom']}{$post['mg_prenom']}{else}{if isset($id_client_manager)}{$manager->getPrenom()}{/if}{/if}"  placeholder="{$globale_trad->label_prenom}">
		</div>
		
		<div class="clearfix"></div>
		
	    </div>
	    
	    <div class="clearfix"></div>
	    
	    <h2 class=" compteTitleModificationh2 margin-top-20">{$globale_trad->label_coordonnees}</h4>
	    
	    <div class="span6">
		
		<div class="span12">
		    {$globale_trad->label_fixe}
		</div>
		
		<div class="span12">
		   
		    <div  class="span2 " >
			<input maxlength="6" name="mg_fixe_indicatif" class="span12 formModification"  type="text" value="{if $post['mg_fixe_indicatif']}{$post['mg_fixe_indicatif']}{else}{if isset($id_client_manager)}{$manager->getFixe()->indicatif}{/if}{/if}"/>
		    </div>
		    
		    <div class="span10 padding-right-10" >
			<input maxlength="10" name="mg_fixe" class="span12 formModification"  type="text"  value="{if $post['mg_fixe']}{$post['mg_fixe']}{else}{if isset($id_client_manager)}{$manager->getFixe()->numero}{/if}{/if}"/>
		    </div>
		    
		    <div class="clearfix"></div>
		    
		 </div>   
		
		<div class="clearfix"></div>
		
	    </div>
		    
	    <div class="span6">
		
		<div class="span12">
		    {$globale_trad->label_portable}
		</div>
		
		<div class="span12">
		    <div  class="span2" >
			<input maxlength="6" name="mg_portable_indicatif" class="span12 formModification"  type="text" value="{if $post['mg_portable_indicatif']}{$post['mg_portable_indicatif']}{else}{if isset($id_client_manager)}{$manager->getPortable()->indicatif}{/if}{/if}"/>
		    </div>
		    <div class="span10" >
			<input maxlength="10" name="mg_portable" class="span12 formModification"  type="text"  value="{if $post['mg_portable']}{$post['mg_portable']}{else}{if isset($id_client_manager)}{$manager->getPortable()->numero}{/if}{/if}"/>
		    </div>
		    
		    <div class="clearfix"></div>
		    
		</div>   

		<div class="clearfix"></div>
		
	    </div>
	    
	    <div class="clearfix"></div>
	    
	    {if isset($error['usr_email'])}
                <div class="span12">
                <div class="alert alert-error span12 margin-bottom-20">
                   {*On envoi nos variable de retour error ou success callBack*}
                   <p>{$error['usr_email']}</p>
                </div>
                </div>
            {/if}

	    <div class="span12">
		
		<div class="span12">
		    {$globale_trad->label_adresse_email}
		</div>
	    
		<div  class="span12" >
		    <input name="usr_email" class="span12 formModification" {if isset($id_client_manager)}disabled="disabled"{/if} type="text" value="{if $post['usr_email']}{$post['usr_email']}{else}{if isset($id_client_manager)}{$manager->getEmail()}{/if}{/if}"/>
		</div>

		<div class="clearfix"></div>

	    </div>
	    
	    <div class="clearfix"></div>
	    
	    {if isset($error['usr_password']) || isset($error['mdp_verif'])}
                <div class="span12">
                <div class="alert alert-error span12 margin-bottom-20">
                   {*On envoi nos variable de retour error ou success callBack*}
                   <p>{$error['usr_password']}</p>
		   <p>{$error['mdp_verif']}</p>
                </div>
                </div>
            {/if}
	    
	    <div class="span6 padding-right-10">
		
		<div class="span12 ">
		    {$globale_trad->label_mdp}
		</div>
	    
		<div  class="span12" >
		    <input name="usr_password" class="span12 formModification" type="password"/>
		</div>

		<div class="clearfix"></div>

	    </div>
	    
	    <div class="span6">
		
		<div class="span12">
		    {$globale_trad->label_mdp_conf}
		</div>
	    
		<div  class="span12" >
		    <input name="mdp_verif" class="span12 formModification" type="password"/>
		</div>

		<div class="clearfix"></div>

	    </div>
	    
	    <div class="clearfix"></div>
	    
	    {if isset($id_client_manager)}
		<a href="{getLink page="new_manager"}">
		    <button type="button" class="btn btn-primary margin-top-20 pull-left" >
			<span class=" icon-repeat icon-white"></span>
			Annuler
		    </button>
		</a>
	    {/if}
	    
	    <button value="{if isset($id_client_manager)}editManager{else}addManager{/if}" name="action" class="btn btn-primary margin-top-20 pull-right" type="submit">
		<span class=" icon-pencil icon-white"></span>
		{$globale_trad->bt_enregistrer}
	    </button>
	    
	    <div class="clearfix"></div>

	</form>
	
	<div class="span8  pull-right padding-left-20">
	    
	    {$messageCallBack}
	    
	    <div class="monCompteDroite ">
	    
		<h1>{$trad->clientNewManagerTitreDeux}</h1>
		
		<p>{$trad->clientNewManagerP1}</p>
		
	    </div>
	    
	    <div class="monCompteDroite">
	    <table width="100%" cellpadding="5" class=" mission">
		    <thead class="MissionListHeader">
			<tr>
		     
			    <td class="">{$globale_trad->label_nom}</td>
	    
			    <td align="center" class="">{$globale_trad->label_prenom}</td>
    
			    <td align="center" class="">{$globale_trad->label_telephone}</td>
			    
			    <td class="" align="center" class="">{$globale_trad->label_email}</td>
			    
			    <td align="center" class="last" width="75">{$globale_trad->label_action}</td>
				
			</tr>
		    </thead>
		
		    <tbody>
			{if isset($count)}
			    {foreach $listManagers as $manager}           
				<tr style="border-top:1px solid #D8D8D8">

				    
				    <td class="">{$manager->getNom()}</td>
		    
				    <td align="center" class="">{$manager->getPrenom()}</td>
	    
				    <td align="" class="">
					{$globale_trad->label_fixe_short} {$manager->getFixe(true)}
					<br>
					{$globale_trad->label_portable_short} {$manager->getPortable(true)}
				    </td>
				    
				    <td align="center" class="">{$manager->getEmail()}</td>
				    
				    <td align="center" class="last">
					
					<div class="btn-group group-link ">
					    <a class="btn btn-primary white dropdown-toggle"  data-toggle="dropdown"  href="#"><i class="icon-tasks icon-white"></i>{$globale_trad->label_action}</a>
					    <a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
					    <ul class="dropdown-menu">
						<li><a href="{getLink page="new_manager" action="editManager" params="id_client_manager="|cat:$manager->getId_manager() crypt=true}"><i class="icon-pencil"></i>{$globale_trad->bt_modifier}</a></li>
						<li><a class="delEntry" href="{getLink page="new_manager" action="removeManager"  params="id_client_manager="|cat:$manager->getId_manager() crypt=true}"><i class="icon-trash "></i> {$globale_trad->bt_supprimer}</a></li>
						
					    </ul>
					</div>
					
				    </td>
					
				</tr>
				   
			    {/foreach}
			{else}
			    <tr>
				<td colspan="5" class=" Aucune profil de diponible  "></td>
			    </tr>
				
			{/if}
		    </tbody>
		
		
		</table>
	    
	    </div>
	</div>
    </div>
</div>
<div class="clearfix"></div>