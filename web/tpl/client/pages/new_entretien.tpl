<div class="container row-fluid corps">
    {include file='web/tpl/client/template/menu.tpl'}
    
    <div class="span10 homeDroite">
	<div class="span6 margin-top-20">
	    
	    {if !isset($id_videosession)}
		
		<form method="post" class="monCompteDroite">    
		    <!-- EN
		    MODE CREATION -->
		    <h1 class="margin-bottom-20 ">{$trad->clientEntretienTitre1}</h1>
		    
		    <h4> {$globale_trad->profil} </h4>
		    
		    {if isset($error['id_prestataire_profil'])}
			<div class="span12">
			<div class="alert alert-error span12 margin-bottom-20">
			   {*On envoi nos variable de retour error ou success callBack*}
			   <p>{$error['id_prestataire_profil']}</p>
		   
			</div>
			</div>
		    {/if}
		    
		    {if !isset($id_prestataire)}
			
			
			
			<div class="styled-select span12">
			    <select name="id_prestataire_profil" id="vs_prestataire profil" style="width: 130%">
				<option value="">{$globale_trad->select_profil}</option>
				{foreach $listProfil as $fav}
				      <option value="{$fav->getId_profil()}">{$fav->getPrenom()} {$fav->getNom()} - {$fav->getTitre()}</option>
				{/foreach}
			    </select>
			</div>
			
		    {else}
			<div class="span6 padding-right-10">
		
				<div class="span12">
					{$globale_trad->label_nom}
				</div>
				
				<div class="span12">
					<input  class="span12 formModification"  name="vs_nom"  type="text"  disabled="disabled" value="{$listProfil[0]->getNom()}"  placeholder="{$globale_trad->label_nom}">
				</div>
				
				<div class="clearfix"></div>
			
			</div>
			
			<div class="span6">
		
				<div class="span12">
					{$globale_trad->label_prenom}
				</div>
				
				<div class="span12">
					<input class="span12 formModification" name="vs_prenom" type="text" disabled="disabled" type="text" value="{$listProfil[0]->getPrenom()}"  placeholder="{$globale_trad->label_prenom}">
				</div>
				
				<div class="clearfix"></div>
			
			</div>
			
			<div class="clearfix"></div>
			
			<div class="span12">
		
				<div class="span12">
					{$globale_trad->label_poste}
				</div>
			
				<div  class="span12" >
					<input name="vs_poste" class="span12 formModification" disabled="disabled" type="text" value="{$listProfil[0]->getTitre()}"/>
				</div>
		
				<div class="clearfix"></div>
	
			</div>
		    {/if}
	    
		    <div class="clearfix"></div>
		     <h4> {$globale_trad->label_session}</h4>
		    <div class="">
			    <label class="" for="vs_id_mission">{$globale_trad->mission}</label>
		    </div>
		    
		    {if isset($error['vs_id_mission'])}
			<div class="span12">
			<div class="alert alert-error span12 margin-bottom-20">
			   {*On envoi nos variable de retour error ou success callBack*}
			   <p>{$error['vs_id_mission']}</p>
		   
			</div>
			</div>
		    {/if}
		    
		    
		    <div class="styled-select span12">
			    <select name="vs_id_mission" id="vs_id_mission" style="width: 130%">
			      <option value="">{$globale_trad->select_projet}</option>
			      {foreach $listMissions as $mission}
				    <option value="{$mission->getId_mission()}">{$mission->getTitre()}</option>
			      {/foreach}
			    </select>
		    </div>
		    <div class="clearfix"></div>
		    <div class="margin-top-10 margin-bottom-10">
			    {$globale_trad->label_date_heure} 
		    </div>
		    <div class="clearfix"></div>
		    
		     {if isset($error['vs_horraires'])}
			<div class="span12">
			<div class="alert alert-error span12 margin-bottom-20">
			   {*On envoi nos variable de retour error ou success callBack*}
			   <p>{$error['vs_horraires']}</p>
		   
			</div>
			</div>
		    {/if}
		    
		    <div class="span6 padding-right-10">
			    <div class="span6 padding-right-10">
				    <div data-date-format="{$datepicker.dateFormat}" data-date="{$smarty.now|date_format:$datepicker.dateDate}" id="" class="span12 newMission datepicker input-append date ">
					    <input type="text"  value="{$smarty.now|date_format:$datepicker.dateDate}" size="16" name="vs_date" class="span12">
					    <span class="add-on"><i class="icon-calendar"></i></span>
				    </div>
			    </div>
			    <div class="span6">
				    <div class="input-append bootstrap-timepicker">
				      <input type="text" class="timepicker span12" data-show-meridian="false">
				      <span class="add-on"><i class="icon-time"></i></span>
				    </div>
			    </div>
			    <div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    
		    <button class="btn btn-primary  pull-right margin-top-20" type="submit"  value="addSession" name="action">
			    <span class="icon-ok icon-white"></span>
			    {$globale_trad->bt_enregistrer}
		    </button>
		    <div class="clearfix"></div>
		    <!-- FIN DU MODE CREATION -->
		
		</form>
		
	    {else}
	    
		<div class="monCompteDroite">
		    <!-- EN MODE EDITION  -->
		    <h1 class="margin-bottom-20 pull-left">
			    {$trad->clientEntretienTitre1Bis}
		    </h1>
		    
		    {if $entretien->getSessionStatut()->id_statut == 2}
		
		    <button class="btn btn-primary pull-right launchEntretien" type="button" data-src="{$session_name}">
			    <span class="icon-ok icon-white"></span>
			    {$globale_trad->bt_rejoindre_session} 
		    </button>
		    <div class="clearfix"></div>
		    {/if}
		    
		    <div class="clearfix"></div>
		    <h4>{$globale_trad->label_session}</h4>
		    <div class="span6 padding-right-10">
			<div class="span6 padding-right-10">
				    <div>{$globale_trad->label_date}</div>
				    <div data-date-format="{$datepicker.dateFormat}" data-date="{$smarty.now|date_format:$datepicker.dateDate}" id="" class="span12 newMission datepicker input-append date ">
					    <input type="text" readonly="" disabled="disabled" value="{$entretien->getSessionTime()->date}" size="16" name="vs_date" class="span12">
				    </div>
			    </div>
			    <div class="span6">
				    <div>{$globale_trad->label_heure}</div>
				    <div class="input-append bootstrap-timepicker">
				      <input type="text" disabled="disabled" class="timepicker span12" data-show-meridian="false" value="{$entretien->getSessionTime()->time}">
				    </div>
			    </div>
			    <div class="clearfix"></div>
		    </div>
		     <div class="clearfix"></div>
		    <div class="clear"></div>
		    <div>{$trad->clientEntretienCode}</div>
		    <div  class="span12" >
			    <input  class="span12 formModification" readonly="" type="text" value="{$entretien->getSessionHost()->host_access}"/>
		    </div>
		    <div class="clearfix"></div>
		    <h4>{$globale_trad->profil} </h4>
		    <div class="span6 padding-right-10">
		    
			    <div class="span12">
				    {$globale_trad->label_nom}
			    </div>
			    
			    <div class="span12">
				    <input  class="span12 formModification"  name="vs_nom"  type="text"  disabled="disabled" value="{$entretien->getSessionGuest()->guest_nom}"  placeholder="{$globale_trad->label_nom}">
			    </div>
			    
			    <div class="clearfix"></div>
		    
		    </div>
		    
		    <div class="span6">
	    
			    <div class="span12">
				    {$globale_trad->label_prenom}
			    </div>
			    
			    <div class="span12">
				    <input class="span12 formModification" name="vs_prenom" type="text" disabled="disabled" type="text" value="{$entretien->getSessionGuest()->guest_prenom}"  placeholder="{$globale_trad->label_prenom}">
			    </div>
			    
			    <div class="clearfix"></div>
		    
		    </div>
		    
		    <div class="clearfix"></div>
		    
		    <div class="span12">
	    
			    <div class="span12">
				    {$globale_trad->label_poste}
			    </div>
		    
			    <div  class="span12" >
				    <input name="vs_poste" class="span12 formModification" disabled="disabled" type="text" value="{$entretien->getSessionGuest()->guest_cv_titre}"/>
			    </div>
	    
			    <div class="clearfix"></div>
	    
		    </div>
		    
		    <div class="clearfix"></div>
		</div>
		<form method="post" class="monCompteDroite">    
		    <h4>{$globale_trad->mission}</h4>
		    <div style="color:#FF786A">
			    {$entretien->getSessionMission()->mi_titre}
		    </div>
		    <hr/>
		    <div  class="" >
				    {$entretien->getSessionMission()->mi_description|truncate:300}
		    </div>
		    <h4>{$trad->clientEntretienLabelCommenter}</h4>
		    <textarea  name="vs_host_commentaire" class="wysywyg formModificationTxtArea">{if $entretien->getSessionHostComm()}{$entretien->getSessionHostComm()}{/if}</textarea>
		    <input type="hidden" name="id_videosession" value="{$entretien->getId_videosession()}" />
		    
		    <p class="margin-top-20 pull-right">
		
			<a href="{getLink page=$page}" class="">
				<button class="btn btn-primary" type="button" >
					<span class="icon-ok icon-white"></span>
					{$globale_trad->bt_retour}
				</button>
			</a>
			
			<button class="btn btn-primary " type="submit"  value="editSession" name="action">
				<span class="icon-ok icon-white"></span>
				{$trad->clientEntretienBtCommenter}
			</button>
		    
		    </p>
		    
		    <div class="clearfix"></div>
		    
		  
		    
		    
		    <!-- FIN DU MODE EDITION -->
		</form>
		
	    {/if}
	    
	
	</div>
    
      <div class="span6 padding-left-20 margin-top-20">
          
          {$messageCallBack}
		  <div class="monCompteDroite ">
			<h1>{$trad->clientEntretienTitre2}</h1>
			<p>
			  {$trad->clientEntretienIntro}
			</p>
		  </div>
		  <div class="monCompteDroite ">
          
            <h1>
              {$trad->clientEntretienTitre3}
            </h1>
	
            <table width="100%" cellpadding="5" class="mission margin-bottom-20 margin-top-20">
              <thead class="MissionListHeader">
                  <tr>
      
                      <td align="center" class="">{$globale_trad->label_statut}</td>
					  
                      <td align="center" class="span4">{$trad->clientEntretienLabelInvite}</td>
                      
                      <td align="center" class=" ">{$globale_trad->label_date_heure}</td>
          
                      <td align="center" class=" last">{$globale_trad->label_action}</td>
                      
                  </tr>
              </thead>
			 
              <tbody>
		    
		    {if $listEntretiens.count>0}
		    
			{foreach $listEntretiens.entretiens as $entretien}
			<tr>
				<td align="center">{$entretien->getSessionStatut()->entretien_statut}</td>
				<td align="center">{$entretien->getSessionGuest()->guest_nom} {$entretien->getSessionGuest()->guest_prenom}</td>
				<td align="center">le {$entretien->getSessionTime()->date}</td>
				<td align="center" class="last">
				    <div class="btn-group group-link ">
					<a class="btn btn-primary dropdown-toggle white"  data-toggle="dropdown"  href="#"><i class="icon-tasks icon-white"></i> {$globale_trad->label_action}</a>
					<a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
					<ul class="dropdown-menu">
						{if $entretien->getSessionStatut()->id_statut == 2}
						<li>
							<a class="launchEntretien" data-src="entretien_{$entretien->getId_videosession()}-{$entretien->getSessionTime()->dbdate}" data-IDSession="{$entretien->getId_videosession()}" >
								<span class=" icon-play-circle margin-0"></span> {$globale_trad->bt_rejoindre_session}
							</a>
						</li>
						{/if}
						<li>
							<a href="{getLink page="new_entretien" action="showDetailSession" params="id_videosession="|cat:$entretien->getId_videosession() crypt=true}"><i class="icon-pencil"></i> {$globale_trad->bt_detail}</a>
						</li>
					
					</ul>
				    </div>
				</td>
				
			</tr>
			{/foreach}
		    {else}
		    
			<tr>
				
			    <td colspan="4" align="center"> Vous n'avez actuellement aucun entretien vidéo</td>
			  
			</tr>
		    
		    {/if}
              </tbody>
            </table>
          
          </div>
      </div>
    
    </div>
</div>