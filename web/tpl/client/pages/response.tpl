<div class="container-fluid row-fluid corps">
    
    {include file='web/tpl/client/template/menu.tpl'}
    
    <div class="span10 homeDroite">
        
	<div class="search">
                             
	    <div class="row-fluid" id="filter">
		
		<div class="form-inline homeDroiteForm">
		    
		    <div class="pull-left padding-left-5 margin-top-5 margin-bottom-5 margin-right-15 margin-left-5">
			
			<a class="scroller" href="{getLink page='responses' params="id_mission="|cat:$mission->getId_mission() crypt=true}">
			    <button type="button" class="btn client btn-fourth pull-left padding-left-10 padding-right-15 border-radius-6">
				<span class="ico20 backresponse"></span>
				{$globale_trad->back_reponses}
			    </button>
			</a>
			
		    </div>
		    
		    <div class="pull-left padding-left-5 margin-top-5 margin-bottom-5 margin-right-15 margin-left-5">
			
			<a class="scroller" href="#reponseAnchor">
			    <button type="button" class="btn client btn-fourth pull-left padding-left-10 padding-right-15 border-radius-6">
				<span class="ico20 editC"></span>
				{$globale_trad->select_send_msg}
			    </button>
			</a>
	    
		    </div>
		    
		    <div class="pull-left padding-left-5 margin-top-5 margin-bottom-5 margin-right-15 margin-left-5">
			
			<a href="{getLink page="response" action="sendCopy" params="id_mission_reponse="|cat:$reponse->getId_reponse()|cat:"&id_mission="|cat:$reponse->getId_mission()|cat:"&mode="|cat:$mode crypt="true"}">
			    <button type="button" class="btn client btn-fourth pull-left padding-left-10 padding-right-15 border-radius-6">
				<span class="ico20 mailme"></span>
				{$globale_trad->send_me}
			    </button>
			</a>
			
		    </div>
		    
		    <div class="pull-left padding-left-5 margin-top-5 margin-bottom-5 margin-right-15 margin-left-5">
			
			    {if !$reponse->getArchiver()}
			    <a href="{getLink page={$page} action="archiver" params="id_mission_reponse="|cat:$reponse->getId_reponse()|cat:"&id_mission="|cat:$reponse->getId_mission()|cat:"&mode="|cat:$mode|cat:"&paginate="|cat:$pageCourante crypt="true"}">
				<button type="button" class="btn client btn-fourth pull-left padding-left-10 padding-right-15 border-radius-6">
				    <span class="ico20 archive"></span>
				    {$globale_trad->bt_archiver}
				</button>
			    </a>
			    {else}
			    <button disabled="disbales" class="btn client btn-fourth pull-left padding-left-10 padding-right-15 border-radius-6">
				    <span class="ico20 archive"></span>
				    {$globale_trad->label_archiver} {$reponse->getArchiver()}
			    </button>
			    {/if}
		    
		    </div>
		    
		    <div class="clearfix"></div>
		    
		</div>
		
	    </div>
		
        </div>
	
	<div class="span12 row-fluid">
	    <div class="monCompteDroite">
		
		<h1> {$mission->getTitre()}</h1>
		<div class="span9">
		    <p>{$mission->getDescription()}</p>
		</div>
		
		<div class="span3 padding-left-20">
		    <h2>{$globale_trad->label_mob} :</h2>
		    <p class="span12">
			{$mission->getLocalite()}
		    </p>
		    <h2>{$globale_trad->label_tarif} :</h2>
		    <p class="span12">
			{$mission->getTarif(true)}
		    </p>
		    <h2>{$globale_trad->label_date} :</h2>
		    <p class="span12">
			du {$mission->getDebut()} Au {$mission->getFin()}
		    </p>
		    {if $missionCompetences->count}
		    <h2>{$globale_trad->label_comp} :</h2>
		    <ul class="span12">
		       {foreach $missionCompetences->competences as $competence}
			<li>{$competence->adm_competence}</li>
		       {/foreach}
		    </ul>
		    {/if}
		    
		    {if $missionLangues->count}
		    <h2>{$globale_trad->label_langue} :</h2>
		    <ul class="span12">
		       {foreach $missionLangues->langues as $langue}
			<li>{$langue->adm_langue}</li>
		       {/foreach}
		    </ul>
		    {/if}
		    
		    {if $missionEtudes->count}
		    <h2>{$globale_trad->label_etude} :</h2>
		    <ul class="span12">
		       {foreach $missionEtudes->etudes as $etude}
			<li>{$etude->adm_etude}</li>
		       {/foreach}
		    </ul>
		    {/if}
		</div>
		<div class="clearfix"></div>
		
	    </div>
	  	
	    <div class="span12 row-fluid">
		
		<div class="monCompteDroite">
		    
		    <h1 class="">{$globale_trad->reponsePrestataire}</h1>
		    
		    <div class="span9">
			<div>{$reponse->getReponse()}</div>
			
			
			
		    </div>
		    
		    <div class="span3 padding-left-20">
			
			<h2>{$globale_trad->label_contact_direct} :</h2>
			<p class="span12">
			    <b>{$globale_trad->label_structure} :</b> {$reponse->getStructure()->structure}
			    <br>
			    <b>{$globale_trad->label_contact} : </b>{$reponse->getNom()} {$reponse->getPrenom()}
			    <br>
			    <b>{$globale_trad->label_email} :</b> {$reponse->getEmail()}
			    <br>
			    <b>{$globale_trad->label_telephone} :</b> {$reponse->getStandard(true)}
			    {*Si je ne suis pas une agence notre contact direct est le freelance ou l'intermittent*}
			    {if $reponse->getStructure()->id_structure != 3}
				<br>
				<span class=""><a href="web/scripts/cv.php?page=cv&action=cv&id_prestataire_profil={$reponse->getId_profil()}&crypt={$crypt}">{$globale_trad->bt_dl_pdf}</a></span>
				<a class="showCv" href="{getLink page='cv' action="cv" params="id_prestataire_profil="|cat:$reponse->getId_profil()}&crypt={$crypt}">{$globale_trad->voir_cv}</a>
			    {/if}
			</p>
			
			{if $reponse->getStructure()->id_structure == 3}
			<h2>{$globale_trad->pf} :</h2>
			<ul>
			{foreach $profils as $profil}
			    <li>
				{$profil->getNom()} {$profil->getPrenom()}
				<br>
				<a href="web/scripts/cv.php?page=cv&action=cv&id_prestataire_profil={$profil->getId_profil()}&crypt={$crypt}">Telecharger en PDF</a>
				<a class="showCv" href="{getLink page='cv' action="cv" params="id_prestataire_profil="|cat:$profil->getId_profil()}&crypt={$crypt}">Voir</a>
			    </li>
			   
			{/foreach}
			</ul>
			{/if}
			<h2>{$globale_trad->label_tarif} :</h2>
			<p class="span12">
			    {$reponse->getTarif(true)}
			</p>
			
			{if $reponse->getCommentaire()}
			    <h2>{$globale_trad->label_comm} :</h2>
			    <p>
				{$reponse->getCommentaire()}
			    </p>
			{/if}
			
			{if $pj->getId_file()}
			    <h2>{$globale_trad->label_pj} :</h2>
			    <p>
				<a target="_blank" href="{$pj->getPath()}">
				    <button class="btn btn-primary " type="button">
					<span class="icon-pencil icon-white"></span>
					{$globale_trad->bt_detail}
				    </button>
				</a>
			    </p>
			{/if}
			
			{if $realisations->count>0}
			    <h2>{$globale_trad->label_rea} :</h2>
			    {foreach $realisations->files as $realisation}
				
				<a href="{$realisation->getPath()}" target="_blank" class="{if $realisation->getType()==1}fancy-image{/if}">
				<img src="{$realisation->getThumbnail(50, 50)}" alt="pj" title="pj"></a>
				 
			    {/foreach}
			{/if}
			
			
		    </div>
		    
		    <div class="clearfix"></div>
		    
		      
		</div>
	    
	    </div>
	    
	    <div class="span12 row-fluid">
		

		<form method="post" id="reponseAnchor" class="monCompteDroite" action="#reponseAnchor" >
		
		    <h1 class="">{$globale_trad->label_message_predefini}</h1>
		
		    {foreach $profils as $profil}
			     <input type="hidden" name="id_prestataire_profil[]" value="{$profil->getId_profil()}">
		    
		    {/foreach}
		    
		    {if $countMessages>0}
			
			<h2 class="margin-top-20">{$globale_trad->label_message_predefini}</h2>
			    
			<div class="span6">
				<div class="span12 formModificationSelectDiv">
				    <select class="formModificationSelect" name="id_client_message">
					{foreach $listMessages as $message}     
					    <option value=" {$message['id_client_message']}">{$message['mess_titre']}</option>
					{/foreach}
				    </select>
				</div>
			</div>
			    
			<button class="margin-left-20 btn btn-primary  pull-left" type="submit" value="flag" name="chooseMessage">
			    <span class="icon-refresh icon-white"></span>
			   {$globale_trad->label_choisir_message}
			</button>
		    
		    {/if}
		    <div class="clearfix"></div>
		    <h2 class="margin-top-20">{$globale_trad->label_rediger_message} </h2>
    
		    
		    {if isset($error['id_identifiant']) ||  isset($error['mail_author']) ||  isset($error['usr_email']) || isset($error['mail_titre']) || isset($error['mess_titre'])}
		    <div class="span12 ">
			<div class="alert alert-error span12">
			    {*On envoi nos variable de retour error ou success callBack*}
			    <p>{$error['id_identifiant']}</p>
			    <p>{$error['mail_author']}</p>
			    <p>{$error['usr_email']}</p>
			    <p>{$error['mess_titre']}{$error['mail_titre']}</p>
			</div>	
		    </div>
		    <div class="clearfix"></div>
		    {/if}
		    
		    <div class="span6">
			<div class="span12">{$globale_trad->label_objet} </div>
			<input type="text" name="mail_titre"  class="formModification span12" value="{if $post['mail_titre'] || $post['mess_titre']}{$post['mail_titre']}{$post['mess_titre']}{/if}" />
			<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
    
    
		    {if isset($error['mail_message']) ||  isset($error['mess_message'])}
		    <div class="span12 ">
			<div class="alert alert-error span12">
			    {*On envoi nos variable de retour error ou success callBack*}
			    <p>{$error['mail_message']}</p>
			    <p>{$error['mess_message']}</p>
			</div>	
		    </div>
		    <div class="clearfix"></div>
		    {/if}
		    
		    <div class="span12">
			<div class="span12">{$globale_trad->label_message}</div>
			<div class="clearfix"></div>
			<textarea class="wysywyg" name="mail_message">
			    {if $post['mail_message'] || $post['mess_message']}
				{$post['mail_message']}{$post['mess_message']}
			    {else}
			
			    {/if}
			</textarea>
		    </div>
		    
		    <div class="clearfix"></div>
		    
    
		    <button class="btn btn-primary  margin-top-20 pull-right" type="submit">
			<span class="icon-pencil icon-white"></span>
			{$globale_trad->bt_envoyer}
		    </button>
		    
		    <div class="clearfix"></div>
		    
		</form>
	    
	    
	    </div>
	    
	    <div class="span12 row-fluid margin-bottom-20">
	    
		    
		    <table width="100%" cellpadding="4" class=" mission">
		 
		    <thead class="MissionListHeader">
			
			<tr>
				
				<td class="first">{$globale_trad->label_reponse} </td>
		
				<td align="center" class="">{$globale_trad->label_tarif} </td>
				
				<td align="center" class="">{$globale_trad->label_date}</td>
				
				<td align="center" class="">{$globale_trad->label_profils}</td>
				
				<td align="center" class="last">{$globale_trad->label_pj}</td>			
			</tr>
			
		    </thead>
		    
		    <tbody>
			{*Si ou moins 2 mission en archives on les liste sinon on met rien en effet il y' au moins une mission mais ce'est que l'on affiche juste au dessus docn pas besoin de la réafficher.*}
			{if $reponsesArchive['count'] >1}
			{foreach $reponsesArchive['reponses'] as $reponse}
			    
	    
			    {include file='web/tpl/client/template/reponse.tpl'}
    
			{/foreach}
			{else}
			    <tr align="center">
				
				<td class="last MissionsListActions" colspan="5">
				    -
				</td>
		    
			    </tr>
			{/if}
		    </tbody>

		</table>
		    
	    
	    </div>
	</div>
    </div>
</div>


