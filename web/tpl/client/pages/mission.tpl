<div class="container-fluid row-fluid corps">
    
    {include file='web/tpl/client/template/menu.tpl'}
    
    <div class="span10 homeDroite">
        
	<div class="search">
            
	    <form class="form-inline homeDroiteForm" method="POST" action="{getlink page=$page  action='search'}" >
                                
		<div class="row-fluid" id="filter">
				
			    <div class="pull-left padding-left-5 margin-top-5 margin-bottom-5 margin-right-15">
				
				<a class="scroller"  href="javascript:history.back()">
				    <button type="button" class="btn client btn-fourth pull-left padding-left-10 padding-right-15 border-radius-6">
					<span class="ico20 backresponse"></span>
					{$globale_trad->bt_retour}
				    </button>
				</a>
		    
			    </div>	
				
			    <div class="pull-left padding-left-5 margin-top-5 margin-bottom-5 margin-right-15 margin-left-5">
				
		
				{if $mission->getStatut()->id_statut != 4 && $mission->getStatut()->id_statut != 1}
				    <a class="pull-left" href="{getLink page='edit_mission' action='editMission' params="id_mission="|cat:$mission->getId_mission() crypt=true}">
					<button type="button" class="btn btn-fourth pull-left padding-left-10 padding-right-15 border-radius-6">
					<span class="ico35 edition pull-left"></span>
					{$globale_trad->projetApercuModifier}
					</button>
				    </a>
				    <a  class="pull-left" href="{getLink page='edit_mission' action="duplicateMission" params="id_mission="|cat:$mission->getId_mission()|cat:"&mode="|cat:$mode crypt=true}">
					<button type="button" class="btn btn-fourth pull-left padding-left-10 padding-right-15 border-radius-6">
					<span class="ico35 duplicate pull-left"></span>
					{$globale_trad->projetApercuDupliquer}
					</button>
				    </a>
				    <a  class="pull-left" href="{getLink page='missions' action="changeStatus" params="id_mission="|cat:$mission->getId_mission()|cat:"&id_mission_statut=4"|cat:"&mode="|cat:$mode  crypt=true}">
					<button type="button" class="btn btn-fourth pull-left padding-left-10 padding-right-15 border-radius-6">
					<span class="ico35 cloturer pull-left"></span>
					{$globale_trad->projetApercuCloturer}
					</button>
				    </a>
				    {if  $mission->getStatut()->id_statut == 2}
				    <a  class="pull-left" href="{getLink page='my_profils' action="proposeMission" params="id_mission="|cat:$mission->getId_mission() crypt=true}">
					<button type="button" class="btn btn-fourth pull-left padding-left-10 padding-right-15 border-radius-6">
					<span class="ico35 return pull-left"></span>
					{$globale_trad->bt_proposer}
					</button>
					
				    </a>
				    {/if}
				    {if $mission->getNotificationEmail()>0}
					<a  class="pull-left" href="{getLink page='mission' action="stopNotifications" params="id_mission="|cat:$mission->getId_mission()|cat:"&mi_reponse_mail=0"|cat:"&mode="|cat:$mode  crypt=true}">
					    <button type="button" class="btn btn-fourth pull-left padding-left-10 padding-right-15 border-radius-6">
					    <span class="ico35 startMail pull-left"></span>
					    {$globale_trad->bt_desactiverMail}
					    </button>
					</a>
				    {else}
					<a  class="pull-left" href="{getLink page='mission' action="stopNotifications" params="id_mission="|cat:$mission->getId_mission()|cat:"&mi_reponse_mail=1"|cat:"&mode="|cat:$mode  crypt=true}"">
					    <button type="button" class="btn btn-fourth pull-left padding-left-10 padding-right-15 border-radius-6">
					    <span class="ico35 stopMail  pull-left"></span>
					    {$globale_trad->bt_activerMail}
					    </button>
					</a>
				    {/if}
				{/if}
				{if ($mission->getRepublication()<=2)  && ($mission->getStatut()->id_statut != 1)  && ($mission->getStatut()->id_statut != 4)}
				<a  class="pull-left" href="{getLink page='edit_mission' action="republishMission" params="id_mission="|cat:$mission->getId_mission()|cat:"&mode="|cat:$mode  crypt=true}">
				    <button type="button" class="btn btn-fourth pull-left padding-left-10 padding-right-15 border-radius-6">
				    <span class="ico35 republish pull-left"></span>
				    {$globale_trad->projetApercuRepublier}
				    </button>
				</a>
				{/if}
				{if $mission->getStatut()->id_statut == 3}
				    <a  class="pull-left" href="{getLink page='mission' action="changeStatus" params="id_mission="|cat:$mission->getId_mission()|cat:"&id_mission_statut=2"|cat:"&mode="|cat:$mode crypt=true}"">
					<button type="button" class="btn btn-fourth pull-left padding-left-10 padding-right-15 border-radius-6">
					<span class="ico35 return pull-left"></span>
					{$globale_trad->projetApercuReprendre}
					</button>
				    </a>
				{else if $mission->getStatut()->id_statut == 2}
				    <a  class="pull-left" href="{getLink page='mission' action="changeStatus" params="id_mission="|cat:$mission->getId_mission()|cat:"&id_mission_statut=3"|cat:"&mode="|cat:$mode  crypt=true}"">
					<button type="button" class="btn btn-fourth pull-left padding-left-10 padding-right-15 border-radius-6">
					<span class="ico35 pause pull-left"></span>
					{$globale_trad->projetApercuSuspendre}
					</button>
				    </a>
				{/if}
				
				{if $mission->getReponses(true)>0}
				    <a  class="pull-left" href="{getLink page='responses' params="id_mission="|cat:$mission->getId_mission() crypt=true}">
					<button type="button" class="btn btn-fourth pull-left padding-left-10 padding-right-15 border-radius-6">
					<span class="ico35 detail pull-left"></span>
					{$globale_trad->projetApercuVoirReponses}
					</button>
				    </a>
				{/if}
				
			    </div>
			    
		
			
		
		</div>
		
            </form>
        </div>
	
	<div class="span12 row-fluid">
	    <div class="monCompteDroite">
		<h1>{$mission->getTitre()}</h1>
		<div class="span9">
		    <p>{$mission->getDescription()}</p>
		</div>
		
		<div class="span3 padding-left-20">
		    <h2>{$globale_trad->label_mob} :</h2>
		    <p class="span12">
			{$mission->getLocalite()}
		    </p>
		    <h2>{$globale_trad->label_tarif} :</h2>
		    <p class="span12">
			{$mission->getTarif(true)}
		    </p>
		    <h2>{$globale_trad->label_date} :</h2>
		    <p class="span12">
			du {$mission->getDebut()} Au {$mission->getFin()}
		    </p>
		   
		    {if $missionPostes->count}
		    <h2>{$globale_trad->label_poste} :</h2>
		    <ul class="span12">
		       {foreach $missionPostes->postes as $poste}
			<li>{$poste->adm_poste}</li>
		       {/foreach}
		    </ul>
		    {/if}
		    
		    {if  $missionCategorie->id_categorie}
		    <h2>{$globale_trad->label_secteur_activite} :</h2>
		    <ul class="span12">
			<li>{$missionCategorie->adm_categorie}</li>
		    </ul>
		    {/if}
		   
		    {if $missionCompetences->count}
		    <h2>{$globale_trad->label_comp} :</h2>
		    <ul class="span12">
		       {foreach $missionCompetences->competences as $competence}
			<li>{$competence->adm_competence}</li>
		       {/foreach}
		    </ul>
		    {/if}
		    
		    {if $missionLangues->count}
		    <h2>{$globale_trad->label_langue} :</h2>
		    <ul class="span12">
		       {foreach $missionLangues->langues as $langue}
			<li>{$langue->adm_langue}</li>
		       {/foreach}
		    </ul>
		    {/if}
		    
		    {if $missionEtudes->count}
		    <h2>{$globale_trad->label_formation} :</h2>
		    <ul class="span12">
		       {foreach $missionEtudes->etudes as $etude}
			<li>{$etude->adm_etude}</li>
		       {/foreach}
		    </ul>
		    {/if}
		</div>
		<div class="clearfix"></div>
		
	    </div>
	    
	    
	    
	</div>
    </div>
</div>


