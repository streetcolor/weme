<div class="container">
     <div class="row-fluid inscription">
	  
	  <div class="span6 inscriptionLeft">
	       <h1 class="blue">{$trad->inscriptionTitre1}</h1>
	       <form method="post" class="">
		    
		    {if isset($error['cl_entreprise'])}
			 <div class="span12">
			      <div class="alert alert-error">
				 {$error['cl_entreprise']}
			      </div>
			      <div class="clearfix"></div>
			 </div>
			 <div class="clearfix"></div>
		    {/if}
		     
		    <div>
		    
			 <label class="span5 for="cl_entreprise">{$globale_trad->label_societe} <em class="smallText red ">*</em></label>
			 <input  name="cl_entreprise" id="cl_entreprise" class="formModification span7" value="{if $post['cl_entreprise']}{$post['cl_entreprise']}{/if}" type="text"/>
			 <div class="clearfix"></div>
		    </div>
		    
		  
		    <div>
			 <label class="span5"  for="">{$globale_trad->label_adresse}</label>
			 <input  name="cl_add_fac_rue" id="cl_add_fac_rue" class="formModification span7" value="{if $post['cl_add_fac_rue']}{$post['cl_add_fac_rue']}{/if}" type="text"/>
			 <div class="clearfix"></div>
		    </div>
	   
		    <div>	
			 <label class="span5"   for="cl_codepostal">{$globale_trad->label_cp}</label>
			 <input maxlength="5"  name="cl_add_fac_codepostal" id="cl_codepostal" type="text"  class="formModification span7" value="{if $post['cl_codepostal']}{$post['cl_codepostal']}{/if}"/>
			 <div class="clearfix"></div>
		    </div>
		   
		    <div>	
			 <label class="span5 "   for="cl_ville"> {$globale_trad->label_ville}</label>
			 <input name="cl_add_fac_ville" id="cl_add_fac_ville" type="text"  class="formModification span7"  value="{if $post['cl_add_fac_ville']}{$post['cl_add_fac_ville']}{/if}"/>
			 <div class="clearfix"></div>
		    </div>
	   
		    <div>	
			 <label class="span5"   for="cl_pays">{$globale_trad->label_pays}</label>
			 <input name="cl_add_fac_pays" id="cl_add_fac_pays" type="text"  class="formModification span7"  value="{if $post['cl_add_fac_pays']}{$post['cl_add_fac_pays']}{/if}"/>
			 <div class="clearfix"></div>
		    </div>
		    
		    <div class="span12">
		
			 <label class="span5"   for="cl_telephone">{$globale_trad->label_telephone}</label>
			 
			 <div class="span7">
			    
			      <div  class="span2 " >
				  <input placeholder="+" maxlength="6" name="cl_standard_indicatif" class="span12 formModification"  type="text" value="{if $post['cl_standard_indicatif']}{$post['cl_standard_indicatif']}{else}+{/if}"/>
			      </div>
			      
			      <div class="span7 padding-right-10" >
				  <input maxlength="10" name="cl_standard" class="span12 formModification"  type="text"  value="{if $post['cl_standard']}{$post['cl_standard']}{/if}"/>
			      </div>
			     
			     <div class="clearfix"></div>
			     
			  </div>   
			 
			 <div class="clearfix"></div>
			 
		     </div>
		    
		    <div class="margin-bototm-10">			   
			<label class="span5" for="mg_prenom">{$globale_trad->label_civilite}</label>
			<div class="formModificationSelectDiv span5 margin-bottom-10">
			      <select name="id_civilite"  class="formModificationSelect span12">
			      {foreach $getListCivilites as $civilite}
				  <option {if {$post['id_civilite']}==$civilite->id_civilite} selected="selected"{/if} value="{$civilite->id_civilite}">{$civilite->adm_civilite}</option>
			      {/foreach}
			      </select>
			 </div>
			<div class="clearfix"></div>
		    </div>
		    
		    {if isset($error['mg_nom'])}
			 <div class="span12">
			      <div class="alert alert-error">
				 {$error['mg_nom']}
			      </div>
			      <div class="clearfix"></div>
			 </div>
			 <div class="clearfix"></div>
		    {/if}
		    <div>
			<label class="span5"  for="mg_nom">{$globale_trad->label_nom} <em class="smallText red ">*</em></label>
			<input name="mg_nom"  id="mg_nom" class="formModification span7" type="text"  value="{if $post['mg_nom']}{$post['mg_nom']}{/if}"  />
			<div class="clearfix"></div>
		    </div>
		    
		    {if isset($error['mg_prenom'])}
			 <div class="span12">
			      <div class="alert alert-error">
				 {$error['mg_prenom']}
			      </div>
			      <div class="clearfix"></div>
			 </div>
			 <div class="clearfix"></div>
		    {/if}
		    <div>			   
			 <label class="span5" for="mg_prenom">{$globale_trad->label_prenom} <em class="smallText red ">*</em></label>
			 <input name="mg_prenom" id="mg_prenom" class="span7 formModification" type="text" value="{if $post['mg_prenom']}{$post['mg_prenom']}{/if}"/>
			 <div class="clearfix"></div>
		    </div>
		    
		    {if isset($error['usr_email'])}
			 <div class="span12">
			      <div class="alert alert-error">
				 {$error['usr_email']}
			      </div>
			      <div class="clearfix"></div>
			 </div>
			 <div class="clearfix"></div>
		    {/if}
		    <div>	
			<label class="span5"  for="usr_mail">{$globale_trad->label_email}  <em class="smallText red ">*</em></label>
			<input id="usr_email" class="formModification span7" name="usr_email" type="text" value="{if $post['usr_email']}{$post['usr_email']}{/if}"/>
			<div class="clearfix"></div>
		    </div>
		    
		    {if isset($error['usr_password'])}
			 <div class="span12">
			      <div class="alert alert-error">
				 {$error['usr_password']}
			      </div>
			      <div class="clearfix"></div>
			 </div>
			 <div class="clearfix"></div>
		    {/if}
		    <div>
			 <label class="span5"  for="usr_password">{$globale_trad->label_mdp}  <em class="smallText red ">*</em></label>
			 <input name="usr_password" id="usr_password" type="password" class="span7 password formModification" value=""/>
			 <div class="clearfix"></div>
		    </div>
		    
		    {if isset($error['mdp_verif'])}
			 <div class="span12">
			      <div class="alert alert-error span12 margin-bottom-20">
				 {$error['mdp_verif']}
			      </div>
			      <div class="clearfix"></div>
			 </div>
			 <div class="clearfix"></div>
		    {/if}
		    <div>
			<label class="span5"  for="mdp_verif">{$globale_trad->label_mdp_conf}  <em class="smallText red ">*</em></label>
			<input name="mdp_verif" id="mdp_verif" type="password" class="span7 password formModification" value=""/>
			<div class="clearfix"></div>
		    </div>
		   
		    {if isset($error['ct_captcha'])}
			 <div class="span12">
			      <div class="alert alert-error">
				 {$error['ct_captcha']}
			      </div>
			      <div class="clearfix"></div>
			 </div>
			 <div class="clearfix"></div>
		    {/if}
		    <div>
			 <div class="span5">
			      <img id="siimage" class="clear" src="web/scripts/captcha.php?sid={$captcha}" alt="CAPTCHA Image" />
			      <a  href="#" title="Refresh Image" class="" onclick="document.getElementById('siimage').src = 'web/scripts/captcha.php?sid=' + Math.random(); this.blur(); return false">
				   <img src="web/img/btn/btn_actualiser.gif" alt="Reload Image" height="16" width="16" onclick="this.blur()" align="bottom" border="0" />
			      </a>
			 </div>
			 <div class="span7">
			      <label class="" >{$trad->copy_code}  <em class="smallText red ">*</em></label>
			      <input type="text" name="ct_captcha" size="12" maxlength="16" class="formModification span5" />
			      
			      <div class="clearfix"></div>
			 </div>
			 <div class="clearfix"></div>
		    </div>
		    
		    {if isset($error['valid_cgu'])}
			 <div class="span12">
			      <div class="alert alert-error">
				 {$error['valid_cgu']}
			      </div>
			      <div class="clearfix"></div>
			 </div>
			 <div class="clearfix"></div>
		    {/if}
		    <div class="padding-top-10">
			 <input id="valid_cgu" type="checkbox" name="valid_cgu" class="pull-left margin-right-10"/>
			 <label class="" for="valid_cgu">{$globale_trad->label_cgu_confirm} <span class="blue hand">{$globale_trad->label_cgu} </span>  <em class="smallText red ">*</em></label>
			 <div class="clearfix"></div>
		    </div>
		    <p class="smallText red alignLeft  padding-top-10">
		    <em class="smallText red ">*</em><i>&nbsp; {$globale_trad->label_obligatoire} </i>
		    </p>
		    <p class="pull-right">
			<input type="hidden" value="inscription" name="action">    
			<button class="btn modalButton" type="submit">{$globale_trad->bt_envoyer}</button>
		    </p>   
	       </form>
	  </div>
	  <div class="span6 inscriptionRight">
	       <h1 class="red">{$trad->inscriptionTitre2}</h1>
	       <p>
		    {$trad->inscriptionClientP1}
	       </p>
		   
		   <h2>{$trad->inscriptionClientInfoTitre2}</h2>
	       <p>
		    {$trad->inscriptionClientInfoP2}
			<ul>
			   <li>{$trad->inscriptionClientInfoLi1}</li>
			   <li>{$trad->inscriptionClientInfoLi2}</li>
			   <li>{$trad->inscriptionClientInfoLi3}</li>
			   <li>{$trad->inscriptionClientInfoLi4}</li>
			</ul>
	       </p>
	       <p>
		    {$trad->inscriptionClientInfoP2End}
	       </p>
		   
		   <h2>{$trad->inscriptionClientInfoTitre3}</h2>
		   <p>{$trad->inscriptionClientInfoIntro3}</p>
		   <p>{$trad->inscriptionClientInfoP3}</p>
		   
		  <table class="tarifs border-radius-5 w100prc importantText">
			   <tbody> 
					<tr>
						<!--
						TABLEAU A R�AFFICHER LORSQU'ON AURA LE BUSINESS PLAN
						<th class="w20prc upper padding-bottom-10">{$trad->inscriptionClientOption}</th>
						<th class="w20prc upper padding-bottom-10">{$trad->inscriptionClientPrice}</th>
						-->
					</tr>
			   </tbody>
		  </table>
	  </div>
     </div>
