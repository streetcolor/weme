<div class="container">

    <div class="row-fluid homeDroiteForm" id="filter">
	
	<div class="border-right-1  pull-left margin-left-5 ">

	    <a href="{getLink page="inscription"}">
		<button type="button" class="btn btn-secondary pull-left margin-right-5 ">
		    <span class="icon-star icon-white "></span>
		    {$trad->missionSaveProjet}
		</button>
		<div class="clearfix"></div>
	    </a>
		
	    <div class="clearfix"></div>
	    
	</div>
	
	<div class="border-right-1 padding-left-5  pull-left ">
	    
	    <a href="{getLink page="inscription"}">
		<button type="button" class="btn btn-secondary pull-left margin-right-5 ">
		    <span class="icon-pencil icon-white "></span>
		    {$trad->missionRepondre}
		</button>
	    </a>

	</div>
	
	<div class="clearfix"></div>
	
    </div>
	    
    <div class="row-fluid bgWhite">
	
	<div class=" padding-10 ">
	    
	    <h1>{$globale_trad->label_titre_projet} {$mission->getTitre()}</h1>
	    
	    <div class="span9">
		<p>{$mission->getDescription()}</p>
	    </div>
	    
	    <div class="span3 padding-left-20">
		<h2>{$globale_trad->label_mob} :</h2>
		<p class="span12">
		    {$mission->getLocalite()}
		</p>
		<h2>{$globale_trad->label_tarif} :</h2>
		<p class="span12">
		    {$mission->getTarif(true)}
		</p>
		<h2>{$globale_trad->label_date} :</h2>
		<p class="span12">
		    {$globale_trad->label_du} {$mission->getDebut()} {$globale_trad->label_au} {$mission->getFin()}
		</p>
		{if $missionCompetences->count}
		<h2>{$globale_trad->label_comp} :</h2>
		<ul class="span12">
		   {foreach $missionCompetences->competences as $competence}
		    <li>{$competence->adm_competence}</li>
		   {/foreach}
		</ul>
		{/if}
		
		{if $missionLangues->count}
		<h2>{$globale_trad->label_langue} :</h2>
		<ul class="span12">
		   {foreach $missionLangues->langues as $langue}
		    <li>{$langue->adm_langue}</li>
		   {/foreach}
		</ul>
		{/if}
		
		{if $missionEtudes->count}
		<h2>{$globale_trad->label_formation} :</h2>
		<ul class="span12">
		   {foreach $missionEtudes->etudes as $etude}
		    <li>{$etude->adm_etude}</li>
		   {/foreach}
		</ul>
		{/if}
	    </div>
	    <div class="clearfix"></div>
	    
	</div>
	
    </div>
