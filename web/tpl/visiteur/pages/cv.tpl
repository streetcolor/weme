<div class="span12">
	 <table id="pageContent" width="100% "cellpadding="0" cellspacing=0>
	    <tr bgcolor="#333333">
		<td id="pageContent">
		    <img src="web/img/logo.png" alt="logo weme" class="pull-left margin-left-10 margin-top-10"/>
		    <p class="pull-right text-right margin-right-10 margin-top-10 white">
		      {$cv->getPrenom()}
		      <br />
		      {$cv->getNom()}<br/>
		      {$cv->getFixe()} / {$cv->getPortable()}<br/>
		      {$cv->getNaissance()} ans
		    </p>
		    <div class="clearfix"></div>
		 
		   <h2 class="blue text-center">{$cv->getTitre()}</h2>
		</td>
	    </tr>
	    <tr>
		<td colspan="2" height="10"></td>
	    </tr>
	    <!-- descritpion -->
	    {if $cv->getDescription() != ""}
		
		<tr bgcolor="#333333">
		   <td colspan="2" class="padding-left-5mm ">
		       <h2 class="padding-10 red">{$globale_trad->label_desc}</h2>
		   </td>
	       </tr>
		<tr>
		    <td colspan="2" height="10"></td>
		</tr>
		<tr>
		    <td colspan="2">
			<p class="description">{$cv->getDescription()}</p>
		    </td>
		</tr>
		<tr>
		    <td colspan="2" height="10"></td>
		</tr>
	    {/if}
	    <!-- compétences -->
	    {if $cvComp->count>0}
		<tr bgcolor="#333333">
		    <td colspan="2" class="padding-left-5mm ">
			 <h2 class="padding-10 red">{$globale_trad->label_comp}</h2>
		    </td>
		</tr>
		<tr>
		    <td colspan="2" height="10"></td>
		</tr>
		<tr>
		    <td colspan="2">
		       {foreach $cvComp->competences as $competence}
			   {$competence->adm_competence}
			   {if $competence@iteration < $cvComp->count}
			   •
			   {/if}
		       {/foreach}
		   </td>
	       </tr>
		<tr>
		    <td colspan="2" height="10"></td>
		</tr>
	    {/if}
	    <!-- exp pro -->
	    {if $cvExp->count>0}
		<tr bgcolor="#333333">
		    <td colspan="2" class="padding-left-5mm ">
			 <h2 class="padding-10 red">{$globale_trad->label_exp}</h2>
		    </td>
		</tr>
		<tr>
		    <td colspan="2" height="10"></td>
		</tr>
		{foreach $cvExp->experiences as $exp}
		<tr>
		   <td colspan="2" valign="top">
			
			<table  width="100% "cellpadding=0 cellspacing=0>
			   
			    <tr>
				<td valign="top" class="span4">{$exp->exp_debut} - {$exp->exp_fin}</td>
				<td valign="top"  class="span8">
				    <p>{$exp->exp_entreprise}</p>
				    {if isset($exp->exp_intitule)}
				    <h4 class="blue">{$exp->exp_intitule}</h4>
				    {/if}
				    <br/>
				    {$exp->exp_description}
				</td>
			    </tr>
			    
			</table>
			
		    </td>
		</tr>
		<tr>
			<td colspan="2">
			    <hr>
			</td>
		</tr>
		{/foreach}
		<tr>
		    <td colspan="2" height="10"></td>
		</tr>
	    {/if}
	    
	    <!-- formations -->
	    {if $cvForm->count>0}
		<tr bgcolor="#333333">
		    <td colspan="2" class="padding-left-5mm ">
			 <h2 class="padding-10 red">{$globale_trad->label_formation}</h2>
		    </td>
		</tr>
		<tr>
		    <td colspan="2" height="10"></td>
		</tr>
		{foreach $cvForm->formations as $form}
		<tr>
		   <td colspan="2" valign="top">
			
			<table  width="100% "cellpadding=0 cellspacing=0>
			   
			    <tr>
				<td valign="top" class="span4">{$form->form_debut} - {$form->form_fin}</td>
				<td valign="top"  class="span8">
				    <h4 class="blue">{$form->form_diplome} <b>{$form->form_ecole}</b></h4>
				    <b>{$exp->exp_entreprise}<br/></b>
				    <br/>
				     {$form->form_description}
				</td>
			    </tr>
			    
			</table>
			
		    </td>
		</tr>
		<tr>
			<td colspan="2">
			    <hr>
			</td>
		</tr>
		{/foreach}
		<tr>
		    <td colspan="2" height="10"></td>
		</tr>
	    {/if}
	    
	    <!-- Langues -->
	    {if $cvLang->count>0}
		<tr bgcolor="#333333">
		    <td colspan="2" class="padding-left-5mm ">
			 <h2 class="padding-10 red">{$globale_trad->label_langue}</h2>
		    </td>
		</tr>
		<tr>
		    <td colspan="2" height="10"></td>
		</tr>
		<tr>
		    <td colspan="2">
	    
			{foreach $cvLang->langues as $langue}
			    {$langue->adm_langue}
			    {if $langue@iteration < $cvLang->count}
			    •
			    {/if}
			{/foreach}
		    </td>
		</tr>
	    {/if}
	    
	    
	
	</table>
</div>