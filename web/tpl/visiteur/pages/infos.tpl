<div class="container">

    <div class="row-fluid bgWhite">
	
	<div class=" padding-10 ">
	    
	    <h1>{$trad->titreInfos}</h1>
	    
	    <div>
		
		{$trad->descriptionCorps}
		
	    </div>
	    
	    <p>
		
		
	     <a class="blue" href="{getLink page="cl_inscription" }">
                {$trad->linkNewProjet}
            </a>
            &nbsp;
             <a class="blue" href="{getLink page="missions" }">
               {$trad->linkAllProjets}
            </a>
            &nbsp;
            <a class="blue" href="{getLink page="contact" }">
                 {$trad->linkContact}
            </a>
		
	    </p>
	    
	</div>
	
    </div>
