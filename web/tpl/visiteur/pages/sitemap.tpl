
<div class="container">

    <div class="row-fluid bgWhite">
	
	<div class=" padding-10 ">
	    
	    <h1 class=" margin-bottom-10">{$footer_trad->footerSitemap}</h1>
       
	 <ul>
	    <li> <a href="{getLink page="home"}" class="blue">Weme</a>
	    <ul class="padding-10">
		    <li><a href="{getLink page="cvteque"}" class="blue">{$header_trad->headerTrPresta}</a></li>
		   
		    <li><a href="{getLink page="mission"}" class="blue">{$header_trad->headerTrProjet}</a></li>
		    
		    <li>
			<a href="{getLink page="inscription"}" class="blue">{$header_trad->headerInscription}</a>
			<ul class="padding-10">
			    <li><a class="blue" href="{getLink page="cl_inscription"}">{$footer_trad->footerClientCvtheque}</a></li>
			    <li><a class=" blue" href="{getLink page="pr_inscription"}">{$footer_trad->footerPrestaProjet}</a></li>
			</ul>
		    </li>
		    
		    
		    <li><a class="blue" href="{getLink page="services"}">{$footer_trad->footerClientSourcing}</a></li>
		   
		    <li><a href="{getLink page="about"}" class="blue">{$footer_trad->footerAPropos}</a></li>
		    
		    <li><a href="{getLink page="mentions"}" class="blue">{$footer_trad->footerMentions}</a></li>
		    
		    <li><a href="{getLink page="contact"}" class="blue">{$footer_trad->footerContact}</a></li>
		  
	    </ul>
	    
	</li></ul>
	    
	</div>
	
    </div>
