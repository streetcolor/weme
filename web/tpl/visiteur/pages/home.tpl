  <!-- debut du corps HP -->
    <!-- encart univers croisés -->
    <div class="container">
      <div class="row-fluid">
	
      {if isset($error['password']) || isset($error['connexion'])}
      <div class="alert alert-error span12">
	  {*On envoi nos variable de retour error ou success callBack*}
	  {$error['password']}
	  {$error['connexion']}
	  
      </div>
      {/if}
      {$messageCallBack}
    
	
	
	  <div class="row-fluid">
	    <div class="span6 zone411 pointer" onclick="javascript:location.href='{getLink page="cvteque"}'">{$trad->hpClientTitre}<p class="span10 margin-top-20">{$trad->hpClientP1}<br/><br/><a href="{getLink page="cvteque"}">{$trad->hpClientP2}</a></p></div>
	    <div class="span6 zone412">
		      <div id="iview">
			<div data-iview:image="web/img/ill/ill_home_1d.jpg">
				<div class="iview-caption caption2" data-x="280" data-y="110" data-transition="wipeLeft">
				   {$trad->slide_1d_top}
				</div>
				<div class="iview-caption caption2" data-x="230" data-y="170" data-transition="wipeRight">
				  {$trad->slide_1d_bottom}
				</div>
			</div>

			<div data-iview:image="web/img/ill/ill_home_2d.jpg">
				<div class="iview-caption caption2" data-x="50" data-y="110" data-transition="wipeLeft">
				  {$trad->slide_2d_top}
				</div>
				<div class="iview-caption caption2" data-x="20" data-y="170" data-transition="wipeRight">
				 {$trad->slide_2d_bottom}
				</div>
			</div>

			<div data-iview:image="web/img/ill/ill_home_3d.jpg">
				<div class="iview-caption caption2" data-x="20" data-y="20" data-transition="wipeLeft">
				  {$trad->slide_3d_top}
				</div>
				<div class="iview-caption caption2" data-x="40" data-y="81" data-transition="wipeRight">
				  {$trad->slide_3d_bottom}
				</div>
			</div>

		
		      </div>
	    </div>
	  </div>
	  <div class="row-fluid zone42">
	    <div class="span6 zone421">
	      
		<div id="iview2">
			  <div data-iview:image="web/img/ill/ill_home_1g.jpg">
				<div class="iview-caption caption2" data-x="80" data-y="110" data-transition="wipeLeft">
				 {$trad->slide_1g_top}
				</div>
				<div class="iview-caption caption2" data-x="20" data-y="170" data-transition="wipeRight">
				 {$trad->slide_1g_bottom}
				</div>
			</div>
  
			  <div data-iview:image="web/img/ill/ill_home_2g.jpg">
				<div class="iview-caption caption2" data-x="250" data-y="110" data-transition="wipeLeft">
				   {$trad->slide_2g_top}
				</div>
				<div class="iview-caption caption2" data-y="170"  data-x="20" data-transition="wipeRight">
				  {$trad->slide_2g_bottom}
				</div>
			</div>
  
			  <div data-iview:image="web/img/ill/ill_home_3g.jpg">
				<div class="iview-caption caption2" data-x="300" data-y="20" data-transition="wipeLeft">
				   {$trad->slide_3g_top}
				</div>
				<div class="iview-caption caption2" data-x="100" data-y="80" data-transition="wipeRight">
				{$trad->slide_3g_bottom}
				</div>
			</div>
  
			
		  </div>
		
	    </div>
	    <div class="span6 zone422 pointer" onclick="javascript:location.href='{getLink page="pr_inscription"}'"><div class="span8">{$trad->hpPrestaTitre}<p class="span10  margin-top-20">{$trad->hpPrestaP1}<br/><br/><a href="{getLink page="pr_inscription"}">{$trad->hpPrestaP2}</a></p></div></div>
	  </div>
      </div>
      <!-- fin encart univers croisés -->
      <!-- encart sliders -->
      <div class="row-fluid sliderplus">
	<div id="accordion" class="span12">
	  <div class="panel">
	    <div id="p1" class="bgpanel0 move"></div>
	    <div class="panelContent item_p1 padding-top-5">
	      <h1 class="blue margin-0 margin-bottom-10">{$trad->hpBienvenueTitre}</h1>
	      <h3>{$trad->hpBienvenueP1}</h3>
		  <p>{$trad->hpBienvenueP2}</p>
	    </div>
	  </div>
	  <div class="panel">
	    <div id="p2" class="bgpanel2 move"></div>
	    <div class="panelContent item_p2 grey">
	      <h3>{$trad->hpSourcingTitre}</h3>
	      <p>{$trad->hpSourcingP1}</p>
		  <p>{$trad->hpSourcingP2}</p>
		  <h4><a href="{getLink page="contact"}"> > {$globale_trad->bt_contact}</a></h4>
	    </div>
	  </div><div class="panel">
	    <div id="p3" class="bgpanel3 move"></div>
	    <div class="panelContent item_p3 grey2">
	      <h3>{$trad->hpIntermittentTitre}</h3>
	      <p>{$trad->hpIntermittentP1}</p><p>{$trad->hpIntermittentP2}</p><h4><a href="{getLink page="services"}"> > {$trad->hpIntermittentP3}</a></h4>
	    </div>
	  </div>
	</div>
	<div class="clearfix"></div>
      </div>