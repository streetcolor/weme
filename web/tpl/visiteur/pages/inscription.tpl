<div class="container">
     <div class="row-fluid inscription">
	  <div class="span6 inscriptionLeft">
             <h1 class="blue">{$trad->inscriptionTitre1}</h1>
            <p>{$trad->inscriptionIntro}
            </p>
            <a href="{getLink page='cl_inscription'}" class="red">{$trad->inscriptionBtDO}</a> / <a href="{getLink page='pr_inscription'}" class="blue">{$trad->inscriptionBtPresta}</a>
	  </div>
	  <div class="span6 inscriptionRight">
	       <h1 class="red">{$trad->inscriptionTitre2}</h1>
	       <ul>
			   <li>
					{$trad->inscriptionInfoLi1}
					<ul>
						 <li>{$trad->inscriptionInfoLi1Li1}</li>
						 <li>{$trad->inscriptionInfoLi1Li2}</li>
					</ul>
			   </li>
			   <li>
					{$trad->inscriptionInfoLi2}
			   </li>
		   </ul>
	  </div>
     </div>
