<div class="container">
     <div class="row-fluid  inscription">
	  
	  <div class="span6 inscriptionLeft">
	       <h1 class="blue">{$trad->inscriptionTitre1}</h1>
	       <form method="post" class="">
	       
	       <div class="margin-bottom-10">{$trad->inscriptionChooseStructure}</div>
	       
	       <div>		    
		    
		    <div class="offset5  span10 text-center margin-bottom-10">			   
			 <div class="formModificationSelectDiv span6 ">
			   <select name="id_structure"  class="span12 formModificationSelect">
				{foreach $getListStrcutures as $structure}
				    <option {if {$post['id_structure']}==$structure->id_structure} selected="selected"{/if} value="{$structure->id_structure}">{$structure->adm_structure}</option>
				{/foreach}
			   </select>  
			 </div>
		     </div>
		    <div class="clearfix"></div>
	       </div>
		
	       
	       {if isset($error['pr_entreprise'])}
		    <div class="span12">
			 <div class="alert alert-error">
			    {$error['pr_entreprise']}
			 </div>
			 <div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
	       {/if}
	       <div>		    
		    <label class="span5"  for="">{$globale_trad->label_societe}</label>
		    <input  name="pr_entreprise" id="pr_entreprise" class="formModification span7" type="text" value="{if $post['pr_entreprise']}{$post['pr_entreprise']}{/if}" />
		    <div class="clearfix"></div>
	       </div>
	       
	       <div>
		    <label class="span5"  for="">{$globale_trad->label_adresse}</label>
		    <input  name="pr_add_fac_rue" id="pr_add_fac_rue" class="formModification span7" value="{if $post['pr_add_fac_rue']}{$post['pr_add_fac_rue']}{/if}" type="text"/>
		    <div class="clearfix"></div>
	       </div>
	       
	       <div>		    
		    <label class="span5"   for="cl_codepostal">{$globale_trad->label_cp}</label>
		    <input maxlength="5" class="formModification span7" name="pr_add_fac_codepostal" id="cl_codepostal" type="text" value="{if $post['pr_add_fac_codepostal']}{$post['pr_add_fac_codepostal']}{/if}"/>
		    <div class="clearfix"></div>
	       </div>
		
	       <div>		    
		    <label class="span5" for="cl_ville"> {$globale_trad->label_ville}</label>
		    <input name="pr_add_fac_ville" id="pr_add_fac_ville" type="text"  class="formModification span7"  value="{if $post['pr_add_fac_ville']}{$post['pr_add_fac_ville']}{/if}"/>
		    <div class="clearfix"></div>
	       </div>
	
	       <div>		    
		    <label class="span5" for="cl_pays">{$globale_trad->label_pays}</label>
		    <input name="pr_add_fac_pays" id="pr_add_fac_pays" type="text"  class="formModification span7"  value="{if $post['pr_add_fac_pays']}{$post['pr_add_fac_pays']}{/if}"/>
		    <br class="clearfix">
		</div>
	      
	       <div class="span12">
		
		    <label class="span5"   for="cl_telephone">{$globale_trad->label_telephone}</label>
		    
		    <div class="span7">
		       
			 <div  class="span2 " >
			     <input maxlength="6" placeholder="+"  name="pr_standard_indicatif" class="span12 formModification"  type="text" value="{if $post['pr_standard_indicatif']}{$post['pr_standard_indicatif']}{else}+{/if}"/>
			 </div>
			 
			 <div class="span7 padding-right-10" >
			     <input maxlength="10" name="pr_standard" class="span12 formModification"  type="text"  value="{if $post['pr_standard']}{$post['pr_standard']}{/if}"/>
			 </div>
			
			<div class="clearfix"></div>
			
		     </div>   
		    
		    <div class="clearfix"></div>
		    
	       </div>
		    
	       <div class="margin-bototm-10">			   
		   <label class="span5" for="mg_prenom">{$globale_trad->label_civilite}</label>
		   <div class="formModificationSelectDiv span5 margin-bottom-10">
			 <select name="id_civilite"  class="formModificationSelect span12">
			 {foreach $getListCivilites as $civilite}
			     <option {if {$post['id_civilite']}==$civilite->id_civilite} selected="selected"{/if} value="{$civilite->id_civilite}">{$civilite->adm_civilite}</option>
			 {/foreach}
			 </select>
		    </div>
		   <div class="clearfix"></div>
	       </div>
	       
	       {if isset($error['pf_nom'])}
		    <div class="span12">
			 <div class="alert alert-error">
			    {$error['pf_nom']}
			 </div>
			 <div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
	       {/if}
	       <div>
		   <label class="span5"  for="pf_nom">{$globale_trad->label_nom} <em class="red">*</em></label>
		   <input name="pf_nom"  id="pf_nom" class="formModification span7" type="text"  value="{if $post['pf_nom']}{$post['pf_nom']}{/if}"  />
		   <div class="clearfix"></div>
	       </div>
	       
	       {if isset($error['pf_prenom'])}
		    <div class="span12">
			 <div class="alert alert-error">
			    {$error['pf_prenom']}
			 </div>
			 <div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
	       {/if}
	       <div>			   
		    <label class="span5" for="pf_prenom">{$globale_trad->label_prenom}<em class="red">*</em></label>
		    <input name="pf_prenom" id="pf_prenom" class="span7 formModification" type="text" value="{if $post['pf_prenom']}{$post['pf_prenom']}{/if}"/>
		    <div class="clearfix"></div>
	       </div>
	       
	       {if isset($error['usr_email'])}
			 <div class="span12">
			      <div class="alert alert-error">
				  {$error['usr_email']}
			      </div>
			      <div class="clearfix"></div>
			 </div>
			 <div class="clearfix"></div>
		    {/if}
		    <div>	
			<label class="span5"  for="usr_mail">{$globale_trad->label_email} <em class="red">*</em></label>
			<input id="usr_email" class="formModification span7" name="usr_email" type="text" value="{if $post['usr_email']}{$post['usr_email']}{/if}"/>
			<div class="clearfix"></div>
		    </div>
		    
		    {if isset($error['usr_password'])}
			 <div class="span12">
			      <div class="alert alert-error">
				 {$error['usr_password']}
			      </div>
			      <div class="clearfix"></div>
			 </div>
			 <div class="clearfix"></div>
		    {/if}
		    <div>
			 <label class="span5"  for="usr_password">{$globale_trad->label_mdp} <em class="red">*</em></label>
			 <input name="usr_password" id="usr_password" type="password" class="span7 password formModification" value=""/>
			 <div class="clearfix"></div>
		    </div>
		    
		    {if isset($error['mdp_verif'])}
			 <div class="span12">
			      <div class="alert alert-error span12 margin-bottom-20">
				 {$error['mdp_verif']}
			      </div>
			      <div class="clearfix"></div>
			 </div>
			 <div class="clearfix"></div>
		    {/if}
		    <div>
			<label class="span5"  for="mdp_verif">{$globale_trad->label_mdp_conf}<em class="red">*</em></label>
			<input name="mdp_verif" id="mdp_verif" type="password" class="span7 password formModification" value=""/>
			<div class="clearfix"></div>
		    </div>
		   
		    {if isset($error['ct_captcha'])}
			 <div class="span12">
			      <div class="alert alert-error">
				 {$error['ct_captcha']}
			      </div>
			      <div class="clearfix"></div>
			 </div>
			 <div class="clearfix"></div>
		    {/if}
		    <div>
			 <div class="span5">
			      <img id="siimage" class="clear" src="web/scripts/captcha.php?sid={$captcha}" alt="CAPTCHA Image" />
			      <a  href="#" title="Refresh Image" class="" onclick="document.getElementById('siimage').src = 'web/scripts/captcha.php?sid=' + Math.random(); this.blur(); return false">
				   <img src="web/img/btn/btn_actualiser.gif" alt="Reload Image" height="16" width="16" onclick="this.blur()" align="bottom" border="0" />
			      </a>
			 </div>
			 <div class="span7">
			      <label class="" >{$trad->copy_code}<em class="red">*</em></label>
			      <input type="text" name="ct_captcha" size="12" maxlength="16" class="formModification span5" />
			      
			      <div class="clearfix"></div>
			 </div>
			 <div class="clearfix"></div>
		    </div>
		    
		    {if isset($error['valid_cgu'])}
			 <div class="span12">
			      <div class="alert alert-error">
				 {$error['valid_cgu']}
			      </div>
			      <div class="clearfix"></div>
			 </div>
			 <div class="clearfix"></div>
		    {/if}
		    <div class="padding-top-10">
			 <input id="valid_cgu" type="checkbox" name="valid_cgu" class="pull-left margin-right-10"/>
			 <label class="" for="valid_cgu">{$globale_trad->label_cgu_confirm} <span class="blue hand">{$globale_trad->label_cgu}<em class="red">*</em></span> </label>
			 <div class="clearfix"></div>
		    </div>
		    <div>
			   <em class="red">*</em>
			   <i class="red smallText">{$globale_trad->label_obligatoire}</i>
			</div>
		    <p class="pull-right">
			<input type="hidden" value="inscription" name="action">    
			<button class="btn modalButton" type="submit">{$globale_trad->bt_envoyer}</button>
		    </p>    
	       </form>
	  </div>
	  <div class="span6 inscriptionRight">
	       <h1 class="red">{$trad->inscriptionTitre2}</h1>
	       <p>
		    {$trad->inscriptionPrestaInfoFreelance}
	       </p>
	  </div>
     </div>
