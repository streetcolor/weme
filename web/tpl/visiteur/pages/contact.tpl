<div class="container">
     <div class="row-fluid bgWhite">
	
   <form class="padding-10" method="post">

		<h1 class="result">{$trad->titreContact}</h1>
		
		<p class="padding-bottom-30">
		   {$trad->descriptionCorps}
		</p>
		<p>
		    <label>{$globale_trad->label_nom} <em class="smallText red ">*</em></label>
		</p>
		{if isset($error['mail_nom'])}
		    <div class="span5 padding-bottom-10">
			<div class="alert alert-error  margin-0">
			    {*On envoi nos variable de retour error ou success callBack*}
			    <p>{$error['mail_nom']}</p>
			</div>
		    </div>
		    <div class="clearfix"></div>
		{/if}
		<p class="span5">
		    <input value="{if $post['mail_nom']}{$post['mail_nom']}{/if}" type ="text" class="span12 formModification" name="mail_nom">
		</p>
		<div class="clearfix"></div>
		
		<p>
		    <label>{$globale_trad->label_email}  <em class="smallText red ">*</em></label>
		</p>
		{if isset($error['mail_author'])}
		    <div class="span5 padding-bottom-10">
			<div class="alert alert-error  margin-0">
			    {*On envoi nos variable de retour error ou success callBack*}
			    <p>{$error['mail_titre']}</p>
			</div>
		    </div>
		    <div class="clearfix"></div>
		{/if}
		<p class="span5">
		    <input value="{if $post['mail_author']}{$post['mail_author']}{/if}" type ="text" class=" span12 formModification" name="mail_author">
		</p>
		<div class="clearfix"></div>
    
		
		<p>
		    <label>{$globale_trad->label_sujet}  <em class="smallText red ">*</em></label>
		</p>
		{if isset($error['mail_titre'])}
		    <div class="span5 padding-bottom-10">
			<div class="alert alert-error  margin-0">
			    {*On envoi nos variable de retour error ou success callBack*}
			    <p>{$error['mail_titre']}</p>
			</div>
		    </div>
		    <div class="clearfix"></div>
		{/if}
		<p class="span5">
		    <input type ="text" value="{if $post['mail_titre']}{$post['mail_titre']}{/if}" class="span12 formModification" name="mail_titre">
		    <br class="clear">
		</p>
		
		<div class="clearfix"></div>
		

		<p>
		    <label>{$globale_trad->label_message}  <em class="smallText red ">*</em></label>
		</p>
		{if isset($error['mail_message'])}
		    <div class=" padding-bottom-10">
			<div class="alert alert-error  margin-0">
			   {*On envoi nos variable de retour error ou success callBack*}
			   <p>{$error['mail_message']}</p>	
			</div>
		    </div>
		{/if}
		<div>
		    <textarea  name="mail_message" class="wysywyg">{if $post['mail_message']}{$post['mail_message']}{/if}</textarea>
		</div>
		<div class="clear"></div>
		 <p class="smallText red alignLeft  padding-top-10">
			<i><em class="smallText red ">*</em>&nbsp; {$globale_trad->label_obligatoire} </i>
		    </p>

		<button class="btn btn-primary margin-top-20 pull-right" type="submit">
		    <span class=" icon-pencil icon-white"></span>
		    {$globale_trad->bt_envoyer} 
		</button>
		
		<div class="clearfix"></div>

	</form>
</div>
<div class="clearfix"></div>
