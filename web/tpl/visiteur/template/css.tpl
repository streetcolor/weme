<link href='http://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,900,800,700,600' rel='stylesheet' type='text/css'>
<link href="web/css/bootstrap.css" rel="stylesheet"/>
<link href="web/css/global.css" rel="stylesheet">
<link href="web/css/weme_visiteur.css" rel="stylesheet">
<link href="web/css/jquery-ui-1.9.0.custom_1.css" rel="stylesheet"/>
<link href="web/css/bootstrap-datepicker.css" rel="stylesheet"/>
<link href="web/css/iview.css"  rel="stylesheet" />
<link href="web/css/jquery.fancybox.css" rel="stylesheet">
<link href="web/css/redactor.css" rel="stylesheet">

{literal}
<!--[if lt IE 9]>
<style media="all" type="text/css">
    .icon-calendar { height: 30px \9; }
    .input-append .manual-insert{ height: 31px \9;line-height: 20px \9;}
    .bootstrap-timepicker .add-on i { height: 28px \9; }
</style>
<![endif]-->
{/literal}