<!-- Headergroup-->
    <div class="navbar navbar-inverse navbar-fixed-top">
      <!-- Header1-->
      <div class="navbar-inner headerbar1">
        <div class="container row-fluid">
          <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <div class="span2 headerbar1 logo"><a class="brand " href="{getLink page="home"}"><img src="web/img/logo/logo_visiteur.png" alt="logo weme"/></a></div>
	  <div class="span4 headerbar1"><p class="navbar-text">{$header_trad->headerBaseline}</p></div>
	  <div class="span2 headerbar1 socialheader"><a href=""><img alt="twitter" src="web/img/pictoTwitter.png"/></a> <a href=""><img alt="facebook" src="web/img/pictoFb.png"/></a> <a href=""><img alt="linkedin" src="web/img/pictoLinkedin.png"/></a> <a href=""><img alt="googleplus" src="web/img/pictoGoogle.png"/></a></div>
          <div class="span4 headerbar1 nav-collapse collapse">
	    <ul class="nav menunav">
	      <div class="span6 headerbar1 dropdownlang"><li class="dropdown padding-top-5">
		<img src="web/img/pictoLangue.png" alt="connection" class="padding-right-15"/>
		<a href="#" class="dropdown-toggle" data-toggle="dropdown">
		  {$globale_trad->hpLangChooser}
		  <b class="caret"></b>
		</a>
		<ul class="dropdown-menu">
		  <li><a href="{getLink page="{$page}" params="lan=fr"}">{$globale_trad->francais}</a></li>
		  <li><a href="{getLink page="{$page}" params="lan=en"}">{$globale_trad->anglais}</a></li>
		  <li><a href="{getLink page="{$page}" params="lan=es"}">{$globale_trad->espagnol}</a></li> 
		</ul>
	      </li></div>
	    <div class="span6 headerbar1 headerconnect">
	      <li>
			<img src="web/img/pictoLoggin.png" alt="connection" class="pull-left"/>
			<a href="#identification" role="button" data-toggle="modal" class="span8">{$header_trad->headerConnexion}</a>
			<div class="clearfix"></div>
	      </li>
	    </div>
	  </ul>
	  </div>
	  <div class="nav-collapse collapse">
          </div><!--/.nav-collapse -->
        </div>
      </div>
      <!-- fin Header1-->
      <!-- Header2-->
      <div class="navbar-inner headerbar2">
	<!--<div id="headerbar2-rightBackground">&nbsp;</div>-->
        <div class="container" id="barnav">
          <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
	  <div class="row-fluid nav-collapse collapse">
            <ul class="nav span8">
              <li class="span4 importantText"><a href="{getlink page="cvteque"}">{$header_trad->headerTrPresta}</a></li>
              <li class="span4 importantText"><a href="{getlink page="missions"}">{$header_trad->headerTrProjet}</a></li>
	      <li class="span4 last"><a href="{getLink page='inscription'}"><img src="web/img/pictoInscription.png" alt="img search"/>{$header_trad->headerInscription}</a></li>
	      
	    </ul>
            <form class="navbar-form pull-left span4" method="post" action="{getlink page="missions"}">
	      <button type="submit" class="span2 btn"><img src="web/img/pictoSearch.png" alt="img search"/></button>
              <input class="span10 headersearch" type="text" name="search[mots_cle]" placeholder="{$globale_trad->menu_rechercher_projets}" value=""/>
	      <input type="hidden" name="action" value="search"/>
            </form>
	    <div class="clearfix"></div>
          </div><!--/.nav-collapse -->
        </div>
	
      </div>
    </div>
  
    <!-- fin Header2-->
    <!-- Fenetre modale pour l'identification-->
    <div id="identification" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <form method="POST" action="{getLink page='home'}">
	<div class="modal-header">
	  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
	  <h3 id="myModalLabel">{$header_trad->headerConnexion}</h3>
	</div>
	<div class="modal-body row-fluid">
	  <div class="span3 offset1">
	    {$globale_trad->label_connexion}
	  </div>
	  <div class="span6 offset1">
	    <input class="modalconnect span12" type="text" placeholder="login" name="usr_email"/>
	  </div>
	  <div class="clearfix"></div>
	  <div class="span3 offset1">
	    {$globale_trad->label_mdp}
	  </div>
	  <div class="span6 offset1">
	    <input class="modalconnect span12" type="password" placeholder="password" name="usr_password"/>
	    <input type="hidden" name="action" value="connexion">
	    <input type="hidden" name="page" value="home">
	      
	    <label class="checkbox"><input type="checkbox" name="remember" > {$globale_trad->label_se_souvenir} </label>
	  </div>
	  
	  
	  <div class="clearfix"></div>
	  <div class="span9 offset2">
	    <button class="btn btn-primary span4  margin-top-20 pull-right" type="submit"><span class="icon-ok icon-white"></span> {$globale_trad->bt_connexion} </button>
	  </div>
	  <div class="clearfix"></div>
	</div>
      </form>
      <div class="modal-footer">
	<a href="{getLink page="inscription"}" class="blue"> {$globale_trad->new_account} </a> <span class="margin-left-10 margin-right-10 greyLight">|</span> 
	<a href="#password" class="toggle passwordLost" id="password">{$header_trad->headerMdpOublie}</a>
	<form method="post" id="password_result" action="{getLink page='home'}" class="none">
	  <div class="row-fluid">
	    <p>
		  {$header_trad->hpSendNewMdp}
	    </p>
	    <div class="span2 offset2">
	      {$globale_trad->label_email}
	    </div>
	    <div class="span6">
	      <input class="modalconnect span12" type="text" placeholder="e-mail" name="usr_email"/>
	      <input type="hidden" value="password" name="action">
	    </div>
	    <div class="span8 offset2">
	      <button class="btn btn-primary span4  margin-top-20 pull-right" type="submit"><span class="icon-ok icon-white"></span>{$globale_trad->bt_envoyer}</button>
	    </div>
	  </div>
	</form>
      </div>
    </div>
    
    <!--fin de la fenetre modale -->