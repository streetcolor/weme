<tr class="MissionsListBackground">
	<td height="20" class="last" colspan="5"><a class="linkCell" href="{getLink page='mission' params="id_mission="|cat:$mission->getId_mission()}"></a></td>
</tr>

<tr class="MissionsListBackground">

	<td class="span4 first">
		<a class="linkCell" href="{getLink page='mission' params="id_mission="|cat:$mission->getId_mission()}">
		{if $mission->getNew()==0}
			<span class="flag missionS{$mission->getStatut()->id_statut}  pull-left"></span>
		{else}
			<span class="flag missionNew  pull-left"></span>
		{/if}
		<h4 class="blue">{$mission->getTitre()}</h4>
		<div class="clearfix"></div>
		<p>{$mission->getDescription()|truncate:300|@strip_tags}(+)</p>
		</a>
	</td>

	<td class="span2" align="center">
			<a class="linkCell" href="{getLink page='mission' params="id_mission="|cat:$mission->getId_mission()}">
			<p>{$mission->getLocalite()}</p>
			</a>
	</td>

	<td class="span2" align="center">
			<a class="linkCell" href="{getLink page='mission' params="id_mission="|cat:$mission->getId_mission()}">
			<p>{$mission->getTarif(true)}</p>
			</a>
	</td>

	<td class="span2" align="center">
			<a class="linkCell" href="{getLink page='mission' params="id_mission="|cat:$mission->getId_mission()}">
			<p>{$globale_trad->label_du} {$mission->getDebut()}<br/>{$globale_trad->label_au} {$mission->getFin()}</p>
			</a>
	</td>

	<td class="span2 last" align="center">
			<a class="linkCell" href="{getLink page='mission' params="id_mission="|cat:$mission->getId_mission()}">

			<p>{$mission->getReponses(true)} {$trad->missionNbReponses}</p>
			
			</a>
	</td>
</tr>

<tr class="MissionsListBackground">
	<td height="20" class="last" colspan="5"></td>
</tr>

<tr>
    <td colspan="5" class="last MissionsListActions">
	    <a class="pull-left" href="{getLink page='mission' params="id_mission="|cat:$mission->getId_mission()}">
			<span class="ico35 detail pull-left"></span>
			 <span class="pull-left">{$trad->missionDetail}</span>
	    </a>
	    
	    <a class="pull-left" href="{getLink page='inscription'}">
			<span class="ico35 addFav pull-left"></span>
			<span class="pull-left">{$trad->missionSaveProjet}</span>
	    </a>
	    

    </td>
</tr>


