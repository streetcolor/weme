  <!-- fin encart sliders -->
      <!-- encart zone partenaire -->
      <div class="row-fluid zonepartenaire">
        <div class="span2 colpartenaire">
          <h4>{$footer_trad->label_titre_partenaire}</h4>
        </div>
        <div class="span2 partenaire">
          <p><img src="web/img/logo/partenaires/logo1.png" alt=""/></p>
       </div>
        <div class="span2 partenaire">
          <p><img src="web/img/logo/partenaires/logo2.png" alt=""/></p>
        </div>
	<div class="span2 partenaire">
          <p><img src="web/img/logo/partenaires/logo3.png" alt=""/></p>
        </div>
	<div class="span2 partenaire">
          <p><img src="web/img/logo/partenaires/logo4.jpg" alt=""/></p>
        </div>
	<div class="span2 partenaire">
          <p><img src="web/img/logo/partenaires/logo2.png" alt=""/></p>
        </div>
      </div>
      
      <!-- fin encart zone partenaire -->
      <!-- fin du corps HP -->
      
      <!-- debut footer -->
      <!-- debut de pied de page -->
      <div class="row-fluid zonefooter">
        <div class="span2 footerlogo">
          <a class="brand " href="{getLink page="home"}"><img src="web/img/logo/logo_visiteur.png" alt="logo weme"/></a>
        </div>
        <div class="span3 footerlist">
	  <img src="web/img/pictoEntrepreneurFooter.png" alt="picto Entrepreneur" class="pull-left"/>
          <h2>{$footer_trad->footerEntrepreneur}</h2>
	 
	  <ul class="decalage">
	    <li><a href="{getLink page="cl_inscription"}">{$footer_trad->footerClientProjet}</a></li>
	    <li><a href="{getLink page="cvteque"}">{$footer_trad->footerClientCvtheque}</a></li>
	    <li><a href="{getLink page="services"}">{$footer_trad->footerClientSourcing}</a></li>
	  </ul>
       </div>
        <div class="span3 footerlist">
	  <img src="web/img/pictoPrestataireFooter.png" alt="picto Prestataire" class="pull-left"/>
          <h2>{$footer_trad->footerPrestataire}</h2>
	  
	  <ul class="decalage">
	    <li><a href="{getLink page="pr_inscription"}">{$footer_trad->footerPrestaCv}</a></li>
	    <li><a href="{getLink page="missions"}">{$footer_trad->footerPrestaProjet}</a></li>
	  </ul>
        </div>
	<div class="span3 footerlist ">
	  <img src="web/img/ico/ico_chaine.png" alt="picto Prestataire" class="pull-left"/>
          <h2>{$footer_trad->footerLiensUtiles}</h2>
	  
	    <ul>
		  <li><a href="{getLink page="about"}">{$footer_trad->footerAPropos}</a></li>
		  <li><a href="{getLink page="mentions"}">{$footer_trad->footerMentions}</a></li>
		  <li><a href="{getLink page="sitemap"}">{$footer_trad->footerSitemap}</a></li>
		  <li><a href="{getLink page="contact"}">{$footer_trad->footerContact}</a></li>
	    </ul>
        </div>
      </div>
      <!-- fin du pied de page -->
      <div id="footer" class="row-fluid">
        <div class="pull-left copyright">
	  <p>{$footer_trad->footerCopyright}</p>
	</div>
	<div class="pull-right socialfooter span4">
	  <a href=""><img alt="twitter" src="web/img/pictoTwitter.png"/></a>
	  <a href=""><img alt="facebook" src="web/img/pictoFb.png"/></a>
	  <a href=""><img alt="linkedin" src="web/img/pictoLinkedin.png"/></a>
	  <a href=""><img alt="googleplus" src="web/img/pictoGoogle.png"/></a>
	</div>
	<div class="clearfix"></div>
      </div>
      <!-- fin du footer -->

    </div> <!-- /container -->