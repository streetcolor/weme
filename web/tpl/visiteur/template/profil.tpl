<tr class="MissionsListBackground">
	<td class=" last" height="20" colspan="4"></td>
</tr>

<tr class="MissionsListBackground">
	
	<td class="first ">
		
		{$globale_trad->label_nom} : {$profil->getNom()}
		<br>
		{$globale_trad->label_tel_short} {$profil->getFixe(true)}
		<br>
		{$globale_trad->label_dispo} : {$profil->getDisponibilite()}
		<br>
		{$globale_trad->label_tjm} : {$profil->getTjm(true)}
		
	</td>
	
	<td class="" align="">
		<div class="padding-left-10 padding-right-10">
		<h4 class="blue">{$profil->getTitre()}</h4>
		<p>
			{$profil->getDescription()|truncate:200}
		</p>
		</div>
	</td>
	
	<td class="" align="">
		<ul>
		{foreach $profil->getLocalites()->localites as $localite}
				{if $localite@iteration > 5}<li>...</li>{break}{/if}
				<li>{$localite}</li>
		{/foreach}
		</ul>
	</td>
	
	<td class="last" align="">
		<ul>
		{foreach $profil->getCompetences()->competences as $competence}
				{if $competence@iteration > 5}<li>...</li>{break}{/if}
				<li>{$competence->adm_competence}</li>
		{/foreach}
		</ul>
	</td>


</tr>

<tr class="MissionsListBackground">
	<td class=" last" height="20" colspan="4"></td>
</tr>
<tr>
				<td colspan="4" class="last MissionsListActions">
					<a class=" pull-left" href="web/scripts/cv.php?page=cv&action=cv&id_prestataire_profil={$profil->getId_profil()}">
						<span class="ico35 pdf pull-left"></span>
						<span class="pull-left">{$globale_trad->bt_dl_pdf}</span>
					</a>
					
					<a class="showCv pull-left" href="{getLink page='cv' action="cv" params="id_prestataire_profil="|cat:$profil->getId_profil()}{if isset($smarty.request.search['mots_cle'])}&mots_cle={$smarty.request.search['mots_cle']}{/if}">
						<span class="ico35 detail pull-left"></span>
						<span class="pull-left">{$globale_trad->bt_plus_details}</span>

					</a>			
				</td>
</tr>      