-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost:3306
-- Généré le :  Mar 20 Septembre 2016 à 22:18
-- Version du serveur :  5.5.41-0ubuntu0.14.04.1
-- Version de PHP :  5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `weme`
--

-- --------------------------------------------------------

--
-- Structure de la table `adm_categories`
--

CREATE TABLE `adm_categories` (
  `id_categorie` int(255) NOT NULL,
  `adm_categorie_fr` varchar(45) NOT NULL,
  `adm_categorie_es` varchar(45) NOT NULL,
  `adm_categorie_en` varchar(45) NOT NULL,
  `actif` tinyint(1) NOT NULL,
  `var` varchar(45) DEFAULT NULL,
  `cree_le` datetime NOT NULL,
  `modifie_le` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `adm_categories`
--

INSERT INTO `adm_categories` (`id_categorie`, `adm_categorie_fr`, `adm_categorie_es`, `adm_categorie_en`, `actif`, `var`, `cree_le`, `modifie_le`) VALUES
(1, 'Culture et production animale, chasse et serv', '', '', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(3, ' Pêche et aquaculture', '3', '3', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(4, ' Extraction de houille et de lignite', '4', '4', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(5, ' Extraction d''hydrocarbures', '5', '5', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(6, ' Extraction de minerais métalliques', '6', '6', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(7, ' Autres industries extractives', '7', '7', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(8, ' Services de soutien aux industries extractiv', '8', '8', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(9, ' Industries alimentaires', '9', '9', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(10, ' Fabrication de boissons', '10', '10', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(11, ' Fabrication de produits à base de tabac', '11', '11', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(12, ' Fabrication de textiles', '12', '12', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(13, ' Industrie de l''habillement', '13', '13', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(14, ' Industrie du cuir et de la chaussure', '14', '14', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(15, ' Travail du bois et fabrication d''articles en', '15', '15', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(16, ' Industrie du papier et du carton', '16', '16', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(17, ' Imprimerie et reproduction d''enregistrements', '17', '17', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(18, ' Cokéfaction et raffinage', '18', '18', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(19, ' Industrie chimique', '19', '19', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(20, ' Industrie pharmaceutique', '20', '20', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(21, ' Fabrication de produits en caoutchouc et en ', '21', '21', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(22, ' Fabrication d''autres produits minéraux non m', '22', '22', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(23, ' Métallurgie', '23', '23', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(24, ' Fabrication de produits métalliques, à l''exc', '24', '24', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(25, ' Fabrication de produits informatiques, élect', '25', '25', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(26, ' Fabrication d''équipements électriques', '26', '26', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(27, ' Fabrication de machines et équipements n.c.a', '27', '27', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(28, ' Industrie automobile', '28', '28', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(29, ' Fabrication d''autres matériels de transport', '29', '29', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(30, ' Fabrication de meubles', '30', '30', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(31, ' Autres industries manufacturières', '31', '31', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(32, ' Réparation et installation de machines et d''', '32', '32', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(33, ' Production et distribution d''électricité, de', '33', '33', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(34, ' Captage, traitement et distribution d''eau', '34', '34', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(35, ' Collecte et traitement des eaux usées', '35', '35', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(36, ' Collecte, traitement et élimination des déch', '36', '36', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(37, ' Dépollution et autres services de gestion de', '37', '37', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(38, ' Construction de bâtiments', '38', '38', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(39, ' Génie civil', '39', '39', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(40, ' Travaux de construction spécialisés', '40', '40', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(41, ' Commerce et réparation d''automobiles et de m', '41', '41', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(42, ' Commerce de gros, à l''exception des automobi', '42', '42', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(43, ' Commerce de détail, à l''exception des automo', '43', '43', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(44, ' Transports terrestres et transport par condu', '44', '44', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(45, ' Transports par eau', '45', '45', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(46, ' Transports aériens', '46', '46', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(47, ' Entreposage et services auxiliaires des tran', '47', '47', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(48, ' Activités de poste et de courrier', '48', '48', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(49, ' Hébergement', '49', '49', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(50, ' Restauration', '50', '50', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(51, ' Édition', '51', '51', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(52, ' Production de films cinématographiques, de v', '52', '52', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(53, ' Programmation et diffusion', '53', '53', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(54, ' Télécommunications', '54', '54', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(55, ' Programmation, conseil et autres activités i', '55', '55', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(56, ' Services d''information', '56', '56', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(57, ' Activités des services financiers, hors assu', '57', '57', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(58, ' Assurance', '58', '58', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(59, ' Activités auxiliaires de services financiers', '59', '59', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(60, ' Activités immobilières', '60', '60', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(61, ' Activités juridiques et comptables', '61', '61', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(62, ' Activités des sièges sociaux ; conseil de ge', '62', '62', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(63, ' Activités d''architecture et d''ingénierie ; a', '63', '63', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(64, ' Recherche-développement scientifique', '64', '64', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(65, ' Publicité et études de marché', '65', '65', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(66, ' Autres activités spécialisées, scientifiques', '66', '66', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(67, ' Activités vétérinaires', '67', '67', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(68, ' Activités de location et location-bail', '68', '68', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(69, ' Activités liées à l''emploi', '69', '69', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(70, ' Activités des agences de voyage, voyagistes,', '70', '70', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(71, ' Enquêtes et sécurité', '71', '71', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(72, ' Services relatifs aux bâtiments et aménageme', '72', '72', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(73, ' Activités administratives et autres activité', '73', '73', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(74, ' Administration publique et défense ; sécurit', '74', '74', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(75, ' Enseignement', '75', '75', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(76, ' Activités pour la santé humaine', '76', '76', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(77, ' Hébergement médico-social et social', '77', '77', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(78, ' Action sociale sans hébergement', '78', '78', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(79, ' Activités créatives, artistiques et de spect', '79', '79', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(80, ' Bibliothèques, archives, musées et autres ac', '80', '80', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(81, ' Organisation de jeux de hasard et d''argent', '81', '81', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(82, ' Activités sportives, récréatives et de loisi', '82', '82', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(83, ' Activités des organisations associatives', '83', '83', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(84, ' Réparation d''ordinateurs et de biens personn', '84', '84', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(85, ' Autres services personnels', '85', '85', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(86, ' Activités des ménages en tant qu''employeurs ', '86', '86', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(87, ' Activités indifférenciées des ménages en tan', '87', '87', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(88, ' Activités des organisations et organismes ex', '88', '88', 1, NULL, '2012-07-11 17:11:32', '2012-07-11 17:11:32'),
(89, 'Edition/Imprimerie', '89', '89', 1, NULL, '2012-07-11 15:11:32', '2012-07-11 17:11:32'),
(90, 'Secteur assurance', '90', '90', 1, NULL, '2012-07-17 15:29:22', '2012-07-17 17:29:22'),
(91, 'secteur pharmaceutique', '91', '91', 1, NULL, '2012-08-22 14:57:42', '2012-08-22 16:57:42'),
(92, 'secteur paramadéciale', '92', '92', 1, NULL, '2012-08-22 14:59:09', '2012-08-22 16:59:09'),
(93, 'Sport', '93', '93', 1, NULL, '2012-08-23 10:04:48', '2012-08-23 12:04:48'),
(94, 'Sourcing', '94', '94', 1, NULL, '2012-08-29 09:50:55', '2012-08-29 11:50:55'),
(95, 'Gestion des dechets', '95', '95', 1, NULL, '2012-09-24 09:42:02', '2012-09-24 11:42:02'),
(96, 'Recyclages', '96', '96', 1, NULL, '2012-09-24 09:42:13', '2012-09-24 11:42:13'),
(97, 'Cinema', '97', '97', 1, NULL, '2012-09-24 10:00:27', '2012-09-24 12:00:27'),
(98, 'Restauration', '98', '98', 1, NULL, '2012-09-25 12:41:43', '2012-09-25 14:41:43'),
(99, 'Société de services informatiques', '99', '99', 1, NULL, '2012-12-13 16:32:10', '2012-12-13 17:32:10'),
(100, 'Tourisme', '100', '100', 1, NULL, '2013-02-04 12:41:47', '2013-02-04 13:41:47'),
(101, 'Electronique', '101', '101', 1, NULL, '2013-02-05 12:21:16', '2013-02-05 13:21:16'),
(102, 'Agroalimentaire', '102', '102', 1, NULL, '2013-02-05 12:21:51', '2013-02-05 13:21:51'),
(103, 'Culture et production animale, chasse et serv', '103', '103', 1, NULL, '2013-02-07 10:24:11', '0000-00-00 00:00:00'),
(104, 'Ecole / Université', '104', '104', 1, NULL, '2013-02-06 14:33:17', '2013-02-06 15:33:17');

-- --------------------------------------------------------

--
-- Structure de la table `adm_civilites`
--

CREATE TABLE `adm_civilites` (
  `id_civilite` int(11) NOT NULL,
  `adm_civilite_fr` varchar(45) NOT NULL,
  `adm_civilite_en` varchar(45) NOT NULL,
  `adm_civilite_es` varchar(45) NOT NULL,
  `actif` int(11) NOT NULL,
  `var` varchar(45) DEFAULT NULL,
  `cree_le` datetime NOT NULL,
  `modifie_le` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `adm_civilites`
--

INSERT INTO `adm_civilites` (`id_civilite`, `adm_civilite_fr`, `adm_civilite_en`, `adm_civilite_es`, `actif`, `var`, `cree_le`, `modifie_le`) VALUES
(1, 'M', 'Mr', 'Sr.', 1, NULL, '2012-01-20 00:00:00', '2013-01-21 16:05:52'),
(2, 'Mme', 'Mrs', 'Sra.', 1, NULL, '2011-12-27 11:30:31', '2013-01-29 20:52:28');

-- --------------------------------------------------------

--
-- Structure de la table `adm_competences`
--

CREATE TABLE `adm_competences` (
  `id_competence` int(255) NOT NULL,
  `adm_competence_fr` varchar(45) DEFAULT NULL,
  `adm_competence_en` varchar(45) DEFAULT NULL,
  `adm_competence_es` varchar(45) DEFAULT NULL,
  `actif` tinyint(1) NOT NULL,
  `var` varchar(45) DEFAULT NULL,
  `cree_le` datetime NOT NULL,
  `modifie_le` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `adm_competences`
--

INSERT INTO `adm_competences` (`id_competence`, `adm_competence_fr`, `adm_competence_en`, `adm_competence_es`, `actif`, `var`, `cree_le`, `modifie_le`) VALUES
(1, '', NULL, 'jQuery', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:38:18'),
(2, NULL, NULL, 'Google Webmaster Tools', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(3, NULL, NULL, 'Social Design', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(4, NULL, NULL, 'Email Marketing', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(5, NULL, NULL, 'Wordpress', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(6, NULL, NULL, 'Wordpress Design', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(7, NULL, NULL, 'HTML + CSS', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(8, NULL, NULL, 'Web Design', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(9, NULL, NULL, 'Packaging', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(10, NULL, NULL, 'Catalog Design', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(11, NULL, NULL, 'Printdesign', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(12, NULL, NULL, 'Logo Design', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(13, NULL, NULL, 'Corporate Identity', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(14, NULL, NULL, 'Original Artwork', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(15, NULL, NULL, 'Design Strategy', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(16, NULL, NULL, 'Freelance Graphics', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(17, NULL, NULL, 'Graphic Design', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(18, NULL, NULL, 'Diseño Gráfico', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(19, NULL, NULL, 'Trabajo en equipo', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(20, NULL, NULL, 'Rapido', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(21, NULL, NULL, 'Habil', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(22, NULL, NULL, 'Responsable', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(23, NULL, NULL, 'HTML 5', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(24, NULL, NULL, 'guin', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(25, NULL, NULL, 'guionista', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(26, NULL, NULL, 'dialoguista', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(27, NULL, NULL, 'dibujante', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(28, NULL, NULL, 'acuarelas', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(29, NULL, NULL, 'acrilicos', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(30, NULL, NULL, 'marcadores', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(31, NULL, NULL, 'lapices', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(32, NULL, NULL, '3D Studio Max', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(33, NULL, NULL, 'Dreamweaver', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(34, NULL, NULL, 'Illustrator', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(35, NULL, NULL, 'Photoshop', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(36, NULL, NULL, 'InDesign', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(37, NULL, NULL, 'After Effects', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(38, NULL, NULL, 'Flash', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(39, NULL, NULL, 'Adobe Creative Suite', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(40, NULL, NULL, 'Adobe Fireworks', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(41, NULL, NULL, 'Responsabilidad', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(42, NULL, NULL, 'dedicación y compromiso con el trabajo', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(43, NULL, NULL, 'Buenas relaciones interpersonales.', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(44, NULL, NULL, 'Predisposición al aprendizaje y la enseñanza.', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(45, NULL, NULL, 'Buen desempeño en el trabajo individual y gru', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(46, NULL, NULL, 'Fácil manejo y uso de tecnología audiovisual.', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(47, NULL, NULL, 'Conocimiento sobre funcionamiento de estudios', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(48, NULL, NULL, 'Conocimiento de Cámara', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(49, NULL, NULL, 'Iluminación y sonido.', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(50, NULL, NULL, 'Conocimientos en Fotografía fija análoga y di', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(51, NULL, NULL, 'Conocimientos en Fotografía para cine y video', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(52, NULL, NULL, 'Conocimientos en Efectos visuales digitales.', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(53, NULL, NULL, 'Conocimientos en Montaje y edición de imagen ', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(54, NULL, NULL, 'Conocimientos en Animación por computadora (C', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(55, NULL, NULL, 'Conocimientos en Procesos y tecnologías de pr', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(56, NULL, NULL, 'Conocimientos en escritura de Guión para cine', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(57, NULL, NULL, 'Producción gráfica', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(58, NULL, NULL, 'Ilustración', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(59, NULL, NULL, 'Retoque', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(60, NULL, NULL, 'Docencia', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(61, NULL, NULL, 'Art Direction', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(62, NULL, NULL, 'Interaction Design', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(63, NULL, NULL, 'Digital Strategy', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(64, NULL, NULL, 'Branding & Identity', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(65, NULL, NULL, 'User Experience', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(66, NULL, NULL, 'Storyboarding', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(67, NULL, NULL, 'Advertising', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(68, NULL, NULL, 'Photography', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(69, NULL, NULL, 'Digital Painting', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(70, NULL, NULL, 'Integrated Marketing', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(71, NULL, NULL, 'Redacción', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(72, NULL, NULL, 'periodismo', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(73, NULL, NULL, 'periodismo digital', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(74, NULL, NULL, 'Blogger', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(75, NULL, NULL, 'Redes Sociales', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(76, NULL, NULL, 'Community Manager', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(77, NULL, NULL, 'Orden', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(78, NULL, NULL, 'Creatividad', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(79, NULL, NULL, 'Puntualidad', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(80, NULL, NULL, 'Producción Audiovisual', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(81, NULL, NULL, 'Realización Audiovisual', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(82, NULL, NULL, 'Diseño Web', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(83, NULL, NULL, 'CSS3', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(84, NULL, NULL, 'PHP', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(85, NULL, NULL, 'MySQL', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(86, NULL, NULL, 'Maquetación', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(87, NULL, NULL, 'java script', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(88, NULL, NULL, 'Action Script', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(89, NULL, NULL, 'CMS', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(90, NULL, NULL, 'Motion Gprahic', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(91, NULL, NULL, 'Premiere', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(92, NULL, NULL, 'Posicionamiento Web', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(93, NULL, NULL, 'Wardrobe- Fashion stylist- Costume designer- ', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(94, NULL, NULL, 'motion graphics', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(95, NULL, NULL, 'Fotografia', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(96, NULL, NULL, 'vegas', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(97, NULL, NULL, 'SEO', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(98, NULL, NULL, 'Adwords', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(99, NULL, NULL, 'copywriting', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(100, NULL, NULL, 'facebook ads', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(101, NULL, NULL, 'Desarrollador Java', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(102, NULL, NULL, 'Analista de Sistemas', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(103, NULL, NULL, 'moda', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(104, NULL, NULL, 'training de modelos', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(105, NULL, NULL, 'representacion de modelos publicitarias', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(106, NULL, NULL, 'Adobe Photoshop', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(107, NULL, NULL, 'Adobe Illustrator', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(108, NULL, NULL, 'Paquete Adobe Diseño (PS', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(109, NULL, NULL, 'AI', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(110, NULL, NULL, 'FL', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(111, NULL, NULL, 'DW', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(112, NULL, NULL, 'AE)', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(113, NULL, NULL, 'Diseño de Imagen Corporativa', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(114, NULL, NULL, 'Adobe Flash', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(115, NULL, NULL, 'Adobe Dreamweaver', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(116, NULL, NULL, 'Diseño', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(117, NULL, NULL, 'Gráfica', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(118, NULL, NULL, 'Editorial', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(119, NULL, NULL, 'Historia', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(120, NULL, NULL, 'Sociología', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(121, NULL, NULL, 'Educación', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(122, NULL, NULL, 'Documentalismo', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(123, NULL, NULL, 'BAN', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(124, NULL, NULL, 'Logos', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(125, NULL, NULL, 'Flyers', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(126, NULL, NULL, 'Banners', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(127, NULL, NULL, 'Folletos', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(128, NULL, NULL, 'Diseño UI', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(129, NULL, NULL, 'fsdgf', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(130, NULL, NULL, 'Illustrator.', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(131, NULL, NULL, 'Web 2.0', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(132, NULL, NULL, 'Marketing Strategy', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(133, NULL, NULL, 'Marketing Communications', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(134, NULL, NULL, 'Marketing', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(135, NULL, NULL, 'French', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(136, NULL, NULL, 'Online Advertising', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(137, NULL, NULL, 'Social Media', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(138, NULL, NULL, 'Social Media Marketing', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(139, NULL, NULL, 'Marketing Management', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(140, NULL, NULL, 'Spanish', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(141, NULL, NULL, 'Public Relations', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(142, NULL, NULL, 'Web Analytics', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(143, NULL, NULL, 'Web Marketing', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(144, NULL, NULL, 'Corporate Communications', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(145, NULL, NULL, 'Digital Marketing', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(146, NULL, NULL, 'Más de 15 años de experiencia en diseño de: B', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(147, NULL, NULL, 'Logo', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(148, NULL, NULL, 'Publicidad Gráfica', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(149, NULL, NULL, 'Brochures', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(150, NULL, NULL, 'Papelería', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(151, NULL, NULL, 'Folletería', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(152, NULL, NULL, 'Posters', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(153, NULL, NULL, 'POP', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(154, NULL, NULL, 'Gigantografías', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(155, NULL, NULL, 'Revistas', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(156, NULL, NULL, 'Sitios Web', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(157, NULL, NULL, 'Aplicaciones Multimediales', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(158, NULL, NULL, 'Interfeces de Usuario', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(159, NULL, NULL, 'Amplio manejo de programas de diseño como Ado', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(160, NULL, NULL, 'AftewrEffects', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(161, NULL, NULL, 'Dreamweaver. Lenguajes de prograamcion: HMTL', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(162, NULL, NULL, 'CSS', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(163, NULL, NULL, 'JavaScript', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(164, NULL, NULL, 'Web Development', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(165, NULL, NULL, 'SQL', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(166, NULL, NULL, 'Android', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(167, NULL, NULL, 'iOS development', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(168, NULL, NULL, 'Visual Communication', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(169, NULL, NULL, 'Project Management', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(170, NULL, NULL, 'Editorial Design', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(171, NULL, NULL, 'Group Management', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(172, NULL, NULL, 'Teaching', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(173, NULL, NULL, 'Software', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(174, NULL, NULL, 'Freelance', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(175, NULL, NULL, 'Elgg', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(176, NULL, NULL, 'Opencart', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(177, NULL, NULL, 'Magento', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(178, NULL, NULL, 'Oscommerce', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(179, NULL, NULL, 'Ilustrator', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(180, NULL, NULL, 'Joomla', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(181, NULL, NULL, 'Modx', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(182, NULL, NULL, 'Drupal', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(183, NULL, NULL, 'Innodb', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(184, NULL, NULL, 'Moodle', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(185, NULL, NULL, 'Experiencia', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(186, NULL, NULL, 'cine', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(187, NULL, NULL, 'manejo de Adobe Illustrator', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(188, NULL, NULL, 'amplio manejo de programas de diseño como Ill', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(189, NULL, NULL, 'Conocimientos basicos de Diseño Web', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(190, NULL, NULL, 'Manejo de herramientas de edición de video', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(191, NULL, NULL, 'Conocimientos en ilustracion', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(192, NULL, NULL, 'diseño editorial', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(193, NULL, NULL, 'Diseño de Plataformas', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(194, NULL, NULL, 'User Interface Design', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(195, NULL, NULL, 'Corporate Branding', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(196, NULL, NULL, 'Creative Branding', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(197, NULL, NULL, 'Brand Communication', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(198, NULL, NULL, 'Communications Strategy', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(199, NULL, NULL, 'Animation', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(200, NULL, NULL, 'Signage', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(201, NULL, NULL, 'Environmental Design', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(202, NULL, NULL, 'Branding Development', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(203, NULL, NULL, 'Brand Development', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(204, NULL, NULL, 'Multimedia Communications', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(205, NULL, NULL, 'Multimedia Solutions', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(206, NULL, NULL, 'Color', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(207, NULL, NULL, 'pintura', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(208, NULL, NULL, 'HTML', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(209, NULL, NULL, 'Vector Illustration', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(210, NULL, NULL, 'Sony Vegas', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(211, NULL, NULL, 'Corel Draw', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(212, NULL, NULL, 'Social Networking', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(213, NULL, NULL, 'Facebook', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(214, NULL, NULL, 'Twitter', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(215, NULL, NULL, 'English', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(216, NULL, NULL, 'Soy Diseñador Gráfico (UBA)', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(217, NULL, NULL, 'poseo experiencia y trayectoria en varios est', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(218, NULL, NULL, 'una editorial', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(219, NULL, NULL, 'una agencia de publicidad y una estampería qu', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(220, NULL, NULL, 'stands e identidad corporativa.', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(221, NULL, NULL, 'Illustrator CS5', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(222, NULL, NULL, 'Illustrator CS6', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(223, NULL, NULL, 'Photoshop CS6', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(224, NULL, NULL, 'Indesign CS6', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(225, NULL, NULL, 'QuarkXPress 8', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(226, NULL, NULL, 'Dreamweaver CS5', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(227, NULL, NULL, 'Flash CS6', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(228, NULL, NULL, 'Dreamweaver CS6', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(229, NULL, NULL, 'Fireworks CS6', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(230, NULL, NULL, 'Microsoft Office', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(231, NULL, NULL, 'Microsoft Office 2010', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(232, NULL, NULL, 'Cinema 4D', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(233, NULL, NULL, 'Final Cut', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(234, NULL, NULL, 'Television', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(235, NULL, NULL, 'Creative Strategy', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(236, NULL, NULL, 'Creative Direction', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(237, NULL, NULL, 'Interactive Advertising', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(238, NULL, NULL, 'Creative Services', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(239, NULL, NULL, 'Content Strategy', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(240, NULL, NULL, 'Screenwriting', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(241, NULL, NULL, 'Radio Advertising', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:01'),
(242, NULL, NULL, 'Outdoor Advertising', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(243, NULL, NULL, 'Campaigns', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(244, NULL, NULL, 'Concept Generation', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(245, NULL, NULL, 'Media Planning', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(246, NULL, NULL, 'Video Production', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(247, NULL, NULL, 'Broadcast', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(248, NULL, NULL, 'Conceptualizer', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(249, NULL, NULL, 'Concept Development', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(250, NULL, NULL, 'Creative Professionals', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(251, NULL, NULL, 'Creative Content Production', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(252, NULL, NULL, 'Promotions', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(253, NULL, NULL, 'Entertainment', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(254, NULL, NULL, 'Film Production', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(255, NULL, NULL, 'Digital Media', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(256, NULL, NULL, 'New Media', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(257, NULL, NULL, 'Post Production', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(258, NULL, NULL, 'Commercials', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(259, NULL, NULL, 'Press Releases', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(260, NULL, NULL, 'Radio', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(261, NULL, NULL, 'Film', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(262, NULL, NULL, 'Video', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(263, NULL, NULL, 'Broadcast Television', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(264, NULL, NULL, 'Producing', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(265, NULL, NULL, 'Designs', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(266, NULL, NULL, 'Product Marketing', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(267, NULL, NULL, 'Merchandising', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(268, NULL, NULL, 'Magazines', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(269, NULL, NULL, 'Newsletters', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(270, NULL, NULL, 'Icon Design', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(271, NULL, NULL, 'desarrollo web', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(272, NULL, NULL, 'formularios web', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(273, NULL, NULL, 'marketing online y posicionamiento web', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(274, NULL, NULL, 'marketing online', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(275, NULL, NULL, 'programador', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(276, NULL, NULL, 'www.raulcorrado.com.ar', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(277, NULL, NULL, 'www.raulcorradophotos.com.ar', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(278, NULL, NULL, 'Illustration', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(279, NULL, NULL, 'Comics', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(280, NULL, NULL, 'Drawing', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(281, NULL, NULL, 'Digital Art', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(282, NULL, NULL, 'Graphics', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(283, NULL, NULL, 'Sketching', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(284, NULL, NULL, 'Character Designs', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(285, NULL, NULL, 'XHTML', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(286, NULL, NULL, 'jQuery UI', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(287, NULL, NULL, 'Brand Architecture', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(288, NULL, NULL, 'Visual Identity Systems', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(289, NULL, NULL, 'Typography', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(290, NULL, NULL, 'Semantic HTML', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(291, NULL, NULL, 'JavaScript Libraries', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(292, NULL, NULL, 'Web Standards', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(293, NULL, NULL, 'W3C Validation', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(294, NULL, NULL, 'Grid Systems', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(295, NULL, NULL, 'SketchUp', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(296, NULL, NULL, 'Vray', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(297, NULL, NULL, '3D rendering', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(298, NULL, NULL, 'Digital Photography', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(299, NULL, NULL, 'Stop Motion', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(300, NULL, NULL, 'digital retouching', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(301, NULL, NULL, 'Macro Photography', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(302, NULL, NULL, 'Taller Aeronáutico de Reparación', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(303, NULL, NULL, 'Microsoft Word', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(304, NULL, NULL, 'Microsoft Excel', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(305, NULL, NULL, 'Microsoft Acces', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(306, NULL, NULL, 'Certifications', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(307, NULL, NULL, 'Stock Control', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(308, NULL, NULL, 'biblioteca técnica', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(309, NULL, NULL, 'rvsm', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(310, NULL, NULL, 'Facturacion', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(311, NULL, NULL, 'metrología', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(312, NULL, NULL, 'Aviation', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(313, NULL, NULL, 'Avionics', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(314, NULL, NULL, 'Electronics', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(315, NULL, NULL, 'Nikon', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(316, NULL, NULL, 'Panoramic Photography', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(317, NULL, NULL, 'Aircraft Maintenance', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(318, NULL, NULL, 'repair station', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(319, NULL, NULL, 'component maintenance manuals', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(320, NULL, NULL, 'test set', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(321, NULL, NULL, 'notepad', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(322, NULL, NULL, 'Tallado en Madera', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(323, NULL, NULL, 'Tallado de Esculturas', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(324, NULL, NULL, 'Sketchbook Pro', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(325, NULL, NULL, 'Identidad corporativa', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(326, NULL, NULL, 'Redacción de contenidos', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(327, NULL, NULL, 'Moderador', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(328, NULL, NULL, 'Retoque Digital', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(329, NULL, NULL, 'Investigación', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(330, NULL, NULL, 'Camarógrafa', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(331, NULL, NULL, 'Edición audiovisual', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(332, NULL, NULL, '3D', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(333, NULL, NULL, 'maya', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(334, NULL, NULL, 'Team Leadership', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(335, NULL, NULL, 'Human Resources', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(336, NULL, NULL, 'ERP', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(337, NULL, NULL, 'Windows', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(338, NULL, NULL, 'toon boom', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(339, NULL, NULL, 'cut out', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(340, NULL, NULL, 'animacion 2D', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(341, NULL, NULL, 'Diseño Multimedial', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(342, NULL, NULL, 'OOP', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(343, NULL, NULL, 'Video Effects', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(344, NULL, NULL, 'ActionScript', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(345, NULL, NULL, 'Sound Forge', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(346, NULL, NULL, 'E-on Vue', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(347, NULL, NULL, 'AutoCad', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(348, NULL, NULL, 'Mac', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(349, NULL, NULL, 'YouTube', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(350, NULL, NULL, 'orientándome a resultados que brinden solucio', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(351, NULL, NULL, 'Ilustrador', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(352, NULL, NULL, 'Conductor de Radio en FMCristal', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(353, NULL, NULL, 'AJAX', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(354, NULL, NULL, 'HTML5', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(355, NULL, NULL, 'Cordova', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(356, NULL, NULL, 'Durante los tres años de carrera lleve a cabo', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(357, NULL, NULL, 'éstos me enseñaron a manejarme de manera corr', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(358, NULL, NULL, 'las más interesantes que realicé fueron:  Gui', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(359, NULL, NULL, 'interacción de dos conductores con un tercero', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(360, NULL, NULL, 'uno de cortos de cine', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(361, NULL, NULL, 'uno de música y uno de literatura. Este últim', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(362, NULL, NULL, 'Guiones literarios y técnicos. Experiencia en', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(363, NULL, NULL, 'Manejo de cámara', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(364, NULL, NULL, 'Experiencia en producción de piso y exteriore', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(365, NULL, NULL, 'Realización de documentales y', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(366, NULL, NULL, 'Realización de entrevistas.', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(367, NULL, NULL, 'UX', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(368, NULL, NULL, 'UI', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(369, NULL, NULL, 'Realizador', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(370, NULL, NULL, 'editor', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(371, NULL, NULL, 'camarógrafo  y fotógrafo. Lleva diez años pro', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(372, NULL, NULL, 'documental', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(373, NULL, NULL, 'televisión y videoclip.', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(374, NULL, NULL, 'Camarógrafo', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(375, NULL, NULL, 'Fotógrafo', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(376, NULL, NULL, 'Buen día', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(377, NULL, NULL, 'quisiera agregar a la información del cv que ', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(378, NULL, NULL, 'en la cual aprendimos diferentes métodos de i', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(379, NULL, NULL, 'acabados e impresos y cómo trabajarlos desde ', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(380, NULL, NULL, 'contamos con la posibiliadad de recorrer los ', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(381, NULL, NULL, 'por lo que los conocimientos se vieron enriqu', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(382, NULL, NULL, 'dejo el link de mi porfolio web: http://www.b', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(383, NULL, NULL, 'Carolina Ayelen Valls', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(384, NULL, NULL, 'A la información del CV quisiera agregar que ', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(385, NULL, NULL, 'Corel Photopaint', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(386, NULL, NULL, 'tipo', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(387, NULL, NULL, 'Diseño de Identidad', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(388, NULL, NULL, 'Web y Packaging', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(389, NULL, NULL, 'Strategic Planning', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(390, NULL, NULL, 'Google Analytics', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(391, NULL, NULL, 'Market Analysis', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(392, NULL, NULL, 'New Business Development', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(393, NULL, NULL, 'Contract Negotiation', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(394, NULL, NULL, 'Strategic Partnerships', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(395, NULL, NULL, 'Events Organisation', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(396, NULL, NULL, 'Sustainable Business', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(397, NULL, NULL, 'Corporate Social Responsibility', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(398, NULL, NULL, 'Media Relations', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(399, NULL, NULL, 'International Development', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(400, NULL, NULL, 'Word', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(401, NULL, NULL, 'Excel', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(402, NULL, NULL, 'Customer Service', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(403, NULL, NULL, 'Translation', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(404, NULL, NULL, 'llslslsll', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(405, NULL, NULL, 'Estrategia y dirección creativa', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(406, NULL, NULL, 'dirección de arte', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(407, NULL, NULL, 'desarrollo de conceptos e ideas para ATL', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(408, NULL, NULL, 'BTL y digital', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(409, NULL, NULL, 'traducción francés-español', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(410, NULL, NULL, 'traducción español-francés', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(411, NULL, NULL, 'Proofreading', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(412, NULL, NULL, 'capacidad de síntexis', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(413, NULL, NULL, 'traducción', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(414, NULL, NULL, 'ZendFramework', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(415, NULL, NULL, 'Desarrollo Clarion', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(416, NULL, NULL, 'Outsourcing', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(417, NULL, NULL, 'Software Factory', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(418, NULL, NULL, 'Desarrollo de Sistemas', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(419, NULL, NULL, 'MSSQL', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(420, NULL, NULL, 'Oracle', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(421, NULL, NULL, 'Desarrollo Movil', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(422, NULL, NULL, 'Sistemas Desktop', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(423, NULL, NULL, 'Desarrollo Desktop', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(424, NULL, NULL, 'Facebook Marketing', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(425, NULL, NULL, 'Social Marketing', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(426, NULL, NULL, 'SEM', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(427, NULL, NULL, 'E-commerce', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(428, NULL, NULL, 'High Availability', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(429, NULL, NULL, 'Online Marketing', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(430, NULL, NULL, 'PnL', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(431, NULL, NULL, 'Coaching', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(432, NULL, NULL, 'ventas', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(433, NULL, NULL, 'Interpretación simultánea', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(434, NULL, NULL, 'Traducción técnica', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(435, NULL, NULL, 'Localización de software y sitios web', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(436, NULL, NULL, 'Control de calidad', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(437, NULL, NULL, 'Auditoría de fábrica', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(438, NULL, NULL, 'Sourcing de proveedores', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(439, NULL, NULL, 'Simultaneous Interpreting', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(440, NULL, NULL, 'Technical Translation', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(441, NULL, NULL, 'Software & Website Localization', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(442, NULL, NULL, 'Quality Control', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(443, NULL, NULL, 'Factory Auditing', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(444, NULL, NULL, 'Strategic Sourcing', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(445, NULL, NULL, 'Italian', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(446, NULL, NULL, 'Linguistics', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(447, NULL, NULL, 'Portuguese', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(448, NULL, NULL, 'Simultaneous Interpretation', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(449, NULL, NULL, 'Consecutive Interpretation', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(450, NULL, NULL, 'Trados', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(451, NULL, NULL, 'InCopy', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(452, NULL, NULL, 'Adobe Acrobat', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(453, NULL, NULL, 'PoEdit', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(454, NULL, NULL, 'Lexicography', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(455, NULL, NULL, 'Copy Editing', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(456, NULL, NULL, 'Subtitling', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(457, NULL, NULL, 'experto en wordpress', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(458, NULL, NULL, 'microsoft visual studio', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(459, NULL, NULL, 'desktop applications', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(460, NULL, NULL, 'Brand Design', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(461, NULL, NULL, 'Mobile Design', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(462, NULL, NULL, 'Team Management', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(463, NULL, NULL, 'Multimedia', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(464, NULL, NULL, 'Technical Support', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(465, NULL, NULL, 'Informatica', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(466, NULL, NULL, 'Camera Operating', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(467, NULL, NULL, 'Canon DSLR', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(468, NULL, NULL, 'XDCAM', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(469, NULL, NULL, 'Documentaries', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(470, NULL, NULL, 'Rentals', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(471, NULL, NULL, 'HD Video', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(472, NULL, NULL, 'Mac OS X', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(473, NULL, NULL, 'iMovie', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(474, NULL, NULL, 'Branding', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(475, NULL, NULL, 'Book Design', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(476, NULL, NULL, 'Visual Identity', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(477, NULL, NULL, 'Business Cards', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(478, NULL, NULL, 'Adobe Design Programs', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(479, NULL, NULL, 'Final Cut Studio', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(480, NULL, NULL, 'Final Cut Pro', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(481, NULL, NULL, 'Layout', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(482, NULL, NULL, 'Diseño de packaging', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(483, NULL, NULL, 'Traditional Animation', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(484, NULL, NULL, 'Video Editing', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(485, NULL, NULL, 'Flash Animation', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(486, NULL, NULL, 'Motion Design', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(487, NULL, NULL, '3D graphics', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(488, NULL, NULL, 'Motion Graphic Design', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(489, NULL, NULL, 'Visual Effects', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(490, NULL, NULL, 'Visual Arts', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(491, NULL, NULL, 'Compositing', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(492, NULL, NULL, 'Computer Graphics', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(493, NULL, NULL, 'Computer Animation', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(494, NULL, NULL, '2D graphics', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(495, NULL, NULL, 'Digital TV', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(496, NULL, NULL, 'Digital Compositing', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(497, NULL, NULL, 'Rotoscoping', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(498, NULL, NULL, '3D Modeling', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(499, NULL, NULL, 'Concept Design', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(500, NULL, NULL, 'BBDD', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(501, NULL, NULL, 'illtration', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(502, NULL, NULL, 'cms joomla', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(503, NULL, NULL, 'perfeccionarme', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(504, NULL, NULL, 'Business Intelligence', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(505, NULL, NULL, 'Wicket', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(506, NULL, NULL, 'Project Planning', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(507, NULL, NULL, 'Sencha', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(508, NULL, NULL, 'Database Tools', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(509, NULL, NULL, 'Analytics', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(510, NULL, NULL, 'Business Strategy', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(511, NULL, NULL, 'Impact Investing', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(512, NULL, NULL, 'SITIOS ACORDES A SUS NECESIDADES', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(513, NULL, NULL, 'Flex', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(514, NULL, NULL, 'XML', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(515, NULL, NULL, 'Requirements Analysis', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(516, NULL, NULL, 'Functional Design', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(517, NULL, NULL, 'Functional Specifications', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(518, NULL, NULL, 'Video Games', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(519, NULL, NULL, 'GXT', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(520, NULL, NULL, 'Proactivo', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(521, NULL, NULL, 'Trabajo en grupo', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(522, NULL, NULL, 'PowerPoint', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(523, NULL, NULL, 'Internal Communications', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(524, NULL, NULL, 'Community Management', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(525, NULL, NULL, 'Strategic Communications', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(526, NULL, NULL, 'Adobe Indesign', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(527, NULL, NULL, 'Google Adword', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(528, NULL, NULL, 'Corrector', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(529, NULL, NULL, 'Redactor', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(530, NULL, NULL, 'Periodista', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(531, NULL, NULL, 'Línea editorial', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(532, NULL, NULL, 'Editor y corrector', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(533, NULL, NULL, 'Redactor y corrector', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(534, NULL, NULL, 'Geóloga', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(535, NULL, NULL, 'graduada de la Universidad Nacional de Colomb', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(536, NULL, NULL, 'Diploma de traducción Inglés-Español de la Un', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(537, NULL, NULL, 'Español como lengua nativa', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(538, NULL, NULL, 'Contacto continuo con el Inglés y su cultura', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(539, NULL, NULL, 'actualmente vivo en Australia y soy casada co', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(540, NULL, NULL, 'Español como lengua materna - Contacto contín', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(541, NULL, NULL, 'Geóloga de la Universidad Nacional de Colombi', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(542, NULL, NULL, 'Diplomado de Traduccion de la Universidad Ibe', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(543, NULL, NULL, 'creative suite', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02');
INSERT INTO `adm_competences` (`id_competence`, `adm_competence_fr`, `adm_competence_en`, `adm_competence_es`, `actif`, `var`, `cree_le`, `modifie_le`) VALUES
(544, NULL, NULL, 'Rendering', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(545, NULL, NULL, 'Industrial Design', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(546, NULL, NULL, 'Product Design', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(547, NULL, NULL, 'Interior Design', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(548, NULL, NULL, 'Architectural Design', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(549, NULL, NULL, 'internet', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(550, NULL, NULL, 'Programming Aptitudes', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(551, NULL, NULL, 'Interpreting', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(552, NULL, NULL, 'Language Teaching', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(553, NULL, NULL, 'Catalan', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(554, NULL, NULL, 'Conference Interpreting', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(555, NULL, NULL, 'Creativa', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(556, NULL, NULL, 'Profesionalismo', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(557, NULL, NULL, 'Comprensión de las ideas', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(558, NULL, NULL, 'Perfectos resultados', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(559, NULL, NULL, 'Calligraphy', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(560, NULL, NULL, 'Animacion 3D', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(561, NULL, NULL, 'c', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(562, NULL, NULL, 'iPhone', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(563, NULL, NULL, 'java', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(564, NULL, NULL, 'mobile', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(565, NULL, NULL, 'c++', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(566, NULL, NULL, 'Imagination', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(567, NULL, NULL, 'Programming', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(568, NULL, NULL, 'Books', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(569, NULL, NULL, 'Publications', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(570, NULL, NULL, 'Journals', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(571, NULL, NULL, 'APIs', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(572, NULL, NULL, 'RESTful WebServices', 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:46:02'),
(573, 'Scrum', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2015-03-03 22:40:56'),
(574, 'PHP5', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2015-03-03 22:40:56'),
(575, 'AJAX', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2015-03-03 22:40:56'),
(576, 'CSS', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2015-03-03 22:40:56'),
(577, 'JQuery', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2015-03-03 22:40:56'),
(578, 'HTML', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2015-03-03 22:40:56'),
(579, 'AS3 Flash', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2015-03-03 22:40:56'),
(580, 'JAVA', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2015-03-03 22:40:56'),
(581, 'Doctrine', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2015-03-03 22:40:56'),
(582, 'Développement informatique', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2015-03-03 22:40:56'),
(583, 'HTML 5', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2015-03-03 22:40:56'),
(584, 'Design', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2015-03-03 22:40:56'),
(585, 'Javascript', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2015-03-03 22:40:56'),
(586, 'CSS 3', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2015-03-03 22:40:56'),
(587, 'PHP', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2015-03-03 22:40:56'),
(588, 'Adobe Photoshop', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2015-03-03 22:40:56'),
(589, 'Adobe Illustrator', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2015-03-03 22:40:56');

-- --------------------------------------------------------

--
-- Structure de la table `adm_contrats`
--

CREATE TABLE `adm_contrats` (
  `id_contrat` int(255) NOT NULL,
  `adm_contrat_fr` varchar(45) NOT NULL,
  `adm_contrat_es` varchar(45) NOT NULL,
  `adm_contrat_en` varchar(45) NOT NULL,
  `actif` tinyint(1) NOT NULL,
  `var` varchar(45) DEFAULT NULL,
  `cree_le` datetime NOT NULL,
  `modifie_le` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `adm_contrats`
--

INSERT INTO `adm_contrats` (`id_contrat`, `adm_contrat_fr`, `adm_contrat_es`, `adm_contrat_en`, `actif`, `var`, `cree_le`, `modifie_le`) VALUES
(1, 'Journalier', 'Diario', 'Daily', 1, NULL, '2012-10-11 17:38:45', '2013-01-21 16:05:04'),
(2, 'Forfait', 'Forfait', 'Package', 1, NULL, '2012-10-11 17:38:45', '2013-01-29 20:51:52');

-- --------------------------------------------------------

--
-- Structure de la table `adm_entretiens_statuts`
--

CREATE TABLE `adm_entretiens_statuts` (
  `id_entretien_statut` int(11) NOT NULL,
  `adm_entretien_statut_fr` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `adm_entretien_statut_en` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `adm_entretien_statut_es` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `cree_le` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifie_le` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `adm_entretiens_statuts`
--

INSERT INTO `adm_entretiens_statuts` (`id_entretien_statut`, `adm_entretien_statut_fr`, `adm_entretien_statut_en`, `adm_entretien_statut_es`, `cree_le`, `modifie_le`) VALUES
(1, 'Session en attente', 'version en ', 'version es', '2013-08-12 08:17:38', '2013-08-12 10:13:19'),
(2, 'Session confirmée', 'version en ', 'version es', '2013-08-12 08:17:38', '2013-08-12 10:13:22'),
(3, 'Session terminée', 'Completed', 'Teminado(s)', '2013-08-12 08:17:38', '2013-08-12 10:13:34');

-- --------------------------------------------------------

--
-- Structure de la table `adm_etudes`
--

CREATE TABLE `adm_etudes` (
  `id_etude` int(255) NOT NULL,
  `adm_etude_fr` varchar(45) DEFAULT NULL,
  `adm_etude_es` varchar(45) DEFAULT NULL,
  `adm_etude_en` varchar(45) DEFAULT NULL,
  `actif` tinyint(1) NOT NULL,
  `var` varchar(45) DEFAULT NULL,
  `cree_le` datetime NOT NULL,
  `modifie_le` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `adm_etudes`
--

INSERT INTO `adm_etudes` (`id_etude`, `adm_etude_fr`, `adm_etude_es`, `adm_etude_en`, `actif`, `var`, `cree_le`, `modifie_le`) VALUES
(2, 'Ecole d\\''ingénieur', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 17:55:23'),
(5, 'Licence', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 17:58:10'),
(7, 'bac +  4/5', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-08-29 11:47:18'),
(8, 'Bac +  3/4', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-10-15 14:37:13'),
(9, 'Ecole de commerce', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-02-05 14:51:43'),
(6, 'bac + 3', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-02-05 15:23:47'),
(4, 'bac + 5', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-02-05 15:55:45'),
(3, 'bac + 2', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-02-06 15:35:54'),
(1, 'bac + 4', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-02-06 15:35:54'),
(10, 'BAC', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-02-19 12:22:24'),
(11, 'aj', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-02-19 12:26:32'),
(12, 'bac   5', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-02-19 12:26:58'),
(13, '2', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-03-13 22:37:55'),
(14, 'licenceprofessionelle', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-06-18 21:58:01'),
(15, 'licence pro', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-06-18 22:00:20'),
(17, 'lice prof', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-06-18 22:05:19'),
(19, 'licence prof', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-06-18 22:08:04'),
(20, 'er', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-06-18 22:12:08'),
(21, 'test', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-06-18 22:14:56'),
(22, 'licence de vendeur de chaussu', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-07-02 12:47:46'),
(23, 'av', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-07-02 14:39:46'),
(24, 'ajax', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-07-03 13:02:21'),
(25, 'lan', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-07-23 18:30:48'),
(26, 'ba', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-07-23 22:53:39'),
(27, 'aja', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-07-23 22:53:51'),
(28, 'bacaaa', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-07-23 22:54:03'),
(29, 'langue', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-07-24 20:28:37'),
(30, 'Maitrise', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-07-30 11:43:30'),
(31, 'IEP', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-07-30 11:43:34'),
(32, 'Licence profes', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-07-30 12:34:34'),
(33, 'azoo', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-08-13 14:31:51'),
(34, 'baca', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-08-14 16:24:24'),
(35, 'bacer', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-08-14 16:24:52'),
(36, 'ety', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-08-14 16:30:33'),
(37, 'BEP', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-08-19 09:57:16'),
(38, 'zzz', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-08-27 13:39:57');

-- --------------------------------------------------------

--
-- Structure de la table `adm_experiences`
--

CREATE TABLE `adm_experiences` (
  `id_experience` int(255) NOT NULL,
  `adm_experience_fr` varchar(45) NOT NULL,
  `adm_experience_es` varchar(45) NOT NULL,
  `adm_experience_en` varchar(45) NOT NULL,
  `actif` tinyint(1) NOT NULL,
  `var` varchar(45) DEFAULT NULL,
  `cree_le` datetime NOT NULL,
  `modifie_le` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `adm_experiences`
--

INSERT INTO `adm_experiences` (`id_experience`, `adm_experience_fr`, `adm_experience_es`, `adm_experience_en`, `actif`, `var`, `cree_le`, `modifie_le`) VALUES
(-1, 'Moins de 1 an ', 'Menos de 1 año ', 'Less than 1 year', 1, NULL, '2012-03-06 10:51:44', '2013-02-13 15:04:38'),
(1, 'Plus de 1 an', 'Mas de 1 año', 'More than 1 year', 1, NULL, '2012-03-06 10:52:03', '2013-02-13 15:04:43'),
(3, 'Plus de 3 ans', 'Mas de 3 años', 'More than 3 years', 1, NULL, '2012-03-06 10:53:01', '2013-02-13 15:04:49'),
(5, 'Plus de 5 ans', 'Mas de 5 años', 'More than 5 years', 1, NULL, '2012-03-06 10:53:32', '2013-02-13 15:04:56'),
(10, 'Plus de 10 ans', 'Mas de 10 años', 'More than 10 years', 1, NULL, '2012-03-06 10:54:00', '2013-02-13 15:05:04');

-- --------------------------------------------------------

--
-- Structure de la table `adm_langues`
--

CREATE TABLE `adm_langues` (
  `id_langue` int(255) NOT NULL,
  `adm_langue_fr` varchar(45) DEFAULT NULL,
  `adm_langue_es` varchar(45) DEFAULT NULL,
  `adm_langue_en` varchar(45) DEFAULT NULL,
  `actif` tinyint(1) NOT NULL,
  `var` varchar(45) DEFAULT NULL,
  `cree_le` datetime NOT NULL,
  `modifie_le` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `adm_langues`
--

INSERT INTO `adm_langues` (`id_langue`, `adm_langue_fr`, `adm_langue_es`, `adm_langue_en`, `actif`, `var`, `cree_le`, `modifie_le`) VALUES
(1, 'Anglais', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(2, 'Arabe', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(3, 'Chinois', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(4, 'Espagnol', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(5, 'Français', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(6, 'Russe', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(7, 'Allemand', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(8, 'Catalan', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(9, 'Coréen', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(10, 'Croate', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(11, 'Danois', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(12, 'Finnois', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(13, NULL, 'Inglés Fluido', NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(14, 'Hongrois', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(15, 'Italien', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(16, 'Japonais', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(17, NULL, 'italiano', NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(18, 'Néerlandais', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(19, NULL, 'Italiano Básico', NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(20, 'Portugais', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(21, NULL, 'Italiano Intermedio', NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(22, NULL, 'Portuguese', NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(23, NULL, 'Español nativo', NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(24, 'Suédois', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(25, NULL, 'Armenio', NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(26, NULL, 'Spanish', NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(27, NULL, 'Portugués', NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(28, 'Baure', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(29, 'Bésiro', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(30, 'Bichelamar', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(31, 'Biélorusse', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(32, 'Birman', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(33, 'Bulgare', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(34, 'Canichana', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(35, 'Cavineña', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(36, 'Cayubaba', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(37, 'Chácobo', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(38, 'Chichewa', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(39, 'Chimane', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(40, 'Cinghalais', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(41, 'Créole de Guinée-Bissau', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(42, 'Créole haïtien', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(43, 'Créole seychellois', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(44, 'Divehi', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(45, 'Dzongkha', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(46, 'Ese Ejja', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(47, 'Estonien', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(48, 'Fidjien', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(49, 'Filipino', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(50, 'Géorgien', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(51, 'Gilbertin', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(52, 'Guarasu''we', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(53, 'Guarayu', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(54, 'Hébreu', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(55, 'Hindoustani', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(56, 'Hindi', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(57, 'Hiri Motu', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(58, 'Iban', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(59, 'Indonésien', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(60, 'Irlandais', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(61, 'Islandais', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(62, 'Itonama', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(63, 'Kallawaya', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(64, 'Kazakh', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(65, 'Khmer', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(66, 'Kichwa', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(67, 'Kirghiz', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(68, 'Kirundi', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(69, 'Lao', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(70, 'Langue des signes néo-zélandaise', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(71, 'Latin', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(72, 'Leko', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(73, 'Letton', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(74, 'Lituanien', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(75, 'Luxembourgveois', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(76, 'Macédonien', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(77, 'Machineri', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(78, 'Malgache', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(79, 'Maltais', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(80, 'M?ori', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(81, 'M?ori des Îles Cook', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(82, 'Maropa', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(83, 'Marshallais', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(84, 'Mojeño-Trinitario', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(85, 'Mojeño-Ignaciano', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(86, 'Monténégrin', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(87, 'Moré', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(88, 'Mosetén', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(89, 'Movima', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(90, 'Nauruan', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(91, 'Népalais', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(92, 'Norvégien', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(93, 'Ouzbek', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(94, 'Pacahuara', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(95, 'Pachto', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(96, 'Paluan', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(97, 'Polonais', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(98, 'Puquina', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(99, 'Sango', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(100, 'Shikomor', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(101, 'Shona', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(102, 'Shuar', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(103, 'Sindebele', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(104, 'Sirionó', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(105, 'Somali', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(106, 'Tacana', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(107, 'Tadjik', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(108, 'Tamazight', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(109, 'Tapiete', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(110, 'Tchèque', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:03'),
(111, 'Tétoum', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(112, 'Tigrinya', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(113, 'Thaï', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(114, 'Tok Pisin', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(115, 'Tonguien', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(116, 'Toromona', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(117, 'Turkmène', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(118, 'Tuvaluan', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(119, 'Ukrainien', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(120, 'Uru-Chipaya', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(121, 'Vietnamien', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(122, 'Yaminahua', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(123, 'Yuki', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(124, 'Yaracaré', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(125, 'Anguarv', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(126, 'Occitan-Aranais', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(127, 'Basque', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(128, 'Cantonais', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(129, 'Carolinien', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(130, 'Chamorro', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(131, 'Galicien', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(132, 'Gallois', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(133, 'Hatohabei', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(134, 'Hawaïen', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(135, 'Inuktitut', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(136, 'Ladin', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(137, 'Limbourgeois', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(138, 'Mannois', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(139, 'Mirandais', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(140, 'Ndébélé', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(141, 'Ouïghour', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(142, 'Romanche', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(143, 'Ruthène', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(144, 'Sarde', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(145, 'Sorabe', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(146, 'Sotho du Nord', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(147, 'Sotho du Sud', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(148, 'Swati', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(149, 'Tibétain', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(150, 'Tsonga', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(151, 'Tswana', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(152, 'Venda', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(153, 'Xhosa', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(154, 'Zoulou', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(155, NULL, 'Ingles Nivel Básico', NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(156, NULL, 'English', NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(157, NULL, 'French', NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(158, NULL, 'Ingles intermedio', NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(159, NULL, 'Espanol', NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(160, NULL, 'frances', NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(161, NULL, 'Espagnol', NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(162, NULL, 'Français', NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(163, NULL, 'ingles', NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(164, NULL, 'castellano', NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(165, NULL, 'Inglés - Avanzado', NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(166, NULL, 'Catalá', NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(167, NULL, 'Hebreo', NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(168, NULL, 'portugués basico', NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(169, NULL, 'Portugués  Nivel Basico', NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(170, NULL, 'Inglés oral y escrito', NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(171, NULL, 'Anglais', NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(172, NULL, 'francés nativo', NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(173, NULL, 'Portugués Nivel Intermedio', NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(174, NULL, 'Chinese', NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(175, NULL, 'Inglés tecnico', NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(176, NULL, 'Neutro', NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(177, NULL, 'Javascript', NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(178, NULL, 'ingl', NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(179, NULL, 'francés y portugués', NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04'),
(180, NULL, 'Catalán', NULL, 1, NULL, '0000-00-00 00:00:00', '2013-09-09 18:50:04');

-- --------------------------------------------------------

--
-- Structure de la table `adm_missions_statuts`
--

CREATE TABLE `adm_missions_statuts` (
  `id_mission_statut` int(11) NOT NULL,
  `adm_mission_statut_fr` varchar(255) NOT NULL,
  `adm_mission_statut_en` varchar(255) NOT NULL,
  `adm_mission_statut_es` varchar(255) NOT NULL,
  `actif` int(11) NOT NULL,
  `cree_le` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifie_le` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `adm_missions_statuts`
--

INSERT INTO `adm_missions_statuts` (`id_mission_statut`, `adm_mission_statut_fr`, `adm_mission_statut_en`, `adm_mission_statut_es`, `actif`, `cree_le`, `modifie_le`) VALUES
(1, 'En attente validation', 'Waiting for validation', 'Esperando la validación', 1, '2012-03-08 15:47:59', '2013-01-29 20:50:15'),
(2, 'En cours', 'Pending', 'En curso', 1, '2012-03-08 15:47:59', '2013-01-29 20:50:03'),
(3, 'Suspendu(s)', 'Suspended', 'Suspendido(s)', 1, '2012-03-08 15:47:59', '2013-01-29 20:49:58'),
(4, 'Terminé(s)', 'Completed', 'Teminado(s)', 1, '2012-03-08 15:47:59', '2013-01-29 20:49:52');

-- --------------------------------------------------------

--
-- Structure de la table `adm_monnaies`
--

CREATE TABLE `adm_monnaies` (
  `id_monnaie` int(255) NOT NULL,
  `adm_monnaie` varchar(45) NOT NULL,
  `actif` tinyint(1) NOT NULL,
  `var` varchar(45) DEFAULT NULL,
  `cree_le` datetime NOT NULL,
  `modifie_le` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `adm_monnaies`
--

INSERT INTO `adm_monnaies` (`id_monnaie`, `adm_monnaie`, `actif`, `var`, `cree_le`, `modifie_le`) VALUES
(1, '€', 1, NULL, '2012-03-05 18:32:51', '2012-03-05 18:33:07'),
(2, '$', 1, NULL, '2012-03-04 18:33:23', '2012-03-04 18:33:29'),
(7, '¥', 1, NULL, '2012-10-19 00:00:00', '2012-10-19 00:00:00'),
(6, '£', 1, NULL, '2012-10-19 00:00:00', '2012-10-19 13:12:06');

-- --------------------------------------------------------

--
-- Structure de la table `adm_niveaux`
--

CREATE TABLE `adm_niveaux` (
  `id_niveau` int(255) NOT NULL,
  `adm_niveau_fr` varchar(45) NOT NULL,
  `adm_niveau_en` varchar(45) NOT NULL,
  `adm_niveau_es` varchar(45) NOT NULL,
  `actif` tinyint(1) NOT NULL,
  `var` varchar(45) DEFAULT NULL,
  `cree_le` datetime NOT NULL,
  `modifie_le` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `adm_niveaux`
--

INSERT INTO `adm_niveaux` (`id_niveau`, `adm_niveau_fr`, `adm_niveau_en`, `adm_niveau_es`, `actif`, `var`, `cree_le`, `modifie_le`) VALUES
(1, 'Débutant(e)', 'Beginner', 'Debutante', 1, NULL, '2012-12-17 14:38:41', '2013-01-29 20:53:05'),
(2, 'Intermédiaire', 'Intermediate', 'Intermediario', 1, NULL, '2012-12-17 14:38:41', '2013-01-29 20:53:52'),
(3, 'Confirmé(e)', 'Confirm', 'Confirmado/a', 1, NULL, '2012-12-17 14:38:41', '2013-01-29 20:53:10'),
(4, 'Avancé(e)', 'Advanced', 'Avanzado/a', 1, NULL, '2012-12-17 14:38:41', '2013-01-29 20:53:57'),
(5, 'Expert(e)', 'Expert', 'Experto/a', 1, NULL, '2013-07-29 09:48:44', '2013-07-29 09:48:47');

-- --------------------------------------------------------

--
-- Structure de la table `adm_postes`
--

CREATE TABLE `adm_postes` (
  `id_poste` int(255) NOT NULL,
  `adm_poste_fr` varchar(45) DEFAULT NULL,
  `adm_poste_en` varchar(45) DEFAULT NULL,
  `adm_poste_es` varchar(45) DEFAULT NULL,
  `actif` tinyint(1) NOT NULL,
  `var` varchar(45) DEFAULT NULL,
  `cree_le` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifie_le` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `adm_postes`
--

INSERT INTO `adm_postes` (`id_poste`, `adm_poste_fr`, `adm_poste_en`, `adm_poste_es`, `actif`, `var`, `cree_le`, `modifie_le`) VALUES
(1, 'Acheteur', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(2, 'Acheteur d''espaces pub', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(3, 'Acheteur Informatique Et Télécom', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(4, 'Actuaire', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(5, 'Adjoint administratif', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(6, 'Adjoint administratif d''administration centra', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(7, 'Adjoint administratif territorial', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(8, 'Adjoint technique de recherche et de formatio', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(9, 'Adjoint territorial d''animation', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(10, 'Administrateur base de données', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(11, 'Administrateur De Bases De Données', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(12, 'Administrateur de biens', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(13, 'Administrateur De Réseau', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(14, 'Administrateur judiciaire', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(15, 'Affréteur', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(16, 'Agent administratif et agent des services tec', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(17, 'Agent d''entretien d''ascenseurs', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(18, 'Agent d''exploitation des équipements audiovis', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(19, 'Agent de maintenance en mécanique', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(20, 'Agent de maîtrise', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(21, 'Agent de police municipale', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(22, 'Agent de réservation', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(23, 'Agent des services techniques d''administratio', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(24, 'Agent des services techniques de préfecture', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(25, 'Agent des systèmes d''information et de commun', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(26, 'Agent immobilier', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(27, 'Agent spécialisé de police technique et scien', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(28, 'Agent technique de recherche et de formation', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(29, 'Aide comptable', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(30, 'Aide de laboratoire', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(31, 'Aide médico-psychologique', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(32, 'Aide soignant', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(33, 'Aide technique de laboratoire', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(34, 'Ambulancier', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(35, 'Analyste D''Exploitation', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(36, 'Analyste financier', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(37, 'Analyste programmeur', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(38, 'Animateur', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(39, 'Animateur de club de vacances', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(40, 'Animateur de formation', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(41, 'animateur environnement', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(42, 'Animateur socioculturel', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(43, 'Antiquaire', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(44, 'Archéologue', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(45, 'Architecte', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(46, 'Architecte d''intérieur', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(47, 'Architecte De Bases De Données', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(48, 'Architecte De Réseau', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(49, 'Architecte De Système D''Information', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(50, 'Architecte Matériel', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(51, 'Archiviste', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(52, 'Artiste-peintre', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(53, 'Assistant de conservation', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(54, 'Assistant de justice', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(55, 'Assistant de ressources humaines', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(56, 'Assistant de service social', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(57, 'Assistant des bibliothèques', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(58, 'Assistant ingénieur', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(59, 'Assistant médico-technique', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(60, 'Assistant socio-éducatif', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(61, 'Assistant son', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(62, 'Assistant vétérinaire', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(63, 'Assistante de gestion PMI/PME', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(64, 'Assistante maternelle', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(65, 'Assistante sociale', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(66, 'Astronome', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(67, 'Attaché d''administration et d''intendance', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(68, 'Attaché d''administration hospitalière', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(69, 'Attaché d''administration centrale', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(70, 'Attaché d''administration scolaire et universi', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(71, 'Attaché de conservateur de patrimoine', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(72, 'Attaché de police', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(73, 'Attaché de préfecture', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(74, 'Attaché de presse', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(75, 'Auditeur Informatique', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(76, 'Auteur-scénariste multimédia', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(77, 'Auxiliaire de puériculture', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(78, 'Auxiliaire de vie sociale', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(79, 'Auxilliaire de vie', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(80, 'Avocat', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(81, 'Barman', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(82, 'Bibliothécaire', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(83, 'Bibliothécaire adjoint spécialisé', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(84, 'Bijoutier joaillier', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(85, 'Billettiste', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(86, 'Bio-informaticien', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(87, 'Biologiste, Vétérinaire, Pharmacien', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(88, 'Bobinier de la construction électrique', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(89, 'Boucher', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(90, 'Boulanger', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(91, 'Brasseur malteur', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(92, 'Bronzier', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(93, 'Bûcheron', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(94, 'Cadreur', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(95, 'Capitaine de Sapeur-Pompier', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(96, 'Carreleur', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(97, 'Carrossier réparateur', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(98, 'Caviste', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(99, 'Charcutier-traiteur', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(100, 'Chargé de clientèle', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(101, 'Chargé De Référencement', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(102, 'Chargé de relations publiques', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(103, 'Charpentier', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(104, 'Chaudronnier', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(105, 'Chef d''atelier des industries graphiques', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(106, 'Chef de chantier', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(107, 'Chef de comptoir', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(108, 'Chef de fabrication', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(109, 'Chef de produits voyages', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(110, 'Chef De Projet - Project Manager', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(111, 'Chef de projet informatique', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(112, 'Chef de projet multimedia', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(113, 'Chef de publicité', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(114, 'Chef de rayon', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(115, 'Chef de service de Police municipale', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(116, 'Chef opérateur', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(117, 'Chercheur', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(118, 'Chercheur En Informatique', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(119, 'Chirurgien-dentiste', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(120, 'Chocolatier confiseur', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(121, 'Clerc de notaire', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(122, 'Coiffeur', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(123, 'Comédien', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(124, 'Commis de cuisine', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(125, 'Commissaire de police', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(126, 'Commissaire priseur', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(127, 'comportementaliste', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(128, 'Comptable', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(129, 'Concepteur De Jeux Électroniques', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(130, 'Concepteur rédacteur', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(131, 'Concierge d''hôtel', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(132, 'Conducteur', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(133, 'Conducteur d''appareils', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(134, 'Conducteur d''autobus', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(135, 'Conducteur d''automobile', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(136, 'Conducteur d''engins en BTP', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(137, 'Conducteur de machine à imprimer d''exploitati', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(138, 'Conducteur de machine à imprimer simple', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(139, 'Conducteur de machines', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(140, 'Conducteur de machines agro', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(141, 'Conducteur de station d''épuration', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(142, 'Conducteur de taxi', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(143, 'conducteur de train', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(144, 'Conducteur de travaux', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(145, 'Conducteur routier', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(146, 'Conseil En Assistance À Maitrise D''Ouvrage', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(147, 'Conseiller d''insertion et de probation', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(148, 'Conseiller d''orientation', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(149, 'Conseiller d''orientation-psychologue', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(150, 'Conseiller en développement touristique', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(151, 'Conseiller en économie sociale et familiale', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(152, 'Conseiller socio-éducatif', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(153, 'Conseiller territorial des activités physique', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(154, 'Conseillers principaux d''éducation', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(155, 'Conservateur de bibliothèque', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(156, 'Conservateur du patrimoine', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(157, 'Consultant Communication & Réseaux', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(158, 'Consultant E-Business', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(159, 'Consultant En Conduite Du Changement', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(160, 'Consultant En E-Learning', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(161, 'Consultant En Gestion De La Relation Client', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(162, 'Consultant En Organisation Des Systèmes D''Inf', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(163, 'Consultant En Technologies', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(164, 'Consultant Erp', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(165, 'Consultant Fonctionnel', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(166, 'Consultant Informatique', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(167, 'Contrôleur aérien', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(168, 'Contrôleur de gestion', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(169, 'Contrôleur de travaux', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(170, 'Contrôleur des services techniques du matérie', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(171, 'Contrôleur des systèmes d''information et de c', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(172, 'Contrôleur du travail', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(173, 'Contrôleur en électricité et électronique', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(174, 'Convoyeur de fonds', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(175, 'Coordinatrice de crèches', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(176, 'Correcteur', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(177, 'Costumier-habilleur', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(178, 'Courtier d''assurances', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(179, 'Couvreur', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(180, 'Créateur de parfum', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(181, 'Cuisinier', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(182, 'Cyberdocumentaliste', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(183, 'Danseur', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(184, 'Décorateur-scénographe', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(185, 'Délégué médical', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(186, 'Déménageur', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(187, 'Démographe', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(188, 'Dépanneur tv électroménager', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(189, 'Designer automobile', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(190, 'Dessinateur de presse', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(191, 'Dessinateur industriel', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(192, 'Détective privé', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(193, 'Développeur', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(194, 'Diététicien', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(195, 'Directeur artistique', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(196, 'Directeur Commercial', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(197, 'Directeur d''établissement social et médico-so', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(198, 'Directeur d''hôpital', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(199, 'Directeur d''établissement d''enseignement arti', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(200, 'Directeur d''établissement sanitaire et social', NULL, NULL, 1, NULL, '0000-00-00 00:00:00', '2012-07-11 15:09:50'),
(547, 'vendeur de chaussure', NULL, NULL, 1, NULL, '2013-06-18 21:01:03', '2013-06-18 23:01:03'),
(548, 'vendeur', NULL, NULL, 1, NULL, '2013-06-18 21:01:22', '2013-06-18 23:01:22'),
(600, 'integrateur', NULL, NULL, 1, NULL, '2013-07-16 10:47:42', '2013-07-16 12:47:42'),
(601, 'Inventeur', NULL, NULL, 1, NULL, '2013-07-16 10:47:46', '2013-07-16 12:47:46'),
(602, 'traduction', NULL, NULL, 1, NULL, '2013-07-16 10:48:52', '2013-07-16 12:48:52'),
(603, 'vendeurde', NULL, NULL, 1, NULL, '2013-07-16 10:52:27', '2013-07-16 12:52:27'),
(604, 'finalisationtest', NULL, NULL, 1, NULL, '2013-07-16 10:53:27', '2013-07-16 12:53:27'),
(605, 'finalisationtesfzf', NULL, NULL, 1, NULL, '2013-07-16 10:53:54', '2013-07-16 12:53:54'),
(606, 'finalisationtesfzffz', NULL, NULL, 1, NULL, '2013-07-16 10:53:58', '2013-07-16 12:53:58'),
(607, 'finalisationtesfzffzff', NULL, NULL, 1, NULL, '2013-07-16 10:54:00', '2013-07-16 12:54:00'),
(608, 'Ingenieur reseau', NULL, NULL, 1, NULL, '2013-07-23 19:48:29', '2013-07-23 21:48:29'),
(609, 're', NULL, NULL, 1, NULL, '2013-07-23 20:02:21', '2013-07-23 22:02:21'),
(610, 'actuair', NULL, NULL, 1, NULL, '2013-07-23 20:02:37', '2013-07-23 22:02:37'),
(611, 'de', NULL, NULL, 1, NULL, '2013-07-23 20:27:51', '2013-07-23 22:27:51'),
(612, 'testeur', NULL, NULL, 1, NULL, '2013-07-26 10:50:48', '2013-07-26 12:50:48'),
(613, 'fina', NULL, NULL, 1, NULL, '2013-07-26 10:51:31', '2013-07-26 12:51:31'),
(614, 'Chargé de contentieux', NULL, NULL, 1, NULL, '2013-07-30 09:42:58', '2013-07-30 11:42:58'),
(615, 'ajax', NULL, NULL, 1, NULL, '2013-08-01 14:40:57', '2013-08-01 16:40:57'),
(616, 'chef de projet web', NULL, NULL, 1, NULL, '2013-08-12 13:00:06', '2013-08-12 15:00:06'),
(617, 'chef de projet 3D', NULL, NULL, 1, NULL, '2013-08-12 13:26:16', '2013-08-12 15:26:16'),
(618, 'développeur java', NULL, NULL, 1, NULL, '2013-08-12 13:28:05', '2013-08-12 15:28:05'),
(619, 'dév', NULL, NULL, 1, NULL, '2013-08-13 09:21:59', '2013-08-13 11:21:59'),
(620, 'déc', NULL, NULL, 1, NULL, '2013-08-13 09:22:38', '2013-08-13 11:22:38'),
(621, 'déca', NULL, NULL, 1, NULL, '2013-08-13 09:35:09', '2013-08-13 11:35:09'),
(622, '"zt', NULL, NULL, 1, NULL, '2013-08-13 12:36:19', '2013-08-13 14:36:19'),
(623, 'responsable magasin', NULL, NULL, 1, NULL, '2013-08-19 07:25:09', '2013-08-19 09:25:09'),
(624, 'Developpeur web', NULL, NULL, 1, NULL, '2013-08-20 15:40:33', '2013-08-20 17:40:33'),
(625, 'Web/Mobile Designer', NULL, NULL, 1, NULL, '2013-08-21 15:42:42', '2013-08-21 17:42:42'),
(626, 'deve', NULL, NULL, 1, NULL, '2013-08-22 15:36:33', '2013-08-22 17:36:33'),
(627, 'graphiste', NULL, NULL, 1, NULL, '2013-08-23 08:59:21', '2013-08-23 10:59:21'),
(628, 'Installateur en télécommunications', NULL, NULL, 1, NULL, '2013-09-04 13:01:51', '2013-09-04 15:01:51'),
(629, 'Personnel technique de l''administration pénit', NULL, NULL, 1, NULL, '2013-09-04 13:01:51', '2013-09-04 15:01:51'),
(630, 'Rédacteur en chef', NULL, NULL, 1, NULL, '2013-09-04 13:01:51', '2013-09-04 15:01:51'),
(631, 'Professeur d''arts plastiques', NULL, NULL, 1, NULL, '2013-09-04 13:09:29', '2013-09-04 15:09:29'),
(632, 'Educateur sportif', NULL, NULL, 1, NULL, '2013-09-04 13:09:29', '2013-09-04 15:09:29'),
(633, 'Ingénieur du génie sanitaire', NULL, NULL, 1, NULL, '2013-09-04 13:09:29', '2013-09-04 15:09:29'),
(634, 'Vétérinaire', NULL, NULL, 1, NULL, '2013-09-04 13:09:29', '2013-09-04 15:09:29'),
(635, 'Ingénieur de production', NULL, NULL, 1, NULL, '2013-09-04 13:09:29', '2013-09-04 15:09:29'),
(636, 'Responsable d''agence bancaire', NULL, NULL, 1, NULL, '2013-09-04 13:09:29', '2013-09-04 15:09:29'),
(637, 'Juriste Informatique', NULL, NULL, 1, NULL, '2013-09-04 13:09:29', '2013-09-04 15:09:29'),
(638, 'Musicien', NULL, NULL, 1, NULL, '2013-09-04 13:09:29', '2013-09-04 15:09:29'),
(639, 'Relieur-doreur', NULL, NULL, 1, NULL, '2013-09-04 13:09:29', '2013-09-04 15:09:29'),
(640, 'Ingénieur des services techniques du matériel', NULL, NULL, 1, NULL, '2013-09-04 13:09:29', '2013-09-04 15:09:29'),
(641, 'Responsable Des Études', NULL, NULL, 1, NULL, '2013-09-04 13:09:29', '2013-09-04 15:09:29'),
(642, 'Secrétaire de Mairie', NULL, NULL, 1, NULL, '2013-09-04 13:09:29', '2013-09-04 15:09:29'),
(643, 'Productrice', NULL, NULL, 1, NULL, '2013-09-04 13:09:29', '2013-09-04 15:09:29'),
(644, 'Chef de projet', NULL, NULL, 1, NULL, '2013-09-04 13:09:29', '2013-09-04 15:09:29'),
(645, 'Chargée de communication', NULL, NULL, 1, NULL, '2013-09-04 13:09:29', '2013-09-04 15:09:29'),
(646, 'Graphiste multimédia', NULL, NULL, 1, NULL, '2013-09-04 13:09:30', '2013-09-04 15:09:30'),
(647, 'Plombier', NULL, NULL, 1, NULL, '2013-09-04 13:09:30', '2013-09-04 15:09:30'),
(648, 'chef de projet événementiel', NULL, NULL, 1, NULL, '2013-09-06 09:02:55', '2013-09-06 11:02:55'),
(649, 'Roberto', NULL, NULL, 1, NULL, '2013-09-06 13:50:38', '2013-09-06 15:50:38'),
(650, 'Webdesigner', NULL, NULL, 1, NULL, '2013-09-06 13:57:02', '2013-09-06 15:57:02'),
(651, 'Les greffiers', NULL, NULL, 1, NULL, '2013-09-06 13:57:02', '2013-09-06 15:57:02'),
(652, 'Orthoptiste', NULL, NULL, 1, NULL, '2013-09-06 13:57:02', '2013-09-06 15:57:02'),
(653, 'Pédicure podologue', NULL, NULL, 1, NULL, '2013-09-06 13:57:02', '2013-09-06 15:57:02'),
(654, 'Pigiste', NULL, NULL, 1, NULL, '2013-09-06 13:57:02', '2013-09-06 15:57:02'),
(655, 'Ingénieur Support Technique', NULL, NULL, 1, NULL, '2013-09-06 13:57:02', '2013-09-06 15:57:02'),
(656, 'Métiers de la production', NULL, NULL, 1, NULL, '2013-09-06 13:57:02', '2013-09-06 15:57:02'),
(657, 'Stenotypiste', NULL, NULL, 1, NULL, '2013-09-06 13:57:02', '2013-09-06 15:57:02'),
(658, 'Technicien forestier', NULL, NULL, 1, NULL, '2013-09-06 13:57:03', '2013-09-06 15:57:03'),
(659, 'Médecin de l''éducation nationale', NULL, NULL, 1, NULL, '2013-09-06 13:57:03', '2013-09-06 15:57:03'),
(660, 'Ouvrier d''entretien et d''accueil (OEA)', NULL, NULL, 1, NULL, '2013-09-06 13:57:03', '2013-09-06 15:57:03'),
(661, 'Ingénieur du son', NULL, NULL, 1, NULL, '2013-09-06 13:57:03', '2013-09-06 15:57:03'),
(662, 'Opérateur sur machine de production électriqu', NULL, NULL, 1, NULL, '2013-09-06 13:57:03', '2013-09-06 15:57:03'),
(663, 'Ebéniste', NULL, NULL, 1, NULL, '2013-09-06 13:57:03', '2013-09-06 15:57:03'),
(664, 'Ergothérapeute', NULL, NULL, 1, NULL, '2013-09-06 13:57:03', '2013-09-06 15:57:03'),
(665, 'Maître chien', NULL, NULL, 1, NULL, '2013-09-06 13:57:03', '2013-09-06 15:57:03'),
(666, 'Pharmacien', NULL, NULL, 1, NULL, '2013-09-06 13:57:03', '2013-09-06 15:57:03'),
(667, 'Psychomotricien', NULL, NULL, 1, NULL, '2013-09-06 13:57:03', '2013-09-06 15:57:03'),
(668, 'Médecin inspecteur de santé publique', NULL, NULL, 1, NULL, '2013-09-06 13:57:03', '2013-09-06 15:57:03'),
(669, 'Préparateur en pharmacie', NULL, NULL, 1, NULL, '2013-09-06 13:57:03', '2013-09-06 15:57:03'),
(670, 'Technicien en mécanique', NULL, NULL, 1, NULL, '2013-09-06 13:57:03', '2013-09-06 15:57:03'),
(671, 'Ouvrier professionnel (OP)', NULL, NULL, 1, NULL, '2013-09-06 13:57:03', '2013-09-06 15:57:03'),
(672, 'Poissonnier', NULL, NULL, 1, NULL, '2013-09-06 13:57:03', '2013-09-06 15:57:03'),
(673, 'Ingénieur d''études', NULL, NULL, 1, NULL, '2013-09-06 13:57:03', '2013-09-06 15:57:03'),
(674, 'Préparateur en pharmacie avec Pub', NULL, NULL, 1, NULL, '2013-09-06 13:57:03', '2013-09-06 15:57:03'),
(675, 'Infirmier anesthésiste diplômé d''Etat', NULL, NULL, 1, NULL, '2013-09-06 13:57:03', '2013-09-06 15:57:03'),
(676, 'Professeur de lycée professionnel', NULL, NULL, 1, NULL, '2013-09-06 13:57:03', '2013-09-06 15:57:03'),
(677, 'Ingénieur de recherche produit', NULL, NULL, 1, NULL, '2013-09-06 13:57:03', '2013-09-06 15:57:03'),
(678, 'Pharmacien inspecteur de santé publique', NULL, NULL, 1, NULL, '2013-09-06 13:57:03', '2013-09-06 15:57:03'),
(679, 'Géographe', NULL, NULL, 1, NULL, '2013-09-06 13:57:03', '2013-09-06 15:57:03'),
(680, 'ingénieur du génie rural des eaux et forêts', NULL, NULL, 1, NULL, '2013-09-06 13:57:04', '2013-09-06 15:57:04'),
(681, 'Responsable De Service Informatique', NULL, NULL, 1, NULL, '2013-09-06 13:57:04', '2013-09-06 15:57:04'),
(682, 'Standardiste', NULL, NULL, 1, NULL, '2013-09-06 13:57:04', '2013-09-06 15:57:04'),
(683, 'Directeur De Projet', NULL, NULL, 1, NULL, '2013-09-06 13:57:04', '2013-09-06 15:57:04'),
(684, 'Hôtesse de l''air', NULL, NULL, 1, NULL, '2013-09-06 13:57:04', '2013-09-06 15:57:04'),
(685, 'Guichetetier', NULL, NULL, 1, NULL, '2013-09-06 13:57:04', '2013-09-06 15:57:04'),
(686, 'Lieutenant de sapeurs-pompiers', NULL, NULL, 1, NULL, '2013-09-06 13:57:04', '2013-09-06 15:57:04'),
(687, 'Sommelier', NULL, NULL, 1, NULL, '2013-09-06 13:57:04', '2013-09-06 15:57:04'),
(688, 'Responsable D''Un Système D''Information Métier', NULL, NULL, 1, NULL, '2013-09-06 13:57:04', '2013-09-06 15:57:04'),
(689, 'Expert En Sécurité Informatique', NULL, NULL, 1, NULL, '2013-09-06 13:57:04', '2013-09-06 15:57:04'),
(690, 'Médecin', NULL, NULL, 1, NULL, '2013-09-06 13:57:04', '2013-09-06 15:57:04'),
(691, 'Educateur de jeunes enfants', NULL, NULL, 1, NULL, '2013-09-06 13:57:04', '2013-09-06 15:57:04'),
(692, 'Educateur territorial des activités physiques', NULL, NULL, 1, NULL, '2013-09-06 13:57:06', '2013-09-06 15:57:06'),
(693, 'Entraîneur sportif', NULL, NULL, 1, NULL, '2013-09-06 13:57:06', '2013-09-06 15:57:06'),
(694, 'Major de sapeur-pompier', NULL, NULL, 1, NULL, '2013-09-06 13:57:06', '2013-09-06 15:57:06'),
(695, 'Ingénieur Qualités Méthodes', NULL, NULL, 1, NULL, '2013-09-06 13:57:06', '2013-09-06 15:57:06'),
(696, 'Electricien du bâtiment', NULL, NULL, 1, NULL, '2013-09-06 13:57:06', '2013-09-06 15:57:06'),
(697, 'Ingénieur Validation', NULL, NULL, 1, NULL, '2013-09-06 13:57:06', '2013-09-06 15:57:06'),
(698, 'Electricien électronicien auto', NULL, NULL, 1, NULL, '2013-09-06 13:57:06', '2013-09-06 15:57:06'),
(699, 'Pépinieriste', NULL, NULL, 1, NULL, '2013-09-06 13:57:06', '2013-09-06 15:57:06'),
(700, 'Tourneur-fraiseur', NULL, NULL, 1, NULL, '2013-09-06 13:57:06', '2013-09-06 15:57:06'),
(701, 'Juge de l''exécution', NULL, NULL, 1, NULL, '2013-09-06 13:57:06', '2013-09-06 15:57:06'),
(702, 'Télévendeur', NULL, NULL, 1, NULL, '2013-09-06 13:57:06', '2013-09-06 15:57:06'),
(703, 'VRP', NULL, NULL, 1, NULL, '2013-09-06 13:57:06', '2013-09-06 15:57:06'),
(704, 'Guide accompagnateur', NULL, NULL, 1, NULL, '2013-09-06 13:57:07', '2013-09-06 15:57:07'),
(705, 'Rédacteur chef', NULL, NULL, 1, NULL, '2013-09-06 13:57:07', '2013-09-06 15:57:07'),
(706, 'Journaliste', NULL, NULL, 1, NULL, '2013-09-06 13:57:07', '2013-09-06 15:57:07'),
(707, 'Sapeur pompier', NULL, NULL, 1, NULL, '2013-09-06 13:57:07', '2013-09-06 15:57:07'),
(708, 'Manipulateur d''électroradiologie médicale', NULL, NULL, 1, NULL, '2013-09-06 13:57:07', '2013-09-06 15:57:07'),
(709, 'Réceptionniste', NULL, NULL, 1, NULL, '2013-09-06 13:57:07', '2013-09-06 15:57:07'),
(710, 'Taxidermiste', NULL, NULL, 1, NULL, '2013-09-06 13:57:07', '2013-09-06 15:57:07'),
(711, 'Soudeur', NULL, NULL, 1, NULL, '2013-09-06 13:57:07', '2013-09-06 15:57:07'),
(712, 'Journaliste radio', NULL, NULL, 1, NULL, '2013-09-06 13:57:07', '2013-09-06 15:57:07'),
(713, 'Prothésiste', NULL, NULL, 1, NULL, '2013-09-06 13:57:07', '2013-09-06 15:57:07'),
(714, 'Tôlier', NULL, NULL, 1, NULL, '2013-09-06 13:57:07', '2013-09-06 15:57:07'),
(715, 'Verrier', NULL, NULL, 1, NULL, '2013-09-06 13:57:07', '2013-09-06 15:57:07'),
(716, 'Menuisier', NULL, NULL, 1, NULL, '2013-09-06 13:57:07', '2013-09-06 15:57:07'),
(717, 'Moniteur éducateur', NULL, NULL, 1, NULL, '2013-09-06 13:57:07', '2013-09-06 15:57:07'),
(718, 'Juge de l''application des peines', NULL, NULL, 1, NULL, '2013-09-06 13:57:07', '2013-09-06 15:57:07'),
(719, 'Ingénieur De Construction De Réseaux', NULL, NULL, 1, NULL, '2013-09-06 13:57:07', '2013-09-06 15:57:07'),
(720, 'Traffic Manager', NULL, NULL, 1, NULL, '2013-09-06 13:57:07', '2013-09-06 15:57:07'),
(721, 'Ingénieur de recherche', NULL, NULL, 1, NULL, '2013-09-06 13:57:08', '2013-09-06 15:57:08'),
(722, 'Paramétreur De Progiciels', NULL, NULL, 1, NULL, '2013-09-06 13:57:08', '2013-09-06 15:57:08'),
(723, 'Skippeur', NULL, NULL, 1, NULL, '2013-09-06 13:57:08', '2013-09-06 15:57:08'),
(724, 'Facteur instrumental', NULL, NULL, 1, NULL, '2013-09-06 13:57:08', '2013-09-06 15:57:08'),
(725, 'Juge d''Instance', NULL, NULL, 1, NULL, '2013-09-06 13:57:08', '2013-09-06 15:57:08'),
(726, 'Magistrat', NULL, NULL, 1, NULL, '2013-09-06 13:57:08', '2013-09-06 15:57:08'),
(727, 'Garde du corps', NULL, NULL, 1, NULL, '2013-09-06 13:57:08', '2013-09-06 15:57:08'),
(728, 'Horiculteur', NULL, NULL, 1, NULL, '2013-09-06 13:57:08', '2013-09-06 15:57:08'),
(729, 'Infirmier', NULL, NULL, 1, NULL, '2013-09-06 13:57:08', '2013-09-06 15:57:08'),
(730, 'Sapeur-pompier', NULL, NULL, 1, NULL, '2013-09-06 13:57:08', '2013-09-06 15:57:08'),
(731, 'Technicien Réseau', NULL, NULL, 1, NULL, '2013-09-06 13:57:08', '2013-09-06 15:57:08'),
(732, 'Ingénieur Développement De Composants', NULL, NULL, 1, NULL, '2013-09-06 13:57:08', '2013-09-06 15:57:08'),
(733, 'Solier moquettiste', NULL, NULL, 1, NULL, '2013-09-06 13:57:08', '2013-09-06 15:57:08'),
(734, 'Maquettiste', NULL, NULL, 1, NULL, '2013-09-06 13:57:08', '2013-09-06 15:57:08'),
(735, 'Les adjoints administratifs de la police nati', NULL, NULL, 1, NULL, '2013-09-06 13:57:08', '2013-09-06 15:57:08'),
(736, 'Serveur de restaurant', NULL, NULL, 1, NULL, '2013-09-06 13:57:08', '2013-09-06 15:57:08'),
(737, 'Météorologue', NULL, NULL, 1, NULL, '2013-09-06 13:57:08', '2013-09-06 15:57:08'),
(738, 'Technicien maintenance auto', NULL, NULL, 1, NULL, '2013-09-06 13:57:08', '2013-09-06 15:57:08'),
(739, 'Ingénieur d''études sanitaires', NULL, NULL, 1, NULL, '2013-09-06 13:57:08', '2013-09-06 15:57:08'),
(740, 'Secrétaire administratif des affaires sanitai', NULL, NULL, 1, NULL, '2013-09-06 13:57:08', '2013-09-06 15:57:08'),
(741, 'Eboueur', NULL, NULL, 1, NULL, '2013-09-06 13:57:08', '2013-09-06 15:57:08'),
(742, 'Hotliner', NULL, NULL, 1, NULL, '2013-09-06 13:57:08', '2013-09-06 15:57:08'),
(743, 'Vendeur En Micro-Informatique', NULL, NULL, 1, NULL, '2013-09-06 13:57:08', '2013-09-06 15:57:08'),
(744, 'Rédacteur en assurances', NULL, NULL, 1, NULL, '2013-09-06 13:57:08', '2013-09-06 15:57:08'),
(745, 'Livreur', NULL, NULL, 1, NULL, '2013-09-06 13:57:08', '2013-09-06 15:57:08'),
(746, 'Technicien de l''éducation nationale', NULL, NULL, 1, NULL, '2013-09-06 13:57:08', '2013-09-06 15:57:08'),
(747, 'Educateur technique spécialisé', NULL, NULL, 1, NULL, '2013-09-06 13:57:09', '2013-09-06 15:57:09'),
(748, 'Secrétaire médico-sociale', NULL, NULL, 1, NULL, '2013-09-06 13:57:09', '2013-09-06 15:57:09'),
(749, 'Enseignant Chercheur', NULL, NULL, 1, NULL, '2013-09-06 13:57:09', '2013-09-06 15:57:09'),
(750, 'Planneur stratégique', NULL, NULL, 1, NULL, '2013-09-06 13:57:09', '2013-09-06 15:57:09'),
(751, 'Technicien de fabrication', NULL, NULL, 1, NULL, '2013-09-06 13:57:09', '2013-09-06 15:57:09'),
(752, 'Secrétaire de rédaction', NULL, NULL, 1, NULL, '2013-09-06 13:57:09', '2013-09-06 15:57:09'),
(753, 'Sage-Femme', NULL, NULL, 1, NULL, '2013-09-06 13:57:09', '2013-09-06 15:57:09'),
(754, 'Technicien en analyses biomédicales', NULL, NULL, 1, NULL, '2013-09-06 13:57:09', '2013-09-06 15:57:09'),
(755, 'Formateur En Informatique', NULL, NULL, 1, NULL, '2013-09-06 13:57:09', '2013-09-06 15:57:09'),
(756, 'Ingénieur Sécurité', NULL, NULL, 1, NULL, '2013-09-06 13:57:09', '2013-09-06 15:57:09'),
(757, 'Moniteur d''auto-école', NULL, NULL, 1, NULL, '2013-09-06 13:57:09', '2013-09-06 15:57:09'),
(758, 'Ergonome', NULL, NULL, 1, NULL, '2013-09-06 13:57:09', '2013-09-06 15:57:09'),
(759, 'Webmestre', NULL, NULL, 1, NULL, '2013-09-06 13:57:09', '2013-09-06 15:57:09'),
(760, 'Ingénieur Technico-Commercial', NULL, NULL, 1, NULL, '2013-09-06 13:57:09', '2013-09-06 15:57:09'),
(761, 'Monteur', NULL, NULL, 1, NULL, '2013-09-06 13:57:09', '2013-09-06 15:57:09'),
(762, 'Puéricultrice', NULL, NULL, 1, NULL, '2013-09-06 13:57:09', '2013-09-06 15:57:09'),
(763, 'Hôtesse d''accueil', NULL, NULL, 1, NULL, '2013-09-06 13:57:10', '2013-09-06 15:57:10'),
(764, 'Responsable marketing', NULL, NULL, 1, NULL, '2013-09-06 13:57:10', '2013-09-06 15:57:10'),
(765, 'Médecin Territorial', NULL, NULL, 1, NULL, '2013-09-06 13:57:10', '2013-09-06 15:57:10'),
(766, 'Professeur agrégé', NULL, NULL, 1, NULL, '2013-09-06 13:57:10', '2013-09-06 15:57:10'),
(767, 'Ouvrier agricole', NULL, NULL, 1, NULL, '2013-09-06 13:57:10', '2013-09-06 15:57:10'),
(768, 'Webmarketeur', NULL, NULL, 1, NULL, '2013-09-06 13:57:10', '2013-09-06 15:57:10'),
(769, 'Iconographe', NULL, NULL, 1, NULL, '2013-09-06 13:57:10', '2013-09-06 15:57:10'),
(770, 'Technicien Chimiste', NULL, NULL, 1, NULL, '2013-09-06 13:57:10', '2013-09-06 15:57:10'),
(771, 'Technicien Micro', NULL, NULL, 1, NULL, '2013-09-06 13:57:10', '2013-09-06 15:57:10'),
(772, 'Manutentionnaire cariste', NULL, NULL, 1, NULL, '2013-09-06 13:57:10', '2013-09-06 15:57:10'),
(773, 'Expert comptable', NULL, NULL, 1, NULL, '2013-09-06 13:57:10', '2013-09-06 15:57:10'),
(774, 'Encadreur', NULL, NULL, 1, NULL, '2013-09-06 13:57:10', '2013-09-06 15:57:10'),
(775, 'Garde-champêtre', NULL, NULL, 1, NULL, '2013-09-06 13:57:10', '2013-09-06 15:57:10'),
(776, 'Monteur électricien réseau edf', NULL, NULL, 1, NULL, '2013-09-06 13:57:10', '2013-09-06 15:57:10'),
(777, 'Régisseur', NULL, NULL, 1, NULL, '2013-09-06 13:57:10', '2013-09-06 15:57:10'),
(778, 'Photographe', NULL, NULL, 1, NULL, '2013-09-06 13:57:11', '2013-09-06 15:57:11'),
(779, 'Guide de haute montagne', NULL, NULL, 1, NULL, '2013-09-06 13:57:11', '2013-09-06 15:57:11'),
(780, 'Techniciens de recherche et de formation', NULL, NULL, 1, NULL, '2013-09-06 13:57:11', '2013-09-06 15:57:11'),
(781, 'Ingénieur système-réseau', NULL, NULL, 1, NULL, '2013-09-06 13:57:11', '2013-09-06 15:57:11'),
(782, 'Hot-Liner Technicien Help-Desk', NULL, NULL, 1, NULL, '2013-09-06 13:57:11', '2013-09-06 15:57:11'),
(783, 'Traducteur', NULL, NULL, 1, NULL, '2013-09-06 13:57:11', '2013-09-06 15:57:11'),
(784, 'Directeur Des Systèmes D''Information', NULL, NULL, 1, NULL, '2013-09-06 13:57:11', '2013-09-06 15:57:11'),
(785, 'Professeur certifié', NULL, NULL, 1, NULL, '2013-09-06 13:57:11', '2013-09-06 15:57:11'),
(786, 'Responsable De Compte', NULL, NULL, 1, NULL, '2013-09-06 13:57:11', '2013-09-06 15:57:11'),
(787, 'Story-boarder', NULL, NULL, 1, NULL, '2013-09-06 13:57:11', '2013-09-06 15:57:11'),
(788, 'Ingénieur logistique', NULL, NULL, 1, NULL, '2013-09-06 13:57:11', '2013-09-06 15:57:11'),
(789, 'Ingénieur Intégration', NULL, NULL, 1, NULL, '2013-09-06 13:57:11', '2013-09-06 15:57:11'),
(790, 'Ingénieur Développement Logiciels', NULL, NULL, 1, NULL, '2013-09-06 13:57:11', '2013-09-06 15:57:11'),
(791, 'Documentaliste', NULL, NULL, 1, NULL, '2013-09-06 13:57:12', '2013-09-06 15:57:12'),
(792, 'Journaliste reporter', NULL, NULL, 1, NULL, '2013-09-06 13:57:12', '2013-09-06 15:57:12'),
(793, 'Trader', NULL, NULL, 1, NULL, '2013-09-06 13:57:12', '2013-09-06 15:57:12'),
(794, 'Webmaster', NULL, NULL, 1, NULL, '2013-09-06 13:57:12', '2013-09-06 15:57:12'),
(795, 'Les Directeurs de services pénitentiaires', NULL, NULL, 1, NULL, '2013-09-06 13:57:12', '2013-09-06 15:57:12'),
(796, 'Maître d''hôtel', NULL, NULL, 1, NULL, '2013-09-06 13:57:12', '2013-09-06 15:57:12'),
(797, 'Océanographe', NULL, NULL, 1, NULL, '2013-09-06 13:57:12', '2013-09-06 15:57:12'),
(798, 'Personnel de surveillance', NULL, NULL, 1, NULL, '2013-09-06 13:57:12', '2013-09-06 15:57:12'),
(799, 'Ingénieur des travaux', NULL, NULL, 1, NULL, '2013-09-06 13:57:12', '2013-09-06 15:57:12'),
(800, 'Magasinier en chef des bibliothèques', NULL, NULL, 1, NULL, '2013-09-06 13:57:12', '2013-09-06 15:57:12'),
(801, 'Ingénieur de laboratoire', NULL, NULL, 1, NULL, '2013-09-06 13:57:12', '2013-09-06 15:57:12'),
(802, 'Prothésiste dentaire', NULL, NULL, 1, NULL, '2013-09-06 13:57:12', '2013-09-06 15:57:12'),
(803, 'Directeur de parc naturel', NULL, NULL, 1, NULL, '2013-09-06 13:57:13', '2013-09-06 15:57:13'),
(804, 'Notaire', NULL, NULL, 1, NULL, '2013-09-06 13:57:13', '2013-09-06 15:57:13'),
(805, 'Juge de Grande Instance', NULL, NULL, 1, NULL, '2013-09-06 13:57:13', '2013-09-06 15:57:13'),
(806, 'Directeur Technique', NULL, NULL, 1, NULL, '2013-09-06 13:57:13', '2013-09-06 15:57:13'),
(807, 'Technicien de labo photo', NULL, NULL, 1, NULL, '2013-09-06 13:57:13', '2013-09-06 15:57:13'),
(808, 'Diseño Packaging', NULL, NULL, 1, NULL, '2013-09-09 12:45:18', '2013-09-09 14:45:18'),
(809, 'Diseño de identidad', NULL, NULL, 1, NULL, '2013-09-09 12:45:18', '2013-09-09 14:45:18'),
(810, 'Diseño Grafico Freelance', NULL, NULL, 1, NULL, '2013-09-09 12:45:18', '2013-09-09 14:45:18'),
(811, 'fotografo de moda', NULL, NULL, 1, NULL, '2013-09-09 12:45:18', '2013-09-09 14:45:18'),
(812, 'Fotografia', NULL, NULL, 1, NULL, '2013-09-09 12:45:18', '2013-09-09 14:45:18'),
(813, 'Diseñador Web', NULL, NULL, 1, NULL, '2013-09-09 12:45:18', '2013-09-09 14:45:18'),
(814, 'Diseñador Gráfico', NULL, NULL, 1, NULL, '2013-09-09 12:45:18', '2013-09-09 14:45:18'),
(815, 'Branding', NULL, NULL, 1, NULL, '2013-09-09 12:45:18', '2013-09-09 14:45:18'),
(816, 'Diseño Editorial', NULL, NULL, 1, NULL, '2013-09-09 12:52:42', '2013-09-09 14:52:42'),
(817, 'Diseño gráfico', NULL, NULL, 1, NULL, '2013-09-09 12:52:42', '2013-09-09 14:52:42'),
(818, 'Diseñadora', NULL, NULL, 1, NULL, '2013-09-09 12:52:43', '2013-09-09 14:52:43'),
(819, 'Diseñadora Grafica', NULL, NULL, 1, NULL, '2013-09-09 12:52:43', '2013-09-09 14:52:43'),
(820, 'Designer', NULL, NULL, 1, NULL, '2013-09-09 12:52:43', '2013-09-09 14:52:43'),
(821, 'edición', NULL, NULL, 1, NULL, '2013-09-09 12:52:52', '2013-09-09 14:52:52'),
(822, 'cámara', NULL, NULL, 1, NULL, '2013-09-09 12:52:52', '2013-09-09 14:52:52'),
(823, 'postproducción', NULL, NULL, 1, NULL, '2013-09-09 12:52:52', '2013-09-09 14:52:52'),
(824, 'diseñador', NULL, NULL, 1, NULL, '2013-09-09 12:52:59', '2013-09-09 14:52:59'),
(825, 'Diseño Web', NULL, NULL, 1, NULL, '2013-09-09 12:53:05', '2013-09-09 14:53:05'),
(826, 'director de arte', NULL, NULL, 1, NULL, '2013-09-09 12:53:08', '2013-09-09 14:53:08'),
(827, 'Diseño Web y Motion Graphics', NULL, NULL, 1, NULL, '2013-09-09 12:53:11', '2013-09-09 14:53:11'),
(828, 'ilustración', NULL, NULL, 1, NULL, '2013-09-09 12:53:16', '2013-09-09 14:53:16'),
(829, 'magazine', NULL, NULL, 1, NULL, '2013-09-09 12:53:16', '2013-09-09 14:53:16'),
(830, 'cine', NULL, NULL, 1, NULL, '2013-09-09 12:53:16', '2013-09-09 14:53:16'),
(831, 'publicidad', NULL, NULL, 1, NULL, '2013-09-09 12:53:16', '2013-09-09 14:53:16'),
(832, 'Productor de moda', NULL, NULL, 1, NULL, '2013-09-09 12:53:16', '2013-09-09 14:53:16'),
(833, 'wardrobe', NULL, NULL, 1, NULL, '2013-09-09 12:53:16', '2013-09-09 14:53:16'),
(834, 'Vestuarista', NULL, NULL, 1, NULL, '2013-09-09 12:53:16', '2013-09-09 14:53:16'),
(835, 'Ilustrador', NULL, NULL, 1, NULL, '2013-09-09 12:53:27', '2013-09-09 14:53:27'),
(836, 'Caligrafía', NULL, NULL, 1, NULL, '2013-09-09 12:53:27', '2013-09-09 14:53:27'),
(837, 'Fotógrafo', NULL, NULL, 1, NULL, '2013-09-09 12:53:32', '2013-09-09 14:53:32'),
(838, 'Investigador', NULL, NULL, 1, NULL, '2013-09-09 12:53:32', '2013-09-09 14:53:32'),
(839, 'Retocador digital', NULL, NULL, 1, NULL, '2013-09-09 12:53:38', '2013-09-09 14:53:38'),
(840, 'Web', NULL, NULL, 1, NULL, '2013-09-09 12:53:40', '2013-09-09 14:53:40'),
(841, 'photo retouch', NULL, NULL, 1, NULL, '2013-09-09 12:53:44', '2013-09-09 14:53:44'),
(842, 'Fotografia publicittaria', NULL, NULL, 1, NULL, '2013-09-09 12:53:44', '2013-09-09 14:53:44'),
(843, 'retoque digital', NULL, NULL, 1, NULL, '2013-09-09 12:53:44', '2013-09-09 14:53:44'),
(844, 'redactor', NULL, NULL, 1, NULL, '2013-09-09 12:53:52', '2013-09-09 14:53:52'),
(845, 'productor', NULL, NULL, 1, NULL, '2013-09-09 12:53:52', '2013-09-09 14:53:52'),
(846, 'director creativo', NULL, NULL, 1, NULL, '2013-09-09 12:53:52', '2013-09-09 14:53:52'),
(847, 'community -manager', NULL, NULL, 1, NULL, '2013-09-09 12:53:55', '2013-09-09 14:53:55'),
(848, '3D', NULL, NULL, 1, NULL, '2013-09-09 12:53:57', '2013-09-09 14:53:57'),
(849, 'Animacion', NULL, NULL, 1, NULL, '2013-09-09 12:53:57', '2013-09-09 14:53:57'),
(850, 'Dibujo', NULL, NULL, 1, NULL, '2013-09-09 12:53:57', '2013-09-09 14:53:57'),
(851, '2D', NULL, NULL, 1, NULL, '2013-09-09 12:53:57', '2013-09-09 14:53:57'),
(852, 'diseño multimedial', NULL, NULL, 1, NULL, '2013-09-09 12:54:00', '2013-09-09 14:54:00'),
(853, 'sr. copywriter producer', NULL, NULL, 1, NULL, '2013-09-09 12:54:01', '2013-09-09 14:54:01'),
(854, 'sr copywriter', NULL, NULL, 1, NULL, '2013-09-09 12:54:01', '2013-09-09 14:54:01'),
(855, 'creative director', NULL, NULL, 1, NULL, '2013-09-09 12:54:01', '2013-09-09 14:54:01'),
(856, 'diseño', NULL, NULL, 1, NULL, '2013-09-09 12:54:04', '2013-09-09 14:54:04'),
(857, 'bebes', NULL, NULL, 1, NULL, '2013-09-09 12:54:05', '2013-09-09 14:54:05'),
(858, 'campañas', NULL, NULL, 1, NULL, '2013-09-09 12:54:05', '2013-09-09 14:54:05'),
(859, 'chicos', NULL, NULL, 1, NULL, '2013-09-09 12:54:05', '2013-09-09 14:54:05'),
(860, 'Caricaturas (para diarios', NULL, NULL, 1, NULL, '2013-09-09 12:54:05', '2013-09-09 14:54:05'),
(861, 'revistas', NULL, NULL, 1, NULL, '2013-09-09 12:54:05', '2013-09-09 14:54:05'),
(862, 'libros', NULL, NULL, 1, NULL, '2013-09-09 12:54:05', '2013-09-09 14:54:05'),
(863, 'agencias', NULL, NULL, 1, NULL, '2013-09-09 12:54:05', '2013-09-09 14:54:05'),
(864, 'pág. web) Ilustraciones', NULL, NULL, 1, NULL, '2013-09-09 12:54:05', '2013-09-09 14:54:05'),
(865, 'Comics', NULL, NULL, 1, NULL, '2013-09-09 12:54:05', '2013-09-09 14:54:05'),
(866, 'Maquetador web', NULL, NULL, 1, NULL, '2013-09-09 12:54:06', '2013-09-09 14:54:06'),
(867, 'Soy Asesor Comercial de Pymes y Emprendedores', NULL, NULL, 1, NULL, '2013-09-09 12:54:10', '2013-09-09 14:54:10'),
(868, 'Sound Designer', NULL, NULL, 1, NULL, '2013-09-09 12:54:11', '2013-09-09 14:54:11'),
(869, 'Sound Producer', NULL, NULL, 1, NULL, '2013-09-09 12:54:11', '2013-09-09 14:54:11'),
(870, 'Produccion', NULL, NULL, 1, NULL, '2013-09-09 12:54:13', '2013-09-09 14:54:13'),
(871, 'Cámara; edición; postproducción.', NULL, NULL, 1, NULL, '2013-09-09 12:54:13', '2013-09-09 14:54:13'),
(872, 'Locutora- Conductora- Actriz de Doblaje', NULL, NULL, 1, NULL, '2013-09-09 12:54:27', '2013-09-09 14:54:27'),
(873, 'Diseadora gráfica', NULL, NULL, 1, NULL, '2013-09-09 12:54:28', '2013-09-09 14:54:28'),
(874, 'Realizadora', NULL, NULL, 1, NULL, '2013-09-09 12:54:30', '2013-09-09 14:54:30'),
(875, 'Redactora', NULL, NULL, 1, NULL, '2013-09-09 12:54:39', '2013-09-09 14:54:39'),
(876, 'Asistente de fotógrafa', NULL, NULL, 1, NULL, '2013-09-09 12:54:39', '2013-09-09 14:54:39'),
(877, 'cuentos', NULL, NULL, 1, NULL, '2013-09-09 12:54:41', '2013-09-09 14:54:41'),
(878, 'Asistente de producción / Asistente de direcc', NULL, NULL, 1, NULL, '2013-09-09 12:54:45', '2013-09-09 14:54:45'),
(879, 'Voice Over', NULL, NULL, 1, NULL, '2013-09-09 12:54:47', '2013-09-09 14:54:47'),
(880, 'Locución', NULL, NULL, 1, NULL, '2013-09-09 12:54:47', '2013-09-09 14:54:47'),
(881, 'E-Learning', NULL, NULL, 1, NULL, '2013-09-09 12:54:47', '2013-09-09 14:54:47'),
(882, 'Diseñadora web', NULL, NULL, 1, NULL, '2013-09-09 12:54:50', '2013-09-09 14:54:50'),
(883, 'jefe de proyecto', NULL, NULL, 1, NULL, '2013-09-09 12:54:53', '2013-09-09 14:54:53'),
(884, 'Desarrollo y Marketing Web', NULL, NULL, 1, NULL, '2013-09-09 12:54:56', '2013-09-09 14:54:56'),
(885, 'Traductora e Intérprete', NULL, NULL, 1, NULL, '2013-09-09 12:54:57', '2013-09-09 14:54:57'),
(886, 'Traductora de español y portugués', NULL, NULL, 1, NULL, '2013-09-09 12:54:59', '2013-09-09 14:54:59'),
(887, 'francés', NULL, NULL, 1, NULL, '2013-09-09 12:55:01', '2013-09-09 14:55:01'),
(888, 'interprete', NULL, NULL, 1, NULL, '2013-09-09 12:55:01', '2013-09-09 14:55:01'),
(889, 'traductor', NULL, NULL, 1, NULL, '2013-09-09 12:55:01', '2013-09-09 14:55:01'),
(890, 'español', NULL, NULL, 1, NULL, '2013-09-09 12:55:01', '2013-09-09 14:55:01'),
(891, 'Desarrollo', NULL, NULL, 1, NULL, '2013-09-09 12:55:02', '2013-09-09 14:55:02'),
(892, 'TRADUCTORA', NULL, NULL, 1, NULL, '2013-09-09 12:55:02', '2013-09-09 14:55:02'),
(893, 'dieñador motion graphics', NULL, NULL, 1, NULL, '2013-09-09 12:55:08', '2013-09-09 14:55:08'),
(894, 'Tecnico Audiovisual', NULL, NULL, 1, NULL, '2013-09-09 12:55:10', '2013-09-09 14:55:10'),
(895, 'Realizador', NULL, NULL, 1, NULL, '2013-09-09 12:55:10', '2013-09-09 14:55:10'),
(896, 'Director', NULL, NULL, 1, NULL, '2013-09-09 12:55:10', '2013-09-09 14:55:10'),
(897, 'Maquetacion web', NULL, NULL, 1, NULL, '2013-09-09 12:55:12', '2013-09-09 14:55:12'),
(898, 'television', NULL, NULL, 1, NULL, '2013-09-09 12:55:14', '2013-09-09 14:55:14'),
(899, 'camarografo', NULL, NULL, 1, NULL, '2013-09-09 12:55:14', '2013-09-09 14:55:14'),
(900, 'editora', NULL, NULL, 1, NULL, '2013-09-09 12:55:14', '2013-09-09 14:55:14'),
(901, 'video', NULL, NULL, 1, NULL, '2013-09-09 12:55:14', '2013-09-09 14:55:14'),
(902, 'Voice Talent', NULL, NULL, 1, NULL, '2013-09-09 12:55:15', '2013-09-09 14:55:15'),
(903, 'locutor comercial', NULL, NULL, 1, NULL, '2013-09-09 12:55:15', '2013-09-09 14:55:15'),
(904, 'voice', NULL, NULL, 1, NULL, '2013-09-09 12:55:15', '2013-09-09 14:55:15'),
(905, 'Locutor', NULL, NULL, 1, NULL, '2013-09-09 12:55:15', '2013-09-09 14:55:15'),
(906, 'tv', NULL, NULL, 1, NULL, '2013-09-09 12:55:19', '2013-09-09 14:55:19'),
(907, 'desarrollo web', NULL, NULL, 1, NULL, '2013-09-09 12:55:20', '2013-09-09 14:55:20'),
(908, 'diseño con joomla', NULL, NULL, 1, NULL, '2013-09-09 12:55:20', '2013-09-09 14:55:20'),
(909, 'html', NULL, NULL, 1, NULL, '2013-09-09 12:55:20', '2013-09-09 14:55:20'),
(910, 'css', NULL, NULL, 1, NULL, '2013-09-09 12:55:20', '2013-09-09 14:55:20'),
(911, 'freelance', NULL, NULL, 1, NULL, '2013-09-09 12:55:22', '2013-09-09 14:55:22'),
(912, 'ANIMACION 2d', NULL, NULL, 1, NULL, '2013-09-09 12:55:23', '2013-09-09 14:55:23'),
(913, 'Editor', NULL, NULL, 1, NULL, '2013-09-09 12:55:27', '2013-09-09 14:55:27'),
(914, 'Corrector', NULL, NULL, 1, NULL, '2013-09-09 12:55:27', '2013-09-09 14:55:27'),
(915, 'Traducción Inglés - Español', NULL, NULL, 1, NULL, '2013-09-09 12:55:28', '2013-09-09 14:55:28'),
(916, 'Diseñador Industrial/Grafico', NULL, NULL, 1, NULL, '2013-09-09 12:55:29', '2013-09-09 14:55:29'),
(917, NULL, NULL, 'Diseño Packaging', 1, NULL, '2013-09-09 16:57:17', '2013-09-09 18:57:17'),
(918, NULL, NULL, 'Diseño de identidad', 1, NULL, '2013-09-09 16:57:17', '2013-09-09 18:57:17'),
(919, NULL, NULL, 'Diseño Grafico Freelance', 1, NULL, '2013-09-09 16:57:17', '2013-09-09 18:57:17');
INSERT INTO `adm_postes` (`id_poste`, `adm_poste_fr`, `adm_poste_en`, `adm_poste_es`, `actif`, `var`, `cree_le`, `modifie_le`) VALUES
(920, NULL, NULL, 'fotografo de moda', 1, NULL, '2013-09-09 16:57:17', '2013-09-09 18:57:17'),
(921, NULL, NULL, 'Fotografia', 1, NULL, '2013-09-09 16:57:17', '2013-09-09 18:57:17'),
(922, NULL, NULL, 'Diseñador Web', 1, NULL, '2013-09-09 16:57:17', '2013-09-09 18:57:17'),
(923, NULL, NULL, 'Diseñador Gráfico', 1, NULL, '2013-09-09 16:57:17', '2013-09-09 18:57:17'),
(924, NULL, NULL, 'Branding', 1, NULL, '2013-09-09 16:57:17', '2013-09-09 18:57:17'),
(925, NULL, NULL, 'Diseño Editorial', 1, NULL, '2013-09-09 16:57:18', '2013-09-09 18:57:18'),
(926, NULL, NULL, 'Diseño gráfico', 1, NULL, '2013-09-09 16:57:18', '2013-09-09 18:57:18'),
(927, NULL, NULL, 'Diseñadora', 1, NULL, '2013-09-09 16:57:18', '2013-09-09 18:57:18'),
(928, NULL, NULL, 'Diseñadora Grafica', 1, NULL, '2013-09-09 16:57:18', '2013-09-09 18:57:18'),
(929, NULL, NULL, 'Designer', 1, NULL, '2013-09-09 16:57:18', '2013-09-09 18:57:18'),
(930, NULL, NULL, 'edición', 1, NULL, '2013-09-09 16:57:19', '2013-09-09 18:57:19'),
(931, NULL, NULL, 'cámara', 1, NULL, '2013-09-09 16:57:19', '2013-09-09 18:57:19'),
(932, NULL, NULL, 'postproducción', 1, NULL, '2013-09-09 16:57:19', '2013-09-09 18:57:19'),
(933, NULL, NULL, 'diseñador', 1, NULL, '2013-09-09 16:57:19', '2013-09-09 18:57:19'),
(934, NULL, NULL, 'Diseño Web', 1, NULL, '2013-09-09 16:57:19', '2013-09-09 18:57:19'),
(935, NULL, NULL, 'director de arte', 1, NULL, '2013-09-09 16:57:20', '2013-09-09 18:57:20'),
(936, NULL, NULL, 'Diseño Web y Motion Graphics', 1, NULL, '2013-09-09 16:57:20', '2013-09-09 18:57:20'),
(937, NULL, NULL, 'ilustración', 1, NULL, '2013-09-09 16:57:20', '2013-09-09 18:57:20'),
(938, NULL, NULL, 'magazine', 1, NULL, '2013-09-09 16:57:20', '2013-09-09 18:57:20'),
(939, NULL, NULL, 'cine', 1, NULL, '2013-09-09 16:57:20', '2013-09-09 18:57:20'),
(940, NULL, NULL, 'publicidad', 1, NULL, '2013-09-09 16:57:20', '2013-09-09 18:57:20'),
(941, NULL, NULL, 'Productor de moda', 1, NULL, '2013-09-09 16:57:20', '2013-09-09 18:57:20'),
(942, NULL, NULL, 'wardrobe', 1, NULL, '2013-09-09 16:57:20', '2013-09-09 18:57:20'),
(943, NULL, NULL, 'Vestuarista', 1, NULL, '2013-09-09 16:57:20', '2013-09-09 18:57:20'),
(944, NULL, NULL, 'Ilustrador', 1, NULL, '2013-09-09 16:57:20', '2013-09-09 18:57:20'),
(945, NULL, NULL, 'Caligrafía', 1, NULL, '2013-09-09 16:57:20', '2013-09-09 18:57:20'),
(946, NULL, NULL, 'Fotógrafo', 1, NULL, '2013-09-09 16:57:21', '2013-09-09 18:57:21'),
(947, NULL, NULL, 'Investigador', 1, NULL, '2013-09-09 16:57:21', '2013-09-09 18:57:21'),
(948, NULL, NULL, 'Retocador digital', 1, NULL, '2013-09-09 16:57:21', '2013-09-09 18:57:21'),
(949, NULL, NULL, 'Web', 1, NULL, '2013-09-09 16:57:21', '2013-09-09 18:57:21'),
(950, NULL, NULL, 'photo retouch', 1, NULL, '2013-09-09 16:57:21', '2013-09-09 18:57:21'),
(951, NULL, NULL, 'Fotografia publicittaria', 1, NULL, '2013-09-09 16:57:21', '2013-09-09 18:57:21'),
(952, NULL, NULL, 'retoque digital', 1, NULL, '2013-09-09 16:57:21', '2013-09-09 18:57:21'),
(953, NULL, NULL, 'redactor', 1, NULL, '2013-09-09 16:57:22', '2013-09-09 18:57:22'),
(954, NULL, NULL, 'productor', 1, NULL, '2013-09-09 16:57:22', '2013-09-09 18:57:22'),
(955, NULL, NULL, 'director creativo', 1, NULL, '2013-09-09 16:57:22', '2013-09-09 18:57:22'),
(956, NULL, NULL, 'community -manager', 1, NULL, '2013-09-09 16:57:22', '2013-09-09 18:57:22'),
(957, NULL, NULL, '3D', 1, NULL, '2013-09-09 16:57:22', '2013-09-09 18:57:22'),
(958, NULL, NULL, 'Animacion', 1, NULL, '2013-09-09 16:57:22', '2013-09-09 18:57:22'),
(959, NULL, NULL, 'Dibujo', 1, NULL, '2013-09-09 16:57:22', '2013-09-09 18:57:22'),
(960, NULL, NULL, '2D', 1, NULL, '2013-09-09 16:57:23', '2013-09-09 18:57:23'),
(961, NULL, NULL, 'diseño multimedial', 1, NULL, '2013-09-09 16:57:23', '2013-09-09 18:57:23'),
(962, NULL, NULL, 'sr. copywriter producer', 1, NULL, '2013-09-09 16:57:23', '2013-09-09 18:57:23'),
(963, NULL, NULL, 'sr copywriter', 1, NULL, '2013-09-09 16:57:23', '2013-09-09 18:57:23'),
(964, NULL, NULL, 'creative director', 1, NULL, '2013-09-09 16:57:23', '2013-09-09 18:57:23'),
(965, NULL, NULL, 'diseño', 1, NULL, '2013-09-09 16:57:23', '2013-09-09 18:57:23'),
(966, NULL, NULL, 'bebes', 1, NULL, '2013-09-09 16:57:23', '2013-09-09 18:57:23'),
(967, NULL, NULL, 'campañas', 1, NULL, '2013-09-09 16:57:23', '2013-09-09 18:57:23'),
(968, NULL, NULL, 'chicos', 1, NULL, '2013-09-09 16:57:23', '2013-09-09 18:57:23'),
(969, NULL, NULL, 'Caricaturas (para diarios', 1, NULL, '2013-09-09 16:57:23', '2013-09-09 18:57:23'),
(970, NULL, NULL, 'revistas', 1, NULL, '2013-09-09 16:57:23', '2013-09-09 18:57:23'),
(971, NULL, NULL, 'libros', 1, NULL, '2013-09-09 16:57:23', '2013-09-09 18:57:23'),
(972, NULL, NULL, 'agencias', 1, NULL, '2013-09-09 16:57:23', '2013-09-09 18:57:23'),
(973, NULL, NULL, 'pág. web) Ilustraciones', 1, NULL, '2013-09-09 16:57:23', '2013-09-09 18:57:23'),
(974, NULL, NULL, 'Comics', 1, NULL, '2013-09-09 16:57:23', '2013-09-09 18:57:23'),
(975, NULL, NULL, 'Maquetador web', 1, NULL, '2013-09-09 16:57:23', '2013-09-09 18:57:23'),
(976, NULL, NULL, 'Soy Asesor Comercial de Pymes y Emprendedores', 1, NULL, '2013-09-09 16:57:24', '2013-09-09 18:57:24'),
(977, NULL, NULL, 'Sound Designer', 1, NULL, '2013-09-09 16:57:24', '2013-09-09 18:57:24'),
(978, NULL, NULL, 'Sound Producer', 1, NULL, '2013-09-09 16:57:24', '2013-09-09 18:57:24'),
(979, NULL, NULL, 'Produccion', 1, NULL, '2013-09-09 16:57:24', '2013-09-09 18:57:24'),
(980, NULL, NULL, 'Cámara; edición; postproducción.', 1, NULL, '2013-09-09 16:57:24', '2013-09-09 18:57:24'),
(981, NULL, NULL, 'Locutora- Conductora- Actriz de Doblaje', 1, NULL, '2013-09-09 16:57:25', '2013-09-09 18:57:25'),
(982, NULL, NULL, 'Diseadora gráfica', 1, NULL, '2013-09-09 16:57:25', '2013-09-09 18:57:25'),
(983, NULL, NULL, 'Realizadora', 1, NULL, '2013-09-09 16:57:25', '2013-09-09 18:57:25'),
(984, NULL, NULL, 'Redactora', 1, NULL, '2013-09-09 16:57:25', '2013-09-09 18:57:25'),
(985, NULL, NULL, 'Asistente de fotógrafa', 1, NULL, '2013-09-09 16:57:25', '2013-09-09 18:57:25'),
(986, NULL, NULL, 'cuentos', 1, NULL, '2013-09-09 16:57:25', '2013-09-09 18:57:25'),
(987, NULL, NULL, 'Asistente de producción / Asistente de direcc', 1, NULL, '2013-09-09 16:57:25', '2013-09-09 18:57:25'),
(988, NULL, NULL, 'Voice Over', 1, NULL, '2013-09-09 16:57:25', '2013-09-09 18:57:25'),
(989, NULL, NULL, 'Locución', 1, NULL, '2013-09-09 16:57:25', '2013-09-09 18:57:25'),
(990, NULL, NULL, 'E-Learning', 1, NULL, '2013-09-09 16:57:25', '2013-09-09 18:57:25'),
(991, NULL, NULL, 'Diseñadora web', 1, NULL, '2013-09-09 16:57:26', '2013-09-09 18:57:26'),
(992, NULL, NULL, 'jefe de proyecto', 1, NULL, '2013-09-09 16:57:26', '2013-09-09 18:57:26'),
(993, NULL, NULL, 'Desarrollo y Marketing Web', 1, NULL, '2013-09-09 16:57:26', '2013-09-09 18:57:26'),
(994, NULL, NULL, 'Traductora e Intérprete', 1, NULL, '2013-09-09 16:57:26', '2013-09-09 18:57:26'),
(995, NULL, NULL, 'Traductora de español y portugués', 1, NULL, '2013-09-09 16:57:26', '2013-09-09 18:57:26'),
(996, NULL, NULL, 'francés', 1, NULL, '2013-09-09 16:57:26', '2013-09-09 18:57:26'),
(997, NULL, NULL, 'interprete', 1, NULL, '2013-09-09 16:57:26', '2013-09-09 18:57:26'),
(998, NULL, NULL, 'traductor', 1, NULL, '2013-09-09 16:57:26', '2013-09-09 18:57:26'),
(999, NULL, NULL, 'español', 1, NULL, '2013-09-09 16:57:26', '2013-09-09 18:57:26'),
(1000, NULL, NULL, 'Desarrollo', 1, NULL, '2013-09-09 16:57:26', '2013-09-09 18:57:26'),
(1001, NULL, NULL, 'TRADUCTORA', 1, NULL, '2013-09-09 16:57:26', '2013-09-09 18:57:26'),
(1002, NULL, NULL, 'dieñador motion graphics', 1, NULL, '2013-09-09 16:57:27', '2013-09-09 18:57:27'),
(1003, NULL, NULL, 'Tecnico Audiovisual', 1, NULL, '2013-09-09 16:57:27', '2013-09-09 18:57:27'),
(1004, NULL, NULL, 'Realizador', 1, NULL, '2013-09-09 16:57:27', '2013-09-09 18:57:27'),
(1005, NULL, NULL, 'Director', 1, NULL, '2013-09-09 16:57:27', '2013-09-09 18:57:27'),
(1006, NULL, NULL, 'Maquetacion web', 1, NULL, '2013-09-09 16:57:27', '2013-09-09 18:57:27'),
(1007, NULL, NULL, 'television', 1, NULL, '2013-09-09 16:57:27', '2013-09-09 18:57:27'),
(1008, NULL, NULL, 'camarografo', 1, NULL, '2013-09-09 16:57:27', '2013-09-09 18:57:27'),
(1009, NULL, NULL, 'editora', 1, NULL, '2013-09-09 16:57:27', '2013-09-09 18:57:27'),
(1010, NULL, NULL, 'video', 1, NULL, '2013-09-09 16:57:27', '2013-09-09 18:57:27'),
(1011, NULL, NULL, 'Voice Talent', 1, NULL, '2013-09-09 16:57:27', '2013-09-09 18:57:27'),
(1012, NULL, NULL, 'locutor comercial', 1, NULL, '2013-09-09 16:57:27', '2013-09-09 18:57:27'),
(1013, NULL, NULL, 'voice', 1, NULL, '2013-09-09 16:57:27', '2013-09-09 18:57:27'),
(1014, NULL, NULL, 'Locutor', 1, NULL, '2013-09-09 16:57:27', '2013-09-09 18:57:27'),
(1015, NULL, NULL, 'tv', 1, NULL, '2013-09-09 16:57:28', '2013-09-09 18:57:28'),
(1016, NULL, NULL, 'desarrollo web', 1, NULL, '2013-09-09 16:57:28', '2013-09-09 18:57:28'),
(1017, NULL, NULL, 'diseño con joomla', 1, NULL, '2013-09-09 16:57:28', '2013-09-09 18:57:28'),
(1018, NULL, NULL, 'html', 1, NULL, '2013-09-09 16:57:28', '2013-09-09 18:57:28'),
(1019, NULL, NULL, 'css', 1, NULL, '2013-09-09 16:57:28', '2013-09-09 18:57:28'),
(1020, NULL, NULL, 'freelance', 1, NULL, '2013-09-09 16:57:28', '2013-09-09 18:57:28'),
(1021, NULL, NULL, 'ANIMACION 2d', 1, NULL, '2013-09-09 16:57:28', '2013-09-09 18:57:28'),
(1022, NULL, NULL, 'Editor', 1, NULL, '2013-09-09 16:57:28', '2013-09-09 18:57:28'),
(1023, NULL, NULL, 'Corrector', 1, NULL, '2013-09-09 16:57:28', '2013-09-09 18:57:28'),
(1024, NULL, NULL, 'Traducción Inglés - Español', 1, NULL, '2013-09-09 16:57:28', '2013-09-09 18:57:28'),
(1025, NULL, NULL, 'Diseñador Industrial/Grafico', 1, NULL, '2013-09-09 16:57:28', '2013-09-09 18:57:28');

-- --------------------------------------------------------

--
-- Structure de la table `adm_reponses_statuts`
--

CREATE TABLE `adm_reponses_statuts` (
  `id_reponse_statut` int(11) NOT NULL,
  `adm_reponse_statut_fr` varchar(255) NOT NULL,
  `adm_reponse_statut_en` varchar(255) NOT NULL,
  `adm_reponse_statut_es` varchar(255) NOT NULL,
  `actif` int(11) NOT NULL,
  `cree_le` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifie_le` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `adm_reponses_statuts`
--

INSERT INTO `adm_reponses_statuts` (`id_reponse_statut`, `adm_reponse_statut_fr`, `adm_reponse_statut_en`, `adm_reponse_statut_es`, `actif`, `cree_le`, `modifie_le`) VALUES
(1, 'Propositions en étude', 'Currently processing proposals', 'Proposiciones en estudio', 1, '2012-04-13 15:59:35', '2013-02-11 16:17:23'),
(2, 'Propositions retenues', 'Selected proposals', 'Proposiciones retenidas', 1, '2012-04-13 15:59:35', '2013-02-11 16:16:54'),
(3, 'Propositions déclinées', 'Declined proposals', 'Proposiciones declinadas', 1, '2012-04-13 16:00:02', '2013-02-11 16:17:05'),
(4, 'Propositions non-traitées', 'Unread proposals', 'Proposiciones no tratadas', 1, '2012-10-09 09:24:38', '2013-02-11 16:16:14');

-- --------------------------------------------------------

--
-- Structure de la table `adm_structures`
--

CREATE TABLE `adm_structures` (
  `id_structure` int(255) NOT NULL,
  `adm_structure_fr` varchar(45) NOT NULL,
  `adm_structure_es` varchar(45) NOT NULL,
  `adm_structure_en` varchar(45) NOT NULL,
  `actif` tinyint(1) NOT NULL,
  `var` varchar(45) DEFAULT NULL,
  `cree_le` datetime NOT NULL,
  `modifie_le` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `adm_structures`
--

INSERT INTO `adm_structures` (`id_structure`, `adm_structure_fr`, `adm_structure_es`, `adm_structure_en`, `actif`, `var`, `cree_le`, `modifie_le`) VALUES
(1, 'Freelance', 'Freelance', 'Freelancer', 1, NULL, '2012-01-20 00:00:00', '2013-01-29 20:50:52'),
(2, 'Intermittent', 'Técnico de espectáculo', 'Independent worker', 1, NULL, '2011-12-27 11:30:31', '2013-01-29 20:51:09'),
(3, 'Agence', 'Agencia', 'Agency', 1, NULL, '2012-01-20 00:00:00', '2013-01-29 20:51:11');

-- --------------------------------------------------------

--
-- Structure de la table `tra_pages`
--

CREATE TABLE `tra_pages` (
  `id_page` int(11) NOT NULL,
  `titre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `nom` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `environnement` int(11) NOT NULL COMMENT '0 = visiteur; 1 = prestataire; 2 = client; 3 = globales',
  `cree_le` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifie_le` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `tra_pages`
--

INSERT INTO `tra_pages` (`id_page`, `titre`, `nom`, `environnement`, `cree_le`, `modifie_le`) VALUES
(1, 'Visiteur Home ', 'home', 0, '2013-08-22 15:46:48', '2013-08-22 17:40:28'),
(2, 'Visiteur Header', 'header', 0, '2013-08-22 15:46:48', '2013-08-22 17:40:45'),
(3, 'Visiteur Footer', 'footer', 0, '2013-08-22 15:46:48', '2013-08-22 17:40:57'),
(4, 'Inscription', 'inscription', 0, '2013-08-22 15:46:48', '2013-08-22 17:41:11'),
(5, 'Visiteur missions ', 'missions', 0, '2013-08-22 15:46:48', '2013-08-22 17:41:37'),
(6, 'Visiteur CVThèque', 'cvtheque', 0, '2013-08-22 15:46:48', '2013-08-22 17:41:52'),
(7, 'Prestataire Home ', 'home', 1, '2013-08-22 15:46:48', '2013-08-22 17:42:21'),
(8, 'Prestataire Header', 'header', 1, '2013-08-29 11:06:48', '2013-08-29 13:01:53'),
(9, 'Prestataire Footer', 'footer', 1, '2013-08-29 11:06:48', '2013-08-29 13:02:17'),
(10, 'Prestataire Mon Compte', 'edit_moncompte', 1, '2013-08-29 11:22:00', '2013-08-29 13:17:29'),
(11, 'Prestataire Profils', 'edit_cv', 1, '2013-08-29 11:24:31', '2013-08-29 13:19:59'),
(12, 'Prestataire Realisations', 'realisations', 1, '2013-08-29 11:28:11', '2013-08-29 13:23:40'),
(13, 'Prestataire mission', 'missions', 1, '2013-08-29 12:33:01', '2013-08-29 14:28:29'),
(14, 'Prestataire reponses', 'responses', 1, '2013-08-29 12:47:44', '2013-08-29 14:43:13'),
(15, 'Prestataire entretien', 'entretiens', 1, '2013-08-29 13:02:05', '2013-08-29 14:57:35'),
(16, 'Client Home', 'home', 2, '2013-08-29 13:25:34', '2013-08-29 15:21:04'),
(17, 'Client Header', 'header', 2, '2013-08-29 13:36:52', '2013-08-29 15:32:21'),
(18, 'Client Footer', 'footer', 2, '2013-08-29 13:38:38', '2013-08-29 15:34:08'),
(19, 'Client Mon Compte', 'edit_moncompte', 2, '2013-08-29 13:40:33', '2013-08-29 15:36:03'),
(20, 'Client Cvtheque', 'cvtheque', 2, '2013-08-29 13:46:15', '2013-08-29 15:41:45'),
(21, 'Client Missions', 'missions', 2, '2013-08-29 13:48:30', '2013-08-29 15:43:58'),
(22, 'Client Edit Mission', 'edit_mission', 2, '2013-08-29 13:52:41', '2013-07-08 12:16:34'),
(23, 'Client Publier Mission', 'new_mission', 2, '2013-08-29 14:05:54', '2013-05-29 15:55:05'),
(24, 'Client Manager', 'new_manager', 2, '2013-08-29 14:07:45', '2013-08-29 16:03:12'),
(25, 'Client Favoris', 'my_profils', 2, '2013-08-29 14:10:36', '2013-08-29 16:06:05'),
(26, 'Client Entretien', 'new_entretien', 2, '2013-08-29 14:13:07', '2013-08-29 16:08:34'),
(27, 'Client Message Modele', 'new_message', 2, '2013-08-29 14:15:47', '2013-08-29 16:11:17'),
(28, 'Toutes les globales', 'globales', 3, '2013-08-29 15:03:50', '2013-08-29 16:59:04'),
(30, 'Visiteur Services', 'services', 0, '2013-09-04 10:00:45', '0000-00-00 00:00:00'),
(29, 'Visiteur Infos', 'infos', 0, '2013-09-04 10:00:45', '0000-00-00 00:00:00'),
(31, 'Visiteur About', 'about', 0, '2013-09-04 10:00:45', '0000-00-00 00:00:00'),
(32, 'Visiteur Mentions', 'mentions', 0, '2013-09-04 10:00:45', '0000-00-00 00:00:00'),
(33, 'Visiteur Contact', 'contact', 0, '2013-09-04 10:00:45', '0000-00-00 00:00:00'),
(34, 'Prestataire Contact', 'contact', 1, '2013-09-04 10:00:45', '0000-00-00 00:00:00'),
(35, 'Client Contact', 'contact', 2, '2013-09-04 10:00:45', '0000-00-00 00:00:00'),
(36, 'Prestataire Services', 'services', 1, '2013-09-04 10:00:45', '0000-00-00 00:00:00'),
(37, 'Prestataire Infos', 'infos', 1, '2013-09-04 10:00:45', '0000-00-00 00:00:00'),
(38, 'Prestataire About', 'about', 1, '2013-09-04 10:00:45', '0000-00-00 00:00:00'),
(39, 'Prestataire Mentions', 'mentions', 1, '2013-09-04 10:00:45', '0000-00-00 00:00:00'),
(40, 'Client Services', 'services', 2, '2013-09-04 10:00:45', '0000-00-00 00:00:00'),
(41, 'Client Infos', 'infos', 2, '2013-09-04 10:00:45', '0000-00-00 00:00:00'),
(42, 'Client About', 'about', 2, '2013-09-04 10:00:45', '0000-00-00 00:00:00'),
(43, 'Client Mentions', 'mentions', 2, '2013-09-04 10:00:45', '0000-00-00 00:00:00'),
(44, 'Client Message', 'message', 2, '2013-09-10 11:54:21', '2013-09-10 13:49:23');

-- --------------------------------------------------------

--
-- Structure de la table `tra_page_traduction`
--

CREATE TABLE `tra_page_traduction` (
  `id_page_traduction` int(11) NOT NULL,
  `id_page` int(11) NOT NULL,
  `id_traduction` int(11) NOT NULL,
  `cree_le` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifie_le` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `tra_page_traduction`
--

INSERT INTO `tra_page_traduction` (`id_page_traduction`, `id_page`, `id_traduction`, `cree_le`, `modifie_le`) VALUES
(1, 1, 1, '2013-08-30 11:39:14', '2013-08-30 13:39:14'),
(2, 1, 2, '2013-08-30 11:39:14', '2013-08-30 13:39:14'),
(3, 1, 3, '2013-08-30 11:39:14', '2013-08-30 13:39:14'),
(4, 1, 4, '2013-08-30 11:39:14', '2013-08-30 13:39:14'),
(5, 1, 5, '2013-08-30 11:39:14', '2013-08-30 13:39:14'),
(6, 1, 6, '2013-08-30 11:39:14', '2013-08-30 13:39:14'),
(7, 1, 7, '2013-08-30 11:39:14', '2013-08-30 13:39:14'),
(8, 1, 8, '2013-08-30 11:39:14', '2013-08-30 13:39:14'),
(9, 1, 9, '2013-08-30 11:39:14', '2013-08-30 13:39:14'),
(10, 1, 10, '2013-08-30 11:39:14', '2013-08-30 13:39:14'),
(11, 1, 11, '2013-08-30 11:39:14', '2013-08-30 13:39:14'),
(12, 1, 12, '2013-08-30 11:39:14', '2013-08-30 13:39:14'),
(13, 1, 13, '2013-08-30 11:39:14', '2013-08-30 13:39:14'),
(14, 1, 14, '2013-08-30 11:39:14', '2013-08-30 13:39:14'),
(15, 1, 15, '2013-08-30 11:39:14', '2013-08-30 13:39:14'),
(16, 1, 16, '2013-08-30 11:39:14', '2013-08-30 13:39:14'),
(17, 1, 17, '2013-08-30 11:39:14', '2013-08-30 13:39:14'),
(18, 1, 18, '2013-08-30 11:39:14', '2013-08-30 13:39:14'),
(19, 1, 19, '2013-08-30 11:39:14', '2013-08-30 13:39:14'),
(20, 1, 20, '2013-08-30 11:39:14', '2013-08-30 13:39:14'),
(21, 1, 21, '2013-08-30 11:39:14', '2013-08-30 13:39:14'),
(22, 1, 22, '2013-08-30 11:39:14', '2013-08-30 13:39:14'),
(23, 1, 23, '2013-08-30 11:39:14', '2013-08-30 13:39:14'),
(24, 1, 24, '2013-08-30 11:39:14', '2013-08-30 13:39:14'),
(25, 2, 25, '2013-08-30 11:41:25', '2013-08-30 13:41:25'),
(26, 2, 26, '2013-08-30 11:41:25', '2013-08-30 13:41:25'),
(27, 2, 27, '2013-08-30 11:41:25', '2013-08-30 13:41:25'),
(28, 2, 28, '2013-08-30 11:41:25', '2013-08-30 13:41:25'),
(29, 2, 29, '2013-08-30 11:41:25', '2013-08-30 13:41:25'),
(30, 2, 30, '2013-08-30 11:41:25', '2013-08-30 13:41:25'),
(31, 2, 31, '2013-08-30 11:41:25', '2013-08-30 13:41:25'),
(32, 2, 32, '2013-08-30 11:41:25', '2013-08-30 13:41:25'),
(33, 2, 33, '2013-08-30 11:41:25', '2013-08-30 13:41:25'),
(34, 2, 34, '2013-08-30 11:41:25', '2013-08-30 13:41:25'),
(35, 2, 35, '2013-08-30 11:41:25', '2013-08-30 13:41:25'),
(36, 2, 36, '2013-08-30 11:41:25', '2013-08-30 13:41:25'),
(37, 3, 37, '2013-08-30 11:42:06', '2013-08-30 13:42:06'),
(38, 3, 38, '2013-08-30 11:42:06', '2013-08-30 13:42:06'),
(39, 3, 39, '2013-08-30 11:42:06', '2013-08-30 13:42:06'),
(40, 3, 40, '2013-08-30 11:42:06', '2013-08-30 13:42:06'),
(41, 3, 41, '2013-08-30 11:42:06', '2013-08-30 13:42:06'),
(42, 3, 42, '2013-08-30 11:42:06', '2013-08-30 13:42:06'),
(43, 3, 43, '2013-08-30 11:42:06', '2013-08-30 13:42:06'),
(44, 3, 44, '2013-08-30 11:42:06', '2013-08-30 13:42:06'),
(45, 3, 45, '2013-08-30 11:42:06', '2013-08-30 13:42:06'),
(46, 3, 46, '2013-08-30 11:42:06', '2013-08-30 13:42:06'),
(47, 3, 47, '2013-08-30 11:42:06', '2013-08-30 13:42:06'),
(48, 3, 48, '2013-08-30 11:42:06', '2013-08-30 13:42:06'),
(49, 3, 49, '2013-08-30 11:42:06', '2013-08-30 13:42:06'),
(50, 3, 50, '2013-08-30 11:42:06', '2013-08-30 13:42:06'),
(51, 3, 51, '2013-08-30 11:42:06', '2013-08-30 13:42:06'),
(52, 3, 52, '2013-08-30 11:42:06', '2013-08-30 13:42:06'),
(53, 3, 53, '2013-08-30 11:42:06', '2013-08-30 13:42:06'),
(54, 4, 54, '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(55, 4, 55, '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(56, 4, 56, '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(57, 4, 57, '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(58, 4, 58, '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(59, 4, 59, '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(60, 4, 60, '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(61, 4, 61, '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(62, 4, 62, '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(63, 4, 63, '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(64, 4, 64, '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(65, 4, 65, '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(66, 4, 66, '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(67, 4, 67, '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(68, 4, 68, '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(69, 4, 69, '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(70, 4, 70, '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(71, 4, 71, '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(72, 4, 72, '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(73, 4, 73, '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(74, 4, 74, '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(75, 4, 75, '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(76, 4, 76, '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(77, 4, 77, '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(78, 4, 78, '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(79, 4, 79, '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(80, 4, 80, '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(81, 4, 81, '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(82, 4, 82, '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(83, 4, 83, '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(84, 4, 84, '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(85, 4, 85, '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(86, 4, 86, '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(87, 4, 87, '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(88, 4, 88, '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(89, 4, 89, '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(90, 4, 90, '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(91, 4, 91, '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(92, 4, 92, '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(93, 4, 93, '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(94, 5, 94, '2013-08-30 11:42:27', '2013-08-30 13:42:27'),
(95, 5, 95, '2013-08-30 11:42:27', '2013-08-30 13:42:27'),
(96, 5, 96, '2013-08-30 11:42:27', '2013-08-30 13:42:27'),
(97, 5, 97, '2013-08-30 11:42:27', '2013-08-30 13:42:27'),
(98, 7, 98, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(99, 7, 99, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(100, 7, 100, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(101, 7, 101, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(102, 7, 102, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(103, 7, 103, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(104, 7, 104, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(105, 7, 105, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(106, 7, 106, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(107, 7, 107, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(108, 7, 108, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(109, 7, 109, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(110, 7, 110, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(111, 7, 111, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(112, 7, 112, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(113, 7, 113, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(114, 7, 114, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(115, 7, 115, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(116, 7, 116, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(117, 7, 117, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(118, 7, 118, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(119, 7, 119, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(120, 7, 120, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(121, 7, 121, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(122, 7, 122, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(123, 7, 123, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(124, 7, 124, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(125, 7, 125, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(126, 7, 126, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(127, 7, 127, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(128, 7, 128, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(129, 7, 129, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(130, 7, 130, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(131, 7, 131, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(132, 7, 132, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(133, 7, 133, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(134, 7, 134, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(135, 7, 135, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(136, 7, 136, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(137, 7, 137, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(138, 7, 138, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(139, 7, 139, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(140, 7, 140, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(141, 7, 141, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(142, 7, 142, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(143, 7, 143, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(144, 7, 144, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(145, 7, 145, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(146, 7, 146, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(147, 7, 147, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(148, 7, 148, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(149, 7, 149, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(150, 7, 150, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(151, 7, 151, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(152, 7, 152, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(153, 7, 153, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(154, 7, 154, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(155, 7, 155, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(156, 7, 156, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(157, 7, 157, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(158, 7, 158, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(159, 7, 159, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(160, 7, 160, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(161, 7, 161, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(162, 7, 162, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(163, 7, 163, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(164, 7, 164, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(165, 7, 165, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(166, 7, 166, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(167, 7, 167, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(168, 7, 168, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(169, 7, 169, '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(170, 8, 170, '2013-08-30 11:43:33', '2013-08-30 13:43:33'),
(171, 8, 171, '2013-08-30 11:43:33', '2013-08-30 13:43:33'),
(172, 8, 172, '2013-08-30 11:43:33', '2013-08-30 13:43:33'),
(173, 8, 173, '2013-08-30 11:43:33', '2013-08-30 13:43:33'),
(174, 8, 174, '2013-08-30 11:43:33', '2013-08-30 13:43:33'),
(175, 8, 175, '2013-08-30 11:43:33', '2013-08-30 13:43:33'),
(176, 8, 176, '2013-08-30 11:43:33', '2013-08-30 13:43:33'),
(177, 8, 177, '2013-08-30 11:43:33', '2013-08-30 13:43:33'),
(178, 8, 178, '2013-08-30 11:43:33', '2013-08-30 13:43:33'),
(179, 8, 179, '2013-08-30 11:43:33', '2013-08-30 13:43:33'),
(180, 8, 180, '2013-08-30 11:43:33', '2013-08-30 13:43:33'),
(181, 8, 181, '2013-08-30 11:43:33', '2013-08-30 13:43:33'),
(182, 8, 182, '2013-08-30 11:43:33', '2013-08-30 13:43:33'),
(183, 8, 183, '2013-08-30 11:43:33', '2013-08-30 13:43:33'),
(184, 8, 184, '2013-08-30 11:43:33', '2013-08-30 13:43:33'),
(185, 8, 185, '2013-08-30 11:43:33', '2013-08-30 13:43:33'),
(186, 8, 186, '2013-08-30 11:43:33', '2013-08-30 13:43:33'),
(187, 8, 187, '2013-08-30 11:43:33', '2013-08-30 13:43:33'),
(188, 8, 188, '2013-08-30 11:43:33', '2013-08-30 13:43:33'),
(189, 8, 189, '2013-08-30 11:43:33', '2013-08-30 13:43:33'),
(190, 8, 190, '2013-08-30 11:43:33', '2013-08-30 13:43:33'),
(191, 8, 191, '2013-08-30 11:43:33', '2013-08-30 13:43:33'),
(192, 8, 192, '2013-08-30 11:43:33', '2013-08-30 13:43:33'),
(193, 8, 193, '2013-08-30 11:43:33', '2013-08-30 13:43:33'),
(194, 8, 194, '2013-08-30 11:43:33', '2013-08-30 13:43:33'),
(195, 8, 195, '2013-08-30 11:43:33', '2013-08-30 13:43:33'),
(196, 9, 196, '2013-08-30 11:43:44', '2013-08-30 13:43:44'),
(197, 9, 197, '2013-08-30 11:43:44', '2013-08-30 13:43:44'),
(198, 9, 198, '2013-08-30 11:43:44', '2013-08-30 13:43:44'),
(199, 9, 199, '2013-08-30 11:43:44', '2013-08-30 13:43:44'),
(200, 9, 200, '2013-08-30 11:43:44', '2013-08-30 13:43:44'),
(201, 9, 201, '2013-08-30 11:43:44', '2013-08-30 13:43:44'),
(202, 9, 202, '2013-08-30 11:43:44', '2013-08-30 13:43:44'),
(203, 9, 203, '2013-08-30 11:43:44', '2013-08-30 13:43:44'),
(204, 9, 204, '2013-08-30 11:43:44', '2013-08-30 13:43:44'),
(205, 9, 205, '2013-08-30 11:43:44', '2013-08-30 13:43:44'),
(206, 9, 206, '2013-08-30 11:43:44', '2013-08-30 13:43:44'),
(207, 9, 207, '2013-08-30 11:43:44', '2013-08-30 13:43:44'),
(208, 9, 208, '2013-08-30 11:43:44', '2013-08-30 13:43:44'),
(209, 9, 209, '2013-08-30 11:43:44', '2013-08-30 13:43:44'),
(210, 9, 210, '2013-08-30 11:43:44', '2013-08-30 13:43:44'),
(211, 9, 211, '2013-08-30 11:43:44', '2013-08-30 13:43:44'),
(212, 9, 212, '2013-08-30 11:43:44', '2013-08-30 13:43:44'),
(213, 10, 213, '2013-08-30 11:43:57', '2013-08-30 13:43:57'),
(214, 10, 214, '2013-08-30 11:43:57', '2013-08-30 13:43:57'),
(215, 10, 215, '2013-08-30 11:43:57', '2013-08-30 13:43:57'),
(216, 10, 216, '2013-08-30 11:43:57', '2013-08-30 13:43:57'),
(217, 10, 217, '2013-08-30 11:43:57', '2013-08-30 13:43:57'),
(218, 10, 218, '2013-08-30 11:43:57', '2013-08-30 13:43:57'),
(219, 10, 219, '2013-08-30 11:43:57', '2013-08-30 13:43:57'),
(220, 10, 220, '2013-08-30 11:43:57', '2013-08-30 13:43:57'),
(221, 10, 221, '2013-08-30 11:43:57', '2013-08-30 13:43:57'),
(222, 10, 222, '2013-08-30 11:43:57', '2013-08-30 13:43:57'),
(223, 10, 223, '2013-08-30 11:43:57', '2013-08-30 13:43:57'),
(224, 10, 224, '2013-08-30 11:43:57', '2013-08-30 13:43:57'),
(225, 10, 225, '2013-08-30 11:43:57', '2013-08-30 13:43:57'),
(226, 10, 226, '2013-08-30 11:43:57', '2013-08-30 13:43:57'),
(227, 11, 227, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(228, 11, 228, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(229, 11, 229, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(230, 11, 230, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(231, 11, 231, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(232, 11, 232, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(233, 11, 233, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(234, 11, 234, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(235, 11, 235, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(236, 11, 236, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(237, 11, 237, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(238, 11, 238, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(239, 11, 239, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(240, 11, 240, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(241, 11, 241, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(242, 11, 242, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(243, 11, 243, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(244, 11, 244, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(245, 11, 245, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(246, 11, 246, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(247, 11, 247, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(248, 11, 248, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(249, 11, 249, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(250, 11, 250, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(251, 11, 251, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(252, 11, 252, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(253, 11, 253, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(254, 11, 254, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(255, 11, 255, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(256, 11, 256, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(257, 11, 257, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(258, 11, 258, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(259, 11, 259, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(260, 11, 260, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(261, 11, 261, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(262, 11, 262, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(263, 11, 263, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(264, 11, 264, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(265, 11, 265, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(266, 11, 266, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(267, 11, 267, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(268, 11, 268, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(269, 11, 269, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(270, 11, 270, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(271, 11, 271, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(272, 11, 272, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(273, 11, 273, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(274, 11, 274, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(275, 11, 275, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(276, 11, 276, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(277, 11, 277, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(278, 11, 278, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(279, 11, 279, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(280, 11, 280, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(281, 11, 281, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(282, 11, 282, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(283, 11, 283, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(284, 11, 284, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(285, 11, 285, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(286, 11, 286, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(287, 11, 287, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(288, 11, 288, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(289, 11, 289, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(290, 11, 290, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(291, 11, 291, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(292, 11, 292, '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(293, 12, 293, '2013-08-30 11:44:22', '2013-08-30 13:44:22'),
(294, 12, 294, '2013-08-30 11:44:22', '2013-08-30 13:44:22'),
(295, 12, 295, '2013-08-30 11:44:22', '2013-08-30 13:44:22'),
(296, 12, 296, '2013-08-30 11:44:22', '2013-08-30 13:44:22'),
(297, 12, 297, '2013-08-30 11:44:22', '2013-08-30 13:44:22'),
(298, 12, 298, '2013-08-30 11:44:22', '2013-08-30 13:44:22'),
(299, 12, 299, '2013-08-30 11:44:22', '2013-08-30 13:44:22'),
(300, 12, 300, '2013-08-30 11:44:22', '2013-08-30 13:44:22'),
(301, 12, 301, '2013-08-30 11:44:22', '2013-08-30 13:44:22'),
(302, 13, 302, '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(303, 13, 303, '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(304, 13, 304, '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(305, 13, 305, '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(306, 13, 306, '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(307, 13, 307, '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(308, 13, 308, '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(309, 13, 309, '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(310, 13, 310, '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(311, 13, 311, '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(312, 13, 312, '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(313, 13, 313, '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(314, 13, 314, '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(315, 13, 315, '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(316, 13, 316, '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(317, 13, 317, '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(318, 13, 318, '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(319, 13, 319, '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(320, 13, 320, '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(321, 13, 321, '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(322, 13, 322, '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(323, 13, 323, '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(324, 13, 324, '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(325, 13, 325, '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(326, 13, 326, '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(327, 13, 327, '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(328, 13, 328, '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(329, 13, 329, '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(330, 13, 330, '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(331, 13, 331, '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(332, 14, 332, '2013-08-30 11:53:17', '2013-08-30 13:53:17'),
(333, 14, 333, '2013-08-30 11:53:17', '2013-08-30 13:53:17'),
(334, 14, 334, '2013-08-30 11:53:17', '2013-08-30 13:53:17'),
(335, 14, 335, '2013-08-30 11:53:17', '2013-08-30 13:53:17'),
(336, 15, 336, '2013-08-30 11:55:28', '2013-08-30 13:55:28'),
(337, 15, 337, '2013-08-30 11:55:28', '2013-08-30 13:55:28'),
(338, 15, 338, '2013-08-30 11:55:28', '2013-08-30 13:55:28'),
(339, 15, 339, '2013-08-30 11:55:28', '2013-08-30 13:55:28'),
(340, 15, 340, '2013-08-30 11:55:28', '2013-08-30 13:55:28'),
(341, 15, 341, '2013-08-30 11:55:28', '2013-08-30 13:55:28'),
(342, 15, 342, '2013-08-30 11:55:28', '2013-08-30 13:55:28'),
(343, 15, 343, '2013-08-30 11:55:28', '2013-08-30 13:55:28'),
(344, 16, 344, '2013-08-30 11:56:06', '2013-08-30 13:56:06'),
(345, 16, 345, '2013-08-30 11:56:06', '2013-08-30 13:56:06'),
(346, 16, 346, '2013-08-30 11:56:06', '2013-08-30 13:56:06'),
(347, 16, 347, '2013-08-30 11:56:06', '2013-08-30 13:56:06'),
(348, 16, 348, '2013-08-30 11:56:06', '2013-08-30 13:56:06'),
(349, 16, 349, '2013-08-30 11:56:06', '2013-08-30 13:56:06'),
(350, 16, 350, '2013-08-30 11:56:06', '2013-08-30 13:56:06'),
(351, 16, 351, '2013-08-30 11:56:06', '2013-08-30 13:56:06'),
(352, 16, 352, '2013-08-30 11:56:06', '2013-08-30 13:56:06'),
(353, 16, 353, '2013-08-30 11:56:06', '2013-08-30 13:56:06'),
(354, 16, 354, '2013-08-30 11:56:06', '2013-08-30 13:56:06'),
(355, 16, 355, '2013-08-30 11:56:06', '2013-08-30 13:56:06'),
(356, 16, 356, '2013-08-30 11:56:06', '2013-08-30 13:56:06'),
(357, 16, 357, '2013-08-30 11:56:06', '2013-08-30 13:56:06'),
(358, 16, 358, '2013-08-30 11:56:06', '2013-08-30 13:56:06'),
(359, 16, 359, '2013-08-30 11:56:06', '2013-08-30 13:56:06'),
(360, 17, 360, '2013-08-30 11:56:29', '2013-08-30 13:56:29'),
(361, 17, 361, '2013-08-30 11:56:29', '2013-08-30 13:56:29'),
(362, 17, 362, '2013-08-30 11:56:29', '2013-08-30 13:56:29'),
(363, 17, 363, '2013-08-30 11:56:29', '2013-08-30 13:56:29'),
(364, 17, 364, '2013-08-30 11:56:29', '2013-08-30 13:56:29'),
(365, 17, 365, '2013-08-30 11:56:29', '2013-08-30 13:56:29'),
(366, 17, 366, '2013-08-30 11:56:29', '2013-08-30 13:56:29'),
(367, 17, 367, '2013-08-30 11:56:29', '2013-08-30 13:56:29'),
(368, 17, 368, '2013-08-30 11:56:29', '2013-08-30 13:56:29'),
(369, 17, 369, '2013-08-30 11:56:29', '2013-08-30 13:56:29'),
(370, 17, 370, '2013-08-30 11:56:29', '2013-08-30 13:56:29'),
(371, 18, 371, '2013-08-30 11:57:50', '2013-08-30 13:57:50'),
(372, 18, 372, '2013-08-30 11:57:50', '2013-08-30 13:57:50'),
(373, 18, 373, '2013-08-30 11:57:50', '2013-08-30 13:57:50'),
(374, 18, 374, '2013-08-30 11:57:50', '2013-08-30 13:57:50'),
(375, 18, 375, '2013-08-30 11:57:50', '2013-08-30 13:57:50'),
(376, 18, 376, '2013-08-30 11:57:50', '2013-08-30 13:57:50'),
(377, 18, 377, '2013-08-30 11:57:50', '2013-08-30 13:57:50'),
(378, 18, 378, '2013-08-30 11:57:50', '2013-08-30 13:57:50'),
(379, 18, 379, '2013-08-30 11:57:50', '2013-08-30 13:57:50'),
(380, 18, 380, '2013-08-30 11:57:50', '2013-08-30 13:57:50'),
(381, 18, 381, '2013-08-30 11:57:50', '2013-08-30 13:57:50'),
(382, 18, 382, '2013-08-30 11:57:50', '2013-08-30 13:57:50'),
(383, 18, 383, '2013-08-30 11:57:50', '2013-08-30 13:57:50'),
(384, 18, 384, '2013-08-30 11:57:50', '2013-08-30 13:57:50'),
(385, 18, 385, '2013-08-30 11:57:50', '2013-08-30 13:57:50'),
(386, 18, 386, '2013-08-30 11:57:50', '2013-08-30 13:57:50'),
(387, 18, 387, '2013-08-30 11:57:50', '2013-08-30 13:57:50'),
(388, 19, 388, '2013-08-30 11:58:36', '2013-08-30 13:58:36'),
(389, 19, 389, '2013-08-30 11:58:36', '2013-08-30 13:58:36'),
(390, 19, 390, '2013-08-30 11:58:36', '2013-08-30 13:58:36'),
(391, 19, 391, '2013-08-30 11:58:36', '2013-08-30 13:58:36'),
(392, 19, 392, '2013-08-30 11:58:36', '2013-08-30 13:58:36'),
(393, 19, 393, '2013-08-30 11:58:36', '2013-08-30 13:58:36'),
(394, 19, 394, '2013-08-30 11:58:36', '2013-08-30 13:58:36'),
(395, 19, 395, '2013-08-30 11:58:36', '2013-08-30 13:58:36'),
(396, 19, 396, '2013-08-30 11:58:36', '2013-08-30 13:58:36'),
(397, 19, 397, '2013-08-30 11:58:36', '2013-08-30 13:58:36'),
(398, 19, 398, '2013-08-30 11:58:36', '2013-08-30 13:58:36'),
(399, 19, 399, '2013-08-30 11:58:36', '2013-08-30 13:58:36'),
(400, 19, 400, '2013-08-30 11:58:36', '2013-08-30 13:58:36'),
(401, 19, 401, '2013-08-30 11:58:36', '2013-08-30 13:58:36'),
(402, 19, 402, '2013-08-30 11:58:36', '2013-08-30 13:58:36'),
(403, 19, 403, '2013-08-30 11:58:36', '2013-08-30 13:58:36'),
(404, 21, 404, '2013-08-30 12:02:49', '2013-08-30 14:02:49'),
(405, 21, 405, '2013-08-30 12:02:49', '2013-08-30 14:02:49'),
(406, 22, 406, '2013-08-30 12:04:38', '2013-08-30 14:04:38'),
(407, 23, 407, '2013-08-30 12:04:57', '2013-08-30 14:04:57'),
(408, 23, 408, '2013-08-30 12:04:57', '2013-08-30 14:04:57'),
(409, 23, 409, '2013-08-30 12:04:57', '2013-08-30 14:04:57'),
(410, 23, 410, '2013-08-30 12:04:57', '2013-08-30 14:04:57'),
(411, 23, 411, '2013-08-30 12:04:57', '2013-08-30 14:04:57'),
(412, 23, 412, '2013-08-30 12:04:57', '2013-08-30 14:04:57'),
(413, 24, 413, '2013-08-30 12:05:15', '2013-08-30 14:05:15'),
(414, 24, 414, '2013-08-30 12:05:15', '2013-08-30 14:05:15'),
(415, 24, 415, '2013-08-30 12:05:15', '2013-08-30 14:05:15'),
(416, 26, 416, '2013-08-30 12:05:56', '2013-08-30 14:05:56'),
(417, 26, 417, '2013-08-30 12:05:56', '2013-08-30 14:05:56'),
(418, 26, 418, '2013-08-30 12:05:56', '2013-08-30 14:05:56'),
(419, 26, 419, '2013-08-30 12:05:56', '2013-08-30 14:05:56'),
(420, 26, 420, '2013-08-30 12:05:56', '2013-08-30 14:05:56'),
(421, 26, 421, '2013-08-30 12:05:56', '2013-08-30 14:05:56'),
(422, 26, 422, '2013-08-30 12:05:56', '2013-08-30 14:05:56'),
(423, 26, 423, '2013-08-30 12:05:56', '2013-08-30 14:05:56'),
(424, 26, 424, '2013-08-30 12:05:56', '2013-08-30 14:05:56'),
(425, 27, 425, '2013-08-30 12:06:15', '2013-08-30 14:06:15'),
(426, 27, 426, '2013-08-30 12:06:15', '2013-08-30 14:06:15'),
(427, 27, 427, '2013-08-30 12:06:15', '2013-08-30 14:06:15'),
(428, 27, 428, '2013-08-30 12:06:15', '2013-08-30 14:06:15'),
(429, 27, 429, '2013-08-30 12:06:15', '2013-08-30 14:06:15'),
(430, 28, 430, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(431, 28, 431, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(432, 28, 432, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(433, 28, 433, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(434, 28, 434, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(435, 28, 435, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(436, 28, 436, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(437, 28, 437, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(438, 28, 438, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(439, 28, 439, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(440, 28, 440, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(441, 28, 441, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(442, 28, 442, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(443, 28, 443, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(444, 28, 444, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(445, 28, 445, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(446, 28, 446, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(447, 28, 447, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(448, 28, 448, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(449, 28, 449, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(450, 28, 450, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(451, 28, 451, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(452, 28, 452, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(453, 28, 453, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(454, 28, 454, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(455, 28, 455, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(456, 28, 456, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(457, 28, 457, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(458, 28, 458, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(459, 28, 459, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(460, 28, 460, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(461, 28, 461, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(462, 28, 462, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(463, 28, 463, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(464, 28, 464, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(465, 28, 465, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(466, 28, 466, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(467, 28, 467, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(468, 28, 468, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(469, 28, 469, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(470, 28, 470, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(471, 28, 471, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(472, 28, 472, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(473, 28, 473, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(474, 28, 474, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(475, 28, 475, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(476, 28, 476, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(477, 28, 477, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(478, 28, 478, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(479, 28, 479, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(480, 28, 480, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(481, 28, 481, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(482, 28, 482, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(483, 28, 483, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(484, 28, 484, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(485, 28, 485, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(486, 28, 486, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(487, 28, 487, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(488, 28, 488, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(489, 28, 489, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(490, 28, 490, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(491, 28, 491, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(492, 28, 492, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(493, 28, 493, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(494, 28, 494, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(495, 28, 495, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(496, 28, 496, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(497, 28, 497, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(498, 28, 498, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(499, 28, 499, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(500, 28, 500, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(501, 28, 501, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(502, 28, 502, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(503, 28, 503, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(504, 28, 504, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(505, 28, 505, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(506, 28, 506, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(507, 28, 507, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(508, 28, 508, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(509, 28, 509, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(510, 28, 510, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(511, 28, 511, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(512, 28, 512, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(513, 28, 513, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(514, 28, 514, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(515, 28, 515, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(516, 28, 516, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(517, 28, 517, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(518, 28, 518, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(519, 28, 519, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(520, 28, 520, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(521, 28, 521, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(522, 28, 522, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(523, 28, 523, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(524, 28, 524, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(525, 28, 525, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(526, 28, 526, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(527, 28, 527, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(528, 28, 528, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(529, 28, 529, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(530, 28, 530, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(531, 28, 531, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(532, 28, 532, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(533, 28, 533, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(534, 28, 534, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(535, 28, 535, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(536, 28, 536, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(537, 28, 537, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(538, 28, 538, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(539, 28, 539, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(540, 28, 540, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(541, 28, 541, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(542, 28, 542, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(543, 28, 543, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(544, 28, 544, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(545, 28, 545, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(546, 28, 546, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(547, 28, 547, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(548, 28, 548, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(549, 28, 549, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(550, 28, 550, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(551, 28, 551, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(552, 28, 552, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(553, 28, 553, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(554, 28, 554, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(555, 28, 555, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(556, 28, 556, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(557, 28, 557, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(558, 28, 558, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(559, 28, 559, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(560, 28, 560, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(561, 28, 561, '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(562, 28, 562, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(563, 28, 563, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(564, 28, 564, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(565, 28, 565, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(566, 28, 566, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(567, 28, 567, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(568, 28, 568, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(569, 28, 569, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(570, 28, 570, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(571, 28, 571, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(572, 28, 572, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(573, 28, 573, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(574, 28, 574, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(575, 28, 575, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(576, 28, 576, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(577, 28, 577, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(578, 28, 578, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(579, 28, 579, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(580, 28, 580, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(581, 28, 581, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(582, 28, 582, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(583, 28, 583, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(584, 28, 584, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(585, 28, 585, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(586, 28, 586, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(587, 28, 587, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(588, 28, 588, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(589, 28, 589, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(590, 28, 590, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(591, 28, 591, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(592, 28, 592, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(593, 28, 593, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(594, 28, 594, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(595, 28, 595, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(596, 28, 596, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(597, 28, 597, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(598, 28, 598, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(599, 28, 599, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(600, 28, 600, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(601, 28, 601, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(602, 28, 602, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(603, 28, 603, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(604, 28, 604, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(605, 28, 605, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(606, 28, 606, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(607, 28, 607, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(608, 28, 608, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(609, 28, 609, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(610, 28, 610, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(611, 28, 611, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(612, 28, 612, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(613, 28, 613, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(614, 28, 614, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(615, 28, 615, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(616, 28, 616, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(617, 28, 617, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(618, 28, 618, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(619, 28, 619, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(620, 28, 620, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(621, 28, 621, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(622, 28, 622, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(623, 28, 623, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(624, 28, 624, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(625, 28, 625, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(626, 28, 626, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(627, 28, 627, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(628, 28, 628, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(629, 28, 629, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(630, 28, 630, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(631, 28, 631, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(632, 28, 632, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(633, 28, 633, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(634, 28, 634, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(635, 28, 635, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(636, 28, 636, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(637, 28, 637, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(638, 28, 638, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(639, 28, 639, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(640, 28, 640, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(641, 28, 641, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(642, 28, 642, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(643, 28, 643, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(644, 28, 644, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(645, 28, 645, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(646, 28, 646, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(647, 28, 647, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(648, 28, 648, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(649, 28, 649, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(650, 28, 650, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(651, 28, 651, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(652, 28, 652, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(653, 28, 653, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(654, 28, 654, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(655, 28, 655, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(656, 28, 656, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(657, 28, 657, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(658, 28, 658, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(659, 28, 659, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(660, 28, 660, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(661, 28, 661, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(662, 28, 662, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(663, 28, 663, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(664, 28, 664, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(665, 28, 665, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(666, 28, 666, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(667, 28, 667, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(668, 28, 668, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(669, 28, 669, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(670, 28, 670, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(671, 28, 671, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(672, 28, 672, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(673, 28, 673, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(674, 28, 674, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(675, 28, 675, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(676, 28, 676, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(677, 28, 677, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(678, 28, 678, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(679, 28, 679, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(680, 28, 680, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(681, 28, 681, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(682, 28, 682, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(683, 28, 683, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(684, 28, 684, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(685, 28, 685, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(686, 28, 686, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(687, 28, 687, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(688, 28, 688, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(689, 28, 689, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(690, 28, 690, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(691, 28, 691, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(692, 28, 692, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(693, 28, 693, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(694, 28, 694, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(695, 28, 695, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(696, 28, 696, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(697, 28, 697, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(698, 28, 698, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(699, 28, 699, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(700, 28, 700, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(701, 28, 701, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(702, 28, 702, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(703, 28, 703, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(704, 28, 704, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(705, 28, 705, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(706, 28, 706, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(707, 28, 707, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(708, 28, 708, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(709, 28, 709, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(710, 28, 710, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(711, 28, 711, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(712, 28, 712, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(713, 28, 713, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(714, 28, 714, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(715, 28, 715, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(716, 28, 716, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(717, 28, 717, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(718, 28, 718, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(719, 28, 719, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(720, 28, 720, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(721, 28, 721, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(722, 28, 722, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(723, 28, 723, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(724, 28, 724, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(725, 28, 725, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(726, 28, 726, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(727, 28, 727, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(728, 28, 728, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(729, 28, 729, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(730, 28, 730, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(731, 28, 731, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(732, 28, 732, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(733, 28, 733, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(734, 28, 734, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(735, 28, 735, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(736, 28, 736, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(737, 28, 737, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(738, 28, 738, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(739, 28, 739, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(740, 28, 740, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(741, 28, 741, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(742, 28, 742, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(743, 28, 743, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(744, 28, 744, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(745, 28, 745, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(746, 28, 746, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(747, 28, 747, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(748, 28, 748, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(749, 28, 749, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(750, 28, 750, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(751, 28, 751, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(752, 28, 752, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(753, 28, 753, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(754, 28, 754, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(755, 28, 755, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(756, 28, 756, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(757, 28, 757, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(758, 28, 758, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(759, 28, 759, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(760, 28, 760, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(761, 28, 761, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(762, 28, 762, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(763, 28, 763, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(764, 28, 764, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(765, 28, 765, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(766, 28, 766, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(767, 28, 767, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(768, 28, 768, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(769, 28, 769, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(770, 28, 770, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(771, 28, 771, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(772, 28, 772, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(773, 28, 773, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(774, 28, 774, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(775, 28, 775, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(776, 28, 776, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(777, 28, 777, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(778, 28, 778, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(779, 28, 779, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(780, 28, 780, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(781, 28, 781, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(782, 28, 782, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(783, 28, 783, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(784, 28, 784, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(785, 28, 785, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(786, 28, 786, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(787, 28, 787, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(788, 28, 788, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(789, 28, 789, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(790, 28, 790, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(791, 28, 791, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(792, 28, 792, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(793, 28, 793, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(794, 28, 794, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(795, 28, 795, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(796, 28, 796, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(797, 28, 797, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(798, 28, 798, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(799, 28, 799, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(800, 28, 800, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(801, 28, 801, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(802, 28, 802, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(803, 28, 803, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(804, 28, 804, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(805, 28, 805, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(806, 28, 806, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(807, 28, 807, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(808, 28, 808, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(809, 28, 809, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(810, 28, 810, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(811, 28, 811, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(812, 28, 812, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(813, 28, 813, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(814, 28, 814, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(815, 28, 815, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(816, 28, 816, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(817, 28, 817, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(818, 28, 818, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(819, 28, 819, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(820, 28, 820, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(821, 28, 821, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(822, 28, 822, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(823, 28, 823, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(824, 28, 824, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(825, 28, 825, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(826, 28, 826, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(827, 28, 827, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(828, 28, 828, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(829, 28, 829, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(830, 28, 830, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(831, 28, 831, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(832, 28, 832, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(833, 28, 833, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(834, 28, 834, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(835, 28, 835, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(836, 28, 836, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(837, 28, 837, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(838, 28, 838, '2013-08-30 12:14:05', '2013-08-30 14:14:05');
INSERT INTO `tra_page_traduction` (`id_page_traduction`, `id_page`, `id_traduction`, `cree_le`, `modifie_le`) VALUES
(839, 28, 839, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(840, 28, 840, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(841, 28, 841, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(842, 28, 842, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(843, 28, 843, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(844, 28, 844, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(845, 28, 845, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(846, 28, 846, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(847, 28, 847, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(848, 28, 848, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(849, 28, 849, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(850, 28, 850, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(851, 28, 851, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(852, 28, 852, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(853, 28, 853, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(854, 28, 854, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(855, 28, 855, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(856, 28, 856, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(857, 28, 857, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(858, 28, 858, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(859, 28, 859, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(860, 28, 860, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(861, 28, 861, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(862, 28, 862, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(863, 28, 863, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(864, 28, 864, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(865, 28, 865, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(866, 28, 866, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(867, 28, 867, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(868, 28, 868, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(869, 28, 869, '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(870, 28, 870, '2013-08-30 12:14:05', '2013-08-30 14:14:05');

-- --------------------------------------------------------

--
-- Structure de la table `tra_traductions`
--

CREATE TABLE `tra_traductions` (
  `id_traduction` int(11) NOT NULL,
  `libelle` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `tra_fr` text COLLATE utf8_unicode_ci NOT NULL,
  `tra_en` text COLLATE utf8_unicode_ci NOT NULL,
  `tra_es` text COLLATE utf8_unicode_ci NOT NULL,
  `trad_cle_unique` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'format : libelle-P''id_page''',
  `cree_le` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifie_le` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `tra_traductions`
--

INSERT INTO `tra_traductions` (`id_traduction`, `libelle`, `tra_fr`, `tra_en`, `tra_es`, `trad_cle_unique`, `cree_le`, `modifie_le`) VALUES
(1, 'hpBienvenueTitre', 'Bienvenue sur WeMe', 'Come and enjoy, NeoFreelo is online ! ', '¡ <span itemprop="name">NeoFreelo</span> ya está en la nube !', 'hpBienvenueTitre-P1', '2013-08-30 11:39:14', '2013-08-30 13:39:14'),
(2, 'hpBienvenueP2', 'Le site de rencontre des entreprises à la recherche de compétences<br/>\r\net des prestataires à la recherche de projets dans les domaines <br/>\r\ndu marketing, de la communication, du web et du multimédia.', '<h2>Your new website with no commission on your projects !</h2>\r\n<br/>The website where Contractors looking for skills get in touch with <br/>Freelancers looking for appropriate projects within South America. <br/>\r\nYou will find here new projects and challenges whether it is in the fields of marketing, communication, multimedia, graphic design, IT, finance, events, etc.', '<h2>Tu nueva <span itemprop="keywords">plataforma</span> <span itemprop="keywords">sin comisión</span> sobre los <span itemprop="keywords">proyectos</span> realizados</h2>\r\n<br/><span itemscope itemtype="http://schema.org/Country">La plataforma web <span itemprop="name">argentina</span></span> diseñada especialmente para <span itemprop="keywords">optimizar</span> el <span itemprop="keywords">encuentro</span> <span itemprop="keywords">profesional</span> entre <span itemprop="keywords">empresas</span> y <span itemprop="keywords">freelances</span> en <span itemscope itemtype="http://schema.org/Continent"><span itemprop="name">América Latina</span>.</span> <br/><span itemscope itemtype="http://schema.org/Specialty">Acá encontrarás nuevos proyectos y desafíos en todos los sectores que sean de <span itemprop="name"> marketing</span>, <span itemprop="name">comunicación</span>, <span itemprop="name">multimedia</span>, <span itemprop="name">diseño</span>, <span itemprop="name">web</span>, <span itemprop="name">nuevas tecnologías</span>, <span itemprop="name">finanzas, eventos</span>, etc.</span>', 'hpBienvenueP2-P1', '2013-08-30 11:39:14', '2013-08-30 13:39:14'),
(3, 'hpRecherche', 'Recherche WeMe', 'Search ', 'Búsqueda', 'hpRecherche-P1', '2013-08-30 11:39:14', '2013-08-30 13:39:14'),
(4, 'hpClientTitre', 'BESOIN D''UN PRESTATAIRE', 'FREELANCER WANTED', 'ENCONTRA UN FREELANCE', 'hpClientTitre-P1', '2013-08-30 11:39:14', '2013-08-30 13:39:14'),
(5, 'hpClientP1', 'Pour trouver simplement et rapidement le prestataire <br/>\r\ndont vous avez besoin : <br/>', 'Looking for the right freelancer ? <br/>\r\nFind here your suitable one <br/>in a simple and quick manner : \r\n<br/>', 'Para encontrar <br/>fácil y rápidamente <br/>el freelance\r\nque necesitás : <br/>', 'hpClientP1-P1', '2013-08-30 11:39:14', '2013-08-30 13:39:14'),
(6, 'hpClientP2', 'Explorez notre Cvthèque.', 'Explore our CV library.', 'Explorá nuestra CVteca.', 'hpClientP2-P1', '2013-08-30 11:39:14', '2013-08-30 13:39:14'),
(7, 'hpPrestaTitre', 'RECHERCHEZ UN PROJET', 'SEARCH FOR A PROJECT', 'ENCONTRA UN PROYECTO', 'hpPrestaTitre-P1', '2013-08-30 11:39:14', '2013-08-30 13:39:14'),
(8, 'hpPrestaP1', 'Votre réseau s''essouffle ? <br/>\r\nConsultez les offres de projets <br/>et répondez à celles <br/>', 'Your network is running out ? <br/>Look at all the projects <br/>listed here and then<br/>', '¿Necesitás nuevos proyectos? <br/>Explorá nuestra sección de proyectos y hacé contacto <br/>', 'hpPrestaP1-P1', '2013-08-30 11:39:14', '2013-08-30 13:39:14'),
(9, 'hpPrestaP2', 'Déposez votre CV', 'Upload your CV', 'Subi tu CV', 'hpPrestaP2-P1', '2013-08-30 11:39:14', '2013-08-30 13:39:14'),
(10, 'hpBienvenueP1', 'WeMe est LA plateforme de mise en relation, <br/> \r\nsans commission sur les projets réalisés.', 'NeoFreelo is THE website where professionals get in touch without any commission on their projects.', 'NeoFreelo es LA plataforma para ponerse en relación, <span itemprop="keywords">sin comisión</span> sobre los proyectos realizados.</span>', 'hpBienvenueP1-P1', '2013-08-30 11:39:14', '2013-08-30 13:39:14'),
(11, 'hpSourcingTitre', 'Sourcing', 'Sourcing', 'Sourcing', 'hpSourcingTitre-P1', '2013-08-30 11:39:14', '2013-08-30 13:39:14'),
(12, 'hpSourcingP1', 'Si vous publiez un projet sur notre site et que vous ne trouvez pas <br/> \r\nle prestataire adéquat : <br/>\r\n<b>contactez nous.</b> <br/>\r\nNos experts trouveront pour vous la personne qui répond <br/>\r\nà vos exigences.', 'If you publish a project on our website and you do not find <br/>the suitable candidate : <b>contact us. </b><br/>Our experts will find you <br/>the person corresponding to <br/>all your requirements.', 'Si publicas un proyecto en nuestra web y no encuentras <br/>\r\nel freelance adecuado : <br/>\r\n<b>contactanos.</b> <br/>\r\nNuestros expertos te encontrarán la persona que responde\r\n<br/>\r\na tus exigencias.', 'hpSourcingP1-P1', '2013-08-30 11:39:14', '2013-08-30 13:39:14'),
(13, 'hpIntermittentTitre', 'Intermittents du spectacle', 'Independent Workers', 'Tecnicos de espectaculo', 'hpIntermittentTitre-P1', '2013-08-30 11:39:14', '2013-08-30 13:39:14'),
(14, 'exclamation', '<br/>Necesitas ayuda con la contratación y la administración de tu freelance? No te preocupes, nuestros profesionales se encargaran de hacer todo el trabajo, vos solo concentrate en tu proyecto!<br/>', '<br/>Do you need help dealing with freelancers ? \r\nDo not worry, our team of experts can provide some help with resources management issues (drafting contracts, employment issues, ...). All is left for you to do is to focus on your project. <br/>', '<br/> <span itemprop="description">Necesitas ayuda con la <span itemprop="keywords">contratación</span> y la <span itemprop="keywords">administración</span> de tu <span itemprop="keywords">freelance</span>? No te preocupes, nuestros profesionales se encargaran de hacer todo el trabajo, vos solo concentrate en tu proyecto!</span>', 'exclamation-P1', '2013-08-30 11:39:14', '2013-08-30 13:39:14'),
(15, 'presta', 'un prestataire', 'a freelancer', 'un freelance', 'presta-P1', '2013-08-30 11:39:14', '2013-08-30 13:39:14'),
(16, 'alt_carroussel', 'image carroussel ', 'carroussel image ', 'imagen carrusel', 'alt_carroussel-P1', '2013-08-30 11:39:14', '2013-08-30 13:39:14'),
(17, 'hpIntermittentP1', 'Vous souhaitez engager un intermittent  du spectacle pour votre projet, mais vous ne savez pas comment procéder ? ', 'You wish to hire an independent worker for your project, but do not know how to proceed ?', 'un tecnico de espectaculo para su proyecto, pero no sabe como hacerlo ?', 'hpIntermittentP1-P1', '2013-08-30 11:39:14', '2013-08-30 13:39:14'),
(18, 'hpIntermittentP2', 'Vous êtes intermittent du spectacle et souhaitez élargir votre ré?seau ? ', 'You are an independent worker and wish to improve your network ?', 'Ã¸ Usted es tecnico de especteculo y quiere expandir su red ?', 'hpIntermittentP2-P1', '2013-08-30 11:39:14', '2013-08-30 13:39:14'),
(19, 'hpIntermittentP3', 'WeMe est la solution', 'WeMe is your solution', 'WeMe es la solucion', 'hpIntermittentP3-P1', '2013-08-30 11:39:14', '2013-08-30 13:39:14'),
(20, 'hpSourcingP2', 'Nos experts trouveront pour vous la personne qui ré?pond <br/>; à vos exigences.', 'Our experts will find you <br/>;the person corresponding to <br/>;all your requirements.', 'Nuestros expertos le encontraron la persona que responde a sus exigencias.', 'hpSourcingP2-P1', '2013-08-30 11:39:14', '2013-08-30 13:39:14'),
(21, 'hpSendNewMdp', 'Merci de nous indiquer votre adresse email. <br/>\r\nVous recevrez un email avec votre nouveau mot de passe.', 'Please indicate your email address. <br/>You will receive an email with a new password.', 'Gracias por indicarnos tu email. <br/>\r\nRecibirás un email con tu nueva contraseña.', 'hpSendNewMdp-P1', '2013-08-30 11:39:14', '2013-08-30 13:39:14'),
(22, 'need_mail', 'Merci de nous indiquer votre adresse email pour recevoir votre nouveau mot de passe', 'Please indicate your email address to receive your new password', 'Gracias por indicarnos tu email para recibir tu nueva contraseña', 'need_mail-P1', '2013-08-30 11:39:14', '2013-08-30 13:39:14'),
(23, 'new_mdp_send', 'Votre demande a bien été prise en compte, vous allez recevoir un email avec votre nouveau mot de passe à l''adresse suivante : ', 'Your request has been registered, you will receive an email with your new password at the following email address : ', 'Tomamos en cuenta tu petición, vas a recibir un email con tu nueva contraseña en la dirección siguiente : ', 'new_mdp_send-P1', '2013-08-30 11:39:14', '2013-08-30 13:39:14'),
(24, 'new_mdp_error', 'Veuillez nous excuser, mais nous n''avons trouvé aucun membre dans notre base correspondant à l''email de connexion suivant : ', 'Sorry, we have not found any member in our database corresponding to the following email address : ', 'Lo sentimos, pero no encontramos ningún miembro en nuestra base de datos correspondiendo al email de conexión siguiente : ', 'new_mdp_error-P1', '2013-08-30 11:39:14', '2013-08-30 13:39:14'),
(25, 'siteName', 'weme.fr', 'neofreelo.com', 'neofreelo.com', 'siteName-P2', '2013-08-30 11:41:25', '2013-08-30 13:41:25'),
(26, 'headerBaseline', 'Creative Place for Creative People', 'The place where a talent finds its project', 'Donde un talento encuentra su proyecto', 'headerBaseline-P2', '2013-08-30 11:41:25', '2013-08-30 13:41:25'),
(27, 'headerTrPresta', 'Trouver un prestataire', 'Find a freelancer', 'Encontrar un freelance', 'headerTrPresta-P2', '2013-08-30 11:41:25', '2013-08-30 13:41:25'),
(28, 'headerTrProjet', 'Trouver un projet', 'Find a project', 'Encontrar un proyecto', 'headerTrProjet-P2', '2013-08-30 11:41:25', '2013-08-30 13:41:25'),
(29, 'headerInscription', 'Inscription', 'Sign Up', 'Inscripción', 'headerInscription-P2', '2013-08-30 11:41:25', '2013-08-30 13:41:25'),
(30, 'headerConnexion', 'connexion à votre compte', 'Log into your account', 'Conexión a tu cuenta', 'headerConnexion-P2', '2013-08-30 11:41:25', '2013-08-30 13:41:25'),
(31, 'headerMailConnexion', 'email de connexion', 'email address', 'email de conexión', 'headerMailConnexion-P2', '2013-08-30 11:41:25', '2013-08-30 13:41:25'),
(32, 'headerSouvenir', 'se souvenir de moi', 'Remember me', 'recordarme', 'headerSouvenir-P2', '2013-08-30 11:41:25', '2013-08-30 13:41:25'),
(33, 'headerMdpOublie', 'mot de passe oublié ?', 'forgotten password ?', '¿ contraseña olvidada ?', 'headerMdpOublie-P2', '2013-08-30 11:41:25', '2013-08-30 13:41:25'),
(34, 'hpSendNewMdp', 'Merci de nous indiquer votre adresse email. <br/>Vous recevrez un email avec votre nouveau mot de passe.', 'Please indicate your email address. <br/> You will receive an email with a new password.', 'Gracias por indicarnos tu email. <br/>>Recibirás un email con tu nueva contraseña.', 'hpSendNewMdp-P2', '2013-08-30 11:41:25', '2013-08-30 13:41:25'),
(35, 'not_actif', 'Votre compte n''ai pas encore activé. <br/>\r\nVous pouvez l''activer grâce au lien fourni dans l''email que vous avez reçu lors de votre inscription', 'Your account has not been activated yet. <br>\r\nYou can activate it via the link we have sent to your email address at the time of your registration', 'Tu cuenta todavía no ha sido activada. <br/>\r\nPuedes activarla gracias al link proporcionado en el email que recibiste en tu inscripción', 'not_actif-P2', '2013-08-30 11:41:25', '2013-08-30 13:41:25'),
(36, 'erreur_connexion', 'Veuillez nous excuser, mais l''identifiant ou le mot de passe renseigné est invalide. Merci de recommencer.\r\n', 'Sorry, your NeoFreelo ID or password is not valid. Please try again.\r\n', 'Lo sentimos, pero el email de conexión o la contraseña indicada no es valido. Gracias por intentar de nuevo.', 'erreur_connexion-P2', '2013-08-30 11:41:25', '2013-08-30 13:41:25'),
(37, 'footerAccueil', 'Accueil', 'Home', 'Inicio', 'footerAccueil-P3', '2013-08-30 11:42:06', '2013-08-30 13:42:06'),
(38, 'footerAPropos', 'A propos', 'About us', 'Sobre nosotros', 'footerAPropos-P3', '2013-08-30 11:42:06', '2013-08-30 13:42:06'),
(39, 'footerMentions', 'Informations légales', 'Legal information', 'Informaciones legales', 'footerMentions-P3', '2013-08-30 11:42:06', '2013-08-30 13:42:06'),
(40, 'footerSitemap', 'Sitemap', 'Sitemap', 'Mapa web', 'footerSitemap-P3', '2013-08-30 11:42:06', '2013-08-30 13:42:06'),
(41, 'footerContact', 'Contact', 'Contact', 'Contacto', 'footerContact-P3', '2013-08-30 11:42:06', '2013-08-30 13:42:06'),
(42, 'footerCopyright', 'Copyright 2012 - WeMe - marque déposée', '', '', 'footerCopyright-P3', '2013-08-30 11:42:06', '2013-08-30 13:42:06'),
(43, 'footerLiensUtiles', 'Liens Utiles', 'Useful links', 'Enlaces de interes', 'footerLiensUtiles-P3', '2013-08-30 11:42:06', '2013-08-30 13:42:06'),
(44, 'footerClientProjet', 'Créer votre projet', 'Version Anglaise', 'Version Espagnol', 'footerClientProjet-P3', '2013-08-30 11:42:06', '2013-08-30 13:42:06'),
(45, 'footerEntrepreneur', 'Entrepreneurs', 'Entre EN', 'Entre ES', 'footerEntrepreneur-P3', '2013-08-30 11:42:06', '2013-08-30 13:42:06'),
(46, 'footerPrestataire', 'Prestataire', 'Presta En ', 'Presta Es', 'footerPrestataire-P3', '2013-08-30 11:42:06', '2013-08-30 13:42:06'),
(47, 'label_titre_partenaire', 'Nos <br/>partenaires', 'Our <br/>Partners', 'Nuestros <br/>Aliados', 'label_titre_partenaire-P3', '2013-08-30 11:42:06', '2013-08-30 13:42:06'),
(48, 'description_partenaire', 'Texte de description pour leur dire que s''ils veulent devenir notre partenaire, qu''ils nous contactent', 'Texte de description pour leur dire que s''ils veulent devenir notre partenaire, qu''ils nous contactent', 'Texte de description pour leur dire que s''ils veulent devenir notre partenaire, qu''ils nous contactent', 'description_partenaire-P3', '2013-08-30 11:42:06', '2013-08-30 13:42:06'),
(49, 'footerClientCvtheque', 'Explorez notre CVthèque', 'Version Anglaise', 'Version Espagnol', 'footerClientCvtheque-P3', '2013-08-30 11:42:06', '2013-08-30 13:42:06'),
(50, 'footerClientSourcing', 'Nos solutions Sourcing', 'Version An', 'Version En', 'footerClientSourcing-P3', '2013-08-30 11:42:06', '2013-08-30 13:42:06'),
(51, 'footerPrestaCv', 'Déposer votre CV', 'Version An', 'Version Es', 'footerPrestaCv-P3', '2013-08-30 11:42:06', '2013-08-30 13:42:06'),
(52, 'footerPrestaIntermittent', 'Nos solutions intermittents', 'Version An', 'Version En', 'footerPrestaIntermittent-P3', '2013-08-30 11:42:06', '2013-08-30 13:42:06'),
(53, 'footerPrestaProjet', 'Consulter les projets', 'Version An', 'Version Es', 'footerPrestaProjet-P3', '2013-08-30 11:42:06', '2013-08-30 13:42:06'),
(54, 'inscriptionBtDO', 'Je suis Donneur d''ordres', 'I am looking for freelancers', 'Busco freelances', 'inscriptionBtDO-P4', '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(55, 'inscriptionBtPresta', 'Je suis Prestataire', 'I am looking for projects', 'Busco proyectos', 'inscriptionBtPresta-P4', '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(56, 'inscriptionIntro', '<b> Pour commencer, </b> <br/>\r\nCliquez sur le bouton vous correspondant :<br/>\r\n<ul class="starBlue margin-top-10">\r\n<li>Cliquez sur "Je suis donneur d''ordres" si vous êtes un donneur d''ordres à  la recherche de prestataires.  </li>\r\n<li>Cliquez sur "Je suis prestataire" si vous êtes un freelance, un intermittent du spectacle ou une agence à la recherche de projets. </li></ul>', '<b> In order to start, </b> <br/>\r\nPlease click on the right button : \r\n<ul class="starBlue margin-top-10">\r\n<li>Click on "I am <span itemprop="keywords">looking for freelancers</span>" if you are a <span itemprop="keywords">contractor </span> who needs to find new professional talents for their projects.</li>\r\n<li>Click on "I am <span itemprop="keywords">looking for projects</span>" if you are a <span itemprop="keywords">freelancer</span>, an <span itemprop="keywords">independent worker</span>, a <span itemprop="keywords">student</span> or an <span itemprop="keywords">agency</span> in search for work.</li></ul>\r\n', '<b> Para empezar, </b> <br/>\r\nHacé clic en el botón que te interesa :<br/>\r\n<ul class="starBlue margin-top-10">\r\n<li>"Busco <span itemprop="keywords">freelances</span>" si sos una empresa que quiere <span itemprop="keywords">contratar</span> nuevos <span itemprop="keywords">talentos profesionales</span> para realizar tus proyectos.  </li>\r\n<li>"Busco <span itemprop="keywords">proyectos</span>" si sos un <span itemprop="keywords">prestatario de servicios</span> (freelance, agencia o estudiante) en búsqueda de <span itemprop="keywords">nuevos clientes</span>. </li></ul>\r\n', 'inscriptionIntro-P4', '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(57, 'inscriptionTitre2', 'Pourquoi s''inscrire à  WeMe ? ', '<span itemprop="about">Why should I sign up for NeoFreelo ?</span>', '<span itemprop="about">¿ Por qué inscribirte a NeoFreelo ?</span>', 'inscriptionTitre2-P4', '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(58, 'inscriptionInfoLi1', 'Vous êtes donneur d''ordres à la recherche de nouvelles compétences ou prestataire à la recherche de nouveaux projets? <br/>\r\nCréez gratuitement votre compte WeMe et accédez à nos services : ', 'You are a contractor looking for <span itemprop="keywords">specific skills</span> or a freelancer looking for new projects ? <br/>\r\nSign up for <span itemprop="keywords">NeoFreelo</span> and access to our services such as : ', '¿ Sos una empresa en búsqueda de nuevas capacidades <span itemprop="keywords">profesionales</span> o un freelance en <span itemprop="keywords">búsqueda</span> de nuevos proyectos ? <br/>\r\nCrea gratuitamente tu cuenta NeoFreelo y accedé a nuestros servicios :', 'inscriptionInfoLi1-P4', '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(59, 'inscriptionInfoLi1Li1', 'La publication de projets et l''accès à la Cvthèque pour le donneur d''ordres.', 'The publication of projects and the access to the <span itemprop="keywords">CV library for the contractor</span>.', 'La publicación de proyectos y el acceso a la <span itemprop="keywords">CVteca</span> para la empresa.', 'inscriptionInfoLi1Li1-P4', '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(60, 'inscriptionInfoLi1Li2', 'La publication de CVs et la recherche de projets pour le prestataire.', 'The publication of <span itemprop="keywords">CVs</span> and the search for projects for the <span itemprop="keywords">freelancer</span>.', 'La publicación de CVs y la búsqueda de proyectos para el Freelance.', 'inscriptionInfoLi1Li2-P4', '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(61, 'inscriptionInfoLi2', 'N''attendez plus, inscrivez-vous pour découvrir tous les services WeMe !', 'Wait no more, sign up to discover all NeoFreelo services !', '¡ No esperes más, inscribite para descubrir todos los servicios de NeoFreelo !', 'inscriptionInfoLi2-P4', '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(62, 'inscriptionTitre1', 'Création de votre compte gratuit ', 'Sign up for NeoFreelo', 'Creación de tu cuenta gratuita', 'inscriptionTitre1-P4', '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(63, 'a_propos_societe', 'A propos de votre société :', 'About your company :', 'Sobre tu empresa :', 'a_propos_societe-P4', '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(64, 'a_propos_vous', 'A propos de vous :', 'About you :', 'Sobre vos :', 'a_propos_vous-P4', '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(65, 'ident', 'Identifiants de connexion :', 'NeoFreelo ID :', 'Identificadores de conexión :', 'ident-P4', '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(66, 'pq_weme', 'Pourquoi s''inscrire à WeMe ?', '<span itemprop="headline">Why should I sign up for <span itemprop="name">NeoFreelo</span>?</span>', '<span itemprop="headline">¿ Por qué inscribirte a NeoFreelo ?</span>', 'pq_weme-P4', '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(67, 'inscriptionClientP1', 'Votre équipe est sur les rotules et a besoin de renfort ? <br/> \r\nAvec WeMe, <b>trouvez vos prestataires rapidement et facilement</b> ! <br>\r\nUtilisez notre plateforme en créant gratuitement un compte à l’aide du formulaire ci-contre. <br>', 'Your team needs some more help ? <br/>\r\nNeoFreelo helps you <b><span itemprop="keywords">find the freelancer</span> you need in a quick and easy way</b> ! <br>\r\n Sign up here by filling in the hereby formulaire.<br>', '¿ Tu equipo está cansado y necesita ayuda ? <br/> \r\nCon NeoFreelo, <b>encontrás a tus <span itemprop="keywords">freelance</span> rápida y fácilmente </b> ! <br>\r\nUtilizá nuestra plataforma creando <span itemprop="keywords">gratuitamente</span> una cuenta con la ayuda del formulario al lado. <br>', 'inscriptionClientP1-P4', '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(68, 'copy_code', 'Recopiez le code ci-contre', 'Enter this code', 'Copia este código', 'copy_code-P4', '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(69, 'alt_captcha', 'Rafraîchir le code', 'Refresh the code', 'Refrescar el código', 'alt_captcha-P4', '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(70, 'asterisque', '* ', '*', '*', 'asterisque-P4', '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(71, 'titre', 'Création de votre compte gratuit', 'Sign up for NeoFreelo', 'Creación de tu cuenta gratuita', 'titre-P4', '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(72, 'free', 'Freelance', 'Freelancer', 'Freelance', 'free-P4', '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(73, 'inter', 'Intermittent du spectacle', 'Student', 'Estudiante', 'inter-P4', '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(74, 'agence', 'Agence', 'Agency', 'Agencia', 'agence-P4', '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(75, 'inscriptionPrestaInfoFreelance', '<p>Vous êtes à  la recherche de nouveaux projets ? \r\n		     <br/>Vous souhaitez élargir votre réseau professionnel ? <br/>\r\n		     <br/> <strong> WeMe est une plateforme 100% gratuite pour les prestataires ! </strong> <br/>\r\n		     N''attendez plus pour créer votre compte !\r\n		</p>\r\n		\r\n		<p class="margin-top-10 margin-bottom-20">\r\n		    Inscrivez-vous, téléchargez vos réalisations et/ou votre dossier de présentation et le tour est joué !\r\n		</p>\r\n		\r\n		<p class="margin-top-10">\r\n		     Une fois inscrit, il ne vous reste qu''à consulter les projets et à répondre à ceux qui vous correspondent.\r\n		     <br>\r\n		     Pratique, si une entreprise sélectionne votre CV, elle peut vous proposer directement un entretien sur WeMe par vidéoconférence.\r\n		     <br>\r\n		     Plus besoin de vous déplacer pour rencontrer vos nouveaux clients !\r\n		</p>\r\n		', '<p >You are looking for <span itemprop="keywords">new projects</span> ?<br/>\r\nYou wish to extend your professional network ?<br/>\r\n<br/> <strong> NeoFreelo is a website <span itemprop="keywords">100% free for <span itemprop="keywords">freelancers</span> !</strong> <br/>\r\nWait no more to create your free account !\r\n		</p>\r\n		\r\n		<p>\r\n		    Sign up, upload your <span itemprop="keywords">CV</span>, <span itemprop="keywords">creations</span> and/or your presentation file <br/>and here we go !\r\n		</p>\r\n		\r\n		<p class="margin-top-10">\r\n		     Once your registration done, you can look at all the <span itemprop="keywords">projects</span> and <span itemprop="keywords">apply</span> to the ones matching your expectations. <br>\r\nEasy to use, if a contractor selects your CV, they may directly ask you for an <span itemprop="keywords">interview</span> on <span itemprop="keywords">NeoFreelo</span> via video call. <br>No need for anyone to move to meet <span itemprop="keywords">new clients</span> !\r\n		</p>\r\n		', '<p >\r\n		     ¿ Buscas nuevos <span itemprop="keywords">proyectos</span> ? \r\n<br/>¿ Deseas expandir tu red profesional ? <br/>\r\n<br/> <strong> ¡ <span itemprop="name">NeoFreelo</span> es una plataforma <span itemprop="name">100% gratuita</span> para los <span itemprop="name">Freelance</span> ! </strong> <br/>\r\n¡ No esperes más para crear tu cuenta !\r\n		</p>\r\n		\r\n		<p class="margin-top-10 margin-bottom-20">\r\n		    Inscribite, subí tu <span itemprop="name">CV</span> y tus <span itemprop="name">realizaciones</span> y ya está !\r\n		</p>\r\n		\r\n		<p class="margin-top-10">\r\n		     Una vez inscrito, sólo queda consultar los proyectos y contestar a los que le interesan. <br>\r\nPráctico, si una empresa selecciona tu CV, puede proponerte una entrevista  por videoconferencia en NeoFreelo. <br>¡ Ya no hace falta desplazarse para conocer a tus nuevos clientes !\r\n		</p>\r\n		', 'inscriptionPrestaInfoFreelance-P4', '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(76, 'inscriptionPrestaInfoStudent', '<p class="margin-top-10 margin-bottom-20">\r\n		     Vous êtes à  la recherche de nouveaux projets ? \r\n		     <br/>Vous souhaitez élargir votre réseau professionnel ? <br/>\r\n		     <br/> <strong> WeMe est une plateforme 100% gratuite pour les prestataires ! </strong> <br/>\r\n		     N''attendez plus pour créer votre compte !\r\n		</p>\r\n		\r\n		<p class="margin-top-10 margin-bottom-20">\r\n		    Inscrivez-vous, téléchargez vos réalisations et/ou votre dossier de présentation et le tour est joué !\r\n		</p>\r\n		\r\n		<p class="margin-top-10">\r\n		     Une fois inscrit, il ne vous reste qu''à consulter les projets et à répondre à ceux qui vous correspondent.\r\n		     <br>\r\n		     Pratique, si une entreprise sélectionne votre CV, elle peut vous proposer directement un entretien sur WeMe par vidéoconférence.\r\n		     <br>\r\n		     Plus besoin de vous déplacer pour rencontrer vos nouveaux clients !\r\n		</p>\r\n		', '<p class="margin-top-10 margin-bottom-20">\r\n		    <br/> You are currently <span itemprop="keywords">studying</span> a <span itemprop="keywords">university degree</span> in <span itemprop="keywords">marketing</span>, <span itemprop="keywords">communications</span>, <span itemprop="keywords">design</span>, <span itemprop="keywords">multimedia</span>, <span itemprop="keywords">audio</span>, <span itemprop="keywords">events</span>, <span itemprop="keywords">web</span>, <span itemprop="keywords">new technologies</span> or any other sector in relation to the world of <span itemprop="keywords">freelancers</span>?\r\n<br/> Would  you like to find an <span itemprop="keywords">internship</span> or <span itemprop="keywords">work</span> as a <span itemprop="keywords">freelancer</span> while carrying on studying ?\r\n<br/>\r\n<br/> \r\n<span itemprop="keywords">NeoFreelo</span> supports you in developping your <span itemprop="keywords">professional experience</span> !\r\n<br/> <strong> NeoFreelo is a website <span itemprop="keywords">100% free</span> for freelancers !</strong> <br/>\r\nWait no more to create your free account !\r\n		</p>\r\n		\r\n		<p class="margin-top-10 margin-bottom-20">\r\n		    Sign up, upload your <span itemprop="keywords">CV</span>, <span itemprop="keywords">creations</span> and/or your presentation file <br/>and here we go !\r\n		</p>\r\n		\r\n		<p class="margin-top-10">\r\n		     Once your registration done, you can look at all the projects and apply to the ones matching your expectations. <br>\r\nEasy to use, if a contractor selects your CV, they may directly ask you for an interview on NeoFreelo via video call. <br>No need for anyone to move to meet <span itemprop="keywords">new clients</span> !\r\n		</p>\r\n		', '<p class="margin-top-10 margin-bottom-20">\r\n		    \r\n<br/>¿ Tu <span itemprop="keywords">carrera</span> universitaria es de <span itemprop="keywords">marketing</span>, <span itemprop="keywords">comunicación</span>, <span itemprop="keywords">diseño</span>, <span itemprop="keywords">multimedia</span>, <span itemprop="keywords">audiovisual</span>, <span itemprop="keywords">web</span>, <span itemprop="keywords">nuevas tecnologías</span> o cualquier sector relacionado con el trabajo freelance ?\r\n<br/> \r\n\r\n¿Querés encontrar una <span itemprop="keywords">pasantía</span> o <span itemprop="keywords">trabajar</span> como <span itemprop="keywords">freelance</span> mientras estás estudiando?\r\n<br/> \r\n <br/><span itemprop="keywords">NeoFreelo</span> te apoya en el desarrollo de tu <span itemprop="keywords">experiencia profesional</span>!		\r\n<br/>\r\n                <strong> ¡ <span itemprop="keywords">100% gratuita</span> para los Freelance ! </strong>\r\n\r\n                <p class="margin-top-10 margin-bottom-20">\r\n		    \r\n		</p>\r\n		\r\n		<p class="margin-top-10">\r\n		     Inscribite, subí tu CV y tus realizaciones y ¡ya está! <br>\r\nUna vez inscrito, sólo queda consultar los <span itemprop="keywords">proyectos</span> y contestar a los que te interesan. \r\n<br>\r\nPráctico, si una empresa selecciona tu <span itemprop="keywords">CV</span>, puede proponerte una <span itemprop="keywords">entrevista</span>  por videoconferencia en NeoFreelo. <br>¡ Ya no hace falta desplazarse para conocer a tus nuevos clientes !\r\n		</p>\r\n		', 'inscriptionPrestaInfoStudent-P4', '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(77, 'inscriptionPrestaInfoAgence', '<p class="margin-top-10 margin-bottom-20">\r\n		     Vous êtes à  la recherche de nouveaux projets ? \r\n		     <br/>Vous souhaitez élargir votre réseau professionnel ? <br/>\r\n		     <br/> <strong> WeMe est une plateforme 100% gratuite pour les prestataires ! </strong> <br/>\r\n		     N''attendez plus pour créer votre compte !\r\n		</p>\r\n		\r\n		<p class="margin-top-10 margin-bottom-20">\r\n		    Inscrivez-vous, téléchargez vos réalisations et/ou votre dossier de présentation et le tour est joué !\r\n		</p>\r\n		\r\n		<p class="margin-top-10">\r\n		     Une fois inscrit, il ne vous reste qu''à consulter les projets et à répondre à ceux qui vous correspondent.\r\n		     <br>\r\n		     Pratique, si une entreprise sélectionne votre CV, elle peut vous proposer directement un entretien sur WeMe par vidéoconférence.\r\n		     <br>\r\n		     Plus besoin de vous déplacer pour rencontrer vos nouveaux clients !\r\n		</p>\r\n		', '<p class="margin-top-10 margin-bottom-20">\r\n		   Is your <span itemprop="keywords">agency</span> looking for <span itemprop="keywords">new projects</span> ?\r\n<br/>You wish to extend your <span itemprop="keywords">professional network</span> ? <br/>\r\n<br/> <strong> <span itemprop="keywords">NeoFreelo</span> is a <span itemprop="keywords">100% free</span> website for <span itemprop="keywords">freelancers</span> ! </strong> <br/>\r\nWait no more to create your free account !\r\n		</p>\r\n		\r\n		<p class="margin-top-10 margin-bottom-20">\r\n		Sign up your agency, upload the <span itemprop="keywords">CVs</span> and <span itemprop="keywords">creations</span> of your team on one and only account <br/>and here we go !\r\n		</p>\r\n		\r\n		<p class="margin-top-10">\r\n		     Once your registration done, all you have to do is to look at <span itemprop="keywords">projects</span> matching your team members’ skills and reply to them. <br> \r\nEasy to use, Convenient, if a Company selects one of your agency’s CV, they can directly ask you for an interview on NeoFreelo via video call\r\n<br>No need for anyone to move to meet <span itemprop="keywords">new clients</span> !\r\n		</p>\r\n		', '<p class="margin-top-10 margin-bottom-20">\r\n		     ¿ Tu agencia está buscando <span itemprop="keywords">nuevos proyectos</span> ? \r\n<br/>¿ Querés expandir tu <span itemprop="keywords">red profesional</span> ? <br/>\r\n<br/> <strong> ¡ <span itemprop="keywords">NeoFreelo</span> es una plataforma <span itemprop="keywords">100% gratuita</span> para los <span itemprop="keywords">prestatarios</span> de servicios ! </strong> <br/>\r\n¡ No esperes más para crear tu cuenta !\r\n		</p>\r\n		\r\n		<p class="margin-top-10 margin-bottom-20">\r\n		    Inscribí tu agencia, subí los <span itemprop="keywords">CV</span> de tu equipo de y sus <span itemprop="keywords">realizaciones</span> en una sola cuenta y ya está !\r\n		</p>\r\n		\r\n		<p class="margin-top-10">\r\n		     Una vez inscrito, sólo queda consultar los proyectos que corresponden a las aptitudes de los miembros de tu equipo y contestar a los que te interesan. <br>\r\nPráctico, si una empresa necesita a alguien de tu equipo para realizar su proyecto, puede proponerte, directamente, una entrevista por videoconferencia en NeoFreelo. <br>¡ Ya no hace falta desplazarse para conocer a tus <span itemprop="keywords">nuevos clientes</span> !\r\n		</p>\r\n		', 'inscriptionPrestaInfoAgence-P4', '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(78, 'inscriptionChooseStructure', 'Vous êtes un Freelance ? Un intermittent du spectacle ? Une agence ? <br/>Cliquez sur le bouton qui vous correspond !', 'Are you a Freelancer ? A Student ? An Agency ? <br/>Click on the right button !', '¿ Sos Freelance ? ¿Un estudiante universitario ? ¿ Una agencia ? <br/>¡ Hacé clic en el botón que te interesa !', 'inscriptionChooseStructure-P4', '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(79, 'select_secteur', 'Saisissez votre secteur d''activité', 'Enter your industry', 'Introducí tu sector de actividad', 'select_secteur-P4', '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(80, 'inscriptionClientInfoTitre3', 'Abonnement WeMe', '<span itemprop="headline">NeoFreelo Subscription</span>', '<span itemprop="headline">Suscripción <span itemprop="name">NeoFreelo</span></span>', 'inscriptionClientInfoTitre3-P4', '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(81, 'inscriptionClientInfoIntro3', 'Souscrivez un abonnement à tout moment ! ', '<span itemprop="alternativeHeadline">Subscribe to <span itemprop="keywords">NeoFreelo</span> at any time !</span>', '<span itemprop="alternativeHeadline">¡ Suscribite en todo momento ! </span>', 'inscriptionClientInfoIntro3-P4', '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(82, 'inscriptionClientInfoP3', 'A la fin de ta période d’essai gratuite nous te proposons de continuer l’aventure avec nous ! <br> \r\nLa plateforme NeoFreelo a été pensée pour s’adapter à tes besoins. Pour plus de flexibilité et afin de faciliter tes recherches, nous proposons trois manières de travailler ensemble. \r\nDécouvre ici nos formules de souscription. ', 'During the <span itemprop="keywords">free trial period</span>, or at the end of it, you can subscribe to NeoFreelo and thus carry on the adventure with us !<br> <br>\r\nThe NeoFreelo website has been designed to adapt itself to your needs. For more flexibility and ease in your <span itemprop="keywords">research</span>, we propose you three different ways to work together. <br/>Discover here our subscription''s options. ', 'Durante el periodo de <span itemprop="keywords">prueba gratis</span>, o al fin de ese mismo periodo, podrás suscribirte a NeoFreelo y así seguir con nosotros ! <br><br>\r\nLa plataforma NeoFreelo está diseñada para adaptarse a tus necesidades. Para más flexibilidad y con el fin de facilitar tu “búsqueda”, proponemos tres maneras para trabajar juntos. <br>\r\nDescubre acá nuestras formulas de suscripción.', 'inscriptionClientInfoP3-P4', '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(83, 'inscriptionClientOption', 'Durée', 'Our options', 'Nuestras Fórmulas ', 'inscriptionClientOption-P4', '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(84, 'inscriptionClientPrice', 'Prix', 'Price', 'Precios', 'inscriptionClientPrice-P4', '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(85, 'monnaie', '€', 'AR$', ' AR$', 'monnaie-P4', '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(86, 'inscriptionClientInfoTitre2', '15 jours d''essai gratuit', '<span itemprop="headline">2 months of free trial</span>', '<span itemprop="headline"> 2 meses de prueba gratis!</span>', 'inscriptionClientInfoTitre2-P4', '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(87, 'inscriptionClientInfoP2', 'Dès votre inscription, vous bénéficiez de 15 jours d’essai gratuit pour vous permettre de tester des services tels que  : ', 'Once registered, you benefit from 2 months of free trial. You can publish for free<span itemprop="keywords">projects</span> and try our services such as : ', 'Con tu inscripción, te ofrecemos un periodo de prueba de 2 meses. Podrás <span itemprop="keywords">publicar gratuitamente</span> todos tus <span itemprop="keywords">proyectos</span> para probar nuestros servicios como:', 'inscriptionClientInfoP2-P4', '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(88, 'inscriptionClientInfoLi1', 'La publication en illimité de vos projets et la gestion des réponses', 'The access to all personal data of freelancers who replied to your publication', 'El acceso a toda la información personal de los freelances que contestan a tu publicación', 'inscriptionClientInfoLi1-P4', '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(89, 'inscriptionClientInfoLi2', 'Les fonctionnalités de votre tableau de bord, complet et simple d’utilisation', 'The functionalities of your dasboard ', 'El descrubrimiento de las funcionalidades de tu panel de control, completo y simple de utilizar', 'inscriptionClientInfoLi2-P4', '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(90, 'inscriptionClientInfoLi3', 'L’organisation d’entretiens vidéo', 'The organisation of video calls with <span itemprop="keywords">Freelancers</span> who replied to your publication', 'Un contacto, directamente en NeoFreelo,  por email, chat y videoconferencia, con los freelances que contestan a tu publicación.', 'inscriptionClientInfoLi3-P4', '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(91, 'inscriptionClientInfoLi4', 'L’accès limité à la Cvthèque', 'A limited access to the CV library', 'Un acceso limitado a la <span itemprop="keywords">CVteca</span>', 'inscriptionClientInfoLi4-P4', '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(92, 'inscriptionClientInfoP2End', '( Toute action sur la Cvthèque et l’accès aux coordonnées nécessite un abonnement )', '( Any action in the <span itemprop="keywords">CV library</span> and any access to users'' data require the purchase of a fee subscription )', '(Toda acción en la CVteca y el acceso a los datos necesitan una suscripción)', 'inscriptionClientInfoP2End-P4', '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(93, 'info_linkedIn', 'Connectez-vous via votre compte LinkedIn ', 'Connect via your LinkedIn account', 'Conectate vía tu cuenta LinkedIn ', 'info_linkedIn-P4', '2013-08-30 11:42:15', '2013-08-30 13:42:15'),
(94, 'missionRepondre', 'Répondre à cette offre', 'Reply to this offer', 'Responder a esta oferta', 'missionRepondre-P5', '2013-08-30 11:42:27', '2013-08-30 13:42:27'),
(95, 'missionSaveProjet', 'Sauvegarder le projet', 'Save the project', 'Guardar el proyecto', 'missionSaveProjet-P5', '2013-08-30 11:42:27', '2013-08-30 13:42:27'),
(96, 'missionDetail', 'Détail du projet', 'Project details', 'Detalle del proyecto', 'missionDetail-P5', '2013-08-30 11:42:27', '2013-08-30 13:42:27'),
(97, 'missionNbReponses', 'Réponses reçues', 'Received responses', 'Respuestas recibidas', 'missionNbReponses-P5', '2013-08-30 11:42:27', '2013-08-30 13:42:27'),
(98, 'bienvenue', 'Bienvenue sur votre tableau de bord', 'Welcome to your dashboard ', 'Bienvenido a tu panel de control', 'bienvenue-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(99, 'intro', 'Vous avez une réponse d''une entreprise suite à une proposition ? Des projets vous correspondent ? <br/> Accédez à toutes les fonctionnalités de votre espace WeMe depuis cette page !', 'You''ve received an answer from a contractor following your proposal ? Some projects match your qualifications ? <br/>Get access to all the functionalities of your NeoFreelo personal space from this page !', '¡ Accedé a todas las funciones de tu espacio NeoFreelo desde esta página !<br/>\r\n', 'intro-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(100, 'derniers_projets', 'Les derniers projets publiés', 'The most recently published projects', 'Los últimos proyectos publicados', 'derniers_projets-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(101, 'prop_en_cours', 'Mes propositions en cours', 'My current proposals', 'Mis propuestas en curso', 'prop_en_cours-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(102, 'calendrier', 'Mon Calendrier', 'My calendar', 'Mi Calendario', 'calendrier-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(103, 'event', 'Retrouvez dans ce module tous vos évènements importants, vos entretiens ainsi que les dates et durée de vos projets.', 'Find in this module all your important events, your interviews and also the dates and duration of your projects.', 'En este módulo, encontrarás todos tus eventos importantes, tus entrevistas así como las fechas y duración de tus proyectos. ', 'event-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(104, 'no_prop', 'Vous n''avez actuellement aucune proposition en cours', 'You have currently no current proposal', 'No tenés actualmente ninguna propuesta en curso', 'no_prop-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(105, 'derniers_match', 'Découvrez les derniers projets publiés correspondants à votre profil ', 'Discover the most recent projects matching your profile', 'Descubrí los últimos proyectos publicados que corresponden con tu perfil', 'derniers_match-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(106, 'mission_supp_success', 'Le projet a bien été supprimé de vos projets sauvegardés', 'The projetc has been deleted from your \\"Saved projects\\"', 'El proyecto ha sido eliminado de tus proyectos guardados', 'mission_supp_success-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(107, 'saved_add_success', 'Le projet a été ajouté dans vos projets sauvegardés avec succès', 'The project has been successfully added to your saved projects file', 'El proyecto ha sido añadido con éxito a tus proyectos guardados', 'saved_add_success-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(108, 'mission_supp_fail', 'Suppresion de la recherche impossible', 'The removal of this research is impossible', 'Supresión de la búsqueda imposible', 'mission_supp_fail-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(109, 'no_match_profil', 'Actuellement, aucun projet ne correspond à votre profil', 'There is currently no project matching your profile', 'No hay actualmente ningún proyecto correspondiendo a tu perfil', 'no_match_profil-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(110, 'complete_profil', 'Rendez-vous dans votre espace personnel pour compléter votre profil', 'Go to your personal space to complete in your profile\r\n', 'Entra en tu espacio personal para completar tu perfil', 'complete_profil-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(111, 'titre_filtre', 'Filtrer mes projets', 'Filter my projects', 'Filtrar mis proyectos', 'titre_filtre-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(112, 'sauvegarder', 'Sauvegarder la recherche', 'Save the research', 'Guardar la búsqueda', 'sauvegarder-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(113, 'recherche_titre', 'Titre de ma recherche', 'Title of my research', 'Título de mi búsqueda', 'recherche_titre-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(114, 'sauvegarde_titre', 'Recherches sauvegardées', 'Saved researchs', 'Búsquedas guardadas', 'sauvegarde_titre-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(115, 'titre_bis', 'Filtrer les projets', 'Filter the projects', 'Filtrar los proyectos', 'titre_bis-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(116, 'searchProjet', 'Rechercher un projet...', 'Version An', 'Version En', 'searchProjet-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(117, 'titre', 'Suivi de mes propositions', 'Follow-up of my proposals', 'Seguimiento de mis propuestas', 'titre-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(118, 'mes_props', 'Mes propositions', 'My proposals', 'Mis propuestas', 'mes_props-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(119, 'select_action', 'Sélectionner une action générale', 'Choose a action', 'Seleccionar una acción general', 'select_action-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(120, 'profilIsCompleteTo', '<h1>Votre profil</h1> <p>est complété à  : </p>', '<h1>Your profile</h1> <p>is up to : </p>', '<h1>Tu perfil</h1> <p>está completo en :</p>', 'profilIsCompleteTo-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(121, 'gerer_agence', 'Compléter ce profil', 'Complete this profile', 'Completar este perfil', 'gerer_agence-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(122, 'gerer', 'Compléter mon profil', 'Complete my profile', 'Completar mi perfil', 'gerer-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(123, 'update_agence', 'Téléchargez rapidement le CV de votre collaborateur au format pdf !', 'Quickly download the CV of your collaborator in pdf format !', '¡ Descargar rápidamente el CV de tu colaborador en formato pdf !', 'update_agence-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(124, 'update', '<span class="higher">Téléchargez </span><br/>rapidement votre CV au format pdf !', '<span class="higher">Quickly download</span><br/> your CV in pdf format ! ', '<span class="higher">¡ Subí rápidamente</span><br/> tu CV en formato pdf !', 'update-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(125, 'visualiser', 'Voir mon CV pdf', 'See my CV in pdf', 'Ver mi CV pdf', 'visualiser-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(126, 'visualiser_agence', 'Voir le CV pdf', 'See the CV in pdf', 'Ver el CV pdf', 'visualiser_agence-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(127, 'bt_update', 'Remplacer', 'Replace', 'Reemplazar', 'bt_update-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(128, 'ajouter_agence', 'Ajouter un Cv pdf', 'Add a CV in pdf', 'Añadir un Cv pdf', 'ajouter_agence-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(129, 'ajouter_cv', 'Ajouter mon Cv pdf', 'Add my CV in pdf', 'Añadir mi Cv pdf', 'ajouter_cv-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(130, 'cv_de', 'Le Cv de ', 'Cv of ', 'El Cv de', 'cv_de-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(131, 'votre_cv', 'Votre CV', 'Your CV', 'Tu Cv', 'votre_cv-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(132, 'contactTitre', 'Contactez-nous', 'Contact us', 'Contactanos', 'contactTitre-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(133, 'contactIntro', 'Un problème technique ? <br/>\r\nUne question d\\''ordre administratif ou commercial ?', 'A technical problem ? <br/>An administrative or business related question ?', '¿ Algún problema técnico ? <br/>\r\n¿ Alguna pregunta de orden administrativo o comercial ?', 'contactIntro-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(134, 'contact_bt', 'Contactez-nous', 'Contact us', 'Contactanos', 'contact_bt-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(135, 'dupliquer', 'Dupliquer', 'Duplicate', 'Duplicar', 'dupliquer-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(136, 'republier', 'Republier', 'Republish', 'Re-publicar', 'republier-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(137, 'suspendre', 'Suspendre', 'Suspend', 'Suspender', 'suspendre-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(138, 'cloturer', 'Clôturer', 'Close', 'Cerrar', 'cloturer-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(139, 'missionSaveProjet', 'Sauvegarder le projet', 'Save the project', 'Guardar el proyecto', 'missionSaveProjet-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(140, 'devis', 'Devis', 'Quote', 'Presupuesto', 'devis-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(141, 'dispo', 'Disponibilité : ', 'Availability : ', 'Disponibilidad : ', 'dispo-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(142, 'permis', 'Permis : ', 'Driving License : ', 'Licencia de conducir : ', 'permis-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(143, 'missionDetail', 'Détail du projet', 'Project details', 'Detalle del proyecto', 'missionDetail-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(144, 'rep_recues', 'Réponses reçues ', 'Received responses ', 'Respuestas recibidas', 'rep_recues-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(145, 'activer', 'Activer', 'Activate', 'Activar', 'activer-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(146, 'deplacer', 'Déplacer vers', 'Move to ', 'Desplazar a', 'deplacer-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(147, 'bt_add_cv', 'ajouter le Cv pdf', 'add the CV in pdf', 'Añadir el Cv', 'bt_add_cv-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(148, 'completer_agence', 'Pour plus de visibilité, pensez à renseigner son profil WeMe !', 'To stand out, fill in completely his/her NeoFreelo profile !', '¡ Para más visibilidad, pensá en completar tu perfil NeoFreelo !', 'completer_agence-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(149, 'completer', 'Pour plus de visibilité, pensez à renseigner votre profil WeMe !', 'To stand out, fill in completely your NeoFreelo profile !', '¡ Para más visibilidad, pensá en completar tu perfil NeoFreelo !', 'completer-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(150, 'agenceImporterPlaquetteTitre', 'Importez votre plaquette', 'Version An', 'Version En', 'agenceImporterPlaquetteTitre-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(151, 'agenceImporterPlaquette', '<span class="higher">Importez</span><br/>\r\nvotre plaquette au format PDF', 'Version An', 'Version Es', 'agenceImporterPlaquette-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(152, 'bt_telecharger', 'Modifier mon CV pdf', 'Edit my CV in pdf', 'Modificar mi CV pdf', 'bt_telecharger-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(153, 'bt_completer', 'Compléter mon profil', 'Complete my profile', 'Completar mi perfil', 'bt_completer-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(154, 'bt_telecharger_agence', 'Modifier ce Cv pdf', 'Edit this CV in pdf', 'Modificar este Cv pdf', 'bt_telecharger_agence-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(155, 'archiver', 'Archiver', 'Archive', 'Archivar', 'archiver-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(156, 'autres_projets', 'Autres projets de ce client', 'Other projects from this client', 'Otros proyectos de este cliente', 'autres_projets-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(157, 'new_prop', 'Faire une nouvelle proposition', 'Make a new proposal', 'Hacer una nueva propuesta', 'new_prop-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(158, 'bt_enable_pdf', 'réactiver le CV pdf', 'reactivate CV in pdf', 'activar el CV pdf', 'bt_enable_pdf-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16');
INSERT INTO `tra_traductions` (`id_traduction`, `libelle`, `tra_fr`, `tra_en`, `tra_es`, `trad_cle_unique`, `cree_le`, `modifie_le`) VALUES
(159, 'bt_disable_pdf', 'désactiver le CV pdf', 'deactivate CV in pdf', 'desactivar el CV pdf', 'bt_disable_pdf-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(160, 'cv_complete_titre', '¡Tu CV falta de visibilidad! ', 'Companies cannot <br>see your CV!', '¡Tu CV falta de visibilidad! ', 'cv_complete_titre-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(161, 'cv_complete_desc', '<p class="padding-10  margin-left-15 margin-right-15">\r\n		¿No querés perderte oportunidades ante las empresas?\r\n		<br>\r\n		Completá a lo menos estos 3 ítems para tener visibilidad en la CVteca y recibir propuestas de proyectos:\r\n		</p>\r\n		<ul class="starPink padding-10 margin-left-30 margin-right-15">\r\n			<li>\r\n				Encabezado de tu CV\r\n			</li>\r\n			<li>\r\n				Información Personal\r\n			</li>\r\n			<li>\r\n				Mis aptitudes profesionales\r\n			</li>\r\n		</ul>', '<p class="padding-10  margin-left-15 margin-right-15">\r\nIf you want to be visible in <br>\r\nthe CV library and not miss a job opportunity, please <b>fill in</b> at least <br>\r\nthe <b>3 following items</b> :\r\n		</p>\r\n		<ul class="starPink padding-10 margin-left-30 margin-right-15">\r\n			<li>\r\n			CV header\r\n			</li>\r\n			<li>\r\n			Personal data\r\n			</li>\r\n			<li>\r\n				My skills\r\n			</li>\r\n		</ul>', '<p class="padding-10  margin-left-15 margin-right-15">\r\n		¿No querés perderte oportunidades ante las empresas?\r\n		<br>\r\n		Para que te vean en la CVteca, <b>completá</b> a lo menos estos <b>3 ítems</b> :\r\n		</p>\r\n		<ul class="starPink padding-10 margin-left-30 margin-right-15">\r\n			<li>\r\n				Encabezado del CV\r\n			</li>\r\n			<li>\r\n				Información Personal\r\n			</li>\r\n			<li>\r\n				Mis aptitudes profesionales\r\n			</li>\r\n		</ul>', 'cv_complete_desc-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(162, 'cv_complete_linked', 'Complétez avec', 'Complete with', 'completar via', 'cv_complete_linked-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(163, 'prestaNoMission', 'Aucun projet trouvé', 'No matching project', 'Ningún proyecto encontrado', 'prestaNoMission-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(164, 'prestaSeeAllProjet', 'Voir tous les projets', 'Version An', 'Version Es', 'prestaSeeAllProjet-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(165, 'prestaSeeAllProp', 'Voir mes propositions', 'Version An', 'Version Es', 'prestaSeeAllProp-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(166, 'prestaCvAstuceTitre', 'Astuce', 'Version An', 'Version En', 'prestaCvAstuceTitre-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(167, 'prestaCvAstuce1', 'plus vous completez votre CV, plus vous ameliorez sa visibilite', 'Version An', 'Versino En', 'prestaCvAstuce1-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(168, 'prestaImporterCvTitre', 'Importez votre CV PDF', 'Version An', 'Version Es', 'prestaImporterCvTitre-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(169, 'prestaImporterCvRules', '<p>\r\n					Vous êtes sur le point d''importer votre CV.\r\n					<br>\r\n					<br>\r\n					Merci de respecter les conditions suivantes    \r\n				    </p>\r\n				    <ul>\r\n					<li>Fichier inferieur à 1mo</li>\r\n					<li>Fichier en PDF non aplati</li>\r\n					<li>Pas de coordonée personnels</li>\r\n				    </ul>', 'version AN', 'Version ES', 'prestaImporterCvRules-P7', '2013-08-30 11:43:16', '2013-08-30 13:43:16'),
(170, 'menu_saved_project', 'Mes projets sauvegardés', 'My saved projects', 'Mis proyectos guardados', 'menu_saved_project-P8', '2013-08-30 11:43:33', '2013-08-30 13:43:33'),
(171, 'menu_mon_profil', 'Mon profil', 'My profile', 'Mi perfil', 'menu_mon_profil-P8', '2013-08-30 11:43:33', '2013-08-30 13:43:33'),
(172, 'menu_mon_curriculum', 'Mon parcours', 'My background', 'Mi carrera', 'menu_mon_curriculum-P8', '2013-08-30 11:43:33', '2013-08-30 13:43:33'),
(173, 'menu_mon_portfolio', 'Mes réalisations', 'My creations', 'Mi portfolio', 'menu_mon_portfolio-P8', '2013-08-30 11:43:33', '2013-08-30 13:43:33'),
(174, 'menu_mes_profils', 'Mes profils', 'My profiles', 'Mis perfiles', 'menu_mes_profils-P8', '2013-08-30 11:43:33', '2013-08-30 13:43:33'),
(175, 'menu_mon_compte', 'Mon compte', 'my account', 'Mi cuenta', 'menu_mon_compte-P8', '2013-08-30 11:43:33', '2013-08-30 13:43:33'),
(176, 'menu_vos_profils', 'Mes profils', 'My profiles', 'Mis perfiles', 'menu_vos_profils-P8', '2013-08-30 11:43:33', '2013-08-30 13:43:33'),
(177, 'menu_mon_cv', 'Mon CV', 'My CV', 'Mi CV', 'menu_mon_cv-P8', '2013-08-30 11:43:33', '2013-08-30 13:43:33'),
(178, 'menu_tableau_bord', 'Tableau de bord', 'Dashboard', 'Panel de Control', 'menu_tableau_bord-P8', '2013-08-30 11:43:33', '2013-08-30 13:43:33'),
(179, 'menu_boite_reception', 'boite de réception', 'inbox', 'Buzón', 'menu_boite_reception-P8', '2013-08-30 11:43:33', '2013-08-30 13:43:33'),
(180, 'menu_rechercher_projets', 'rechercher des projets', 'search for projects', 'Buscar proyectos', 'menu_rechercher_projets-P8', '2013-08-30 11:43:33', '2013-08-30 13:43:33'),
(181, 'menu_mes_propositions', 'Mes propositions', 'my proposals', 'Mis propuestas', 'menu_mes_propositions-P8', '2013-08-30 11:43:33', '2013-08-30 13:43:33'),
(182, 'menu_mes_entretiens', 'mes entretiens', 'my interviews', 'Mis entrevistas', 'menu_mes_entretiens-P8', '2013-08-30 11:43:33', '2013-08-30 13:43:33'),
(183, 'menu_accueil', 'Accueil', 'Home', 'Inicio', 'menu_accueil-P8', '2013-08-30 11:43:33', '2013-08-30 13:43:33'),
(184, 'menu_trouver_prestataire', 'Trouver un prestataire', 'Find a Freelancer', 'Encontrar un freelance', 'menu_trouver_prestataire-P8', '2013-08-30 11:43:33', '2013-08-30 13:43:33'),
(185, 'menu_trouver_projet', 'Trouver un projet', 'Find a Project', 'Encontrar un proyecto', 'menu_trouver_projet-P8', '2013-08-30 11:43:33', '2013-08-30 13:43:33'),
(186, 'menu_inscription', 'Inscription', 'Sign Up', 'Inscripción', 'menu_inscription-P8', '2013-08-30 11:43:33', '2013-08-30 13:43:33'),
(187, 'menu_gerer_projets', 'gérer mes projets', 'Manage my projects', 'gestionar mis proyectos', 'menu_gerer_projets-P8', '2013-08-30 11:43:33', '2013-08-30 13:43:33'),
(188, 'menu_cvteque', 'cvthèque', 'Cv library', 'CVteca', 'menu_cvteque-P8', '2013-08-30 11:43:33', '2013-08-30 13:43:33'),
(189, 'hpLangChooser', 'Français', 'English', 'Español', 'hpLangChooser-P8', '2013-08-30 11:43:33', '2013-08-30 13:43:33'),
(190, 'francais', 'Français', 'French', 'Francès', 'francais-P8', '2013-08-30 11:43:33', '2013-08-30 13:43:33'),
(191, 'anglais', 'Anglais', 'English', 'Inglès', 'anglais-P8', '2013-08-30 11:43:33', '2013-08-30 13:43:33'),
(192, 'espagnol', 'Espagnol', 'Spanish', 'Español', 'espagnol-P8', '2013-08-30 11:43:33', '2013-08-30 13:43:33'),
(193, 'bt_edit_profil', 'modifier mon profil', 'edit my profile', 'modificar mi perfil', 'bt_edit_profil-P8', '2013-08-30 11:43:33', '2013-08-30 13:43:33'),
(194, 'bt_projets', 'projets', 'projects', 'proyectos', 'bt_projets-P8', '2013-08-30 11:43:33', '2013-08-30 13:43:33'),
(195, 'bt_message', 'message', 'message', 'mensaje', 'bt_message-P8', '2013-08-30 11:43:33', '2013-08-30 13:43:33'),
(196, 'footerAccueil', 'Accueil', 'Home', 'Inicio', 'footerAccueil-P9', '2013-08-30 11:43:44', '2013-08-30 13:43:44'),
(197, 'footerAPropos', 'A propos', 'About us', 'Sobre nosotros', 'footerAPropos-P9', '2013-08-30 11:43:44', '2013-08-30 13:43:44'),
(198, 'footerMentions', 'Informations légales', 'Legal information', 'Informaciones legales', 'footerMentions-P9', '2013-08-30 11:43:44', '2013-08-30 13:43:44'),
(199, 'footerSitemap', 'Sitemap', 'Sitemap', 'Mapa web', 'footerSitemap-P9', '2013-08-30 11:43:44', '2013-08-30 13:43:44'),
(200, 'footerContact', 'Contact', 'Contact', 'Contacto', 'footerContact-P9', '2013-08-30 11:43:44', '2013-08-30 13:43:44'),
(201, 'footerCopyright', 'Copyright 2012 - WeMe - marque déposée', '', '', 'footerCopyright-P9', '2013-08-30 11:43:44', '2013-08-30 13:43:44'),
(202, 'footerLiensUtiles', 'Liens Utiles', 'Useful links', 'Enlaces de interes', 'footerLiensUtiles-P9', '2013-08-30 11:43:44', '2013-08-30 13:43:44'),
(203, 'footerClientProjet', 'Créer votre projet', 'Version Anglaise', 'Version Espagnol', 'footerClientProjet-P9', '2013-08-30 11:43:44', '2013-08-30 13:43:44'),
(204, 'footerEntrepreneur', 'Entrepreneurs', 'Entre EN', 'Entre ES', 'footerEntrepreneur-P9', '2013-08-30 11:43:44', '2013-08-30 13:43:44'),
(205, 'footerPrestataire', 'Prestataire', 'Presta En ', 'Presta Es', 'footerPrestataire-P9', '2013-08-30 11:43:44', '2013-08-30 13:43:44'),
(206, 'label_titre_partenaire', 'Nos <br/>partenaires', 'Our <br/>Partners', 'Nuestros <br/>Aliados', 'label_titre_partenaire-P9', '2013-08-30 11:43:44', '2013-08-30 13:43:44'),
(207, 'description_partenaire', 'Texte de description pour leur dire que s''ils veulent devenir notre partenaire, qu''ils nous contactent', 'Texte de description pour leur dire que s''ils veulent devenir notre partenaire, qu''ils nous contactent', 'Texte de description pour leur dire que s''ils veulent devenir notre partenaire, qu''ils nous contactent', 'description_partenaire-P9', '2013-08-30 11:43:44', '2013-08-30 13:43:44'),
(208, 'footerClientCvtheque', 'Explorez notre CVthèque', 'Version Anglaise', 'Version Espagnol', 'footerClientCvtheque-P9', '2013-08-30 11:43:44', '2013-08-30 13:43:44'),
(209, 'footerClientSourcing', 'Nos solutions Sourcing', 'Version An', 'Version En', 'footerClientSourcing-P9', '2013-08-30 11:43:44', '2013-08-30 13:43:44'),
(210, 'footerPrestaCv', 'Déposer votre CV', 'Version An', 'Version Es', 'footerPrestaCv-P9', '2013-08-30 11:43:44', '2013-08-30 13:43:44'),
(211, 'footerPrestaIntermittent', 'Nos solutions intermittents', 'Version An', 'Version En', 'footerPrestaIntermittent-P9', '2013-08-30 11:43:44', '2013-08-30 13:43:44'),
(212, 'footerPrestaProjet', 'Consulter les projets', 'Version An', 'Version Es', 'footerPrestaProjet-P9', '2013-08-30 11:43:44', '2013-08-30 13:43:44'),
(213, 'infos', 'Mes informations générales', 'General information', 'Mis informaciones generales', 'infos-P10', '2013-08-30 11:43:57', '2013-08-30 13:43:57'),
(214, 'ident', 'Mes identifiants de connexion', 'My NeoFreelo ID', 'Mis identificadores de conexión', 'ident-P10', '2013-08-30 11:43:57', '2013-08-30 13:43:57'),
(215, 'photo_titre', 'Ma photo de profil', 'My profile picture', 'Mi foto de perfil', 'photo_titre-P10', '2013-08-30 11:43:57', '2013-08-30 13:43:57'),
(216, 'imp_photo', 'Importer ma photo', 'Import my picture', 'Importar mi foto', 'imp_photo-P10', '2013-08-30 11:43:57', '2013-08-30 13:43:57'),
(217, 'photo_modif', 'Modifier ma photo', 'Edit my picture', 'Modificar mi foto', 'photo_modif-P10', '2013-08-30 11:43:57', '2013-08-30 13:43:57'),
(218, 'photo', 'Ma photo', 'My picture', 'Mi foto', 'photo-P10', '2013-08-30 11:43:57', '2013-08-30 13:43:57'),
(219, 'recadrer_photo', 'Recadrer ma photo', 'Crop my picture', 'Reencuadrar mi foto', 'recadrer_photo-P10', '2013-08-30 11:43:57', '2013-08-30 13:43:57'),
(220, 'juridique', 'Juridique', 'Legal', 'Jurídico', 'juridique-P10', '2013-08-30 11:43:57', '2013-08-30 13:43:57'),
(221, 'config_notif', 'Configuration de vos alertes email', 'Setting up your email alerts', 'Configurar mis notificaciones por email', 'config_notif-P10', '2013-08-30 11:43:57', '2013-08-30 13:43:57'),
(222, 'ignorer', 'Annuler l''importation de cette photo', 'Cancel the import of this picture', 'Cancelar la importación de esta foto', 'ignorer-P10', '2013-08-30 11:43:57', '2013-08-30 13:43:57'),
(223, 'prestaImporterPhotoRules', 'Vous êtes sur le point de modifier votre avatar.\r\n                                            <br>\r\n                                            <br>\r\n                                            Merci de fournir un fichier jpeg inferieur à 1 mo.\r\n                                            <br>\r\n                                            <br>\r\n                                            Une fois l''importation terminée vous pourrez redimensionner votre photo et l''enregistrer', 'Version En', 'Version Es', 'prestaImporterPhotoRules-P10', '2013-08-30 11:43:57', '2013-08-30 13:43:57'),
(224, 'agenceImporterPlaquetteTitre', 'Importez votre plaquette', 'Version An', 'Version En', 'agenceImporterPlaquetteTitre-P10', '2013-08-30 11:43:57', '2013-08-30 13:43:57'),
(225, 'agenceImporterPlaquette', '<span class="higher">Importez</span><br/>\r\nvotre plaquette au format PDF', 'Version An', 'Version Es', 'agenceImporterPlaquette-P10', '2013-08-30 11:43:57', '2013-08-30 13:43:57'),
(226, 'agenceDescription', 'Description de ma société', 'Description of my company\\', 'Descripción de mi empresa', 'agenceDescription-P10', '2013-08-30 11:43:57', '2013-08-30 13:43:57'),
(227, 'infos_persos', 'Informations personelles', 'Personal data', 'Información personal', 'infos_persos-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(228, 'infos_persosTitre', 'Mes informations personelles', 'My personal data', 'Mis información personal', 'infos_persosTitre-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(229, 'entete_cv', 'Entête du CV', 'CV header', 'Encabezado del CV', 'entete_cv-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(230, 'modif_infos_persos', 'Modifier mes informations personnelles', 'Edit my personal data', 'Modificar mi información personal', 'modif_infos_persos-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(231, 'exp_pro', 'Expériences professionnelles ', 'Work experiences', 'Experiencias profesionales', 'exp_pro-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(232, 'exp_pro_short', 'Expériences', 'Experiences', 'Experiencias', 'exp_pro_short-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(233, 'exp_proTitre', 'Mes expériences professionnelles ', 'My work experiences', 'Mis experiencias profesionales', 'exp_proTitre-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(234, 'add_exp_pro', 'Ajouter une expérience professionnelle', 'Add a work experience', 'Añadir una experiencia profesional', 'add_exp_pro-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(235, 'edit_exp_pro', 'Modifier expérience professionnelle', 'Edit my work experience', 'Modificar mi experiencia profesional', 'edit_exp_pro-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(236, 'form', 'Formations', 'Education', 'Formaciones', 'form-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(237, 'formTitre', 'Mes Formations', 'My Education', 'Mis Formaciones', 'formTitre-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(238, 'add_form', 'Ajouter une formation', 'Add a school', 'Añadir una formación', 'add_form-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(239, 'edit_form', 'Modifier ma formation', 'Edit my school', 'modificar mi formación', 'edit_form-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(240, 'diplomes', 'Vos diplômes', 'Your qualifications or degrees', 'Tu diplomas', 'diplomes-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(241, 'rea', 'Mes réalisations', 'My creations', 'Mi portfolio', 'rea-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(242, 'comp', 'Compétences', 'Skills', 'Aptitudes profesionales', 'comp-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(243, 'compTitre', 'Mes compétences', 'My skills', 'Mis aptitudes profesionales', 'compTitre-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(244, 'langues', 'Mes langues maitrisées', 'Languages', 'Mis idiomas', 'langues-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(245, 'modif_entete_cv', 'Modifier l''entête du cv', 'Edit the CV header', 'Modificar el encabezado del CV', 'modif_entete_cv-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(246, 'modif_exp_pro', 'Modifier cette expérience professionnelle', 'Edit this work experience', 'Modificar esta experiencia profesional', 'modif_exp_pro-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(247, 'modif_form', 'Modifier cette formation', 'Edit this school', 'Modificar esta formación', 'modif_form-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(248, 'choix_comp', 'Saisissez vos compétences', 'Enter your skills', 'Introduce tus aptitudes profesionales', 'choix_comp-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(249, 'choix_langue', 'Saisissez les langues maîtrisées', 'Enter the languages you know', 'Introduce los idiomas', 'choix_langue-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(250, 'choix_poste', 'Saisissez le(s) type(s) de poste(s) souhaité(s)', 'Enter the desired position(s)', 'Introduce el tipo(s) de puesto(s) deseado(s)', 'choix_poste-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(251, 'choix_mob', 'Saisissez votre mobilité', 'Enter your mobility', 'Introduce tu movilidad', 'choix_mob-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(252, 'profilIsCompleteTo', '<h1>Votre profil</h1> <p>est complété à  : </p>', '<h1>Your profile</h1> <p>is up to : </p>', '<h1>Tu perfil</h1> <p>está completo en :</p>', 'profilIsCompleteTo-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(253, 'gerer_agence', 'Compléter ce profil', 'Complete this profile', 'Completar este perfil', 'gerer_agence-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(254, 'gerer', 'Compléter mon profil', 'Complete my profile', 'Completar mi perfil', 'gerer-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(255, 'update_agence', 'Téléchargez rapidement le CV de votre collaborateur au format pdf !', 'Quickly download the CV of your collaborator in pdf format !', '¡ Descargar rápidamente el CV de tu colaborador en formato pdf !', 'update_agence-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(256, 'update', '<span class="higher">Téléchargez </span><br/>rapidement votre CV au format pdf !', '<span class="higher">Quickly download</span><br/> your CV in pdf format ! ', '<span class="higher">¡ Subí rápidamente</span><br/> tu CV en formato pdf !', 'update-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(257, 'visualiser', 'Voir mon CV pdf', 'See my CV in pdf', 'Ver mi CV pdf', 'visualiser-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(258, 'visualiser_agence', 'Voir le CV pdf', 'See the CV in pdf', 'Ver el CV pdf', 'visualiser_agence-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(259, 'bt_update', 'Remplacer', 'Replace', 'Reemplazar', 'bt_update-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(260, 'ajouter_agence', 'Ajouter un Cv pdf', 'Add a CV in pdf', 'Añadir un Cv pdf', 'ajouter_agence-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(261, 'ajouter_cv', 'Ajouter mon Cv pdf', 'Add my CV in pdf', 'Añadir mi Cv pdf', 'ajouter_cv-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(262, 'cv_de', 'Le Cv de ', 'Cv of ', 'El Cv de', 'cv_de-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(263, 'votre_cv', 'Votre CV', 'Your CV', 'Tu Cv', 'votre_cv-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(264, 'bt_add_cv', 'ajouter le Cv pdf', 'add the CV in pdf', 'Añadir el Cv', 'bt_add_cv-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(265, 'completer_agence', 'Pour plus de visibilité, pensez à renseigner son profil WeMe !', 'To stand out, fill in completely his/her NeoFreelo profile !', '¡ Para más visibilidad, pensá en completar tu perfil NeoFreelo !', 'completer_agence-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(266, 'completer', 'Pour plus de visibilité, pensez à renseigner votre profil WeMe !', 'To stand out, fill in completely your NeoFreelo profile !', '¡ Para más visibilidad, pensá en completar tu perfil NeoFreelo !', 'completer-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(267, 'no_formation', 'Vous n''avez actuellement aucune formation d''enregistrée. <br/>Pour en renseigner une, cliquez sur le bouton "AJOUTER"', 'You have currently no school registered. <br/>To add one, please click on the button \\"ADD\\"', 'No tienes ninguna formación registrada.<br/>Para poner una, hacé clic sobre el botón "AÑADIR"', 'no_formation-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(268, 'modif_formation', 'Modifier cette formation', 'Edit this school', 'Modificar esta formación', 'modif_formation-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(269, 'no_rea', 'Vous n''avez actuellement aucune réalisation d''enregistrée. <br/>Pour en renseigner une, cliquez sur le bouton "AJOUTER"', 'You have currently no creation registered. <br/>To add one, please click on the button \\"ADD\\"', 'No tienes ninguna realización registrada.<br/>Para poner una, hacé clic sobre el botón "AÑADIR"', 'no_rea-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(270, 'no_exp', 'Vous n''avez actuellement aucune expérience professionnelle d''enregistrée. <br/>Pour en renseigner une, cliquez sur le bouton "AJOUTER"', 'You have currently no work experience registered. <br/>To add one, please click on the button \\"ADD\\" ', 'No tenés ninguna experiencia profesional registrada.<br/>Para poner una, hacé clic en el botón "AÑADIR"', 'no_exp-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(271, 'infos_comp', 'Informations complémentaires', 'Additional information', 'Información complementaria', 'infos_comp-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(272, 'utilisation_service', 'Importation de fichier', 'Uploads', 'Importación de documentos', 'utilisation_service-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(273, 'agenceImporterPlaquetteTitre', 'Importez votre plaquette', 'Version An', 'Version En', 'agenceImporterPlaquetteTitre-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(274, 'agenceImporterPlaquette', '<span class="higher">Importez</span><br/>\r\nvotre plaquette au format PDF', 'Version An', 'Version Es', 'agenceImporterPlaquette-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(275, 'bt_telecharger', 'Modifier mon CV pdf', 'Edit my CV in pdf', 'Modificar mi CV pdf', 'bt_telecharger-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(276, 'bt_completer', 'Compléter mon profil', 'Complete my profile', 'Completar mi perfil', 'bt_completer-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(277, 'bt_telecharger_agence', 'Modifier ce Cv pdf', 'Edit this CV in pdf', 'Modificar este Cv pdf', 'bt_telecharger_agence-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(278, 'bt_enable_pdf', 'réactiver le CV pdf', 'reactivate CV in pdf', 'activar el CV pdf', 'bt_enable_pdf-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(279, 'bt_disable_pdf', 'désactiver le CV pdf', 'deactivate CV in pdf', 'desactivar el CV pdf', 'bt_disable_pdf-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(280, 'explication_service', 'Seul les fichiers DOC, XLS, PDF, JPG, PNG et GIF sont acceptés.\r\n<br>', 'Only DOC, XLS, PDF, JPG, PNG and GIF files may be uploaded.', 'Solo los documentos DOC, XLS, PDF, JPG, PNG y GIF son autorizados.', 'explication_service-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(281, 'cv_complete_titre', '¡Tu CV falta de visibilidad! ', 'Companies cannot <br>see your CV!', '¡Tu CV falta de visibilidad! ', 'cv_complete_titre-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(282, 'cv_complete_desc', '<p class="padding-10  margin-left-15 margin-right-15">\r\n		¿No querés perderte oportunidades ante las empresas?\r\n		<br>\r\n		Completá a lo menos estos 3 ítems para tener visibilidad en la CVteca y recibir propuestas de proyectos:\r\n		</p>\r\n		<ul class="starPink padding-10 margin-left-30 margin-right-15">\r\n			<li>\r\n				Encabezado de tu CV\r\n			</li>\r\n			<li>\r\n				Información Personal\r\n			</li>\r\n			<li>\r\n				Mis aptitudes profesionales\r\n			</li>\r\n		</ul>', '<p class="padding-10  margin-left-15 margin-right-15">\r\nIf you want to be visible in <br>\r\nthe CV library and not miss a job opportunity, please <b>fill in</b> at least <br>\r\nthe <b>3 following items</b> :\r\n		</p>\r\n		<ul class="starPink padding-10 margin-left-30 margin-right-15">\r\n			<li>\r\n			CV header\r\n			</li>\r\n			<li>\r\n			Personal data\r\n			</li>\r\n			<li>\r\n				My skills\r\n			</li>\r\n		</ul>', '<p class="padding-10  margin-left-15 margin-right-15">\r\n		¿No querés perderte oportunidades ante las empresas?\r\n		<br>\r\n		Para que te vean en la CVteca, <b>completá</b> a lo menos estos <b>3 ítems</b> :\r\n		</p>\r\n		<ul class="starPink padding-10 margin-left-30 margin-right-15">\r\n			<li>\r\n				Encabezado del CV\r\n			</li>\r\n			<li>\r\n				Información Personal\r\n			</li>\r\n			<li>\r\n				Mis aptitudes profesionales\r\n			</li>\r\n		</ul>', 'cv_complete_desc-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(283, 'cv_complete_linked', 'Complétez avec', 'Complete with', 'completar via', 'cv_complete_linked-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(284, 'prestaCvAstuceTitre', 'Astuce', 'Version An', 'Version En', 'prestaCvAstuceTitre-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(285, 'prestaCvAstuce1', 'plus vous completez votre CV, plus vous ameliorez sa visibilite', 'Version An', 'Versino En', 'prestaCvAstuce1-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(286, 'prestaImporterCvTitre', 'Importez votre CV PDF', 'Version An', 'Version Es', 'prestaImporterCvTitre-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(287, 'prestaImporterCvRules', '<p>\r\n					Vous êtes sur le point d''importer votre CV.\r\n					<br>\r\n					<br>\r\n					Merci de respecter les conditions suivantes    \r\n				    </p>\r\n				    <ul>\r\n					<li>Fichier inferieur à 1mo</li>\r\n					<li>Fichier en PDF non aplati</li>\r\n					<li>Pas de coordonée personnels</li>\r\n				    </ul>', 'version EN', 'Version ES', 'prestaImporterCvRules-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(288, 'prestaCvCompleteprc', 'de votre profil est complété', 'Version En', 'Version Es', 'prestaCvCompleteprc-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(289, 'prestaCvSituationPro', 'Situation professionnelle', 'Version En', 'Version Es', 'prestaCvSituationPro-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(290, 'prestaDescProfil', 'Description de mon profil', 'Description of my profil', 'Descripción de mi perfil', 'prestaDescProfil-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(291, 'prestaDescExp', 'Description de l''éxpérience', 'Description of the experiences', 'Descripción de la experiencias', 'prestaDescExp-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(292, 'prestaDescForm', 'Description de la formations', 'Description of the education', 'Descripción de la formaciones', 'prestaDescForm-P11', '2013-08-30 11:44:08', '2013-08-30 13:44:08'),
(293, 'realisationTitre', 'Mes réalisations', 'My creations', 'Mi portfolio', 'realisationTitre-P12', '2013-08-30 11:44:22', '2013-08-30 13:44:22'),
(294, 'noRealisations', 'Vous n''avez actuellement aucune réalisation d''enregistrée. <br/>Pour en renseigner une, cliquez sur le bouton "AJOUTER"', 'You have currently no creation registered. <br/>To add one, please click on the button "ADD"', 'No tienes ninguna realización registrada.<br/>Para poner una, hacé clic sobre el botón "AÑADIR"', 'noRealisations-P12', '2013-08-30 11:44:22', '2013-08-30 13:44:22'),
(295, 'importationReaTitre', 'Importation de fichier', 'Uploads', 'Importación de documentos', 'importationReaTitre-P12', '2013-08-30 11:44:22', '2013-08-30 13:44:22'),
(296, 'importationReaConditions', '<p>Vous êtes sur le point d''ajouter une réalisation.<br><br>Merci de respecter les conditions suivantes</p><ul><li>Fichier inferieur à 1mo</li><li>Pas de fichier à caractere raciste</li><li>Pas de fichier à caractere pornographique</li></ul>', 'Only DOC, XLS, PDF, JPG, PNG and GIF files may be uploaded.', 'Solo los documentos DOC, XLS, PDF, JPG, PNG y GIF son autorizados.', 'importationReaConditions-P12', '2013-08-30 11:44:22', '2013-08-30 13:44:22'),
(297, 'modificationRealisation', 'Modifier une réalisation', 'Edit a creation', 'Modificar un documentos', 'modificationRealisation-P12', '2013-08-30 11:44:22', '2013-08-30 13:44:22'),
(298, 'realisationType', 'Type de fichier : ', 'Version En ', 'Version Es ', 'realisationType-P12', '2013-08-30 11:44:22', '2013-08-30 13:44:22'),
(299, 'realisationDate', 'Date de telechargement : ', 'Version En', 'Version Es', 'realisationDate-P12', '2013-08-30 11:44:22', '2013-08-30 13:44:22'),
(300, 'realisationTaille', 'Taille de fichier : ', 'Version En ', 'Version Es ', 'realisationTaille-P12', '2013-08-30 11:44:22', '2013-08-30 13:44:22'),
(301, 'realisationResolution', 'Type de fichier : ', 'Version En ', 'Version Es ', 'realisationResolution-P12', '2013-08-30 11:44:22', '2013-08-30 13:44:22'),
(302, 'titre', 'Les projets WeMe', '<span itemprop="about"><span itemprop="name">NeoFreelo</span> projects</span>', '<span itemprop="about">Los proyectos <span itemprop="name">NeoFreelo</span></span>', 'titre-P13', '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(303, 'intro', '<b>Vous recherchez des projets et des collaborateurs à la hauteur de vos ambitions :</b><br/> Utilisez notre moteur de recherche avec ses filtres par critères. <br/>Si des projets vous intéressent, vous pouvez les sauvegarder afin d''y répondre plus tard.<br/>WeMe c''est aussi partager des projets, en les transmettant par email à votre réseau !', '<b>You are looking for <span itemprop="keywords">projects</span> and <span itemprop="keywords">contractors</span> at the same height as your ambitions : </b><br/> Use our <span itemprop="keywords">search engine</span> with our filters per criteria. <br/>If you are interested in any project, you can save them and reply to them later. <br/><span itemprop="keywords">NeoFreelo</span> is also about sharing projects via email to your network !', '<b>Buscás <span itemprop="keywords">proyectos</span> y <span itemprop="keywords">colaboradores</span> a la altura de tus aspiraciones : </b><br/>Utilizá nuestro <span itemprop="keywords">motor de búsqueda</span> con los filtros por criterios. <br/>Si algunos proyectos te interesan, podés guardarlos para contestarlos más tarde.<br/>¡ <span itemprop="keywords">NeoFreelo</span> es también compartir proyectos, transmitiéndolos por email a tu red !', 'intro-P13', '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(304, 'projets', 'Projets en cours', 'Current projects', 'Proyectos en curso', 'projets-P13', '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(305, 'need_abo', 'certaines actions ne sont accessibles qu\\''une fois <span class=\\"pink\\">connecté</span>', 'Some actions are only available once <span class=\\"pink\\">connected</span>', 'algunas acciones sólo son accesibles una vez <span class=\\"pink\\">conectado</span>', 'need_abo-P13', '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(306, 'searchProjet', 'Rechercher un projet...', 'Version EN', 'Version ES', 'searchProjet-P13', '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(307, 'transfere', 'Transférer à un ami', 'Forward to a friend', 'Transferir a un amigo', 'transfere-P13', '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(308, 'sauvegarder', 'Sauvegarder la recherche', 'Save the research', 'Guardar la búsqueda', 'sauvegarder-P13', '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(309, 'recherche_titre', 'Titre de ma recherche', 'Title of my research', 'Título de mi búsqueda', 'recherche_titre-P13', '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(310, 'sauvegarde_titre', 'Recherches sauvegardées', 'Saved researchs', 'Búsquedas guardadas', 'sauvegarde_titre-P13', '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(311, 'titre_bis', 'Filtrer les projets', 'Filter the projects', 'Filtrar los proyectos', 'titre_bis-P13', '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(312, 'search_saved_ok', 'Votre recherche a été sauvegardée avec succès', 'Your research has been successfully saved', 'Tu búsqueda ha sido guardada con éxito', 'search_saved_ok-P13', '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(313, 'no_projets', 'Actuellement, aucun projet en cours correspond à votre recherche', 'There is currently no matching result to your search', 'Actualmente, ningún proyecto en curso corresponde a tu búsqueda', 'no_projets-P13', '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(314, 'saved_add_success', 'Le projet a été ajouté dans vos projets sauvegardés avec succès', 'The project has been successfully added to your saved projects list', 'El proyecto ha sido añadido dentro de tus proyectos guardados', 'saved_add_success-P13', '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(315, 'send_research', 'Votre recherche a bien été envoyé à ', 'Your research has been sent to ', 'Tu búsqueda ha sido enviada a ', 'send_research-P13', '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(316, 'saved_supp_success', 'Votre recherche a bien été supprimée', 'Your research has been deleted', 'Tu búsqueda ha sido suprimida', 'saved_supp_success-P13', '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(317, 'saved_supp_fail', 'Suppresion de la recherche impossible', 'Removal of the research is impossible', 'Supresión de la búsqueda imposible', 'saved_supp_fail-P13', '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(318, 'mission_supp_success', 'Le projet a bien été supprimé de vos projets sauvegardés', 'The project has been deleted from your Saved projects list', 'El proyecto ha sido suprimido de tus proyectos guardados', 'mission_supp_success-P13', '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(319, 'dupliquer', 'Dupliquer', 'Duplicate', 'Duplicar', 'dupliquer-P13', '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(320, 'republier', 'Republier', 'Republish', 'Re-publicar', 'republier-P13', '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(321, 'suspendre', 'Suspendre', 'Suspend', 'Suspender', 'suspendre-P13', '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(322, 'cloturer', 'Clôturer', 'Close', 'Cerrar', 'cloturer-P13', '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(323, 'missionRepondre', 'Répondre à cette offre', 'Reply to this offer', 'Responder a esta oferta', 'missionRepondre-P13', '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(324, 'missionSaveProjet', 'Sauvegarder le projet', 'Save the project', 'Guardar el proyecto', 'missionSaveProjet-P13', '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(325, 'devis', 'Devis', 'Quote', 'Presupuesto', 'devis-P13', '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(326, 'missionDetail', 'Détail du projet', 'Project details', 'Detalle del proyecto', 'missionDetail-P13', '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(327, 'rep_recues', 'Réponses reçues ', 'Received responses ', 'Respuestas recibidas', 'rep_recues-P13', '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(328, 'activer', 'Activer', 'Activate', 'Activar', 'activer-P13', '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(329, 'deplacer', 'Déplacer vers', 'Move to ', 'Desplazar a', 'deplacer-P13', '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(330, 'supp_mission_fail', 'Suppression du projet impossible', 'Removal of the project is impossible', 'Supresión del proyecto imposible', 'supp_mission_fail-P13', '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(331, 'missionNbReponses', 'Réponses reçues', 'Received responses', 'Respuestas recibidas', 'missionNbReponses-P13', '2013-08-30 11:44:36', '2013-08-30 13:44:36'),
(332, 'prestaReponsesNoPropositions', 'Aucune proposition de diponible pour la rechercheé', 'Version En', 'Version Es', 'prestaReponsesNoPropositions-P14', '2013-08-30 11:53:17', '2013-08-30 13:53:17'),
(333, 'detailMissionReponseTitre', 'Ma réponse', 'My answer', 'Mi respuesta', 'detailMissionReponseTitre-P14', '2013-08-30 11:53:17', '2013-08-30 13:53:17'),
(334, 'detailMissionSaveReponse', 'Envoyer ma réponse', 'Send my answer', 'Enviar mi respuesta', 'detailMissionSaveReponse-P14', '2013-08-30 11:53:17', '2013-08-30 13:53:17'),
(335, 'detailMissionCommDefaut', 'Commentaire de votre réponse', 'Your answer''s comment', 'Comentario de tu respuesta', 'detailMissionCommDefaut-P14', '2013-08-30 11:53:17', '2013-08-30 13:53:17'),
(336, 'prestaEntretienTitre1', 'A propos de l''entretien sélectionnée', 'About the selected interview', 'Sobre la entrevista seleccionada', 'prestaEntretienTitre1-P15', '2013-08-30 11:55:28', '2013-08-30 13:55:28'),
(337, 'prestaEntretienCode', 'code d''accès', 'access code', 'codigo de acceso', 'prestaEntretienCode-P15', '2013-08-30 11:55:28', '2013-08-30 13:55:28'),
(338, 'prestaEntretienTitre2', 'Les entretiens vidéo WeMe', 'Version En', 'Version Es', 'prestaEntretienTitre2-P15', '2013-08-30 11:55:28', '2013-08-30 13:55:28'),
(339, 'prestaEntretienTitre3', 'Liste de vos entretiens vidéos', 'Version En', 'Version Es', 'prestaEntretienTitre3-P15', '2013-08-30 11:55:28', '2013-08-30 13:55:28'),
(340, 'prestaEntretienIntro', 'Créez en un tour de main un entretien vidéo, individuel ou collectif, en invitant un ou plusieurs prestataires de vos favoris. Une fois l’entretien planifié, suivez depuis cette page l’évolution de l’invitation auprès du (des) participant(s).', 'Start easily an individual or collective video call by inviting one or more freelancers from your favourites. Once the interview has been planned, follow the evolution of your invitation to the freelancer(s) from this page.', 'Podés crear una entrevista video, colectiva o individual, invitando a uno o varios freelances de tus favoritos. Una vez que está planificada la entrevista, podés seguir la evolución de la invitación del o los participante(s) desde esta página.', 'prestaEntretienIntro-P15', '2013-08-30 11:55:28', '2013-08-30 13:55:28'),
(341, 'prestaEntretienLabelInvite', 'Invité', 'freelancer', 'Invitado', 'prestaEntretienLabelInvite-P15', '2013-08-30 11:55:28', '2013-08-30 13:55:28'),
(342, 'prestaEntretienLabelHote', 'Hôte', 'Host', 'Huésped', 'prestaEntretienLabelHote-P15', '2013-08-30 11:55:28', '2013-08-30 13:55:28'),
(343, 'prestaEntretienBtConfirmer', 'Confirmer l''entretien', 'Version en', 'Version es', 'prestaEntretienBtConfirmer-P15', '2013-08-30 11:55:28', '2013-08-30 13:55:28'),
(344, 'HpClientNoProfilMatch', 'Aucun profil ne correspond à votre recherche. Nous vous affichons par conséquents toutes les missions disponibles', 'No matching results found ', 'Ningún perfil corresponde a tu búsqueda', 'HpClientNoProfilMatch-P16', '2013-08-30 11:56:06', '2013-08-30 13:56:06'),
(345, 'HpClientNoProfil', 'Il n''y a actuellement aucun profil dans la base de donnée', 'There is currently no profile in the database', 'No hay actualmente ningún perfil en la base de datos', 'HpClientNoProfil-P16', '2013-08-30 11:56:06', '2013-08-30 13:56:06'),
(346, 'HpClientAllProfil', 'Voir tous les profils', 'Version An', 'Version Es', 'HpClientAllProfil-P16', '2013-08-30 11:56:06', '2013-08-30 13:56:06'),
(347, 'HpClientPropositionEnCours', 'Mes propositions en cours', 'My current proposals', 'Mis propuestas en curso', 'HpClientPropositionEnCours-P16', '2013-08-30 11:56:06', '2013-08-30 13:56:06'),
(348, 'HpClientNoReponse', 'Aucune réponse', 'No proposal', 'Ninguna respuesta', 'HpClientNoReponse-P16', '2013-08-30 11:56:06', '2013-08-30 13:56:06'),
(349, 'HpClientSuiviMission', 'Mon suivi', 'My follow-up', 'Mi seguimiento', 'HpClientSuiviMission-P16', '2013-08-30 11:56:06', '2013-08-30 13:56:06'),
(350, 'HpClientProfilSauvegarde', 'Mes profils sauvegardés', 'My saved profile', 'Mis perfil guardados', 'HpClientProfilSauvegarde-P16', '2013-08-30 11:56:06', '2013-08-30 13:56:06'),
(351, 'HpClientContact', '<p>Un probleme technique</p><p>Une question d''ordre administratif ou commercial?</p>', 'Version en', 'Version es', 'HpClientContact-P16', '2013-08-30 11:56:06', '2013-08-30 13:56:06'),
(352, 'HpClientDerniersProfils', 'Les derniers profils enregistrés', 'The most recently profils', 'Los últimos perfiles', 'HpClientDerniersProfils-P16', '2013-08-30 11:56:06', '2013-08-30 13:56:06'),
(353, 'titre', 'Suivi de mes propositions', 'Follow-up of my proposals', 'Seguimiento de mis propuestas', 'titre-P16', '2013-08-30 11:56:06', '2013-08-30 13:56:06'),
(354, 'intro', '<b>Suivez l''évolution des dernières propositions que vous avez faites pour des offres de projet :</b><br/>Le statut de votre proposition correspond au traitement de cette dernière de la part du client concerné. <br/>Il est "Non traité" par défaut. Dès lors qu''un donneur d''ordres étudie votre proposition, son statut change en fonction de l''action qu''il effectue : "Proposition en cours d''étude", "Proposition validée" ou "Proposition déclinée".<br/><br/>A l''aide de notre outil de recherche et de ses filtres, retrouvez en quelques clics toutes vos propositions !', '<b>Follow the evolution of your last proposals to projects\\'' offers : </b><br/>The status of your proposal is equivalent to how it is processed by the client. \r\n<br/>It is \\"Unread\\" by default. At the moment a Contractor looks at your proposal, its status changes according to the action done by the Contractor : \\"Currently processing proposal\\", \\"Selected proposal\\" or \\"Declined proposal\\". <br/><br/>Via our search tool and its filters, find in a quick way all your proposals !', '<b>Seguí la evolución de las últimas propuestas que hiciste para ofertas de proyecto :</b><br/>El estado de tu propuesta corresponde al estado por parte del cliente. Este estado está "No Tratado" por defecto. Cuando una empresa estudie tu propuesta, tu estado cambia en función de la acción que él efectúa : "Propuesta en curso de estudio", "Propuesta válida" o "Propuesta declinada".<br/><br/>¡ Con la ayuda de nuestra herramienta de búsqueda y de sus filtros, encontrá en pocos clics todas tus propuestas !', 'intro-P16', '2013-08-30 11:56:06', '2013-08-30 13:56:06'),
(355, 'mes_props', 'Mes propositions', 'My proposals', 'Mis propuestas', 'mes_props-P16', '2013-08-30 11:56:06', '2013-08-30 13:56:06'),
(356, 'select_action', 'Sélectionner une action générale', 'Choose a action', 'Seleccionar una acción general', 'select_action-P16', '2013-08-30 11:56:06', '2013-08-30 13:56:06'),
(357, 'contactTitre', 'Contactez-nous', 'Contact us', 'Contactanos', 'contactTitre-P16', '2013-08-30 11:56:06', '2013-08-30 13:56:06'),
(358, 'contactIntro', 'Un problème technique ? <br/>\r\nUne question d\\''ordre administratif ou commercial ?', 'A technical problem ? <br/>An administrative or business related question ?', '¿ Algún problema técnico ? <br/>\r\n¿ Alguna pregunta de orden administrativo o comercial ?', 'contactIntro-P16', '2013-08-30 11:56:06', '2013-08-30 13:56:06'),
(359, 'contact_bt', 'Contactez-nous', 'Contact us', 'Contactanos', 'contact_bt-P16', '2013-08-30 11:56:06', '2013-08-30 13:56:06'),
(360, 'menu_tableau_bord', 'Tableau de bord', 'Dashboard', 'Panel de Control', 'menu_tableau_bord-P17', '2013-08-30 11:56:29', '2013-08-30 13:56:29'),
(361, 'menu_mon_compte', 'Mon compte', 'my account', 'Mi cuenta', 'menu_mon_compte-P17', '2013-08-30 11:56:29', '2013-08-30 13:56:29'),
(362, 'menu_mes_entretiens', 'mes entretiens', 'my interviews', 'Mis entrevistas', 'menu_mes_entretiens-P17', '2013-08-30 11:56:29', '2013-08-30 13:56:29'),
(363, 'menu_gerer_projets', 'gérer mes projets', 'Manage my projects', 'gestionar mis proyectos', 'menu_gerer_projets-P17', '2013-08-30 11:56:29', '2013-08-30 13:56:29'),
(364, 'menu_cvteque', 'cvthèque', 'Cv library', 'CVteca', 'menu_cvteque-P17', '2013-08-30 11:56:29', '2013-08-30 13:56:29'),
(365, 'bt_edit_profil', 'modifier mon profil', 'edit my profile', 'modificar mi perfil', 'bt_edit_profil-P17', '2013-08-30 11:56:29', '2013-08-30 13:56:29'),
(366, 'menu_publier_projet', 'Publier un projet', 'Publish a project', 'Publicar un proyecto', 'menu_publier_projet-P17', '2013-08-30 11:56:29', '2013-08-30 13:56:29'),
(367, 'menu_favoris', 'Mes CVs favoris', 'My favourite CVs', 'Mis CVs favoritos', 'menu_favoris-P17', '2013-08-30 11:56:29', '2013-08-30 13:56:29'),
(368, 'menu_messages_modele', 'Mes réponses modèles', 'My model answers', 'Mis respuestas tipo', 'menu_messages_modele-P17', '2013-08-30 11:56:29', '2013-08-30 13:56:29'),
(369, 'menu_managers', 'Mes managers', 'My managers', 'Mis managers', 'menu_managers-P17', '2013-08-30 11:56:29', '2013-08-30 13:56:29'),
(370, 'HpClientVosMissions', 'Mes missions', 'My projects', 'Mis proyectos', 'HpClientVosMissions-P17', '2013-08-30 11:56:29', '2013-08-30 13:56:29'),
(371, 'footerAccueil', 'Accueil', 'Home', 'Inicio', 'footerAccueil-P18', '2013-08-30 11:57:50', '2013-08-30 13:57:50'),
(372, 'footerAPropos', 'A propos', 'About us', 'Sobre nosotros', 'footerAPropos-P18', '2013-08-30 11:57:50', '2013-08-30 13:57:50'),
(373, 'footerMentions', 'Informations légales', 'Legal information', 'Informaciones legales', 'footerMentions-P18', '2013-08-30 11:57:50', '2013-08-30 13:57:50'),
(374, 'footerSitemap', 'Sitemap', 'Sitemap', 'Mapa web', 'footerSitemap-P18', '2013-08-30 11:57:50', '2013-08-30 13:57:50'),
(375, 'footerContact', 'Contact', 'Contact', 'Contacto', 'footerContact-P18', '2013-08-30 11:57:50', '2013-08-30 13:57:50'),
(376, 'footerCopyright', 'Copyright 2012 - WeMe - marque déposée', '', '', 'footerCopyright-P18', '2013-08-30 11:57:50', '2013-08-30 13:57:50'),
(377, 'footerLiensUtiles', 'Liens Utiles', 'Useful links', 'Enlaces de interes', 'footerLiensUtiles-P18', '2013-08-30 11:57:50', '2013-08-30 13:57:50'),
(378, 'footerClientProjet', 'Créer votre projet', 'Version Anglaise', 'Version Espagnol', 'footerClientProjet-P18', '2013-08-30 11:57:50', '2013-08-30 13:57:50'),
(379, 'footerEntrepreneur', 'Entrepreneurs', 'Entre EN', 'Entre ES', 'footerEntrepreneur-P18', '2013-08-30 11:57:50', '2013-08-30 13:57:50'),
(380, 'footerPrestataire', 'Prestataire', 'Presta En ', 'Presta Es', 'footerPrestataire-P18', '2013-08-30 11:57:50', '2013-08-30 13:57:50'),
(381, 'label_titre_partenaire', 'Nos <br/>partenaires', 'Our <br/>Partners', 'Nuestros <br/>Aliados', 'label_titre_partenaire-P18', '2013-08-30 11:57:50', '2013-08-30 13:57:50'),
(382, 'description_partenaire', 'Texte de description pour leur dire que s''ils veulent devenir notre partenaire, qu''ils nous contactent', 'Texte de description pour leur dire que s''ils veulent devenir notre partenaire, qu''ils nous contactent', 'Texte de description pour leur dire que s''ils veulent devenir notre partenaire, qu''ils nous contactent', 'description_partenaire-P18', '2013-08-30 11:57:50', '2013-08-30 13:57:50'),
(383, 'footerClientCvtheque', 'Explorez notre CVthèque', 'Version Anglaise', 'Version Espagnol', 'footerClientCvtheque-P18', '2013-08-30 11:57:50', '2013-08-30 13:57:50'),
(384, 'footerClientSourcing', 'Nos solutions Sourcing', 'Version An', 'Version En', 'footerClientSourcing-P18', '2013-08-30 11:57:50', '2013-08-30 13:57:50'),
(385, 'footerPrestaCv', 'Déposer votre CV', 'Version An', 'Version Es', 'footerPrestaCv-P18', '2013-08-30 11:57:50', '2013-08-30 13:57:50'),
(386, 'footerPrestaIntermittent', 'Nos solutions intermittents', 'Version An', 'Version En', 'footerPrestaIntermittent-P18', '2013-08-30 11:57:50', '2013-08-30 13:57:50'),
(387, 'footerPrestaProjet', 'Consulter les projets', 'Version An', 'Version Es', 'footerPrestaProjet-P18', '2013-08-30 11:57:50', '2013-08-30 13:57:50'),
(388, 'ClientMonCompteInfosGeneral', 'Mes informations générales', 'General information', 'Mis informaciones generales', 'ClientMonCompteInfosGeneral-P19', '2013-08-30 11:58:36', '2013-08-30 13:58:36'),
(389, 'ClientMonCompteModifPhoto', 'Modifier ma photo', 'Edit my picture', 'Modificar mi foto', 'ClientMonCompteModifPhoto-P19', '2013-08-30 11:58:36', '2013-08-30 13:58:36'),
(390, 'clientImporterPhotoRules', 'Vous êtes sur le point de modifier votre avatar.\r\n                    <br>\r\n                    <br>\r\n                    Merci de fournir un fichier jpeg inferieur à 1 mo.\r\n                    <br>\r\n                    <br>\r\n                    Une fois l''importation terminée vous pourrez redimensionner votre photo et l''enregistrer', 'Version En', 'Version Es', 'clientImporterPhotoRules-P19', '2013-08-30 11:58:36', '2013-08-30 13:58:36'),
(391, 'clientMyPhoto', 'Ma photo', 'My picture', 'Mi foto', 'clientMyPhoto-P19', '2013-08-30 11:58:36', '2013-08-30 13:58:36'),
(392, 'clientDescriptiontitre', 'Ma Description', 'My description ', 'Mi Descripción', 'clientDescriptiontitre-P19', '2013-08-30 11:58:36', '2013-08-30 13:58:36'),
(393, 'clientEnregisterPresentation', 'Enregistrer ma présentation', 'version en ', 'Version es', 'clientEnregisterPresentation-P19', '2013-08-30 11:58:36', '2013-08-30 13:58:36'),
(394, 'ident', 'Mes identifiants de connexion', 'My NeoFreelo ID', 'Mis identificadores de conexión', 'ident-P19', '2013-08-30 11:58:36', '2013-08-30 13:58:36'),
(395, 'photo_titre', 'Ma photo de profil', 'My profile picture', 'Mi foto de perfil', 'photo_titre-P19', '2013-08-30 11:58:36', '2013-08-30 13:58:36'),
(396, 'imp_photo', 'Importer ma photo', 'Import my picture', 'Importar mi foto', 'imp_photo-P19', '2013-08-30 11:58:36', '2013-08-30 13:58:36'),
(397, 'recadrer_photo', 'Recadrer ma photo', 'Crop my picture', 'Reencuadrar mi foto', 'recadrer_photo-P19', '2013-08-30 11:58:36', '2013-08-30 13:58:36'),
(398, 'juridique', 'Juridique', 'Legal', 'Jurídico', 'juridique-P19', '2013-08-30 11:58:36', '2013-08-30 13:58:36'),
(399, 'config_notif', 'Configuration de vos alertes email', 'Setting up your email alerts', 'Configurar mis notificaciones por email', 'config_notif-P19', '2013-08-30 11:58:36', '2013-08-30 13:58:36'),
(400, 'ignorer', 'Annuler l''importation de cette photo', 'Cancel the import of this picture', 'Cancelar la importación de esta foto', 'ignorer-P19', '2013-08-30 11:58:36', '2013-08-30 13:58:36'),
(401, 'agenceImporterPlaquetteTitre', 'Importez votre plaquette', 'Version An', 'Version En', 'agenceImporterPlaquetteTitre-P19', '2013-08-30 11:58:36', '2013-08-30 13:58:36'),
(402, 'agenceImporterPlaquette', '<span class="higher">Importez</span><br/>\r\nvotre plaquette au format PDF', 'Version An', 'Version Es', 'agenceImporterPlaquette-P19', '2013-08-30 11:58:36', '2013-08-30 13:58:36'),
(403, 'agenceDescription', 'Description de ma société', 'Description of my company\\', 'Descripción de mi empresa', 'agenceDescription-P19', '2013-08-30 11:58:36', '2013-08-30 13:58:36'),
(404, 'clientProposerMissionsFavoris', 'Proposer les missions selectionnées aux favoris', 'Propose to favourites', 'Proponer a los favoritos', 'clientProposerMissionsFavoris-P21', '2013-08-30 12:02:49', '2013-08-30 14:02:49'),
(405, 'clientChangeMissionStatut', 'Changer le statut', 'Version en ', 'Version es', 'clientChangeMissionStatut-P21', '2013-08-30 12:02:49', '2013-08-30 14:02:49');
INSERT INTO `tra_traductions` (`id_traduction`, `libelle`, `tra_fr`, `tra_en`, `tra_es`, `trad_cle_unique`, `cree_le`, `modifie_le`) VALUES
(406, 'clientEditMissionsTitre', 'Editer une mission', 'version en', 'version es', 'clientEditMissionsTitre-P22', '2013-08-30 12:04:38', '2013-08-30 14:04:38'),
(407, 'clientPublierMissionTitre', 'Publication d''un projet', 'Publication of a project', 'Publicación de un proyecto', 'clientPublierMissionTitre-P23', '2013-08-30 12:04:57', '2013-08-30 14:04:57'),
(408, 'clientAjouterSecteur', 'saisissez le domaine d''activité dans lequel s''inscrit votre projet', 'Enter the industry which your project belongs to', 'introduce el sector de actividad en el que esta inscrito tu proyecto', 'clientAjouterSecteur-P23', '2013-08-30 12:04:57', '2013-08-30 14:04:57'),
(409, 'clientAjouterProfil', 'saisissez le type de profil que vous recherchez', 'Enter the type of profile you are searching for', 'introduce el tipo de perfil que buscas', 'clientAjouterProfil-P23', '2013-08-30 12:04:57', '2013-08-30 14:04:57'),
(410, 'clientAjouterCompetence', 'saisissez les compétences souhaitées', 'Enter the requested skills', 'introduce las competencias profesionales requeridas', 'clientAjouterCompetence-P23', '2013-08-30 12:04:57', '2013-08-30 14:04:57'),
(411, 'clientAjouterEtude', 'saisissez le niveau d''études souhaité', 'Enter the requested level of study', 'introduce el nivel de estudios requerido', 'clientAjouterEtude-P23', '2013-08-30 12:04:57', '2013-08-30 14:04:57'),
(412, 'clientAjouterLangue', 'saisissez le(s) langue(s) maîtrisée(s) que vous recherchez', 'Enter the requested languages ', 'introduce el/los idioma(s) dominado(s) requirido(s)', 'clientAjouterLangue-P23', '2013-08-30 12:04:57', '2013-08-30 14:04:57'),
(413, 'clientNewManagerTitreUn', 'Associez un manager à votre compte', 'Version En', 'Version Es', 'clientNewManagerTitreUn-P24', '2013-08-30 12:05:15', '2013-08-30 14:05:15'),
(414, 'clientNewManagerTitreDeux', 'Liste de vos managers', 'Version En', 'Version Es', 'clientNewManagerTitreDeux-P24', '2013-08-30 12:05:15', '2013-08-30 14:05:15'),
(415, 'clientNewManagerP1', 'Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l''imprimerie depuis les années 1500, quand un peintre anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n''a pas fait que survivre cinq siècles, mais s''est aussi adapté à la bureautique informatique, sans que son contenu n''en soit modifié. Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker. ', 'Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l''imprimerie depuis les années 1500, quand un peintre anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n''a pas fait que survivre cinq siècles, mais s''est aussi adapté à la bureautique informatique, sans que son contenu n''en soit modifié. Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker.', 'Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l''imprimerie depuis les années 1500, quand un peintre anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n''a pas fait que survivre cinq siècles, mais s''est aussi adapté à la bureautique informatique, sans que son contenu n''en soit modifié. Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker. ', 'clientNewManagerP1-P24', '2013-08-30 12:05:15', '2013-08-30 14:05:15'),
(416, 'clientEntretienTitre1', 'Créer un entretien vidéo', 'Start a video interview', 'Crear un entrevista video', 'clientEntretienTitre1-P26', '2013-08-30 12:05:56', '2013-08-30 14:05:56'),
(417, 'clientEntretienTitre1Bis', 'A propos de l''entretien sélectionnée', 'About the selected interview', 'Sobre la entrevista seleccionada', 'clientEntretienTitre1Bis-P26', '2013-08-30 12:05:56', '2013-08-30 14:05:56'),
(418, 'clientEntretienCode', 'code d''accès', 'access code', 'codigo de acceso', 'clientEntretienCode-P26', '2013-08-30 12:05:56', '2013-08-30 14:05:56'),
(419, 'clientEntretienTitre2', 'Les entretiens vidéo WeMe', 'Version En', 'Version Es', 'clientEntretienTitre2-P26', '2013-08-30 12:05:56', '2013-08-30 14:05:56'),
(420, 'clientEntretienTitre3', 'Liste de vos entretiens vidéos', 'Version En', 'Version Es', 'clientEntretienTitre3-P26', '2013-08-30 12:05:56', '2013-08-30 14:05:56'),
(421, 'clientEntretienLabelCommenter', 'Commentaire - aide mémoire (beta)', 'Version en', 'Version Es', 'clientEntretienLabelCommenter-P26', '2013-08-30 12:05:56', '2013-08-30 14:05:56'),
(422, 'clientEntretienBtCommenter', 'Commenter', 'Version en', 'Version es', 'clientEntretienBtCommenter-P26', '2013-08-30 12:05:56', '2013-08-30 14:05:56'),
(423, 'clientEntretienIntro', 'Créez en un tour de main un entretien vidéo, individuel ou collectif, en invitant un ou plusieurs prestataires de vos favoris. Une fois l’entretien planifié, suivez depuis cette page l’évolution de l’invitation auprès du (des) participant(s).', 'Start easily an individual or collective video call by inviting one or more freelancers from your favourites. Once the interview has been planned, follow the evolution of your invitation to the freelancer(s) from this page.', 'Podés crear una entrevista video, colectiva o individual, invitando a uno o varios freelances de tus favoritos. Una vez que está planificada la entrevista, podés seguir la evolución de la invitación del o los participante(s) desde esta página.', 'clientEntretienIntro-P26', '2013-08-30 12:05:56', '2013-08-30 14:05:56'),
(424, 'clientEntretienLabelInvite', 'Invité', 'freelancer', 'Invitado', 'clientEntretienLabelInvite-P26', '2013-08-30 12:05:56', '2013-08-30 14:05:56'),
(425, 'clientMessageTitre1', 'Créez des messages types', 'Create model messages', 'Crear mensajes tipo', 'clientMessageTitre1-P27', '2013-08-30 12:06:15', '2013-08-30 14:06:15'),
(426, 'clientMessageLabelTitre', 'Titre de mon message', 'My message title', 'Título de mi mensaje', 'clientMessageLabelTitre-P27', '2013-08-30 12:06:15', '2013-08-30 14:06:15'),
(427, 'clientMessageLabelMessage', 'Message', 'Message', 'Mensaje', 'clientMessageLabelMessage-P27', '2013-08-30 12:06:15', '2013-08-30 14:06:15'),
(428, 'clientMessageTitre2', 'Liste de vos mail type', 'Your model message list', 'Lista de tu mensaje tipo', 'clientMessageTitre2-P27', '2013-08-30 12:06:15', '2013-08-30 14:06:15'),
(429, 'clientMessageIntro', 'Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l''imprimerie depuis les années 1500, quand un peintre anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n''a pas fait que survivre cinq siècles, mais s''est aussi adapté à la bureautique informatique, sans que son contenu n''en soit modifié. Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker.', 'Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l''imprimerie depuis les années 1500, quand un peintre anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n''a pas fait que survivre cinq siècles, mais s''est aussi adapté à la bureautique informatique, sans que son contenu n''en soit modifié. Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker.', 'Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l''imprimerie depuis les années 1500, quand un peintre anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n''a pas fait que survivre cinq siècles, mais s''est aussi adapté à la bureautique informatique, sans que son contenu n''en soit modifié. Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker.', 'clientMessageIntro-P27', '2013-08-30 12:06:15', '2013-08-30 14:06:15'),
(430, 'no_profil_match', 'Aucun profil ne correspond à votre recherche', 'No matching results found', 'Ningún perfil corresponde a tu búsqueda', 'no_profil_match-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(431, 'titre', 'Mon suivi', 'My follow-up', 'Mi seguimiento', 'titre-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(432, 'projets', 'Mes projets', 'My projects', 'Mis proyectos', 'projets-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(433, 'prop_recu', 'Propositions reçues', 'Received proposals', 'Propuestas recibidas', 'prop_recu-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(434, 'fav', 'Mes favoris', 'My favourites', 'Mis favoritos', 'fav-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(435, 'prop', 'Mes propositions', 'My proposals', 'Mis propuestas', 'prop-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(436, 'saved', 'Mes projets sauvegardés', 'My saved projects', 'Mis proyectos guardados', 'saved-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(437, 'precedent', '< précedent', '< previous', '< anterior', 'precedent-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(438, 'suivant', 'suivant >', 'next >', 'siguiente >', 'suivant-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(439, 'non_rens', ' ', '', '', 'non_rens-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(440, 'non_com', ' ', ' ', ' ', 'non_com-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(441, 'abo', 'abonnez-vous', 'Subscribe', 'Suscribirte', 'abo-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(442, 'masque', 'XXXXXXXXXX', 'XXXXXXXXXX', 'XXXXXXXXXX', 'masque-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(443, 'ans', ' ans', ' years', ' años', 'ans-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(444, 'lien_masque', 'www.xxxxxxx.fr', 'www.xxxxxxx.com', 'www.xxxxxxx.fr', 'lien_masque-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(445, 'immediate', 'Immédiate', 'Immediate', 'Inmediata', 'immediate-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(446, 'non', 'Non', 'No', 'No', 'non-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(447, 'oui', 'Oui', 'Yes', 'Sí', 'oui-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(448, 'inconnu', 'Inconnu', 'Unknown', 'Desconocido', 'inconnu-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(449, 'missionRepRecu', 'Réponse(s) ', 'answer(s) ', 'Respuesta(s) ', 'missionRepRecu-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(450, 'label_reponse', 'Réponse(s)', 'answer(s)', 'Respuesta(s)', 'label_reponse-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(451, 'unknown2', 'Réponses retenues : ', 'Selected answers : ', 'Respuestas retenidas : ', 'unknown2-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(452, 'unknown3', 'Proposer aux favoris', 'Propose to favourites', 'Proponer a los favoritos', 'unknown3-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(453, 'projetApercuModifier', 'Modifier', 'Edit', 'Modificar', 'projetApercuModifier-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(454, 'projetApercuDupliquer', 'Dupliquer', 'Duplicate', 'Duplicar', 'projetApercuDupliquer-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(455, 'projetApercuRepublier', 'Republier', 'Republish', 'Re-publicar', 'projetApercuRepublier-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(456, 'projetApercuSuspendre', 'Suspendre', 'Suspend', 'Suspender', 'projetApercuSuspendre-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(457, 'projetApercuReprendre', 'Reprendre', 'Version en', 'Version es', 'projetApercuReprendre-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(458, 'projetApercuCloturer', 'Clôturer', 'Close', 'Cerrar', 'projetApercuCloturer-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(459, 'projetApercuReponseOffre', 'Répondre à cette offre', 'Reply to this offer', 'Responder a esta oferta', 'projetApercuReponseOffre-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(460, 'projetApercuVoirReponses', 'Voir devis', 'See quote', 'Ver presupuesto', 'projetApercuVoirReponses-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(461, 'unknown6', 'Retirer l''offre de mes projets sauvegardés', 'Delete the offer from my saved projects', 'Quitar la oferta de mis proyectos guardados', 'unknown6-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(462, 'saveProjet', 'Sauvegarder le projet', 'Save the project', 'Guardar el proyecto', 'saveProjet-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(463, 'btSave', 'Sauvegarder', 'Save', 'Guardar', 'btSave-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(464, 'unknown8', 'Réponses non traitées', 'Non covered responses', 'Respuestas no tratadas', 'unknown8-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(465, 'devis', 'Devis', 'Quote', 'Presupuesto', 'devis-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(466, 'dispo', 'Disponibilité : ', 'Availability : ', 'Disponibilidad : ', 'dispo-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(467, 'permis', 'Permis : ', 'Driving License : ', 'Licencia de conducir : ', 'permis-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(468, 'unknown9', 'Ce profil a été supprimé de notre Cvthèque', 'This profile has been deleted from our CV library', 'Este perfil ha sido suprimido de nuestra CVteca', 'unknown9-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(469, 'missionDetail', 'Détail du projet', 'Project details', 'Detalle del proyecto', 'missionDetail-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(470, 'propositionDetailBt', 'Détails', 'Details', 'Detalles', 'propositionDetailBt-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(471, 'bt_detail', 'Détails', 'Details', 'Detalles', 'bt_detail-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(472, 'rep_recues', 'Réponses reçues ', 'Received responses ', 'Respuestas recibidas', 'rep_recues-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(473, 'activer', 'Activer', 'Activate', 'Activar', 'activer-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(474, 'deplacer', 'Déplacer vers', 'Move to ', 'Desplazar a', 'deplacer-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(475, 'desc', 'Description du projet', 'Project''s description ', 'Descripción del proyecto', 'desc-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(476, 'pf', 'Profil(s) proposé(s) ', 'Suggested profile(s) ', 'Perfil(es) propuesto(s) ', 'pf-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(477, 'unknown10', 'Attention, ce profil n''existe plus', 'Sorry, this profile no longer exists', 'Cuidado, este perfil ya no existe', 'unknown10-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(478, 'client', 'Client ', 'Client ', 'Cliente', 'client-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(479, 'propose_le', 'Période du projet ', 'Project timeframe ', 'Periodo del proyecto', 'propose_le-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(480, 'date_reponse', 'Date limite de réponse  ', 'Response Deadline ', 'Fecha límite de respuesta', 'date_reponse-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(481, 'detail_devis', 'Détail du Devis', 'Quote''s details', 'Detalle del presupuesto', 'detail_devis-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(482, 'bt_archiver', 'Archiver', 'Archive', 'Archivar', 'bt_archiver-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(483, 'bt_autre_projet_client', 'Autres projets de ce client', 'Other projects from this client', 'Otros proyectos del cliente', 'bt_autre_projet_client-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(484, 'bt_nouvelle_prop', 'Faire une nouvelle proposition', 'Make a new proposal', 'Hacer una nueva propuesta', 'bt_nouvelle_prop-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(485, 'rep_archivee', 'Réponse archivée', 'Archived response', 'Respuesta archivada', 'rep_archivee-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(486, 'label_nom', 'Nom ', 'Name', 'Apellido(s) ', 'label_nom-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(487, 'label_prenom', 'Prénom', 'First name', 'Nombre', 'label_prenom-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(488, 'label_civilite', 'Civilité', 'Civil status', 'Género', 'label_civilite-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(489, 'label_nom_societe', 'Nom de votre société ', 'Company''s name', 'Nombre de tu empresa ', 'label_nom_societe-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(490, 'label_telephone', 'Téléphone ', 'Phone number ', 'Teléfono ', 'label_telephone-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(491, 'label_tel_short', 'Tel : ', 'Phone :', 'Tel : ', 'label_tel_short-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(492, 'label_fixe', 'Téléphone fixe ', 'Phone number', 'Teléfono fijo ', 'label_fixe-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(493, 'label_portable', 'Téléphone Portable ', 'Mobile Phone', 'Celular ', 'label_portable-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(494, 'label_email', 'Email', 'Email', 'Email', 'label_email-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(495, 'label_mdp', 'Mot de passe', 'Password', 'Contraseña', 'label_mdp-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(496, 'label_coordonnees', 'Coordonnées', 'Version en', 'Version es', 'label_coordonnees-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(497, 'label_mdp_short', 'MDP', 'Password', 'Contraseña', 'label_mdp_short-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(498, 'label_mdp_conf', 'Confirmer le mot de passe', 'Confirm the password', 'Confirmar la contraseña', 'label_mdp_conf-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(499, 'label_cp', 'Code Postal', 'Postcode', 'Código postal ', 'label_cp-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(500, 'label_ville', 'Ville', 'Town', 'Ciudad', 'label_ville-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(501, 'label_site', 'Site internet', 'Website', 'Página Web', 'label_site-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(502, 'label_desc_activite', 'Description de l''activité', 'Description of the activity', 'Descripción de la actividad', 'label_desc_activite-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(503, 'label_pays', 'Pays', 'Country', 'País ', 'label_pays-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(504, 'label_siret', 'Numéro SIRET', 'Business Identification number', 'Identificación Fiscal', 'label_siret-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(505, 'label_secteur_activite', 'Secteur d''activité', 'Industry', 'Sector de actividad', 'label_secteur_activite-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(506, 'label_effectif', 'Effectif', 'Workforce', 'Número de empleados', 'label_effectif-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(507, 'label_fixe_short', 'Fixe', 'Phone', 'Fijo', 'label_fixe_short-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(508, 'label_portable_short', 'Portable', 'Mobile', 'Celular', 'label_portable_short-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(509, 'label_connexion', 'Email de connexion', 'Email address', 'Email de conexión', 'label_connexion-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(510, 'label_current_mdp', 'Mot de passe actuel', 'Current password', 'Contraseña actual', 'label_current_mdp-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(511, 'label_new_mdp', 'Nouveau mot de passe', 'New password', 'Nueva contraseña', 'label_new_mdp-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(512, 'label_confirm_mdp', 'Confirmer le nouveau mot de passe', 'Confirm the new password', 'Confirmar la nueva contraseña', 'label_confirm_mdp-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(513, 'label_titre_projet', 'Titre du projet', 'Project title', 'Título del proyecto', 'label_titre_projet-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(514, 'label_profil_search', 'Profil recherché', 'Sought profile', 'Perfil buscado', 'label_profil_search-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(515, 'label_desc', 'Description ', 'Description', 'Descripción ', 'label_desc-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(516, 'label_lieu', 'Lieu ', 'Location', 'Lugar', 'label_lieu-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(517, 'label_budget', 'Budget', 'Budget', 'Presupuesto', 'label_budget-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(518, 'label_tarif', 'Tarif ', 'Tarif', 'Tarifa', 'label_tarif-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(519, 'label_debut_projet', 'Date de début de projet', 'Starting date ', 'Fecha de inicio del proyecto', 'label_debut_projet-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(520, 'label_duree', 'Durée', 'Duration', 'Duración ', 'label_duree-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(521, 'limite_reponse', 'Date limite de réponse', 'Response Deadline', 'Fecha límite de respuesta', 'limite_reponse-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(522, 'label_niveau_etude', 'Niveau d''études', 'Level of studies', 'Nivel de estudios', 'label_niveau_etude-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(523, 'label_comp', 'Compétences', 'Skills', 'Aptitudes profesionales', 'label_comp-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(524, 'add_comp', 'Ajouter une compétences', 'Add a skill', 'Añadir una aptitude profesionales', 'add_comp-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(525, 'label_exp', 'Expériences', 'Experiences', 'Experiencias profesionales', 'label_exp-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(526, 'label_langue', 'Langue(s)', 'Language(s)', 'Idioma(s)', 'label_langue-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(527, 'add_langue', 'Ajouter une langue', 'Add a language', 'Añadir una idioma', 'add_langue-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(528, 'label_accept_mail', 'J''accepte de recevoir les propositions des prestataires directement par email ', 'I accept to receive freelancers'' proposals directly by email', 'Acepto recibir las propuestas de los freelances directamente por email', 'label_accept_mail-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(529, 'label_mots_cles', 'Mots clés', 'Keywords', 'Palabras claves', 'label_mots_cles-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(530, 'label_poste_search', 'Poste(s) recherché(s)', 'Sought position(s)', 'Puesto(s) buscado(s)', 'label_poste_search-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(531, 'label_mob', 'Mobilité', 'Mobility', 'Movilidad', 'label_mob-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(532, 'label_dispo', 'Disponibilité ', 'Availability', 'Disponibilidad', 'label_dispo-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(533, 'label_permis', 'Permis ', 'Driving license ', 'Licencia de conducir ', 'label_permis-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(534, 'label_titre_session', 'Titre de la session', 'Session title', 'Título de la sesión ', 'label_titre_session-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(535, 'label_session', 'Session', 'Session', 'Sesión ', 'label_session-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(536, 'label_projet', 'Projet', 'Project', 'Proyecto', 'label_projet-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(537, 'label_date', 'Date', 'Date', 'Fecha', 'label_date-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(538, 'label_heure', 'Heure de début', 'Begin time', 'Hora de inicio', 'label_heure-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(539, 'label_date_heure', 'Date et Heure', 'Date and time', 'Fecha y Hora', 'label_date_heure-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(540, 'label_profils', 'Profil(s)', 'Profile(s)', 'Perfil(es)', 'label_profils-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(541, 'label_age', 'Age ', 'Age', 'Edad', 'label_age-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(542, 'label_naissance', 'Date de naissance', 'Birth date', 'Fecha de nacimiento', 'label_naissance-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(543, 'label_titre_cv', 'Titre de mon Cv', 'CV Title', 'Titulo de mi CV', 'label_titre_cv-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(544, 'label_date_dispo', 'Disponibilité ', 'Availability', 'Disponibilidad', 'label_date_dispo-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(545, 'label_debut', 'Date de début ', 'Project''s date ', 'Fechas del proyecto ', 'label_debut-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(546, 'label_fin', 'Date de fin', 'End date', 'Fecha de fin', 'label_fin-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(547, 'label_poste_occupe', 'Poste occupé', 'Position held', 'Puesto ocupado', 'label_poste_occupe-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(548, 'desc_poste', 'Description du poste', 'Position''s description ', 'Descripción del puesto', 'desc_poste-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(549, 'label_societe', 'Société', 'Company', 'Empresa', 'label_societe-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(550, 'label_diplome', 'Diplôme obtenu', 'Qualifications or degrees', 'Diploma obtenido', 'label_diplome-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(551, 'label_date_obtention', 'Date d''obtention', 'Graduation date', 'Fecha de obtención ', 'label_date_obtention-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(552, 'label_ecole', 'Ecole', 'School', 'Universidad', 'label_ecole-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(553, 'label_entrer_lien', 'Entrer un lien URL ', 'Enter a link', 'Introduce un link URL', 'label_entrer_lien-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(554, 'label_imp_rea', 'Ou importer un fichier', 'Or upload a file', 'O importar un documento', 'label_imp_rea-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(555, 'label_reference', 'Référence', 'Reference', 'Referencia', 'label_reference-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(556, 'label_publication', 'Publication', 'Publication', 'Publicación ', 'label_publication-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(557, 'label_proposition', 'Proposition(s)', 'Proposal(s)', 'Propuesta(s)', 'label_proposition-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(558, 'label_desc_prop', 'Description de ma proposition', 'Description of my proposal', 'Descripción de mi propuesta', 'label_desc_prop-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(559, 'label_add_pj', 'Ajouter une pièce jointe', 'Add an attached file', 'Añadir un documento adjunto', 'label_add_pj-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(560, 'label_select_pj', 'Sélectionner une réalisation', 'Choose a creation', 'Seleccionar una realización ', 'label_select_pj-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(561, 'label_remarque', 'Remarque complémentaire', 'Additional comments', 'Observación complementaria ', 'label_remarque-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(562, 'label_send_prop_mail', 'Envoyer ma proposition par email', 'Send my proposal by email', 'Enviar mi propuesta por email', 'label_send_prop_mail-P', '2013-08-30 12:14:04', '2013-08-30 14:14:04'),
(563, 'label_rep_mail', 'Je souhaite recevoir la réponse du client par email', 'I wish to receive the client''s response by email', 'Quiero recibir la respuesta del cliente por email', 'label_rep_mail-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(564, 'label_select_all', 'Tout sélectionner', 'Select all', 'Seleccionar todo', 'label_select_all-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(565, 'label_obligatoire', ' Champs obligatoires ', ' Mandatory fields', ' Campos obligatorios', 'label_obligatoire-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(566, 'label_statut', 'Statut', 'Status', 'Estado', 'label_statut-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(567, 'label_client', 'Client', 'Client', 'Cliente', 'label_client-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(568, 'label_dates_projet', 'Date de projet', 'Project date', 'Fecha del proyecto', 'label_dates_projet-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(569, 'label_agence_pres', 'Présentation de l''agence', 'Presentation of the agency', 'Presentación de la agencia', 'label_agence_pres-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(570, 'label_agence_plaq', 'Plaquette de l''agence', 'The agency''s brochure', 'Folleto presentación de la agencia', 'label_agence_plaq-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(571, 'label_adresse', 'Adresse', 'Address', 'Dirección ', 'label_adresse-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(572, 'label_rue', 'rue', 'street', 'Version Es ', 'label_rue-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(573, 'label_etat_civil', 'Etat civil', 'Version En', 'Version Es ', 'label_etat_civil-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(574, 'label_informe_par_mail', 'Je souhaite recevoir une alerte email lorsqu''un projet publié me correspond', 'I wish to receive an alert email when a project matches my requirements', 'Quiero recibir una alerta por email cuando un proyecto publicado me interesa', 'label_informe_par_mail-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(575, 'bt_enregistrer', 'Enregistrer', 'Save', 'Guardar', 'bt_enregistrer-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(576, 'bt_ajouter', 'Ajouter ', 'Add', 'Añadir', 'bt_ajouter-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(577, 'bt_modifier', 'Modifier', 'Edit', 'Modificar', 'bt_modifier-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(578, 'bt_supprimer', 'Supprimer ', 'Delete', 'Eliminar', 'bt_supprimer-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(579, 'bt_masquer', 'Masquer ', 'Hide', 'Ocultar', 'bt_masquer-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(580, 'bt_ignorer', 'Ignorer', 'Ignore', 'Ignorar', 'bt_ignorer-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(581, 'bt_ok', 'Ok', 'OK', 'Ok', 'bt_ok-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(582, 'bt_completer', 'Compléter ', 'Fill in', 'Completar', 'bt_completer-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(583, 'bt_dl', 'Télécharger ', 'Download', 'Descargar', 'bt_dl-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(584, 'bt_dl_tarif', 'Télécharger la grille tarifaire', 'Download the price list', 'Descargar las tarifas', 'bt_dl_tarif-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(585, 'bt_initialiser', 'Réinitialiser la recherche ', 'Restart the research', 'Reinicializar la búsqueda', 'bt_initialiser-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(586, 'bt_actualiser', 'Actualiser la recherche ', 'Update the research', 'Actualizar la búsqueda ', 'bt_actualiser-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(587, 'bt_contact', 'Contactez-nous ', 'Contact us', 'Contáctanos', 'bt_contact-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(588, 'label_contact', 'Contact ', 'Contact', 'Contácto', 'label_contact-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(589, 'modif_mdp', 'Modifier le mot de passe', 'Edit my password', 'Modificar la contraseña', 'modif_mdp-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(590, 'modif_logo', 'Modifier mon image', 'Edit my picture', 'Modificar mi imagen ', 'modif_logo-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(591, 'bt_fermer', 'Fermer ', 'Close', 'Cerrar', 'bt_fermer-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(592, 'bt_retour', 'Retour', 'Back', 'Atrás', 'bt_retour-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(593, 'bt_envoyer', 'Envoyer ', 'Send', 'Enviar', 'bt_envoyer-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(594, 'bt_plus', 'En savoir plus', 'More information', 'Saber más', 'bt_plus-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(595, 'bt_dl_pdf', 'Télécharger en pdf ', 'Download as pdf', 'Descargar en pdf', 'bt_dl_pdf-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(596, 'no_pres', 'Aucune présentation', 'No presentation', 'Ninguna presentación ', 'no_pres-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(597, 'non_def', 'Non défini', 'Non defined', 'No definido', 'non_def-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(598, 'nc', 'N/C', 'N/A', 'N/C', 'nc-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(599, 'erreur_format_cv', 'Le format de votre Cv n''est pas pris en charge par notre plateforme. Merci de l''importer en format pdf.', 'The format of your CV cannot be imported on our website. Please import it in pdf format', 'Nuestra plataforma no puede leer el formato de tu CV. Gracias por importarlo en formato pdf.', 'erreur_format_cv-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(600, 'erreur_get_cv', 'Veuillez nous excuser, un problème a été rencontré lors de la récupération du contenu de votre Cv', 'Sorry, a problem was encountered during the recovery of the content of your CV', 'Lo sentimos, hubo un problema durante la recuperación del contenido de tu CV', 'erreur_get_cv-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(601, 'prolonge_abo', 'Vous allez recevoir un email vous confirmant le renouvellement de votre abonnement', 'You will receive a confirmation email regarding the renewal of your subscription', 'Vas a recibir un email confirmando la renovación de tu suscripción', 'prolonge_abo-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(602, 'erreure', 'Suite à une erreur, merci de nous contacter', 'Following an error, please contact us', 'Debido a un error, gracias por contactarnos', 'erreure-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(603, 'erreure_send_mail', 'Echec lors de l''envoi de l''email, merci de recommencer', 'Failure during the sending of the email, please restart', 'Fallo durante el envío del email, gracias por volver a empezar', 'erreure_send_mail-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(604, 'profil_complete_nom', 'Ajoutez votre nom (+5%)', 'Add your name (+5%)', 'Añadir tu apellido (+5%)', 'profil_complete_nom-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(605, 'profil_complete_prenom', 'Ajoutez votre prénom (+5%)', 'Add your first name (+5%)', 'Añadir tu nombre (+5%)', 'profil_complete_prenom-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(606, 'label_rea', 'Fichier', 'File', 'Archivo', 'label_rea-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(607, 'label_maj', 'Mis à jour ', 'Updated', 'Actualizado ', 'label_maj-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(608, 'label_oui', 'Oui', 'Yes', 'Sí', 'label_oui-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(609, 'label_non', 'Non', 'No', 'No', 'label_non-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(610, 'label_dispo_immediate', 'Pour une disponibilité immédiate, laisser ce champs vide', 'When directly available, leave this field empty', 'Para una disponibilidad inmediata, dejar este campo vacío', 'label_dispo_immediate-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(611, 'select_poste', 'Sélectionnez un poste ', 'Choose a position', 'Seleccionar un puesto', 'select_poste-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(612, 'select_comp', 'Sélectionnez une compétence', 'Choose a skill', 'Seleccionar una aptitud profesional', 'select_comp-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(613, 'select_exp', 'Sélectionnez une expérience ', 'Choose an experience', 'Seleccionar una experiencia', 'select_exp-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(614, 'select_lieu', 'Sélectionnez un lieu ', 'Choose a location', 'Seleccionar un lugar', 'select_lieu-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(615, 'select_parution', 'Affichez les offres parues', 'Display published offers', 'Mostrar las ofertas publicadas', 'select_parution-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(616, 'label_du', 'Du ', 'From ', 'Del ', 'label_du-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(617, 'label_au', 'au ', 'to ', 'al ', 'label_au-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(618, 'label_profil', 'Profil(s) ', 'Profile(s)', 'Perfil(es)', 'label_profil-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(619, 'label_limite_rep', 'Date limite de réponse ', 'Response deadline', 'Fecha límite de respuesta', 'label_limite_rep-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(620, 'label_jour', 'jour(s)', 'day(s)', 'día(s) ', 'label_jour-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(621, 'label_mois', 'mois ', 'month', 'mes(es)', 'label_mois-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(622, 'label_annee', 'an(s) ', 'year(s)', 'año(s)', 'label_annee-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(623, 'add_element', 'Ajouter un élément ', 'Add an element', 'Añadir un elemento', 'add_element-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(624, 'bt_reponse_offre', 'Répondre à l''offre', 'Reply to the offer', 'Contestar a la oferta', 'bt_reponse_offre-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(625, 'bt_supp_profil', 'Supprimer de la liste des profils ', 'Delete from the profiles'' list', 'Eliminar de la lista de los perfiles', 'bt_supp_profil-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(626, 'bt_online', 'Voir en ligne', 'Watch online', 'Ver en línea', 'bt_online-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(627, 'bt_capture', 'capturer', 'Capture', 'sacar foto', 'bt_capture-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(628, 'bt_choose_photo', 'Enregistrer', 'Record', 'Guardar', 'bt_choose_photo-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(629, 'bt_aide', 'Aide', 'Help', 'Ayuda', 'bt_aide-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(630, 'voir_devis', 'Voir devis', 'See quote', 'Ver presupuesto', 'voir_devis-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(631, 'voir_cv', 'Voir CV', 'See CV', 'Ver CV', 'voir_cv-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(632, 'bt_visu', 'Visualiser', 'Visualize', 'Visualizar ', 'bt_visu-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(633, 'bt_dl_pj', 'Télécharger la pièce jointe', 'Download an attached file', 'Descargar el documento adjunto', 'bt_dl_pj-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(634, 'label_objet', 'Objet ', 'Object ', 'Objeto ', 'label_objet-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(635, 'label_prop', 'Proposition ', 'Proposal ', 'Propuesta ', 'label_prop-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(636, 'label_a', 'à  ', 'to ', 'a ', 'label_a-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(637, 'label_cv_de', 'Cv de ', 'CV from ', 'CV de ', 'label_cv_de-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(638, 'label_fonction', 'Fonction ', 'Position ', 'Función ', 'label_fonction-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(639, 'label_ou', 'Ou ', 'Or', 'O ', 'label_ou-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(640, 'label_com_princ', 'Compétence(s) principale(s) ', 'Main skills', 'Aptitud(es) principal(es) ', 'label_com_princ-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(641, 'label_formation', 'Formation(s)', 'Education', 'Formación(es)', 'label_formation-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(642, 'label_add_form', 'Ajouter une formation', 'Add a school', 'Añadir una formación', 'label_add_form-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(643, 'select_action', 'Sélectionnez une action', 'Choose an action', 'Seleccionar una acción ', 'select_action-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(644, 'select_send_msg', 'Envoyer un message', 'Send a message', 'Enviar un mensaje', 'select_send_msg-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(645, 'titre_devis', 'Devis détaillé de ', 'Detailed quote of ', 'Presupuesto detallado de', 'titre_devis-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(646, 'rea', 'Les réalisations', 'Creations ', 'Las realizaciones', 'rea-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(647, 'no_rea', 'Aucune réalisation d''enregistrée', 'No registered creation', 'Ninguna realización guardada', 'no_rea-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(648, 'pj', 'Pièces jointes', 'Attached Files', 'Documentos adjuntos', 'pj-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(649, 'no_comm', 'Aucun commentaire supplémentaire', 'No additional comments', 'Ninguno comentario suplementario', 'no_comm-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(650, 'label_comm', 'Commentaire', 'Comments', 'Comentario', 'label_comm-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(651, 'select_entretien', 'Proposer un entretien vidéo', 'Suggest a video call', 'Proponer una entrevista vídeo', 'select_entretien-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(652, 'select_prop_projet', 'Proposer un projet', 'Suggest a project', 'Proponer un proyecto', 'select_prop_projet-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(653, 'select_supp_fav', 'Supprimer des favoris', 'Delete from favourites', 'Suprimir de los favoritos', 'select_supp_fav-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(654, 'select_add_favoris', 'Ajouter aux favoris', 'Add to favourites', 'Añadir a los favoritos', 'select_add_favoris-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(655, 'select_all', 'Tout cocher', 'Select all ', 'Marcar todo', 'select_all-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(656, 'btTchater', 'Tchater', 'Version en', 'Version es', 'btTchater-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(657, 'label_archiver', 'Archivée(s)', 'Archived', 'Archivada(s)', 'label_archiver-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(658, 'label_pj', 'Pièces jointes', 'Attached files', 'Documentos adjuntos', 'label_pj-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(659, 'label_cat', 'Catégorie(s)', 'Categories', 'Categoría(s) ', 'label_cat-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(660, 'label_poste', 'Poste ', 'Position', 'Puesto ', 'label_poste-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(661, 'label_add_poste', 'Ajouter un poste ', 'Add a position', 'Añadir a puesto ', 'label_add_poste-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(662, 'label_periode', 'Période ', 'Period', 'Periodo ', 'label_periode-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(663, 'facture', 'FACTURE', 'INVOICE', 'FACTURA', 'facture-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(664, 'paie_ligne', 'Paiement en ligne', 'Online payment', 'Pago en línea ', 'paie_ligne-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(665, 'ref', 'REFERENCE', 'REFERENCE', 'REFERENCIA', 'ref-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(666, 'design', 'DESIGNATION', 'DESIGNATION', 'DETALLE', 'design-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(667, 'quantite', 'QUANTITE', 'QUANTITY', 'CANTIDAD', 'quantite-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(668, 'puht', 'P.U. HT', 'U.P. ex-tax', 'P.U. sin IVA', 'puht-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(669, 'montant', 'MONTANT H.T', 'AMOUNT NET OF TAX', 'MONTO sin IVA', 'montant-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(670, 'tva', 'TVA', 'VAT', 'IVA', 'tva-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(671, 'renouvellement_abo_periode', 'Renouvellement de votre abonnement jusqu''au {{date}}', 'Renewal of your membership until {{date}}', 'Renovación de tu suscripción hasta el {{date}}', 'renouvellement_abo_periode-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(672, 'renouvellement_abo_quotas', 'Achat de {{quota}}', 'Purchase of {{quota}}', 'Compra de {{quota}}', 'renouvellement_abo_quotas-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(673, 'profil_complete_fixe', 'Précisez un numéro de téléphone fixe (+5%)', 'Enter a house number (+5%)', 'Indicar un número de teléfono fijo (+5%)', 'profil_complete_fixe-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(674, 'profil_complete_portable', 'Précisez un numéro de téléphone portable (+5%)', 'Enter a mobile number (+5%)', 'Indicar un número de celular (+5%)', 'profil_complete_portable-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(675, 'profil_complete_naissance', 'Ajoutez votre date de naissance (+5%)', 'Add your birth date (+5%)', 'Añadir tu fecha de nacimiento (+5%)', 'profil_complete_naissance-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(676, 'profil_complete_exp', 'Ajoutez une expérience professionnelle (+5%)', 'Add a professional experience (+5%)', 'Añadir una experiencia profesional (+5%)', 'profil_complete_exp-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(677, 'profil_complete_postes', 'Indiquez le type de poste(s) recherché(s) (+10%)', 'Indicate the type of positions sought (+10%)', 'Indicar el tipo de puesto(s) buscado(s) (+10%)', 'profil_complete_postes-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(678, 'profil_complete_comp', 'Indiquez vos compétences (+10%)', 'Indicate your skills (+10%)', 'Indicar tus aptitudes profesionales (+10%)', 'profil_complete_comp-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(679, 'profil_complete_exp_pro', 'Ajoutez une expérience professionnelle (+10%)', 'Add a professional experience (+10%)', 'Añadir una experiencia profesional (+10%)', 'profil_complete_exp_pro-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(680, 'profil_complete_formation', 'Ajoutez une formation (+10%)', 'Add a school (+10%)', 'Añadir una formación (+10%)', 'profil_complete_formation-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(681, 'profil_complete_langues', 'Ajoutez les langues maîtrisées (+5%)', 'Add languages (+5%)', 'Añadir los idiomas (+5%)', 'profil_complete_langues-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(682, 'profil_complete_book', 'Ajoutez un book en ligne (+5%)', 'Add an online book (+5%)', 'Añadir un book en línea (+5%)', 'profil_complete_book-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(683, 'profil_complete_permis', 'Avez-vous le permis de conduire ? (+5%)', 'Do you have your driving license ? (+5%)', '¿ Tienes la licencia de conducir ? (+5%)', 'profil_complete_permis-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(684, 'profil_complete_mob', 'Précisez votre mobilité (+5%)', 'Indicate your mobility (+5%)', 'Indicar tu movilidad (+5%)', 'profil_complete_mob-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(685, 'profil_complete_rea', 'Ajoutez des réalisations (+10%)', 'Add creations (+10%)', 'Añadir algunas realizaciones (+10%)', 'profil_complete_rea-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(686, 'tous_champs', 'Tous les champs marqué d''une étoile sont requis', 'All fields with an asterisk are mandatory', 'Todos los campos marcados de un asterisco están requeridos', 'tous_champs-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(687, 'au_moins_un', 'Vous devez renseigner au moins un champs parmis les suivant : ', 'You must fill in at least one of the fields among the following : ', 'Debes completar al menos un campo entre los siguientes :  ', 'au_moins_un-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(688, 'label_adresse_site', 'Site Internet', 'Website', 'Página web ', 'label_adresse_site-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(689, 'select_fichier', 'Choisissez un fichier', 'Choose a file', 'Elige un archivo', 'select_fichier-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(690, 'select', 'Sélectionner', 'Select', 'Seleccionar', 'select-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(691, 'label_last_update', 'Dernière mise à jour', 'Latest updates', 'Última actualización ', 'label_last_update-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(692, 'label_action', 'Actions', 'Actions', 'Acciones', 'label_action-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(693, 'send_me', 'Me l''envoyer par email', 'Send it to me by email', 'Enviarmelo por email', 'send_me-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(694, 'dep_vers', 'Deplacer vers ', 'Move to ', 'Mover hacia ', 'dep_vers-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(695, 'prop_retenu', 'Proposition retenue', 'Selected proposal', 'Propuesta retenida', 'prop_retenu-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(696, 'prop_declinee', 'Proposition déclinée', 'Declined proposal', 'Propuesta declinada', 'prop_declinee-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(697, 'prop_attente', 'Proposition en cours d''étude', 'Currently processing proposal', 'Propuesta en curso de estudio', 'prop_attente-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(698, 'send_msg', 'Envoyer un message', 'Send a message', 'Enviar un mensaje', 'send_msg-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(699, 'entre_le', 'Entre le ', 'Between ', 'Entre el ', 'entre_le-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(700, 'et_le', 'et le ', 'and ', 'y el ', 'et_le-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(701, 'label_cgu_confirm', 'J''ai lu et j''accepte les conditions générales d''utilisation décrites dans les ', 'I hereby agree and confirm that I have read and accepted  ', 'Leí y acepto las condiciones generales de uso descritas en las ', 'label_cgu_confirm-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(702, 'label_cgu', 'CGU', 'the terms and conditions', 'CGU', 'label_cgu-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05');
INSERT INTO `tra_traductions` (`id_traduction`, `libelle`, `tra_fr`, `tra_en`, `tra_es`, `trad_cle_unique`, `cree_le`, `modifie_le`) VALUES
(703, 'label_allow_sn', 'J''autorise WeMe a communiquer mon inscription sur mon réseau social ', 'I hereby authorise NeoFreelo to communicate my registration on my social network ', 'Autorizo a NeoFreelo a comunicar mi inscripción en mi red social ', 'label_allow_sn-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(704, 'prop_non_traitee', 'Proposition non traitée', 'Unread proposal', 'Propuesta no tratada', 'prop_non_traitee-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(705, 'label_cgv_confirm', 'J''ai lu et j''accepte les conditions générales de vente décrites dans les ', 'I hereby agree and confirm that I have read and accepted  ', 'Leí y acepto las condiciones generales de venta descritas en las ', 'label_cgv_confirm-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(706, 'label_cgv', 'CGV', 'the sales'' terms and conditions', 'CGV', 'label_cgv-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(707, 'ln_comment', ' vient de s’inscrire sur WeMe ! ', ' just signed up for NeoFreelo !', ' acaba de inscribirse en NeoFreelo !', 'ln_comment-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(708, 'ln_titre', 'Rejoindre la communauté WeMe !', 'Join the NeoFreelo community !', 'Unirme a la comunidad NeoFreelo !', 'ln_titre-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(709, 'ln_desc', 'WeMe, la plateforme de mise en relation pour les professionnels de la communication. <br/>WeMe, Creative place for creative people !', 'NeoFreelo, the website where companies and freelancers get in touch. <br/>', 'NeoFreelo, la plataforma para unir a los freelances y a las empresas de todos los sectores.', 'ln_desc-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(710, 'label_defaut_debut', '(la date de début a été mise par défaut à la date du jour)', '(the start date was set up by default as today''s date)', '(la fecha de inicio ha sido puesta por defecto a la fecha de hoy)', 'label_defaut_debut-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(711, 'label_defaut_fin', '(la date de fin a été mise par défaut à la date du jour)', '(the end date was set up by default as today''s date)', '(la fecha de fin ha sido puesta por defecto a la fecha de hoy)', 'label_defaut_fin-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(712, 'label_erreure_dates', 'Attention, la date de fin ne peut être antérieure à la date de début du projet ', 'the end date cannot be previous to the start date', 'Cuidado, la fecha de fin del proyecto no puede ser anterior a la fecha de inicio', 'label_erreure_dates-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(713, 'label_obligatoire_erreure', 'Tous les champs marqués d''une astérisque sont requis', 'All fields with an asterisk are mandatory', 'Todos los campos con asterisco están requeridos', 'label_obligatoire_erreure-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(714, 'notice_get_rep_by_mail', 'Vous avez demandé de recevoir par email les propositions reçues pour ce projet', 'You have asked to receive responses for this project by email', 'Pediste recibir por email las propuestas recibidas para este proyecto', 'notice_get_rep_by_mail-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(715, 'label_select_maintenant', 'Maintenant', 'Now', 'Ahora', 'label_select_maintenant-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(716, 'entretien_attente_rep', 'En attente de réponse', 'Waiting for a response', 'Esperando respuesta', 'entretien_attente_rep-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(717, 'entretien_dispo', 'Disponible', 'Available', 'Disponible', 'entretien_dispo-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(718, 'entretien_indispo', 'Indisponible', 'Unavailable', 'Indisponible', 'entretien_indispo-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(719, 'label_tjm', 'Tarif jour moyen (TJM)', 'Average daily rate', 'Remuneración diaria pretendida', 'label_tjm-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(720, 'bt_ln_inscription', 'S''inscrire avec LinkedIn', 'Sign up via LinkedIn', 'Inscribirse con LinkedIn', 'bt_ln_inscription-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(721, 'bt_ln_not_you', 'Vous n''êtes pas ', 'You are not ', 'No estás', 'bt_ln_not_you-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(722, 'entretien_confirme', 'Entretien confirmé', 'Confirmed interview', 'Entrevista confirmada', 'entretien_confirme-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(723, 'entretien_termine', 'Entretien terminé', 'Interview ended', 'Entrevista terminada', 'entretien_termine-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(724, 'entretien_valid', 'En attente de validation', 'Waiting for validation', 'Esperando validación', 'entretien_valid-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(725, 'bt_participants', 'Voir participants', 'See participants', 'Ver participantes', 'bt_participants-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(726, 'bt_modif_horaires', 'Modifier les horaires', 'Edit the schedule', 'Modificar los horarios', 'bt_modif_horaires-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(727, 'bt_confirm_entretien', 'Confirmer cet entretien', 'Confirm this interview', 'Confirmar esta entrevista', 'bt_confirm_entretien-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(728, 'bt_rejoindre_session', 'Rejoindre cette session', 'Join this session', 'Unirme a esta sesión', 'bt_rejoindre_session-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(729, 'bt_rejoindre_session_pop', 'Rejoindre cette session (pop up)', 'Join this session (pop up)', 'Unirme a esta sesión (pop up)', 'bt_rejoindre_session_pop-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(730, 'select_projet', 'Sélectionnez un projet', 'Select a project', 'Selecciona un proyecto', 'select_projet-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(731, 'select_profil', 'Sélectionnez un profil dans vos favoris', 'Select a profile in your favourite CV', 'Selecciona un perfil en sus favoritos', 'select_profil-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(732, 'sujet_prop_mission', 'Offre de projet', 'Project''s offer', 'Oferta de proyecto', 'sujet_prop_mission-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(733, 'bt_repondre', 'Répondre', 'Reply', 'Responder', 'bt_repondre-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(734, 'label_de', 'De ', 'From ', 'De', 'label_de-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(735, 'label_sujet', 'Objet', 'Subject ', 'Asunto', 'label_sujet-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(736, 'label_recu', 'Reçu ', 'Received', 'Recibido', 'label_recu-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(737, 'recadre_photo', 'Merci de sélectionner votre zone de recadrage', 'Please select the cropping area', 'Por favor selecciona tu zona de reencuadramiento', 'recadre_photo-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(738, 'bt_utiliser_cam', 'Utiliser WebCam', 'Use WebCam', 'Utilizar WebCam', 'bt_utiliser_cam-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(739, 'need_cgu', 'Vous devez accepter les CGU', 'You must accept the Users'' terms and conditions', 'Debes aceptar las CGU', 'need_cgu-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(740, 'need_cgv', 'Vous devez accepter les CGV', 'You must accept the Sales'' terms and conditions', 'Debes aceptar las CGV', 'need_cgv-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(741, 'invalide_phone', 'Téléphone invalide', 'Incorrect phone number', 'Teléfono no valido', 'invalide_phone-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(742, 'invalide_age', 'Age invalide', 'Invalid age', 'Edad no valida', 'invalide_age-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(743, 'invalide_mail', 'Adresse email invalide', 'Invalid email address', 'Dirección email no valida', 'invalide_mail-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(744, 'label_adresse_email', 'Adresse email', 'Email address', 'Dirección email', 'label_adresse_email-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(745, 'invalide_url', 'Url invalide', 'Invalid link', 'URL no valida', 'invalide_url-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(746, 'invalide_cp', 'Code postal invalide', 'Incorrect postcode', 'Código postal no valido', 'invalide_cp-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(747, 'invalide_date', 'La date est invalide', 'The date is invalid', 'Fecha no valida', 'invalide_date-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(748, 'mdp_no_match', 'Les mots de passe indiqués ne sont pas identiques', 'The typed passwords are not identical', 'Las contraseñas indicadas no son idénticas', 'mdp_no_match-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(749, 'need_zip', 'Merci de nous fournir une archive de type .zip uniquement', 'Please attach an archive in a .zip format only', 'Por favor entrega un archivo de tipo .zip', 'need_zip-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(750, 'need_image', 'Merci d''importer vos images', 'Please import your images', 'Por favor importa tus imagenes', 'need_image-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(751, 'sup_max_size', 'L''image que vous tentez d''importer doit être inférieur à ', 'The image that you try to import must be inferior to ', 'La imagen que intentas importar debe ser inferior a  ', 'sup_max_size-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(752, 'invalide_format_image', 'L''image que vous tentez d''importer n''est pas un format valide', 'The image that you try to import is not in a valid format', 'La imagen que intentas importar no tiene un formato valido ', 'invalide_format_image-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(753, 'sup_largeur_max', 'La largeur de l''image doit être inférieure à ', 'The width of the image must be inferior to ', 'El ancho de la imagen debe ser inferior a ', 'sup_largeur_max-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(754, 'sup_hauteur_max', 'La hauteur de l''image doit être inférieure à ', 'The height of the image must be inferior to ', 'La altura de la imagen debe ser inferior a ', 'sup_hauteur_max-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(755, 'date_anterieure', 'Vous ne pouvez pas sélectionner une date de disponibilité antérieure à la date du jour', 'You cannot select a date of availability previous to today''s date', 'No podés seleccionar una fecha de disponibilidad anterior a la de hoy', 'date_anterieure-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(756, 'invalide_siret', 'Le siret est invalide', 'The registration number of the company is incorrect', 'El número de identificación fiscal no es valido', 'invalide_siret-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(757, 'lundi', 'Lundi', 'Monday', 'Lunes', 'lundi-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(758, 'mardi', 'Mardi', 'Tuesday', 'Martes', 'mardi-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(759, 'mercredi', 'Mercredi', 'Wednesday', 'Miércoles', 'mercredi-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(760, 'jeudi', 'Jeudi', 'Thursday', 'Jueves', 'jeudi-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(761, 'vendredi', 'Vendredi', 'Friday', 'Viernes', 'vendredi-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(762, 'samedi', 'Samedi', 'Saturday', 'Sábado', 'samedi-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(763, 'dimanche', 'Dimanche', 'Sunday', 'Domingo', 'dimanche-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(764, 'jan', 'Janvier', 'January', 'Enero', 'jan-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(765, 'fev', 'Fevrier', 'February', 'Febrero', 'fev-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(766, 'mar', 'Mars', 'March', 'Marzo', 'mar-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(767, 'avr', 'Avril', 'April', 'Abril', 'avr-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(768, 'mai', 'Mai', 'May', 'Mayo', 'mai-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(769, 'jui', 'Juin', 'June', 'Junio', 'jui-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(770, 'juil', 'Juillet', 'July', 'Julio', 'juil-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(771, 'aou', 'Aout', 'August', 'Agosto', 'aou-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(772, 'sept', 'Septembre', 'September', 'Septiembre', 'sept-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(773, 'oct', 'Octobre', 'October', 'Octubre', 'oct-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(774, 'nov', 'Novembre', 'November', 'Noviembre', 'nov-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(775, 'dec', 'Décembre', 'December', 'Diciembre', 'dec-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(776, 'invalide_adresse', 'Adresse invalide', 'Incorrect address', 'Dirección no valida', 'invalide_adresse-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(777, 'infos_comp', 'Informations complémentaires', 'Additional information', 'Información complementaria', 'infos_comp-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(778, 'label_profil_concerne', 'Profil(s) concerné(s) : ', 'Profile(s) concerned : ', 'Perfil(es) seleccionado(s) : ', 'label_profil_concerne-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(779, 'label_expediteur', 'Expediteur', 'Sender', 'Expediente', 'label_expediteur-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(780, 'equipe', 'L''équipe ', 'The team ', 'El Equipo ', 'equipe-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(781, 'label_hote', 'Hôte', 'Host', 'Huésped ', 'label_hote-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(782, 'session_archive', 'Afficher mes sessions archivées', 'Display my archived sessions', 'Mostrar mis sesiones archivadas', 'session_archive-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(783, 'label_code', 'Code d''accès', 'Access code', 'Código de acceso', 'label_code-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(784, 'menu_mon_profil', 'Mon profil', 'My profile', 'Mi perfil', 'menu_mon_profil-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(785, 'menu_mon_curriculum', 'Mon parcours', 'My background', 'Mi carrera', 'menu_mon_curriculum-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(786, 'menu_mon_portfolio', 'Mes réalisations', 'My creations', 'Mi portfolio', 'menu_mon_portfolio-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(787, 'menu_mes_profils', 'Mes profils', 'My profiles', 'Mis perfiles', 'menu_mes_profils-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(788, 'menu_mon_compte', 'Mon compte', 'my account', 'Mi cuenta', 'menu_mon_compte-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(789, 'menu_vos_profils', 'Mes profils', 'My profiles', 'Mis perfiles', 'menu_vos_profils-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(790, 'menu_mon_cv', 'Mon CV', 'My CV', 'Mi CV', 'menu_mon_cv-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(791, 'menu_tableau_bord', 'Tableau de bord', 'Dashboard', 'Panel de Control', 'menu_tableau_bord-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(792, 'menu_boite_reception', 'boite de réception', 'inbox', 'Buzón', 'menu_boite_reception-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(793, 'menu_rechercher_projets', 'rechercher des projets', 'search for projects', 'Buscar proyectos', 'menu_rechercher_projets-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(794, 'menu_mes_propositions', 'Mes propositions', 'my proposals', 'Mis propuestas', 'menu_mes_propositions-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(795, 'menu_mes_entretiens', 'mes entretiens', 'my interviews', 'Mis entrevistas', 'menu_mes_entretiens-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(796, 'menu_accueil', 'Accueil', 'Home', 'Inicio', 'menu_accueil-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(797, 'menu_trouver_prestataire', 'Trouver un prestataire', 'Find a Freelancer', 'Encontrar un freelance', 'menu_trouver_prestataire-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(798, 'menu_trouver_projet', 'Trouver un projet', 'Find a Project', 'Encontrar un proyecto', 'menu_trouver_projet-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(799, 'menu_inscription', 'Inscription', 'Sign Up', 'Inscripción', 'menu_inscription-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(800, 'menu_gerer_projets', 'gérer mes projets', 'Manage my projects', 'gestionar mis proyectos', 'menu_gerer_projets-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(801, 'menu_cvteque', 'cvthèque', 'Cv library', 'CVteca', 'menu_cvteque-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(802, 'bt_editer_avatar', 'Modifier ma photo', 'Edit my picture', 'Modificar mi foto', 'bt_editer_avatar-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(803, 'bt_editer_logo', 'Modifier mon logo', 'Edit my logo', 'Modificar mi logo', 'bt_editer_logo-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(804, 'bt_no_webcam', 'Je n''ai pas de webcam ', 'I do not have a webcam', 'No tengo WebCam', 'bt_no_webcam-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(805, 'bt_enter_session', 'rejoindre session', 'join session', 'Unirse a sesión', 'bt_enter_session-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(806, 'bt_acheter_cv', 'Acheter ce CV', 'Buy this CV', 'Comprar este CV', 'bt_acheter_cv-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(807, 'bt_transfere', 'Transférer à un ami', 'Forward to a friend', 'Transferir a un amigo', 'bt_transfere-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(808, 'label_recall', 'Je souhaite être appelé par l''équipe WeMe', 'I wish to receive a call from the NeoFreelo team', 'Me gustaría ser llamado por el equipo NeoFreelo', 'label_recall-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(809, 'label_supprimer_confirm', 'Vous êtes sur le point de supprimer une entrée, souhaitez vous continuer?', 'You are about to delete a data, do you wish to carry on ?', '¿ Estás a punto de suprimir una entrada, deseas seguir ?', 'label_supprimer_confirm-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(810, 'label_ajouter_localite', 'Ajouter un lieu', 'Add a location', 'Añadir un lugar', 'label_ajouter_localite-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(811, 'label_code_invite', 'Le code d''accès de votre invité : ', 'The access code for the freelancer : ', 'El código de acceso de tu invitado : ', 'label_code_invite-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(812, 'label_header_entretien', 'Entretien Vidéo WeMe', 'NeoFreelo Video call  ', 'Entrevista Video NeoFreelo', 'label_header_entretien-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(813, 'label_mail_reponse_short', 'Réponses par email : ', 'Responses by email : ', 'Respuestas por email : ', 'label_mail_reponse_short-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(814, 'label_mail_reponse', 'Recevoir les réponses par email : ', 'Receive responses by email : ', 'Recibir las respuestas por email : ', 'label_mail_reponse-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(815, 'label_prevue', 'Date prévue ', 'Date', 'Fecha prevista ', 'label_prevue-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(816, 'label_auteur', 'Auteur ', 'Author ', 'Autor  ', 'label_auteur-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(817, 'label_apropos_session', 'A propos de la session sélectionnée', 'About the selected session', 'Sobre la sesión seleccionada', 'label_apropos_session-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(818, 'label_entretien_encours', 'Suivi de vos entretiens vidéos', 'Follow-up of your video calls', 'Seguimiento de tus entrevistas video', 'label_entretien_encours-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(819, 'bt_creer_session', 'Créer une session', 'Start a session', 'Crear una sesión video', 'bt_creer_session-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(820, 'label_code_title', 'code d''accès', 'access code', 'codigo de acceso', 'label_code_title-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(821, 'label_nom_universite', 'Ecole', 'University', 'Universidad', 'label_nom_universite-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(822, 'invalide_email', 'Votre email est invalide', 'Your email address is incorrect', 'Tu email está inválido', 'invalide_email-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(823, 'facture_page', 'PAGE', 'PAGE', 'PAGINA', 'facture_page-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(824, 'facture_date', 'DATE', 'DATE', 'FECHA', 'facture_date-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(825, 'facture_client', 'CLIENT', 'CLIENT', 'CLIENTE', 'facture_client-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(826, 'facture_mode_reglement', 'MODE DE REGLEMENT', 'WAY OF PAIEMENT', 'FORMA DE PAGO', 'facture_mode_reglement-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(827, 'facture_tva_intra', 'TVA intracommunautaire', 'VAT', 'IVA intracomunitaria', 'facture_tva_intra-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(828, 'facture_remarque', 'Remarque :', 'Comment : ', 'Comentario :', 'facture_remarque-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(829, 'facture_aucune_remarque', 'aucune remarque', 'no comment', 'Ningún comentario', 'facture_aucune_remarque-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(830, 'facture_bases_ht', 'BASES HT', 'PRICE EX-TAX', 'BASES sin IVA', 'facture_bases_ht-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(831, 'facture_remise', 'REMISE', 'DISCOUNT', 'DESCUENTO', 'facture_remise-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(832, 'facture_prc_tva', '%TVA', '%VAT', '%IVA', 'facture_prc_tva-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(833, 'facture_port', 'PORT', 'SHIPPING', 'CARGOS DE ENVIO', 'facture_port-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(834, 'facture_totaux', 'TOTAUX', 'TOTAL', 'TOTALES', 'facture_totaux-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(835, 'facture_totaux_ht', 'H.T. :', 'EX.T. :', 'NETO :', 'facture_totaux_ht-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(836, 'facture_totaux_tva', 'T.V.A :', 'V.A.T :', 'IVA :', 'facture_totaux_tva-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(837, 'facture_total_ttc', 'TOTAL TTC', 'TOTAL incl. T.', 'TOTAL con IVA', 'facture_total_ttc-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(838, 'facture_acompte', 'ACOMPTE', 'DEPOSIT', 'DEPOSITO', 'facture_acompte-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(839, 'facture_net_a_payer', 'NET A PAYER', 'NET BALANCE', 'NETO A PAGAR', 'facture_net_a_payer-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(840, 'facture_mt_tva', 'MT TVA', 'AMOUNT VAT', 'MONTO IVA', 'facture_mt_tva-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(841, 'notifictaion_champs_obligatoir', 'Les champs marqués d''une astérisque sont obligatoires', 'Fields marked with an asterisk are mandatory', 'Todos los campos con asterisco son obligatorios', 'notifictaion_champs_obligatoir-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(842, 'notifictaion_date_invalide', 'Pour saisir une date de fin vous devez saisir une date de début', 'To enter an ending date, you must enter a start date', 'Para introducir una fecha de fin, tenés que introducir una fecha de inicio', 'notifictaion_date_invalide-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(843, 'notifictaion_saisie_reponse', 'Veuillez saisir votre message', 'Please enter your message', 'Por favor, introduce tu mensaje', 'notifictaion_saisie_reponse-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(844, 'notifictaion_saisie_profil', 'Merci de sélectionner un ou des profils afin de pouvoir transmettre votre proposition', 'Please select one or more profiles to send your proposal ', 'Por favor, selecciona uno o varios perfiles para poder enviar tu propuesta', 'notifictaion_saisie_profil-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(845, 'notifictaion_budget_invalide', 'Votre budget est invalide !', 'Your budget is incorrect !', 'Tu presupuesto es inválido', 'notifictaion_budget_invalide-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(846, 'label_modif_entete_cv', 'Modifier l''entête du cv', 'Edit the CV header', 'Modificar el encabezado del CV', 'label_modif_entete_cv-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(847, 'notification_champs_invalide', '{{entry}} est invalide', '{{entry}} is incorrect\n', '{{entry}} está inválido', 'notification_champs_invalide-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(848, 'label_validation_paypal', 'Valider le paiement', 'Confirm your payment', 'Confirmar el pago', 'label_validation_paypal-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(849, 'label_entrer_videosession', 'Entrer', 'Enter', 'Ingresar', 'label_entrer_videosession-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(850, 'hpLangChooser', 'Français', 'English', 'Español', 'hpLangChooser-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(851, 'francais', 'Français', 'French', 'Francès', 'francais-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(852, 'anglais', 'Anglais', 'English', 'Inglès', 'anglais-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(853, 'espagnol', 'Espagnol', 'Spanish', 'Español', 'espagnol-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(854, 'presentation', 'Presentation', 'Presentation', 'Presentación', 'presentation-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(855, 'identite', 'Identité', 'Identity', 'Version Es', 'identite-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(856, 'searchResult', 'Résultat de votre recherche : ', 'Version En', 'Version Es', 'searchResult-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(857, 'profil', 'profil', 'profile', 'perfil', 'profil-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(858, 'profils', 'profils', 'profile', 'perfil', 'profils-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(859, 'indifferent', 'Indifférent', 'Version An', 'Version Es', 'indifferent-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(860, 'bt_plus_details', 'Plus de détails', 'More details', 'Version Es', 'bt_plus_details-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(861, 'missions', 'missions', 'projects', 'proyectos', 'missions-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(862, 'mission', 'mission', 'project', 'proyecto', 'mission-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(863, 'no_mission_match', 'Aucune mission de diponible pour la recherche', 'No matching result found', 'Ningún proyecto corresponde a tu búsqueda', 'no_mission_match-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(864, 'label_devise', 'Devise', 'version En', 'version Es', 'label_devise-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(865, 'minus_week', 'Il y a moins d''une semaine', 'Less than a week ago', 'Hace menos de una semana', 'minus_week-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(866, 'minus_month', 'Il y a moins d''un mois', 'Less than a month ago', 'Hace menos de un mes', 'minus_month-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(867, 'minus_six_month', 'Il y a moins de 6 mois', 'Less than 6 months ago', 'Hace menos de 6 meses', 'minus_six_month-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(868, 'today', 'Aujourd''hui', 'Today', 'Hoy', 'today-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(869, 'searchProjet', 'Rechercher un projet...', 'search for a project', 'Buscar un proyecto', 'searchProjet-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05'),
(870, 'searchCv', 'Rechercher un CV...', 'Search for a CV', 'Buscar un CV', 'searchCv-P', '2013-08-30 12:14:05', '2013-08-30 14:14:05');

-- --------------------------------------------------------

--
-- Structure de la table `usr_clients`
--

CREATE TABLE `usr_clients` (
  `id_client` int(255) NOT NULL,
  `cl_standard` varchar(255) DEFAULT NULL,
  `cl_standard_indicatif` varchar(10) DEFAULT NULL,
  `cl_entreprise` varchar(255) DEFAULT NULL,
  `cl_presentation` text,
  `cl_effectif` varchar(255) DEFAULT NULL,
  `cl_siret` varchar(45) DEFAULT NULL,
  `cl_siteweb` varchar(45) DEFAULT NULL,
  `cl_add_liv_rue` varchar(45) DEFAULT NULL,
  `cl_add_liv_codepostal` varchar(45) DEFAULT NULL,
  `cl_add_liv_ville` varchar(45) DEFAULT NULL,
  `cl_add_liv_pays` varchar(45) DEFAULT NULL,
  `cl_add_fac_rue` varchar(45) DEFAULT NULL,
  `cl_add_fac_codepostal` varchar(45) DEFAULT NULL,
  `cl_add_fac_ville` varchar(45) DEFAULT NULL,
  `cl_add_fac_pays` varchar(45) DEFAULT NULL,
  `var` varchar(45) DEFAULT NULL,
  `modifie_le` datetime NOT NULL,
  `cree_le` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `usr_clients`
--

INSERT INTO `usr_clients` (`id_client`, `cl_standard`, `cl_standard_indicatif`, `cl_entreprise`, `cl_presentation`, `cl_effectif`, `cl_siret`, `cl_siteweb`, `cl_add_liv_rue`, `cl_add_liv_codepostal`, `cl_add_liv_ville`, `cl_add_liv_pays`, `cl_add_fac_rue`, `cl_add_fac_codepostal`, `cl_add_fac_ville`, `cl_add_fac_pays`, `var`, `modifie_le`, `cree_le`) VALUES
(1, '713390383', '+06', 'Streetcolor', '', '', '', '', '', '', '', '', '8 rue Paul Gimont', '92500', 'Rueil Malmaison', 'France', NULL, '2015-03-03 22:07:26', '2015-03-03 20:53:42');

-- --------------------------------------------------------

--
-- Structure de la table `usr_clients_managers`
--

CREATE TABLE `usr_clients_managers` (
  `id_client_manager` int(255) NOT NULL,
  `id_identifiant` int(255) DEFAULT NULL,
  `id_client` int(255) DEFAULT NULL,
  `id_role` int(1) DEFAULT NULL,
  `id_civilite` int(1) DEFAULT NULL,
  `mg_nom` varchar(45) DEFAULT NULL,
  `mg_prenom` varchar(45) DEFAULT NULL,
  `mg_portable_indicatif` varchar(45) DEFAULT NULL,
  `mg_portable` varchar(45) DEFAULT NULL,
  `mg_fixe_indicatif` varchar(45) DEFAULT NULL,
  `mg_fixe` varchar(45) DEFAULT NULL,
  `var` varchar(45) DEFAULT NULL,
  `cree_le` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifie_le` varchar(45) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `usr_clients_managers`
--

INSERT INTO `usr_clients_managers` (`id_client_manager`, `id_identifiant`, `id_client`, `id_role`, `id_civilite`, `mg_nom`, `mg_prenom`, `mg_portable_indicatif`, `mg_portable`, `mg_fixe_indicatif`, `mg_fixe`, `var`, `cree_le`, `modifie_le`) VALUES
(1, 1, 1, 1, 1, 'Tetard', 'Sébastien', '', '', '', '', NULL, '2015-03-03 20:53:42', '2015-03-03 22:07:26');

-- --------------------------------------------------------

--
-- Structure de la table `usr_clients_messages`
--

CREATE TABLE `usr_clients_messages` (
  `id_client_message` int(255) NOT NULL,
  `id_identifiant` int(255) NOT NULL COMMENT 'id du client',
  `mess_titre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mess_message` longtext COLLATE utf8_unicode_ci NOT NULL,
  `cree_le` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifie_le` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `usr_files`
--

CREATE TABLE `usr_files` (
  `id_file` int(255) NOT NULL,
  `id_identifiant` int(255) NOT NULL,
  `id_prestataire_profil` varchar(255) DEFAULT NULL,
  `fichier_reference` varchar(255) NOT NULL,
  `fichier_nom` varchar(45) DEFAULT NULL,
  `fichier_path` varchar(45) DEFAULT NULL,
  `fichier_extension` varchar(45) DEFAULT NULL,
  `fichier_description` text,
  `fichier_type` int(1) NOT NULL COMMENT '1 :visualisable \n2 : telechargeable \n3 : consultable \n4:CV \n5:piece jointe \n6 : Avatar',
  `var` varchar(45) DEFAULT NULL,
  `cree_le` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifie_le` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `usr_identifiants`
--

CREATE TABLE `usr_identifiants` (
  `id_identifiant` int(255) NOT NULL,
  `id_type` int(1) NOT NULL COMMENT '1: Client 2 : Prestataire',
  `usr_trad` varchar(5) NOT NULL COMMENT 'Es, Fr, EN',
  `usr_email` varchar(45) NOT NULL,
  `usr_password` varchar(45) NOT NULL,
  `usr_activate` tinyint(1) NOT NULL COMMENT 'Si le user active son mail',
  `actif` tinyint(1) NOT NULL,
  `cree_le` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifie_le` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='usr_modifie_le';

--
-- Contenu de la table `usr_identifiants`
--

INSERT INTO `usr_identifiants` (`id_identifiant`, `id_type`, `usr_trad`, `usr_email`, `usr_password`, `usr_activate`, `actif`, `cree_le`, `modifie_le`) VALUES
(1, 3, 'fr', 'sebastien.tetard@yahoo.fr', '*F22F61E0D74109FC2FDA071ECA51B6A09097C88C', 1, 1, '2015-03-03 20:53:42', '2015-03-03 21:53:42'),
(2, 2, 'fr', 'sebastien.tetard@gmail.com', '*F22F61E0D74109FC2FDA071ECA51B6A09097C88C', 1, 1, '2015-03-03 21:40:22', '2015-03-03 22:40:22');

-- --------------------------------------------------------

--
-- Structure de la table `usr_jabbers`
--

CREATE TABLE `usr_jabbers` (
  `id_jabber` int(255) NOT NULL,
  `id_identifiant` int(11) NOT NULL,
  `jbr_loggin` varchar(255) NOT NULL,
  `jbr_password` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `usr_logs`
--

CREATE TABLE `usr_logs` (
  `id_log` int(255) NOT NULL,
  `id_identifiant` int(255) NOT NULL,
  `cree_le` datetime NOT NULL,
  `modifie_le` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `usr_logs`
--

INSERT INTO `usr_logs` (`id_log`, `id_identifiant`, `cree_le`, `modifie_le`) VALUES
(1, 1, '2015-03-03 21:53:42', '2015-03-03 21:53:42'),
(2, 2, '2015-03-03 22:40:23', '2015-03-03 22:40:23'),
(3, 1, '2015-03-08 22:00:50', '2015-03-08 22:00:50');

-- --------------------------------------------------------

--
-- Structure de la table `usr_managers_favoris`
--

CREATE TABLE `usr_managers_favoris` (
  `id_favori` int(255) NOT NULL,
  `id_identifiant` int(45) NOT NULL,
  `id_prestataire_profil` int(255) NOT NULL,
  `composante` varchar(10) NOT NULL COMMENT 'id_prestataire_profil+id_identifiant',
  `cree_le` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifie_le` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `usr_missions`
--

CREATE TABLE `usr_missions` (
  `id_mission` int(255) NOT NULL,
  `id_client` int(255) NOT NULL,
  `id_identifiant` int(11) NOT NULL,
  `id_categorie` int(11) DEFAULT NULL,
  `id_experience` int(25) NOT NULL,
  `id_mission_statut` int(1) NOT NULL COMMENT 'Statut :\nEn attente\nEn cours\nSuspendu\nTerminée\n',
  `mi_reference` varchar(10) NOT NULL,
  `mi_titre` text NOT NULL,
  `mi_description` text NOT NULL,
  `mi_tarif` int(10) DEFAULT NULL COMMENT 'En euro ou dollars ou yen...',
  `id_monnaie` int(1) DEFAULT NULL,
  `id_contrat` int(1) DEFAULT NULL COMMENT 'Forfait ou Régie',
  `mi_debut` date DEFAULT NULL,
  `mi_fin` date DEFAULT NULL,
  `mi_int_fin` int(3) DEFAULT NULL,
  `mi_string_fin` varchar(6) DEFAULT NULL,
  `mi_date_reponse` date DEFAULT NULL,
  `mi_republication` int(1) DEFAULT NULL COMMENT 'Nombre de fois que la mission est republié',
  `mi_reponse_mail` tinyint(1) NOT NULL COMMENT 'Si le client souhaite être notifié par email',
  `lo_quartier` varchar(50) NOT NULL,
  `lo_ville` varchar(50) NOT NULL,
  `lo_code_postal` varchar(50) NOT NULL,
  `lo_departement_short` varchar(255) NOT NULL,
  `lo_departement` varchar(50) NOT NULL,
  `lo_region_short` varchar(50) NOT NULL,
  `lo_region` varchar(50) NOT NULL,
  `lo_pays_short` varchar(50) NOT NULL,
  `lo_pays` varchar(50) NOT NULL,
  `mi_reponse_count` int(10) NOT NULL,
  `actif` tinyint(1) NOT NULL COMMENT 'Actif :\nDéstinée à la modération',
  `var` varchar(45) DEFAULT NULL,
  `cree_le` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifie_le` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `usr_missions_complements`
--

CREATE TABLE `usr_missions_complements` (
  `id_mission_complement` int(255) NOT NULL,
  `id_mission` int(255) NOT NULL,
  `id_complement` int(255) NOT NULL,
  `complement` int(1) NOT NULL COMMENT '1 : Langue\n2 : Experience\n3 : Etude\n4 : Competence\n5 : Poste',
  `composante` varchar(15) NOT NULL,
  `cree_le` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifie_le` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `usr_missions_reponses`
--

CREATE TABLE `usr_missions_reponses` (
  `id_mission_reponse` int(255) NOT NULL,
  `id_mission` int(255) NOT NULL,
  `id_identifiant` int(25) NOT NULL COMMENT 'id_identifiant du PRESTA',
  `id_prestataire_profil` varchar(255) NOT NULL,
  `rp_reponse` text NOT NULL,
  `rp_tarif` float NOT NULL,
  `id_monnaie` int(1) NOT NULL COMMENT '3 : CV decliné\n2: CV Retenu\n1: Online\n0 : Offline\n\nIf true = visible\nelse = not visible\n',
  `id_contrat` int(1) DEFAULT NULL,
  `id_file` varchar(255) DEFAULT NULL,
  `id_pj` int(255) DEFAULT NULL,
  `rp_commentaire` text,
  `rp_reponse_mail` tinyint(1) DEFAULT NULL COMMENT 'recevoir Reponse par email\n',
  `id_reponse_statut` int(1) DEFAULT NULL COMMENT 'Accepté par le clientDeclinéen attente',
  `rp_archiver` datetime DEFAULT NULL,
  `var` varchar(45) DEFAULT NULL,
  `cree_le` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifie_le` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `usr_prestataires`
--

CREATE TABLE `usr_prestataires` (
  `id_prestataire` int(255) NOT NULL,
  `id_identifiant` int(255) NOT NULL,
  `id_structure` int(1) NOT NULL,
  `pr_notification_mail` tinyint(1) NOT NULL,
  `pr_entreprise` varchar(45) DEFAULT NULL,
  `pr_presentation` varchar(255) DEFAULT NULL,
  `pr_standard_indicatif` varchar(255) DEFAULT NULL,
  `pr_standard` varchar(45) DEFAULT NULL,
  `pr_add_liv_rue` varchar(45) DEFAULT NULL,
  `pr_add_liv_codepostal` varchar(45) DEFAULT NULL,
  `pr_add_liv_ville` varchar(45) DEFAULT NULL,
  `pr_add_liv_pays` varchar(45) DEFAULT NULL,
  `pr_add_fac_rue` varchar(45) DEFAULT NULL,
  `pr_add_fac_codepostal` varchar(45) DEFAULT NULL,
  `pr_add_fac_ville` varchar(45) DEFAULT NULL,
  `pr_add_fac_pays` varchar(45) DEFAULT NULL,
  `var` varchar(45) DEFAULT NULL,
  `modifie_le` datetime NOT NULL,
  `cree_le` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `usr_prestataires`
--

INSERT INTO `usr_prestataires` (`id_prestataire`, `id_identifiant`, `id_structure`, `pr_notification_mail`, `pr_entreprise`, `pr_presentation`, `pr_standard_indicatif`, `pr_standard`, `pr_add_liv_rue`, `pr_add_liv_codepostal`, `pr_add_liv_ville`, `pr_add_liv_pays`, `pr_add_fac_rue`, `pr_add_fac_codepostal`, `pr_add_fac_ville`, `pr_add_fac_pays`, `var`, `modifie_le`, `cree_le`) VALUES
(1, 2, 1, 1, 'Streetcolor', '', '+33', '0671339038', '', '', '', '', '8 rue paul gimont', '92500', 'Rueil Malmaison', 'France', NULL, '2015-03-03 22:40:22', '2015-03-03 21:40:22');

-- --------------------------------------------------------

--
-- Structure de la table `usr_prestataires_favoris`
--

CREATE TABLE `usr_prestataires_favoris` (
  `id_prestataire_favoris` int(255) NOT NULL COMMENT '	',
  `id_identifiant` int(255) NOT NULL,
  `id_mission` int(255) NOT NULL,
  `composante` varchar(10) NOT NULL COMMENT 'id_mission+id_identifiant',
  `var` varchar(45) DEFAULT NULL,
  `cree_le` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifie_le` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `usr_prestataires_profils`
--

CREATE TABLE `usr_prestataires_profils` (
  `id_prestataire_profil` int(255) NOT NULL,
  `id_prestataire` int(255) NOT NULL,
  `id_identifiant` int(255) NOT NULL,
  `id_experience` int(255) DEFAULT NULL,
  `id_role` int(1) NOT NULL,
  `id_civilite` int(1) DEFAULT NULL,
  `pf_reference` varchar(45) DEFAULT NULL,
  `pf_nom` varchar(45) DEFAULT NULL,
  `pf_prenom` varchar(45) DEFAULT NULL,
  `pf_naissance` int(11) DEFAULT NULL,
  `pf_disponibilite` date DEFAULT NULL,
  `pf_siteweb` varchar(45) DEFAULT NULL,
  `pf_permis` tinyint(1) DEFAULT NULL,
  `pf_tjm` int(6) DEFAULT NULL,
  `id_monnaie` int(1) DEFAULT NULL,
  `pf_fixe` varchar(45) DEFAULT NULL,
  `pf_fixe_indicatif` varchar(10) DEFAULT NULL,
  `pf_portable` varchar(45) DEFAULT NULL,
  `pf_portable_indicatif` varchar(10) DEFAULT NULL,
  `pf_cv_titre` text,
  `pf_cv_description` longtext,
  `pf_cv_pdf_string` longtext,
  `pf_profil_statut` tinyint(1) DEFAULT NULL COMMENT 'Activer ou nom la visibilité dans la CVtèque\n',
  `pf_active_pdf` int(1) NOT NULL,
  `pf_pourcentage_identite` int(3) NOT NULL,
  `pf_pourcentage_formation` int(3) NOT NULL,
  `pf_pourcentage_competence` int(3) NOT NULL,
  `pf_pourcentage_experience` int(3) NOT NULL,
  `actif` tinyint(1) NOT NULL COMMENT 'Pour la modération\n',
  `var` varchar(45) DEFAULT NULL,
  `cree_le` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifie_le` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `usr_prestataires_profils`
--

INSERT INTO `usr_prestataires_profils` (`id_prestataire_profil`, `id_prestataire`, `id_identifiant`, `id_experience`, `id_role`, `id_civilite`, `pf_reference`, `pf_nom`, `pf_prenom`, `pf_naissance`, `pf_disponibilite`, `pf_siteweb`, `pf_permis`, `pf_tjm`, `id_monnaie`, `pf_fixe`, `pf_fixe_indicatif`, `pf_portable`, `pf_portable_indicatif`, `pf_cv_titre`, `pf_cv_description`, `pf_cv_pdf_string`, `pf_profil_statut`, `pf_active_pdf`, `pf_pourcentage_identite`, `pf_pourcentage_formation`, `pf_pourcentage_competence`, `pf_pourcentage_experience`, `actif`, `var`, `cree_le`, `modifie_le`) VALUES
(1, 1, 2, 0, 1, 1, 'A8VV3M', 'tetard', 'sebastien', 0, '2015-03-03', '', 0, 0, 0, '', '', '', '', 'Directeur technique , Artsper', 'Originaire de Paris et graphiste de formation, je suis à la fois rigoureux, attentif et ouvert d''esprit. Fort d’une expérience de plus de 5 ans et actuellement consultant depuis Février 2011 pour la société Keyrium. J''effectue des interventions coté client, dans le développement de plateformes web hautement interactives basées sur les technologies JavaScript (maitrise du Framework jQuery), HTML5 et CSS3. Développement par ailleurs des web Apps responsives destinées aux supports mobiles et tablettes en utilisant des technologies récentes (gestion du cache manifest, gestion de flux http pour utilisation de lecteurs vidéos mobile, gestion des requêtes Ajax).\r\n\r\nCoté serveur, je possède une maitrise parfaite de PHP 5 orienté objet ainsi que de solides connaissances en MySql (création et optimisation de requêtes lourdes dédiées à l’affichage et la gestion de données). Connaissances de l’environnement Linux permettant de configurer aisément des serveurs dédiés au web (gestion des users, des vhosts, installation de librairies, installation et configuration de MySQL)\r\n', '', 1, 0, 10, 15, 25, 25, 1, NULL, '2015-03-03 21:40:22', '2015-03-03 22:40:56');

-- --------------------------------------------------------

--
-- Structure de la table `usr_profils_complements`
--

CREATE TABLE `usr_profils_complements` (
  `id_profil_complement` int(255) NOT NULL,
  `id_prestataire_profil` int(255) NOT NULL,
  `id_niveau` int(1) NOT NULL,
  `id_complement` int(255) NOT NULL,
  `complement` int(1) NOT NULL COMMENT '1 : Langue\n2 : Experience\n3 : Etude\n4 : Competence\n5 : Poste',
  `composante` varchar(15) NOT NULL,
  `var` varchar(45) DEFAULT NULL,
  `cree_le` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifie_le` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `usr_profils_complements`
--

INSERT INTO `usr_profils_complements` (`id_profil_complement`, `id_prestataire_profil`, `id_niveau`, `id_complement`, `complement`, `composante`, `var`, `cree_le`, `modifie_le`) VALUES
(1, 1, 0, 573, 3, '1-573-3', NULL, '2015-03-03 21:40:56', '2015-03-03 22:40:56'),
(2, 1, 0, 574, 3, '1-574-3', NULL, '2015-03-03 21:40:56', '2015-03-03 22:40:56'),
(3, 1, 0, 575, 3, '1-575-3', NULL, '2015-03-03 21:40:56', '2015-03-03 22:40:56'),
(4, 1, 0, 576, 3, '1-576-3', NULL, '2015-03-03 21:40:56', '2015-03-03 22:40:56'),
(5, 1, 0, 577, 3, '1-577-3', NULL, '2015-03-03 21:40:56', '2015-03-03 22:40:56'),
(6, 1, 0, 578, 3, '1-578-3', NULL, '2015-03-03 21:40:56', '2015-03-03 22:40:56'),
(7, 1, 0, 579, 3, '1-579-3', NULL, '2015-03-03 21:40:56', '2015-03-03 22:40:56'),
(8, 1, 0, 580, 3, '1-580-3', NULL, '2015-03-03 21:40:56', '2015-03-03 22:40:56'),
(9, 1, 0, 581, 3, '1-581-3', NULL, '2015-03-03 21:40:56', '2015-03-03 22:40:56'),
(10, 1, 0, 582, 3, '1-582-3', NULL, '2015-03-03 21:40:56', '2015-03-03 22:40:56'),
(11, 1, 0, 583, 3, '1-583-3', NULL, '2015-03-03 21:40:56', '2015-03-03 22:40:56'),
(12, 1, 0, 584, 3, '1-584-3', NULL, '2015-03-03 21:40:56', '2015-03-03 22:40:56'),
(13, 1, 0, 585, 3, '1-585-3', NULL, '2015-03-03 21:40:56', '2015-03-03 22:40:56'),
(14, 1, 0, 586, 3, '1-586-3', NULL, '2015-03-03 21:40:56', '2015-03-03 22:40:56'),
(15, 1, 0, 587, 3, '1-587-3', NULL, '2015-03-03 21:40:56', '2015-03-03 22:40:56'),
(16, 1, 0, 588, 3, '1-588-3', NULL, '2015-03-03 21:40:56', '2015-03-03 22:40:56'),
(17, 1, 0, 589, 3, '1-589-3', NULL, '2015-03-03 21:40:56', '2015-03-03 22:40:56'),
(18, 1, 0, 1, 1, '1-1-1', NULL, '2015-03-03 21:40:56', '2015-03-03 22:40:56');

-- --------------------------------------------------------

--
-- Structure de la table `usr_profils_experiences`
--

CREATE TABLE `usr_profils_experiences` (
  `id_profil_experience` int(11) NOT NULL,
  `id_prestataire_profil` int(11) NOT NULL,
  `exp_debut` date NOT NULL,
  `exp_fin` date NOT NULL,
  `exp_intitule` varchar(100) NOT NULL,
  `exp_entreprise` varchar(255) NOT NULL,
  `exp_description` longtext NOT NULL,
  `composante` varchar(14) NOT NULL,
  `cree_le` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifie_le` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `usr_profils_experiences`
--

INSERT INTO `usr_profils_experiences` (`id_profil_experience`, `id_prestataire_profil`, `exp_debut`, `exp_fin`, `exp_intitule`, `exp_entreprise`, `exp_description`, `composante`, `cree_le`, `modifie_le`) VALUES
(1, 1, '2014-01-01', '2015-03-03', 'Directeur technique ', 'Artsper', '- Direction globale de l''IT\r\n- Pilotage et suivi des projets\r\n- Analyse des besoins marchés\r\n- Cahier des charges\r\n- Architecture des services\r\n- Management d''équipe\r\n- Coordination et pilotage des lntervenants extérieurs (hébergement, seo, free-lance..)\r\n- Participation et décision sur les recrutements prévus\r\n- Planification \r\n- R&D sur des problématiques Liées aux market places (web marketing, cross selling...)\r\n- Gestion et suivi des tests\r\n- Leadership technique PHP (fork MVC home made)\r\n- Création et implémentation des nouvelles fonctionnalités\r\n- implementation de webservices (php curl, json) dont notamment mandrill (envoi massif de newsletter ~100 000 mails  plus recuperation des stats)', '1-2014-01-01', '2015-03-03 21:40:55', '2015-03-03 22:40:55'),
(2, 1, '2011-01-01', '2014-01-01', 'Chef de projet / Développeur Web PHP 5 Objet, CSS3, HTML 5, MySQL, jQuery', 'Keyrium SAS (Anciennement TvOnZeweb)', 'Consultant depuis Janvier 2011 pour la société Keyrium. Intervention coté client, dans le développement de plateformes web hautement interactives  basées sur les technologies JavaScript (maitrise du Framework jQuery), HTML5 et CSS3. Développement par ailleurs des web Apps responsives destinées aux supports mobiles et tablettes en utilisant des technologies récentes (gestion du cache manifest, gestion de flux http pour utilisation de lecteurs vidéos mobile, gestion des requêtes Ajax).\r\n\r\nCoté serveur, maitrise parfaite de PHP 5 orienté objet ainsi que de solides connaissances en MySql (création et optimisation de requêtes lourdes dédiées à l’affichage et la gestion de données). Connaissances de  l’environnement Linux permettant de configurer aisément des serveurs dédiés au web (gestion des users, des vhosts, installation de librairies, installation et configuration de MySQL)\r\n\r\n\r\nPrincipales réalisations :\r\nGestion, expertise technique et développement du site http://www.artsper.com \r\no	Rédaction d’un rapport d’expertise sur l’architecture technique du site existant\r\no	Quantification des nouvelles fonctionnalités demandées par la maitrise d’ouvrage\r\no	Conception et développement  des nouvelles fonctionnalités en PHP 5 et MySQL  \r\n\r\nDéveloppement de la plateforme communautaire http://www .neofreelo.com\r\no	Recueil des besoins définis par la maitrise d’ouvrage\r\no	Rédaction des spécifications fonctionnelles générales\r\no	Mise en place des reporting de développement de l’outil\r\no	Rédaction des cahiers de recettes  \r\no	Mise en place d’une structure MVC coté serveur embarquant le générateur de templates Smarty  pour l’affichage des pages.\r\no	Conception et développement du front end (Html, CSS, jQuery)\r\no	Conception et mise en place de l’architecture serveur (Linux, Apache, MySQL) sur trois environnements distincts (développement, qualification et production)\r\no	Installation de librairies permettant la génération de fichier PDF en Jpeg ainsi que l’indexation des contenus textuels en base de donnée (Text Extraction Toolkit).\r\no	Conception et développement de web services externalisés jabber et bosh pour la création d’un système de Tchat embarqué utilisant la technologie XMPP.\r\no	Création et optimisation de requêtes lourdes en MySQL pour l’affichage des données sur la plateforme.\r\no	Mise en place de crons périodiques pour l’automatisation des tâches redondantes.\r\n\r\n\r\nDéveloppement de web l’application mobile Orange Convention 2012\r\no	Recueil des besoins définis par le client.\r\no	Conception du modèle de données (MCD).\r\no	Conception et développement de l ‘environnement technique PHP 5.\r\no	Intégration du front end utilisant les technologies mobiles responsives ainsi que le requêtage de données via Ajax et Json.\r\no	Intégration d’un lecteur vidéo utilisant le protocole http pour la gestion d’un pseudo streaming compatible IOS.\r\no	Intégration d’un Tchat  embarqué utilisant les technologies Ajax, XML et Json pour les échanges de données.\r\no	Recherche et développement d’une technologie tactile pour la manipulation de la galerie photo sur Smartphone.\r\no	Redaction de la documentation technique et des procédures de la plateforme\r\n\r\nDéveloppement  de la web application tablette du bar à application Orange pour le mondial de l’automobile 2012\r\no	Recueil des besoins définis par les équipes du pilotage commercial.\r\no	Conception du modèle de données (MCD).\r\no	Conception et développement de l ‘environnement technique PHP 5.\r\no	Intégration  du front end utilisant les technologie mobiles responsives ainsi que le requêtage de données via Ajax et Json.\r\no	Intégration d’un système de QRcode  (flashcode évolué) pour le téléchargement sur les stores respectifs des Smartphones visiteurs. \r\n', '1-2011-01-01', '2015-03-03 21:40:55', '2015-03-03 22:40:55'),
(3, 1, '2009-01-01', '2011-01-01', 'Développeur web PHP  objet, MySQL, jQuery, CSS3 et HTML5', 'Clicpostal', 'Clicpostal est une carterie en ligne spécialisée dans la vente de faire-part de naissance et divers autres produits tels que des calendriers photos, des albums photos, marque-page...\r\n\r\nMissions principales : \r\n\r\n-	Recueil des besoins exprimés par les équipes marketing.\r\no	Mise en lumière des différents produits de la marque par le biais de pages de présentation et de parcours utilisateurs intuitifs et ergonomiques utilisant les technologies Ajax, jQuery et HTML5. \r\no	Intégration et envoi de newsletters promotionnelles en HTML et CSS (~80K prospects)\r\no	Développement et déploiement de scripts PHP afin d''optimiser de façon significative la production et la gestion de nos produits (Module de recherche Back Office,  génératrice de templates  pour les newsletters).\r\no	Recherche et développement d’un algorithme PHP utilisant la librairie GD Image afin de classifier les différents produits par couleur.\r\no	Création, Intégration et développement du blog http://www.clicpostal.com/blog/ sous WordPress.\r\n', '1-2009-01-01', '2015-03-03 21:40:55', '2015-03-03 22:40:55'),
(4, 1, '2008-01-01', '2009-01-01', 'Infographiste / Intégrateur', 'Transatel', '\r\nMissions principales : \r\n\r\no	Recueil des besoins exprimés par les équipes marketing.\r\no	Création de chartes graphiques.\r\no	Conception et Intégration HTML et CSS des différentes pages de contenus pour le site www.transatel.com et www.eurokeitai.com.\r\no	Conception et intégration de newsletters.', '1-2008-01-01', '2015-03-03 21:40:55', '2015-03-03 22:40:55');

-- --------------------------------------------------------

--
-- Structure de la table `usr_profils_formations`
--

CREATE TABLE `usr_profils_formations` (
  `id_profil_formation` int(11) NOT NULL,
  `id_prestataire_profil` int(11) NOT NULL,
  `form_debut` date NOT NULL,
  `form_fin` date NOT NULL,
  `form_description` longtext NOT NULL,
  `form_diplome` varchar(50) NOT NULL,
  `form_ecole` varchar(50) NOT NULL,
  `composante` varchar(16) NOT NULL COMMENT 'id_profil-date_fin',
  `cree_le` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifie_le` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `usr_profils_formations`
--

INSERT INTO `usr_profils_formations` (`id_profil_formation`, `id_prestataire_profil`, `form_debut`, `form_fin`, `form_description`, `form_diplome`, `form_ecole`, `composante`, `cree_le`, `modifie_le`) VALUES
(1, 1, '2013-01-01', '0000-00-00', 'CNAM la Défense \r\nObtention UE NSY115 – Conduite d’un projet informatique \r\n\r\no	Le modèle de développement d''un logiciel et le cycle de vie.\r\no	La conduite d''un projet informatique et ses différentes phases : de l''étude préalable à la mise en oeuvre - Production des documents.\r\no	Les diverses approches des méthodes de conception - exemple: UML\r\no	Estimation des charges, planning\r\no	Structure des p', '-', 'CNAM Paris : Conservatoire National Des Arts Et Mé', '1-2013-01-01', '2015-03-03 21:40:55', '2015-03-03 22:40:55'),
(2, 1, '2007-01-01', '0000-00-00', 'Licence spécialisée dans les métiers numériques', '-', 'Institut Universitaire De Technologie De Haguenau', '1-2007-01-01', '2015-03-03 21:40:55', '2015-03-03 22:40:55');

-- --------------------------------------------------------

--
-- Structure de la table `usr_profils_localites`
--

CREATE TABLE `usr_profils_localites` (
  `id_profil_localite` int(255) NOT NULL,
  `id_prestataire_profil` int(255) NOT NULL,
  `lo_departement` varchar(255) DEFAULT NULL,
  `lo_quartier` varchar(255) DEFAULT NULL,
  `lo_ville` varchar(45) DEFAULT NULL,
  `lo_pays` varchar(45) DEFAULT NULL,
  `lo_region` varchar(45) DEFAULT NULL,
  `lo_pays_short` varchar(45) DEFAULT NULL,
  `lo_code_postal` varchar(45) DEFAULT NULL,
  `lo_region_short` varchar(45) DEFAULT NULL,
  `lo_departement_short` varchar(45) DEFAULT NULL,
  `var` varchar(45) DEFAULT NULL,
  `modifie_le` datetime NOT NULL,
  `cree_le` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `usr_videosession`
--

CREATE TABLE `usr_videosession` (
  `id_videosession` int(11) NOT NULL,
  `id_client` int(11) NOT NULL,
  `id_identifiant` int(11) NOT NULL,
  `vs_id_mission` int(11) NOT NULL,
  `id_prestataire_profil` int(11) NOT NULL,
  `vs_hostaccess` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `vs_guestaccess` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `vs_horraires` datetime NOT NULL,
  `vs_statut` smallint(6) NOT NULL,
  `vs_host_commentaire` text COLLATE utf8_unicode_ci NOT NULL,
  `cree_le` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifie_le` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `adm_categories`
--
ALTER TABLE `adm_categories`
  ADD PRIMARY KEY (`id_categorie`),
  ADD UNIQUE KEY `adm_categorie_es_2` (`adm_categorie_es`),
  ADD UNIQUE KEY `adm_categorie_en_2` (`adm_categorie_en`);
ALTER TABLE `adm_categories` ADD FULLTEXT KEY `adm_categorie_fr` (`adm_categorie_fr`);
ALTER TABLE `adm_categories` ADD FULLTEXT KEY `adm_categorie_es` (`adm_categorie_es`);
ALTER TABLE `adm_categories` ADD FULLTEXT KEY `adm_categorie_en` (`adm_categorie_en`);

--
-- Index pour la table `adm_civilites`
--
ALTER TABLE `adm_civilites`
  ADD PRIMARY KEY (`id_civilite`);
ALTER TABLE `adm_civilites` ADD FULLTEXT KEY `adm_civilite_fr` (`adm_civilite_fr`);
ALTER TABLE `adm_civilites` ADD FULLTEXT KEY `adm_civilite_en` (`adm_civilite_en`);
ALTER TABLE `adm_civilites` ADD FULLTEXT KEY `adm_civilite_es` (`adm_civilite_es`);

--
-- Index pour la table `adm_competences`
--
ALTER TABLE `adm_competences`
  ADD PRIMARY KEY (`id_competence`),
  ADD UNIQUE KEY `adm_competence_fr_2` (`adm_competence_fr`),
  ADD UNIQUE KEY `adm_competence_en_2` (`adm_competence_en`),
  ADD UNIQUE KEY `adm_competence_es_2` (`adm_competence_es`);
ALTER TABLE `adm_competences` ADD FULLTEXT KEY `adm_competence_fr` (`adm_competence_fr`);
ALTER TABLE `adm_competences` ADD FULLTEXT KEY `adm_competence_en` (`adm_competence_en`);
ALTER TABLE `adm_competences` ADD FULLTEXT KEY `adm_competence_es` (`adm_competence_es`);

--
-- Index pour la table `adm_contrats`
--
ALTER TABLE `adm_contrats`
  ADD PRIMARY KEY (`id_contrat`);
ALTER TABLE `adm_contrats` ADD FULLTEXT KEY `adm_contrat_fr` (`adm_contrat_fr`);
ALTER TABLE `adm_contrats` ADD FULLTEXT KEY `adm_contrat_es` (`adm_contrat_es`);
ALTER TABLE `adm_contrats` ADD FULLTEXT KEY `adm_contrat_en` (`adm_contrat_en`);

--
-- Index pour la table `adm_entretiens_statuts`
--
ALTER TABLE `adm_entretiens_statuts`
  ADD PRIMARY KEY (`id_entretien_statut`);

--
-- Index pour la table `adm_etudes`
--
ALTER TABLE `adm_etudes`
  ADD PRIMARY KEY (`id_etude`),
  ADD UNIQUE KEY `adm_etude_fr_2` (`adm_etude_fr`),
  ADD UNIQUE KEY `adm_etude_es_2` (`adm_etude_es`),
  ADD UNIQUE KEY `adm_etude_en_2` (`adm_etude_en`);
ALTER TABLE `adm_etudes` ADD FULLTEXT KEY `adm_etude_fr` (`adm_etude_fr`);
ALTER TABLE `adm_etudes` ADD FULLTEXT KEY `adm_etude_es` (`adm_etude_es`);
ALTER TABLE `adm_etudes` ADD FULLTEXT KEY `adm_etude_en` (`adm_etude_en`);

--
-- Index pour la table `adm_experiences`
--
ALTER TABLE `adm_experiences`
  ADD PRIMARY KEY (`id_experience`);
ALTER TABLE `adm_experiences` ADD FULLTEXT KEY `adm_experience_fr` (`adm_experience_fr`);
ALTER TABLE `adm_experiences` ADD FULLTEXT KEY `adm_experience_es` (`adm_experience_es`);
ALTER TABLE `adm_experiences` ADD FULLTEXT KEY `adm_experience_en` (`adm_experience_en`);

--
-- Index pour la table `adm_langues`
--
ALTER TABLE `adm_langues`
  ADD PRIMARY KEY (`id_langue`),
  ADD UNIQUE KEY `adm_langue_fr_2` (`adm_langue_fr`),
  ADD UNIQUE KEY `adlm_langue_es_2` (`adm_langue_es`),
  ADD UNIQUE KEY `adm_langue_en_2` (`adm_langue_en`),
  ADD UNIQUE KEY `adm_langue_en_3` (`adm_langue_en`);
ALTER TABLE `adm_langues` ADD FULLTEXT KEY `adm_langue_fr` (`adm_langue_fr`);
ALTER TABLE `adm_langues` ADD FULLTEXT KEY `adlm_langue_es` (`adm_langue_es`);
ALTER TABLE `adm_langues` ADD FULLTEXT KEY `adm_langue_en` (`adm_langue_en`);

--
-- Index pour la table `adm_missions_statuts`
--
ALTER TABLE `adm_missions_statuts`
  ADD PRIMARY KEY (`id_mission_statut`);

--
-- Index pour la table `adm_monnaies`
--
ALTER TABLE `adm_monnaies`
  ADD PRIMARY KEY (`id_monnaie`);
ALTER TABLE `adm_monnaies` ADD FULLTEXT KEY `adm_monnaie` (`adm_monnaie`);

--
-- Index pour la table `adm_niveaux`
--
ALTER TABLE `adm_niveaux`
  ADD PRIMARY KEY (`id_niveau`);
ALTER TABLE `adm_niveaux` ADD FULLTEXT KEY `adm_niveau_fr` (`adm_niveau_fr`);
ALTER TABLE `adm_niveaux` ADD FULLTEXT KEY `adm_niveau_en` (`adm_niveau_en`);
ALTER TABLE `adm_niveaux` ADD FULLTEXT KEY `adm_niveau_es` (`adm_niveau_es`);

--
-- Index pour la table `adm_postes`
--
ALTER TABLE `adm_postes`
  ADD PRIMARY KEY (`id_poste`),
  ADD UNIQUE KEY `adm_poste_es` (`adm_poste_es`),
  ADD UNIQUE KEY `adm_poste_fr` (`adm_poste_fr`),
  ADD UNIQUE KEY `adm_poste_en` (`adm_poste_en`);
ALTER TABLE `adm_postes` ADD FULLTEXT KEY `adm_poste_fr_2` (`adm_poste_fr`);
ALTER TABLE `adm_postes` ADD FULLTEXT KEY `adm_poste_es_2` (`adm_poste_es`);
ALTER TABLE `adm_postes` ADD FULLTEXT KEY `adm_poste_en_2` (`adm_poste_en`);

--
-- Index pour la table `adm_reponses_statuts`
--
ALTER TABLE `adm_reponses_statuts`
  ADD PRIMARY KEY (`id_reponse_statut`);

--
-- Index pour la table `adm_structures`
--
ALTER TABLE `adm_structures`
  ADD PRIMARY KEY (`id_structure`);
ALTER TABLE `adm_structures` ADD FULLTEXT KEY `adm_structure_fr` (`adm_structure_fr`);
ALTER TABLE `adm_structures` ADD FULLTEXT KEY `adm_structure_es` (`adm_structure_es`);
ALTER TABLE `adm_structures` ADD FULLTEXT KEY `adm_structure_en` (`adm_structure_en`);

--
-- Index pour la table `tra_pages`
--
ALTER TABLE `tra_pages`
  ADD PRIMARY KEY (`id_page`);
ALTER TABLE `tra_pages` ADD FULLTEXT KEY `titre` (`titre`);
ALTER TABLE `tra_pages` ADD FULLTEXT KEY `nom` (`nom`);

--
-- Index pour la table `tra_page_traduction`
--
ALTER TABLE `tra_page_traduction`
  ADD PRIMARY KEY (`id_page_traduction`);

--
-- Index pour la table `tra_traductions`
--
ALTER TABLE `tra_traductions`
  ADD PRIMARY KEY (`id_traduction`),
  ADD UNIQUE KEY `trad_cle_unique` (`trad_cle_unique`);

--
-- Index pour la table `usr_clients`
--
ALTER TABLE `usr_clients`
  ADD PRIMARY KEY (`id_client`);
ALTER TABLE `usr_clients` ADD FULLTEXT KEY `cl_entreprise` (`cl_entreprise`);
ALTER TABLE `usr_clients` ADD FULLTEXT KEY `cl_presentation` (`cl_presentation`);
ALTER TABLE `usr_clients` ADD FULLTEXT KEY `cl_siteweb` (`cl_siteweb`);

--
-- Index pour la table `usr_clients_managers`
--
ALTER TABLE `usr_clients_managers`
  ADD PRIMARY KEY (`id_client_manager`),
  ADD KEY `id_identifiant` (`id_identifiant`),
  ADD KEY `id_client` (`id_client`);
ALTER TABLE `usr_clients_managers` ADD FULLTEXT KEY `mg_nom` (`mg_nom`);
ALTER TABLE `usr_clients_managers` ADD FULLTEXT KEY `mg_prenom` (`mg_prenom`);

--
-- Index pour la table `usr_clients_messages`
--
ALTER TABLE `usr_clients_messages`
  ADD PRIMARY KEY (`id_client_message`),
  ADD KEY `id_identifiant` (`id_identifiant`);

--
-- Index pour la table `usr_files`
--
ALTER TABLE `usr_files`
  ADD PRIMARY KEY (`id_file`),
  ADD UNIQUE KEY `fichier_reference_2` (`fichier_reference`),
  ADD KEY `id_identifiant` (`id_identifiant`),
  ADD KEY `id_profil` (`id_prestataire_profil`);
ALTER TABLE `usr_files` ADD FULLTEXT KEY `fichier_reference` (`fichier_reference`);
ALTER TABLE `usr_files` ADD FULLTEXT KEY `fichier_description` (`fichier_description`);

--
-- Index pour la table `usr_identifiants`
--
ALTER TABLE `usr_identifiants`
  ADD PRIMARY KEY (`id_identifiant`),
  ADD UNIQUE KEY `usr_email_2` (`usr_email`);
ALTER TABLE `usr_identifiants` ADD FULLTEXT KEY `usr_email` (`usr_email`);

--
-- Index pour la table `usr_jabbers`
--
ALTER TABLE `usr_jabbers`
  ADD PRIMARY KEY (`id_jabber`),
  ADD UNIQUE KEY `id_jabber` (`id_jabber`),
  ADD UNIQUE KEY `id_identifiant` (`id_identifiant`),
  ADD UNIQUE KEY `jbr_loggin` (`jbr_loggin`),
  ADD UNIQUE KEY `jbr_password` (`jbr_password`),
  ADD UNIQUE KEY `jbr_password_2` (`jbr_password`),
  ADD UNIQUE KEY `jbr_password_3` (`jbr_password`);

--
-- Index pour la table `usr_logs`
--
ALTER TABLE `usr_logs`
  ADD PRIMARY KEY (`id_log`),
  ADD KEY `id_identifiant` (`id_identifiant`);

--
-- Index pour la table `usr_managers_favoris`
--
ALTER TABLE `usr_managers_favoris`
  ADD PRIMARY KEY (`id_favori`),
  ADD UNIQUE KEY `composante` (`composante`),
  ADD KEY `id_identifiant` (`id_identifiant`),
  ADD KEY `id_profil` (`id_prestataire_profil`);

--
-- Index pour la table `usr_missions`
--
ALTER TABLE `usr_missions`
  ADD PRIMARY KEY (`id_mission`),
  ADD KEY `id_client` (`id_client`),
  ADD KEY `id_identifiant` (`id_identifiant`),
  ADD KEY `id_categorie` (`id_categorie`);
ALTER TABLE `usr_missions` ADD FULLTEXT KEY `mi_reference` (`mi_reference`);
ALTER TABLE `usr_missions` ADD FULLTEXT KEY `mi_titre` (`mi_titre`,`mi_description`);
ALTER TABLE `usr_missions` ADD FULLTEXT KEY `mi_titre_2` (`mi_titre`);
ALTER TABLE `usr_missions` ADD FULLTEXT KEY `mi_description` (`mi_description`);
ALTER TABLE `usr_missions` ADD FULLTEXT KEY `lo_quartier` (`lo_quartier`,`lo_ville`,`lo_code_postal`,`lo_departement_short`,`lo_departement`,`lo_region_short`,`lo_region`,`lo_pays_short`,`lo_pays`);

--
-- Index pour la table `usr_missions_complements`
--
ALTER TABLE `usr_missions_complements`
  ADD PRIMARY KEY (`id_mission_complement`),
  ADD UNIQUE KEY `composante` (`composante`),
  ADD KEY `id_mission` (`id_mission`),
  ADD KEY `id_complement` (`id_complement`),
  ADD KEY `complement` (`complement`),
  ADD KEY `composante_2` (`composante`);

--
-- Index pour la table `usr_missions_reponses`
--
ALTER TABLE `usr_missions_reponses`
  ADD PRIMARY KEY (`id_mission_reponse`),
  ADD KEY `id_mission` (`id_mission`),
  ADD KEY `id_profil` (`id_prestataire_profil`),
  ADD KEY `id_identifiant` (`id_identifiant`);

--
-- Index pour la table `usr_prestataires`
--
ALTER TABLE `usr_prestataires`
  ADD PRIMARY KEY (`id_prestataire`),
  ADD KEY `id_identifiant` (`id_identifiant`);
ALTER TABLE `usr_prestataires` ADD FULLTEXT KEY `pr_entreprise` (`pr_entreprise`);
ALTER TABLE `usr_prestataires` ADD FULLTEXT KEY `pr_presentation` (`pr_presentation`);

--
-- Index pour la table `usr_prestataires_favoris`
--
ALTER TABLE `usr_prestataires_favoris`
  ADD PRIMARY KEY (`id_prestataire_favoris`),
  ADD UNIQUE KEY `composante` (`composante`),
  ADD KEY `id_mission` (`id_mission`),
  ADD KEY `id_identifiant` (`id_identifiant`);

--
-- Index pour la table `usr_prestataires_profils`
--
ALTER TABLE `usr_prestataires_profils`
  ADD PRIMARY KEY (`id_prestataire_profil`),
  ADD KEY `id_prestataire` (`id_prestataire`),
  ADD KEY `id_experience` (`id_experience`),
  ADD KEY `id_identifiant` (`id_identifiant`);
ALTER TABLE `usr_prestataires_profils` ADD FULLTEXT KEY `pf_reference` (`pf_reference`);
ALTER TABLE `usr_prestataires_profils` ADD FULLTEXT KEY `pf_nom` (`pf_nom`);
ALTER TABLE `usr_prestataires_profils` ADD FULLTEXT KEY `pf_prenom` (`pf_prenom`);
ALTER TABLE `usr_prestataires_profils` ADD FULLTEXT KEY `pf_siteweb` (`pf_siteweb`);
ALTER TABLE `usr_prestataires_profils` ADD FULLTEXT KEY `pf_cv_titre` (`pf_cv_titre`);
ALTER TABLE `usr_prestataires_profils` ADD FULLTEXT KEY `pf_cv_description` (`pf_cv_description`);
ALTER TABLE `usr_prestataires_profils` ADD FULLTEXT KEY `pf_cv_pdf_string` (`pf_cv_pdf_string`,`pf_cv_titre`,`pf_cv_description`);

--
-- Index pour la table `usr_profils_complements`
--
ALTER TABLE `usr_profils_complements`
  ADD PRIMARY KEY (`id_profil_complement`),
  ADD UNIQUE KEY `composante` (`composante`),
  ADD KEY `id_prestataire_profil` (`id_prestataire_profil`),
  ADD KEY `id_niveau` (`id_niveau`),
  ADD KEY `id_complement` (`id_complement`);

--
-- Index pour la table `usr_profils_experiences`
--
ALTER TABLE `usr_profils_experiences`
  ADD PRIMARY KEY (`id_profil_experience`),
  ADD UNIQUE KEY `composante` (`composante`),
  ADD KEY `id_prestataire_profil` (`id_prestataire_profil`);
ALTER TABLE `usr_profils_experiences` ADD FULLTEXT KEY `exp_description` (`exp_description`);
ALTER TABLE `usr_profils_experiences` ADD FULLTEXT KEY `exp_entreprise` (`exp_entreprise`,`exp_description`);
ALTER TABLE `usr_profils_experiences` ADD FULLTEXT KEY `exp_entreprise_2` (`exp_entreprise`);

--
-- Index pour la table `usr_profils_formations`
--
ALTER TABLE `usr_profils_formations`
  ADD PRIMARY KEY (`id_profil_formation`),
  ADD UNIQUE KEY `composane` (`composante`),
  ADD KEY `id_prestataire_profil` (`id_prestataire_profil`);
ALTER TABLE `usr_profils_formations` ADD FULLTEXT KEY `form_diplome` (`form_diplome`);
ALTER TABLE `usr_profils_formations` ADD FULLTEXT KEY `form_ecole` (`form_ecole`);
ALTER TABLE `usr_profils_formations` ADD FULLTEXT KEY `form_description` (`form_description`,`form_ecole`,`form_diplome`);
ALTER TABLE `usr_profils_formations` ADD FULLTEXT KEY `form_description_2` (`form_description`);

--
-- Index pour la table `usr_profils_localites`
--
ALTER TABLE `usr_profils_localites`
  ADD PRIMARY KEY (`id_profil_localite`),
  ADD KEY `id_profil` (`id_prestataire_profil`);

--
-- Index pour la table `usr_videosession`
--
ALTER TABLE `usr_videosession`
  ADD PRIMARY KEY (`id_videosession`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `adm_categories`
--
ALTER TABLE `adm_categories`
  MODIFY `id_categorie` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;
--
-- AUTO_INCREMENT pour la table `adm_civilites`
--
ALTER TABLE `adm_civilites`
  MODIFY `id_civilite` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `adm_competences`
--
ALTER TABLE `adm_competences`
  MODIFY `id_competence` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=590;
--
-- AUTO_INCREMENT pour la table `adm_contrats`
--
ALTER TABLE `adm_contrats`
  MODIFY `id_contrat` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `adm_entretiens_statuts`
--
ALTER TABLE `adm_entretiens_statuts`
  MODIFY `id_entretien_statut` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `adm_etudes`
--
ALTER TABLE `adm_etudes`
  MODIFY `id_etude` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT pour la table `adm_experiences`
--
ALTER TABLE `adm_experiences`
  MODIFY `id_experience` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT pour la table `adm_langues`
--
ALTER TABLE `adm_langues`
  MODIFY `id_langue` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=181;
--
-- AUTO_INCREMENT pour la table `adm_missions_statuts`
--
ALTER TABLE `adm_missions_statuts`
  MODIFY `id_mission_statut` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `adm_monnaies`
--
ALTER TABLE `adm_monnaies`
  MODIFY `id_monnaie` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `adm_niveaux`
--
ALTER TABLE `adm_niveaux`
  MODIFY `id_niveau` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `adm_postes`
--
ALTER TABLE `adm_postes`
  MODIFY `id_poste` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1026;
--
-- AUTO_INCREMENT pour la table `adm_reponses_statuts`
--
ALTER TABLE `adm_reponses_statuts`
  MODIFY `id_reponse_statut` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `adm_structures`
--
ALTER TABLE `adm_structures`
  MODIFY `id_structure` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `tra_pages`
--
ALTER TABLE `tra_pages`
  MODIFY `id_page` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT pour la table `tra_page_traduction`
--
ALTER TABLE `tra_page_traduction`
  MODIFY `id_page_traduction` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=871;
--
-- AUTO_INCREMENT pour la table `tra_traductions`
--
ALTER TABLE `tra_traductions`
  MODIFY `id_traduction` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=871;
--
-- AUTO_INCREMENT pour la table `usr_clients`
--
ALTER TABLE `usr_clients`
  MODIFY `id_client` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `usr_clients_managers`
--
ALTER TABLE `usr_clients_managers`
  MODIFY `id_client_manager` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `usr_clients_messages`
--
ALTER TABLE `usr_clients_messages`
  MODIFY `id_client_message` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `usr_files`
--
ALTER TABLE `usr_files`
  MODIFY `id_file` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `usr_identifiants`
--
ALTER TABLE `usr_identifiants`
  MODIFY `id_identifiant` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `usr_jabbers`
--
ALTER TABLE `usr_jabbers`
  MODIFY `id_jabber` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `usr_logs`
--
ALTER TABLE `usr_logs`
  MODIFY `id_log` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `usr_managers_favoris`
--
ALTER TABLE `usr_managers_favoris`
  MODIFY `id_favori` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `usr_missions`
--
ALTER TABLE `usr_missions`
  MODIFY `id_mission` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `usr_missions_complements`
--
ALTER TABLE `usr_missions_complements`
  MODIFY `id_mission_complement` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `usr_missions_reponses`
--
ALTER TABLE `usr_missions_reponses`
  MODIFY `id_mission_reponse` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `usr_prestataires`
--
ALTER TABLE `usr_prestataires`
  MODIFY `id_prestataire` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `usr_prestataires_favoris`
--
ALTER TABLE `usr_prestataires_favoris`
  MODIFY `id_prestataire_favoris` int(255) NOT NULL AUTO_INCREMENT COMMENT '	';
--
-- AUTO_INCREMENT pour la table `usr_prestataires_profils`
--
ALTER TABLE `usr_prestataires_profils`
  MODIFY `id_prestataire_profil` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `usr_profils_complements`
--
ALTER TABLE `usr_profils_complements`
  MODIFY `id_profil_complement` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT pour la table `usr_profils_experiences`
--
ALTER TABLE `usr_profils_experiences`
  MODIFY `id_profil_experience` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `usr_profils_formations`
--
ALTER TABLE `usr_profils_formations`
  MODIFY `id_profil_formation` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `usr_profils_localites`
--
ALTER TABLE `usr_profils_localites`
  MODIFY `id_profil_localite` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `usr_videosession`
--
ALTER TABLE `usr_videosession`
  MODIFY `id_videosession` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
