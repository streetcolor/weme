<?php

//Initialisation de l'environnement
include('./config/config_init.php');
//Le routing permet de selectionner le controleur en fonction de la page sur la quelle on se trouve
//Le controleur determine ensuite l'action � accomplir sur le page en fonction du $_REQUEST["action"]
include('./config/routing.php');

/****************************************************************************************************************/
/********************************    ICI ON affiche nos templates  **********************************************/
/****************************************************************************************************************/
?>
<!DOCTYPE html>
<html lang="en">
 
  <head>
    <?PHP
        $smarty->display(_TPL_ .$environnement.'/template/meta.tpl');
        $smarty->display(_TPL_ .$environnement.'/template/css.tpl');
    ?>
  </head>

  <body id="<?PHP echo $environnement ?>" class="<?PHP echo $environnement ?>">

<?PHP

$smarty->display(_TPL_ .$environnement.'/template/header.tpl');


//Plusieurs cas de figure
//Soit on affiche la page qui est d�finie manuellement � la sortie du controlleur via la variable $setTemplate
//Soit on affiche la variable pass�e dans l'url via $_GET[page]
//Si la page existe bien dans le r�peroire des vue on l'affiche sinon on afficle le template 404.tpl

//On demarre la temporisation
ob_start('Tools::replaceFlush');

$page = isset($setTemplate) &&  !empty($setTemplate) ? $setTemplate : $_GET['page'];

if (isset($page) && file_exists(_TPL_.$environnement.'/pages/'.str_replace('.', '', $page).'.tpl')){
    $smarty->display(_TPL_.$environnement.'/pages/'.$page.'.tpl');
}

//Si le template que l'on tente d'appeler n'exsite pas
elseif(isset($page) && !file_exists(_TPL_.$environnement.'/pages/'.str_replace('.', '', $page).'.tpl')){
    $smarty->display(_TPL_.$environnement.'/pages/404.tpl');
}
//Sinon on maintien la home
else{
    $smarty->display(_TPL_ .$environnement.'/pages/home.tpl');
}

//On vide la temporisation
ob_end_flush();


$smarty->display(_TPL_ .$environnement.'/template/footer.tpl');
$smarty->display(_TPL_ .$environnement.'/template/js.tpl');

if(DEBUG){

    $smarty->assign('debug', DBConnect::$request);
    $smarty->display(_TPL_ .$environnement.'/template/debug.tpl');
    
    $options = array(
       'HTML_DIV_images_path' => 'http://5.39.77.136/debugBar/images', 
       'HTML_DIV_css_path' => 'http://5.39.77.136/debugBar/css', 
       'HTML_DIV_js_path' => 'http://5.39.77.136/debugBar/js'
   );   
?>
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo $options['HTML_DIV_css_path']; ?>/html_div.css" />
    <script type="text/javascript" src="<?php echo $options['HTML_DIV_js_path']; ?>/html_div.js"></script>
<?PHP

   include_once("../debugBar/PHP/Debug.php");
    
   // Debug object
   $Dbg = new PHP_Debug($options);
   $Dbg->add('DEBUG INFO');
   $Dbg->display(); 
   

}
?>



  </body>
</html>
