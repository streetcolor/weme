<?PHP
//Définition de la time zone
date_default_timezone_set('Europe/Paris');
//On affiche toutes les erreurs sauf les notice
error_reporting(E_ALL ^ E_NOTICE);
//On active le log des erreur dans un fichier
ini_set('log_errors', 'On');
//Dans ce ficheir
ini_set('error_log', '/logs/error.log');
ini_set('memory_limit', '64M'); // 
set_time_limit(600); // en secondes
ini_set('upload_max_filesize', '80M');
ini_set('post_max_size', '80M');

ini_set('display_errors', 0);
// Initialisation de la session
session_name("front");

session_start();
header("Cache-Control: no-cache");

// Chargement des variables Defines
require('defines.inc.php');
//Chargement d'environnement smarty
require(_TOOLS_ . 'smarty/libs/Smarty.class.php');

// On charge toutes la classes du Coeur (core) a l'aide d'un auto load
function chargerClasse($classname){
    if (is_file(_CORE_.$classname.'.class.php')){
	require_once _CORE_.$classname.'.class.php';
    }
}

spl_autoload_register('chargerClasse');

include_once('connect.inc.php');

//On force $page à sortir home si jamais le $_REQUEST est vide;
$page = $_REQUEST["page"] ? $_REQUEST["page"] : 'home';

// Initialisation Smarty
//global $smarty ;
$smarty = new Smarty();

$DBmembre = new DBMembre;
//Definition de l'environnement
switch(DBMembre::$id_type){
    
    case DBMembre::TYPE_CLIENT:
        $environnement = 'client';
        break;
    case DBMembre::TYPE_PRESTATAIRE :
        $environnement = 'prestataire';
        break;
    default;
        $environnement = 'visiteur';
        break;
}


$smarty->assign('environnement', $environnement);
 
//Temp : tableau des page variablisé :
$variab_page = array('visiteur-home', 'visiteur-cvteque', 'visiteur-missions','visiteur-mission', 'visiteur-inscription', 'visiteur-cl_inscription', 'visiteur-pr_inscription',  'visiteur-contact', 'visiteur-infos', 'visiteur-about', 'visiteur-services', 'visiteur-mentions','visiteur-sitemap',
		     'prestataire-home', 'prestataire-edit_moncompte', 'prestataire-edit_cv', 'prestataire-realisations', 'prestataire-missions', 'prestataire-my_missions', 'prestataire-new_response', 'prestataire-responses', 'prestataire-entretiens','prestataire-contact','prestataire-infos', 'prestataire-about', 'prestataire-services', 'prestataire-mentions','prestataire-sitemap',
		      'client-home', 'client-edit_moncompte', 'client-missions', 'client-new_mission', 'client-new_manager', 'client-new_entretien', 'client-new_message', 'client-edit_mission','client-contact','client-infos', 'client-about', 'client-services', 'client-mentions','client-sitemap', 'client-message');

$header_trad = new XMLEngine( "header", $environnement, $smarty);
$smarty->assign('header_trad', $header_trad);


//Si la page est dans le tableau des page déjà variabilisé on charge la trad, sinon ...page à variabiliser
if(in_array($environnement.'-'.$page, $variab_page)){
    
    //Chargement de la traduction de la page
    $corps_trad = new XMLEngine($page, $environnement);
    $smarty->assign('trad', $corps_trad);
    //Tools::debugVar($corps_trad, false);
}

$footer_trad = new XMLEngine( "footer", $environnement);
$smarty->assign('footer_trad', $footer_trad);

$global_trad = new XMLEngine( "globales");//Tools::debugVar($global_trad);
$smarty->assign('globale_trad', $global_trad);

$trad = array(
	    'global_trad'=>$global_trad,
	    'header_trad'=>$header_trad,
	    'corps_trad' =>$corps_trad,
	    'footer_trad'=>$footer_trad
	    );
	    
XMLEngine::setTradTab($trad);

$smarty->assign('_TOOLS_', _TOOLS_);
